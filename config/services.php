<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => 'pk_test_4x6o4S3xdLIrE8IlXTvvRqup',
        'secret' => 'sk_test_2NQbNhVj6YJbBioFdDB4iEpR',
    ],
    'google' => [
        //'client_id' => '',
        'client_id' => '585340303603-14b9tlep1g7hvq6llauljbmhh3d37r3e.apps.googleusercontent.com',
        'client_secret' => 'kZG0_pPBdcmMAug7_J7SSJLT',
        'redirect' => 'http://bsedemo.com/enterprise/user/social/google',
    ],

    'twitter' => [
        //'client_id' => '', 
        'client_id' => 'jEk8MjWI2l3iY6XCh9MZFaN5D',
        'client_secret' => 'kfC7pWdbFSV9QjOlbf2ToEQSOAwDVoSlzFNUFlIPlZMTeBjrmc',
        'redirect' => 'http://bsedemo.com/enterprise/user/social/twitter',
    ],
    'facebook' => [
        //'client_id' => '',
        'client_id' => '1826449870987844',
        'client_secret' => '682f42f7efe20fca19c15fce68e0ef0b',
        'redirect' => 'http://bsedemo.com/enterprise/user/social/facebook',
    ],  
    'linkedin' => [
        'client_id' => env('CLIENT_ID'),
        'client_secret' => env('CLIENT_SECRET'),
    ],

];
