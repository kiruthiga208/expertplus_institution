1. Institution Register Alter Fields

    ALTER TABLE `bse_institution` ADD `username` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL AFTER `type`, ADD `password` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL AFTER `username`;

2. Institution Registration Fields Stored in User Table - Completed

    Added user_type as institution in users table
    
    ALTER TABLE `bse_institution` ADD `user_id` int(11) NOT NULL AFTER `id`;

3. Institution Register Mobile No - Issue Fixed
  
    ALTER TABLE `bse_institution` CHANGE `contact_no` `contact_no` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL AFTER `email`;

4. Institution Dashboard (Department Module) - Completed

    CREATE TABLE `bse_departments` (
      `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `department` varchar(255) NOT NULL,
      `institution_id` int(11) NOT NULL,
      `status` enum('0','1') NOT NULL,
      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      `updated_at` timestamp NULL
    );

5. Institution Course Module - Completed

    CREATE TABLE `bse_institution_course` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `name` varchar(255) NOT NULL,
      `institution_id` int(11) NOT NULL,
      `department_id` int(11) NOT NULL,
      `status` enum('0','1') NOT NULL,
      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      `updated_at` timestamp NULL DEFAULT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

6. Semester Module - Completed

    CREATE TABLE `bse_institution_semester` (
      `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `institution_id` int(11) NOT NULL,
      `department_id` int(11) NOT NULL,
      `institution_course_id` int(11) NOT NULL,
      `semester` varchar(255) NOT NULL,
      `status` enum('0','1') NOT NULL,
      `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      `updated_at` timestamp NULL
    );

7. Student Registration - Completed

	ALTER TABLE `bse_users` ADD `institution_id` int(11) NOT NULL, ADD `department_id` int(11) NOT NULL AFTER `institution_id`, ADD `institution_course_id` int(11) NOT NULL AFTER `department_id`, ADD `semester_id` int(11) NOT NULL AFTER `institution_course_id`;

8. Create sidemenu for institution Dashboard

	Create sitemenu->sidemenu with permission user

	INSERT INTO `bse_menu` (`menu_id`, `parent_id`, `module`, `url`, `menu_name`, `menu_type`, `role_id`, `deep`, `ordering`, `position`, `menu_icons`, `active`, `access_data`, `allow_guest`, `menu_lang`) VALUES 
	(83,	0,	'departments',	'',	'Departments',	'internal',	NULL,	NULL,	NULL,	'sidebar',	'fa fa-book',	'1',	'{\"1\":\"0\",\"2\":\"0\",\"3\":\"1\"}',	NULL,	'[]'), 
	(84,	0,	'institutioncourse',	'',	'Institution Course',	'internal',	NULL,	NULL,	NULL,	'sidebar',	'fa fa-book',	'1',	'{\"1\":\"0\",\"2\":\"0\",\"3\":\"1\"}',	NULL,	'[]'),
	(85,	0,	'semester',	'',	'Semester',	'internal',	NULL,	NULL,	NULL,	'sidebar',	'icon-question',	'1',	'{\"1\":\"0\",\"2\":\"0\",\"3\":\"1\"}',	NULL,	'[]'),
	(86,	0,	'',	'students',	'Students',	'external',	NULL,	NULL,	NULL,	'sidebar',	'fa fa-users',	'1',	'{\"1\":\"0\",\"2\":\"0\",\"3\":\"1\"}',	NULL,	'[]') 
	ON DUPLICATE KEY UPDATE `menu_id` = VALUES(`menu_id`), `parent_id` = VALUES(`parent_id`), `module` = VALUES(`module`), `url` = VALUES(`url`), `menu_name` = VALUES(`menu_name`), `menu_type` = VALUES(`menu_type`), `role_id` = VALUES(`role_id`), `deep` = VALUES(`deep`), `ordering` = VALUES(`ordering`), `position` = VALUES(`position`), `menu_icons` = VALUES(`menu_icons`), `active` = VALUES(`active`), `access_data` = VALUES(`access_data`), `allow_guest` = VALUES(`allow_guest`), `menu_lang` = VALUES(`menu_lang`);