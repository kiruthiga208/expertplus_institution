<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\Models\bsetec;

class InvoiceController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'invoice';
	static $per_page	= '10';

	public function __construct()
	{
		$this->bsetec = new bsetec();
		$this->model = new Invoice();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'invoice',
			'return'	=> self::returnUrl()
			
		);
	}

	public function Index( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('invoice');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		$pay = $this->bsetec->get_payment_method_enable();
		$_pay[''] = \Lang::get('core.select');
		// $_pay['free']= \Lang::get('core.free');
		foreach ($pay as $key => $value) {
			$_pay[$value->code] = $value->code;
		}		
		$this->data['method'] = $_pay;

		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('invoice.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('invoice'); 
		}

		$this->data['id'] = $id;

		// For Payment method 
		$pay = $this->bsetec->get_payment_method_enable();
		$_pay[''] = \Lang::get('core.select');
		$_pay['free']= \Lang::get('core.free');
		foreach ($pay as $key => $value) {
			$_pay[$value->code] = $value->code;
		}		
		$this->data['method'] = $_pay;
		
		return view('invoice.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('invoice'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('invoice.view',$this->data);	
	}	

	function postSave( Request $request, $id =0)
	{
		$rules = array(
				'transaction_id' => 'required|numeric',	
				'customer_name' => 'required|regex:/^[\pL\s\-]+$/u',	
				'email' => 'required|email',	
				'course_title' => 'required|min:2',	
				'amount' => 'required|numeric',	
				'status' => 'required',	
				'payment_method' => 'required',	
				'invoice_number' => 'required|numeric',	
				'ordered_on' => 'required'
			);
		//$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('invoice');
			$newID = $this->model->insertRow($data , $request->input('id'));
			if( ($data['payment_method'] == 'cod' || $data['payment_method'] == 'bank') && ($data['status'] == 'completed')){ 
			 $checkexist = $this->model->coursetaken($data['course_title'],$data['email']);
			 if(count($checkexist)<=0){
			 		$user_id = $this->model->getuserid($data['email']);
			 		$course_id = $this->model->getcourseid($data['course_title']);
				 	$courseTaken = new CourseTaken;
	                $courseTaken->user_id = $user_id->id;
	                $courseTaken->course_id = $course_id->course_id;
	                $courseTaken->save();
			 	}
			}
			if(!is_null($request->input('apply')) && $request->input('id') !=0)
			{
				$return = 'invoice/update/'.$request->input('id').'?return='.self::returnUrl();
			} else {
				$return = 'invoice?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('invoice/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('invoice')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('invoice')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}			


}