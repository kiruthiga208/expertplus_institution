<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Instructors;
use Illuminate\Http\Request;
use App\Models\Core\Users;
use App\Models\CourseImages;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect, File ; 
use Intervention\Image\ImageManagerStatic as Image;


class InstructorsController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'instructors';
	static $per_page	= '10';

	public function __construct()
	{

		$this->model = new Instructors();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'instructors',
			'return'	=> self::returnUrl()
			
		);
	}

	public function Index( Request $request )
	{
		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? \bsetecHelpers::getdbprefix().'instructors.'.$request->input('sort') : \bsetecHelpers::getdbprefix().'instructors.id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query 

		// Filter Search for query		

		if(!is_null($request->input('search'))){
			$search = $request->input('search');
			$pos = strrpos($search, ":")+1;
			$value = substr($search, strpos($search, ":") + 1);    
			if(strtolower($value)=='active'){
				$_GET['search'] = substr_replace($search, 1, $pos, strlen($search));
			} else if(strtolower($value)=='inactive'){
				$_GET['search'] = substr_replace($search, 0, $pos, strlen($search));
			}
		}

		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('instructors');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		

		foreach($this->data['tableGrid'] as $key => $value){
			if($value['field']=='user_id')
				$this->data['tableGrid'][$key]['label'] = 'Username';
			else if($value['field']=='institution_id')
				$this->data['tableGrid'][$key]['label'] = 'Institution';
		}	

		foreach ($this->data['tableForm'] as $key => $value) {
			if($value['field']=='user_id') {
				$this->data['tableForm'][$key]['option']['lookup_value'] = 'username';
				$this->data['tableForm'][$key]['option']['lookup_table'] = "bse_users Where user_type='instructor'";
			}
			else if($value['field']=='institution_id') {
				$this->data['tableForm'][$key]['option']['lookup_value'] = 'name';
				$this->data['tableForm'][$key]['option']['lookup_table'] = "bse_institution Where status='1'";
			}
		}
// print_r($this->data['tableForm']);die();
		// Render into template
		return view('instructors.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->where('instructors.id', $id)
				->Select('instructors.*', 'users.username', 'users.email', 'users.first_name', 'users.last_name', 'institution.name as institution')
				->leftJoin('users', 'instructors.user_id', '=', 'users.id')
				->leftJoin('institution', 'institution.id', '=', 'instructors.institution_id')
				->first();

		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('instructors'); 
		}

		$institutions = \DB::table('institution')->select('name', 'id')->where('status', '1')->get();
		$this->data['institutions'] = array();
		foreach ($institutions as $key => $value) {
			$this->data['institutions'][$value->id] = $value->name;
		}

// print_r($row);die();
		$this->data['id'] = $id;
		return view('instructors.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			if($row->status==1)
				$row->status='Active';
			else
				$row->status='Inactive';
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('bse_instructors'); 
		}
		// print_r($row);die();
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('instructors.view',$this->data);	
	}	

	function postSave( Request $request, $id =0)
	{
		
		$rules = $this->validateForm();

		$rules['email'] = 'required|email|unique:users,id,'.$request->input('id');
		$rules['first_name'] = 'required|alpha|min:2';
		$rules['last_name'] = 'required|alpha|min:2';			
		$rules['username'] = 'required|alpha_num||min:2|unique:users,id,'.$request->input('id');
		$rules['avatar'] = 'mimes:jpg,jpeg,gif,png';

		$user = Users::find($request->input('user_id'));
		$uploadProcess = false;

		$validator = Validator::make($request->all(), $rules);	

		if ($validator->passes()) {
			$avatar = $request->file('avatar');
			if($id != null){
				if($avatar){
					$getImage = CourseImages::find($user->avatar);
					if($getImage != null){
	    				$image_path = $getImage->id.".".$getImage->image_type;
				    	if(file_exists('./uploads/users/'.$image_path)){
			            	File::Delete('./uploads/users/'.$image_path);
			            	File::Delete('./uploads/users/'.$getImage->id."_lg.".$getImage->image_type);
				        	File::Delete('./uploads/users/'.$getImage->id."_medium.".$getImage->image_type);
		                    File::Delete('./uploads/users/'.$getImage->id."_small.".$getImage->image_type);
			        		$getImage->delete();
			        	}
			        }
			       	$uploadProcess = true;
				}
			}

			$user->username 	= $request->input('username');
			$user->first_name 	= $request->input('first_name');
			$user->last_name 	= $request->input('last_name');
			$user->email 		= $request->input('email');
			$user->active 		= $request->input('status');
			$user->save();

			$user_id = $user->id;

			if($uploadProcess && $avatar) {
				// image process	
				$type 						= $avatar->getClientOriginalExtension();
				$courseImages 				= new CourseImages;
	            $courseImages->image_title 	= $request->input('username');
	            $courseImages->image_type 	= $type;
	            $courseImages->image_tag 	= "dummy tag";
	            $courseImages->uploader_id 	= $user_id;
	            $courseImages->created_at 	= time();
	            $courseImages->updated_at 	= time();
	            $courseImages->save();
	            $image_id 					= $courseImages->id;
	            $image_update				= CourseImages::find($image_id);
	            $image_update->image_hash 	= md5($image_id.$image_update->created_at);
	            $image_update->save();
		
				// resize image width and height
	            $image_size[] = array('imgW'=>'500','imgH'=>'500','value'=>'normal');
	            $image_size[] = array('imgW'=>'150','imgH'=>'150','value'=>'medium');
	            $image_size[] = array('imgW'=>'64','imgH'=>'64','value'=>'small');

	            foreach($image_size as $size) {
	                $save_path = ($size['value'] =='normal') ? $image_id.'.'.$type : $image_id.'_'.$size['value'].'.'.$type; 
	                Image::make($avatar->getRealPath())->resize($size['imgW'],$size['imgH'])->save('./uploads/users/'.$save_path);
	            }

	            Image::make($avatar->getRealPath())->save('./uploads/users/'.$image_id.'_lg.'.$type);
	            $user_update = Users::find($user->id);
	            $user_update->avatar = $image_id;
	            $user_update->save();
        	}

			$data = $this->validatePost('tb_instructors');
			$data['institution_id'] = $request->get('institution');
			if($id){
				$data['updated_at'] = date("Y-m-d H:i:s");
			}
			$newID = $this->model->insertRow($data , $request->input('id'));
			if(!is_null($request->input('apply')))
			{
				$return = 'instructors/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'instructors?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('instructors/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{

		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{

			foreach ($request->input('id') as $id) {
				$user_id = $this->model->where('id', $id)->pluck('user_id');
				$user = Users::find($user_id[0]);
				$checkforCCR = \bsetecHelpers::isappliedccr($user_id);
				if(count($checkforCCR)){
					return Redirect::to('core/users')
        		->with('messagetext', $user->username.'</br>'.\Lang::get('core.not_delete_user'))->with('msgstatus','error'); 
				}
				$getImage = CourseImages::find($user->avatar);
				if($getImage != null){
    				$image_path = $getImage->id.".".$getImage->image_type;
			    	if(file_exists('./uploads/users/'.$image_path)){
		            	File::Delete('./uploads/users/'.$image_path);
		            	File::Delete('./uploads/users/'.$getImage->id."_lg.".$getImage->image_type);
			        	File::Delete('./uploads/users/'.$getImage->id."_medium.".$getImage->image_type);
	                    File::Delete('./uploads/users/'.$getImage->id."_small.".$getImage->image_type);
		        		$getImage->delete();
		        	}
		        }
		        $user->destroy($user_id);
			}

			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('instructors')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('instructors')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}			

	public function getRegister(){
		if(\Auth::check())
			return Redirect::to('')->with('message',\SiteHelpers::alert('success','Youre already login'));

		$institutions = \DB::table('institution')->select('name', 'id')->where('status', '1')->get();

		$data['institutions'] = array();
		$data['institutions'][''] = 'Select';
		foreach ($institutions as $key => $value) {
			$data['institutions'][$value->id] = $value->name;
		}		

		return view('instructors.register', $data);  
	}

}