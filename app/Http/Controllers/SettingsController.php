<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Core\Groups;
use App\Models\bsetec;
use App\User;
use Illuminate\Http\Request;
use Validator, Input, Redirect; 

class SettingsController extends Controller {

    public function __construct()
    {
    	parent::__construct();
		if( \Auth::check() or \Session::get('gid') != '1')
		{
		//	echo 'redirect';
			return Redirect::to('dashboard');
		};
		
    }

    public function postStore()
    {
    	$this->model = new bsetec();
    	$this->model->save_options('commision_percentage', array('commision_percentage'=>Input::get('commision_percentage')));
    	$this->model->save_options('commision_to_admin_id', array('commision_to_admin_id'=>Input::get('commision_to_admin_id')));
    	$this->model->save_options('minimum_withdraw', array('minimum_withdraw'=>Input::get('minimum_withdraw')));
		return Redirect::to('settings')->with('messagetext','Settings Saved')->with('msgstatus','success');
    }

    public function Index()
    {
		
		$this->model = new bsetec();

		$this->data['commision_percentage'] = $this->model->get_option('commision_percentage');
		$this->data['commision_to_admin_id'] = $this->model->get_option('commision_to_admin_id');
		$this->data['minimum_withdraw'] = $this->model->get_option('minimum_withdraw');
		return view('settings.index', $this->data);
    }

	
}