<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Sitecategory;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 


class SitecategoryController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'sitecategory';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->model = new Sitecategory();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 'Course Category',//	$this->info['title'],
			'pageNote'	=>  'Course Category',//$this->info['note'],
			'pageModule'=> 'sitecategory',
			'return'	=> self::returnUrl()
			
		);
	}

	public function Index( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('sitecategory');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('sitecategory.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('categories'); 
		}

		$this->data['id'] = $id;
		return view('sitecategory.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('categories'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('sitecategory.view',$this->data);	
	}	

	function postSave( Request $request, $id=null)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('sitecategory');
			
			$now_date 			= date("Y-m-d H:i:s");
			if($id){
				$data['updated_at']  = $now_date;
			}else{
				$data['created_at']  = $now_date;
			}

			$data['name'] 						= $request->input('name');
			$data['slug'] 						= str_slug($request->input('name'));
			$status 							= $request->input('status');
			if(empty($status)){
				$status 						= 'disable';
			}
			$data['status'] 					= $status;
			$newID = $this->model->insertRow($data , $id);
			
			if(!is_null($request->input('apply')) && $id !=null)
			{
				$return = 'sitecategory/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'sitecategory?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			//check categories cache
			$this->categoryCache();

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('sitecategory/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{

		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{

			if(count($request->input('id'))>0){
				foreach ($request->input('id') as $key => $value) {
					$checkcatid	= \SiteHelpers::checkcatecourse($value);
					$this->categoryCache();	
					if($checkcatid==NULL && !$checkcatid){
						$this->model->destroy($value);
					//	\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$value)."  , Has Been Removed Successfull");
						return Redirect::to('sitecategory')
						->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
					}else{
						return Redirect::to('sitecategory')
						->with('messagetext', \Lang::get('core.cat_delete_msg'))->with('msgstatus','error'); 

					}

				}
			}
	
		} else {
			return Redirect::to('sitecategory')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}

	/*
	* @process - category cache process
	*/
	private function categoryCache(){
		
		if(\Cache::has('siteCategoriescache'))
		{	
			\Cache::forget('siteCategoriescache');
		}
	}	
}