<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Institution;
use App\Models\Core\Users;
use Illuminate\Http\Request;
use App\Http\Controllers\CommonmailController;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Models\CourseImages;
use Intervention\Image\ImageManagerStatic as Image;
use Validator, Input, Redirect, File ; 


class InstitutionController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'institution';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->model = new Institution();
		$this->sendmail = new CommonmailController();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'institution',
			'return'	=> self::returnUrl()
			
		);
	}

	public function Index( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query 

		// Filter Search for query	
		if(!is_null($request->input('search'))){
			$search = $request->input('search');
			$key = substr($search, 0, strpos($search, ":"));
			if(strtolower($key)=='type'){
				$value = substr($search, strpos($search, ":") + 1);
				$id = \bsetecHelpers::getInstitutionTypeId($value);
				$pos = strrpos($search, ":")+1;
				if($id)
					$_GET['search'] = substr_replace($search, $id, $pos, strlen($search));
				else
					$_GET['search'] = substr_replace($search, 0, $pos, strlen($search));
			}
		}

		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('institution');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('institution.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('BSE_institution'); 
		}

		$this->data['id'] = $id;
		return view('institution.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('BSE_institution'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('institution.view',$this->data);	
	}	


	function postSave(Request $request, $id =0){
		$rules = array('id' => 'required');
		$validator = Validator::make($request->all(), $rules);	

		if ($validator->passes()) {

			$update_id = $request->get('id');
			$status = $_POST['status'];

			$user_id = \DB::table('institution')->where('id', $update_id)->value('user_id');

			if($id && $user_id) {
				\DB::table('institution')->where('id', $update_id)->update(['status' => $status]);
				\DB::table('users')->where('id', $user_id)->update(['active' => $status]);
			}

			if(!is_null($request->input('apply'))) {
				$return = 'institution/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'institution?return='.self::returnUrl();
			}
			
			// Insert logs into database
			if($id!=0) {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
			}
			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
		} else {

			return Redirect::to('institution/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}
	}

	function postSave_old( Request $request, $id =0)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_institution');
			
			$newID = $this->model->insertRow($data , $request->input('id'));
			if(!is_null($request->input('apply')))
			{
				$return = 'institution/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'institution?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('institution/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			$user_id = $this->model->where('id', $request->input('id'))->value('user_id');
			$user = Users::find($user_id);
			$user->destroy($user_id);
			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('institution')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('institution')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}	

	public function getImage($image_hash = '', $size = 'normal')
	{
		$size = $size == 'normal' ? '' : '_'.$size;

		$image = \DB::table('course_images')->where('image_hash', '=', $image_hash)->first();
		
		header('content-type: '.$image->image_type);
	    readfile('uploads/logo/'.$image->id.$size.'.'.$image->image_type);	
	}

	public function postUpdateapproval(Request $request){
		$ids = $_POST['ids'];
		$type = $_POST['type'];
		foreach ($ids as $id) {
			$user_id = \DB::table('institution')->where('id', $id)->value('user_id');
			if($id && ($type=='Approved')) {
				\DB::table('institution')->where('id', $id)->update(['status' => '1']);
				\DB::table('users')->where('id', $user_id)->update(['active' => '1']);
			} else if($id && ($type=='Unapproved')) {
				\DB::table('institution')->where('id', $id)->update(['status' => '0']);
				\DB::table('users')->where('id', $user_id)->update(['active' => '0']);
			}
		}
		return response()->json(['success' => $type.' !']);
	}

}