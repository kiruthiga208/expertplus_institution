<?php  namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\bsetec;
use Validator, Input, Redirect ;
use App\Models\Course;
// use App\Models\Featured;
use App\Models\CourseImages;
use App\Models\Banner;
use File;
use Lang;

class HomeController extends Controller {

	public function __construct()
	{
		//$this->middleware('auth');
		$this->coursemodel= new Course;
		$this->bsetec = new bsetec;
		$this->sendmail = new CommonmailController();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	function createDateRangeArray($strDateFrom,$strDateTo)
	{
	    $aryRange=array();
	    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
	    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

	    if ($iDateTo>=$iDateFrom)
	    {
	        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
	        while ($iDateFrom<$iDateTo)
	        {
	            $iDateFrom+=86400; // add 24 hours
	            array_push($aryRange,date('Y-m-d',$iDateFrom));
	        }
	    }
	    return $aryRange;
	}

	public function getTest(){
		echo date_default_timezone_get();
		exit();

		$webinars = \GotoWebinar::getAllWebinars();
		echo '<pre>';print_r($webinars);exit;
		$this->data= array();

		$time1 = '2017-03-24T02:00:00Z';
		$time2 = '2017-04-24T02:00:00Z';

		$array = $this->createDateRangeArray($time1,$time2);

		foreach ($array as $key => $value) {

			$times[]=array(
				'startTime'=>$value,
				'EndTime'=>$value,
				);
		}

		echo '<pre>';print_r($times);exit;

		exit();
		// $webinar = [ 'subject'     => 'API Test 2',
  //            'description' => 'This Webinar is created via the API',
  //            'startTime'   => "2016-03-23T19:00:00Z",
  //            'endTime'     => "2016-03-23T20:00:00Z", ];
		// $dateRange = [  'fromTime' => "2017-03-20T12:00:00Z",
  //               		'toTime'   => "2017-03-21T13:00:00Z", ];
  //       $webinars = \GotoWebinar::getHistoricalWebinars( $dateRange );
  //       echo '<pre>';print_r($webinars);exit;

		//$webinars = \GotoWebinar::getWebinar('6169519554012598285');
	//	echo '<pre>';print_r($webinars);exit;
      
		// echo '<pre>';print_r($webinar);exit;
		// $times = array(
		// 		 'startTime'   => "2017-04-23T09:00:00Z",
  //           	 'endTime'     => "2017-04-23T12:00:00Z",            	
		// 	);
		// $times1 = array(
		// 		 'startTime'   => "2017-04-24T09:00:00Z",
  //           	 'endTime'     => "2017-04-24T12:00:00Z",            	
		// 	);

		// $times= array(
		// 		'times'=>array(
		// 				array(
		// 					 'startTime'   => "2017-04-23T09:00:00Z",
		// 	            	 'endTime'     => "2017-04-23T12:00:00Z",            	
		// 				),
		// 				array(
		// 					 'startTime'   => "2017-04-24T09:00:00Z",
		// 	            	 'endTime'     => "2017-04-24T12:00:00Z",            	
		// 				),
		// 			)
		// 	);


		// echo '<pre>';print_r($times);exit;
		// $webinars = \GotoWebinar::getAllWebinars();
		// echo '<pre>';print_r($webinars);exit;
		// $webinars = \GotoWebinar::getUpcomingWebinars();
		// echo '<pre>';print_r($webinars);exit;
		//echo '<pre>';print_r($webinars);exit;
		// foreach ($webinars->body as $key => $value) {
		// 	echo $value->webinarKey;exit();
		// }
		// $webinar = [ 'subject'     => 'API Test 2',
  //            'description' => 'This Webinar is created via the API',
  //            'startTime'   => "2017-03-23T19:00:00Z",
  //            'endTime'     => "2017-03-24T20:00:00Z", ];
		// $webinars = \GotoWebinar::createWebinar( $webinar );
		// echo '<pre>';print_r($webinars);exit;
		return view('pages.test',$this->data);
		// echo '<pre>';print_r($webinars);exit;
		// echo $webinars->body;exit();
		// echo '<pre>';print_r($webinars);exit;
		//$webinarKey = '962015880476224770';
		//$result = \GotoWebinar::deleteWebinar( $webinarKey, $sendNotification = true );
		//echo '<pre>';print_r($result);exit;
		//$webinars = \GotoWebinar::getUpcomingWebinars();
		//$webinar = \GotoWebinar::getWebinar($webinarKey); // working properly

		//$dateRange = [  'fromTime' => "2017-02-19T01:00:00Z",
              //  'toTime'   => "2017-03-23T20:00:00Z", ];

		//$webinars = \GotoWebinar::getHistoricalWebinars( $dateRange );
		//echo '<pre>';print_r($webinars);exit;

	  // $webinar = [ 'subject'     => 'Sample Webinar test update',
	  //             'description' => 'This Webinar is updated via the API',
	  //             'startTime'   => "2017-03-23T19:00:00Z",
	  //             'endTime'     => "2017-03-25T18:00:00Z",
	  //            ];

  	  //$rest = \GotoWebinar::createWebinar( $webinar );

		// $result = \GotoWebinar::updateWebinar( $webinarKey, $params, $sendNotification = true);

		//$webinars = \GotoWebinar::getUpcomingWebinars();
		//echo '<pre>';print_r($webinars);exit;
		// $webinar = [ 'subject'     => 'API Test 2',
  //            'description' => 'This Webinar is created via the API',
  //            'startTime'   => "2017-02-19T19:00:00Z",
  //            'endTime'     => "2017-02-20T20:00:00Z", ];

	//$webinar = \GotoWebinar::createWebinar( $webinar );
	//echo '<pre>';print_r($rest);exit;
	}
	public function postTest(Request $request){
		
		// $webinar = ['subject'     => 'API Test 12345',
  //            'description' => 'This Webinar is created via the API',
  //            'times'=>array(
		// 			array(
		// 				 'startTime'   => "2017-04-23T09:00:00Z",
		//             	 'endTime'     => "2017-04-23T12:00:00Z",            	
		// 			),
		// 			array(
		// 				 'startTime'   => "2017-04-24T09:00:00Z",
		//             	 'endTime'     => "2017-04-24T12:00:00Z",            	
		// 			),
		// 		),
  //            'type'=> 'Daily',
  //             ];

		$webinar = [ 'subject'     => 'API Test 123',
             'description' => 'This Webinar is created via the API',
             'startTime'   => "2017-04-23T09:00:00Z",
             'endTime'     => "2017-04-23T12:00:00Z",
             'type'=> 'Daily',
              ];
		$webinars = \GotoWebinar::createWebinar( $webinar );

		// Update a Webinar - date format standard: W3C - ISO 8601
		// $webinar = [ 'subject'     => 'API Test 2.2',
		//              'description' => 'This Webinar is updated via the API',
		//              'startTime'   => "2016-03-24T19:00:00Z",
		//              'endTime'     => "2016-03-24T20:00:00Z", ];
		// $webinar = GotoWebinar::updateWebinar( $webinarKey, $params, $sendNotification = true);



		echo '<pre>';print_r($webinars);exit;
	}
	public function index( Request $request )
	{
		
		if(CNF_FRONT =='false' && $request->segment(1) =='' ) :
			return Redirect::to('dashboard');
		endif; 		
		if(is_null($request->input('p')))
		{
			$page = $request->segment(1); 	
		} else {
			$page = $request->input('p'); 	
		}


		
		if($page !='') :
			$content = \DB::table('pages')->where('alias','=',$page)->where('status','=','enable')->get();
		//print_r($content);
		//return '';

			if(count($content) >=1 && $content[0]->filename!='home')
			{

				$row = $content[0];
				$this->data['pageTitle'] = $row->title;
				$this->data['pageNote'] = $row->note;
				$this->data['pageMetakey'] = ($row->metakey !='' ? $row->metakey : CNF_METAKEY) ;
				$this->data['pageMetadesc'] = ($row->metadesc !='' ? $row->metadesc : CNF_METADESC) ;

				$this->data['breadcrumb'] = 'active';					
				
				if($row->access !='')
				{
					$access = json_decode($row->access,true)	;	
				} else {
					$access = array();
				}	

				// If guest not allowed 
				if($row->allow_guest !=1)
				{	
					$group_id = \Session::get('gid');				
					$isValid =  (isset($access[$group_id]) && $access[$group_id] == 1 ? 1 : 0 );	
					if($isValid ==0)
					{
						return Redirect::to('')
							->with('message', \SiteHelpers::alert('error',Lang::get('core.note_restric')));				
					}
				}
				if($row->template =='backend')
				{
					 $page = 'pages.'.$row->filename;
				} else {
					
					$theme=\bsetecHelpers::get_options('theme');
					$page = 'layouts.'.$theme['theme'].'.home.index';
				}
				//print_r($this->data);exit;
				
				$filename = str_replace('/app','',base_path()) ."/resources/views/pages/".$row->filename.".blade.php";
				if(file_exists($filename))
				{
					$this->data['pages'] = 'pages.'.$row->filename;
				//	print_r($this->data);exit;

					return view($page,$this->data);
				} else {
					return Redirect::to('')
						->with('message', \SiteHelpers::alert('error',\Lang::get('core.note_noexists')));					
				}
				
			} elseif(count($content) >=1 && $content[0]->filename=='home'){
				$this->data['pageTitle'] = 'Home';
				$this->data['pageNote'] = 'Welcome To Our Site';
				$this->data['breadcrumb'] = 'inactive';	
				$this->data['pageMetakey'] =  CNF_METAKEY ;
				$this->data['pageMetadesc'] = CNF_METADESC ;
	
				$this->data['pages'] = 'pages.home';	
				$theme=\bsetecHelpers::get_options('theme');
				$page = 'layouts.'.$theme['theme'].'.home.index';
				$course=$this->coursemodel->getlatestcourse();
				$featured=$this->coursemodel->getfeaturedcourse();
				$viwed=$this->coursemodel->getmostviwedcourse();
				$coursestudents=$this->coursemodel->getcousestudents();
				$banner_model = new banner;
				$banners = $banner_model->where('banner_status', '1')->get();
				return view($page,$this->data)->with('course',$course)->with('feature',$featured)->with('viwed',$viwed)->with('coursestudents',$coursestudents)->with('banners',$banners);
			} else {
				return Redirect::to('')
					->with('message', \SiteHelpers::alert('error',\Lang::get('core.note_noexists')));	
			}
			
			
		else :
			
			$this->data['pageTitle'] = 'Home';
			$this->data['pageNote'] = 'Welcome To Our Site';
			$this->data['breadcrumb'] = 'inactive';	
			$this->data['pageMetakey'] =  CNF_METAKEY ;
			$this->data['pageMetadesc'] = CNF_METADESC ;

			$this->data['pages'] = 'pages.home';	
			$theme=\bsetecHelpers::get_options('theme');
			$page = 'layouts.'.$theme['theme'].'.home.index';
			$course=$this->coursemodel->getlatestcourse();
			$featured=$this->coursemodel->getfeaturedcourse();
			$viwed=$this->coursemodel->getmostviwedcourse();
			$coursestudents=$this->coursemodel->getcousestudents();
			$banner_model = new banner;
			$banners = $banner_model->where('banner_status', '1')->get();
			return view($page,$this->data)->with('course',$course)->with('feature',$featured)->with('viwed',$viwed)->with('coursestudents',$coursestudents)->with('banners',$banners);
		endif;	

		
	}

	public function  getLang($lang='en')
	{
		\Session::put('lang', $lang);
		return  Redirect::back();
	}	

	public function  postContact( Request $request)
	{
	
		$rules = array(
				'name'		=>'required',
				'subject'	=>'required',
				'message'	=>'required|min:20',
				'sender'	=>'required|email'			
		);
		$validator = Validator::make(Input::all(), $rules);	
		if ($validator->passes()) 
		{
		
			$fromMail   = $request->input('sender');
            $toMail     = CNF_EMAIL;
            $subject    = $request->input('subject');
            $data = array('name'=>$request->input('name'),'sender'=>$request->input('sender'),'subject'=>$request->input('subject'),'notes'=>$request->input('message')); 
            $tempname   = 'emails.contact';
			$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
			return Redirect::to('?p='.$request->input('redirect'))->with('message', \SiteHelpers::alert('success','Thank You , Your message has been sent !'));	
				
		} else {
			return Redirect::to('?p='.$request->input('redirect'))->with('message', \SiteHelpers::alert('error','The following errors occurred'))
			->withErrors($validator)->withInput();
		}		
	}	

	public function getPreview(Request $request)
	{
		$theme =  $request->input('theme');
		$this->data['pageTitle'] = 'Preview';
		$this->data['pageNote'] = 'Welcome To Our Site';
		$this->data['breadcrumb'] = 'inactive';	
		$this->data['pageMetakey'] =  CNF_METAKEY ;
		$this->data['pageMetadesc'] = CNF_METADESC ;
		
		$this->data['pages'] = 'pages.home';	
		
		$course=$this->coursemodel->getlatestcourse();
		$featured=$this->coursemodel->getfeaturedcourse();
		$viwed=$this->coursemodel->getmostviwedcourse();
		$coursestudents=$this->coursemodel->getcousestudents();
		$banner_model = new banner;
		$banners = $banner_model->where('banner_status', '1')->get();
		$page = 'layouts.'.$theme.'.home.index';
		return view($page,$this->data)->with('course',$course)->with('feature',$featured)->with('viwed',$viwed)->with('coursestudents',$coursestudents)->with('banners',$banners);
	}

	public function postUpdatenotification(Request $request)
	{
		$userid 	= \Session::get('uid');
		$getrecord  = \DB::table("notifications")->where("user_id",'=',$userid)->get();
		if(count($getrecord)>0){
			$data       = array();
			$data['notify'] = 1;
			\DB::table("notifications")->where("user_id",'=',$userid)->update($data);
			return '0';
		}else{
			return '1';
		}
	}

	public function getCheckTime()
	{
		// return 1000;
		$reset_site_at = $this->bsetec->get_option('reset_site_at');
		// return time();
		$reset_minutes = 60 * 60;

		// echo ($reset_site_at+$reset_minutes) - time();

		if(($reset_site_at+$reset_minutes) - time() > 0)
		{
			echo ($reset_site_at+$reset_minutes) - time();
		}
		else
		{
			echo $reset_minutes;
		}
		
	}

	public function getNotpage(){
		$this->data['page']='';
		return view('errors.404',$this->data);  
	}
}
