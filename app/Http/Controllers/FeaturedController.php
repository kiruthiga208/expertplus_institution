<?php
/**
 * Company : Bsetec
 * Controller : Course Controller
 * Email : support@bsetec.com
 */
namespace App\Http\Controllers;
use App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Course;
class FeaturedController extends Controller {

	public function __construct()
	{
		$this->model = new Course();
	}

	public function getIndex()
	{  
		$this->data['course']       =  $this->model->getfeaturedcourse();
		$this->data['search']       = \Lang::get('core.feature');
		return view('course.list',$this->data);
	}


	public function freeCourses()
	{  
		$this->data['course']       = $this->model->getfreecourses();
		$this->data['search']       = \Lang::get('core.free_courses');
		return view('course.list',$this->data);
	}

	public function TopRated()
	{  
		$this->data['course']		= $this->model->gettopratedcourses();
		$this->data['search']       = \Lang::get('core.top_rating');
		return view('course.list',$this->data);
	}

	public function topFree()
	{
		$this->data['course']       = $this->model->gettopfreecourses();
		$this->data['search']       = \Lang::get('core.top_free');
		return view('course.list',$this->data);
	}

	public function topPaid()
	{
		$this->data['course']       = $this->model->gettoppaidcourses();
		$this->data['search']       = \Lang::get('core.top_paid');
		return view('course.list',$this->data);
	}

	public function MostViewed()
    {
    	$this->data['course']       = $this->model->getmostviwedcourse();
		$this->data['search']       = \Lang::get('core.most_viewed');
		return view('course.list',$this->data);
    }

    public function lastestCourses()
    {
    	$this->data['course']       = $this->model->getlatestcourse();
		$this->data['search']       = \Lang::get('core.new');
		return view('course.list',$this->data);
    }

	public function takenCourses()
    {
    	$this->data['course']       = $this->model->getCoursecountby();
		$this->data['search']       = \Lang::get('core.digital');
		return view('course.list',$this->data);
    }
    

}