<?php namespace App\Http\Controllers;

use App\User;
use App\Models\Categories;
use App\Models\CourseFiles;
use Illuminate\Http\Request;
use App\Models\Questionanswer;
use App\Http\Controllers\Controller;
use Validator, Input, Redirect, Lang ,Auth ;
use App\Http\Controllers\CommonmailController;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class QuestionanswerController extends Controller {

	protected $layout 	= "layouts.main";
	protected $data 	= array();
	public $module 		= 'questionanswer';
	static $per_page	= '10';

	public function __construct()
	{


		$this->model 	= new Questionanswer();
		$this->info 	= $this->model->makeInfo( $this->module);
		$this->access 	= $this->model->validAccess($this->info['id']);
      	$this->sendmail = new CommonmailController();

		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'questionanswer',
			'return'	=> self::returnUrl()

		);

		$this->db_prefix = \DB::getTablePrefix();

	}

	public function Index( Request $request )
	{
		$gid = \Session::get('gid');
		if(!Auth::check())
			 return Redirect::to('user/login');
		// if($this->access['is_view'] ==0 || $gid != 1)
		// 	return Redirect::to('questionanswer/view');
				// ->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
	
		$sort 	= (!is_null($request->input('sort')) ? $request->input('sort') : 'question_id');
		$order 	= (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query
		// Filter Search for query
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

        
		$page 	= $request->input('page', 1);

		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);

		// Get Query
		$results = $this->model->getRows( $params );
		
		// Build pagination setting
		$page 		= $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);
		$pagination->setPath('questionanswer');

		$this->data['rowData']		= $results['rows'];
		// Build Pagination
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();
		// Row grid Number
		$this->data['i']			= ($page * $params['limit'])- $params['limit'];
		// Grid Configuration
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any

		// Master detail link if any
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
		// Render into template
		// echo '<pre>';print_r($this->data['rowData']);exit;
		return view('questionanswer.admin.index',$this->data);
	}

	public function getView( Request $request )
	{
		if(!\Auth::check()) return Redirect::to('questionanswer/guestview');
		// if($this->access['is_view'] ==0)
			// return Redirect::to('questionanswer/view');
				// ->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort 	= (!is_null($request->input('sort')) ? $request->input('sort') : 'question_id');
		$order 	= (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query
		// Filter Search for query
		$filter = " AND ".$this->db_prefix."questions.approved = '2' AND ".$this->db_prefix."questions.question_status = '1' ";
		if(!is_null($request->input('search')) && !empty($request->input('search'))){
			// $search_words = explode(' ',$request->input('search'));
			// $keywords = \bsetecHelpers::stopwords($search_words);
			$keywords=$request->input('search');
			if(!empty($keywords)){
				$filter .= " AND ";
				$filter .= " ".$this->db_prefix."questions.question_text = '".$keywords."' ";
				// $search_filter = '';
				// foreach($keywords as $key => $value){
				// 	if(!empty($search_filter)) $search_filter .= " OR ";
				// 	$search_filter .= " ".$this->db_prefix."questions.question_text LIKE '".$value."' ";
				// }
				// $filter .= $search_filter." ) ";
			} else {
				$filter = " AND ".$this->db_prefix."questions.approved = '3' ";	// no search match
			}
		}

		$page 	= $request->input('page', 1);
		$type 	= $request->input('t', '1');
		$qtype 	= $request->input('qt', '1');
		$search = $request->input('search', '');
		if($type == '2'){
			$filter .= " AND entry_by ='".\Session::get('uid')."' ";
		}
		if($qtype == '2'){
			$filter .= " AND answer_count !='0' ";
		} else if($qtype == '3'){
			$filter .= " AND answer_count = '0' ";
		}
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global'])) ? $this->access['is_global'] : 0
		);
		// Get Query
		$results = $this->model->getRows( $params );

		//echo "<pre>";print_r($results['rows']);exit;

		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);
		$pagination->setPath(url('questionanswer/view'));

		$rowData		= $results['rows'];

		foreach ($rowData as $key => $row) {
			$usrinfo 					 = \bsetecHelpers::getuserinfobyid($row->entry_by);
		
			$rowData[$key]->imgpath   	 = \SiteHelpers::customavatar($usrinfo->email,$row->entry_by);
			
			$rowData[$key]->user_link 	 = \URL::to('profile/'.$usrinfo->username);
            $rowData[$key]->display_name = $usrinfo->first_name.' '.$usrinfo->last_name;
		}
		$this->data['rowData'] = $rowData;

		// Build Pagination
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();
		// Row grid Number
		$this->data['i']			= ($page * $params['limit'])- $params['limit'];
		// Grid Configuration
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any

		// Master detail link if any
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
		// Render into template
		$this->data['type'] 	= $type;
		$this->data['qtype'] 	= $qtype;
		$this->data['search'] 	= $search;
		$this->data['view'] 	= 'view';
		$this->data['model'] 	= $this->model;
		$this->data['loggedid'] = \Session::get('uid');
		return view('questionanswer.index',$this->data);
	}


	public function getGuestview( Request $request )
	{

		// if($this->access['is_view'] ==0)
		// 	return Redirect::to('questionanswer/view')->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'question_id');
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query
		// Filter Search for query
		$filter = " AND ".$this->db_prefix."questions.approved = '2' AND ".$this->db_prefix."questions.question_status = '1' ";
		if(!is_null($request->input('search')) && !empty($request->input('search'))){
			// $search_words = explode(' ',$request->input('search'));
			$keywords=$request->input('search');

			// $keywords = \bsetecHelpers::stopwords($search_words);
			if(!empty($keywords)){
				$filter .= " AND  ";
				$filter .=" ".$this->db_prefix."questions.question_text LIKE '%".$keywords."%' ";
				// $search_filter = '';
				// foreach($keywords as $key => $value){
				// 	if(!empty($search_filter)) $search_filter .= " OR ";
				// 	$search_filter .= " ".$this->db_prefix."questions.question_text LIKE '".$value."' ";
				// }
				// $filter .= $search_filter." ) ";
			} else {
				$filter = " AND ".$this->db_prefix."questions.approved = '3' ";	// no search match
			}
		}

		$page 	= $request->input('page', 1);
		$type 	= $request->input('t', '0');
		$qtype 	= $request->input('qt', '1');
		$search = $request->input('search', '');
		if($qtype == '2'){
			$filter .= " AND answer_count !='0' ";
		} else if($qtype == '3'){
			$filter .= " AND answer_count = '0' ";
		}
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global'])) ? $this->access['is_global'] : 0
		);
		// Get Query
		$results = $this->model->getRows( $params );

		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);
		$pagination->setPath('guestview');

		$rowData		= $results['rows'];
		foreach ($rowData as $key => $row) {
			$usrinfo 					 = \bsetecHelpers::getuserinfobyid($row->entry_by);
			$rowData[$key]->imgpath   	 = \SiteHelpers::customavatar($usrinfo->email,$row->entry_by);
			$rowData[$key]->user_link 	 = \URL::to('profile/'.$usrinfo->username);
            $rowData[$key]->display_name = $usrinfo->first_name.' '.$usrinfo->last_name;
		}
		$this->data['rowData'] = $rowData;

		// Build Pagination
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();
		// Row grid Number
		$this->data['i']			= ($page * $params['limit'])- $params['limit'];
		// Grid Configuration
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any

		// Master detail link if any
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
		// Render into template
		$this->data['type'] 	= $type;
		$this->data['qtype'] 	= $qtype;
		$this->data['search'] 	= $search;
		$this->data['view'] 	= 'guestview';
		$this->data['model'] 	= $this->model;
		$this->data['loggedid'] = \Session::get('uid');
		return view('questionanswer.index',$this->data);
	}

	public function getSearch( Request $request )
	{

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'question_id');
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query
		// Filter Search for query
		$filter = " AND ".$this->db_prefix."questions.approved = '2' AND ".$this->db_prefix."questions.question_status = '1' ";
		if(!is_null($request->input('search')) && !empty($request->input('search'))){
			// $search_words = explode(' ',$request->input('search'));
			$keywords=$request->input('search');
			// $keywords = \bsetecHelpers::stopwords($search_words);
			if(!empty($keywords)){
				$filter .= " AND ";
				// $search_filter = '';
				$filter .=" ".$this->db_prefix."questions.question_text LIKE '%".$keywords."%' ";
				// foreach($keywords as $key => $value){
				// 	if(!empty($search_filter)){ $search_filter .= " OR ";}
				// 	$search_filter .= " ".$this->db_prefix."questions.question_text LIKE '".$value."' ";
				// }
				// $filter .= $search_filter." ) ";
			} else {
				$filter = " AND ".$this->db_prefix."questions.approved = '3' ";	// no search match
			}
		}

		$page = $request->input('page', 1);
		$type = $request->input('t', '0');
		$qtype = $request->input('qt', '1');
		$search = $request->input('search', '');
		if($qtype == '2'){
			$filter .= " AND answer_count !='0' ";
		} else if($qtype == '3'){
			$filter .= " AND answer_count = '0' ";
		}
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global'])) ? $this->access['is_global'] : 0
		);
		// Get Query
		$results = $this->model->getRows( $params );

		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);
		$pagination->setPath('guestview');

		$rowData		= $results['rows'];
		foreach ($rowData as $key => $row) {
			$usrinfo = \bsetecHelpers::getuserinfobyid($row->entry_by);
			$rowData[$key]->imgpath   = \SiteHelpers::customavatar($usrinfo->email,$row->entry_by);
			$rowData[$key]->user_link = \URL::to('profile/'.$usrinfo->username);
            $rowData[$key]->display_name = $usrinfo->first_name.' '.$usrinfo->last_name;
		}
		$this->data['rowData'] = $rowData;

		// Build Pagination
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();
		// Row grid Number
		$this->data['i']			= ($page * $params['limit'])- $params['limit'];
		// Grid Configuration
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any

		// Master detail link if any
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
		// Render into template
		$this->data['type'] = $type;
		$this->data['qtype'] = $qtype;
		$this->data['search'] = $search;
		$this->data['view'] = 'guestview';
		$this->data['model'] = $this->model;
		$this->data['loggedid'] = \Session::get('uid');
		return view('questionanswer.result_block',$this->data);
	}


	function getUpdate(Request $request, $id = null)
	{

		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('questions');
		}
        //echo '';print_r($this->data['row']);exit;
		$this->data['id'] 			= $id;
		$this->data['loggedid'] 	= \Session::get('uid');
		$this->data['categories']  	= Categories::lists('name', 'id');
		$this->data['model'] 		= $this->model;
		return view('questionanswer.admin.form',$this->data);
	}

	public function getShow($id = null,$ans_id=null)
	{

		if($this->access['is_detail'] ==0)
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');

		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('questions');
		}
     
		$rowData 				= $this->data['row'];
		$usrinfo  				= \bsetecHelpers::getuserinfobyid($this->data['row']->entry_by);
		$rowData->imgpath   	= \SiteHelpers::customavatar($usrinfo->email,$this->data['row']->entry_by);
		$rowData->user_link 	= \URL::to('profile/'.$usrinfo->username);
		$rowData->display_name 	= $usrinfo->first_name.' '.$usrinfo->last_name;
		if($rowData->file_ids)
			$rowData->file_id_list = implode(',', json_decode($rowData->file_ids));
		else
			$rowData->file_id_list = '';

		$this->data['row'] = $rowData;
		$type = '1';
		if($this->data['row']->entry_by == \Session::get('uid')){
			$type = '2';
		}

		$answers = $this->model->getAnswer($id);
		foreach ($answers as $key => $answer) {
			$usrinfo 					 = \bsetecHelpers::getuserinfobyid($answer->user_id);
			$answers[$key]->imgpath   	 = \SiteHelpers::customavatar($usrinfo->email,$answer->user_id);
			$answers[$key]->user_link 	 = \URL::to('profile/'.$usrinfo->username);
			$answers[$key]->display_name = $usrinfo->first_name.' '.$usrinfo->last_name;
			$answers[$key]->current_user = \Session::get('uid');
			//$answers[$key]->createdOn = $this->timeAgo(strtotime($answer->createdOn));
		}
		$this->model->updateViews($id);

        $this->data['ans'] 		= $answers;
		$this->data['ans_id'] 	= $ans_id;
		$this->data['id'] 		= $id;
		$this->data['type'] 	= $type;
		$this->data['access'] 	= $this->access;
		$this->data['loggedid'] = \Session::get('uid');
		$this->data['model'] 	= $this->model;
		return view('questionanswer.view',$this->data);
	}

	function postSave( Request $request, $id ='')
	{

		$rules 		= $this->validateForm();
		$validator 	= Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$data 					= $this->validatePost('questions');
			$data['createdOn'] 		= date("Y-m-d H:i:s");
			$data['updatedOn'] 		= $data['createdOn'];
			$data['entry_by'] 		= \Session::get('uid');
			$data['question_status']= '1';
			$data['sub_cat_id'] 	= $request->input('sub_cat_id') ? $request->input('sub_cat_id') : '';

			$file_ids 		= '';
			$file_id_data 	= $request->input('file_ids');
			if(!empty($file_id_data)){
				$file_id 	= explode(',', $file_id_data);
				$file_ids 	= json_encode($file_id);
			}

			$data['file_ids'] = $file_ids;

			$newID = $this->model->insertRow($data , $request->input('question_id'));

			if(is_null($request->input('question_id')) && $newID){
				// send message
				$uid 		= \Session::get('uid');
				$usrinfo 	= \bsetecHelpers::getuserinfobyid($uid);
				if(count($usrinfo)>0){
		            //join meeting mail
					$alink      = \URL::to('questionanswer/show/'.$newID);
					$toMail   	= CNF_EMAIL;
					// $fromMail   = CNF_SENDEMAIL;
					$fromMail   = CNF_EMAIL;
					$subject    = CNF_APPNAME." Custom Course/Tutorial/Question Request Created";
					$data       = array('username'=>$usrinfo->username,'link'=>$alink,'title'=>$request->input('question_text'),'desc'=>$request->input('question_description'));
					$tempname   = 'customcourserequest.mail.created';
					// return view('customcourserequest.mail.created')->with($data);
					$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
				}
			}

			// if(!is_null($request->input('apply')))
			// {
				// $return = 'questionanswer/show/'.$newID.'?return='.self::returnUrl();
			// } else {
			   $return = 'questionanswer/show/'.$newID.'?return='.self::returnUrl();
			// }

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');

		} else {

			return Redirect::to('questionanswer/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}

	}

	public function getDeletequestion( $id)
	{
		if($this->access['is_remove'] ==0)
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows
		if($id >=1)
		{
			$qinfo = $this->model->getquestioninfo($id);
			if(!empty($qinfo->file_ids)){
				$file_ids = json_decode($qinfo->file_ids,true);
				foreach ($file_ids as $key => $file_id) {
					$this->model->postDocdelete($id,$file_id);
				}
			}
			$this->model->destroy($id);
			// redirect
			return Redirect::to('questionanswer/view')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success');

		} else {
			return Redirect::to('questionanswer/view')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');
		}
	}

	public function postDelete( Request $request)
	{
		$type  = $request->input('types');
		$data  = array();
		if($type==''){
			if($this->access['is_remove'] ==0)
				return Redirect::to('dashboard')
			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

			$id 	= $request->input('id');
			if(count($id)>0){
				foreach ($id as $key => $qvalue) {
					$qinfo = $this->model->getquestioninfo($qvalue);
					$usrinfo = \bsetecHelpers::getuserinfobyid($qinfo->entry_by);
					if(count($usrinfo)>0){
						$fromMail   = CNF_EMAIL;
						$toMail     = $usrinfo->email;
						$subject    = CNF_APPNAME." Question Deleted ";
						$data       = array('item'=>'Question','authorname'=>'Admin','title'=>$qinfo->question_text,'description'=>$qinfo->question_description,'username'=>$usrinfo->username);
						$tempname   = 'questionanswer.mail.delete';
						$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
					}
					if(!empty($qinfo->file_ids)){
						$file_ids = json_decode($qinfo->file_ids,true);
						foreach ($file_ids as $key => $file_id) {
							$this->model->postDocdelete($qvalue,$file_id);
						}
					}
		            $this->model->destroy($qvalue);
				}

				\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
				// redirect
				return Redirect::to('questionanswer')
				->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success');
			} else {
				return Redirect::to('questionanswer/view')
				->with('messagetext','No Item Deleted')->with('msgstatus','error');
			}
		}elseif ($type=='1') {
			$id = $request->input('id');
			if(count($id)>0)
			{
				foreach ($id as $key => $qvalue)
				{
					$qinfo = $this->model->getquestioninfo($qvalue);
					$usrinfo = \bsetecHelpers::getuserinfobyid($qinfo->entry_by);
					if(count($usrinfo)>0){
		                //approve mail
						$alink      = \URL::to('questionanswer/show/'.$qvalue);
						$fromMail   = CNF_EMAIL;
						$toMail     = $usrinfo->email;
						$subject    = CNF_APPNAME." Question Approved";
						$data       = array('item'=>'Question','authorname'=>$usrinfo->username,'link'=>$alink,'title'=>$qinfo->question_text);
						$tempname   = 'questionanswer.mail.approved';
						$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
					}
					$this->model->updateStatus($qvalue,'2');
				}
				return Redirect::to('questionanswer')
				->with('messagetext', 'Question has been approved successfully.')->with('msgstatus','success');
			} else {
				return Redirect::to('questionanswer')
				->with('messagetext','No Question Approved')->with('msgstatus','error');
			}

		}elseif ($type=='2')
		{
			$id = $request->input('id');
			if(count($id)>0)
			{
				foreach ($id as $key => $qvalue)
				{
					$qinfo = $this->model->getquestioninfo($qvalue);
					$usrinfo = \bsetecHelpers::getuserinfobyid($qinfo->entry_by);
					if(count($usrinfo)>0){
		                //approve mail
						$alink      = \URL::to('questionanswer/show/'.$qvalue);
						$fromMail   = CNF_EMAIL;
						$toMail     = $usrinfo->email;
						$subject    = CNF_APPNAME." Question Unapproved";
						$data       = array('item'=>'Question','authorname'=>$usrinfo->username,'link'=>$alink,'title'=>$qinfo->question_text);
						$tempname   = 'questionanswer.mail.unapproved';
						// return view('questionanswer.mail.approved')->with($data);
						$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
					}
					$this->model->updateStatus($qvalue,'1');
				}
				return Redirect::to('questionanswer')
				->with('messagetext', 'Question has been Unapproved successfully.')->with('msgstatus','success');
			} else {
				return Redirect::to('questionanswer')
				->with('messagetext','No Question Unapproved')->with('msgstatus','error');
			}

		}
		return Redirect::to('questionanswer');


	}

    public function postAnswer(Request $request,$id=0)
	{

		$data['question_id'] = $request->input('question_id');
		$data['user_id'] 	 = $request->input('user_id');
		$data['answer_text'] = $request->input('answer_text');
		$answer_id  		 = $request->input('answer_id');
		if($answer_id != "") {
			$this->model->updateAnswer($answer_id,$request->input('answer_text'));
			$messagetext 	= 'Answer Updated Successfully';
		} else {
			$qinfo = $this->model->getquestioninfo($request->input('question_id'));
			$usrinfo = \bsetecHelpers::getuserinfobyid($qinfo->entry_by);
			if(count($usrinfo)>0){
				$alink      = \URL::to('questionanswer/show/'.$request->input('question_id'));
				$answer     = $request->input('answer_text');
				$fromMail   = CNF_EMAIL;
				$toMail     = $usrinfo->email;
				$subject    = CNF_APPNAME." Answer";
				$maildata   = array('item'=>'Question','authorname'=>$usrinfo->username,'link'=>$alink,'title'=>$qinfo->question_text,'answer'=>$answer);
				$tempname   = 'questionanswer.mail.answer';
				$this->sendmail->getMail($fromMail,$toMail,$subject,$maildata,$tempname);
			}
			$this->model->insertAnswer($data);
			$messagetext = 'Answer Saved Successfully';
		}
		return Redirect::to('questionanswer/show/'.$request->input('question_id'))
			->with('messagetext',$messagetext)->with('msgstatus','success');

	}

	public function postUpload(Request $request)
    {
		$document 	= $request->file('luploaddoc');
        $file 	  	= array('document' => $document);
        $rules 		= array('document' => 'required|mimes:zip,jpg,jpeg,png,pdf,doc,docx,txt');
        $validator 	= Validator::make($file, $rules);

        if( $validator->fails() )
        {
			$return_data = array(
                'status'=>false,
            );
		} else {
            $file_name 	= explode('.',$document->getClientOriginalName());
            $file_name 	= $file_name[0].'_'.time().rand(4,9999);
            $file_type 	= $document->getClientOriginalExtension();
			$file_title = $document->getClientOriginalName();
			$file_size 	= $document->getSize();

			$request->file('luploaddoc')->move('./uploads/files/', $file_name.'.'.$file_type
			);

            $courseFiles 					= new CourseFiles;
            $courseFiles->file_name 		= $file_name;
            $courseFiles->file_title 		= $file_title;
            $courseFiles->file_type 		= $file_type;
            $courseFiles->file_extension 	= $file_type;
            $courseFiles->file_size 		= $file_size;
			$courseFiles->duration 			= '';
            $courseFiles->file_tag 			= 'questions';
            $courseFiles->uploader_id 		= \Auth::user()->id;
            $courseFiles->created_at 		= time();
            $courseFiles->updated_at 		= time();
            if($courseFiles->save()){
                $return_data = array(
                    'id'=>$courseFiles->id,
                    'status'=>true,
                    'file_title'=> $file_title,
                    'file_ext'=> $file_type
                );
            }else{
                $return_data = array(
                    'status'=>false,
                );
            }
        }
        return json_encode($return_data);
        exit;
	}

	public function postDocdelete(Request $request){
		$this->model->postDocdelete($request->input('lid'),$request->input('rid'));
		return '1';
	}

    public function getDownloadresource($name)
    {
        $geturls     = ($name == null ? '' : \SiteHelpers::encryptID($name,true));
        $filename    = \DB::table('course_files')->where('id',$geturls)->get();
        $file        = "./uploads/files/".$filename['0']->file_name.'.'.$filename['0']->file_extension;
        return \Response::download($file);

    }

    public function postSavefiledata(Request $request)
    {
    	$data = array();
		// $data['entry_by'] = \Session::get('uid');
		$file_ids = '';
		$file_id_data = $request->input('file_ids');
		if(!empty($file_id_data)){
			$file_id 	= explode(',', $file_id_data);
			$file_ids 	= json_encode($file_id);
		}
		$data['file_ids'] = $file_ids;
		if($request->input('question_id'))
			$newID = $this->model->insertRow($data , $request->input('question_id'));


      // $cid  = $request->input('cats');
      // $sid  = $request->input('subcat');
      // if(!empty($cid)){
      //   $results = \bsetecHelpers::sitesubcategories($cid);
      //   if(!empty($sid)){
      //     $sid                 = $sid;
      //   }else{
      //     $sid                 = 0;
      //   }
      //   $this->data['sid']     = $sid;
      //   $this->data['results'] = $results;
      //   return view('course.subcats',$this->data);
      // }
    }

}