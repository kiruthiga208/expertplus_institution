<?php
namespace App\Http\Controllers;
use App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Search;
use App\Models\Course;
use Validator, Input, Redirect ; 
class SearchController extends Controller {

    public function __construct()
    {
        $this->model = new Search();
        $this->coursemodel = new Course();
    }

    public function getIndex()
    {   $currency = \SiteHelpers::getCurrentcurrency(CNF_CURRENCY);
        $prices = array('price_from=0&price_to=0' => \Lang::get('core.free'),
                                'price_from=1&price_to=49' => \Lang::get('core.less_than').$currency .' 50', 
                                'price_from=50&price_to=99' => $currency .' 50 - '.$currency .' 99', 
                                'price_from=100&price_to=199' => $currency .' 100 - '.$currency .'199' , 
                                'price_from=200&price_to=299' => $currency .' 200 - '.$currency .' 299', 
                                'price_from=400&price_to=499' => $currency .' 400 - '.$currency .' 499',
                                'price_from=500&price_to=10000' => $currency .' 500 - '.$currency .' 10000');
        $courseLevel = array('level=4' => \Lang::get('core.appropriate_all'),
                             'level=1' => \Lang::get('core.beginner'),
                             'level=2' => \Lang::get('core.Intermediate'),
                             'level=3' => \Lang::get('core.Advanced'));

        //check conditions whether the type is keyword or price
        $request = \Request::all();
        if(array_key_exists('q', $request))
        {
            $type = 'keyword';
            $search_query = $search = \Request::get('q');
        }
        elseif(array_key_exists('price_from', $request))
        {
            $type = 'price';
            $search_query = $request;
            $search = $prices['price_from='.$request['price_from'].'&price_to='.$request['price_to']];
        }
        elseif(array_key_exists('level', $request))
        {
            $type = 'level';
            $search_query = $request;
            $search = $courseLevel['level='.$request['level']];
        }
        
        if (empty($search_query)) {
            return Redirect::to('course');
        }
        
        $course                     = $this->model->searchcourses($search_query, $type);
        $banners                    = $this->coursemodel->getbanners();
        $this->data['bannercount']  = count($banners);
        $this->data['banners']      = $banners;
        $this->data['course']       = $course;
        $this->data['search']       = $search;
        $this->data['search_page']  = true;
        $this->data['search_type']  = $type;
        return view('course.list',$this->data);
        
    }
}
