<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Forumcategory;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 


class ForumcategoryController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'forumcategory';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->model = new Forumcategory();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'forumcategory',
			'return'	=> self::returnUrl()
			
		);
	}

	public function Index( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'forum_cat_id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('forumcategory');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('forumcategory.index',$this->data);
	}	



	/*function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('forum_category'); 
		}

		$this->data['id'] = $id;
		return view('forumcategory.form',$this->data);
	}	*/
	public function getUpdate(Request $request, $id = null)
	{	
		if($id=='')
		{
			$view='Add Forum Management';
			$categories = Forumcategory::get();
			//$category = \DB::('forum_category')->get();
			return view('forumcategory.form',$this->data)->with('category',$categories);	
		}
		elseif($id)
		{	//echo $id;exit;
			$categories = Forumcategory::where('forum_cat_id','=',$id)->get();
			return view('forumcategory.edit',$this->data)->with('row',$categories)->with('id',$id);	
		}
	}
	/* To update save new categories */
	public function postCategoryupdate( Request $request )
	{
		$input=array(
			'name'=>$request->get('title'),
			);
		$rules=array(
			'name'=>'required|min:6',
			);
		$message=array(
			'name.min'=>'The Title must be a 6 character',
			);
		$validator = Validator::make($input, $rules,$message);
		if($validator->passes()){
			$title = Input::get('title');
			$id = Input::get('id');
			$slug = $this->create_slug($title); 
			$updated_at=date("Y-m-d h:i:sa");
			$category=Forumcategory::where('forum_cat_id', $id)->update(['name' => $title,'slug'=>$slug,'slug'=>$slug,'updated_at'=>$updated_at]);
			return Redirect::to('forumcategory')->with('message','Successfully updated ');	
		}else{
			return Redirect::to('forumcategory/update/'.$request->get('id'))->with('messagetext','The following errors occurred')->with('msgstatus','error')
			->withInput()->withErrors($validator);
		}
		
	}
	/* Add New Category */
	public function postCategory( Request $request )
	{	
		$input=array(
			'name'=>$request->get('title'),
			);
		$rules=array(
			'name'=>'required|min:6',
			);
		$message=array(
			'name.min'=>'The Title must be a 6 character',
			);
		$validator = Validator::make($input, $rules,$message);
		if ($validator->passes()){
			$title = Input::get('title');
			$slug = $this->create_slug($title); 
			$updated_at=date("Y-m-d h:i:sa");
			$save=Forumcategory::insert(['name' => $title,'slug'=>$slug,'created_at'=>$updated_at]);
			return Redirect::to('forumcategory')->with('message','Successfully Added ');		
		}else{
			return Redirect::to('forumcategory/update')->with('messagetext','The following errors occurred')->with('msgstatus','error')
			->withInput()->withErrors($validator);
		}
		
	}
	public function create_slug($string){     
        $replace = '-';         
        $string = strtolower($string);     
		
        //replace / and . with white space     
        $string = preg_replace("/[\/\.]/", " ", $string);     
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);     
		
        //remove multiple dashes or whitespaces     
        $string = preg_replace("/[\s-]+/", " ", $string);     
		
        //convert whitespaces and underscore to $replace     
        $string = preg_replace("/[\s_]/", $replace, $string);     
		
        //limit the slug size     
        $string = substr($string, 0, 100);     
		
        //slug is generated     
        return $string; 
    } 

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('forum_category'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('forumcategory.view',$this->data);	
	}	

	function postSave( Request $request, $id =0)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('forumcategory');
			
			$newID = $this->model->insertRow($data , $request->input('forum_cat_id'));
			if(!is_null($request->input('apply')))
			{
				$return = 'forumcategory/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'forumcategory?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('forumcategory/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('forumcategory')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('forumcategory');
        		
		}

	}			


}