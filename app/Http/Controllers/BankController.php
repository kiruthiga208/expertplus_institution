<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\Models\Course;
use App\Models\bsetec;
use App\Models\Transaction;
use App\Models\UserCredits;
use App\Models\CourseTaken;
use App\Models\Coupon;
use App\Models\Admincoupon;
use danielme85\CConverter\Currency;
use Carbon\Carbon;

class BankController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'bank';
	static $per_page	= '10';

	public function __construct()
	{
		$this->bsetec = new bsetec();
		$this->transaction = new Transaction();
		$this->course = new Course();
		$this->user_credits = new UserCredits();
		
		$this->model = new Bank();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'bank',
			'return'	=> self::returnUrl()
			
		);
	}

	public function Index( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('bank');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('bank.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('transactions'); 
		}

		$this->data['id'] = $id;
		return view('bank.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('transactions'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('bank.view',$this->data);	
	}	

	function postSave( Request $request, $id =0)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_bank');
			
			$newID = $this->model->insertRow($data , $request->input('id'));
			if(!is_null($request->input('apply')))
			{
				$return = 'bank/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'bank?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('bank/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('bank')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('bank')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}	

	// Bank Transfer and Cash on Delivery
	public function postCashondelivery(Request $request){
		print_r($request->all());

		$rules=array(
		  'price' =>  'required',
		  'name'  =>  'required',
		  'phone'  =>   'required',
		  );
			if($request->paymenttype=='bank')
			{
				$rules['bankdetails']='required';
			}else{
				$rules['address']='required';
			}
		// $messages = array(
		// 		'address.min'=>"Minimum character 25",
		// 		'bankdetails.min'=>"Minimum character 25"
		// 	);
		$validator = Validator::make($request->all(), $rules);
           if($validator->fails()){
           if($request->devicetypes=='moblie'){
	           return Redirect::to('payment-form/'.$request->input('course_id').'/'.$request->input('uid'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')->withErrors($validator)->withInput();
	           }
	           return Redirect::to('payment/form')->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')->withErrors($validator)->withInput();
           }

           $details=array('Name'=>$request->input('name'),
           	              'phone' => $request->input('phone'),
           	             );

           ($request->paymenttype=='bank')?$details['Bankdetails']=$request->input('bankdetails'):$details['Address']=$request->input('address');

           $detail=json_encode($details);
          
           if($request->devicetypes=='moblie'){
              $this->transaction->user_id=$request->input('uid');
           }else{
           	  $this->transaction->user_id=\Session::get('uid');
           }

         $this->transaction->course_id=$request->input('course_id');
         	
			$coupon_price = $request->input('coupon_price');
			if($coupon_price)
			{
				$amount = $coupon_price;
			}
			else
			{	
         		$amount = $request->input('price');

			}
         $this->transaction->amount=$amount;
         $this->transaction->status='pending';
         $this->transaction->order_details=$detail;
         $this->transaction->payment_method=$request->input('paymenttype');
         $this->transaction->save();
         // $this->generate_invoice($this->transaction->id);
        if(trim($request->devicetypes)=='mobile'){
              $course = Course::find($request->input('course_id'));	
          	return view('payment-mobile/success')->with('course', $course)->with('status', 'success')->with('transId', $this->transaction->id)->with('title', 'Course');
          }
         return Redirect::to('course')->with('messagetext','request sent to admin')->with('msgstatus','success');
       }

       function generate_invoice($transaction_id)
	{
		$transaction_detail = $this->transaction->get_transaction_detail($transaction_id);

		// echo '<pre>';print_r($transaction_detail);exit;
		//update the order details on transaction table
		$invoice['transaction_id'] = $transaction_detail->id;
		$invoice['customer_name'] = $transaction_detail->customer_name;
		$invoice['course_title'] = $transaction_detail->course_title;
		$invoice['amount'] = $transaction_detail->amount;
		$invoice['email'] = $transaction_detail->email;
		$invoice['status'] = $transaction_detail->status;
		$invoice['payment_method'] = $transaction_detail->payment_method;
		
		$invoice['invoice_number'] = time().$transaction_id;
		$invoice['ordered_on'] = date('Y-m-d H:i:s');
		
		$this->transaction->save_invoice($invoice);

		$email = $invoice['email'];
		$this->data['invoice'] = $invoice;
		
		// echo view('course.invoice',$this->data);exit;
		//send mail to the customer
		/*\Mail::send('course.invoice', $this->data, function($message) use($email)
		{
		    $message->subject('Invoice from expert plus');
		    $message->from('admin@expertplus.com', 'Expert Plus');
		    $message->to($email);

			//pass the same data to pdf view file and generate a pdf    
			$pdf = \PDF::loadView('course.invoice-pdf', $this->data);
			//return $pdf->download('invoice.pdf');
			//get the pdf file as data
			$attach =  $pdf->stream();
			//attach the pdf in mail
		    $message->attachData($attach, 'invoice.pdf');
		});*/
	}	

	function save_credits($transaction_id)
	{
		//get transaction details
		$transaction = $this->transaction->find($transaction_id);

		//get commision percentage from db
		$commision_percentage = $this->bsetec->get_option('commision_percentage');
		
		//calculate the credits
		$amount = $transaction->amount;
		$admin_credit = ($amount * $commision_percentage)/100;
		$instructor_credit = $amount - $admin_credit;

		//get instructor id for the course id
		$instructor_id = $this->course->getCourseInstructor($transaction->course_id);

		//save credit for instructor
		$credit['transaction_id'] = $transaction_id;
		$credit['instructor_user_id'] = $instructor_id;
		$credit['course_id'] = $transaction->course_id;
		$credit['by_user_id'] = $transaction->user_id;
		$credit['is_admin'] = 0;
		$credit['credits_for'] = 'course_cost';
		$credit['credit'] = $instructor_credit;
		$credit['created_at'] = time();

		$this->user_credits->save_credits($credit);
		//save credit for instructor
		$credit['instructor_user_id'] = 0;
		$credit['is_admin'] = 1;
		$credit['credits_for'] = 'course_commision';
		$credit['credit'] = $admin_credit;
		$this->user_credits->save_credits($credit);
		
	}	


}