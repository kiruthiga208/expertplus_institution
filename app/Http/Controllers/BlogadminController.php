<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Blogadmin;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\Models\Blogcategories;

class BlogadminController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'blogadmin';
	static $per_page	= '10';
	
	public function __construct() {
		parent::__construct();
		$this->model = new Blogadmin();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'	=> 'blogadmin',
		);
			
				
	} 

	
	public function Index()
	{
		if($this->access['is_view'] ==0) 
			return Redirect::to('')
				->with('message', \SiteHelpers::alert('error','Sorry, You are not allowed to access this page!'));
				
		// Filter sort and order for query 
		$sort = (!is_null(Input::get('sort')) ? Input::get('sort') : 'created'); 
		$order = (!is_null(Input::get('order')) ? Input::get('order') : 'desc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null(Input::get('search')) ? $this->buildSearch() : '');
		// End Filter Search for query 
		
		// Take param master detail if any
		$master  = $this->buildMasterDetail(); 
		// append to current $filter
		$filter .=  $master['masterFilter'];
	
		
		$page = Input::get('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null(Input::get('rows')) ? filter_var(Input::get('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );	
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		// $pagination = Paginator::make($results['rows'], $results['total'],$params['limit']);		
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('blogadmin');
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];

		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		$this->data['details']		= $master['masterView'];
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('blogadmin.index',$this->data);

						// ->with('menus', \SiteHelpers::menus());
	}		
	

	function getAdd( $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('')->with('message', \SiteHelpers::alert('error',\Lang::get('core.note_restric')));
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('')->with('message', \SiteHelpers::alert('error',\Lang::get('core.note_restric')));
		}				
			
		$id = ($id == null ? '' : \SiteHelpers::encryptID($id,true)) ;
		
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('blogs'); 
		}
		$this->data['id'] = $id;
		$this->data['categorys'] = Blogcategories::where('enable','1')->orderBy('name')->pluck('name', 'CatID');
		return view('blogadmin.form',$this->data);
		// ->with('menus', $this->menus );	
	}
	
	function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('')
				->with('message', \SiteHelpers::alert('error',\Lang::get('core.note_restric')));
					
		$ids = ($id == null ? '' : \SiteHelpers::encryptID($id,true)) ;
		$row = $this->model->getRow($ids);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('blogs'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('blogadmin.view',$this->data);
		// ->with('menus', $this->menus );	
	}	
	
	function postSave(Request $request,$id =0)
	{

		$rules = array(
			'title'=>'required|min:6',
			'content'=>'required|min:200',
			'CatID'=>'required'
			);
		$message = array(
			'CatID.required'=>'The category field is required.'
		);

		$validator = Validator::make(Input::all(), $rules,$message);	
		if ($validator->passes()) {
			$data = $this->validatePost('blogs');
			$data['slug'] = \SiteHelpers::seoUrl( trim(Input::get('title')));
			$data['content'] =$_POST['content'];
			$data['entryby']=\Session::get('uid');
			if(!is_null(Input::file('blogImage'))){
				$file = $request->file('blogImage'); 
				$destinationPath = './uploads/blog/';
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension(); //if you need extension of the file
				$newfilename = \Session::get('uid').time().'.'.$extension;
				$uploadSuccess = $request->file('blogImage')->move($destinationPath, $newfilename);				 
				if( $uploadSuccess ) {
					$exblogimg=$this->model->find($id);
					if(!empty($exblogimg->blog_image)){
						$image_path = './uploads/blog/'.$exblogimg->blog_image;
						if(file_exists($image_path)){
							\File::Delete($image_path);
						}
					}
					$data['blog_image'] = $newfilename; 
				} 
			}
			if(Input::get('created')==''){
				$data['created'] = date("Y-m-d H:i:s");
			}
			$ID = $this->model->insertRow($data , Input::get('blogID'));
			// Input logs
			if(Input::get('blogID') =='')
			{
				\SiteHelpers::auditTrail( $request ,"New Entry row with ID : $ID  , Has Been Save Successfull");
			} else {
				\SiteHelpers::auditTrail( $request ," ID : $ID  , Has Been Changed Successfull");
			}
			// Redirect after save	
			return Redirect::to('blogadmin')->with('message', \SiteHelpers::alert('success','Saved successfully!'));
		} else {
			if($id=='0'){
				return Redirect::to('blogadmin/add/')->withErrors($validator)->withInput();
			}else{
				return Redirect::to('blogadmin/add/'.$id)->withErrors($validator)->withInput();
			}
		}	
	
	}
	
	public function postDestroy(Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('')
				->with('message', \SiteHelpers::alert('error',\Lang::get('core.note_restric')));		
		if(count(Input::get('id'))>0){			
			// delete multipe rows 
			$this->model->destroy(Input::get('id'));
			\SiteHelpers::auditTrail( $request ,"ID : ".implode(",",Input::get('id'))."  , Has Been Removed Successfull");
			// redirect
			\Session::flash('message', \SiteHelpers::alert('success','Removed successfully!'));
		}else{
			\Session::flash('message', \SiteHelpers::alert('error','please select atleast one checkbox!'));
		}
		return Redirect::to('blogadmin');
	}	
	
			
		
}