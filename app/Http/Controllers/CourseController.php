<?php
/**
 * Company : Bsetec
 * Controller : Course Controller
 * Email : support@bsetec.com
 */
namespace App\Http\Controllers;
use App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Course;
use Validator, Input, Redirect ; 
use App\Library\VideoHelpers;
use App\Models\CourseVideos;
use App\Models\CourseSurvey;
use App\Models\CourseImages;
use App\Models\CourseFiles;
use File;

use Auth;
use App\Models\Transaction;
use App\Models\bsetec;
use App\Models\Users;
use App\Models\Notificationsettings;
use App\Models\CourseTaken;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Models\Coupon;
use Response, Lang;
use Mail;
use App\User;
use App\Http\Controllers\CommonmailController;
use App\Library\BigBlueButton;
use App\Library\S3;

class CourseController extends Controller {

    public function __construct()
    {
      $this->model = new Course();
      $this->sendmail = new CommonmailController();
      $this->bbb = new BigBlueButton();
      $this->notification_settings = new Notificationsettings();
    }

    /**
     * Main course of site
     */
    public function getIndex()
    {      
        $this->data['trending_course']          = $this->model->getmostviwedcourse();
        $this->data['latest_course']            = $this->model->getlatestcourse();
        $this->data['staff_picks_course']       = $this->model->gettopratedcourses();
        $this->data['digital_marketing_course'] = $this->model->getCoursecountby();
        $this->data['banners']                  = $this->model->getbanners();
        $this->data['bannercount']              = count($this->data['banners']);
        return view('course.index',$this->data);
    }

    public function postAjaxReplies()
    {
      $this->data['replys'] = \SiteHelpers::getlecturereplies(\Request::get('comment_id'),\Request::get('offset'));
      return view('course.ajax-replies',$this->data);
    }
    public function getDetails($id, $slug = NULL)
    {
        $t_return                   = strtoupper(\Request::get('return'));
        $loggedid                   = \Session::get('uid');
        $course                     = $this->model->getcourseinfo($id);
        if (count($course)==0) {
            return Redirect::to('course');
        }
        $takencourse                = array();
        $inststatus                 = array();
        $privacycheck               = array();
        if(!empty($loggedid)){
            $takencourse            = $this->model->getcoursetaken($id,$loggedid);
            $inststatus             = $this->model->getinstructorstatus($course->user_id,$id,1);
            if($course->privacy==2){
                $privacycheck           = $this->model->getprivacystatus($id,$loggedid);
            }
        }
            
        if (empty($slug)) {
            return Redirect::to('courseview/' . $course->course_id . '/' . $course->slug);
        }
        if ($slug != $course->slug) {
            return Redirect::to('courseview/' . $course->course_id . '/' . $course->slug);
        }
        if ($course->approved == 0 || $course->approved == 2) {
           	 return Redirect::to('/');
        }
        //invite user check

        if($course->privacy==2 && empty($loggedid)){
            return Redirect::to('/');
        }elseif($course->privacy==2 && !empty($loggedid) && count($privacycheck)==0 && $course->user_id!=$loggedid){

            return Redirect::to('/');   
        }

        

        if (($course->approved == 1 && $course->user_id==$loggedid) || (count($takencourse)>0) || (count($inststatus)>0)) { 

            if(!\Auth::check()) return Redirect::to('user/login');
               $loggedid                   = \Session::get('uid');
               if($course->user_id!=$loggedid && count($inststatus)=='0'){
                   $takencourse                = '';
                   if(!empty($loggedid)){
                        $takencourse           = $this->model->getcoursetaken($id,$loggedid);
                        if(count($takencourse)==0){
                            return Redirect::to('course');        
                        }
                   }
               
               }

               if($course->privacy>2 && \Session::get('cplogin-'.$course->course_id)==0 && $course->user_id!=$loggedid){

                    $courseinfo                 = $this->model->getcourseinfo($course->course_id); 
                    $this->data['courseinfo']   = $courseinfo;
                    return view('course.passwordcheck',$this->data);
                }else{

                   $courseinfo                 = $this->model->getcourseinfo($id); 
                   $curriculum                 = $this->model->getcurriculumsection($id);
                   $comments                   = $this->model->getalllecturecomments($id);
                   $announcement               = $this->model->getannouncement($id);
    			   $this->data['coursenotification'] = $this->model->getCourseNotificationInfo($id,$loggedid);
    				if($course->user_id!=$loggedid)	// if not author check for instructor
    					$this->data['coursesettings'] = $this->model->checkInstructor($id,$loggedid);
    				else
    					$this->data['coursesettings'] = 0;
    			   $this->data['courseprogress'] = $this->model->getCourseProgress($id,$loggedid);
    			   $this->data['courselecturecompleted'] = $this->model->getCourseCompletedCount($id,$loggedid);
    			   $this->data['courselecturetotal'] = $this->model->getCourseLectureTotal($id);
                   $this->data['curriculum']   = $curriculum;
                   $this->data['courseid']     = $id;
                   if($slug=='' && count($courseinfo)>0){
                        $this->data['slug']    = $courseinfo->slug;
                   }else{
                        $this->data['slug']    = $slug;
                   }
                   
                   $this->data['courseinfo']   = $courseinfo;
                   $this->data['course_completion']  = $this->checkCompletion($id);
                   $this->data['comments']     = $comments;
                   $this->data['announcement'] = $announcement;
                   // echo '<pre>';print_r($this->data);exit;
                   if(\Request::ajax()) 
                   {  
                      return view('course.ajax-comments',$this->data);
                   }
                   return view('course.preview',$this->data);
               }
            //return Redirect::to('learn/' . $course->course_id . '/' . $course->slug);
        }
        $curriculum                 = $this->model->getcurriculum($id);
        $visitors                   = $this->model->getvisitors($id);
        $userinfo                   = $this->model->getuserinfos($course->user_id);
        $comments                   = $this->model->getcomments($id);
        $instructors                = $this->model->getallmanageinstructors($id);
        $inststatus                 = $this->model->getinstructorstatus($course->user_id,$id);
        $students                   = $this->model->getstudents($id);
        $allstudents                = $this->model->getstudents($id,1);
        $morecourse                 = $this->model->getmorecourse($id,$course->user_id);
        $getviewed                  = $this->model->getviewedcoures();
		    $videos                     = $this->model->getvideoinfo($course->video);
        $reviews                    = $this->model->getreviews($id);
        $allreviews                 = $this->model->getreviews($id,1);
        $pendkey                    = end($reviews);
        $pnexts                     = array();
        if(!empty($pendkey->ratings_id)){
            $pnexts       = $this->model->getreviewsall($pendkey->ratings_id,$id);
        }
        $sendkey                     = end($students);
        $snexts                      = array();
        if(!empty($sendkey->ratings_id)){
            $snexts       = $this->model->getallstudents($sendkey->ratings_id,$id);
        }
        $filename = '';
        $filetype = '';
         $awsurl='';
        if(!empty($videos['0']->video_title)){
            $filename = $videos['0']->video_title;
            $filetype = $videos['0']->video_type; 
            $awsurl   = $videos['0']->aws_url;
        }
		
        $this->data['courseid']     = $id;       
        $this->data['course']       = $course;
        $this->data['curriculum']   = $curriculum;
        $this->data['author']       = $userinfo;
        $this->data['comments']     = $comments;
        $this->data['instructor']   = $instructors;
        $this->data['insstatus']    = $inststatus;
        $this->data['students']     = $students;
        $this->data['morecourse']   = $morecourse;
        $this->data['mostviewed']   = $getviewed;
    		$this->data['filename']     = $filename;
    		$this->data['filetype']     = $filetype;
        $this->data['awsurl']       = $awsurl;
        $this->data['reviews']      = $reviews;
        $this->data['allreviews']   = $allreviews;
        $this->data['allstudents']  = $allstudents;
        $this->data['pnext']        = $pnexts;
        $this->data['snext']        = $snexts;

        $this->data['t_return'] = ($t_return) ? $t_return : "none";
        return view('course.view',$this->data);
    }

    public function getCoursePreview($id, $slug = NULL)
    {
      
      if(!\Auth::check()) return Redirect::to('user/login');
      $t_return                   = strtoupper(\Request::get('return'));
      $loggedid                   = \Session::get('uid');
      $course                     = $this->model->getcourseinfo($id);
        
      //course preview with lectures listing for instructor and admin
      if(\Request::get('PreviewMode') == 'instructor')
      {
          //check if the course instructor or superadmin, else redirect to course page
          if($course->user_id == $loggedid || \Auth::user()->group_id == 1)
          {
            $courseinfo                 = $this->model->getcourseinfo($id); 
            $curriculum                 = $this->model->getcurriculumsection($id);
            $comments                   = $this->model->getalllecturecomments($id);
            $announcement               = $this->model->getannouncement($id);
            $this->data['coursenotification'] = $this->model->getCourseNotificationInfo($id,$loggedid);
            
            $this->data['coursesettings'] = 0;
            $this->data['courseprogress'] = $this->model->getCourseProgress($id,$loggedid);
            $this->data['courselecturecompleted'] = $this->model->getCourseCompletedCount($id,$loggedid);
            $this->data['courselecturetotal'] = $this->model->getCourseLectureTotal($id);
            $this->data['curriculum']   = $curriculum;
            $this->data['courseid']     = $id;
            $this->data['slug']    = $courseinfo->slug;
            
                   
            $this->data['courseinfo']   = $courseinfo;
            $this->data['course_completion']  = $this->checkCompletion($id);
            $this->data['comments']     = $comments;
            $this->data['announcement'] = $announcement;
            return view('course.preview.preview',$this->data);
          }
          else
          {
            return Redirect::to('course');
          }
      }
           
      $curriculum                 = $this->model->getcurriculum($id);
      $visitors                   = $this->model->getvisitors($id);
      $userinfo                   = $this->model->getuserinfos($course->user_id);
      $comments                   = $this->model->getcomments($id);
      $instructors                = $this->model->getallmanageinstructors($id);
      $inststatus                 = $this->model->getinstructorstatus($course->user_id,$id);
      $students                   = $this->model->getstudents($id);
      $allstudents                = $this->model->getstudents($id,1);
      $morecourse                 = $this->model->getmorecourse($id,$course->user_id);
      $getviewed                  = $this->model->getviewedcoures();
      $videos                     = $this->model->getvideoinfo($course->video);
      $reviews                    = $this->model->getreviews($id);
      $allreviews                 = $this->model->getreviews($id,1);
      $pendkey                    = end($reviews);
      $pnexts                     = array();
      if(!empty($pendkey->ratings_id)){
          $pnexts       = $this->model->getreviewsall($pendkey->ratings_id,$id);
      }
      $sendkey                     = end($students);
      $snexts                      = array();
      if(!empty($sendkey->ratings_id)){
          $snexts       = $this->model->getallstudents($sendkey->ratings_id,$id);
      }
       $filename = '';
        $filetype = '';
      if(!empty($videos['0']->video_title)){
        $filename = $videos['0']->video_title;
        $filetype = $videos['0']->video_type; 
      }

      $this->data['courseid']     = $id;       
      $this->data['course']       = $course;
      $this->data['curriculum']   = $curriculum;
      $this->data['author']       = $userinfo;
      $this->data['comments']     = $comments;
      $this->data['instructor']   = $instructors;
      $this->data['insstatus']    = $inststatus;
      $this->data['students']     = $students;
      $this->data['morecourse']   = $morecourse;
      $this->data['mostviewed']   = $getviewed;
      $this->data['filename']     = $filename;
      $this->data['filetype']     = $filetype;
      $this->data['reviews']      = $reviews;
      $this->data['allreviews']   = $allreviews;
      $this->data['allstudents']  = $allstudents;
      $this->data['pnext']        = $pnexts;
      $this->data['snext']        = $snexts;

      $this->data['t_return'] = ($t_return) ? $t_return : "none";

      return view('course.preview.view',$this->data);
    }


    public function getCreate($id,$step)
    {    
      if(!\Auth::check()) return Redirect::to('user/login');
      $loggedid     = \Session::get('uid');
      $course       = $this->model->getcourseinfo($id);
      if (count($course)>0 && $course->user_id!=$loggedid) {
            return Redirect::to('course');
      }
      $videos       = $this->model->getvideoinfo($course->video);
      $testvideos   = $this->model->getvideoinfo($course->test_video);
      $courseImage   = $this->model->getimageinfo($course->image);
	  if($step == '15' && !empty($course->survey)) {
		$coursesurvey = $this->model->getsurveyinfo($course->survey);
		$this->data['coursesurvey'] = $coursesurvey['0'];
	  }elseif($step == '9') {
		$user_id = \Auth::user()->id;
		$coursecurriculum = $this->model->getcurriculuminfo($id,$user_id);
		// echo "<pre>";
		// print_r($coursecurriculum);
		// exit;
		$this->data['sections'] = $coursecurriculum['sections'];
		$this->data['lecturesquiz'] = $coursecurriculum['lecturesquiz'];
    $this->data['lecturesquizquestions'] = $coursecurriculum['lecturesquizquestions'];
		$this->data['lecturesmedia'] = $coursecurriculum['lecturesmedia'];
		$this->data['lecturesresources'] = $coursecurriculum['lecturesresources'];
		$this->data['uservideos'] = $coursecurriculum['uservideos'];
		$this->data['useraudios'] = $coursecurriculum['useraudios'];
        $this->data['userpresentation'] = $coursecurriculum['userpresentation'];
		$this->data['userdocuments'] = $coursecurriculum['userdocuments'];
		$this->data['userresources'] = $coursecurriculum['userresources'];		
  }elseif($step == '13') {
      $feedback_parent  = $this->model->getallcomments($id);
      $feedback = array();
      if(count($feedback_parent)>0){
          for($i=0;$i<count($feedback_parent);$i++)
          {
            $feedback[] = $feedback_parent[$i];
            $feedback_child = $this->model->getallcomments($id,$feedback_parent[$i]->feedback_id);
            if(count($feedback_child)>0)
            {
              for($j=0;$j<count($feedback_child);$j++) 
              {
                $feedback[] = $feedback_child[$j];
              }
            }
          }
      }
        $this->data['feedback']  = $feedback;
      }elseif($step == '7') {
        $this->data['priceinfo'] = $this->model->getpriceinfo($id);

        if(count($this->data['priceinfo']) != 0){
            if($this->data['priceinfo'][0]->p_country != "")
                $this->data['priceinfo_complettion'] = true;
            else
                $this->data['priceinfo_complettion'] = false;
        }else{
           $this->data['priceinfo_complettion'] = false;
        }

        $this->data['return']   = "";
        $bsetec = new bsetec;
        $this->data['couponinfo'] = Coupon::where('user_id',Auth::user()->id)->where('course_id',$id)->paginate($bsetec->get_option('paginationPerpage')); 
       }elseif($step == '17'){
            $course->coursefrom=($course->coursefrom=='0000-00-00')?'':$course->coursefrom;
            $course->courseto=($course->courseto=='0000-00-00')?'':$course->courseto;
            $course->closedate=($course->closedate=='0000-00-00 00:00:00')?'':$course->closedate;
	}
      $getinstructors            = $this->model->getinstructors($id);
      $getinstructorsdefault     = $this->model->getinstructors($id,1);
      $getinviterusers            = $this->model->getinviterusers($id);

      // $curriculum                = $this->model->getcurriculumcount($id);
      // $allcurriculum             = $this->model->getcurriculumcount($id,1);
      $this->data['course']      = $course;
      $this->data['step']        = $step;
      $this->data['videos']      = $videos;
      $this->data['testvideos']  = $testvideos;
      $this->data['courseImage']  = $courseImage;
      $this->data['instructors'] = $getinstructors;
      $this->data['inviteusers'] = $getinviterusers;
      $this->data['dinstructors']= $getinstructorsdefault;
      $this->data['courseid']    = $id;
      // $this->data['fname']       = \Session::get('fid');
      // $this->data['curriculum']   = $curriculum;
      // $this->data['allcurriculum'] = $allcurriculum;
        // return view($page,$this->data);
      return view('course.createstep'.$step,$this->data);
	}
	

    public function postUpdatecourse(Request $request)
    {

		$course = Course::find($request->input('course_id'));
		if ($request->input('step') == '2') {
			// Basic
            $rules = array(
              'title'=>'required',        
            );    
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->passes()) { 
                $title = $request->input('title');
                $slug = str_slug($title);
                if (!$slug) {
                    $slug = str_random(9);
                }
                $course->course_title = $request->input('title');
                $course->slug = $slug;
                $course->cat_id = $request->input('cat_id');
               // echo "<pre>";print_r($request->input('sub_cat_id'));exit();
                // if($request->input('sub_cat_id')!="")
                // {
                $course->sub_cat_id = $request->input('sub_cat_id');
                // }
                $course->user_id = \Auth::user()->id;
                $course->lang_id = $request->input('lang_id');
                $course->keywords = $request->input('tags');
                $course->subtitle = $request->input('subtitle');
            } else {
                return Redirect::to('course/create/' . $request->input('course_id') . '/' . $request->input('step'))->with('messagetext',"Course title should not be empty")->with('msgstatus','error');
            }
		} else if ($request->input('step') == '3') {
      // Details
      $rules = array(
        'description'=>'required|min:50',        
        );    
      $validator = Validator::make(Input::all(), $rules);
      if ($validator->passes()) { 
            $course->description = $request->input('description');
            $course->course_goal = $request->input('course_goal');
            $course->int_audience = $request->input('int_audience');
            $course->course_req = $request->input('course_req');
            $course->course_level = $request->input('course_level');
            $course->course_admin_discount = $request->input('course_admin_discount');
          }
          else {
            return Redirect::to('course/create/' . $request->input('course_id') . '/' . $request->input('step'))
        ->with('messagetext',"Description field required(Minimum 50 letters)")->with('msgstatus','error');
      } 			
		} else if ($request->input('step') == '4') {
			// Image
			
		} else if ($request->input('step') == '5') {
			// Promo Video
			
		} else if ($request->input('step') == '6') {
			// Privacy
            // $privacy  = $request->input('privacy');
            $privacy  = $request->input('privacyhidden');
            if($course->approved=='1'){
                if ($privacy=='2') {
                    $getinviterusers  = $this->model->getinviteuserinfo($request->input('course_id'));
  
                    if(count($getinviterusers)>0){
                        foreach ($getinviterusers as $key => $prvalue) {
                            $alink      = \URL::to('courseview/'.$prvalue->course_id.'/'.$prvalue->slug);
                            $fromMail   = Auth::user()->email;
                            $toMail     = $prvalue->email;
                            $subject    = CNF_APPNAME." Course Invite User";
                            $data       = array('username'=>$prvalue->username,'link'=>$alink,'subject' => "[ " .CNF_APPNAME." ] Course Invite User "); 
                            $tempname   = 'course.emails.inviteuser';
                            $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
                           
                        }
                    }
                }elseif ($privacy==3) {
                    $getinviterusers  = $this->model->gettakencourseinfo($request->input('course_id'));
                    if(count($getinviterusers)>0){
                        foreach ($getinviterusers as $key => $prvalue) {
                            $pass       = $request->input('privacy_password');
                            $alink      = \URL::to('courseview/'.$prvalue->course_id.'/'.$prvalue->slug);
                            $fromMail   = Auth::user()->email;
                            $toMail     = $prvalue->email;
                            $subject    = CNF_APPNAME." Course Password ";
                            $data       = array('username'=>$prvalue->username,'link'=>$alink,'subject' => "[ " .CNF_APPNAME." ] Course Password ",'password'=>$pass); 
                            $tempname   = 'course.emails.password';
                            $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
                        }
                    }
                       
                }

                if($privacy == 3){
                    $course->privacy = $privacy.'--pwd--'.$request->input('privacy_password');
                }else{
                    $course->privacy = $privacy;   
                }

            }else{
                if($privacy == 3){
                    $course->privacy = $privacy.'--pwd--'.$request->input('privacy_password');
                }else{
                    $course->privacy = $privacy;   
                }
            }
		} else if ($request->input('step') == '7') {
			// Pricing
          $input_price = $request->input('price');
          if($course->approved=='1')
          {
            if($course->pricing!=$input_price)
            {
              $this->model->courseunpublish($input_price);
            }
            $course->pricing = $input_price;    
			     }
           else
           {
              $course->pricing = $input_price;
           }

       $alert_message = 'Price updated successfully';     
		} else if ($request->input('step') == '10') {
			// Danger Zone
			echo "<pre>";
            print_r($_POST);exit;
		}else if($request->input('step') == '17'){
      $course->course_type = $request->input('course_type');
      $course->course_session = $request->input('course_session');
      $course->coursefrom = $request->input('coursefrom');
      $course->courseto = $request->input('courseto');
      $course->course_start_time = $request->input('course_start_time');
      $course->closedate = $request->input('closedate');
      $course->duration = $request->input('course_duration');   
      $type = ($request->input('course_session')==1)?'single_session':'sequence';
      $t= $request->input('course_duration');
      $duration = sprintf("%02d:%02d", floor($t/60), $t%60);
      $startTime = $request->input('course_start_time');
      $secs = strtotime($duration)-strtotime("00:00:00");
      $EndTime = date("H:i:s",strtotime($startTime)+$secs);
      echo date_default_timezone_set("Asia/Calcutta");
        $startTimee = $request->input('coursefrom').'T'.$request->input('course_start_time').'Z';
      $EndTimee = $request->input('courseto').'T'.$EndTime.'Z';  
      $Dates = $this->DateRangeArray($request->input('coursefrom'),$request->input('courseto'));
      foreach ($Dates as $key => $value) {
        $times[]=array(
          'startTime'=>$value.'T'.$request->input('course_start_time').'Z',
          'endTime'=>$value.'T'.$EndTime.'Z',
          );
      }
      $loggeduser = \Session::get('uid');
      $liveclass=\DB::table('citrix')->where('user_id',$loggeduser)->orderBy('id', 'desc')->first();
      $timeZone = "GMT";
      $headers = array(
        "Content-type: application/json",
        "Accept: application/json",
        "Authorization:".$liveclass->access_token,
      );
      if($request->input('webinarkey')==''){
        $url = "https://api.citrixonline.com:443/G2W/rest/organizers/".$liveclass->organizer_key."/webinars";
        $fields = array(
            'subject' => $course->course_title,
            'description' => ($course->description!='')?$course->description:$course->course_title,
            'times' => $times,
            'type'=>$type,
            'timeZone'=>$timeZone,
        );
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        $webinars = json_decode($result);
        $key = property_exists($webinars,'webinarKey');
        if($key==1){
          $course->webinarkey = $webinars->webinarKey;
        }else{
          return Redirect::to('course/create/' . $request->input('course_id') . '/' . $request->input('step'))
            ->with('messagetext',$webinars->Details)->with('msgstatus','error');
        }
      }else{
          $url = "https://api.citrixonline.com:443/G2W/rest/organizers/".$liveclass->organizer_key."/webinars/".$request->input('webinarkey')."?notifyParticipants=true";
          $fields = array(
              'subject' => $course->course_title,
              'description' => ($course->description!='')?$course->description:$course->course_title,
              'times' => $times,
              'type'=>$type,
              'timeZone'=>$timeZone,
          );
          $ch = curl_init();
          curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
          curl_setopt($ch,CURLOPT_URL, $url);
          curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
          curl_setopt($ch,CURLOPT_POST, true);
          curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
          $result = curl_exec($ch);
          curl_close($ch);
          $webinars = json_decode($result);
          if($webinars!=''){
             return Redirect::to('course/create/' . $request->input('course_id') . '/' . $request->input('step'))
            ->with('messagetext',$webinars->Details)->with('msgstatus','error');
          }
      }
    }

		$course->save();
		$step = $request->input('step');
		$id = $request->input('course_id');
		$coursedata = $this->model->getcourseinfo($id);
		$this->data['course']     = $coursedata;
		$this->data['step']       = $step;

    $flash_message = isset($alert_message) ? $alert_message : \Lang::get('core.note_success');
        return Redirect::to('course/create/' . $id . '/' . $step)->with('messagetext', $flash_message)->with('msgstatus','success');
		// return view('course.createstep'.$step,$this->data);
    }


    public function getCoursenameexits(Request $request)
    {
       $coursename = $request->input('coursename');
       $this->model->checkalreadyexits($coursename);
    }

    
    public function postCreatecourse(Request $request)
    {
        $course = new Course();
        $title  = $request->input('coursename');

      

        $slug = str_slug($title);
        if (!$slug) {
            $slug = str_random(9);
        }

        $course->course_title = $title;
        $course->slug = $slug;
        $course->approved = 0;
        $course->user_id = \Auth::user()->id;
        $course->save();
        $this->model->insertauthor($course->course_id);
       return Redirect::to('course/create/'.$course->course_id.'/1')->with('messagetext'," Well done! you have created a course.")->with('msgstatus','success');

    }


    public function getDeletecourses($id='')
    {   
        $getcount   = $this->model->getvaliduser($id);
        if(count($getcount)>0){
            $cinfo      = $this->model->getcourseinfo($id);
            $alink      = \URL::to('courseview/'.$cinfo->course_id.'/'.$cinfo->slug);
            $fromMail   = Auth::user()->email;
            $toMail     = CNF_EMAIL;
            $subject    = CNF_APPNAME." Course Deleted ";
            $data       = array('authorname'=>Auth::user()->username,'link'=>$alink,'title'=>$cinfo->course_title,'username'=>'Admin'); 
            $tempname   = 'mailtemplates.course_delete';
            $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
            $this->deleteCourseImage($id);
            $this->model->deletecourses($id);
            return Redirect::to('user/mycourse')->with('messagetext',"Course Has Been Deleted successfully.")->with('msgstatus','success');
        }else{
            return Redirect::to('user/mycourse')->with('messagetext'," Sorry! Error occur try again later.")->with('msgstatus','error');
        }
        
    }

    public function postTestvideo(Request $request)
    {
        $video = Input::file('testVideo');
        if( $video->getPathName())
        {
            $file_tmp_name = $video->getPathName();
            $file_name = explode('.',$video->getClientOriginalName());
            $file_name = $file_name[0].'_'.time().rand(4,9999);
            $file_type = $video->getClientMimeType();
            $extension = $video->getClientOriginalExtension();
			      $file_title = $video->getClientOriginalName();
            // ffmpeg.exe file path
            $file_name = str_slug($file_name, "-");
            
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {				$ffmpeg_path = base_path().'\resources\assets\ffmpeg\ffmpeg_win\ffmpeg';			} else {				$ffmpeg_path = base_path().'/resources/assets/ffmpeg/ffmpeg_lin/ffmpeg.exe';			}

            $ffmpeg = new VideoHelpers($ffmpeg_path , $file_tmp_name, $file_name);
            $ffmpeg->convertImages();
            //$ffmpeg->convertVideos($file_type);
            $duration = $ffmpeg->getDuration();
      			$duration = explode('.',$duration);
      			$duration = $duration[0];
      			$created_at=time();

             $mime = $video->getClientMimeType();
            if ($mime == "video/x-matroska" || $mime == "video/mp4" || $mime == "video/x-ms-wmv" || $mime == "video/x-msvideo") {
                  $request->file('testVideo')->move('./uploads/videos/', 'raw_'.$created_at.'_'.$file_name.'.'.$extension);
            }
       
            $courseVideos = new CourseVideos;
            $courseVideos->video_title = $file_name;
            $courseVideos->video_name = $file_title;
            $courseVideos->video_type = $extension;
            $courseVideos->duration = $duration;
            $courseVideos->image_name = $file_name.'.jpg';
            $courseVideos->video_tag = 'curriculum';
            $courseVideos->uploader_id = \Auth::user()->id;
            $courseVideos->processed = '0';
            $courseVideos->created_at = $created_at;
            $courseVideos->updated_at = $created_at;
            if($courseVideos->save()){
                $courseid = \Request::segment(3);
                if(!empty($courseid)){
                    $course   = Course::find($courseid);
                    $course->test_video = $courseVideos->id;
                    $course->save();
                }
                $return_data = array(
                    'status'=>true,
                    'video_path'=> 'Video Processing',
                    'mime_type' => "video/webm",
                    'video_name'=> $file_name
                );
            }else{
                $return_data = array(
                    'status'=>false,
                );
            }
        }else{
            $return_data = array(
                'status'=>false,
                'error_msg'=>'Invalid File.'
            );
        }
        echo json_encode($return_data);
        exit;
    }

    public function deleteTestvideo(Request $request)
    {
        $video_file = explode('.', $request->get("video_file"));
        $path = './uploads/videos/'.$video_file[0];
        $course = Course::find($request->get("course_id"));
        $courseVideos = new CourseVideos;
        $courseVideos = $courseVideos->where('video_title',$video_file[0])->first();
       
        if(file_exists($path.'.webm'))
          File::Delete($path.'.webm');
        
       if(file_exists($path.'.mp4'))
          File::Delete($path.'.mp4');
          
        if(file_exists($path.'.ogv'))
           File::Delete($path.'.ogv');

         if($courseVideos->aws_url != NULL){
          preg_match("'https://(.*?).s3.amazonaws.com'si", $courseVideos->aws_url, $match);
          if(isset($match[1])){
            $bucket = $match[1];
            $s3 = new \S3(AWS_KEY, AWS_SECRET);
            if($s3->getObjectInfo($bucket, $courseVideos->video_title.".mp4", false)){
              $s3->deleteObject($bucket, $courseVideos->video_title.".mp4", 'public-read');
              $s3->deleteObject($bucket, $courseVideos->video_title.".webm", 'public-read');
              $s3->deleteObject($bucket, $courseVideos->video_title.".ogv", 'public-read');
            }
          }
        }


        if(file_exists('./uploads/images/'.$video_file[0].'.jpg'))
          File::Delete('./uploads/images/'.$video_file[0].'.jpg');
        
          $courseVideos->delete();
          echo 'success';
          $course->test_video = 0;
          $course->save();

    }


    public function postUploadvideo(Request $request)
    {
        $video = $request->file('promoVideo');
        $courseid = $request->input('course_id');
        $steps = $request->input('step');
        if ($video->getPathName()) {
            $file_tmp_name = $video->getPathName();
            $file_name = explode('.',$video->getClientOriginalName());
            $file_name = $file_name[0].'_'.time().rand(4,9999);
            $file_type = $video->getClientMimeType();
            $extension = $video->getClientOriginalExtension();
			      $file_title = $video->getClientOriginalName();
            $file_name = str_slug($file_name, "-");
            $file_title =str_slug($file_title, "-");
            // ffmpeg.exe file path
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {				$ffmpeg_path = base_path().'\resources\assets\ffmpeg\ffmpeg_win\ffmpeg';			} else {				$ffmpeg_path = base_path().'/resources/assets/ffmpeg/ffmpeg_lin/ffmpeg.exe';			}
            $ffmpeg = new VideoHelpers($ffmpeg_path , $file_tmp_name, $file_name);
            $ffmpeg->convertImages();
            //$ffmpeg->convertVideos($file_type);
            $duration = $ffmpeg->getDuration();
      			$duration = explode('.',$duration);
      			$duration = $duration[0];
      			$created_at=time();
            $mime = $video->getClientMimeType();
            
              if ($mime == "video/x-matroska" || $mime == "video/mp4" || $mime == "video/x-ms-wmv" || $mime == "video/x-msvideo") {
      			     $request->file('promoVideo')->move('./uploads/videos/', 'raw_'.$created_at.'_'.$file_name.'.'.$extension);
              }
            $courseVideos = new CourseVideos;
            $courseVideos->video_title = $file_name;
            $courseVideos->video_name = $file_title;
            $courseVideos->video_type = $extension;
            $courseVideos->duration = $duration;
            $courseVideos->image_name = $file_name.'.jpg';
            $courseVideos->video_tag = 'curriculum';
            $courseVideos->uploader_id = \Auth::user()->id;
            $courseVideos->processed = '0';
            $courseVideos->created_at = $created_at;
            $courseVideos->updated_at = $created_at;
          
            if($courseVideos->save()){
                
                if(!empty($courseid)){
                    $course   = Course::find($courseid);
                    $course->video = $courseVideos->id;
                    $course->save();
                }
                $return_data = array(
                        'status'=>true,
                        'video_path'=> 'video processing',
                        'mime_type' => "video/webm",
                        'video_name'=> $file_name
                    );
                }else{
                    $return_data = array(
                        'status'=>false,
                    );
                }
            }else{
                $return_data = array(
                    'status'=>false,
                    'error_msg'=>'Invalid File.'
                );
            }

            echo json_encode($return_data);
            exit;
    }

    /*
    * task - delete promo video 
    * @parm - testVideo ( input file tag )
    * @plugin - fileupload ajax
    */

    public function deleteUploadvideo( Request $request ){
        $video_file = explode('.', $request->get("video_file"));
        $path = './uploads/videos/'.$video_file[0];
        $course = Course::find($request->get("course_id"));
        $courseVideos = new CourseVideos;
        $courseVideos = $courseVideos->where('video_title',$video_file[0])->first();
        
        if(file_exists($path.'.webm'))
          File::Delete($path.'.webm');
        
        if(file_exists($path.'.mp4'))
          File::Delete($path.'.mp4');
          
        if(file_exists($path.'.ogv'))
           File::Delete($path.'.ogv');

         if($courseVideos->aws_url != NULL){
          preg_match("'https://(.*?).s3.amazonaws.com'si", $courseVideos->aws_url, $match);
          if(isset($match[1])){
            $bucket = $match[1];
            $s3 = new \S3(AWS_KEY, AWS_SECRET);
            if($s3->getObjectInfo($bucket, $courseVideos->video_title.".mp4", false)){
              $s3->deleteObject($bucket, $courseVideos->video_title.".mp4", 'public-read');
              $s3->deleteObject($bucket, $courseVideos->video_title.".webm", 'public-read');
              $s3->deleteObject($bucket, $courseVideos->video_title.".ogv", 'public-read');
            }
          }
        }

        if(file_exists('./uploads/images/'.$video_file[0].'.jpg'))
          File::Delete('./uploads/images/'.$video_file[0].'.jpg');
        
          $courseVideos->delete();
          echo 'success';
          $course->video = 0;
          $course->save();
    }

    public function getInstructors(Request $request)
    {
        $courseid = \Request::segment(3);
        $searchtext = $request->input('term');
        $allusers = $this->model->getallusersinfos($courseid,$searchtext);
        $dataarray      = array();
        if(count($allusers)>0){
            for ($i=0; $i < count($allusers); $i++) { 
                $dataarray[$i]['id']    = $allusers[$i]->id;         
                $dataarray[$i]['value'] = $allusers[$i]->email;         
            }
        }

        echo json_encode($dataarray);exit;
    }

    public function postAddinstructors(Request $request)
    {
        $courseid   = $request->input('courseid');
        $emailid    = $request->input('emails');

        if($emailid == '')
        {
          $datas = array('status'=>3,'results'=>'Please enter valid email');
          echo json_encode($datas);exit;
        }
        
        $checkalready = $this->model->getmanageinstructors($courseid,$emailid);
        if(count($checkalready)=='0'){
            $allusers   = $this->model->getusersinfobyemail($courseid,$emailid);
            if(!empty($allusers['0']->first_name) && !empty($allusers['0']->last_name)){
                $userinfo   = array('user_id'=>$allusers['0']->id,'username'=>$allusers['0']->first_name.' '.$allusers['0']->last_name);
                $datas      = array('status'=>1,'results'=>$userinfo);
            }elseif (!empty($allusers['0']->first_name)) {
                $userinfo   = array('user_id'=>$allusers['0']->id,'username'=>$allusers['0']->first_name);
                $datas      = array('status'=>1,'results'=>$userinfo);
            }else{
              $datas      = array('status'=>3,'results'=>'Please enter valid email');  
            }
            
        }else{
            $datas      = array('status'=>2,'results'=>'already exists');
        }
        echo json_encode($datas);exit;
    }

    public function postDeleteinstructors(Request $request)
    {
        $courseid   = $request->input('courseid');
        $userid     = $request->input('userid');
        $this->model->getdeleteinstructors($courseid,$userid);
        echo "success";
    }

    public function postUpdateinstructors(Request $request)
    {
        $courseid   = $request->input('course_id');
        $userid     = $request->input('user_id');
        $visible    = $request->input('visiblehidden');
        $canedit    = $request->input('canedithidden');
        $steps      = $request->input('step');  
        $loggeduser = \Session::get('uid');
        $courseinfo = $this->model->getcourseinfo($courseid); 
        if(count($userid)){
            for ($i=0; $i < count($userid); $i++) { 

                $exists = \DB::table('manage_instructors')->where('course_id', '=', $courseid)->where('user_id', '=', $userid[$i])->get();
                $dataarray                          = array();
                 if(count($exists)=='0'){
                   $dataarray['course_id']          = $courseid;
                   $dataarray['user_id']            = $userid[$i];
                   // if(isset($visible[$i])){
                   //      $dataarray['instrctor_visitble']  = $visible[$i];
                   // }else{
                   //      $dataarray['instrctor_visitble']  = 0;
                   // }
                   $dataarray['instrctor_edit']     = 1;
                   $dataarray['created_at']         = date("Y-m-d H:i:s");
                   $dataarray['modified_at']        = date("Y-m-d H:i:s");
                   \DB::table('manage_instructors')->insertGetId($dataarray);
                 }else{
                   $insid                            = $exists['0']->manage_instrctor_id;
        
                   // if(isset($visible[$i])){
                   //      $dataarray['instrctor_visitble']  = $visible[$i];
                   //  }else{
                   //      $dataarray['instrctor_visitble']  = 0;
                   //  }
                   // if(isset($canedit[$i])){
                   //      $dataarray['instrctor_edit']      = $canedit[$i];
                   //  }else{
                   //      $dataarray['instrctor_edit']      = 0;
                   //  }
                   $dataarray['modified_at']         = date("Y-m-d H:i:s");
                   \DB::table('manage_instructors')->where('manage_instrctor_id', $insid)->update($dataarray);
                 }
                
            }//exit;
        }

        if($courseinfo->approved=='1'){

            // \DB::table('manage_instructors')->where('course_id',$courseid)->where('user_id','!=',$loggeduser)->delete();

            // \DB::table('manage_instructors')->where('course_id',$cid)->update(array('instrctor_status'=>'0'));
            $allinstructor        = $this->model->getinstructors($courseid);
            if(count($allinstructor)>0){
              foreach ($allinstructor as $key => $ivalue) {
                  // $courseinfo = $this->model->getcourseinfo($ivalue->course_id);
                  $fromdetails = \DB::table('users')->where('id', '=', $courseinfo->user_id)->get();
                  $getdetails = \DB::table('users')->where('id', '=', $ivalue->user_id)->where('id', '!=', $courseinfo->user_id)->get();

                   if(count($getdetails)>0){
                      $alink      = \URL::to('courseview/'.$courseinfo->course_id.'/'.$courseinfo->slug);
                      $fromMail   = $fromdetails['0']->email;
                      $toMail     = $getdetails['0']->email;
                      $subject    = CNF_APPNAME." Invite Instructor ";
                      $data       = array('username'=>$getdetails['0']->username,'link'=>$alink,'subject' => "[ " .CNF_APPNAME." ] Invite Instructor "); 
                      $tempname   = 'course.emails.instructor_invite';
                      $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
                      
                   }
              }
          }
        }

        return Redirect::to('course/create/' . $courseid . '/' . $steps)->with('messagetext', 'updated')->with('msgstatus','success');;
        
    }

    public function postInsertcomments(Request $request)
    {
        $courseid   = $request->input('courseid');
        $comments   = $request->input('comments');

        if(!empty($comments) && !empty($courseid)){
             $lastinsertid        = $this->model->insertcomment($courseid,$comments);
             $result              = array();
             $result['imgpath']   = \SiteHelpers::customavatar(\Session::get('email'),\Session::get('uid'));
             $result['fname']     = \Session::get('fid');
             $result['comments']  = $comments;
             $result['commentid'] = $lastinsertid;
             $result['courseid']  = $courseid;
             $result['userid']    = \Session::get('uid');
             $datas     = array('status'=>1,'results'=>$result);

        }else{
             $datas      = array('status'=>2,'results'=>'error');
        }
        echo json_encode($datas);exit;
    }

    public function postInsertreply(Request $request)
    {
        $commentid  = $request->input('commentid');
        $courseid   = $request->input('courseid');
        $comments   = $request->input('comments');

        if(!empty($comments) && !empty($courseid) && !empty($commentid)){
             $this->model->insertcommentreply($commentid,$courseid,$comments);
             $result              = array();
             $result['imgpath']   = \SiteHelpers::customavatar(\Session::get('email'),\Session::get('uid'));
             $result['fname']     = \Session::get('fid');
             $result['comments']  = $comments;
             $result['commentid'] = $commentid;
             $result['courseid']  = $courseid;
             $result['userid']    = \Session::get('uid');
             $datas     = array('status'=>1,'results'=>$result);
        }else{
            $datas      = array('status'=>2,'results'=>'error');
        }
        echo json_encode($datas);exit;
    }
	
    public function postSurvey(Request $request)
    {
        $surveyid = $request->input('survey');
        $courseid = $request->input('course_id');
        $steps = $request->input('step');
		if(!empty($surveyid)){
			$courseSurvey = CourseSurvey::find($surveyid);
		} else {
			$courseSurvey = new CourseSurvey;
		}
		$courseSurvey->user_id = \Auth::user()->id;
		$courseSurvey->course_id = $courseid;
		$courseSurvey->language_id = $request->input('lang_id');
		$courseSurvey->category_id = $request->input('cat_id');
		$courseSurvey->objective_persona = $request->input('objective_persona');
		$courseSurvey->resource_persona = $request->input('resource_persona');
		$courseSurvey->email_subscribers = $request->input('email_subscribers');
		$courseSurvey->youtube_subscribers = $request->input('youtube_subscribers');
		$now_date = date("Y-m-d H:i:s");
		$courseSurvey->created_at = $now_date;
		$courseSurvey->updated_at = $now_date;
		$courseSurvey->updated_at = $now_date;
		if($courseSurvey->save() && empty($surveyid) && !empty($courseid)){
			$course = Course::find($courseid);
			$course->survey = $courseSurvey->course_survey_id;
			$course->save();
		}
    $course                   = Course::find($courseid);
    $toMail                   = CNF_EMAIL;
    // $toMail                   = \Session::get('eid');
    $subject                  = CNF_APPNAME.' '.$course->course_title." Survey ";      
    $maildata                 = array();
    $languagename             = \bsetecHelpers::siteLanguage($request->input('lang_id'));
    $topicname                = \bsetecHelpers::sitecategories($request->input('cat_id'));
    $maildata['username']     = \Session::get('fid');
    $maildata['title']        = $course->course_title;
    $maildata['language']     = $languagename->language;
    $maildata['topic']        = $topicname->name;
    $maildata['goal']         = $request->input('objective_persona');
    $maildata['ideal']        = $request->input('resource_persona');
    $maildata['subscribers']  = $request->input('email_subscribers');
    $maildata['youtubes']     = $request->input('youtube_subscribers');

    $fromMail                 = \Session::get('eid');
    // $fromMail                 = CNF_EMAIL;
    $tempname                 = 'mailtemplates.course_survey';
        
    $this->sendmail->getMail($fromMail,$toMail,$subject,$maildata,$tempname);


		return Redirect::to('course/create/'.$courseid.'/'.$steps)->with('messagetext','Saved successfully!')->with('msgstatus','success');
    }


	
    public function postSectionSave(Request $request)
    {	
		$data['course_id'] = $request->input('courseid');
		$data['title'] = $request->input('section');
		$data['sort_order'] = $request->input('position');
		$now_date = date("Y-m-d H:i:s");
		$data['createdOn'] = $now_date;
		$data['updatedOn'] = $now_date;
		
		if($request->input('sid') == 0){
			$newID = $this->model->insertSectionRow($data , '');
		} else {
			$newID = $this->model->insertSectionRow($data , $request->input('sid'));
		}
		echo $newID;
	}
	
    public function postLectureSave(Request $request)
    {	
		$data['section_id'] = $request->input('sectionid');
		$data['title'] = $request->input('lecture');
		$data['sort_order'] = $request->input('position');
		$data['type'] = '0';
		$now_date = date("Y-m-d H:i:s");
		$data['createdOn'] = $now_date;
		$data['updatedOn'] = $now_date;
		
		if($request->input('lid') == 0){
			$newID = $this->model->insertLectureQuizRow($data , '');
		} else {
			$newID = $this->model->insertLectureQuizRow($data , $request->input('lid'));
		}
		echo $newID;
	}
	
    public function postQuizSave(Request $request)
    {	
		$data['section_id'] = $request->input('sectionid');
		$data['title'] = $request->input('quiz');
		$data['description'] = $request->input('description');
		$data['sort_order'] = $request->input('position');
		$data['type'] = '1';
		$now_date = date("Y-m-d H:i:s");
		$data['createdOn'] = $now_date;
		$data['updatedOn'] = $now_date;
		
		if($request->input('lid') == 0){
			$newID = $this->model->insertLectureQuizRow($data , '');
		} else {
			$newID = $this->model->insertLectureQuizRow($data , $request->input('lid'));
		}
		echo $newID;
	}
	
	public function postQuizQuestionAdd(Request $request)
    {	
		$data['quiz_id'] = $request->input('lid');
		$data['question'] = $request->input('question');
		$data['question_type'] = $request->input('qtype');
		$data['options'] = json_encode($request->input('options'));
		$data['correct_option'] = $request->input('coption');
		$data['sort_order'] = $request->input('position');
		$now_date = date("Y-m-d H:i:s");
		$data['createdOn'] = $now_date;
		$data['updatedOn'] = $now_date;
		
		if($request->input('qid') == 0){
			$newID = $this->model->insertQuizQuestionRow($data , '');
		} else {
			$newID = $this->model->insertQuizQuestionRow($data , $request->input('qid'));
		}
		echo $newID;
	}
		
    public function postCurriculumSort(Request $request)
    {	
		if($request->input('type') == 'section') {
			$sections = $request->input('sectiondata');
			if(!empty($sections)){
				foreach($sections as $section){
					$data['sort_order'] = $section['position'];
					$newID = $this->model->insertSectionRow($data , $section['id']);
				}
			}
		} else if($request->input('type') == 'lecturequiz') {
			$lecturequiz = $request->input('lecturequizdata');
			if(!empty($lecturequiz)){
				foreach($lecturequiz as $lq){
					$data['section_id'] = $lq['sectionid'];
					$data['sort_order'] = $lq['position'];
					$newID = $this->model->insertLectureQuizRow($data , $lq['id']);
				}
			}
		}
	}
		
    public function postCurriculumQuizQuestionSort(Request $request)
    {	
		if($request->input('type') == 'quizquestion') {
			$questions = $request->input('quizquestiondata');
			if(!empty($questions)){
				foreach($questions as $question){
					$data['sort_order'] = $question['position'];
					$newID = $this->model->insertQuizQuestionRow($data , $question['id']);
				}
			}
		}
	}
	
	public function postSectionDelete(Request $request){
		$this->model->postSectionDelete($request->input('sid'));
		echo '1';
	}
	
	public function postLectureQuizDelete(Request $request){
		$this->model->postLectureQuizDelete($request->input('lid'));
		echo '1';
	}
	
	public function postQuizQuestionDelete(Request $request){
		$this->model->postQuizQuestionDelete($request->input('qid'));
		echo '1';
	}
	
	public function postLectureResourceDelete(Request $request){
		$this->model->postLectureResourceDelete($request->input('lid'),$request->input('rid'));
		echo '1';
	}
	
	public function postLectureDescSave(Request $request)
    {	
		$data['description'] = $request->input('lecturedescription');
		$now_date = date("Y-m-d H:i:s");
		$data['updatedOn'] = $now_date;
		
		if($request->input('lid') == 0){
			$newID = $this->model->insertLectureQuizRow($data , '');
		} else {
			$newID = $this->model->insertLectureQuizRow($data , $request->input('lid'));
		}
		echo $newID;
	}
	
	public function postLectureVideoSave($lid,Request $request)
    {
        $video = $request->file('lecturevideo');
        $file = array('video' => $video);
        $rules = array('video' => 'required|mimes:mp4,mov,avi,flv');
        $validator = Validator::make($file, $rules);

            $file_tmp_name = $video->getPathName();
            $file_name = explode('.',$video->getClientOriginalName());
            $file_name = $file_name[0].'_'.time().rand(4,9999);
            $file_type = $video->getClientMimeType();
            $extension = $video->getClientOriginalExtension();
			$file_title = $video->getClientOriginalName();
            $file_name = str_slug($file_name, "-");
            // ffmpeg.exe file path
            $file_name = str_slug($file_name, "-");
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {				$ffmpeg_path = base_path().'\resources\assets\ffmpeg\ffmpeg_win\ffmpeg';			} else {				$ffmpeg_path = base_path().'/resources/assets/ffmpeg/ffmpeg_lin/ffmpeg.exe';			}

            $ffmpeg = new VideoHelpers($ffmpeg_path , $file_tmp_name, $file_name);
            $ffmpeg->convertImages();
            //$ffmpeg->convertVideos($file_type);
            $duration = $ffmpeg->getDuration();
			$duration = explode('.',$duration);
			$duration = $duration[0];
			$created_at=time();
			$request->file('lecturevideo')->move('./uploads/videos/', 'raw_'.$created_at.'_'.$file_name.'.'.$extension);
       
            $courseVideos = new CourseVideos;
            $courseVideos->video_title = $file_name;
            $courseVideos->video_name = $file_title;
            $courseVideos->video_type = $extension;
            $courseVideos->duration = $duration;
            $courseVideos->image_name = $file_name.'.jpg';
            $courseVideos->video_tag = 'curriculum';
            $courseVideos->uploader_id = \Auth::user()->id;
            $courseVideos->processed = '0';
            $courseVideos->created_at = $created_at;
            $courseVideos->updated_at = $created_at;
            if($courseVideos->save()){
                if(!empty($lid)){
					$this->model->checkDeletePreviousFiles($lid);
					$data['media'] = $courseVideos->id;
					$data['media_type'] = '0';
					$data['publish'] = '0';
					$newID = $this->model->insertLectureQuizRow($data , $lid);
                }
                $return_data = array(
                    'status'    => true,
                    'duration'  => $duration,
                    'file_title'=> $file_title,
                    'file_link'=> url('')."/uploads/videos/".$file_name.".webm",
                );
            }else{
                $return_data = array(
                    'status'=>false,
                );
            }
        echo json_encode($return_data);
        exit;
	}
			
	public function postLectureAudioSave($lid,Request $request)
    {
		$audio = $request->file('lectureaudio');
        $file = array('audio' => $audio);
        $rules = array('audio' => 'required|mimes:mp3,wav');
        $validator = Validator::make($file, $rules);
        $file_tmp_name = $audio->getPathName();
        $file_name = explode('.',$audio->getClientOriginalName());
        $file_name = $file_name[0].'_'.time().rand(4,9999);
        $file_type = $audio->getClientOriginalExtension();
  			$file_title = $audio->getClientOriginalName();
  			$file_size = $audio->getSize();
        $file_title = str_slug($file_title, "-");
        $file_name = str_slug($file_name, "-");
           if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') 
            {				
              $ffmpeg_path = base_path().'\resources\assets\ffmpeg\ffmpeg_win\ffmpeg';			} else {				$ffmpeg_path = base_path().'/resources/assets/ffmpeg/ffmpeg_lin/ffmpeg.exe';		
          	}
            
            $ffmpeg = new VideoHelpers($ffmpeg_path , $file_tmp_name, $file_name);
            $duration = $ffmpeg->getDuration();
			$duration = explode('.',$duration);
			$duration = $duration[0];
     		
			$request->file('lectureaudio')->move('./uploads/files/', $file_name.'.'.$file_type);
       
            $courseFiles = new CourseFiles;
			$courseFiles->file_title = $file_title;
            $courseFiles->file_name = $file_name;
            $courseFiles->file_type = $file_type;
            $courseFiles->file_extension = $file_type;
            $courseFiles->file_size = $file_size;
            $courseFiles->duration = $duration;
            if($file_type!='mp3' && $file_type!='wav'){
              $courseFiles->processed = 0;
            }
            $courseFiles->file_tag = 'curriculum';
            $courseFiles->uploader_id = \Auth::user()->id;
            $courseFiles->created_at = time();
            $courseFiles->updated_at = time();
            if($courseFiles->save()){
                if(!empty($lid)){
					$data['media'] = $courseFiles->id;
					$data['media_type'] = '1';
					$data['publish'] = '0';
					$newID = $this->model->insertLectureQuizRow($data , $lid);
                }
                $return_data = array(
                    'status'=>true,
                    'duration'  => $duration,
                    'file_title'=> $file_title,
                    'file_type' => $file_type,
                    'file_link'=> url('')."/uploads/files/".$file_name.'.'.$file_type,
                );
            }else{
                $return_data = array(
                    'status'=>false,
                );
            }
        echo json_encode($return_data);
        exit;
	}
		
	public function postLecturePresentationSave($lid,Request $request)
    {
		$document = $request->file('lecturepre');
        $file = array('document' => $document);
        $rules = array('document' => 'required|mimes:pdf');
        $validator = Validator::make($file, $rules);

        if( $validator->fails() )
        {
			$return_data[] = array(
                'status'=>false,
            );
		} else {
			$pdftext = file_get_contents($document);
			$pdfPages = preg_match_all("/\/Page\W/", $pdftext, $dummy);
            $file_tmp_name = $document->getPathName();
            $file_name = explode('.',$document->getClientOriginalName());
            $file_name = $file_name[0].'_'.time().rand(4,9999);
            $file_type = $document->getClientOriginalExtension();
			$file_title = $document->getClientOriginalName();
			$file_size = $document->getSize();
			
			$request->file('lecturepre')->move('./uploads/files/', $file_name.'.'.$file_type
			);
       
            $courseFiles = new CourseFiles;
            $courseFiles->file_name = $file_name;
            $courseFiles->file_title = $file_title;
            $courseFiles->file_type = $file_type;
            $courseFiles->file_extension = $file_type;
            $courseFiles->file_size = $file_size;
			$courseFiles->duration = $pdfPages;
            $courseFiles->file_tag = 'curriculum_presentation';
            $courseFiles->uploader_id = \Auth::user()->id;
            $courseFiles->created_at = time();
            $courseFiles->updated_at = time();
            if($courseFiles->save()){
                if(!empty($lid)){
					$data['media'] = $courseFiles->id;
					$data['media_type'] = '5';
					$data['publish'] = '0';
					$newID = $this->model->insertLectureQuizRow($data , $lid);
                }
				if($pdfPages == 1){ 
					$pdfPage = $pdfPages.' Page'; 
				} else { 
					$pdfPage = $pdfPages.' Pages';
				}
                $return_data = array(
                    'status'=>true,
                    'file_title'=> $file_title,
                    'duration'=> $pdfPage
                );
            }else{
                $return_data = array(
                    'status'=>false,
                );
            }
        }
        echo json_encode($return_data);
        exit;
	}
		
	public function postLectureDocumentSave($lid,Request $request)
    {
		$document = $request->file('lecturedoc');
        $file = array('document' => $document);
        $rules = array('document' => 'required|mimes:pdf');
        $validator = Validator::make($file, $rules);

        if( $validator->fails() )
        {
			$return_data[] = array(
                'status'=>false,
            );
		} else {
			$pdftext = file_get_contents($document);
			$pdfPages = preg_match_all("/\/Page\W/", $pdftext, $dummy);
            $file_tmp_name = $document->getPathName();
            $file_name = explode('.',$document->getClientOriginalName());
            $file_name = $file_name[0].'_'.time().rand(4,9999);
            $file_type = $document->getClientOriginalExtension();
			$file_title = $document->getClientOriginalName();
			$file_size = $document->getSize();
			
			$request->file('lecturedoc')->move('./uploads/files/', $file_name.'.'.$file_type
			);
       
            $courseFiles = new CourseFiles;
            $courseFiles->file_name = $file_name;
            $courseFiles->file_title = $file_title;
            $courseFiles->file_type = $file_type;
            $courseFiles->file_extension = $file_type;
            $courseFiles->file_size = $file_size;
			$courseFiles->duration = $pdfPages;
            $courseFiles->file_tag = 'curriculum';
            $courseFiles->uploader_id = \Auth::user()->id;
            $courseFiles->created_at = time();
            $courseFiles->updated_at = time();
            if($courseFiles->save()){
                if(!empty($lid)){
					$data['media'] = $courseFiles->id;
					$data['media_type'] = '2';
					$data['publish'] = '0';
					$newID = $this->model->insertLectureQuizRow($data , $lid);
                }
				if($pdfPages == 1){ 
					$pdfPage = $pdfPages.' Page'; 
				} else { 
					$pdfPage = $pdfPages.' Pages';
				}
                $return_data = array(
                    'status'=>true,
                    'file_title'=> $file_title,
                    'duration'=> $pdfPage
                );
            }else{
                $return_data = array(
                    'status'=>false,
                );
            }
        }
        echo json_encode($return_data);
        exit;
	}
		
	public function postLectureResourceSave($lid,Request $request)
    {
		    $document = $request->file('lectureres');
        // $file = array('document' => $document);
        // $rules = array('document' => 'required|mimes:pdf,doc,docx');
        // $validator = Validator::make($file, $rules);
        // if( $validator->fails() )
        // {
        //   echo '<pre>';print_r($document);exit;
        // 	$return_data[] = array(
        //             'status'=>false,
        //         );
		    // } else {
            $file_tmp_name = $document->getPathName();
            $file_name = explode('.',$document->getClientOriginalName());
            $file_name = $file_name[0].'_'.time().rand(4,9999);
            $file_type = $document->getClientOriginalExtension();
			$file_title = $document->getClientOriginalName();
			$file_size = $document->getSize();
			
			if($file_type == 'pdf'){
				$pdftext = file_get_contents($document);
				$pdfPages = preg_match_all("/\/Page\W/", $pdftext, $dummy);
			} else {
				$pdfPages = '';
			}
			
			$request->file('lectureres')->move('./uploads/files/', $file_name.'.'.$file_type);
       
            $courseFiles = new CourseFiles;
            $courseFiles->file_name = $file_name;
            $courseFiles->file_title = $file_title;
            $courseFiles->file_type = $file_type;
            $courseFiles->file_extension = $file_type;
            $courseFiles->file_size = $file_size;
			$courseFiles->duration = $pdfPages;
            $courseFiles->file_tag = 'curriculum_resource';
            $courseFiles->uploader_id = \Auth::user()->id;
            $courseFiles->created_at = time();
            $courseFiles->updated_at = time();
            if($courseFiles->save()){
                if(!empty($lid)){
					$data['resources'] = $courseFiles->id;
					$newID = $this->model->insertLectureQuizResourceRow($data , $lid);
                }
                $return_data = array(
                    'status'=>true,
                    'file_id'=> $courseFiles->id,
                    'file_title'=> $file_title,
                    'file_size'=> \bsetecHelpers::HumanFileSize($file_size)
                );
            }else{
                $return_data = array(
                    'status'=>false,
                );
            }
        // }
        echo json_encode($return_data);
        exit;
	}

    public function postLectureTextSave(Request $request)
    {
        $document = $request->input('lecturedescription');
        $lid = $request->input('lid');        
        if(!empty($lid)){
            $data['contenttext'] = $document;
            $data['media_type'] = '3';
			$data['publish'] = '0';
            $newID = $this->model->insertLectureQuizRow($data , $lid);
        }
        $return_data = array(
            'status'=>true,
            'file_title'=> 'Text'
        );
        
        echo json_encode($return_data);
        exit;
    }

    public function postLectureLibrarySave(Request $request)
    {
		$data['media'] = $request->input('lib');
		$data['media_type'] = $request->input('type');
		$newID = $this->model->insertLectureQuizRow($data , $request->input('lid'));
					
		if($request->input('type') == 0){
			$libraryDetails = $this->model->getvideoinfo($request->input('lib'));
      $file_title = $libraryDetails[0]->video_name;
			$duration = $libraryDetails[0]->duration;
			$processed = $libraryDetails[0]->processed;
			if($processed == 1)
				$file_link = url('')."/uploads/videos/".$libraryDetails[0]->video_title.'.webm';
			else 
				$file_link = '';
			
		} else if($request->input('type') == 1){
		
			$libraryDetails = $this->model->getfileinfo($request->input('lib'));
			$file_title = $libraryDetails[0]->file_title;
			$duration = $libraryDetails[0]->duration;
			$file_link = url('')."/uploads/files/".$libraryDetails[0]->file_name.'.'.$libraryDetails[0]->file_extension;
			
		}else if($request->input('type') == 2 || $request->input('type') == 5){
		
			$libraryDetails = $this->model->getfileinfo($request->input('lib'));
			$file_title = $libraryDetails[0]->file_title;
			if($libraryDetails[0]->duration <= 1){ 
				$pdfPage = $libraryDetails[0]->duration.' Page';
			} else { 
				$pdfPage = $libraryDetails[0]->duration.' Pages';
			}
			$duration = $pdfPage;
			$file_link = '';
			
		}
		
		$return_data = array(
			'status'=>true,
			'duration'  => $duration,
			'file_title'=> $file_title,
			'file_link'=> $file_link
		);
		
        echo json_encode($return_data);
        exit;
    }


    public function postLectureLibraryResourceSave(Request $request)
    {
		$data['resources'] = $request->input('lib');
		$newID = $this->model->insertLectureQuizResourceRow($data , $request->input('lid'));
		$return_data = array(
			'status'=>true,
			'file_id'=> $request->input('lib')
		); 
		echo json_encode($return_data);
        exit;
    }

    public function postLectureExternalResourceSave(Request $request)
    {
		$lid = $request->input('lid');
		$courseFiles = new CourseFiles;
		$courseFiles->file_name = $request->input('link');
		$courseFiles->file_title = $request->input('title');
		$courseFiles->file_type = 'link';
		$courseFiles->file_extension = 'link';
		$courseFiles->file_tag = 'curriculum_resource_link';
		$courseFiles->uploader_id = \Auth::user()->id;
		$courseFiles->created_at = time();
		$courseFiles->updated_at = time();
		if($courseFiles->save()){
			if(!empty($lid)){
				$data['resources'] = $courseFiles->id;
				$newID = $this->model->insertLectureQuizResourceRow($data , $lid);
			}
			$return_data = array(
				'status'=>true,
				'file_id'=> $courseFiles->id,
				'file_title'=> $request->input('title'),
				'file_size'=> ''
			);
		}else{
			$return_data = array(
				'status'=>false,
			);
		}
		echo json_encode($return_data);
        exit;
    }

    public function postLecturePublishSave(Request $request)
    {
        $data['publish'] = $request->input('publish');
		if($request->input('lid') == 0){
			$newID = $this->model->insertLectureQuizRow($data , '');
		} else {
			$newID = $this->model->insertLectureQuizRow($data , $request->input('lid'));
		}
    $publish = $request->input('publish');
    $lid     = $request->input('lid');
    if($publish=='1' && $lid!='0'){
        $getcourseid              = $this->model->getcourseid($lid);
        if(count($getcourseid)>0){
          $cid                      = $getcourseid['0']->course_id;
          $courseinfo               = $this->model->getcourseinfo($cid);
          $getstudents              = $this->model->getallsubscribers($cid);
          if(count($getstudents)>0){
             foreach ($getstudents as $key => $svalue) {
              $toMail                   = $svalue->email;
              $subject                  = CNF_APPNAME." Course New Lecture ";      
              $maildata                 = array();
              $maildata['authorname']   = \Session::get('fid');
              $maildata['title']        = $courseinfo->course_title;
              $maildata['username']     = $svalue->username;
              $maildata['link']         = \URL::to('courseview/'.$courseinfo->course_id.'/'.$courseinfo->slug);
              $fromMail                 = \Session::get('eid');
              $tempname                 = 'mailtemplates.course_lecture';
              $this->sendmail->getMail($fromMail,$toMail,$subject,$maildata,$tempname);
            }
          }
        }
    }
		echo $newID;
        exit;
    }

	public function getTakeCourse($id, $slug = NULL, $lid = NULL)
    {
        $curriculum                 = $this->model->getcurriculumsection($id);
        $getid                      = \Request::segment(4);
        $geturls                    = ($getid == null ? '' : \SiteHelpers::encryptID($getid,true));
       
        if($geturls != null){
              $lectures             = $this->model->getlecturedetails($geturls);
              
              // course previous and next
              $lectures_all  =  $this->model->getalllecture($id);
             
              if(count($lectures_all)>0){
                for($lec=0;$lec < count($lectures_all);$lec++){
                  if($lectures_all[$lec]->lecture_quiz_id == $geturls){
                    if($lec-1 >= 0)
                      $prev = $lectures_all[$lec-1]->lecture_quiz_id;
                    else
                      $prev = false;

                    if($lec+1 <count($lectures_all))
                      $next = $lectures_all[$lec+1]->lecture_quiz_id;
                    else
                      $next = false;

                    break;
                  }
                }
            }
        }else{
           $lectures            = $this->model->getlecturedetails($id); 
           $next                = false;
           $prev                = false;
        }

        $notes                      = array();
        $comments                   = array();
		$lectureid              = $lectures['0']->lecture_quiz_id;
        if(count($lectures)>0){
            $user_id = $this->model->getcourseuser($id);
            $notes                  = $this->model->getnotes($lectureid);
            $comments               = $this->model->getlecturecomments($lectureid,$user_id->user_id,$id);
        }
            
       
        $this->data['comments']     = $comments;
        $this->data['curriculum']   = $curriculum;
        $this->data['courseid']     = $id;
        $this->data['luserid']     = \Auth::user()->id;
        $this->data['slug']         = $slug;
        $this->data['getdata']      = $lectures;
        $this->data['notes']        = $notes;
        $this->data['next']         = $next;
        $this->data['prev']         = $prev;
        // course completion status
		if($this->model->getCoursecompletedStatus($lectureid))
			$this->data['completion_status'] = true;
		else
			$this->data['completion_status'] = false;

        if($lectures['0']->type == 0){
          if(\Request::ajax()) 
           {  
              return view('course.ajax-comments-learn',$this->data);
           }
			return view('course.takecourse',$this->data);
		} else {
			$this->data['questions'] = $this->model->getquizquestions($lectureid);
			if($this->data['completion_status'])
				$this->data['quizprogress'] = $this->getQuizProgress($lectureid);
			return view('course.takequiz',$this->data);
		}

    }

    public function getTakeCoursePreview($id, $slug = NULL, $lid = NULL)
    {
        $curriculum                 = $this->model->getcurriculumsection($id);
        $getid                      = \Request::segment(4);
        $geturls = ($getid == null ? '' : \SiteHelpers::encryptID($getid,true));
        
        if($geturls != null){
              $lectures             = $this->model->getlecturedetails($geturls);
              
              // course previous and next
              $lectures_all  =  $this->model->getalllecture($id);
             
              if(count($lectures_all)>0){
                for($lec=0;$lec < count($lectures_all);$lec++){
                  if($lectures_all[$lec]->lecture_quiz_id == $geturls){
                    if($lec-1 >= 0)
                      $prev = $lectures_all[$lec-1]->lecture_quiz_id;
                    else
                      $prev = false;

                    if($lec+1 <count($lectures_all))
                      $next = $lectures_all[$lec+1]->lecture_quiz_id;
                    else
                      $next = false;

                    break;
                  }
                }
            }
        }else{
           $lectures            = $this->model->getlecturedetails($id); 
           $next                = false;
           $prev                = false;
        }
       
        $notes                      = array();
        $comments                   = array();
        $lectureid              = $lectures['0']->lecture_quiz_id;
        if(count($lectures)>0){
            $notes                  = $this->model->getnotes($lectureid);
            $comments               = $this->model->getlecturecomments($lectureid);
        }
            
       
        $this->data['comments']     = $comments;
        $this->data['curriculum']   = $curriculum;
        $this->data['courseid']     = $id;
        $this->data['luserid']     = \Auth::user()->id;
        $this->data['slug']         = $slug;
        $this->data['getdata']      = $lectures;
        $this->data['notes']        = $notes;
        $this->data['next']         = $next;
        $this->data['prev']         = $prev;

        $this->data['completion_status'] = false;

        if($lectures['0']->type == 0)
        {
          $this->data['quiz_page'] = false;
        }
        else
        {
          $this->data['quiz_page'] = true;
        }
        return view('course.preview.takecourse',$this->data);
       

    }

    public function postTakenotes(Request $request)
    {
        $lid        = $request->input('lid');
        $sectionid  = $request->input('sectionid');
        $duration   = $request->input('duration');
        $notes      = $request->input('notes');
        if(!empty($lid) && $notes!='' && $duration!=''){
          $results  = $this->model->insertlecturenotes($lid,$sectionid,$duration,$notes);
        }
        echo $results;
    }

    public function postRemovenotes(Request $request)
    {
        $lid        = $request->input('noteid');
        if(!empty($lid)){
          $results  = $this->model->deletenotes($lid);
        }
    }

    public function getDownloadnotes($id)
    {
        $getnotes = $this->model->getnotes($id);
        $filename = "downloadnotes.csv";
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('Time', 'Note'));

        foreach($getnotes as $row) {
            fputcsv($handle, array($row->duration,$row->notes));
        }

        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
        );

        return \Response::download($filename, 'downloadnotes.csv', $headers);
    }

    public function getDownloadresource($name)
    {
        $geturls     = ($name == null ? '' : \SiteHelpers::encryptID($name,true));
        $filename    = \DB::table('course_files')->where('id',$geturls)->get();
        if($filename['0']->aws_url != NULL){
          $file        = $filename['0']->aws_url.$filename['0']->file_name.'.'.$filename['0']->file_extension;
        } else {
          $file        = "./uploads/files/".$filename['0']->file_name.'.'.$filename['0']->file_extension;
        }
        return \Response::download($file);

    }

    public function postInsertlecturecomments(Request $request)
    {
        $lid        = $request->input('lid');
        $comments   = $request->input('comments');
        $title      = $request->input('title');
        $sid        = $request->input('sid');
        $cid        = $request->input('cid');
        $render_file        = $request->input('render_file');
        if(!empty($comments) && !empty($lid) && !empty($title) && !empty($sid) && !empty($cid)){
             $lastinsertid        = $this->model->insertlcomment($lid,$comments,$title,$sid,$cid);
             $result              = array();
             $result['imgpath']   = \SiteHelpers::customavatar(\Session::get('email'),\Session::get('uid'));
             $result['fname']     = \Session::get('fid');
             $result['comments']  = $comments;
             $result['commentid'] = $lastinsertid;
             $result['lid']       = $lid;
             $result['texttitles']= $title;
             $result['userid']    = \Session::get('uid');
             $lastlectureid       = $this->model->lastlectureid($lastinsertid);
             $result['lecture']   = \SiteHelpers::getcheckOrder($cid,$lid);
             $result['stype']     = $lastlectureid->type;
             $datas               = array('status'=>1,'results'=>$result);
        }else{
             $datas      = array('status'=>2,'results'=>'error');
        }
        if(!empty($render_file))
        {
          $this->data['comment'] = $this->model->getlecturecomment($lastinsertid);
          return view('course.right-comments', $this->data);
        }
        else
        {
          echo json_encode($datas);exit;
        }
        
    }

    public function postInsertlecturereply(Request $request)
    {
        $commentid  = $request->input('commentid');
        $lid        = $request->input('lid');
        $comments   = $request->input('comments');
        $render_file        = $request->input('render_file');

        if(!empty($comments) && !empty($lid) && !empty($commentid)){
             $lastinsertid        = $this->model->insertlcommentreply($commentid,$lid,$comments);
             $result              = array();
             $result['imgpath']   = \SiteHelpers::customavatar(\Session::get('email'),\Session::get('uid'));
             $result['fname']     = \Session::get('fid');
             $result['user_link']  = \URL::to('profile/'.\Auth::user()->username);
             $result['comments']  = $comments;
             $result['commentid'] = $commentid;
             $result['replyid']   = $lastinsertid;
             $result['lid']       = $lid;
             $result['userid']    = \Session::get('uid');
             $datas     = array('status'=>1,'results'=>$result);
        }else{
            $datas      = array('status'=>2,'results'=>'error');
        }
        if(!empty($render_file))
        {
          $this->data['reply'] = $this->model->getlecturecommentreply($lastinsertid);
          // echo '<pre>';print_r($this->data);exit;
          return view('course.right-comment-replies', $this->data);
        }
        else
        {
          echo json_encode($datas);exit;
        }
    }

    public function postRemovelecturecomments(Request $request)
    {
        $lid        = $request->input('cid');
        if(!empty($lid)){
          $results  = $this->model->deletelecturecomments($lid);
        }
    }

    public function postRemovelecturereply(Request $request)
    {
        $lid        = $request->input('rid');
        if(!empty($lid)){
          $results  = $this->model->deletelecturereply($lid);
        }
    }

    public function postUpdatelecturecomments(Request $request)
    {
        $lid        = $request->input('lid');
        $comments   = $request->input('comments');
        $title      = $request->input('title');
        $cid        = $request->input('cid');
        if(!empty($comments) && !empty($lid) && !empty($title) && !empty($cid)){
             $lastinsertid        = $this->model->updatelcomment($lid,$comments,$title,$cid);
             $result              = array();
             $result['imgpath']   = \SiteHelpers::customavatar(\Session::get('email'),\Session::get('uid'));
             $result['fname']     = \Session::get('fid');
             $result['comments']  = $comments;
             $result['commentid'] = $lastinsertid;
             $result['lid']       = $lid;
             $result['userid']    = \Session::get('uid');
             $datas     = array('status'=>1,'results'=>$result);

        }else{
             $datas      = array('status'=>2,'results'=>'error');
        }
        echo json_encode($datas);exit;
    }

    
    
    public function postUpdatelecturereply(Request $request)
    {
        $commentid  = $request->input('commentid');
        $lid        = $request->input('lid');
        $comments   = $request->input('comments');
        $rid        = $request->input('rid');

        if(!empty($comments) && !empty($lid) && !empty($commentid) && !empty($rid)){
             $lastinsertid        = $this->model->updatelcommentreply($commentid,$lid,$comments,$rid);
             $result              = array();
             $result['imgpath']   = \SiteHelpers::customavatar(\Session::get('email'),\Session::get('uid'));
             $result['fname']     = \Session::get('fid');
             $result['comments']  = $comments;
             $result['commentid'] = $commentid;
             $result['replyid']   = $lastinsertid;
             $result['lid']       = $lid;
             $result['userid']    = \Session::get('uid');
             $datas     = array('status'=>1,'results'=>$result);
        }else{
            $datas      = array('status'=>2,'results'=>'error');
        }
        echo json_encode($datas);exit;
    }

    public function postUpdatecoursestatus(Request $request)
    {
        $cid        = $request->input('cid');
        $lid        = $request->input('lid');
        $sid        = $request->input('sid');
        if(!empty($cid) && !empty($lid)){
            $this->model->updatelcoursestaus($cid,$lid,$sid);
        }
		
		$clearresult = $request->input('clearresult');
		if(!empty($lid) && !empty($clearresult)){
			return $this->model->clearquizresultretake($lid);
		}
    }

    public function postSubmitreview(Request $request)
    {
        $courseid   = $request->input('courseid');
        
        // $courseid   = 75;
        $course               = $this->model->getcoursedetails($courseid);
        $curriculum           = $this->model->getcoursecurriculum($courseid);
        $getsite              = \bsetecHelpers::get_options('autoApprove');
        $getinstrctor         = $this->model->getinstructorinfo($courseid);
        $getallinstrctor      = $this->model->getinstructorinfo($courseid,1);
        $allinstructor        = $this->model->getinstructors($courseid);
        $getuserid            = $this->model->getcourseuserid($courseid);
        // echo "<pre>";
        // print_r($getsite);exit;
        // print_r($getallusersinfo);
        // print_r($getallinstrctor);
        // print_r($getinstrctor);
        // exit;
        $errormsg = '';
        if($course['0']->course_title == '' || $course['0']->subtitle == '' || $course['0']->cat_id == '' || $course['0']->lang_id == ''){
            $errormsg .= '<li>'.Lang::get('core.course_submit_msg_info').'</li>';
        }

        if($course['0']->image == ''){
            $errormsg .= '<li>'.Lang::get('core.course_submit_msg_stuff').'</li>';
        }
        if($course['0']->course_goal == ''){
            $errormsg .= '<li>'.Lang::get('core.course_submit_msg_goal').'</li>';
        }
        if($course['0']->int_audience == ''){
            $errormsg .= '<li>'.Lang::get('core.course_submit_msg_audience').'</li>';
        }
        if($course['0']->int_audience == ''){
            $errormsg .= '<li>'.Lang::get('core.course_submit_msg_requirement').'</li>';
        }
        if($course['0']->description == '' || str_word_count($course['0']->description) < 50 ){
            $errormsg .= '<li>'.Lang::get('core.course_submit_msg_summery').'</li>';
        }

        if(count($curriculum)==0){
          $errormsg .= '<li>'.Lang::get('core.course_submit_msg_lecture').'</li>';
        }

        if($getallinstrctor!=$getinstrctor && $course['0']->pricing !='0'){
            $errormsg .= '<li>'.Lang::get('core.course_submit_msg_instructor').'</li>';
        }
 
        if($errormsg == ''){
            if(count($getsite)>0 && $getsite['autoApprove'] == '2') {
                $status = '2';
            } else {
                $status = '1';
            }
            $this->model->updatecourse($courseid,$status);
            if(count($getsite)>0 && $getsite['autoApprove'] == '2') {
              $success  = '<p style="color:green">'.Lang::get('core.course_submit_msg_waiting_approval').'<p>';
              $estatus   = 2;

              $toMail                   = CNF_EMAIL;
              // $toMail                   = \Session::get('eid');
              $subject                  = CNF_APPNAME.' '.$course['0']->course_title." Submitted ";      
              $maildata                 = array();
              $maildata['link']      = \URL::to('courseview/'.$course['0']->course_id);
              $maildata['username']     = \Session::get('fid');
              $maildata['title']        = $course['0']->course_title;
              $fromMail                 = \Session::get('eid');
              // $fromMail                 = CNF_EMAIL;
              $tempname                 = 'mailtemplates.course_submitted';
                  
              $this->sendmail->getMail($fromMail,$toMail,$subject,$maildata,$tempname);

            }else{
                $success = '<p style="color:green">'.Lang::get('core.course_submit_msg_published').'<p>';
                $estatus  = 0;
                if(count($allinstructor)>0){
                    foreach ($allinstructor as $key => $ivalue) {
                        $courseinfo = $this->model->getcourseinfo($ivalue->course_id);
                        $fromdetails = \DB::table('users')->where('id', '=', $courseinfo->user_id)->get();
                        $getdetails = \DB::table('users')->where('id', '=', $ivalue->user_id)->where('id', '!=', $courseinfo->user_id)->get();

                         if(count($getdetails)>0){
                            $alink      = \URL::to('courseview/'.$courseinfo->course_id.'/'.$courseinfo->slug);
                            $fromMail   = $fromdetails['0']->email;
                            $toMail     = $getdetails['0']->email;
                            $subject    = CNF_APPNAME." Invite Instructor ";
                            $data       = array('username'=>$getdetails['0']->username,'link'=>$alink,'subject' => "[ " .CNF_APPNAME." ] Invite Instructor "); 
                            $tempname   = 'course.emails.instructor_invite';
                            $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
                            
                         }
                    }
                }

                $getinviterusers  = $this->model->getinviteuserinfo($courseid);
                if(count($getinviterusers)>0){
                    foreach ($getinviterusers as $key => $prvalue) {
                        $alink      = \URL::to('courseview/'.$prvalue->course_id.'/'.$prvalue->slug);
                        $fromMail   = Auth::user()->email;
                        $toMail     = $prvalue->email;
                        $subject    = CNF_APPNAME." Course Invite User";
                        $data       = array('username'=>$prvalue->username,'link'=>$alink,'subject' => "[ " .CNF_APPNAME." ] Course Invite User "); 
                        $tempname   = 'course.emails.inviteuser';
                        $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
                    }
                }

                $couponinfo = Coupon::where('course_id',$courseid)->where('coupon_status','1')->get();
                if(count($getuserid)>0 && count($couponinfo)>0){
                  $courseinfo = $this->model->getcourseinfo($courseid);
                  foreach ($getuserid as $key => $uvalue) {
                      if($couponinfo['0']->coupon_type==1){
                        $amount = $couponinfo['0']->coupon_value.'%';
                      }else{
                        $amount = '$'.$couponinfo['0']->coupon_value;
                      }
                      $sdate      = date('m/d/Y',strtotime($couponinfo['0']->coupon_start_date));
                      $edate      = date('m/d/Y',strtotime($couponinfo['0']->coupon_end_date));
                      $code       = $couponinfo['0']->coupon_code;
                      $alink      = \URL::to('courseview/'.$courseinfo->course_id.'/'.$courseinfo->slug);
                      $fromMail   = Auth::user()->email;
                      $toMail     = $uvalue->email;
                      $subject    = CNF_APPNAME.' '.$courseinfo->course_title." Coupon Info";
                      $data       = array('username'=>$uvalue->username,'link'=>$alink,'title'=>$courseinfo->course_title,'couponcode'=>$code,'amount'=>$amount,'startdate'=>$sdate,'enddate'=>$edate); 
                      $tempname   = 'mailtemplates.course_coupon';
                      $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
                  }
                }
            }
           
            $array      = array('status'=>$estatus,'message'=>$success);
            echo json_encode($array);

        }else{
            $topmessage = '<li class="title" style="color:red">'.Lang::get('core.course_submit_error_msg').'</li>'.$errormsg;
            $array      = array('status'=>'1','message'=>$topmessage);
            echo json_encode($array);
        }

    }

    public function getToread()
    {
        $getid       = \Request::segment(3);
        $geturls     = ($getid == null ? '' : \SiteHelpers::encryptID($getid,true));
        $getfilename = $this->model->getpdffilename($geturls);
        if(count($getfilename)>0){
          if($getfilename['0']->aws_url != NULL){
            $file = $getfilename['0']->aws_url.$getfilename['0']->file_name.'.'.$getfilename['0']->file_extension;
          } else {
            $file = './uploads/files/'.$getfilename['0']->file_name.'.'.$getfilename['0']->file_extension;
          }
			header('Content-type: application/pdf');
			header('Content-Disposition: inline; filename=document.pdf');
			header('Content-Transfer-Encoding: binary');
			header('Accept-Ranges: bytes');
        	@readfile($file);
        }
    }

    public function postSavefeedbackreply(Request $request)
    {
        $commentid  = $request->input('commentid');
        $courseid   = $request->input('courseid');
        $comments   = $request->input('comments');

        if(!empty($comments) && !empty($courseid) && !empty($commentid)){
             $lastinsertid        = $this->model->insertfeedbackreply($commentid,$courseid,$comments);
             $result              = array();
             $result['imgpath']   = \SiteHelpers::customavatar(\Session::get('email'),\Session::get('uid'));
             $result['fname']     = \Session::get('fid');
             $result['user_link'] = \URL::to('profile/'.\Auth::user()->username);
             $result['comments']  = $comments;
             $result['commentid'] = $commentid;
             $result['replyid']   = $lastinsertid;
             $result['userid']    = \Session::get('uid');
             $datas     = array('status'=>1,'results'=>$result);
        }else{
             $datas      = array('status'=>2,'results'=>'error');
        }
        echo json_encode($datas);exit;
    }
    
    /*
    * @task - post image and image crop option
    * @param - imageFile and image details via post
    * @retur - json
    * @process - image croping and add to db
    */
    public function postImage( Request $request ){
        if(!$request->has('imgUrl')){
            $image = Input::file('cImage');
            $what = getimagesize($image);
            if($what[0] < 650 || $what[1] < 400){
                $response = array(
                    'status'=>false,
                    'message'=>'Uploaded image size is '.$what[0].'px * '.$what[1].'px. The Minimum allowed size is 650px * 400px.'
                );
             }else{
                $file_tmp_name = $image->getPathName();
                $file_ext =  $image->getClientOriginalExtension();
                $file_name = rand(4,9999);
                $file_name = $file_name.'.'.$file_ext;
                $file_type = $image->getClientMimeType();
                
                if (!file_exists('./uploads/tmp')) 
                {
                  mkdir('./uploads/tmp', 0755, true);
                }

                $rules = array(
                  'cImage'       => 'required | mimes:jpeg,png,jpg,gif',
                  );
                $validator = Validator::make($request->all(),$rules);

                 if( $validator->fails() ){
                  $response = array(
                          'status'=>false,
                          'message'=>'please upload image file'
                      );
                  }else{
                    if(File::move($file_tmp_name,'./uploads/tmp/'.$file_name)){
    					          chmod('./uploads/tmp/'.$file_name, 0755);
                        $response = array(
                            'status'=>true,
                            'image_path'=> url('uploads/tmp/'.$file_name),
                            'mime_type' => $file_type,
                            'image_name'=> $file_name
                            );
                    }else{
                        $response = array(
                            'status'=>false,
                        );
                    }
                }
            }
        } else {
			
            $r_status = false;
            $image_name = $request->get('image_name');
            $imagec_name = $request->get('imagec_name');
            $imgUrl     = $request->get('imgUrl');
            $imgInitW   = $request->get('imgInitW');
            $imgInitH   = $request->get('imgInitH');
            $imgW       = $request->get('imgW');
            $imgH       = $request->get('imgH');
            $imgY1      = $request->get('imgY1');
            $imgX1      = $request->get('imgX1');
            $cropW      = $request->get('cropW');
            $cropH      = $request->get('cropH');
            $course_id = $request->get("course_id");

            $jpeg_quality = 100;
            $file_name = explode('.',$image_name);
            $filename = $file_name[0];

            $what = getimagesize($imgUrl);
            switch(strtolower($what['mime']))
            {
                case 'image/png':
                    $img_r = imagecreatefrompng($imgUrl);
                    $source_image = imagecreatefrompng($imgUrl);
                    $type = '.png';
                    break;
                case 'image/jpeg':
                    $img_r = imagecreatefromjpeg($imgUrl);
                    $source_image = imagecreatefromjpeg($imgUrl);
                    $type = '.jpg';
                    break;
                case 'image/gif':
                    $img_r = imagecreatefromgif($imgUrl);
                    $source_image = imagecreatefromgif($imgUrl);
                    $type = '.gif';
                    break;
                default: die('image type not supported');
             }
            
            $output_filename = './uploads/course/'.$filename.'_crop';
            $check = CourseImages::where('image_title',$filename)->first();
            if($check != NULL){
              preg_match("'https://(.*?).s3.amazonaws.com'si", $check->aws_url, $match);
              if(isset($match[1])){
                $bucket = $match[1];
                $s3 = new \S3(AWS_KEY, AWS_SECRET);
                if($s3->getObjectInfo($bucket, $check->id.".".$check->image_type, false)){
                  $s3->deleteObject($bucket, $check->id.".".$check->image_type, 'public-read');
                  $s3->deleteObject($bucket, $check->id."_lg.".$check->image_type, 'public-read');
                  $s3->deleteObject($bucket, $check->id."_medium.".$check->image_type, 'public-read');
                  $s3->deleteObject($bucket, $check->id."_small.".$check->image_type, 'public-read');
                }
              }
            }
            
            

            if($check != null){
                $image_p = $check->id.".".$check->image_type;
                if (file_exists('./uploads/course/'.$image_p)) {
                    $r_status = true;
                    File::Delete('./uploads/course/'.$image_p);
                    File::Delete('./uploads/course/'.$check->id."_medium.".$check->image_type);
                    File::Delete('./uploads/course/'.$check->id."_small.".$check->image_type);
                    $image_n = $check->id;
                    $output_filename = './uploads/course/'.$image_n.'_crop';
                }
            }

            $courseImages = new CourseImages;
            $courseImages->image_title = $filename;
            $courseImages->image_type = substr($type, 1);
            $courseImages->image_tag = "dummy tag";
            $courseImages->uploader_id = \Session::get('uid');
            $courseImages->created_at = time();
            $courseImages->updated_at = time();

            if(!$r_status){
                if($courseImages->save()){
                    $image_n = $courseImages->id;
                    $courseUpdate = CourseImages::find($courseImages->id);
                    $courseUpdate->image_hash = md5($courseImages->id.$courseUpdate->created_at);
                    $courseUpdate->save();
                    $output_filename = './uploads/course/'.$image_n.'_crop';
                }
            }
            

            $resizedImage = imagecreatetruecolor($imgW, $imgH);
            imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);   
            $dest_image = imagecreatetruecolor($cropW, $cropH);
            imagecopyresampled($dest_image, $resizedImage, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);    
            imagejpeg($dest_image, $output_filename.$type, $jpeg_quality);
           
            // resize image width and height
            $image_size[] = array('imgW'=>'650','imgH'=>'400','value'=>'normal');
            $image_size[] = array('imgW'=>'480','imgH'=>'300','value'=>'medium');
            $image_size[] = array('imgW'=>'200','imgH'=>'150','value'=>'small');

            if(file_exists('./uploads/course/'.$image_n.'_crop'.$type)){
                $path = './uploads/course/'.$image_n.'_crop'.$type;
                foreach($image_size as $size){
                    $save_path = ($size['value'] =='normal') ? $image_n.$type : $image_n.'_'.$size['value'].$type; 
                    //Image::make($path)->resize($size['imgW'],$size['imgH'])->save('./uploads/course/'.$save_path);
                    $img =Image::make($path);
                    $img->fit($size['imgW'],$size['imgH']);
                    $img->save('./uploads/course/'.$save_path);
                }
                if (file_exists($path))
                    File::Delete($path);
               rename('./uploads/tmp/'.$image_name,'./uploads/course/'.$image_n.'_lg'.$type);
            }
         
            $course = Course::find($course_id);
            $course->image = $image_n;
            if($course->save()){
                $response = array(
                    "status" => 'success',
                    "url" => url('course/image/'.$courseUpdate->image_hash),
                    "image_name" =>$image_n.$type,
                    "image_id"=>$image_n,
                ); 
            }
        
		}
        print json_encode($response);
    }

    /*
    * task - delete upload images
    * @parm - image_file ( input file tag ) and also delete in db
    * @plugin - fileupload ajax
    */

    public function deleteImage( Request $request ){
        $image_file = explode('.', $request->get("image_file"));

        $getImage = CourseImages::where('image_title','=',$image_file[0])
                        ->orWhere('id','=',$image_file[0])->first();

        if(file_exists('./uploads/tmp/'.$request->get("image_file"))){
            $path = './uploads/tmp/'.$request->get("image_file");
        }else{
            $path = './uploads/course/'.$getImage->id.".".$getImage->image_type;
        }

        if(!file_exists($path)){
            $course = Course::find($request->get("course_id"));
            $course->image = 0;
            $course->save();
            $getImage->delete();

            if($getImage->aws_url != NULL){
              preg_match("'https://(.*?).s3.amazonaws.com'si", $getImage->aws_url, $match);
              if(isset($match[1])){
                $bucket = $match[1];
                $s3 = new \S3(AWS_KEY, AWS_SECRET);
                if($s3->getObjectInfo($bucket, $getImage->id.".".$getImage->image_type, false)){
                  $s3->deleteObject($bucket, $getImage->id.".".$getImage->image_type, 'public-read');
                  $s3->deleteObject($bucket, $getImage->id."_lg.".$getImage->image_type, 'public-read');
                  $s3->deleteObject($bucket, $getImage->id."_medium.".$getImage->image_type, 'public-read');
                  $s3->deleteObject($bucket, $getImage->id."_small.".$getImage->image_type, 'public-read');
                }
              }
            }
            echo "success";
            exit();
        }

        if(File::Delete($path)){
            if($getImage != NULL){
                File::Delete('./uploads/course/'.$getImage->id."_lg.".$getImage->image_type);
                File::Delete('./uploads/course/'.$getImage->id."_medium.".$getImage->image_type);
                File::Delete('./uploads/course/'.$getImage->id."_small.".$getImage->image_type);
                $course = Course::find($request->get("course_id"));
                $course->image = 0;
                $course->save();
                $getImage->delete();
                if($getImage->aws_url != NULL){
                  preg_match("'https://(.*?).s3.amazonaws.com'si", $getImage->aws_url, $match);
                  if(isset($match[1])){
                    $bucket = $match[1];
                    $s3 = new \S3(AWS_KEY, AWS_SECRET);
                    if($s3->getObjectInfo($bucket, $getImage->id.".".$getImage->image_type, false)){
                      $s3->deleteObject($bucket, $getImage->id.".".$getImage->image_type, 'public-read');
                      $s3->deleteObject($bucket, $getImage->id."_lg.".$getImage->image_type, 'public-read');
                      $s3->deleteObject($bucket, $getImage->id."_medium.".$getImage->image_type, 'public-read');
                      $s3->deleteObject($bucket, $getImage->id."_small.".$getImage->image_type, 'public-read');
                    }
                  }
                }
            }
            echo "success";
        }else{
            echo "fail";
        }
        exit();
    }
     public function CourseSubscribe($id,$slug){
        if(!\Auth::check()){
            \Session::put('user_redirect','subscribe-course/'.$id.'/'.$slug);
            return Redirect::to('user/login');  
        }
                
        $course = Course::find($id);

        if($course->user_id == Auth::user()->id){
            return Redirect::to('courseview/'.$id.'/'.$slug);
        }else{
            if(!\bsetecHelpers::checkPurchase($course->course_id)){
                if($course->pricing > 1){
                  if(!\bsetecHelpers::checkmembership(Auth::user()->id,$course->course_id)){
                    return Redirect::to('courseview/'.$id.'/'.$slug);
                }
              }
                $purchase = new Transaction();
                $purchase->user_id = Auth::user()->id;
                $purchase->course_id = $id;
                $purchase->amount = 0;
                $purchase->status = 'completed';
                $purchase->payment_method = "free";

                // add for taken course by user
                $courseTaken = new CourseTaken;
                $courseTaken->user_id = Auth::user()->id;
                $courseTaken->course_id = $id;
                $courseTaken->save();

                $frominfo = User::find($course->user_id);
                if($course->privacy>2){
                    $psw = explode('--pwd--',$course->privacy);
                    if(count($psw)>0 && !empty($psw['1'])){
                        $pass       = $psw['1'];
                        $alink      = \URL::to('courseview/'.$course->course_id.'/'.$course->slug);
                        $fromMail   = $frominfo->email;
                        $toMail     = Auth::user()->email;
                        $subject    = CNF_APPNAME." Course Password ";
                        $data       = array('username'=>Auth::user()->username,'link'=>$alink,'subject' => "[ " .CNF_APPNAME." ] Course Password ",'password'=>$pass); 
                        $tempname   = 'course.emails.password';
                        $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
                                                      
                    }
                }
                
                $check = $this->notification_settings->where('user_id', $course->user_id)->where('notif_course_subscribed', 1)->where('notif_for_all', 0)->first();
                if(count($check)>0)
                {
                    //to instructor mail for join
                    $date       = date("Y-m-d H:i:s");
                    $fromMail   = Auth::user()->email;
                    $toMail     = $frominfo->email;
                    $subject    = CNF_APPNAME." Student Joined ";
                    $alink      = \URL::to('courseview/'.$course->course_id.'/'.$course->slug);
                    $data       = array('authorname'=>$frominfo->username,'link'=>$alink,'studentname'=>Auth::user()->username,'title'=>$course->course_title,'joindate'=>$date); 
                    $tempname   = 'mailtemplates.course_student_join';
                    $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
                }
                if($purchase->save())
                return view('course/success')->with('course', $course)->with('transId', 0)->with('status', 'success')->with('title', 'Course');
            }else{
                return Redirect::to('courseview/'.$id.'/'.$slug.'?return=success');
            }
        }
    }
    public function getPreview($id, $slug='')
    {
       if(!\Auth::check()) return Redirect::to('user/login');
       $loggedid                   = \Session::get('uid');
       $takencourse                = '';
       if(!empty($loggedid)){
            $takencourse           = $this->model->getcoursetaken($id,$loggedid);
       }
       $courseinfo                 = $this->model->getcourseinfo($id); 
       if(count($takencourse)==0){
            return Redirect::to('course');        
       }
       $curriculum                 = $this->model->getcurriculumsection($id);
       $comments                   = $this->model->getalllecturecomments($id);
       $this->data['curriculum']   = $curriculum;
       $this->data['courseid']     = $id;
       if($slug=='' && count($courseinfo)>0){
            $this->data['slug']    = $courseinfo->slug;
       }else{
            $this->data['slug']    = $slug;
       }
       
       $this->data['courseinfo']   = $courseinfo;
       $this->data['comments']     = $comments;
       return view('course.preview',$this->data);
    }

    public function postRatingform(Request $request)
    {
        $cid                        = $request->input('cid');
        $checkrate                  = $this->model->checkrating($cid);
        $this->data['courseid']     = $cid;
        $this->data['rateinfo']     = $checkrate;
        return view('course.rating',$this->data);
    }

    public function postReviewrating(Request $request)
    {
        // $title          = $request->input('title');
        $description    = $request->input('description');
        $cid            = $request->input('cid');
        $rate           = $request->input('rate');
        $rateid         = $request->input('rateid');
        
        if(!empty($rate) && !empty($cid)){
            $insertid = $this->model->insertratecomments($description,$cid,$rate,$rateid);
            if($rateid=='0'){
              $courseinfo               = $this->model->getcourseinfo($cid);
              $authorinfo               = $this->model->getuserinfos($courseinfo->user_id);
              $toMail                   = $authorinfo['0']->email;
              $subject                  = CNF_APPNAME." Course Rating By ".\Session::get('fid');      
              $maildata                 = array();
              $maildata['authorname']   = $authorinfo['0']->username;
              $maildata['title']        = $courseinfo->course_title;
			  $maildata['coursename']        = $courseinfo->course_title;
              $maildata['link']         = \URL::to('courseview/'.$courseinfo->course_id);
              $maildata['studentname']  = \Session::get('fid');
              $maildata['rating']       = $rate;
              $maildata['description']  = $description;
              $fromMail                 = \Session::get('eid');
              $tempname                 = 'mailtemplates.course_rating';
              $this->sendmail->getMail($fromMail,$toMail,$subject,$maildata,$tempname);
            }
            $results  = array('status'=>'1','id'=>$insertid);
        }else{
            $results  = array('status'=>'2');
        }
        echo json_encode($results);
    }

    public function postDeletereview(Request $request)
    {
         $did        = $request->input('did');
        if(!empty($did)){
          $results  = $this->model->deletereview($did);
          echo "1";
        }else{
            echo "2";
        }
    }

    public function postAdddiscussions(Request $request)
    {
        $cid                        = $request->input('cid');
        $curriculum                 = $this->model->getcurriculumsection($cid);
        $this->data['courseid']     = $cid;
        $this->data['curriculum']   = $curriculum;
        return view('course.adddiscussions',$this->data);
    }

    public function postInsertannouncement(Request $request)
    { 
        $comments   = $request->input('comments');
        $cid        = $request->input('cid');
        if(!empty($comments) && !empty($cid)){
             $lastinsertid        = $this->model->insertannouncementcomments($comments,$cid);
             $result              = array();
             $result['imgpath']   = \SiteHelpers::customavatar(\Session::get('email'),\Session::get('uid'));
             $result['fname']     = \Session::get('fid');
             $result['comments']  = $comments;
             $result['commentid'] = $lastinsertid;
             $result['userid']    = \Session::get('uid');

            $courseinfo               = $this->model->getcourseinfo($cid);
            $getstudents              = $this->model->getallsubscribers($cid);
            $push_notification =array();
            if(count($getstudents)>0){
               foreach ($getstudents as $key => $svalue) {
                $check = $this->notification_settings->where('user_id', $svalue->user_id)->where('notif_announcement', 1)->where('notif_for_all', 0)->first();
                if(count($check)>0)
                {
                  $toMail                   = $svalue->email;
                  $subject                  = CNF_APPNAME." Course Announcement ";      
                  $maildata                 = array();
                  $maildata['authorname']   = \Session::get('fid');
                  $maildata['title']        = $courseinfo->course_title;
                  $maildata['link']         = \URL::to('courseview/'.$courseinfo->course_id);
                  $maildata['username']     = $svalue->username;
                  $maildata['announcement'] = $comments;
                  $fromMail                 = \Session::get('eid');
                  $tempname                 = 'mailtemplates.course_announcement';
                  $this->sendmail->getMail($fromMail,$toMail,$subject,$maildata,$tempname);
                  if(!empty($check)){
                    //array_push($push_notification, $svalue->user_id);
                    $data = array();
                    $data['type']= 'announcement';
                    $data['type_id'] = $lastinsertid;
                    $data['users'] = $svalue->user_id;
                    $data['status'] = 0;
                      \DB::table('push_notification')->insertGetId($data);
                  }
                  
                }
              }
            }

            $datas     = array('status'=>1,'results'=>$result);

        }else{
             $datas      = array('status'=>2,'results'=>'error');
        }
        echo json_encode($datas);exit;
    }

    public function postUpdateannoncement(Request $request)
    {
        $commentid  = $request->input('commentid');
        $comments   = $request->input('comments');
        $cid        = $request->input('courseid');
        if(!empty($comments) && !empty($commentid) && !empty($cid)){
             $lastinsertid        = $this->model->updateannouncementcomments($commentid,$comments,$cid);
             $result              = array();
             $result['imgpath']   = \SiteHelpers::customavatar(\Session::get('email'),\Session::get('uid'));
             $result['fname']     = \Session::get('fid');
             $result['comments']  = $comments;
             $result['commentid'] = $commentid;
             $result['userid']    = \Session::get('uid');
             $datas     = array('status'=>1,'results'=>$result);

        }else{
             $datas      = array('status'=>2,'results'=>'error');
        }
        echo json_encode($datas);exit;
    }

    public function postRemoveannoucement(Request $request)
    {
        $lid        = $request->input('rid');
        if(!empty($lid)){
          $results  = $this->model->deleteannoucement($lid);
        }
    }
    
    public function postReviews(Request $request)
    {

        $next        = $request->input('next');
        $cid         = $request->input('cid');
        $reviews     = $this->model->getreviewsall($next,$cid);
        $endkey      = end($reviews);
        $nexts       = array();
        if(!empty($endkey->ratings_id)){
            $nexts       = $this->model->getreviewsall($endkey->ratings_id,$cid);
        }
      

        $this->data['reviews']      = $reviews;
        $this->data['courseid']     = $cid;
        $this->data['nextrec']      = $nexts;

        return view('course.listreviews',$this->data);
    }

    public function postStudents(Request $request)
    {
        $next        = $request->input('next');
        $cid         = $request->input('cid');
        $students    = $this->model->getallstudents($next,$cid);
        $endkey      = end($students);
        $nexts       = array();
        if(!empty($endkey->taken_id)){
            $nexts       = $this->model->getallstudents($endkey->taken_id,$cid);
        }
        $this->data['students']   = $students;
        $this->data['courseid']   = $cid;
        $this->data['nextrec']    = $nexts;
        return view('course.studentlist',$this->data);

    }

    public function postWishlist(Request $request)
    {
       $cid        = $request->input('cid');   
       $type       = $request->input('type'); 
       if (\Auth::check()){
            if(!empty($cid) && $type!=''){
                $this->model->addwishlist($cid,$type);
                $datas = array('status'=>'0');
            }else{
                $datas = array('status'=>'2');
            }
       }else{
                $datas = array('status'=>'1');
       }

       echo json_encode($datas);exit;
       
    }

    public function postUpdateinsstatus(Request $request)
    {
        $mid        = $request->input('mid');
        $cid        = $request->input('cid');
        if (\Auth::check()){
            if(!empty($mid) && !empty($cid)){
                $this->model->updateinstrouctorstatus($cid,$mid);
                $course = Course::find($cid);
                if($course->privacy>2){
                    $psw = explode('--pwd--',$course->privacy);
                    $frominfo = User::find($course->user_id);
                    if(count($psw)>0 && !empty($psw['1'])){
                        
                        $pass       = $psw['1'];
                        $alink      = \URL::to('courseview/'.$course->course_id.'/'.$course->slug);
                        $fromMail   = $frominfo->email;
                        $toMail     = Auth::user()->email;
                        $subject    = CNF_APPNAME." Course Password ";
                        $data       = array('username'=>Auth::user()->username,'link'=>$alink,'subject' => "[ " .CNF_APPNAME." ] Course Password ",'password'=>$pass); 
                        $tempname   = 'course.emails.password';
                        $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
                    }
                    $insname    = \Session::get('fid');
                    $toMail     = $frominfo->email;
                    $alink      = \URL::to('courseview/'.$course->course_id.'/'.$course->slug);
                    $fromMail   = Auth::user()->email;
                    $subject    = CNF_APPNAME." Instructor Join ";
                    $data       = array('authorname'=>$frominfo->username,'title'=>$course->course_title,'instructorname'=>$insname,'link'=>$alink); 
                    $tempname   = 'mailtemplates.course_joined';
                    $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);

                }

                $datas = array('status'=>'0');
            }else{
                $datas = array('status'=>'2');
            }
        }else{
                $datas = array('status'=>'1');
       }
       echo json_encode($datas);exit;
    }
	
	public function postCoursenotification(Request $request){
		$data['course_id'] = $request->input('course_id');
		$data['user_id'] = \Auth::user()->id;
		if($request->input('type') == '1'){
			$data['new_lecture_emails'] = $request->input('status');
		} else if($request->input('type') == '2'){
			$data['new_announcement_emails'] = $request->input('status');
		} else if($request->input('type') == '3'){
			$data['promotional_emails'] = $request->input('status');
		}
		
		if($request->input('id') == 0){
			$newID = $this->model->insertCourseNotificationInfo($data , '');
		} else {
			$newID = $this->model->insertCourseNotificationInfo($data , $request->input('id'));
		}
		echo $newID;
	}
	
    public function postUnenroll(Request $request){
		$this->model->deleteCourseTaken($request->input('course_id'),\Auth::user()->id);
		echo \URL::to('course');
	}
	
	public function postCourseSubscriberlist($id, Request $request)
	{
		if($id!=''){
			$skip = $request->input('skip');
			if(empty($skip) && $skip != '0'){
				$skip = 0;
				$this->data['courseinfo'] = $this->model->getcourseinfo($id);
				$this->data['subscribers'] = $this->model->getCourseSubscribers($id,$request->input('search'),$skip);
				$this->data['skip'] = $skip+count($this->data['subscribers']);
				return view('course.coursesubscribers',$this->data);
			} else {
				$search = $request->input('search');
				$subscribers = $this->model->getCourseSubscribers($id,$request->input('search'),$skip);
				if(!empty($subscribers)){
					$result = '';
					foreach($subscribers as $subscriber){
						$result .= '<tr class="student-list-item" data-userid="'.$subscriber->id.'"> <td style="width: 50px;"> <a href="'.\URL::to("profile/".$subscriber->username).'" target="_blank" style="display: inline-block;">'. \SiteHelpers::customavatar($subscriber->email,$subscriber->id).'</a> </a> </td> <td style="width: 150px;"> <div class="ellipsis w150" style="text-align: left; display: inline-block;"> <a href="'.\URL::to("profile/".$subscriber->username).'" target="_blank">'.$subscriber->first_name.' '.$subscriber->last_name.'</a> </div> </td><td></td><td></td> </tr>';
					}
					$skip = $skip+count($subscribers);
					$datas = array('status'=>1,'result'=>$result,'skip'=>$skip);
				} else {
					$datas = array('status'=>2,'skip'=>$skip);
				}
				echo json_encode($datas);exit;
			}
		}
	}
    /*
    * @process - coupon add
    * @param - input request for using post method 
    * @return - ajax with return json array response 
    */
    public function postCoupon( Request $request ){

        $carbon = Carbon::today();
        $input = array(
            'coupon_type'   => $request->get('coupon_type'),
            'coupon_value'  => $request->get('coupon_value'),
            'coupon_start_date' => $request->get('coupon_start_date'),
            'coupon_end_date'   => $request->get('coupon_end_date'),

        );
        $rules = array(
            'coupon_type'       => 'required',
            'coupon_value'      => 'required|numeric|min:1',
            'coupon_start_date' => 'before:coupon_end_date',
            'coupon_end_date'   => 'after:coupon_start_date|after_or_equal:'.$carbon,
        );

        $messages = array(
            'coupon_value.min' =>'Coupon value must be Numeric.',
            'coupon_start_date.before' =>'Coupon start date must be less than end date.',
            'coupon_end_date.after' =>'Coupon end date must be greater than start date.',
        );

        //amount maximum
        if($request->get('coupon_type') == '1'){
          $rules['coupon_value'] = 'required|numeric|max:100';
          $messages['coupon_value.max'] = 'Coupon value maximum percentage limit 100%.';
        }else{
            $course = Course::find($request->get('course_id'));
            $rules['coupon_value'] = 'required|numeric|max:'.$course->pricing;
            $messages['coupon_value.max'] = 'Coupon value maximum price limit $'.$course->pricing;
        }

        $validator = Validator::make($input, $rules, $messages);

        if($validator->fails())
        {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 400); // 400 being the HTTP code for an invalid request.
        }else{
            if($request->get('coupon_id') != null){
                $coupon = Coupon::find($request->get('coupon_id'));
                $coupon->updated_at = time();
            }else{
                $coupon = new Coupon;
                $coupon->created_at = time();
            }
             
             $coupon->user_id = Auth::user()->id;
             $coupon->course_id = $request->get('course_id');
             $coupon->coupon_desc = $request->get('coupon_desc');
             $coupon->coupon_type = $request->get('coupon_type');
             $coupon->coupon_value = $request->get('coupon_value');
             $coupon->coupon_start_date = $request->get('coupon_start_date');
             $coupon->coupon_end_date = $request->get('coupon_end_date');
             $coupon->coupon_status = $request->get('coupon_status');

             $courseid   = $request->get('course_id');
             $ctype      = $request->get('coupon_type');
             $cvalue     = $request->get('coupon_value');
             $csdate     = $request->get('coupon_start_date');
             $cedate     = $request->get('coupon_end_date');
             $cstatus    = $request->get('coupon_status');
             if($coupon->save()){

                if($request->get('coupon_id') == null){
                  $coupon_code = $carbon->month.$carbon->year.strtoupper(Str::random(4)).$coupon->id;
                  $coupon_update = Coupon::find($coupon->id);

                  $coupon_update->coupon_code = $coupon_code;

                  $coupon_update->save();


                   $courseinfo           = $this->model->getcourseinfo($courseid);
                   $getuserid            = $this->model->getcourseuserid($courseid);

                   if($courseinfo->approved=='1'){
                     if(count($getuserid)>0 && $request->get('coupon_id')==null && $cstatus=='1'){
                        foreach ($getuserid as $key => $uvalue) {
                            if($ctype==1){
                              $amount = $cvalue.'%';
                            }else{
                              $amount = '$'.$cvalue;
                            }

                            $sdate      = ($csdate!=null) ? $csdate : date('Y-m-d'); 
                            $edate      = ($cedate!=null) ? $cedate : "Unlimited";
                            $code       = $coupon_code;
                            $alink      = \URL::to('courseview/'.$courseinfo->course_id.'/'.$courseinfo->slug);
                            $fromMail   = Auth::user()->email;
                            $toMail     = $uvalue->email;
                            $subject    = CNF_APPNAME.' '.$courseinfo->course_title." Coupon Info";
                            $data       = array('username'=>$uvalue->username,'link'=>$alink,'title'=>$courseinfo->course_title,'couponcode'=>$code,'amount'=>$amount,'startdate'=>$sdate,'enddate'=>$edate); 
                            $tempname   = 'mailtemplates.course_coupon';
                            $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
                        }
                      }
                    }
               }

                return response()->json(array(
                    'success' => true,
                    'message' => "success",
                ),200);

            }
        }
    }

    public function getCoupon( Request $request ){
        if($request->get('coupon_id')){
            $coupon = Coupon::find($request->get('coupon_id'));
            return response()->json($coupon);
        }
    }

    public function deleteCoupon( Request $request ){
        if(count($request->get('coupon_id')) >= 1)
        {
         if(Coupon::destroy($request->input('coupon_id')))
            return response()->json(array(
                    'success' => true,
                ),200);
        }
    }
    
    public function postCouponcheck( Request $request ){
        if($request->get('code') != NULL){
            $coupon = Coupon::where('coupon_code',$request->get('code'))
                        ->where('course_id',$request->get('course_id'))
                        ->where('coupon_status',1)
                        ->first();                      
            if($coupon == NULL){
                    return Response::json(array(
                        'errors' => 'Sorry, the coupon code "<b>'.$request->get('code').'</b>" is invalid.'
                    ), 400);
            }else{
                if($coupon->coupon_start_date !='0000-00-00'){
                  $coupon = Coupon::where('coupon_code',$request->get('code'))
                            ->where('course_id',$request->get('course_id'))
                            ->where('coupon_start_date','<=',Carbon::now()->toDateString())
                            ->where('coupon_end_date','>=',Carbon::now()->toDateString())
                            ->where('coupon_status',1)
                            ->first();
                    if($coupon == NULL){  
                        return Response::json(array(
                                'errors' => 'Sorry, the coupon code "<b>'.$request->get('code').'</b>" is date invalid.'
                        ), 400);
                   }
                }

                $course = Course::find($request->get('course_id'));
                $course_price = $course->pricing;

                if(\Session::has('admin_discount')){
                    $course_price = \Session::get('admin_discount');
                }

                $discount_amount = $coupon->coupon_value;

                if($coupon->coupon_type == 1){
                    $discount_amount =  $course_price * ($discount_amount / 100);
                    $amount = $course_price - $discount_amount;
                }else{
                    $amount = $course_price - $discount_amount;
                }

                if($amount < 0)
                       $amount = 0;
                
                $return_msg = array(
                        'discount_amount'=>$discount_amount,
                        'amount'=>$amount,
                        'success_message'=>'Success!, the coupon code "<b>'.$request->get('code').'</b>" is valid amount.'
                    );

                if(\Session::has('coupon_price'))
                    \Session::forget('coupon_price',$amount);

                
                    \Session::put('coupon_price',$amount);
                    \Session::save();
                }

              return response()->json($return_msg,200);
            }
    }
    
    public function postCheckcoursepassword(Request $request)
    {
        $cid        = $request->get('courseid');
        $password   = $request->get('password');
        if(!empty($cid) && !empty($password)){
            $courseinfo = Course::find($cid);  
            if($courseinfo->privacy>2){
                $psw = explode('--pwd--',$courseinfo->privacy);
                if(count($psw)>0 && !empty($psw['1']) && $psw['1']==$password){
                    \Session::put('cplogin-'.$cid, 1);
                    $datas = array('status'=>0);                                                             
                }else{
                    $datas = array('status'=>1);
                }
            }else{
               $datas = array('status'=>2);
            }
        }else{
               $datas = array('status'=>2);
        }
        echo json_encode($datas);
    }

    public function postUnpublish(Request $request)
    {
        $cid        = $request->get('courseid');
        if(!empty($cid)){
            $this->model->courseunpublish($cid);
            $datas = array('status'=>0);  
        }else{
            $datas = array('status'=>1);  
        }
        echo json_encode($datas);
    }

    public function getAllusers(Request $request)
    {
        $courseid   = \Request::segment(3);
        $searchtext = $request->input('term');
        $allusers = $this->model->getallusersdata($searchtext);
        $dataarray      = array();
        if(count($allusers)>0){
            for ($i=0; $i < count($allusers); $i++) { 
                $dataarray[$i]['id']    = $allusers[$i]->id;         
                $dataarray[$i]['value'] = $allusers[$i]->email;         
            }
        }

        echo json_encode($dataarray);exit;
    }

    public function postAddusers(Request $request)
    {
        $courseid   = $request->input('courseid');
        $emailid    = $request->input('emails');
        
        $checkalready = $this->model->checkinviteuserexists($courseid,$emailid);
        if(count($checkalready)=='0'){
            $allusers   = $this->model->insertinviterusers($courseid,$emailid);
            if(!empty($allusers['0']->first_name) && !empty($allusers['0']->last_name)){
                $userinfo   = array('user_id'=>$allusers['0']->id,'username'=>$allusers['0']->first_name.' '.$allusers['0']->last_name);
                $datas      = array('status'=>1,'results'=>$userinfo);
            }elseif (!empty($allusers['0']->first_name)) {
                $userinfo   = array('user_id'=>$allusers['0']->id,'username'=>$allusers['0']->first_name);
                $datas      = array('status'=>1,'results'=>$userinfo);
            }else{
              $datas      = array('status'=>3,'results'=>'Please enter valid email');  
            }
            
        }else{
            $datas      = array('status'=>2,'results'=>'already exists');
        }
        echo json_encode($datas);exit;
    }

    public function postDeleteinviteusers(Request $request)
    {
        $courseid   = $request->input('courseid');
        $userid     = $request->input('userid');
        $this->model->getdeleteinviteusers($courseid,$userid);
        echo "success";
    }

    public function postSearchdiscussion(Request $request)
    {
        $courseid   = $request->input('courseid');
        $stext      = $request->input('stext');
        if(!empty($courseid) && !empty($stext)){
            $results = $this->model->getsearchresults($courseid,$stext);
        }elseif(!empty($courseid)){
            $results = $this->model->getsearchresults($courseid,$stext,1);
        }
        $this->data['comments'] = $results;
        return view('course.search_discussion',$this->data);
    }
    
    /*
    * @process - course cerificate
    * @method - get
    * @param - course_id and slug as optional
    * @return - certificate as pdf version
    */

    public function getCoursecertificate($course_id, $slug = NULL){
       ini_set('max_execution_time', 0);
       if(!\Auth::check()) return Redirect::to('user/login');
    
       if(!$this->checkCompletion($course_id)){
            return Redirect::to('courseview/' . $course_id . '/' . $slug);
       }

       $course = Course::find($course_id);
       $this->data['course_title'] = $course->course_title;
       $this->data['name'] = \Session::get('fid');
       $this->data['description'] = $course->description;
      // return view('course.certificate',$this->data);
       return \PDF::loadView('course/certificate', $this->data)
            ->setPaper('a4')
            // ->setOrientation('landscape')
            ->setWarnings(false)
            ->stream('completion.pdf');
    }

    // check course completion
    public function checkCompletion($id){
       $loggedid = \Session::get('uid');
       $takencourse  = $this->model->getcoursetaken($id,$loggedid);
        if(count($takencourse) < 1){
            return false;
        }else{
           $courselecturetotal= $this->model->getCourseLectureTotal($id);
           $courselecturecompleted = $this->model->getCourseCompletedCount($id,$loggedid);
           if($courselecturetotal != $courselecturecompleted){
             return false;
           }
        }
        return true;
    } 
    
    
    public function postQuizsubmit(Request $request) {
		$coption = $this->model->getQuizAnswer($request->input('qid'));
		
		$status = 0;
		if($coption->correct_option == $request->input('uoption')){
			$status = 1;
		}
		
		$data['quiz_id'] = $request->input('lid');
		$data['quiz_question_id'] = $request->input('qid');
		$data['user_id'] = \Auth::user()->id;
		$data['user_option'] = $request->input('uoption');
		$data['correct_option'] = $coption->correct_option;
		$data['status'] = $status;
		$data['complete'] = '1';
		$now_date = date("Y-m-d H:i:s");
		$data['createdOn'] = $now_date;
		$data['updatedOn'] = $now_date;
		
		if($request->input('rid') == 0){
			$newID = $this->model->insertQuizResult($data , '');
		} else {
			$newID = $this->model->insertQuizResult($data , $request->input('rid'));
		}
		
		$last = $request->input('last');
		if($last == '1'){
			$this->model->updatelcoursestaus($request->input('cid'),$request->input('lid'),$last);
			$return_data = $this->getQuizProgress($request->input('lid'));
		}
		
		$return_data['status'] = $status;
		$return_data['rid'] = $newID;
		$return_data['coption'] = $coption->correct_option;
		$return_data['options'] = $coption->options;
		$return_data['type'] = $coption->question_type;
		
		echo json_encode($return_data);
        exit;
	}
	
	public function getQuizProgress($lid){
		$userid = \Auth::user()->id;
		$quizquestions = $this->model->getquizquestions($lid);
		$quizrecords = $this->model->getquizrecords($lid);
		
		$correctcount = 0;
		$overallcorrectcount = 0;
		$answeredcount = 0;
		$usersarray = array();
		$quizresult = array();
		$quizusersresult = array();	
		
		foreach($quizquestions as $key => $question){
			$questionno = $key+1;
			$questioncorrectcount = 0;
			$questionincorrectcount = 0;
			$quizresult[$question->sort_order] = 2;
			
			foreach($quizrecords as $record){
				
				// For users count
				if(!in_array($record->user_id, $usersarray, true)){
					array_push($usersarray, $record->user_id);
				}
				// For users result, correct/incorrect count, overall correct/incorrect count & users each quiz correct/incorrect count
				if($record->quiz_question_id == $question->quiz_question_id){
					if($record->user_id == $userid){
						$quizresult[$question->sort_order] = $record->status;
						if($record->status != '2'){
							$answeredcount++;
						}
						if($record->status == '1'){
							$correctcount++;
						}
					}
					if($record->status == '1'){
						$questioncorrectcount++;
					} else if($record->status == '0'){
						$questionincorrectcount++;
					}
				}
			}
			$quizusersresult[$question->sort_order] = array('correct'=>$questioncorrectcount,'incorrect'=>$questionincorrectcount);
		}
		foreach($quizrecords as $record){
			if($record->status == '1'){
				$overallcorrectcount++;
			}
		}
		
		$quizcount = count($quizquestions);
		$userscount = count($usersarray);
		$incorrectcount = $answeredcount-$correctcount;
		$skipcount = $quizcount-$answeredcount;
		$score = round( (100 * $correctcount) / $quizcount );
		
		$quizrecordcount = count($quizrecords);
		$overallscore = round( (100 * $overallcorrectcount) / $quizrecordcount );
		
		$data['score'] = $score;
		$data['correctcount'] = $correctcount;
		$data['incorrectcount'] = $incorrectcount;
		$data['skipcount'] = $skipcount;
		$data['overallscore'] = $overallscore;
		$data['userscount'] = $userscount;
		$data['quizresult'] = $quizresult;
		$data['quizusersresult'] = $quizusersresult;
		
		return $data;
	}

  public function getImage($image_hash = '', $size = '')
  {
    if($image_hash == 'default')
    {
      header('content-type: jpg');
      readfile('uploads/course/default.jpg');
    }
    else
    {
      $image = \DB::table('course_images')->where('image_hash', '=', $image_hash)->first();
    
      if($image->aws_url != NULL){
        header('content-type: jpg ');
        readfile($image->aws_url.$image->id.$size.'.'.$image->image_type);
      } else {
        $path = './uploads/course/'.$image->id.$size.'.'.$image->image_type;
        if(file_exists($path)){
          header('content-type: '.$image->image_type);
          readfile('uploads/course/'.$image->id.$size.'.'.$image->image_type);
        }else{
          header('content-type: jpg ');
          readfile('uploads/course/default.jpg');
        }
      }
     
    }
  }
  /*
  *@process -- get video image by id
  */
  public function getVideoimage($video_id = '')
  {
    if($video_id == '')
    {
      header('content-type: png');
      readfile('uploads/images/no-image.png');
    }
    else
    {
      $image = \DB::table('course_videos')->where('id', '=', $video_id)->first();
      $path = './uploads/images/'.$image->image_name;
        if(file_exists($path)){
           header('content-type: '.$image->image_name);
           readfile('uploads/images/'.$image->image_name);
        }else{
          header('content-type: jpg ');
          readfile('uploads/images/default.jpg');
        }    
    }
  }

  /*
  * @process - delete course image
  * @param - course_id
  */

  private function deleteCourseImage($id){
    $course = Course::find($id);
      if($course != NULL){
        $image = CourseImages::find($course->image);
        if($image !=NULL){
            $path = './uploads/course/'.$image->id.".".$image->image_type;
            if(file_exists($path)){
               if(File::Delete($path)){
                  if($image != NULL){
                      File::Delete('./uploads/course/'.$image->id."_lg.".$image->image_type);
                      File::Delete('./uploads/course/'.$image->id."_medium.".$image->image_type);
                      File::Delete('./uploads/course/'.$image->id."_small.".$image->image_type);
                      if($image->aws_url != NULL){
                        preg_match("'https://(.*?).s3.amazonaws.com'si", $image->aws_url, $match);
                        if(isset($match[1])){
                          $bucket = $match[1];
                          $s3 = new \S3(AWS_KEY, AWS_SECRET);
                          if($s3->getObjectInfo($bucket, $image->id.".".$image->image_type, false)){
                            $s3->deleteObject($bucket, $image->id.".".$image->image_type, 'public-read');
                            $s3->deleteObject($bucket, $image->id."_lg.".$image->image_type, 'public-read');
                            $s3->deleteObject($bucket, $image->id."_medium.".$image->image_type, 'public-read');
                            $s3->deleteObject($bucket, $image->id."_small.".$image->image_type, 'public-read');
                          }
                        }
                      }
                      $image->delete();
                  }
               }
            }
        }
      }
  }
  //for course certificate for mobile app
  public function getCertificate($course_id, $uid ,$slug = NULL){
       ini_set('max_execution_time', 0);   
       if(!$this->checkCompletionapi($course_id,$uid)){
            return Redirect::to('courseview/' . $course_id . '/' . $slug);
       }
       $course = Course::find($course_id);
       $this->data['course_title'] = $course->course_title;
       $users = Users::find($uid);

       $this->data['name'] = $users->username;
       $this->data['description'] = $course->description;
      // return view('course.certificate',$this->data);
       return \PDF::loadView('course/certificate', $this->data)
            ->setPaper('a4')
            // ->setOrientation('landscape')
            ->setWarnings(false)
            ->stream('completion.pdf');
    } 
    // check course completion API
    public function checkCompletionapi($id='',$uid=''){
     
       $takencourse  = $this->model->getcoursetaken($id,$uid);
        if(count($takencourse) < 1){
            return false;
        }else{
           $courselecturetotal= $this->model->getCourseLectureTotal($id);
           $courselecturecompleted = $this->model->getCourseCompletedCount($id,$uid);
           if($courselecturetotal != $courselecturecompleted){
             return false;
           }
        }
        return true;
    } 
    // Download resource file for mobile api
    public function getDownloadresourcemob($name)
    {
        $geturls     = ($name == null ? '' : \SiteHelpers::encryptID($name,true));
        $filename    = \DB::table('course_files')->where('id',$geturls)->get();
        $file        = "./uploads/files/".$filename['0']->file_name.'.'.$filename['0']->file_extension;
        return \Response::download($file);
    }
    // Download lecture notes for mobile
    public function getDownloadnotesmob($lid,$uid)
    {
        $getnotes = $this->model->getnotesmob($lid,$uid);       
        $filename = "downloadnotes.csv";
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('Number', 'Note'));
        $i=1;
        foreach($getnotes as $row) {
            fputcsv($handle, array($i,$row->notes));
            $i++;
        }

        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
        );

        return \Response::download($filename, 'downloadnotes.csv', $headers);
    }

    public function postVideo(){
      $video_id = $_POST['vid'];
      $vidoes = $this->model->getVideobyid($video_id);
      if(isset($vidoes->aws_url)){
          if($vidoes->aws_url != NULL)
            echo $vidoes->aws_url.$vidoes->video_title;
          else
            echo \URL::to('uploads/videos/'.$vidoes->video_title);
        }
        exit(0);
    }

    // YouTube Video features

     public function getYoutubeupload($videoid,$course_id)
     {
      if(!\Auth::check()) return Redirect::to('user/login');
      $t_return                   = strtoupper(\Request::get('return'));
      $loggedid                   = \Session::get('uid');
      $youtube                     = $this->model->inserety_id($videoid,$course_id);
      print_r($youtube); 
     }

     public function getYoutubepriupload($videoid,$course_id)
     {
     
      if(!\Auth::check()) return Redirect::to('user/login');
      $t_return                   = strtoupper(\Request::get('return'));
      $loggedid                   = \Session::get('uid');
      $youtube                    = $this->model->inseretypri_id($videoid,$course_id);
      echo $youtube;
     } 

    public function getYoutubelectureupload($videoid,$lecture_id)
     {
      
      if(!\Auth::check()) return Redirect::to('user/login');
      $t_return                   = strtoupper(\Request::get('return'));
      $loggedid                   = \Session::get('uid');
      $youtube                     = $this->model->inseretyoucur_id($videoid,$lecture_id);
      print_r($youtube); 
     }

  
  // end class

    public function postSubcategory(Request $request)
    {     
      $cid  = $request->input('cats');
      $sid  = $request->input('subcat');
      if(!empty($cid)){ 
        $results = \bsetecHelpers::sitesubcategories($cid);
        if(!empty($sid)){
          $sid                 = $sid;
        }else{
          $sid                 = 0;
        }
        $this->data['sid']     = $sid;
        $this->data['results'] = $results;
        return view('course.subcats',$this->data);
      }
    }

	    function DateRangeArray($strDateFrom,$strDateTo){
        $aryRange=array();
        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                array_push($aryRange,date('Y-m-d',$iDateFrom));
            }
        }
        return $aryRange;
    }

	 function postCountdowntimer()
    {
      $product_time=$_POST['product_time'];
      $remaining = $product_time - strtotime(date('Y-m-d H:i:s'));
      $data['days_remaining'] = sprintf("%02d",floor($remaining / 86400));
      $data['hours_remaining'] = sprintf("%02d",floor(($remaining % 86400) / 3600));
      $data['minutes_remaining'] = sprintf("%02d",floor(($remaining / 60) % 60));
      $data['seconds_remaining'] = sprintf("%02d", floor($remaining % 60));
      $final=json_encode($data);
      echo $final;
      exit();
    }

   

  
  // end class
    
}  
