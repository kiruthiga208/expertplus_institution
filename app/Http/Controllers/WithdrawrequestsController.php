<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Withdrawrequests;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\Models\Users;


class WithdrawrequestsController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'withdrawrequests';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->model = new Withdrawrequests();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'withdrawrequests',
			'return'	=> self::returnUrl()
			
		);
	}

	public function Index( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('withdrawrequests');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('withdrawrequests.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('withdraw_requests'); 
		}

		$this->data['id'] = $id;
		$this->data['users_list']  = Users::select(\DB::raw("CONCAT(first_name,' ', last_name) AS full_name"),'id')->where('active',1)->pluck('full_name', 'id');
		return view('withdrawrequests.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');

		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('withdraw_requests'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('withdrawrequests.view',$this->data);	
	}	

	function postSave( Request $request, $id =0)
	{
		
		$rules = $this->validateForm();

		$rules['user_id']   = 'required';
		$rules['paypal_id'] = 'required|email';

		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('withdrawrequests');
			$now_date 			= date("Y-m-d H:i:s");

			if($id==0){
				$data['requested_on']  = date('Y-m-d H:i:s');
			}

			$newID = $this->model->insertRow($data , $request->input('id'));
			if(!is_null($request->input('apply')) && $id)
			{
				$return = 'withdrawrequests/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'withdrawrequests?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('withdrawrequests/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('withdrawrequests')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('withdrawrequests')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}

	public function getDelete($id = null)	
	{
		$row = $this->model->getRow($id);
		if($row)
		{
			if($this->model->get_delete($row))
			{
				return Redirect::to('withdrawrequests')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
			} else 
			{
				return Redirect::to('withdrawrequests')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
			}
		}
	}	

	public function getToken() 
	{
		//generate access token using the application client id and secret key
		$ch = curl_init();
		$clientId = "AX4KXVQpmwmazIixHqrZ-Uqvb1UmBPVTusqV75K0YAEguVEGiE3lBkWE2ZI0gRnNVSYsYr12SQX7gpS9";
		$secret = "EOGVLofZyMQ_SOHradT8iNUUalDnjuSbZ33JyxenLzeIpsUcHtCYXFYqrBKe3d9esTQtAqoVTDhtJy0t";

		curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/oauth2/token");
		// curl_setopt($ch, CURLOPT_HEADER, false);


		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
		
		$result = curl_exec($ch);

		if(empty($result))die("Error: No response.");
		else
		{
		    $json = json_decode($result);
		    print_r($json->access_token);
		}

		curl_close($ch);
	}

	public function getSendMoney($id = '') 
	{
		
		$request = $this->model->find($id);
		//generate access token
		$ch = curl_init();
		$access_token = 'A101.ywzhFe-EdytIxxSl9s0R5Lw2iEN-oDUoq1goWqHf9qH9MDu33g_RUruEvGTzTN7h.BmlPooQJqlzdkm3I_X0951D7ra4';

		curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/payments/payouts/");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Accept: application/json',
				'Content-Type: application/json',
			    'Authorization: Bearer '.$access_token
			  ));

		$items = array(
					// 'sender_batch_header'=>
					// array('sender_batch_id'=>'2014021805',
					// 	  'email_subject'=>'You have a payment',
					// 	  'recipient_type'=>'EMAIL'),
					'items'=>array('0'=>
					array('recipient_type'=>'EMAIL',
						'amount'=>array('value'=>'0.99', 'currency'=>'USD'),
						'receiver'=> $request->paypal_id,
						'note'=>'Thank you.',
						'sender_item_id'=>'22254'
						)),
					
				);

		// echo '<pre>';print_r(json_encode($items));exit;

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($items));

		$result = curl_exec($ch);

		// echo '<pre>';print_r($result);exit;
		curl_close($ch);

		$this->model->insertRow(array('status'=>'completed') , $id);
		return Redirect::to('withdrawrequests')
        		->with('messagetext', 'Amount transferred')->with('msgstatus','success'); 
	}		


}

 