<?php namespace App\Http\Controllers;

use App\Models\bsetec;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ;

class StripeController extends Controller {

    protected $headers;
    protected $api_url = "https://api.stripe.com/v1/";
    protected $call_url = "";
    protected $stripe_api_key = "";
    protected $fields = array();

    public function __construct()
    {
        $this->httpClient = new Client();
        $this->bsetec = new bsetec();
        $stripe = $this->bsetec->get_options('stripe');
        $this->headers = array('Authorization: Bearer '.$stripe['secret_key']); // STRIPE_API_KEY - your stripe api key
    }

    /**
     * SEND THE API CALL
     * Returns array
     * @param string, array, string
     */
    public function send($name, $data, $method = '') {

        if(!empty($name) && !empty($data)){
            $this->call_url = $this->api_url.$name;
            $ch = curl_init();
            if($method == 'DELETE'){
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                if(is_array($data)){
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                    curl_setopt($ch, CURLOPT_URL, $this->call_url);
                } else {
                    curl_setopt($ch, CURLOPT_URL, $this->call_url.'/'.$data);
                }
            } else if($method == 'GET'){
                $api_request_url .= '?' . http_build_query($data);
                curl_setopt($ch, CURLOPT_URL, $api_request_url);
            } else if($method == 'PUT'){
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                curl_setopt($ch, CURLOPT_URL, $this->call_url);
            } else {
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                curl_setopt($ch, CURLOPT_URL, $this->call_url);
            }
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSLVERSION, 6);
            curl_setopt($ch, CURLOPT_CAINFO, app_path() . '/Library/cacert.pem'); //CA cert file
            $output = curl_exec($ch);
            curl_close($ch);
            return json_decode($output, true); // return php array with api response
        }
        return false;
    }

}
