<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Ccavenue;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\Models\Course;
use App\Models\bsetec;
use App\Models\Transaction;
use App\Models\UserCredits;

use App\Models\CourseTaken;
use App\Models\Coupon;
use App\Models\Admincoupon;
use Carbon\Carbon;
use danielme85\CConverter\Currency;

class CcavenueController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'ccavenue';
	static $per_page	= '10';

	public function __construct()
	{
		$this->bsetec = new bsetec();
		$this->transaction = new Transaction();
		$this->course = new Course();
		$this->user_credits = new UserCredits();
		
		$this->model = new Ccavenue();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'ccavenue',
			'return'	=> self::returnUrl()
			
		);
	}

	public function Index( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('ccavenue');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('ccavenue.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('transactions'); 
		}

		$this->data['id'] = $id;
		return view('ccavenue.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('transactions'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('ccavenue.view',$this->data);	
	}	

	function postSave( Request $request, $id =0)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_ccavenue');
			
			$newID = $this->model->insertRow($data , $request->input('id'));
			if(!is_null($request->input('apply')))
			{
				$return = 'ccavenue/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'ccavenue?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('ccavenue/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('ccavenue')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('ccavenue')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}

	// ccavenue payment methods
	public function postCcavenuerequest(Request $request){
		$ccavenue = $this->bsetec->get_options('ccavenue');
		$mode = $ccavenue['TestMode'];
		$working_key=$ccavenue['working_key'];//Shared by CCAVENUES
		$access_code=$ccavenue['access_code'];//Shared by CCAVENUES
		$merchant_data='';
		
		$admin_discount = $this->Admindiscount($request->input('course_id'));
		foreach ($_POST as $key => $value){
			if($key==='amount'){
					if(is_array($admin_discount)){
					$this->data['admin_discount'] = true;
					$this->data['discount_amount'] = $admin_discount['discount_amount'];
					$value = $admin_discount['amount'];
					}else{
						$this->data['admin_discount'] = false;
						$this->data['d_amount'] = false;
						$value = $request->input('amount');
					}
				if(\Session::has('coupon_price'))
           			 $value = \Session::get('coupon_price');
					$merchant_data.=$key.'='.$value.'&';
			}
			else{
				$merchant_data.=$key.'='.$value.'&';
			}
		}
		$encrypted_data= $this->model->encrypt($merchant_data,$working_key); // Method for encrypting the data.
		$this->data['encrypt'] = $encrypted_data;
		$this->data['access_code'] =$access_code;
		
		if($mode==='false')
			return redirect('https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction&encRequest='.$encrypted_data.'&access_code='.$access_code);
		else
			return redirect('https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction&encRequest='.$encrypted_data.'&access_code='.$access_code);
	}

	public function postCcavenueresponse(){

		$ccavenue = $this->bsetec->get_options('ccavenue');
		$course_id = \Session::get('course_id');
		$course = Course::find($course_id);

		$workingKey=$ccavenue['working_key'];		//Working Key should be provided here.
		$encResponse=$_REQUEST["encResp"];			//This is the response sent by the CCAvenue Server
		$rcvdString= $this->model->decrypt($encResponse,$workingKey);	//Crypto Decryption used as per the specified working key.
		$order_status="";
		
		$decryptValues=explode('&', $rcvdString);
		$order_details = json_encode($decryptValues);
		$dataSize=sizeof($decryptValues);
		echo "<center>";

		for($i = 0; $i < $dataSize; $i++) 
		{
			$information=explode('=',$decryptValues[$i]);
			if($i==3)	$order_status=$information[1];
			if($i==10)	$amount = $information[1];
		}

		//save the transaction details in DB
			if($order_status==="Success")
				$order_status ="completed";
			$transaction['user_id'] = \Session::get('uid');
			$transaction['course_id'] = $course_id;
			$transaction['amount'] = floatval($amount);
			$transaction['status'] = $order_status;
			$transaction['payment_method'] = 'ccavenue';
			$transaction['order_details'] = $order_details;
			$id = $this->save_transaction($transaction);

			\Session::put('transaction_id', $id);
			\Session::put('course_id', $course_id);
			\Session::save();

		if($order_status==="Success")
		{
			$courseTaken = new CourseTaken;
			$courseTaken->user_id = \Session::get('uid');
			$courseTaken->course_id = $course_id;
			$courseTaken->save();
			return view('course/success')->with('course', $course)->with('title', 'Course')->with('status', 'success')->with('transId', $id);
		}
		else if($order_status==="Aborted")
		{
			return view('course/success')->with('course', $course)->with('title', 'Course')->with('status', 'Aborted')->with('transId', $id);
		}
		else if($order_status==="Failure")
		{
			return view('course/success')->with('course', $course)->with('title', 'Course')->with('status', 'failed')->with('transId', $id);
		}
		else
		{
			return view('course/success')->with('course', $course)->with('title', 'Course')->with('status', 'Security Error')->with('transId', $id);
		}	
		
	}

	// Transaction support function
	function save_transaction($data)
	{
		//check if the status is completed
		$completed = in_array('completed', $data) ? true : false;
		
		//check if there is transaction id, if so find it or else create a new one
		$transaction = array_key_exists('id', $data) ? Transaction::find($data['id']) : new Transaction;
		//insert all the values in object
		foreach ($data as $key => $value) 
		{
			$transaction->$key = $value;
		}
		
		$transaction->save();
		//process the invoice generation(get transaction details and save it in invoice table), if the status is completed
		if($completed)
		{
			$this->generate_invoice($transaction->id);
			//save credits
			$this->save_credits($transaction->id);
		}
		return $transaction->id;
	}

	function generate_invoice($transaction_id)
	{
		$transaction_detail = $this->transaction->get_transaction_detail($transaction_id);

		//update the order details on transaction table
		$invoice['transaction_id'] = $transaction_detail->id;
		$invoice['customer_name'] = $transaction_detail->customer_name;
		$invoice['course_title'] = $transaction_detail->course_title;
		$invoice['amount'] = $transaction_detail->amount;
		$invoice['email'] = $transaction_detail->email;
		$invoice['status'] = $transaction_detail->status;
		$invoice['payment_method'] = $transaction_detail->payment_method;
		
		$invoice['invoice_number'] = time().$transaction_id;
		$invoice['ordered_on'] = date('Y-m-d H:i:s');
		
		$this->transaction->save_invoice($invoice);

		$email = $invoice['email'];
		$this->data['invoice'] = $invoice;
		
		//send mail to the customer
		/*\Mail::send('course.invoice', $this->data, function($message) use($email)
		{
		    $message->subject('Invoice from expert plus');
		    $message->from('admin@expertplus.com', 'Expert Plus');
		    $message->to($email);

			//pass the same data to pdf view file and generate a pdf    
			$pdf = \PDF::loadView('course.invoice-pdf', $this->data);
			//get the pdf file as data
			$attach =  $pdf->stream();
			//attach the pdf in mail
		    $message->attachData($attach, 'invoice.pdf');
		});*/
	}

	function save_credits($transaction_id)
	{
		//get transaction details
		$transaction = $this->transaction->find($transaction_id);

		//get commision percentage from db
		$commision_percentage = $this->bsetec->get_option('commision_percentage');
		
		//calculate the credits
		$amount = $transaction->amount;
		$admin_credit = ($amount * $commision_percentage)/100;
		$instructor_credit = $amount - $admin_credit;

		//get instructor id for the course id
		$instructor_id = $this->course->getCourseInstructor($transaction->course_id);

		//save credit for instructor
		$credit['transaction_id'] = $transaction_id;
		$credit['instructor_user_id'] = $instructor_id;
		$credit['course_id'] = $transaction->course_id;
		$credit['by_user_id'] = $transaction->user_id;
		$credit['is_admin'] = 0;
		$credit['credits_for'] = 'course_cost';
		$credit['credit'] = $instructor_credit;
		$credit['created_at'] = time();

		$this->user_credits->save_credits($credit);
		//save credit for instructor
		$credit['instructor_user_id'] = 0;
		$credit['is_admin'] = 1;
		$credit['credits_for'] = 'course_commision';
		$credit['credit'] = $admin_credit;
		$this->user_credits->save_credits($credit);
	}

	//admin discount
	public function Admindiscount($course_id){
        $coupon = Admincoupon::where('coupon_start_date','<=',Carbon::now()->toDateString())
                    ->where('coupon_end_date','>=',Carbon::now()->toDateString())
                    ->where('coupon_status',1)
                    ->first();

        $course = Course::find($course_id);
        
        $course_price = $course->pricing;

        if($course->course_admin_discount == 0){
			return false;
        }
     
        if($coupon == NULL){
           return false;
        }else{
        	$discount_amount = $coupon->coupon_value;
	        if($coupon->coupon_type == 1){
	        	$discount_amount =  $course_price * ($discount_amount / 100);
	            $amount = $course_price - $discount_amount;
	        	
	        }else{
	           $amount = $course_price - $discount_amount;
	        }
        }

        if($amount < 0)
            $amount = 0;

        return array('discount_amount'=>$discount_amount,'amount'=>$amount);
   }

}