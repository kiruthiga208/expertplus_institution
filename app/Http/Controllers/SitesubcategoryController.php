<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Sitesubcategory;
use App\Models\Sitecategory;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 


class SitesubcategoryController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'sitesubcategory';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->model = new Sitesubcategory();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'sitesubcategory',
			'return'	=> self::returnUrl()
			
		);
	}

	public function Index( Request $request )
	{
		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
				
		

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'sub_cat_id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');
		$filter = str_replace('expert_sub_categories', \bsetecHelpers::getdbprefix().'expert_sub_categories',$filter );
		// echo $filter;exit;
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('sitesubcategory');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('sitesubcategory.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('expert_sub_categories'); 
		}

		$this->data['id'] = $id;
		$this->data['category']  = Sitecategory::pluck('name', 'id');
		return view('sitesubcategory.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('expert_sub_categories'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('sitesubcategory.view',$this->data);	
	}	

	function postSave( Request $request, $id=null)
	{
		
		$rules = $this->validateForm();
		$messages = [

        'cat_id.required' => 'Category Id is required',
        'sub_name.required' => 'Subcategory Name is required',
    ];
		$validator = Validator::make($request->all(), $rules,$messages);	
		if ($validator->passes()) {
			$data = $this->validatePost('sitesubcategory');

			$now_date 			= date("Y-m-d H:i:s");
			if($id){
				$data['updated_at']  = $now_date;
			}else{
				$data['created_at']  = $now_date;
			}
			
			$data['cat_id'] 					= $request->input('cat_id');
			$data['sub_name'] 					= $request->input('sub_name');
			$data['sub_slug'] 					= str_slug($request->input('sub_name'));
			$status 							= $request->input('status');
			if(empty($status)){
				$status 						= 'disable';
			}
			$data['status'] 					= $status;
			$newID = $this->model->insertRow($data , $id);
		
			if(!is_null($request->input('apply')) && $id !=null)
			{
				$return = 'sitesubcategory/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'sitesubcategory?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('sitesubcategory/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			if(count($request->input('id'))>0){
				foreach ($request->input('id') as $key => $value) {
					$checkcatid	= \SiteHelpers::checkcatecourse($value,1);
					if($checkcatid==NULL && !$checkcatid){
						$this->model->destroy($value);
						return Redirect::to('sitesubcategory')
						->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
					}else{
						return Redirect::to('sitesubcategory')
						->with('messagetext', \Lang::get('core.subcat_delete_msg'))->with('msgstatus','error'); 

					}

				}
			}
	
		} else {
			return Redirect::to('sitesubcategory')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}			


}