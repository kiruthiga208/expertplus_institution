<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Courselisting;
use App\Models\Course;
use App\Models\Notificationsettings;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use Mail;
use App\User;
use App\Http\Controllers\CommonmailController;
class CourselistingController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'courselisting';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->model = new Courselisting();
		$this->coursemodel = new Course();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
		$this->sendmail = new CommonmailController();
		$this->notification_settings = new Notificationsettings();

		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'courselisting',
			'return'	=> self::returnUrl()
			
			);
		$this->videomodel = new Course();
	}

	public function Index( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
		->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'course_id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');
		
		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
			);
		// Get Query 

		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('courselisting');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any

		$this->data['user_lists'] 	= $this->model->getsearchColumn('user_id');
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('courselisting.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{

		if($id =='')
		{
			if($this->access['is_add'] ==0 )
				return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
				return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				

		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('course'); 
		}

		$this->data['id'] = $id;
		return view('courselisting.form',$this->data);
	}	

	public function getShow( $id = null)
	{

		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
		->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');

		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('course'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('courselisting.view',$this->data);	
	}	

	function postSave( Request $request, $id =0)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('courselisting');
			
			$newID = $this->model->insertRow($data , $request->input('course_id'));
			if(!is_null($request->input('apply')))
			{
				$return = 'courselisting/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'courselisting?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('courselisting/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	

	}	

	public function postDelete( Request $request)
	{
		$type  = $request->input('types');
		$data  = array();
		if($type=='0'){
			if($this->access['is_remove'] ==0) 
				return Redirect::to('dashboard')
			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
			// delete multipe rows 
			// if(count($request->input('id')) >=1)
			// {
				//$this->model->destroy($request->input('id'));
			$id 	= $request->input('id');
			if(count($id)>0){
				foreach ($id as $key => $cvalue) {
					$cinfo      = $this->coursemodel->getcourseinfo($cvalue);
					$userinfo 	= $this->coursemodel->getuserinfos($cinfo->user_id);
					if(count($userinfo)>0){
						$check = $this->notification_settings->where('user_id', $cinfo->user_id)->where('notif_course_delete', 1)->where('notif_for_all', 0)->first();
		                if(count($check)>0)
		                {
		                	$alink      = \URL::to('courseview/'.$cinfo->course_id.'/'.$cinfo->slug);
							$fromMail   = CNF_EMAIL;
				            $toMail     = $userinfo['0']->email;
				            $subject    = CNF_APPNAME." Course Deleted ";
				            $data       = array('authorname'=>'Admin','link'=>$alink,'title'=>$cinfo->course_title,'username'=>$userinfo['0']->username); 
				            $tempname   = 'mailtemplates.course_delete';
				            $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
						}
					}
		           
		            $this->coursemodel->deletecourses($cvalue);
				}

				\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
				// redirect
				return Redirect::to('courselisting')
				->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 

			} else {
				return Redirect::to('courselisting')
				->with('messagetext','No Item Deleted')->with('msgstatus','error');				
			}
		}elseif ($type=='1') {
			$id 	= $request->input('id');
			if(count($id)>0)
			{
				foreach ($id as $key => $value)
				{
					$user = $this->model->getuser($value);
					if($user == 1)
					{
						$data['approved']   = 1;
						\DB::table('course')->where('course_id','=', $value)->update(['approved' => '1']);
						 $allinstructor   = $this->coursemodel->getinstructors($value);
						if(count($allinstructor)>0){
		                    foreach ($allinstructor as $key => $ivalue) {
		                    	$courseinfo = $this->coursemodel->getcourseinfo($ivalue->course_id);
		                        $fromdetails = \DB::table('users')->where('id', '=', $courseinfo->user_id)->get();
		                        $getdetails = \DB::table('users')->where('id', '=', $ivalue->user_id)->where('id', '!=', $courseinfo->user_id)->get();

		                         if(count($getdetails)>0){
		                            $alink      = \URL::to('courseview/'.$courseinfo->course_id.'/'.$courseinfo->slug);
		                            $fromMail   = $fromdetails['0']->email;
		                            $toMail     = $getdetails['0']->email;
		                            $subject    = CNF_APPNAME." Invite Instructor ";
		                            $data       = array('username'=>$getdetails['0']->username,'link'=>$alink,'subject' => "[ " .CNF_APPNAME." ] Invite Instructor "); 
		                          	$tempname   = 'course.emails.instructor_invite';
		                          	$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
		                         }	                                            
		                    }
		                }

		                $getinviterusers  = $this->coursemodel->getinviteuserinfo($value);
		                if(count($getinviterusers)>0){
		                    foreach ($getinviterusers as $key => $prvalue) {
		                        $alink      = \URL::to('courseview/'.$prvalue->course_id.'/'.$prvalue->slug);
		                        $fromMail   = \Auth::user()->email;
		                        $toMail     = $prvalue->email;
		                        $subject    = CNF_APPNAME." Course Invite User";
		                        $data       = array('username'=>$prvalue->username,'link'=>$alink,'subject' => "[ " .CNF_APPNAME." ] Course Invite User "); 
		                        $tempname   = 'course.emails.inviteuser';
		                        $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
		                      	                       
		                    }
		                }
		                //approve mail
		                $courseinfo = $this->coursemodel->getcourseinfo($value);	                
		                $check = $this->notification_settings->where('user_id', $courseinfo->user_id)->where('notif_course_approve', 1)->where('notif_for_all', 0)->first();
		                if(count($check)>0)
		                {	
		                	$alink      = \URL::to('courseview/'.$courseinfo->course_id.'/'.$courseinfo->slug);
		            		$todetails = \DB::table('users')->where('id', '=', $courseinfo->user_id)->get();
			                $fromMail   = CNF_EMAIL;
			                $toMail     = $todetails['0']->email;
			                $subject    = CNF_APPNAME." Course Approved";
			                $data       = array('authorname'=>$todetails['0']->username,'link'=>$alink,'title'=>$courseinfo->course_title); 
			                $tempname   = 'mailtemplates.course_approved';
			                // return view('mailtemplates.course_approved')->with('link',$alink);
			                $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
	                	}
					}
					else
					{
						return Redirect::to('courselisting')
						->with('messagetext', 'Course Author lost an membership!!')->with('msgstatus','info');
					}
				}					      
				return Redirect::to('courselisting')
				->with('messagetext', 'Course has been approved successfully.')->with('msgstatus','success');
			}
			else
			{
				return Redirect::to('courselisting')
				->with('messagetext','No Course Approved')->with('msgstatus','error');	
			}

		}elseif ($type=='2') 
		{
			$id = $request->input('id');
			if(count($id)>0)
			{
				foreach ($id as $key => $value) 
				{
					$user = $this->model->getuser($value);
					if($user == 1)
					{					
						$data['approved']   = 2;
						\DB::table('course')->where('course_id','=', $value)->update(['approved' => '2']);
						\DB::table('manage_instructors')->where('course_id',$value)->update(array('instrctor_status'=>'0'));
						
						//unapprove mail	
						$courseinfo = $this->coursemodel->getcourseinfo($value);
			            $alink      = \URL::to('courseview/'.$courseinfo->course_id.'/'.$courseinfo->slug);
			            $todetails = \DB::table('users')->where('id', '=', $courseinfo->user_id)->get();
		                $fromMail   = CNF_EMAIL;
		                $toMail     = $todetails['0']->email;
		                $subject    = CNF_APPNAME."Unpproved Course";
		                $data       = array('authorname'=>$todetails['0']->username,'link'=>$alink,'title'=>$courseinfo->course_title); 
		                $tempname   = 'mailtemplates.course_unapproved';
		                $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
					}
					else
					{
						return Redirect::to('courselisting')
						->with('messagetext', 'Course Author lost an membership!!')->with('msgstatus','info');
					}
				}
				return Redirect::to('courselisting')
				->with('messagetext', 'Course has been Unapproved successfully.')->with('msgstatus','success');
			}else{
				return Redirect::to('courselisting')
				->with('messagetext','No Course Approved')->with('msgstatus','error');	
			}
			
		}elseif ($type=='3')
		{
			$id = $request->input('id');
			if(count($id)>0)
			{
				foreach ($id as $key => $value)
				{
					$user = $this->model->updateCourseGamification($value,'1');
				}
				return Redirect::to('courselisting')
				->with('messagetext', 'Gamification Enabled for selected Courses')->with('msgstatus','success');
			}else{
				return Redirect::to('courselisting')
				->with('messagetext','No Course Selected')->with('msgstatus','error');
			}
		}elseif ($type=='4')
		{
			$id = $request->input('id');
			if(count($id)>0)
			{
				foreach ($id as $key => $value)
				{
					$user = $this->model->updateCourseGamification($value,'0');
				}
				return Redirect::to('courselisting')
				->with('messagetext', 'Gamification Disabled for selected Courses')->with('msgstatus','success');
			}else{
				return Redirect::to('courselisting')
				->with('messagetext','No Course Selected')->with('msgstatus','error');
			}
		}
		return Redirect::to('courselisting');
				

	}		

	public function getFeedback($id, Request $request)
	{
		// $getdata 				= $this->model->getallcomments($id);
		$feedback_parent  = $this->model->getallcomments($id);
	      $feedback = array();
	      if(count($feedback_parent)>0){
	          for($i=0;$i<count($feedback_parent);$i++)
	          {
	            $feedback[] = $feedback_parent[$i];
	            $feedback_child = $this->model->getallcomments($id,$feedback_parent[$i]->feedback_id);
	            if(count($feedback_child)>0)
	            {
	              for($j=0;$j<count($feedback_child);$j++) 
	              {
	                $feedback[] = $feedback_child[$j];
	              }
	            }
	          }
	      }





		$this->data['courseid'] = $id;
		$this->data['results']  = $feedback;
		return view('courselisting.feedback',$this->data);
	}	

	public function postSavefeedback(Request $request)
	{
		$rules 						= $this->validateForm();
		$courseid 					= $request->input('course_id');
		$rules['comments'] 			= 'required';
		$validator 					= Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$comments 				= $request->input('comments');
			// course_id
			$getinstructorinfo      = $this->model->getcourseinfos($courseid);
			$courseinfo = $this->coursemodel->getcourseinfo($courseid);
			if(count($getinstructorinfo)>0){
				$toMail 					= $getinstructorinfo->email;
				$subject 					= CNF_APPNAME." Course Feedback "; 			
				$maildata 					= array();
				if(!empty($getinstructorinfo->username)){
					$uname  = $getinstructorinfo->username;
				}else{
					$uname  = $getinstructorinfo->first_name.' '.$getinstructorinfo->last_name;
				}
				$maildata['link']     = \URL::to('courseview/'.$courseinfo->course_id.'/'.$courseinfo->slug);
				$maildata['username'] 		= $uname;
				$maildata['coursename']   	= $getinstructorinfo->course_title;
				$maildata['feedback']   	= $comments;
				$fromMail 					= CNF_EMAIL;
				$tempname 					= 'mailtemplates.course_feedback';
				 		
		 		$this->sendmail->getMail($fromMail,$toMail,$subject,$maildata,$tempname);
			}
			
			$this->model->insertfeedback($comments,$courseid);	
			return Redirect::to('courselisting/feedback/'.$courseid)
				->with('messagetext', 'Feedback has been added successfully.')->with('msgstatus','success');
		} else {

			return Redirect::to('courselisting/feedback/'.$courseid)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}
	}

	public function getRemovefeedback()
	{
		$fid = \Request::segment(3);
		$cid = \Request::segment(4);

		if(!empty($fid) && !empty($cid)){
			$this->model->deletefeedback($fid,$cid);	
			return Redirect::to('courselisting/feedback/'.$cid)->with('messagetext', 'Comments has been deleted successfully.')->with('msgstatus','success');
		}else{
			return Redirect::to('courselisting/feedback/'.$cid)->with('messagetext', 'Error occur please try again later.')->with('msgstatus','success');
		}
		

	}
	public function getTestvideo()
	{
		$id = Input::get('id');
		return $this->videomodel->getvideoinfo($id);
	}

}