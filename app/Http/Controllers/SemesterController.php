<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Semester;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 


class SemesterController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'semester';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->model = new Semester();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'semester',
			'return'	=> self::returnUrl()
			
		);
	}

	public function Index( Request $request )
	{

		if(!\Session::has('iid')) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query


		// Filter Search for query		


		if(!is_null($request->input('search'))){
			$search = $request->input('search');
			$pos = strrpos($search, ":")+1;
			$key = substr($search, 0, strpos($search, ":"));
			$value = substr($search, strpos($search, ":") + 1);

			if(strtolower($key)=='status'){
				if(strtolower($value)=='active'){
					$_GET['search'] = substr_replace($search, 1, $pos, strlen($search));
				} else if(strtolower($value)=='inactive'){
					$_GET['search'] = substr_replace($search, 0, $pos, strlen($search));
				}
			}
			
		}



		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');
		$filter .= " AND ".\bsetecHelpers::getdbprefix()."institution_semester.institution_id = '".\Session::get('iid')."'" ;
		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('semester');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 

		foreach($this->data['tableGrid'] as $key => $value){
			if($value['field']=='institution_id')
				$this->data['tableGrid'][$key]['label'] = 'Institution';
			else if($value['field']=='department_id')
				$this->data['tableGrid'][$key]['label'] = 'Department';
			else if($value['field']=='institution_course_id')
				$this->data['tableGrid'][$key]['label'] = 'Course';
		}


		foreach ($this->data['tableForm'] as $key => $value) {

			if($value['field']=='department_id') {
				$this->data['tableForm'][$key]['type'] = 'select';
				$this->data['tableForm'][$key]['option']['opt_type'] = 'external';
				$this->data['tableForm'][$key]['option']['lookup_key'] = 'id';
				$this->data['tableForm'][$key]['option']['lookup_value'] = 'department';
				$this->data['tableForm'][$key]['option']['lookup_table'] = "bse_departments Where institution_id='".\Session::get('iid')."' AND status='1'";
			} else if($value['field']=='institution_course_id') {
				$this->data['tableForm'][$key]['type'] = 'select';
				$this->data['tableForm'][$key]['option']['opt_type'] = 'external';
				$this->data['tableForm'][$key]['option']['lookup_key'] = 'id';
				$this->data['tableForm'][$key]['option']['lookup_value'] = 'name';
				$this->data['tableForm'][$key]['option']['lookup_table'] = "bse_institution_course Where institution_id='".\Session::get('iid')."' AND status='1'";
			}
	
		}


		// Render into template
		return view('semester.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if(!\Session::has('iid'))
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
			
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('institution_semester'); 
		}

		$departments = \DB::table('departments')->select('department', 'id')->where('institution_id', \Session::get('iid'))->where('status', '1')->get();
		$this->data['departments'] = array();
		foreach ($departments as $key => $value) {
			$this->data['departments'][$value->id] = $value->department;
		}

		$this->data['courses'] = array();

		$this->data['id'] = $id;
		return view('semester.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if(!\Session::has('iid'))
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('institution_semester'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('semester.view',$this->data);	
	}	

	function postSave( Request $request, $id =0)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	

		if ($validator->passes()) {
			$data['semester'] = $request->get('semester');
			$data['institution_id'] = \Session::get('iid');
			$data['department_id'] = $request->get('department_id');
			$data['institution_course_id'] = $request->get('institution_course_id');
			$data['status'] = $request->get('status');
			if($id){
				$data['updated_at'] = date("Y-m-d H:i:s");
			}else{
				$data['created_at'] = date("Y-m-d H:i:s");
				$data['updated_at'] = date("Y-m-d H:i:s");
			}
			
			$newID = $this->model->insertRow($data , $request->input('id'));
			if(!is_null($request->input('apply')))
			{
				$return = 'semester/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'semester?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('semester/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if(!\Session::has('iid'))
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('semester')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('semester')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}

}