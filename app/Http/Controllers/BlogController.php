<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator, Input, Redirect ; 
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Models\Blog;
use App\Models\Blogcomment;
use App\Models\Notifications;
class BlogController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'blog';
	static $per_page	= '5';
	
	public function __construct() {
		parent::__construct();

		$this->data = array(
			'pageTitle'	=> 	'Blog',
			'pageNote'	=>  'Simple Blog',
			'pageModule'	=> 'blog',
			'breadcrumb'	=> 'inactive',
			);
	} 

	public function Index( $type ='' , $id ='')
	{		
		// Filter sort and order for query 
		$sort = (!is_null(Input::get('sort')) ? Input::get('sort') : 'created'); 
		$order = (!is_null(Input::get('order')) ? Input::get('order') : 'desc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null(Input::get('search')) ? $this->buildSearch() : '');
		// End Filter Search for query 
		
		// Take param master detail if any
		$master  = $this->buildMasterDetail(); 
		// append to current $filter
		$filter .=  $master['masterFilter'];

		// if($type !='' && $type =='category') $filter .= " AND blogcategories.alias ='".$id."' ";
		if($type !='' && $type =='category') $filter .= " AND blogs.CatID ='".$id."' ";
		

		$page_1 = Input::get('page', 1);
		$params = array(
			'page'		=> $page_1 ,
			'limit'		=> (!is_null(Input::get('rows')) ? filter_var(Input::get('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> 0
			);
		// Get Query 
		$results = Blog::getRows( $params );	

		// Build pagination setting
		$page_1 = $page_1 >= 1 && filter_var($page_1, FILTER_VALIDATE_INT) !== false ? $page_1 : 1;	
		// $pagination = Paginator::make($results['rows'], $results['total'],$params['limit']);		
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page_1 * $params['limit'])- $params['limit']; 
		// Grid Configuration 

		$this->data['blogcategories']			= Blog::summaryCategory();
		$this->data['clouds']					= Blog::clouds();
		$this->data['recent']					= Blog::recentPosts();

		// echo "<pre>";
		// print_r($this->data);exit;
		$this->data['pageTitle']	= 'Blog';
		$this->data['pageNote'] 	= 'Blog';
		$this->data['pageMetakey'] 	= CNF_METAKEY;
		$this->data['pageMetadesc'] = CNF_METADESC;
		$this->data['breadcrumb'] 	= 'active';		
		$theme=\bsetecHelpers::get_options('theme');
		$page = 'layouts.'.$theme['theme'].'.home.index';
		$this->data['pages'] 		= 'blog.index';
		$this->data['pagecategory']='';
		// Render into template
		return view($page,$this->data);
	}		
	
	function getRead($id = null)
	{
		$row = Blog::getRowBlog($id);
		if($row)
		{
			$this->data['row'] =  $row;
			$this->data['id'] = $row->blogID;
			$this->data['alias'] =  $row->slug;


			$this->data['blogcategories']			= Blog::summaryCategory();
			$this->data['clouds']					= Blog::clouds();
			$this->data['recent']					= Blog::recentPosts();
			$this->data['comments']					= Blog::getComments($row->blogID);

			
			$this->data['pageTitle']	= 'Blog';
			$this->data['pageNote'] 	= 'Blog';
			$this->data['pageMetakey'] 	= CNF_METAKEY;
			$this->data['pageMetadesc'] = CNF_METADESC;
			$this->data['breadcrumb'] 	= 'active';		
			$theme=\bsetecHelpers::get_options('theme');
			$page = 'layouts.'.$theme['theme'].'.home.index';
			$this->data['pages'] 		= 'blog.view';

			return view($page,$this->data);
			// ->with('page', $this->data);
		} else {
			return Redirect::to('blogs')->with('message', \SiteHelpers::alert('error',' Article not found !'));
		}	
	}
	
	function getCategory( $id = null )
	{
		if($id == null ) 
			return Redirect::to('blogs')->with('message', \SiteHelpers::alert('error','Could not find category'));
		$type ='category';
		// self::getIndex('category' ,$id );

		// Filter sort and order for query 
		$sort = (!is_null(Input::get('sort')) ? Input::get('sort') : 'created'); 
		$order = (!is_null(Input::get('order')) ? Input::get('order') : 'desc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null(Input::get('search')) ? $this->buildSearch() : '');
		// End Filter Search for query 
		
		// Take param master detail if any
		$master  = $this->buildMasterDetail(); 
		// append to current $filter
		$filter .=  $master['masterFilter'];

		if($type !='' && $type =='category') $filter .= " AND ".\bsetecHelpers::getdbprefix()."blogcategories.alias ='".$id."' ";
		// if($type !='' && $type =='category') $filter .= " AND blogs.CatID ='".$id."' ";
		

		$page_1 = Input::get('page', 1);
		$params = array(
			'page'		=> $page_1 ,
			'limit'		=> (!is_null(Input::get('rows')) ? filter_var(Input::get('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> 0
			);
		// Get Query 
		$results = Blog::getRows( $params );	

		// Build pagination setting
		$page_1 = $page_1 >= 1 && filter_var($page_1, FILTER_VALIDATE_INT) !== false ? $page_1 : 1;	
		// $pagination = Paginator::make($results['rows'], $results['total'],$params['limit']);		
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page_1 * $params['limit'])- $params['limit']; 
		// Grid Configuration 

		$this->data['blogcategories']			= Blog::summaryCategory();
		$this->data['clouds']					= Blog::clouds();
		$this->data['recent']					= Blog::recentPosts();

		// echo "<pre>";
		// print_r($this->data);exit;
		$this->data['pageTitle']	= 'Blog';
		$this->data['pageNote'] 	= 'Blog';
		$this->data['pageMetakey'] 	= CNF_METAKEY;
		$this->data['pageMetadesc'] = CNF_METADESC;
		$this->data['breadcrumb'] 	= 'active';		
		$theme=\bsetecHelpers::get_options('theme');
		$page = 'layouts.'.$theme['theme'].'.home.index';
		$this->data['pages'] 		= 'blog.index';
		$this->data['pagecategory']=$id;
		// Render into template
		return view($page,$this->data);


	}
	
	function getTags( $id = null )
	{
		if($id == null ) 
			return Redirect::to('blogs')->with('message', \SiteHelpers::alert('error','Could not find category'));
		self::getIndex('tags' ,$id );
	}	


	function getShow( $id = null)
	{

		if($this->access['is_detail'] ==0) 
			return Redirect::to('')
		->with('message', \SiteHelpers::alert('error',\Lang::get('core.note_restric')));

		$ids = ($id == null ? '' : \SiteHelpers::encryptID($id,true)) ;
		$row = Blog::getRow($ids);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('blogs'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('blog.view',$this->data);
		// ->with('menus', $this->menus );	
	}	
	
	function postSave( $id =0)
	{

		$rules = array(
			
			);
		$validator = Validator::make(Input::all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('blogs');
			$data['content'] = $_POST['content'];
			
			$ID = $this->model->insertRow($data , Input::get('blogID'));
			// Input logs
			if( Input::get('blogID') =='')
			{
				$this->inputLogs("New Entry row with ID : $ID  , Has Been Save Successfull");
			} else {
				$this->inputLogs(" ID : $ID  , Has Been Changed Successfull");
			}
			// Redirect after save	
			return Redirect::to('blog/add/'.$id)->with('message', \SiteHelpers::alert('success',\Lang::get('core.note_success')));
		} else {
			return Redirect::to('blog/add/'.$id)->with('message', \SiteHelpers::alert('error',\Lang::get('core.note_error')))
			->withErrors($validator)->withInput();
		}	

	}

	function postSavecomment( $id =0)
	{

		$rules = array(
			'comment' => 'required'	
			);
		$validator = Validator::make(Input::all(), $rules);	
		if ($validator->passes()) {
			$data = array(
				'comment'	=> Input::get("comment"),
				'blogID'	=> Input::get("blogID"),
				'created'	=> date("Y-m-d H:i:s"),
				'user_id'	=> \Session::get('uid')
			);
			$ID = \DB::table("blogcomments")->insertGetId($data);	
			//insert notificatioins
		 	Notifications::insertGetId(['user_id' => 1,'from_id'=>\Session::get('uid'),'on_id'=>$ID,'notification_type'=>2,'created_at'=>date("Y-m-d H:i:s")]);	
			return Redirect::to('blogs/read/'.Input::get('alias'))->with('message', \SiteHelpers::alert('success',\Lang::get('core.note_success')));
		} else {
			return Redirect::to('blogs/read/'.Input::get('alias'))->with('message', \SiteHelpers::alert('error','Comment field should be required'))
			->withErrors($validator)->withInput();
		}	

	}
	


	public function getRemovecomm( $id , $alias )
	{
		
		// if(\Session::get('uid') != 1) 
		// 	return Redirect::to('')
		// ->with('message', \SiteHelpers::alert('error',\Lang::get('core.note_restric')));

		// delete multipe rows 

		\DB::table('blogcomments')->where('commentID',$id)->delete(); 
		Notifications::where('user_id', '=', 1)->where('on_id', '=', $id)->where('notification_type', '=', 2)->delete();  
		// redirect
		\Session::flash('message', \SiteHelpers::alert('success','Comment has been removed !'));
		return Redirect::to('blogs/read/'.$alias);
	}
	public function postUpdatecomm( Request $request )
     {
	   $blogid = Input::get('blogid');
	   $commentid =Input::get('commentid');
	   $des = Input::get('content');
	   $user_id=Input::get('user_id');
	   $slug=Input::get('slug');
	   $comments =Blogcomment::find($commentid);  
	   $comments->comment = $des;
	   $comments->save();
	   return Redirect::to('blogs/read/'.$slug)->with('message', \SiteHelpers::alert('success',\Lang::get('core.note_success')));
  }			
	
}