<?php  
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator, Input, Redirect ; 
use App\Models\Forum;
use App\Models\Forumcategory;
use App\Models\Forumcomments;
use App\Models\Core\Users;
use Illuminate\Contracts\Auth\Authenticatable as User;
class UserforumController extends Controller {


	public function __construct()
	{
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	/* Show Forum Details */
	public function index( Request $request )
	{
		$user = \Auth::user();
		$forum= \DB::table('forum')
            ->join('forum_category', 'forum.category_id', '=', 'forum_category.forum_cat_id')
            ->join('users', 'forum.created_by', '=', 'users.id')
            ->select('users.*', 'forum.*', 'forum_category.name')->where('forum.status','=',1)
            ->orderBy('forum_id', 'desc')->paginate(3);
        $cforum = Forumcategory::get();
        $category = Forumcategory::get();
        //$count=Forumcomments::groupBy('forum_id')->count();
        $count= \DB::table('forum_comments')
                 ->select('forum_id', \DB::raw('count(*) as total'))
                 ->groupBy('forum_id')
                 ->get();

        //print_r($count);
        if (\Auth::check()){
		    $disable=Forum::where('status','=',0)->where('created_by','=',$user->id)->get();
		}
		else{
			$disable=Forum::get();
		}
		$this->data['pageTitle'] = 'Forum';
		$this->data['pageNote'] = 'Welcome To Our Site';
		$this->data['breadcrumb'] = 'inactive';	
		$this->data['pageMetakey'] =  CNF_METAKEY ;
		$this->data['pageMetadesc'] = CNF_METADESC ;
		$this->data['pages'] = 'forum.userforum';	
		$theme=\bsetecHelpers::get_options('theme');
		$page = 'layouts.'.$theme['theme'].'.home.index';
	
		return view($page,$this->data)->with('forum',$forum)->with('category',$category)->with('cforum',$cforum)->with('message','')->with('disable',$disable)->with('count',$count);
	}
	/* Show Categories wise Forum */
	public function getTopic($slug)
	{
		$forum=new Forum;
		$forum= Forum::join('forum_category', 'forum.category_id', '=', 'forum_category.forum_cat_id')
            ->join('users', 'forum.created_by', '=', 'users.id')
            ->select('users.*', 'forum.*', 'forum_category.name')->where('forum_category.slug','=',$slug)
            ->get();
        $count= count($forum);
        if($count==0)
        	$message='No Records';
        else
        	$message='';
        $category = Forumcategory::get();
		$this->data['pageTitle'] = 'Forum';
		$this->data['pageNote'] = 'Welcome To Our Site';
		$this->data['breadcrumb'] = 'inactive';	
		$this->data['pageMetakey'] =  CNF_METAKEY ;
		$this->data['pageMetadesc'] = CNF_METADESC ;
		$this->data['pages'] = 'forum.userforum';	
		$theme=\bsetecHelpers::get_options('theme');
		$page = 'layouts.'.$theme['theme'].'.home.index';
		$user = \Auth::user();
		$disable=Forum::where('status','=',0)->where('created_by','=',$user->id)->get();
		return view($page,$this->data)->with('forum',$forum)->with('category',$category)->with('cforum',$category)->with('message',$message)->with('disable',$disable);
	}
	/* Insert Forum Comments*/
	public function postInsert($id=false)
	{
		 $comments = Input::get('comments');
		 $user_id = Input::get('user_id');
		 $forum_id = Input::get('forum_id');
		 $updated_at=date("Y-m-d h:i:sa");
		 Forumcomments::insert(['user_id' => $user_id,'forum_id'=>$forum_id,'comment'=>$comments,'created_at'=>$updated_at]);
		 return Redirect::to('user-forum/'.$forum_id)->with('message','Successfully Added ');	
	}
	/* Update Forum Comments*/
	public function postCommentsupdate()
	{
		 $comment_id = Input::get('id');
		 $content = Input::get('content');
		 $forum_id = Input::get('forum_id');
		 $updated_at=date("Y-m-d h:i:sa");
		 Forumcomments::where('comment_id', $comment_id)->update(['comment'=>$content,'updated_at'=>$updated_at]);
		 return Redirect::to('user-forum/'.$forum_id)->with('message','Successfully Added ');	
	}
	/* Forum Comment delete */
	public function getCommentdestory($id)
	{	
		$forum= Forumcomments::where('comment_id','=',$id)->first();
		Forumcomments::where('comment_id', '=', $id)->delete();
		return Redirect::to('user-forum/'.$forum->forum_id)->with('message','Successfully Deleted ');	
	}
	/* Insert new forum */
	public function postForuminsert()
	{
		 $title = Input::get('title');
		 $description = Input::get('content');
		 $cat_id = Input::get('cat_id');
		 $slug = $this->create_slug($title); 
		 $status = Input::get('status');
		 $updated_at=date("Y-m-d h:i:sa");
		 $user_id = Input::get('user_id');
		if($status=='on')
			$status=1;
		else
			$status=0;
		$des='<p>'.$description.'</p>';
		Forum::insert(['forum_topic' => $title,'category_id'=>$cat_id,'slug'=>$slug,'forum_content'=>$des,'created_by'=>$user_id,'status'=>$status,'created_at'=>$updated_at]);
		return Redirect::to('user-forum')->with('message','Successfully Added ');
	}
	/* Forum Delete */
	public function getForumdelete($forum_id)
	{
		Forum::where('forum_id', '=', $forum_id)->delete();
		return Redirect::to('user-forum')->with('message','Successfully Deleted ');
	}
	public function create_slug($string){     
        $replace = '-';         
        $string = strtolower($string);     
		
        //replace / and . with white space     
        $string = preg_replace("/[\/\.]/", " ", $string);     
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);     
		
        //remove multiple dashes or whitespaces     
        $string = preg_replace("/[\s-]+/", " ", $string);     
		
        //convert whitespaces and underscore to $replace     
        $string = preg_replace("/[\s_]/", $replace, $string);     
		
        //limit the slug size     
        $string = substr($string, 0, 100);     
		
        //slug is generated     
        return $string; 
    }
    /* user Forum Update*/
    public function postForumupdate()
	{	
			$title = Input::get('title');
			$id = Input::get('id');
			$description = Input::get('content');
			$cat_id = Input::get('cat_id');
			$slug = $this->create_slug($title); 
			$status = Input::get('status');
			$updated_at=date("Y-m-d h:i:sa");
			$user_id = Input::get('user_id');
			if($status=='on')
				$status=1;
			else
				$status=0;			
			$des='<p>'.$description.'</p>';
			Forum::where('forum_id', $id)->update(['forum_topic' => $title,'category_id'=>$cat_id,'slug'=>$slug,'forum_content'=>$des,'created_by'=>$user_id,'status'=>$status,'updated_at'=>$updated_at]);
			return Redirect::to('user-forum')->with('message','Successfully updated ');
	}
	/* Get Particular Forum Details */	
	public function getForum($id)
	{	$chk=Forum::find($id);

		$cnt=count($chk);
		if($cnt==1)
		{
		$forum= \DB::table('forum')
            ->join('forum_category', 'forum.category_id', '=', 'forum_category.forum_cat_id')
            ->join('users', 'forum.created_by', '=', 'users.id')
            ->select('users.*', 'forum.*', 'forum_category.name')->where('forum.forum_id','=',$id)
            ->get();

        $list= \DB::table('forum_comments')
            ->join('forum', 'forum_comments.forum_id', '=', 'forum.forum_id')
            ->join('users', 'forum_comments.user_id', '=', 'users.id')
            ->select('users.*', 'forum.*', 'forum_comments.*')->where('forum_comments.forum_id','=',$id)
            ->get();

        //$category = Forumcomment::get();
        $category = Forumcategory::get();
		$this->data['pageTitle'] = 'Forum';
		$this->data['pageNote'] = 'Welcome To Our Site';
		$this->data['breadcrumb'] = 'inactive';	
		$this->data['pageMetakey'] =  CNF_METAKEY ;
		$this->data['pageMetadesc'] = CNF_METADESC ;

		$this->data['pages'] = 'forum.forumtopic';	
		$theme=\bsetecHelpers::get_options('theme');
		$page = 'layouts.'.$theme['theme'].'.home.index';
		return view($page,$this->data)->with('forum',$forum)->with('category',$category)->with('list',$list)->with('message','');
		}
		else
		{
			return Redirect::to('user-forum')->with('message','');
		}
	}
	public function getEnable($id)
	{
		$title = Input::get('status');
		$count=count($title);
		for($i=0;$i<$count;$i++)
		{
			Forum::where('forum_id', $title[$i])->update(['status' => 1]);
		}
		return Redirect::to('user-forum')->with('message','');
	}

	public function  getLang($lang='en')
	{
		\Session::put('lang', $lang);
		return  Redirect::back();
	}	

	public function  postContact( Request $request)
	{
	
		$this->beforeFilter('csrf', array('on'=>'post'));
		$rules = array(
				'name'		=>'required',
				'subject'	=>'required',
				'message'	=>'required|min:20',
				'sender'	=>'required|email'			
		);
		$validator = Validator::make(Input::all(), $rules);	
		if ($validator->passes()) 
		{
			
			$data = array('name'=>$request->input('name'),'sender'=>$request->input('sender'),'subject'=>$request->input('subject'),'notes'=>$request->input('message')); 
			$message = View::make('emails.contact', $data); 		
			
			$to 		= 	CNF_EMAIL;
			$subject 	= $request->input('subject');
			$headers  	= 'MIME-Version: 1.0' . "\r\n";
			$headers 	.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers 	.= 'From: '.$request->input('name').' <'.$request->input('sender').'>' . "\r\n";
				mail($to, $subject, $message, $headers);			

			return Redirect::to('?p='.$request->input('redirect'))->with('message', \SiteHelpers::alert('success','Thank You , Your message has been sent !'));	
				
		} else {
			return Redirect::to('?p='.$request->input('redirect'))->with('message', \SiteHelpers::alert('error','The following errors occurred'))
			->withErrors($validator)->withInput();
		}		
	}	

}
