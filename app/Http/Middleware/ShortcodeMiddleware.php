<?php namespace App\Http\Middleware;

use Closure;

class ShortcodeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

            $response = $next($request);
            $url = $request->fullUrl();
            if(strpos($url, 'advertisement'))
            {
                return $response; 
            }else{
                $count = substr_count($response,"[[");
                for($i=0;$i<$count;$i++){
                   $response = $this->Shortcode($response);
                }
                return $response;
            }
    }
    // shortcode change function 

    function Shortcode($response){
        $start = strpos($response,"[[");
        if($start!=''){
            $app_name  = CNF_APPNAME;
            $App_firstname = explode(' ', $app_name);
            $length_app = strlen($App_firstname[0]);
            $len = $length_app+23;
            $end = strpos($response,"]]");
            $advt = substr($response,$start,$len);
            if($advt!=''){
                $advtt = \bsetecHelpers::ifInstalled('advertisement');
                $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                $pos = strpos($actual_link, 'course/coursecertificate');
                $txt='';
                if ($pos === false) {    
                   $txt = \bsetecHelpers::getAdvtBlock($advt);
                } 
                if(isset($response->original)){
                    if($advtt > 0 && $txt!=''){
                        $content = str_replace($advt, $txt, $response->original);
                        $response->setContent($content);
                    }elseif($advtt==0 && $txt!=''){
                        $content = str_replace($advt, '', $response->original);
                        $response->setContent($content);
                    }
                }
               return $response; 
            }else{
               return $response;
            }
        }else{
            return $response;
        }
    }

}
