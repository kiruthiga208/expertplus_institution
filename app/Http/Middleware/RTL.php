<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class RTL
{

    /**
   * The Guard implementation.
   *
   * @var Guard
   */
/*  protected $auth;
  public function __construct(Guard $auth)
  {
    $this->auth = $auth;
  }*/
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       $response = $next($request);
        $rtl_template = 0;
        if(method_exists($response,'getOriginalContent')){
             $rtl = CNF_RTL;
             if($rtl == '1'){
                $content = str_replace('styles.css','styles_rtl.css', $response->getOriginalContent());
                $style =  str_replace('bsetec.css','bsetec_rtl.css', $content);
                $change_img = str_replace('star-half.png', 'star-halfrtl.png', $style);
                $final_style = str_replace('coursepreview.css','coursepreview_rtl.css', $change_img);
                $response->setContent($final_style);
                $rtl_template = 1;
             }
              return $response;
        }else{         
          return $response;
        }
    }
}
