<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class institutioncourse extends bsetec  {
	
	protected $table = 'institution_course';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT bse_institution_course.* FROM bse_institution_course  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE bse_institution_course.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
