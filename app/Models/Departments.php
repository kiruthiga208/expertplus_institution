<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class departments extends bsetec  {
	
	protected $table = 'departments';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT bse_departments.* FROM bse_departments  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE bse_departments.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
