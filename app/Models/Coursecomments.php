<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class coursecomments extends bsetec  {
	
	protected $table = 'comments';
	protected $primaryKey = 'comment_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."comments.*,".\bsetecHelpers::getdbprefix()."comments.user_id AS userid,
		CONCAT(".\bsetecHelpers::getdbprefix()."users.first_name, ' ', ".\bsetecHelpers::getdbprefix()."users.last_name) AS user_id,
		".\bsetecHelpers::getdbprefix()."course.course_title AS course_id FROM ".\bsetecHelpers::getdbprefix()."comments 
		LEFT JOIN ".\bsetecHelpers::getdbprefix()."users ON users.id = ".\bsetecHelpers::getdbprefix()."comments.user_id
		LEFT JOIN ".\bsetecHelpers::getdbprefix()."course ON ".\bsetecHelpers::getdbprefix()."course.course_id = ".\bsetecHelpers::getdbprefix()."comments.course_id ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."comments.comment_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
