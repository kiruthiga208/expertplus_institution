<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class clientbanner extends bsetec  {
	
	protected $table = 'client_banner';
	protected $primaryKey = 'id';

	public $timestamps = false;

	public function __construct() {
		parent::__construct();
		
	}

	protected function getDateFormat()
    {
        return 'U';
    }

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."client_banner.* FROM ".\bsetecHelpers::getdbprefix()."client_banner  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."client_banner.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
