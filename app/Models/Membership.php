<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class membership extends bsetec  {

	protected $table = 'membership_plan';
	protected $primaryKey = 'plan_id';

	public function __construct() {
		parent::__construct();

	}

	public static function querySelect(  ){

		return "  SELECT ".\bsetecHelpers::getdbprefix()."membership_plan.* FROM ".\bsetecHelpers::getdbprefix()."membership_plan  ";
	}

	public static function queryWhere(  ){

		return " WHERE ".\bsetecHelpers::getdbprefix()."membership_plan.plan_id IS NOT NULL   ";
	}

	public static function queryGroup(){
		return "  ";
	}

	public static function getMonthlyplan(){
			return \DB::table('membership_plan')->where('status', '=' , 1)->orWhere('status_y','=',1)->limit(3)->get();
			// return \DB::table('membership_plan')->limit(3)->get();
	}

	public static function getYearlyplan(){
			// return \DB::table('membership_plan')->where('status_y', '=' , 1)->limit(3)->get();
			return \DB::table('membership_plan')->limit(3)->get();
	}

	public function getmemberships($id)
    {

      	$gid = \Session::get('gid');
        return \DB::table('course')
			    ->leftJoin('membership_course', 'membership_course.course_id', '=', 'course.course_id')
			    ->where('membership_course.plan_id', '=', $id)
			    ->where('course.approved', '=', '1')
			    ->where('course.privacy','1')
			    ->where(function ($query) use ($gid) {
                      $query->where('course.protected', '=', '0')
                            ->orwhere('course.protected', '=', $gid);
                  })
                    ->orderBy('course.created_at', 'desc')
                    ->paginate($this->get_option('paginationPerpage'));

    }

    public function getCountry(){
        return \DB::table('country')->get();
    }

    public function getcourseselect( $params , $limit =null)
    {   

        $limit = explode(':',$limit);
        if(count($limit) >=3)
        {
            $table = $params[0]; 
            $condition ="WHERE `approved` = '1'".$limit[0]." `".$limit[1]."` ".$limit[2]." ".$limit[3]." "; 
            $row =  \DB::select( "SELECT * FROM ".$table." ".$condition);
        }else{
            $table = $params[0]; 
            $row =  \DB::table($table)->where('approved','=','1')->where('approved','=','1')->where('privacy','=','1')->where('pricing','!=',0)->get();
        }

        return $row;


    }

    public function deletecourse($plan_id){
    	\DB::table('membership_course')->where('plan_id','=',$plan_id)->delete();
    }

}
