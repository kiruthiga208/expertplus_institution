<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class sitesubcategory extends bsetec  {
	
	protected $table = 'expert_sub_categories';
	protected $primaryKey = 'sub_cat_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."expert_sub_categories.* FROM ".\bsetecHelpers::getdbprefix()."expert_sub_categories  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."expert_sub_categories.sub_cat_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
