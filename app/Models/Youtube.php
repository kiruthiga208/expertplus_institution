<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class youtube extends bsetec  {
	
	protected $table = 'tb_youtube';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_youtube.* FROM tb_youtube  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_youtube.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
