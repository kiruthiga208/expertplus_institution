<?php
namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
class Blogadmin extends bsetec  {
	
	protected $table = 'blogs';
	protected $primaryKey = 'blogID';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."blogs.* FROM ".\bsetecHelpers::getdbprefix()."blogs  ";
	}
	public static function queryWhere(  ){
		
		return " WHERE ".\bsetecHelpers::getdbprefix()."blogs.blogID IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
