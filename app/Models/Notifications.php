<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Notifications extends bsetec  {
	
	protected $table = 'notifications';
	protected $primaryKey = 'notification_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT notifications.* FROM notifications  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE notifications.notification_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
