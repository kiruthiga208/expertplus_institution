<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class subscriber extends bsetec  {
	
	protected $table = 'subscriber_list';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."subscriber_list.*,".\bsetecHelpers::getdbprefix()."users.username,first_name,".\bsetecHelpers::getdbprefix()."users.email,".\bsetecHelpers::getdbprefix()."user_group.group_name FROM ".\bsetecHelpers::getdbprefix()."subscriber_list left JOIN ".\bsetecHelpers::getdbprefix()."users ON ".\bsetecHelpers::getdbprefix()."subscriber_list.user_id= ".\bsetecHelpers::getdbprefix()."users.id left JOIN ".\bsetecHelpers::getdbprefix()."user_group ON ".\bsetecHelpers::getdbprefix()."subscriber_list.group_id = ".\bsetecHelpers::getdbprefix()."user_group.id";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."subscriber_list.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "GROUP BY ".\bsetecHelpers::getdbprefix()."subscriber_list.group_id";
	}
	public function editgroupuser($id)
	{
		return \DB::table('subscriber_list')
                    ->leftjoin('user_group', 'subscriber_list.group_id', '=','user_group.id')
                    ->leftjoin('users','subscriber_list.user_id','=','users.id')
                    ->select('users.first_name','subscriber_list.*','user_group.group_name')->where('subscriber_list.group_id','=',$id)
                    ->paginate(12);
	}
	public function group_delete($id){
		\DB::table('subscriber_list')->where('group_id','=',$id)->delete();
	}
	

}
