<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class ccavenue extends bsetec  {
	
	protected $table = 'transactions';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."transactions.* FROM ".\bsetecHelpers::getdbprefix()."transactions   ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."transactions.id IS NOT NULL AND `payment_method`= 'ccavenue' ";
	}
	
	public static function queryGroup(){
		return "  ";
	}


	// ccavenue payment gateway helpers
  public static function encrypt($plainText,$key)
  {
    $secretKey = Self::hextobin(md5($key));
    $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
      $openMode = @mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
      $blockSize = @mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
    $plainPad = Self::pkcs5_pad($plainText, $blockSize);
      if (@mcrypt_generic_init($openMode, $secretKey, $initVector) != -1) 
    {
          $encryptedText = @mcrypt_generic($openMode, $plainPad);
                @mcrypt_generic_deinit($openMode);
                
    } 
    return bin2hex($encryptedText);
  }

  public static function decrypt($encryptedText,$key)
  {
    $secretKey = Self::hextobin(md5($key));
    $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
    $encryptedText=Self::hextobin($encryptedText);
      $openMode = @mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
    @mcrypt_generic_init($openMode, $secretKey, $initVector);
    $decryptedText = @mdecrypt_generic($openMode, $encryptedText);
    $decryptedText = rtrim($decryptedText, "\0");
    @mcrypt_generic_deinit($openMode);
    return $decryptedText;
    
  }
  //*********** Padding Function *********************

  public static function pkcs5_pad ($plainText, $blockSize)
  {
      $pad = $blockSize - (strlen($plainText) % $blockSize);
      return $plainText . str_repeat(chr($pad), $pad);
  }

  //********** Hexadecimal to Binary function for php 4.0 version ********

   public static function hextobin($hexString) 
     { 
          $length = strlen($hexString); 
          $binString="";   
          $count=0; 
          while($count<$length) 
          {       
              $subString =substr($hexString,$count,2);           
              $packedString = pack("H*",$subString); 
              if ($count==0)
        {
        $binString=$packedString;
        } 
              
        else 
        {
        $binString.=$packedString;
        } 
              
        $count+=2; 
          } 
            return $binString; 
        }
	

}
