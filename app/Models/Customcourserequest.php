<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\Models\Transaction;
use File;

class customcourserequest extends bsetec  {

	protected $table = 'custom_course_request';
	protected $primaryKey = 'ccr_id';

	public function __construct() {
		$driver             = config('database.default');
		$database           = config('database.connections');
		parent::__construct();
	}

	public static function querySelect(  ){

		return "  SELECT ".\bsetecHelpers::getdbprefix()."custom_course_request.* FROM ".\bsetecHelpers::getdbprefix()."custom_course_request  ";
	}

	public static function queryWhere(  ){

		return "  WHERE ".\bsetecHelpers::getdbprefix()."custom_course_request.ccr_id IS NOT NULL ";
	}

	public static function queryGroup(){
		return "  ";
	}

	function getccrinfo($id='') {
        return \DB::table('custom_course_request')->where('ccr_id','=', $id)->first();
    }

	function getmeetinginfo($id='') {
        return \DB::table('online_meetings')->where('meeting_id','=', $id)->first();
    }

    function updateStatus($id,$status) {
		return \DB::table('custom_course_request')->where('ccr_id',$id)->update(['approved' => $status]);
	}

	function gettutorsbycategoryskills($uid='',$cid='',$skills='') {
		$wherequery = '';
		$skillset = json_decode($skills,true);
		if(!empty($skillset)){
			foreach ($skillset as $key => $skill) {
				$skill = '"'.$skill.'"';
				if($key){
					$query = " OR skills LIKE  '".$skill."'";
				} else {
					$query = "skills LIKE  '".$skill."'";
				}
				$wherequery .= $query;
			}
		}

		if(empty($wherequery)){
	        $result = \DB::table('users')
	        	->join('course', 'users.id', '=', 'course.user_id')
	        	->join('custom_user_settings', 'users.id', '=', 'custom_user_settings.user_id')
	        	->distinct()->select('users.email','users.username')
	        	->where('users.active', '=', '1')
	        	->where('users.id', '!=', $uid)
	        	->where('course.cat_id', '=', $cid)
	        	->get();
	   	} else {
			$result = \DB::table('users')
	        	->join('course', 'users.id', '=', 'course.user_id')
	        	->join('custom_user_settings', 'users.id', '=', 'custom_user_settings.user_id')
	        	->distinct()->select('users.email','users.username')
	        	->where('users.active', '=', '1')
	        	->where('users.id', '!=', $uid)
	        	->where(function ($query) use ($cid,$wherequery) {
	                $query->where('course.cat_id', '=', $cid)
	                      ->orwhereRaw($wherequery);
	            })
	        	->get();
	   	}
	   	return $result;
    }

    function applyRequest($id,$uid,$price,$createdOn) {
		$applied = \DB::table('custom_course_request')->select('applied_tutor_ids','tutor_count')->where('ccr_id','=', $id)->first();
		if(isset($applied->applied_tutor_ids) && !empty($applied->applied_tutor_ids)){
			$applied_tutor_ids = $applied->applied_tutor_ids.','.$uid;
			$tutor_count = $applied->tutor_count + 1;
		} else {
			$applied_tutor_ids = $uid;
			$tutor_count = 1;
		}
		\DB::table('custom_course_request')->where('ccr_id',$id)->update(['applied_tutor_ids' => $applied_tutor_ids,'tutor_count' => $tutor_count]);
		return \DB::table('custom_course_request_applications')->insert([
		    ['ccr_id' => $id, 'user_id' => $uid, 'price' => $price, 'createdOn' => $createdOn]
		]);
	}

	
	function applied_id($id,$uid) {

		 $res =  \DB::table('custom_course_request_applications')->select('application_id')->where('ccr_id','=', $id)->where('user_id','=', $uid)->first();

		 return isset($res->application_id)?$res->application_id:0;
	}

	function checkApplied($id,$uid) {
		// echo \DB::table('custom_course_request_applications')->where('ccr_id','=', $id)->where('user_id','=', $uid)->toSql();
		// echo $id; echo "<br>"; echo $uid;
		// exit;
        return \DB::table('custom_course_request_applications')->select('application_id')->where('ccr_id','=', $id)->where('user_id','=', $uid)->first();
    }

	function getApplied($id='') {
        return \DB::table('custom_course_request_applications')->where('ccr_id','=', $id)->get();
    }

	function getAwarded($id='',$uid='') {
        return \DB::table('custom_course_request_applications')->where('ccr_id','=', $id)->where('user_id','=', $uid)->first();
    }

    function getAwardStatus($id,$user_id,$credited_user_id) {
        $credits =  \DB::table('user_details')->where('user_details.user_id', '=', $user_id)->first();
        if($credits)
        {
          	$total_credits = $credits->total_credits;
	    	$ccra = \DB::table('custom_course_request_applications')->where('application_id','=', $id)->first();
	        $price = $ccra->price;
        	if($total_credits >= $price){
        		//debited
        		$currentcredits=$total_credits-$price;
        		$credits =  \DB::table('user_details')->where('user_details.user_id', '=', $user_id)->update(['total_credits'=>$currentcredits]);
        		//credited
        		$credited_account = \DB::table('user_details')->where('user_details.user_id','=',$credited_user_id)->first();
        		if(count($credited_account)){
    				$addedcredits = $credited_account->total_credits + $price;
        			\DB::table('user_details')->where('user_id','=',$credited_user_id)->update(['total_credits'=>$addedcredits]);
        		}else{
        			\DB::table('user_details')->insert(['user_id'=>$credited_user_id,'total_credits'=>$price]);
        		}
        		return true;
        	}
        }

        return false;
	}

    function getapplinfo($id) {
    	return \DB::table('custom_course_request_applications')->where('application_id','=', $id)->first();
	}

    function awardRequest($id,$payment_type,$user_id) {
    	\DB::table('custom_course_request_applications')->where('application_id','=', $id)->increment('application_status');
    	$data = \DB::table('custom_course_request_applications')->where('application_id','=', $id)->first();
		\DB::table('custom_course_request')->where('ccr_id',$data->ccr_id)->update(['ccr_status' => '1','tutor_id' => $data->user_id,'final_price' => $data->price]);

		$transaction = new Transaction();
		$transaction['user_id'] = $user_id;
		$transaction['course_id'] = 0;
		$transaction['status'] = 'pending';
		$transaction['payment_method'] = 'credits';
		$transaction['type'] = 'custom_course_request';

		if($payment_type != 'hour'){
			//calculate the credits
			$transaction['amount'] = floatval($data->price);
			$transaction['commission'] = '0.00';
			$transaction['amount_to_user'] = '0.00';
		} else {
			$transaction['amount'] = '0.00';
			$transaction['commission'] = '0.00';
			$transaction['amount_to_user'] = '0.00';
		}
		$transaction->save();
		$transaction_id = $transaction->id;
		\DB::table('custom_course_request_applications')->where('application_id',$id)->update(['transaction_id' => $transaction->id]);

		if($payment_type != 'hour'){
			$admin = \DB::table('options')->where('code', '=', 'commision_to_admin_id')->first();
          	$admin_user_id = $admin->option;
			// \DB::table('user_credits')->insertGetId($cdata);
		}

		return $data->ccr_id;
	}

    function startCourse($id) {
    	\DB::table('custom_course_request')->where('ccr_id',$id)->update(['meeting_status' => '1']);
	}

    function cancelCourse($data) {
    	$ccr_id = $data['ccr_id'];
    	$dispute_id = \DB::table('custom_course_request_dispute')->insertGetId($data);
    	return \DB::table('custom_course_request')->where('ccr_id',$ccr_id)->update(['ccr_status' => '4', 'dispute_id' => $dispute_id]);
	}

    function getDispute($id) {
		return \DB::table('custom_course_request_dispute')->where('dispute_id','=', $id)->first();
	}

    function messageStore($id,$messageid) {
		$messages = \DB::table('custom_course_request_applications')->select('message')->where('application_id','=', $id)->first();
		if(isset($messages->message) && !empty($messages->message)){
			$message = $messages->message.','.$messageid;
		} else {
			$message = $messageid;
		}
		\DB::table('custom_course_request_applications')->where('application_id',$id)->update(['message' => $message]);
	}

    function messageList($id) {
    	return \DB::table('custom_course_request_applications')->select('message')->where('application_id','=', $id)->first();
		$res = \DB::table('custom_course_request_applications')->select('message')->where('application_id','=', $id)->toSql();
		var_dump($id); echo "<br>";
		var_dump($res); exit();
	}

    function getApplicationId($id,$user_id) {
    	return \DB::table('custom_course_request_applications')->select('application_id')->where('ccr_id','=', $id)->first();
		// return \DB::table('custom_course_request_applications')->select('application_id')->where('ccr_id','=', $id)->where('user_id','=', $user_id)->first();
	}

    function messageSingle($id) {
		return \DB::table('message')->where('id','=', $id)->first();
	}

	public function check_invite_userexists($cid='',$text='') {
		return \DB::table('users')
		->join('invitation_users', 'users.id', '=', 'invitation_users.user_id')
		->where('users.email','=', $text)
		->where('invitation_users.course_id','=', $cid)
		->get();
	}

	public function insert_inviter_users($cid='',$text='') {
		$loggeduser     = \Session::get('uid');
		$emails = \DB::table('users')->where('active', '=' , '1')->where('email', '=', $text)->where('id', '!=', $loggeduser)->get();

		if(count($emails)>0){
			$userid                          = $emails['0']->id;
			$dataarray                       = array();
			$now_date                        = date("Y-m-d H:i:s");
			$dataarray['course_id']          = $cid;
			$dataarray['user_id']            = $userid;
			$dataarray['created_at']         = $now_date;
			$dataarray['modified_at']        = $now_date;
			\DB::table('invitation_users')->insertGetId($dataarray);
			return $emails;
		}
	}

	public function proposalprice($dispute_id='',$price='',$type='') {
		if($type == '1')
			\DB::table('custom_course_request_dispute')->where('dispute_id',$dispute_id)->update(['price_proposed_by_student' => $price]);
		else if($type == '2')
			\DB::table('custom_course_request_dispute')->where('dispute_id',$dispute_id)->update(['price_proposed_by_tutor' => $price]);
		else if($type == '3')
			\DB::table('custom_course_request_dispute')->where('dispute_id',$dispute_id)->update(['price_proposed_by_admin' => $price]);
	}

    public function update_shared_course($cid,$ccr_id) {
    	\DB::table('custom_course_request')->where('ccr_id',$ccr_id)->update(['shared_course_id' => $cid]);
	}

	public function get_course_info($id='') {
        return \DB::table('course')->where('course_id','=', $id)->first();
    }

    function completerequestccr($id) {
    	\DB::table('custom_course_request')->where('ccr_id',$id)->update(['request_completion' => '1']);
	}

    function completeccr($id,$amount,$return,$user_id) {
    	if($return != 0){
			$admin = \DB::table('options')->where('code', '=', 'commision_to_admin_id')->first();
          	$admin_user_id = $admin->option;
			\DB::table('user_details')->where('user_id',$admin_user_id)->decrement('total_credits', $return);
    		\DB::table('user_details')->where('user_id',$user_id)->increment('total_credits', $return);

    		$data = \DB::table('custom_course_request_applications')->where('ccr_id',$id)->where('application_status','1')->first();
    		if(isset($data->transaction_id)){
	    		$transaction = Transaction::find($data->transaction_id);
				$transaction['amount'] = $amount;
				$transaction['commission'] = $amount-$return;
				$transaction['amount_to_user'] = $return;
				$transaction['status'] = 'completed';
				$transaction->save();
	    	}
    	}
    	\DB::table('custom_course_request')->where('ccr_id',$id)->update(['ccr_status' => '3']);
    	\DB::table('custom_course_request_applications')->where('ccr_id',$id)->where('application_status','1')->update(['application_status' => '2','send_price' => $amount]);
	}

	public function postDocdelete($lid,$rid){

		$resfiles = \DB::table('course_files')->where('id', '=', $rid)->get();

		if(!empty($resfiles)){
			// foreach($resfiles as $resfile) {
			// 	File::Delete('./uploads/files/'.$resfile->file_name.'.'.$resfile->file_extension);
			// }
			// \DB::table('course_files')->where('id', '=', $rid)->delete();

			$ccr = \DB::table('custom_course_request')->where('ccr_id', '=', $lid)->get();

			if(!empty($ccr) && (isset($ccr['0']->file_ids))){
				$resources = json_decode($ccr['0']->file_ids,true);
				if(($key = array_search($rid, $resources)) !== false) {
					unset($resources[$key]);
				}
				$resources = array_values($resources);
				$file_ids = json_encode($resources);
				\DB::table('custom_course_request')->where('ccr_id',$lid)->update(['file_ids' => $file_ids]);
			}
		}
	}

	public function getfiledata($id='') {
        return \DB::table('course_files')->where('id', '=', $id)->first();
    }

	public function createmeeting($data,$ccr_id) {
        $meet_id = \DB::table('online_meetings')->insertGetId($data);
        if(!empty($meet_id)){
        	\DB::table('custom_course_request')->where('ccr_id',$ccr_id)->update(['meeting_id' => $meet_id]);
        	return true;
        } else {
        	return false;
        }
    }

    public static function userCredits($user_id)
	{
	  $credits =  \DB::table('user_details')
	                ->where('user_details.user_id', '=', $user_id)
	                ->first();
	    if($credits)
	    {
	      return $credits->total_credits;
	    }
	    else
	    {
	      return '0';
	    }
	}

	public function updatemeeting($uid,$minfo,$qinfo,$type,$guid='') {
		$duration = $minfo->meeting_duration;
		$online_users = $minfo->online_users;
		$id = $minfo->meeting_id;

		// Online Users
		if($type != 'log'){
			$onlineusers = array();
			if(!empty($online_users)){
				$onlineusers = json_decode($online_users,true);
			}
			if($type == 'join' || $type == 'start'){
				array_push($onlineusers, $uid);
			} else if($type == 'end' || $type == 'close'){
				if(($key = array_search($uid, $onlineusers)) !== false) {
				    unset($onlineusers[$key]);
				}
			}
			$online_users = json_encode($onlineusers);
			\DB::table('online_meetings')->where('meeting_id',$id)->update(['online_users' => $online_users]);
		}

		// Deduct credits from Student
		if($qinfo->payment_type == 'hour' && ($type == 'log' || $type == 'start')){

			$deduct_credit = false;
			// check duration exceeds an hour
			if(!empty($minfo->meeting_duration)){	// if duration is not empty
				$time_spent = 0;
				$durations = json_decode($minfo->meeting_duration,true);
				foreach ($durations as $key => $duration_value) {
					if(isset($duration_value['end'])){
						$time_spent += strtotime($duration_value['end']) - strtotime($duration_value['start']);
					}
				}
				if($time_spent != 0){
					$hours_spent = $time_spent / (60 * 60);
					if($hours_spent >= $qinfo->deducted_hours){
						$deduct_credit = true;
					}
				}
			} else {
				$deduct_credit = true;
			}

			if($deduct_credit){
			// check user have enough credits
				$user_credits = $this->userCredits($qinfo->entry_by);
				if($user_credits > $qinfo->final_price){

					if(!empty($duration)){
						$meet_duration = json_decode($duration,true);
						$time_spent = 0;
						foreach ($meet_duration as $key => $duration_value) {
							if(isset($duration_value['end'])){
								$time_spent += strtotime($duration_value['end']) - strtotime($duration_value['start']);
							}
						}

						if($time_spent != 0){
							$round_off = $time_spent - ($qinfo->deducted_hours * 3600);
							$count = count($meet_duration);
							$meet_duration[$count]['end'] = date("Y-m-d H:i:s", (strtotime($meet_duration[$count]['end'])-$round_off));
							$meeting_duration = json_encode($meet_duration);
							\DB::table('online_meetings')->where('meeting_id',$id)->update(['meeting_status' => '0', 'meeting_duration' => $meeting_duration]);
						}
					}

					\DB::table('user_details')->where('user_id',$qinfo->entry_by)->decrement('total_credits', $qinfo->final_price);
					$admin = \DB::table('options')->where('code', '=', 'commision_to_admin_id')->first();
		          	$admin_user_id = $admin->option;
					\DB::table('user_details')->where('user_id',$admin_user_id)->increment('total_credits', $qinfo->final_price);
					$deducted_hours = $qinfo->deducted_hours + 1;
					\DB::table('custom_course_request')->where('ccr_id',$qinfo->ccr_id)->update(['deducted_hours' => $deducted_hours]);

				} else {
                    return false;
                }
			}
		}

		if($type == 'join'){

			\DB::table('online_meetings')->where('meeting_id',$id)->update(['meeting_status' => '1']);

		} else if($type == 'start'){

			if(!empty($duration)){
				$meet_duration = json_decode($duration,true);
				$count = count($meet_duration);
				$meet_duration[$count+1]['start'] = date("Y-m-d H:i:s");
				$meeting_duration = json_encode($meet_duration);
			} else {
				$now_date = date("Y-m-d H:i:s");
				$duration = array('1'=>array('start'=>$now_date));
				$meeting_duration = json_encode($duration);
			}

			\DB::table('online_meetings')->where('meeting_id',$id)->update(['meeting_status' => '2', 'meeting_duration' => $meeting_duration]);

		} else if($type == 'log'){

			$logdata = $minfo->meeting_logdata;
			$meeting_status = 2;
			if(!empty($guid) && !empty($logdata)){
				$log_data = json_decode($logdata,true);
				if(count($log_data) > 4){
					if($log_data['0'] == $log_data['1'] && $log_data['1'] == $log_data['2'] && $log_data['2'] == $log_data['3'] && $log_data['3'] == $log_data['4'] && $log_data['4'] == $guid){
						$meeting_status = 1;
					}
					if($meeting_status == 2){
						array_shift($log_data);
					}
				}
				if($meeting_status == 2){
					array_push($log_data, $guid);
				// } else if($meeting_status == 1){
				}
				$logdata = json_encode($log_data);
			} else if(!empty($guid) && empty($logdata)){
				$logdata = json_encode(array($guid));
			}

			if(!empty($duration)){
				$meet_duration = json_decode($duration,true);
				$count = count($meet_duration);
				if($meeting_status == 2){
					$meet_duration[$count]['end'] = date("Y-m-d H:i:s");
				} else if($meeting_status == 1){
					$meet_duration[$count]['end'] = date("Y-m-d H:i:s", time() - 25);
				}
				$meeting_duration = json_encode($meet_duration);
			}

			\DB::table('online_meetings')->where('meeting_id',$id)->update(['meeting_status' => $meeting_status, 'meeting_duration' => $meeting_duration, 'meeting_logdata' => $logdata]);

		} else if($type == 'end'){

			if(!empty($duration)){
				$meet_duration = json_decode($duration,true);
				$count = count($meet_duration);
				$meet_duration[$count]['end'] = date("Y-m-d H:i:s");
				$meeting_duration = json_encode($meet_duration);
			}
			\DB::table('online_meetings')->where('meeting_id',$id)->update(['meeting_status' => '1', 'meeting_duration' => $meeting_duration]);

		} else if($type == 'close'){

			\DB::table('online_meetings')->where('meeting_id',$id)->update(['meeting_status' => '0']);

		}

		return true;
    }

	public function paymentDetails($uid,$page,$take) {
		// echo $uid;exit;
		$skip = ($page-1) * $take;

		$result = \DB::table('custom_course_request')
			->where('ccr_status','!=','0')
            ->where(function ($query) use ($uid) {
	            $query->where('entry_by','=', $uid)->orwhere(function ($query2) use ($uid) {
	                $query2->where('tutor_id','=', $uid);
	            });
            })
			->orderBy('ccr_id', 'desc')
			->skip($skip)->take($take)
			->get();

		$count = \DB::table('custom_course_request')
			->where('ccr_status','!=','0')
            ->where(function ($query) use ($uid) {
	            $query->where('entry_by','=', $uid)
	            ->orwhere(function ($query2) use ($uid) {
	                $query2->where('tutor_id','=', $uid);
	            });
            })
			->count();
		return  array('result' => $result,'count' => $count );
	}

    public function finishdispute($id,$amount,$return,$user_id,$dispute_id,$price) {
    	if($return != 0){
			$admin = \DB::table('options')->where('code', '=', 'commision_to_admin_id')->first();
          	$admin_user_id = $admin->option;
			\DB::table('user_details')->where('user_id',$admin_user_id)->decrement('total_credits', $return);
    		\DB::table('user_details')->where('user_id',$user_id)->increment('total_credits', $return);

    		$data = \DB::table('custom_course_request_applications')->where('ccr_id',$id)->where('application_status','1')->first();
    		if(isset($data->transaction_id)){
	    		$transaction = Transaction::find($data->transaction_id);
				$transaction['amount'] = $amount;
				$transaction['commission'] = $amount-$return;
				$transaction['amount_to_user'] = $return;
				$transaction['status'] = 'completed';
				$transaction->save();
	    	}
    	}
    	\DB::table('custom_course_request')->where('ccr_id',$id)->update(['ccr_status' => '5','refund_price' => $price]);
    	\DB::table('custom_course_request_dispute')->where('dispute_id',$dispute_id)->update(['dispute_status' => '1']);
    	\DB::table('custom_course_request_applications')->where('ccr_id',$id)->where('application_status','1')->update(['application_status' => '2','send_price' => $amount]);
	}

}
