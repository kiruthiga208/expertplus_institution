<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class courselanguage extends bsetec  {
	
	protected $table = 'language';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."language.* FROM ".\bsetecHelpers::getdbprefix()."language  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."language.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
