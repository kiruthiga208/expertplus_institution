<?php
/**
 * Company : Bsetec
 * Models : Course
 * Email : support@bsetec.com
 */
namespace App\Models;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Featured extends bsetec
{
	public function __construct() {
		parent::__construct();

	}

	public function getallfeatured()
	{    	
		return \DB::table('course')->leftJoin('users', 'users.id', '=', 'course.user_id')
		->leftJoin('comments', 'comments.user_id', '=', 'course.user_id')
		->leftJoin('favorite', 'favorite.user_id', '=', 'course.user_id')
		->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
		->select('course.*')
		->where('course.is_featured', '=', '1')->where('course.approved','=','1')->where('course.curriculum','!=', '')->groupBy('course.course_id')->orderBy('course.created_at', 'desc')
		->paginate(\Config::get('site.course_items_per_page'));

	}

	public function getfreecourses()
	{   
		return \DB::table('course')->leftJoin('users', 'users.id', '=', 'course.user_id')
		->leftJoin('comments', 'comments.user_id', '=', 'course.user_id')
		->leftJoin('favorite', 'favorite.user_id', '=', 'course.user_id')
		->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
		->select('course.*')
		->where('course.pricing', '=', '0')->where('course.approved','=','1')->where('course.curriculum','!=', '')->groupBy('course.course_id')->orderBy('course.created_at', 'desc')
		->paginate(\Config::get('site.course_items_per_page'));

	}

	public function gettopratedcourses()
	{   

		return \DB::table('course')->leftJoin('users', 'users.id', '=', 'course.user_id')
		->leftJoin('comments', 'comments.user_id', '=', 'course.user_id')
		->leftJoin('favorite', 'favorite.user_id', '=', 'course.user_id')
		->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
		->select('course.*')
		->where('course.rating_count', '!=', '0')->where('course.approved','=','1')->where('course.curriculum','!=', '')->groupBy('course.course_id')->orderBy('course.rating_count', 'desc')
		->paginate(\Config::get('site.course_items_per_page'));

	}

	public function gettopfreecourses()
	{   

		return \DB::table('course')->leftJoin('users', 'users.id', '=', 'course.user_id')
		->leftJoin('comments', 'comments.user_id', '=', 'course.user_id')
		->leftJoin('favorite', 'favorite.user_id', '=', 'course.user_id')
		->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
		->select('course.*')
		->where('course.approved', '=', '1')->where('course.curriculum','!=', '')->where('course.pricing', '=', 0)->where('course.rating_count', '!=', '0')->groupBy('course.course_id')->orderBy('course.pricing', 'desc')->orderBy('course.created_at', 'desc')
		->paginate(\Config::get('site.course_items_per_page'));

	}

	public function gettoppaidcourses()
	{   

		return \DB::table('course')->leftJoin('users', 'users.id', '=', 'course.user_id')
		->leftJoin('comments', 'comments.user_id', '=', 'course.user_id')
		->leftJoin('favorite', 'favorite.user_id', '=', 'course.user_id')
		->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
		->select('course.*')
		->where('course.approved', '=', '1')->where('course.curriculum','!=', '')->where('course.pricing', '!=', '0')->groupBy('course.course_id')->orderBy('course.pricing', 'desc')->orderBy('course.created_at', 'desc')
		->paginate(\Config::get('site.course_items_per_page'));

	}

	public function getmostviewedcourses()
	{
		$views = \DB::table('hits')->select( \DB::raw('COUNT(course_id) as counting'),'course_id')->orderBy('counting', 'desc')->groupBy('course_id')->get();
        foreach ($views as $key => $value) {
            $view[] = $value->course_id;
        }
        return \DB::table('course')->leftJoin('users', 'users.id', '=', 'course.user_id')
		->leftJoin('comments', 'comments.user_id', '=', 'course.user_id')
		->leftJoin('favorite', 'favorite.user_id', '=', 'course.user_id')
		->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
		->select('course.*')
		->whereIn('course.course_id', $view)->where('course.approved', '=', '1')->where('course.curriculum','!=', '')->groupBy('course.course_id')->orderBy('course.created_at', 'desc')
		->paginate(\Config::get('site.course_items_per_page'));

	}

}
