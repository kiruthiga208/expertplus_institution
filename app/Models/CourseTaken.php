<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseTaken extends Model {

	protected $table 		= 'course_taken';
	protected $primaryKey 	= 'taken_id';

}
