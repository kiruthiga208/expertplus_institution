<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;

class transaction extends bsetec  {
	
	protected $table = 'transactions';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return " SELECT ".\bsetecHelpers::getdbprefix()."transactions.*,".\bsetecHelpers::getdbprefix()."course.course_title,CONCAT( ".\bsetecHelpers::getdbprefix()."users.first_name,' ',".\bsetecHelpers::getdbprefix()."users.last_name) as username  FROM ".\bsetecHelpers::getdbprefix()."transactions  LEFT JOIN ".\bsetecHelpers::getdbprefix()."users ON ".\bsetecHelpers::getdbprefix()."transactions.user_id = ".\bsetecHelpers::getdbprefix()."users.id  LEFT JOIN ".\bsetecHelpers::getdbprefix()."course ON ".\bsetecHelpers::getdbprefix()."transactions.course_id = ".\bsetecHelpers::getdbprefix()."course.course_id ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."transactions.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}

	public function user()
    {
        return $this->belongsTo('User');
    }

    public function course()
    {
        return $this->belongsTo('Course', 'course_id');
    }

    public function get_transaction_detail($id)
    {
        // echo $id;exit;
        return \DB::table('transactions')
            ->select('transactions.id', 
                    'transactions.status',
                    'transactions.amount',
                    'transactions.payment_method', 
                    DB::raw('CONCAT('.\bsetecHelpers::getdbprefix().'users.first_name, " ", '.\bsetecHelpers::getdbprefix().'users.last_name) AS customer_name'), 
                    'users.email', 'course.course_title')
            ->join('course', 'course.course_id', '=', 'transactions.course_id')
            ->join('users', 'users.id', '=', 'transactions.user_id')
            ->where('transactions.id','=', $id)
            ->first();
    }

    
    public function save_invoice($invoice)
    {
       
        $old_invoice = \DB::table('invoice')
        ->where('invoice.transaction_id','=', $invoice['transaction_id'])
        ->first();

        $old_invoice = (array) $old_invoice;

        if($old_invoice)
        {
            $id = $old_invoice['id'];
            unset($old_invoice['id']);
            \DB::table('invoice')->where('transaction_id',$old_invoice['transaction_id'])->update($old_invoice);
            
        }
        else
        {
            $id =  \DB::table('invoice')->insertGetId($invoice);
        }
        return $id;
    }
	

}
