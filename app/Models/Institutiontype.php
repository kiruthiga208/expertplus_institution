<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class institutiontype extends bsetec  {
	
	protected $table = 'institution_types';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT BSE_institution_types.* FROM BSE_institution_types  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE BSE_institution_types.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
