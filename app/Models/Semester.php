<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class semester extends bsetec  {
	
	protected $table = 'institution_semester';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT bse_institution_semester.* FROM bse_institution_semester  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE bse_institution_semester.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
