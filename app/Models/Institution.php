<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class institution extends bsetec  {
	
	protected $table = 'institution';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT BSE_institution.* FROM BSE_institution  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE BSE_institution.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
