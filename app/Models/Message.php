<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class message extends bsetec  {
	
	protected $table = 'message';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."message.* FROM ".\bsetecHelpers::getdbprefix()."message  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."message.id IS NOT NULL ";
	}

	public static function queryInboxWhere( $uid ){ // NOT IN DRAFT, NOT IN TRASH, NOT DELETED, RECEIVED BY USER
	return "  WHERE ".\bsetecHelpers::getdbprefix()."message.id IS NOT NULL AND ".\bsetecHelpers::getdbprefix()."message.draft IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.trashed_by_recipient IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.deleted_by_recipient IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.recipient = '$uid' AND ".\bsetecHelpers::getdbprefix()."message.label_by_recipient IS NULL AND ".\bsetecHelpers::getdbprefix()."message.label_by_sender IS NULL";
	// return "  WHERE ".\bsetecHelpers::getdbprefix()."message.id IS NOT NULL AND ".\bsetecHelpers::getdbprefix()."message.draft IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.trashed_by_recipient IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.deleted_by_recipient IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.recipient = '$uid'";
	}

	public static function queryStarredWhere( $uid ){ // NOT IN DRAFT, NOT IN TRASH, NOT DELETED, RECEIVED/SENT BY USER, IN STARRED
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."message.id IS NOT NULL AND ".\bsetecHelpers::getdbprefix()."message.draft IN ( 0 ) AND ( (".\bsetecHelpers::getdbprefix()."message.trashed_by_recipient IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.deleted_by_recipient IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.recipient = '$uid'  AND ".\bsetecHelpers::getdbprefix()."message.starred_by_recipient IN ( 1 )) OR (".\bsetecHelpers::getdbprefix()."message.trashed_by_sender IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.deleted_by_sender IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.entry_by = '$uid'  AND ".\bsetecHelpers::getdbprefix()."message.starred_by_sender IN ( 1 )) )AND ".\bsetecHelpers::getdbprefix()."message.label_by_recipient IS NULL AND ".\bsetecHelpers::getdbprefix()."message.label_by_sender IS NULL";
	}

	public static function querySentWhere( $uid ){ // NOT IN DRAFT, NOT IN TRASH, NOT DELETED, SENT BY USER
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."message.id IS NOT NULL AND ".\bsetecHelpers::getdbprefix()."message.draft IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.trashed_by_sender IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.deleted_by_sender IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.entry_by = '$uid'AND ".\bsetecHelpers::getdbprefix()."message.label_by_recipient IS NULL AND ".\bsetecHelpers::getdbprefix()."message.label_by_sender IS NULL";
	}

	public static function queryDraftWhere( $uid ){  // IN DRAFT, NOT IN TRASH, NOT DELETED, DRAFT BY USER
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."message.id IS NOT NULL AND ".\bsetecHelpers::getdbprefix()."message.draft IN ( 1 ) AND ".\bsetecHelpers::getdbprefix()."message.trashed_by_sender IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.deleted_by_sender IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.entry_by = '$uid'AND ".\bsetecHelpers::getdbprefix()."message.label_by_recipient IS NULL AND ".\bsetecHelpers::getdbprefix()."message.label_by_sender IS NULL";
	}

	public static function queryTrashWhere( $uid ){	// TRASH INCLUDES BOTH SENT AND INBOX MESSAGES, EXCLUDES DELETED
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."message.id IS NOT NULL AND (".\bsetecHelpers::getdbprefix()."message.entry_by = '$uid' OR ".\bsetecHelpers::getdbprefix()."message.recipient = '$uid') AND ( (".\bsetecHelpers::getdbprefix()."message.trashed_by_recipient IN ( 1 ) AND ".\bsetecHelpers::getdbprefix()."message.deleted_by_recipient IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.recipient = '$uid') OR (".\bsetecHelpers::getdbprefix()."message.trashed_by_sender IN ( 1 ) AND ".\bsetecHelpers::getdbprefix()."message.deleted_by_sender IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.entry_by = '$uid') )";
	}

	public static function queryLabelWhere( $uid, $label ){	// NOT IN TRASH, NOT DELETED, RECEIVED/SENT BY USER, LABEL INCLUDES BOTH SENT AND INBOX MESSAGES
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."message.id IS NOT NULL AND (".\bsetecHelpers::getdbprefix()."message.entry_by = '$uid' OR ".\bsetecHelpers::getdbprefix()."message.recipient = '$uid') AND ( (".\bsetecHelpers::getdbprefix()."message.trashed_by_recipient IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.deleted_by_recipient IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.label_by_recipient = '$label' AND ".\bsetecHelpers::getdbprefix()."message.recipient = '$uid') OR (".\bsetecHelpers::getdbprefix()."message.trashed_by_sender IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.deleted_by_sender IN ( 0 ) AND ".\bsetecHelpers::getdbprefix()."message.label_by_sender = '$label' AND ".\bsetecHelpers::getdbprefix()."message.entry_by = '$uid') )";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	
	public static function getMessageUsers( $uid ){
		return \DB::select("SELECT id,username,first_name,last_name,email,avatar FROM ".\bsetecHelpers::getdbprefix()."users");
	}
	
	public static function getLabels( $uid ){
		return \DB::select("SELECT id,name FROM ".\bsetecHelpers::getdbprefix()."message_labels WHERE ".\bsetecHelpers::getdbprefix()."message_labels.entry_by IN ( $uid )");
	}
	
	public static function getMessageRows( $args, $type, $label, $uid )
	{
       $table = with(new static)->table;
	   $key = with(new static)->primaryKey;
	   
        extract( array_merge( array(
			'page' 		=> '0' ,
			'limit'  	=> '0' ,
			'sort' 		=> '' ,
			'order' 	=> '' ,
			'params' 	=> '' ,
			'global'	=> 1	  
        ), $args ));
		
		$offset = ($page-1) * $limit ;	
		$limitConditional = ($page !=0 && $limit !=0) ? "LIMIT  $offset , $limit" : '';	
		$orderConditional = ($sort !='' && $order !='') ?  " ORDER BY {$sort} {$order} " : '';

		// Update permission global / own access new ver 1.1
		$table = with(new static)->table;
		//if($global == 0 )
			//	$params .= " AND {$table}.entry_by ='".\Session::get('uid')."'"; 	
		// End Update permission global / own access new ver 1.1
		
		if($type == '0'){
			$whereQuery = self::queryInboxWhere($uid);
		}else if($type == '1'){
			$whereQuery = self::queryStarredWhere($uid);
		} else if($type == '2'){
			$whereQuery = self::querySentWhere($uid);
		} else if($type == '3'){
			$whereQuery = self::queryDraftWhere($uid);
		} else if($type == '4'){
			$whereQuery = self::queryTrashWhere($uid);
		} else if($type == '5' && $label != '0'){
			$whereQuery = self::queryLabelWhere($uid,$label);
		}
        
		$rows = array();
	    $result = \DB::select( self::querySelect() . " {$whereQuery} " . " {$params} ". self::queryGroup() ." {$orderConditional}  {$limitConditional} ");
		
		if($key =='' ) { $key ='*'; } else { $key = $table.".".$key ; }	
		$counter_select = preg_replace( '/[\s]*SELECT(.*)FROM/Usi', 'SELECT count('.\bsetecHelpers::getdbprefix().''.$key.') as total FROM', self::querySelect() ); 	
		//echo 	$counter_select; exit; 
		$res = \DB::select( $counter_select . " {$whereQuery} " . " {$params} ". self::queryGroup());
		$total = $res[0]->total;
		
		$totalType = array();
		
		$whereQuery0 = self::queryInboxWhere($uid);
		$res0 = \DB::select( $counter_select . " {$whereQuery0} " . " {$params} ". self::queryGroup());
		$totalType[] = $res0[0]->total;
		
		$whereQuery1 = self::queryStarredWhere($uid);
		$res1 = \DB::select( $counter_select . " {$whereQuery1} " . " {$params} ". self::queryGroup());
		$totalType[] = $res1[0]->total;
		
		$whereQuery2 = self::querySentWhere($uid);
		$res2 = \DB::select( $counter_select . " {$whereQuery2} " . " {$params} ". self::queryGroup());
		$totalType[] = $res2[0]->total;
		
		$whereQuery3 = self::queryDraftWhere($uid);
		$res3 = \DB::select( $counter_select . " {$whereQuery3} " . " {$params} ". self::queryGroup());
		$totalType[] = $res3[0]->total;
		
		$whereQuery4 = self::queryTrashWhere($uid);
		$res4 = \DB::select( $counter_select . " {$whereQuery4} " . " {$params} ". self::queryGroup());
		$totalType[] = $res4[0]->total;


		return $results = array('rows'=> $result , 'total' => $total, 'totalType' => $totalType);	

	
	}
	
	public static function postStar($id,$starred,$type){
		
		if($type == 0){
			\DB::table('message')->where('id', $id)->update(['starred_by_recipient' => $starred]);
		} else if($type == 2){
			\DB::table('message')->where('id', $id)->update(['starred_by_sender' => $starred]);
		}		
		
	}
	
	public static function postRead($id){
		
		\DB::table('message')->where('id', $id)->update(['read' => '1']);
		
	}
	
	public static function postTrash($id,$isSender){
		
		if($isSender == 0){
			\DB::table('message')->where('id', $id)->update(['trashed_by_recipient' => '1']);
		} else if($isSender == 1){
			\DB::table('message')->where('id', $id)->update(['trashed_by_sender' => '1','trashed_by_recipient' => '1']);
		}		
		
	}
	
	public static function postDeleteTrash($id,$isSender){
		
		if($isSender == 0){
			\DB::table('message')->where('id', $id)->update(['deleted_by_recipient' => '1']);
		} else if($isSender == 1){
			\DB::table('message')->where('id', $id)->update(['deleted_by_sender' => '1']);
		}		
		
	}
	
	public  function insertLabelRow($data,$id){
	
       $table = 'message_labels';
	   $key = 'id';
	    if($id == NULL )
        {
			 $id = \DB::table( $table)->insertGetId($data);
            
        } else {
            // Update here 
			// update created field if any
			if(isset($data['createdOn'])) unset($data['createdOn']);	
			if(isset($data['updatedOn'])) $data['updatedOn'] = date("Y-m-d H:i:s");			
			 \DB::table($table)->where($key,$id)->update($data);    
        }    
        return $id;    
	}	

	public static function postApplyLabel($id,$isSender,$label){
		
		if($isSender == 0){
			\DB::table('message')->where('id', $id)->update(['label_by_recipient' => $label]);
		} else if($isSender == 1){
			\DB::table('message')->where('id', $id)->update(['label_by_sender' => $label]);
		}		
		
	}

	public static function postRemoveLabel($id,$isSender){
		
		if($isSender == 0){
			\DB::table('message')->where('id', $id)->update(['label_by_recipient' => NULL]);
		} else if($isSender == 1){
			\DB::table('message')->where('id', $id)->update(['label_by_sender' => NULL]);
		}		
		
	}

	public static function postDeleteLabel($id){
		
		\DB::table('message_labels')->where('id', '=', $id)->delete();
		
	}
}
