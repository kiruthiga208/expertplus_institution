<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class instructors extends bsetec  {
	
	protected $table = 'instructors';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "SELECT bse_instructors.*, "
		.\bsetecHelpers::getdbprefix()."users.username as Username, "
		.\bsetecHelpers::getdbprefix()."users.email, "
		.\bsetecHelpers::getdbprefix()."users.first_name, "
		.\bsetecHelpers::getdbprefix()."users.last_name, "
		.\bsetecHelpers::getdbprefix()."institution.name as Institution
		 FROM bse_instructors LEFT JOIN bse_users ON ".\bsetecHelpers::getdbprefix()."users.id = ".\bsetecHelpers::getdbprefix()."instructors.user_id LEFT JOIN bse_institution ON ".\bsetecHelpers::getdbprefix()."institution.id = ".\bsetecHelpers::getdbprefix()."instructors.institution_id";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE bse_instructors.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
