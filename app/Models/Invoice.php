<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class invoice extends bsetec  {
	
	protected $table = 'invoice';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."invoice.* FROM ".\bsetecHelpers::getdbprefix()."invoice  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."invoice.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	
	public function coursetaken($coursename,$email)
	{
		return \DB::table('course_taken')
		->leftJoin('users','users.id','=','course_taken.user_id')
		->leftJoin('course','course.course_id','=','course_taken.course_id')
		->where('users.email','=',$email)->where('course.course_title','=',$coursename)->get();
	}

	public function getuserid($email)
	{
		return \DB::table('users')->where('email','=',$email)->select('id')->first();
	}

	public function getcourseid($name){
		return \DB::table('course')->where('course_title','=',$name)->select('course_id')->first();
	}

}
