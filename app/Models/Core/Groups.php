<?php namespace App\Models\Core;

use App\Models\bsetec;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Groups extends bsetec  {
	
	protected $table = 'groups';
	protected $primaryKey = 'group_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return " SELECT  
		".\bsetecHelpers::getdbprefix()."groups.group_id,
		".\bsetecHelpers::getdbprefix()."groups.name,
		".\bsetecHelpers::getdbprefix()."groups.description,
		".\bsetecHelpers::getdbprefix()."groups.level
		FROM ".\bsetecHelpers::getdbprefix()."groups  ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."groups.group_id IS NOT NULL    ";
	}
	
	public static function queryGroup(){
		return "    ";
	}
	

}
