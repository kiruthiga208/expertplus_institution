<?php namespace App\Models\Core;

use App\Models\bsetec;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Users extends bsetec  {
	
	protected $table = 'users';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return " SELECT  ".\bsetecHelpers::getdbprefix()."users.*,  ".\bsetecHelpers::getdbprefix()."groups.name 
FROM ".\bsetecHelpers::getdbprefix()."users LEFT JOIN ".\bsetecHelpers::getdbprefix()."groups ON ".\bsetecHelpers::getdbprefix()."groups.group_id = ".\bsetecHelpers::getdbprefix()."users.group_id ";
	}	

	public static function queryWhere(  ){
		
		return "    WHERE ".\bsetecHelpers::getdbprefix()."users.id !=''   ";
	}
	
	public static function queryGroup(){
		return "      ";
	}

	function getwebinarlogin($user_id){
		$users=\DB::table('course_online')
		->join('course','course.course_id' , '=' , 'course_online.course_id' )
		->join('citrix','citrix.user_id' , '=' , 'course.user_id' )->select('course_online.registrantkey','course.webinarkey','citrix.access_token','citrix.organizer_key')->where('course_online.user_id',$user_id)->get();
		$registrantkeys=array();
		// echo "<pre>";print_r($users);exit();
		if(!empty($users)){
			$i=0;
			foreach ($users as $key ){
				 $registrantkeys[]=array(
				 	'registrantkey'=>$key->registrantkey,
				 	'webinarkey'=>$key->webinarkey,
				 	'access_token'=>$key->access_token,
				 	'organizer_key'=>$key->organizer_key);
				 ++$i;
			}
		}
		return $registrantkeys;
	}
	
	function deletewebinerlogin($user_id){
		 \DB::table('course_online')->where('user_id','=',$user_id)->delete();
	}

}
