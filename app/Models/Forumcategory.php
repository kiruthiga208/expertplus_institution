<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class forumcategory extends bsetec  {
	
	protected $table = 'forum_category';
	protected $primaryKey = 'forum_cat_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."forum_category.* FROM ".\bsetecHelpers::getdbprefix()."forum_category  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."forum_category.forum_cat_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
