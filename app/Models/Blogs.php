<?php
/**
 * @author Bsetec <support@bsetec.com>
 */

class Blogs extends Eloquent
{
    protected $table = 'blogs';
    protected $primaryKey = 'blog_id';

    public function categories()
    {
        return $this->belongsTo('Blogcategories','category_id');
    }

    public function blog()
    {
        return $this->belongsTo('Blogcomment','blog_id');
    }

    public function user()
    {
        return $this->belongsTo('User','created_by');
    }

    public function comments()
    {
       return $this->hasMany('Blogcomment', 'blog_id');
    }
}
