<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class coupon extends bsetec  {
	
	protected $table = 'coupon';
	protected $primaryKey = 'id';

	public $timestamps = false;
			
	public function __construct() {
		parent::__construct();
	}

	protected function getDateFormat()
    {
        return 'U';
    }
    
	public static function querySelect(  ){
		
		return " SELECT ".\bsetecHelpers::getdbprefix()."coupon.*,".\bsetecHelpers::getdbprefix()."course.course_title,CONCAT( ".\bsetecHelpers::getdbprefix()."users.first_name,' ',".\bsetecHelpers::getdbprefix()."users.last_name) as username FROM ".\bsetecHelpers::getdbprefix()."coupon LEFT JOIN ".\bsetecHelpers::getdbprefix()."users ON ".\bsetecHelpers::getdbprefix()."coupon.user_id = ".\bsetecHelpers::getdbprefix()."users.id  LEFT JOIN ".\bsetecHelpers::getdbprefix()."course ON ".\bsetecHelpers::getdbprefix()."coupon.course_id = ".\bsetecHelpers::getdbprefix()."course.course_id ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."coupon.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
}
