<?php
/**
 * Company : Bsetec
 * Models : Search
 * Email : support@bsetec.com
 */
namespace App\Models;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
class Search extends bsetec
{
    public function __construct() {
        parent::__construct();
        
    }

    public function searchcourses($search, $type)
    {             
        if($type == 'keyword')
        {
            $search = strtoupper($search);
            $extends = explode(' ', $search);
            $result = \DB::table('course')
                    ->leftJoin('users', 'users.id', '=', 'course.user_id')
                    // ->leftJoin('comments', 'comments.user_id', '=', 'course.user_id')
                    // ->leftJoin('favorite', 'favorite.user_id', '=', 'course.user_id')
                    ->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
                    ->select('course.*')
                    ->where('course.privacy','=', '1')
                    ->where('course.deleted_at', '=', NULL)
                    ->where('course.approved','=','1')
                    ->where('course.course_title', 'LIKE', '%' . $search . '%')
                    ->orWhere('course.keywords', 'LIKE', '%' . $search . '%')
                    ->where('course.approved','=','1')
                    ->orWhere('course.subtitle', '=', $search)
                    ->where('course.approved','=','1')
                    ->orderBy('course.created_at', 'desc')
                    ->orWhere('course.description', 'LIKE', '%' . $search . '%')
                    ->where('course.privacy','=', '1')
                    ->where('course.approved','=','1')
                    ->paginate($this->get_option('paginationPerpage'));
                    // ->groupBy('course.course_id');

            // foreach ($extends as $extend) {
            //     $images->orWhere('course.keywords', 'LIKE', '%' . $extend . '%')
            //         ->orWhere('course.course_title', 'LIKE', '%' . $search . '%')
            //         ->orWhere('course.description', 'LIKE', '%' . $search . '%');
            // }
            // $result = $images->paginate($this->get_option('paginationPerpage'));
        }
        elseif($type == 'price')
        {
            $result = \DB::table('course')
                        ->whereBetween('pricing', array($search['price_from'],$search['price_to']))
                        ->where('course.deleted_at', '=', NULL)->where('course.privacy','=', '1')->where('course.approved','=','1')->orderBy('course.created_at', 'desc')
                        ->paginate($this->get_option('paginationPerpage'));
        }
        elseif($type == 'level')
        {
            $result = \DB::table('course')
                        ->where('course.course_level', '=', $search['level'])
                        ->where('course.deleted_at', '=', NULL)->where('course.privacy','=', '1')->where('course.approved','=','1')->orderBy('course.created_at', 'desc')
                        ->paginate($this->get_option('paginationPerpage'));
        }
        return $result;
    }
  
}
