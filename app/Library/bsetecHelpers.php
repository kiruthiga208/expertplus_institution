<?php 
use App\Http\Controllers\CommonmailController;
class bsetecHelpers {


  public static function get_zip_content($name , $zip ) {  

    $contents = '';  

    if( file_exists( $zip )){  

      $fp = fopen('zip://' . $zip . '#'. $name , 'r');  
      if (!$fp) {  
        SiteHelpers::alert("error","cannot open zip file: {$zip}n");  
      }  
      while (!feof($fp)) {  
        $contents .= fread($fp, 2);  
      } 
      
    } else {  
      SiteHelpers::alert("error","cannot find zip file: {$zip}n");  
    }  

    fclose($fp); 
    return $contents;  

  }    

  public static function cf_unpackage($zip_path) { 

    $ain = unserialize( base64_decode( self::get_zip_content ( '.ain' , $zip_path )));  

    $tmp_name  = md5( date('YmdHis') ); 

    $zip = new ZipArchive; 
    $zipres = $zip->open( $zip_path ); 
    if( $zipres === TRUE){ 

       $app_path = str_replace('/protected/app','',app_path()); //dirname( base_path()) .'/uploads/zip/'. $tmp_name; ''
     //exit;

       if( ! is_dir( $app_path )){
        mkdir( $app_path ); 
      }
      $zip->extractTo( $app_path ); 
      $zip->close(); 

      $sql_str = file_get_contents( $app_path .'/.mysql'); 
      
      /* CHANGE START HERE: */
      
      preg_match_all( '/[\s]*(CREATE|DROP|TRUNCATE|ALTER)(.*);/Usi',$sql_str, $sql_table );
      
      preg_match_all( '/[\s]*(INSERT|UPDATE|DELETE|ALTER)(.*)[\s\)]+;/Usi',$sql_str, $sql_row );

      $mysql_pass = true;

      try
      {
        foreach ( $sql_table[0] as $k => $sql ){
          $res = DB::statement( $sql );
        }
      }
      catch(Exception $e) { 
        //throw $e; 
        $mysql_pass = false;
      }
      
      try
      {

        foreach ( $sql_row[0] as $k => $sql ){
          $res = DB::statement( $sql );
        }
      }
      catch(Exception $e) { 
        //throw $e; 
        $mysql_pass = false;
        
      }
      
      /* CHANGE END HERE */
      
      $setting_str = file_get_contents( $app_path .'/.setting'); 
      $_setting = unserialize( base64_decode( $setting_str )); 
      $prefix = self::getdbprefix();
      self::store_setting( $_setting['module'], 'module','module_name','module_id'); 

      $_tmpfile = array( '.ain','.mysql','.setting'); 
      foreach ( $_tmpfile as $_file ){ 
        unlink( $app_path.'/'. $_file ); 
      } 

      if( $mysql_pass ){
        $data['status'] = '1'; 
        $data['error'] = 0;
      } else {
        $data['status'] = 1;
        $data['sql_error'] = 'mysql parsing error, please install mysql manually';
      }

    } else { 

      $data['status'] = 0; 
      $data['error'] = "unzip error"; 

    }

    return $data;
  } 
  
  public static function store_setting( $rows , $table_name , $where_field , $pk_field ) {
    if( !is_array( $rows )){
      $rows = array( $rows );
    }
    foreach ( $rows as $k => $row ){
      $prefix = self::getdbprefix();
      $table_namee = $prefix.$table_name;
      $r = DB::select(" select * from {$table_namee} where {$where_field}= '{$row->$where_field}' ");
      if( count( $r )){
        // update
        unset( $row->{$pk_field} );
        DB::table( $table_name )
        ->where( $where_field , $row->$where_field )
        ->update( (array) $row );
        
      } else {
        // insert
        $_id = DB::table( $table_name )
        ->insertGetId( (array) $row );
          $add_data = array();
          $add_data['module_id'] = $_id;
          $add_data['status'] =1;
          DB::table('additional_module')->insertGetId($add_data);
          $group_data = array();
          $group_data['group_id'] =1;
          $group_data['module_id'] = $_id;
          $group_data['access_data'] = '{"is_global":"1","is_view":"1","is_detail":"1","is_add":"1","is_edit":"1","is_remove":"1","is_excel":"1"}';
          DB::table('groups_access')->insertGetId($group_data);
      }
      
      //trace($this->db->last_query(),'lq');
    }
    
  }

  public static function write_route(){
    $prefix = self::getdbprefix();
    $table_name = $prefix.'module';
    $rows = DB::select("select module_name from {$table_name} where module_type = 'addon' ");

    $str = "<?php\n";
    foreach ( $rows as $k => $row ){
      $str .= "Route::controller('{$row->module_name}', '".studly_case($row->module_name )."Controller');\n";
    }
    $str .= "?>";
    
    file_put_contents( app_path().'/moduleroutes.php', $str );

  }
  
  static function tf_read ( $a ){
    return base64_decode( $a  );
  }

  static function tf_write ( $a ){
    return base64_encode( $a );
  }

  public static function CourseCount($id)
  {
    $course = DB::table('course')
    ->where('cat_id', '=', $id)
    ->where('approved', '=', '1')
    ->where('privacy','1')
    ->count();
    
    return $course;
  }

  public static function zoomCrop($image, $width = 200, $height = 150, $type = NULL, $library = 'gd')
  {
    if (!file_exists($image)) {
      $image = url('static/img/users.png');
    }
    if ($library == 'imagick') {
      if ($type != NULL) {
        return ImageMagic::open($image)->cropThumbnailImage($width, $height)->$type(90);
      } else {
        return ImageMagic::open($image)->cropThumbnailImage($width, $height)->png(90);
      }
    }
    if ($type != NULL) {
      return Image::open($image)->zoomCrop($width, $height);
    }
    return Image::open($image)->zoomCrop($width, $height);
  }

  public static  function checkFavorite($id)
  {
    if (Auth::check() == FALSE) {
      return FALSE;
    }

    if (DB::table('favorite')->where('course_id', '=', DB::raw($id))->where('user_id', '=', DB::raw(Auth::user()->id))->count() == 1) {
      return TRUE;
    }
    return FALSE;
  }

  public static function avatar($avatar)
  { 
    if($avatar!='')
      return asset('uploads/avatar/' . $avatar);
    else
      return asset('static/img/users.png');

  }

  public static function siteSettings($request)
  {
    $request = DB::table('sitesettings')->where('option', '=', $request)->first();
    return $request->value;
  }

  public static function unreadNotifications()
  {
    if (Auth::check() == FALSE){
      return '';
    }

    return DB::table('notification')->where('notification.user_id', '=', Auth::user()->id)->where('notification.is_read', '=', '0')
    ->leftJoin('users', 'users.id', '=', 'notification.user_id')
    ->leftJoin('course', 'course.user_id', '=', 'notification.user_id')
    ->orderBy('notification.created_at', 'desc')->paginate(10);
  }

  public static function numberOfNotifications()
  {
    if (Auth::check() == FALSE){
      return '';
    }

    return DB::table('notification')->where('user_id', '=', Auth::user()->id)->where('is_read', '=', '0')->count();
  }

  public static function getUserCredit(){}

  public static function completedPercentage($id)
  {

    if (Auth::check() == FALSE) {
      return FALSE;
    }

    $val = 0;

    $Completed_per = \DB::table('course_taken')->where('course_id', '=', $id)->where('user_id','=',Auth::user()->id)->get();
    foreach ($Completed_per as $value) {
      $val = $value->completed;
    }
    return $val;
  }

  public static function checkReview($id,$u_id=false)
  {
    if($u_id){

      $review = \DB::table('ratings')->where('course_id', '=', \DB::raw($id))->where('user_id', '=', $u_id)->limit(1)->get();

      if(count($review) > 0 ){
        foreach ($review as $review)
        {
          $count = $review->rating;
        }
        return $count;
      }else{
       return 0; 
     }
   }else{
    $review = \DB::table('ratings')->where('course_id', '=', $id)->get();
    $count = count($review);
    if($count > 0){
      $total = \DB::table('ratings')->where('course_id', '=', $id)->sum('rating');
      return $result = $total / $count; 
    }else{
      return 0;
    }

  }

}

public static function checkPurchase($id)
{
  if (Auth::check() == false) {
    return false;
  }
  //$count =  \DB::table('transaction')->where('user_id', '=', Auth::user()->id)->where('course_id', '=', $id)->count();
  $count =  \DB::table('transactions')->where('user_id', '=', Auth::user()->id)->where('course_id', '=', $id)->where('status','completed')->where('purchase_type','=','course')->count();
  if ($count >= 1) {
    return true;
  }
  return false;
}

public static function lecturedetails($id='')
{
  return \DB::table('curriculum_lectures_quiz')->where('section_id', '=', $id)->where('publish', '=', '1')->orderBy('sort_order', 'asc')->get(); 
}



public static function siteLanguage($id='')
{
  if($id!=''){
    return DB::table('language')->where('id',$id)->first();
  }else{
    return DB::table('language')->where('status','enable')->get();  
  }
}

public static function getuserinfobyid($id='')
{
  return \DB::table('users')->where('id', '=', $id)->first(); 
}
public static function getbloginfobyid($id='')
{
  return \DB::table('blogs')->where('blogID', '=', $id)->first(); 
}

 
  
  public static function HumanFileSize($size,$unit="") {
    if( (!$unit && $size >= 1<<30) || $unit == "GB")
      return number_format($size/(1<<30),2)." GB";
    if( (!$unit && $size >= 1<<20) || $unit == "MB")
      return number_format($size/(1<<20),2)." MB";
    if( (!$unit && $size >= 1<<10) || $unit == "KB")
      return number_format($size/(1<<10),2)." KB";
      return number_format($size)." bytes";
  }

  public static function recentBlogs() {
   $blogs = \DB::table('blogs')
            ->leftJoin('blogcomments' , 'blogs.blogID', '=', 'blogcomments.blogID')
            ->select('blogs.*',\DB::raw('count('.\bsetecHelpers::getdbprefix().'blogcomments.blogID) as comments '))
            ->where('status','=','publish')
            ->groupBy('blogs.blogID')
            ->orderBy('blogs.created', 'desc')
            ->take(2)
            ->get();
   return $blogs;
 }
 
 public static function getdbprefix() {
	 	$driver             = config('database.default');
        $database           = config('database.connections');
		// return $database[$driver]['prefix']; 
    return \DB::getTablePrefix();
 }

 public static function getToprated(){
    return \DB::table('course')
              ->leftJoin('users', 'users.id', '=', 'course.user_id')
              ->join('ratings','ratings.course_id','=','course.course_id')
              ->select('course.*',\DB::raw('count('.\bsetecHelpers::getdbprefix().'ratings.rating) as ratings_count'),\DB::raw('sum('.\bsetecHelpers::getdbprefix().'ratings.rating) as ratings'))
              ->where('course.approved', '=', '1')
              ->where('course.privacy', '=', '1')
              ->groupBy('ratings.course_id')
              ->orderBy('ratings', 'desc')
              ->take(2)
              ->get();
 }

 public static function getClientAds(){
    $clients = \DB::table('client_banner')
              ->where('banner_status','=','1')
              ->orderBy('banner_order', 'asc')
              ->get();

    if($clients == NULL)
        $clients = array('0'=>(object)array('banner_title'=>'No Image','banner_image'=>'client-banner-noimage.png'));

  return $clients;          
 }

/*
* @process - get image with image table
* @return  - image file else default image
* @parm - image_id  
*/
public static function getImage($id,$type='medium')
{
  $image = \DB::table('course_images')->whereId($id)->first();

  $path = url('course/image');
  if($image != NULL)
  {
    if(!file_exists('./uploads/course/'.$image->id.'.'.$image->image_type))
    {
      $return_image = $path.'/default';
    }
    else
    {
      $size = $type == 'normal' ? '' : '_'.$type;
      $return_image = $path.'/'.$image->image_hash.'/'.$size;
    }
  }
  else
  {
    $return_image = $path.'/default';
  }

  return $return_image;
}

public static function getImageUser($id,$type='medium')
{
  $path = url('user/image');
  if($id)
  {
      $image = \DB::table('course_images')->whereId($id)->first();
      
      if($image != NULL)
      {
        if(!file_exists('./uploads/users/'.$image->id.'.'.$image->image_type))
        {
          $return_image = $path.'/default';
        }
        else
        {
          $size = $type == 'normal' ? '' : $type;
          $return_image = $path.'/'.$image->image_hash.'/'.$size;
        }
      }
      else
      {
        $return_image = $path.'/default';
      }
  }
  else
  {
    $return_image = $path.'/default';
  }
  
  return $return_image;
}

public static function getUserCredits()
{
  $credits =  \DB::table('user_details')
                ->where('user_details.user_id', '=', \Session::get('uid'))
                ->first();
    if($credits)
    {
      return $credits->total_credits;
    }
    else
    {
      return false;
    }
}

public static function CourseCountPrice($search)
  {
    $search = explode('&', $search);
    $price_from = str_replace('price_from=', '', $search[0]);
    $price_to = str_replace('price_to=', '', $search[1]);
    // echo '<pre>';print_r($search);exit;
    $price = \DB::table('course')
                        ->whereBetween('pricing', array($price_from,$price_to))
                        ->where('course.deleted_at', '=', NULL)->where('course.approved','=','1')->orderBy('course.created_at', 'desc')
                        ->where('privacy',1)
                        ->count();
    
    return $price;
  }
   public static function CourseCountLevel($search)
  {
    $search = explode('=',$search);
    $level = \DB::table('course')
                        ->where('course.course_level', '=', $search[1])
                        ->where('course.deleted_at', '=', NULL)
                        ->where('course.approved','=','1')
                        ->where('privacy','1')
                        ->orderBy('course.created_at', 'desc')
                        ->count();   
    return $level;
  }

  public static function studentscount($id='')
  {
      return \DB::table('course_taken')
                  ->where('course_taken.course_id', '=',$id)
                  ->count();
  }

   public static function getusername(){
  $user = \DB::table('users')->select('username')->whereId(\Session::get('uid'))->first();
  return $user->username;
}

	public static function get_options($code = '')
	{
		$options = \DB::table('options')->where('code', '=', $code)->get();
    
    
		$return	= array();
		foreach($options as $results)
		{
			$return[$results->option_key]	= $results->option;
		}
  
		return $return;	
	}
		public static function getBlogimage($id)
		{
		$image = \DB::table('blogs')->where('blogID','=',$id)->first();
		if($image->blog_image != NULL){
		if(!file_exists('./uploads/blog/'.$image->blog_image)){
		$return_image = 'blog-800.gif';
		}else{
		$return_image = $image->blog_image;
		}
		}else{
		$return_image = 'blog-800.gif'; 
		}
		return $return_image;
		}

  public static function checkcoupon($cid='')
  {
      $coupons = \DB::table('coupon')->where('course_id',$cid)->where('coupon_status', 1)->get();
      return (count($coupons) > 0) ? true : false; 
  }

// Advertisement Block
  public static function getAdvtBlock($code){
    if(\Schema::hasTable('advertisement'))
    {
      $module = self::ifInstalled('advertisement');
        $script = \DB::table('advertisement')->where('code',$code)->where('status','1')->first();
        if($script){
          $rtn = '<div class="advt" style="margin: 0 auto;width: 600px;">'.$script->script.'</div>';
          return $rtn;
        }else{
          return ' ';
        }
    }else{
      return '';
    }
  }


  public static function getModuleName($module_id){
     $module = \DB::table('module')->where('module_id',$module_id)->first();
     return $module->module_name;
  }


  public static function getModuledeDescription($module_id)
  {
    $module = \DB::table('module')->where('module_id',$module_id)->first();
    return $module->module_note;

  }

    public static function sitecategories($id='')
  {
    if($id!=''){
      return \DB::table('categories')->where('id',$id)->first();
    }else{
      // return Cache::remember('siteCategoriescache', 999, function()
      // {
          return \DB::table('categories')->where('status','enable')->orderBy(DB::raw('name'))->get();
      // });
    }

  }
  public static function allcategories()
  {
   return  \DB::table('categories')->select('name','id')->where('status','=','enable')->get();
   
  }


public static function sitesubcategories($cid='',$sid='')
  {
    $table=Schema::hasTable('expert_sub_categories');
    if($table=='expert_sub_categories')
    {
    if($sid!=''){
      return \DB::table('expert_sub_categories')->where('status','enable')->where('cat_id',$cid)->first();
    }else{
      return \DB::table('expert_sub_categories')->where('status','enable')->where('cat_id',$cid)->orderBy(DB::raw('sub_cat_id'))->get();
    }
    } 

}
  public static function timeago($ptime)
  {
    $etime = time() - $ptime;
    if ($etime < 1) {
      return '0 seconds';
    }
    $a = array( 365 * 24 * 60 * 60  =>  'year',
           30 * 24 * 60 * 60  =>  'month',
              24 * 60 * 60  =>  'day',
                 60 * 60  =>  'hour',
                  60  =>  'minute',
                   1  =>  'second'
          );
    $a_plural = array( 'year'   => 'years',
               'month'  => 'months',
               'day'    => 'days',
               'hour'   => 'hours',
               'minute' => 'minutes',
               'second' => 'seconds'
          );
    foreach ($a as $secs => $str) {
      $d = $etime / $secs;
      if ($d >= 1)
      {
        $r = round($d);
        return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
      }
    }
  }

  // If Module Status enable / disable
  public static function ifInstalled($module){
      $module = ($module=='cod')?'bank':$module;
      return \DB::table('module')
              ->where('module_name',$module)
              ->count();
    
  }
	
  //gamifications
  public static function getUserPoints($all = 0)
  {
    $options = \DB::table('options')->where('code', '=','gamification')->get();
    $gamification = array();
    foreach($options as $results)
    {
      $gamification[$results->option_key] = $results->option;
    }
    if(!isset($gamification['Status']) || $gamification['Status'] != 'true'){
      return false;
    }

    $user_points =  \DB::table('gamification_userpoints')
                  ->where('gamification_userpoints.user_id', '=', \Session::get('uid'))
                  ->first();

      if($user_points)
      {
        if($all)
          return $user_points;
        else
          return $user_points->points_balance;
      }
      else
      {
        return '10';  // 10 is the default point
      }
  }

  public static function getCourseName($id)
  {
    $result =  \DB::table('course')
                  ->select('course_title')
                  ->where('course.course_id', '=', $id)
                  ->first();
    if($result)
    {
      return $result->course_title;
    }else{
      return '-';
    }
  }

  public static function getLectureName($id)
  {
    $result =  \DB::table('curriculum_lectures_quiz')
                  ->select('title')
                  ->where('curriculum_lectures_quiz.lecture_quiz_id', '=', $id)
                  ->first();
    if($result)
    {
      return $result->title;
    }else{
      return '-';
    }
  }

  public static function getStreakName($id)
  {
    $result =  \DB::table('gamification_streaks')
                  ->select('title')
                  ->where('gamification_streaks.streak_id', '=', $id)
                  ->first();
    if($result)
    {
      return $result->title;
    }else{
      return '-';
    }
  }

  public static function getUserLevel()
  {
    $options = \DB::table('options')->where('code', '=','gamification')->get();
    $gamification = array();
    foreach($options as $results)
    {
      $gamification[$results->option_key] = $results->option;
    }
    if(!isset($gamification['Status']) || $gamification['Status'] != 'true'){
      return false;
    }

    $user_points =  \DB::table('gamification_userpoints')
                  ->where('gamification_userpoints.user_id', '=', \Session::get('uid'))
                  ->first();

      if($user_points) {
          $total_points = $user_points->total_points;
      } else {
          $total_points = '10';
      }

      $leveldata = \DB::table('gamification_levels')->where('reach_points', '<=', $total_points)->orderBy('reach_points', 'desc')->first();
      if($leveldata) {
        return $leveldata;
      } else {
        return false;
      }

  }

  public static function updateGamificationStreak($user_id)
  {
    $streak_days = \bsetecHelpers::getUserStreak();

    if($streak_days){ // if streak days is not zero
      $streak_data = \DB::table('gamification_streaks')->where('days', '>=', $streak_days)->orderBy('days', 'asc')->first();
      if($streak_data){ // if next streak is found
        $next_streak_days = $streak_data->days;
        if($streak_days >= $next_streak_days){ // if user reaches the next streak
          $streak_id = $streak_data->streak_id;
          $streak_points = $streak_data->points;
          $action_id = 10;
          $gamstreak = \DB::table('gamification')->where('action_id', '=',$action_id)->where('streak_id', '=',$streak_id)->where('user_id', '=',$user_id)->whereRaw('Date(created_at) = CURDATE()')->first();
          if(empty($gamstreak)){ // if the next streak not added yet for same day
            $data['user_id'] = $user_id;
            $data['streak_id'] = $streak_id;
            $data['action_id'] = $action_id;
            $data['points'] = $streak_points;
            $data['created_at'] = date("Y-m-d H:i:s");
            \DB::table('gamification')->insert($data);
          }
        }
      }
    }
  }

  public static function getGamificationSignupPoints($user_id)
  {
    $options = \DB::table('options')->where('code', '=','gamification')->get();
    $gamification = array();
    foreach($options as $results)
    {
      $gamification[$results->option_key] = $results->option;
    }
    return $gamification['signup_points'];
  }

  public static function updateGamificationSignup($user_id)
  {
    $options = \DB::table('options')->where('code', '=','gamification')->get();
    $gamification = array();
    foreach($options as $results)
    {
      $gamification[$results->option_key] = $results->option;
    }
    $points = $gamification['signup_points'];
    if(!isset($gamification['Status']) || $gamification['Status'] != 'true'){   // check if gamification enabled
      $updata['user_id'] = $user_id;
      $updata['total_points'] = $points; // with default 10 points
      $updata['points_balance'] = $points; // with default 10 points
      $updata['points_spent'] = 0;
      \DB::table('gamification_userpoints')->insert($updata);
      return false;
    } else {
      $action_id = 11;
      $data['user_id'] = $user_id;
      $data['action_id'] = $action_id;
      $data['points'] = $points;
      $data['created_at'] = date("Y-m-d H:i:s");
      \DB::table('gamification')->insert($data);

      $updata['user_id'] = $user_id;
      $updata['total_points'] = $points; // with default 10 points
      $updata['points_balance'] = $points; // with default 10 points
      $updata['points_spent'] = 0;
      \DB::table('gamification_userpoints')->insert($updata);

      \bsetecHelpers::updateGamificationStreak($user_id);
      return true;
    }

  }

  public static function updateGamificationCourseReview($user_id,$course_id)
  {
    $options = \DB::table('options')->where('code', '=','gamification')->get();
    $gamification = array();
    foreach($options as $results)
    {
      $gamification[$results->option_key] = $results->option;
    }
    $d_course = DB::table('course')->where('course_id', '=', $course_id)->first();
    if($d_course && $d_course->gamification == '0'){
      $gamification['Status'] = 'false';
    }
    if(!isset($gamification['Status']) || $gamification['Status'] != 'true'){   // check if gamification enabled
      return false;
    }

    $type = 'free';
    $action_id = 9;
    $gam = \DB::table('gamification')->where('action_id', '=',$action_id)->where('course_id', '=',$course_id)->where('user_id', '=',$user_id)->first();
    if(empty($gam)){

      $pointdata = \DB::table('gamification_actions')->select('paid_course_points','free_course_points')->where('action_id', '=', $action_id)->where('status', '=', '0')->first();
      if(!empty($pointdata)){   // check if gamification action enabled
        $free_course = DB::table('course')->where('course_id', '=', $course_id)->where('pricing', '=', '0')->count();
        if(!$free_course){
          $type = 'paid';
          $points = $pointdata->paid_course_points;
        } else {
          $type = 'free';
          $points = $pointdata->free_course_points;
        }

        if($type == 'paid' || ($type == 'free' && isset($gamification['apply_for_free_course']) && $gamification['apply_for_free_course'] == 'true')){  // check if paid or free enabled

          $data['user_id'] = $user_id;
          $data['course_id'] = $course_id;
          $data['action_id'] = $action_id;
          $data['points'] = $points;
          $data['created_at'] = date("Y-m-d H:i:s");
          \DB::table('gamification')->insert($data);

          $tpoints = \DB::table('gamification_userpoints')->select('total_points')->where('user_id', '=', $user_id)->first();
          if(isset($tpoints->total_points)){
            $total_points = $tpoints->total_points;
            $leveldata = \DB::table('gamification_levels')->where('reach_points', '>=', $total_points)->orderBy('reach_points', 'asc')->first();
            if($leveldata) {
              $reach_points = $leveldata->reach_points;
              $added_total = $total_points+$points;
              if($total_points < $reach_points && $reach_points >= $added_total){ // user has leveled up
                $sendmail = new CommonmailController();
                $userinfo = \DB::table('users')->where('id', '=', $user_id)->first();

                $image      = \URL::to('uploads/images/'.$leveldata->image);
                $level      = $leveldata->title;
                $alink      = \URL::to('/'.$userinfo->username);
                $toMail     = Auth::user()->email;
                $fromMail   = CNF_EMAIL;
                $subject    = CNF_APPNAME." Level Up";
                $mdata       = array('username'=>$userinfo->username,'level' => $level,'image' => $image,'alink' => $alink,'points' => $points);
                $tempname   = 'mailtemplates.levelup';
                $sendmail->getMail($fromMail,$toMail,$subject,$mdata,$tempname);
              }

            }
          }

          \DB::table('gamification_userpoints')->where('user_id', '=', $user_id)->increment('total_points', $points);
          \DB::table('gamification_userpoints')->where('user_id', '=', $user_id)->increment('points_balance', $points);
        }
      }
    }

    \bsetecHelpers::updateGamificationStreak($user_id);

  }

  public static function updateGamificationCourseDiscussion($user_id,$course_id,$lecture_id)
  {
    $options = \DB::table('options')->where('code', '=','gamification')->get();
    $gamification = array();
    foreach($options as $results)
    {
      $gamification[$results->option_key] = $results->option;
    }
    $d_course = DB::table('course')->where('course_id', '=', $course_id)->first();
    if($d_course && $d_course->gamification == '0'){
      $gamification['Status'] = 'false';
    }
    if(!isset($gamification['Status']) || $gamification['Status'] != 'true'){   // check if gamification enabled
      return false;
    }

    $type = 'free';
    $action_id = 8;
    $gam = \DB::table('gamification')->where('action_id', '=',$action_id)->where('course_id', '=',$course_id)->where('lecture_id', '=',$lecture_id)->where('user_id', '=',$user_id)->first();

    if(empty($gam)){

      $pointdata = \DB::table('gamification_actions')->select('paid_course_points','free_course_points')->where('action_id', '=', $action_id)->where('status', '=', '0')->first();
      if(!empty($pointdata)){   // check if gamification action enabled
        $free_course = DB::table('course')->where('course_id', '=', $course_id)->where('pricing', '=', '0')->count();
        if(!$free_course){
          $type = 'paid';
          $points = $pointdata->paid_course_points;
        } else {
          $type = 'free';
          $points = $pointdata->free_course_points;
        }
        if($type == 'paid' || ($type == 'free' && isset($gamification['apply_for_free_course']) && $gamification['apply_for_free_course'] == 'true')){  // check if paid or free enabled

          $data['user_id'] = $user_id;
          $data['course_id'] = $course_id;
          $data['action_id'] = $action_id;
          $data['lecture_id'] = $lecture_id;
          $data['points'] = $points;
          $data['created_at'] = date("Y-m-d H:i:s");
          \DB::table('gamification')->insert($data);

          $tpoints = \DB::table('gamification_userpoints')->select('total_points')->where('user_id', '=', $user_id)->first();

          if(isset($tpoints->total_points)){
            $total_points = $tpoints->total_points;

            $leveldata = \DB::table('gamification_levels')->where('reach_points', '>=', $total_points)->orderBy('reach_points', 'asc')->first();
            if($leveldata) {
              $reach_points = $leveldata->reach_points;
              $added_total = $total_points+$points;
              if($total_points < $reach_points && $reach_points >= $added_total){ // user has leveled up
                $sendmail = new CommonmailController();
                $userinfo = \DB::table('users')->where('id', '=', $user_id)->first();

                $image      = \URL::to('uploads/images/'.$leveldata->image);
                $level      = $leveldata->title;
                $alink      = \URL::to('/'.$userinfo->username);
                $toMail     = Auth::user()->email;
                $fromMail   = CNF_EMAIL;
                $subject    = CNF_APPNAME." Level Up";
                $mdata       = array('username'=>$userinfo->username,'level' => $level,'image' => $image,'alink' => $alink,'points' => $points);
                $tempname   = 'mailtemplates.levelup';
                $sendmail->getMail($fromMail,$toMail,$subject,$mdata,$tempname);
              }

            }
          }

          \DB::table('gamification_userpoints')->where('user_id', '=', $user_id)->increment('total_points', $points);
          \DB::table('gamification_userpoints')->where('user_id', '=', $user_id)->increment('points_balance', $points);
        }
      }
    }
    \bsetecHelpers::updateGamificationStreak($user_id);

  }

  public static function updateGamificationCourseSubActions($user_id,$course_id,$lecture_id,$status)
  {
    if($status == '1'){
      $options = \DB::table('options')->where('code', '=','gamification')->get();
      $gamification = array();
      foreach($options as $results)
      {
        $gamification[$results->option_key] = $results->option;
      }
      $d_course = DB::table('course')->where('course_id', '=', $course_id)->first();
      if($d_course && $d_course->gamification == '0'){
        $gamification['Status'] = 'false';
      }
      if(!isset($gamification['Status']) || $gamification['Status'] != 'true'){   // check if gamification enabled
        return false;
      }

      $lq = \DB::table('curriculum_lectures_quiz')->where('lecture_quiz_id', '=',$lecture_id)->first();
      if(!empty($lq)){
        $type = 'free';
        $action_id = 3;
        if($lq->type == '0'){
            $action_id = 4+$lq->media_type;
        }
        $gam = \DB::table('gamification')->where('action_id', '=',$action_id)->where('course_id', '=',$course_id)->where('lecture_id', '=',$lecture_id)->where('user_id', '=',$user_id)->first();
        if(empty($gam)){

          $pointdata = \DB::table('gamification_actions')->select('paid_course_points','free_course_points')->where('action_id', '=', $action_id)->where('status', '=', '0')->first();
          if(!empty($pointdata)){   // check if gamification action enabled
            $free_course = DB::table('course')->where('course_id', '=', $course_id)->where('pricing', '=', '0')->count();
            if(!$free_course){
              $type = 'paid';
              $points = $pointdata->paid_course_points;
            } else {
              $type = 'free';
              $points = $pointdata->free_course_points;
            }

            if($type == 'paid' || ($type == 'free' && isset($gamification['apply_for_free_course']) && $gamification['apply_for_free_course'] == 'true')){  // check if paid or free enabled

              $data['user_id'] = $user_id;
              $data['course_id'] = $course_id;
              $data['action_id'] = $action_id;
              $data['lecture_id'] = $lecture_id;
              $data['points'] = $points;
              $data['created_at'] = date("Y-m-d H:i:s");
              \DB::table('gamification')->insert($data);

              $tpoints = \DB::table('gamification_userpoints')->select('total_points')->where('user_id', '=', $user_id)->first();
              if(isset($tpoints->total_points)){
                $total_points = $tpoints->total_points;
                $leveldata = \DB::table('gamification_levels')->where('reach_points', '>=', $total_points)->orderBy('reach_points', 'asc')->first();
                if($leveldata) {
                  $reach_points = $leveldata->reach_points;
                  $added_total = $total_points+$points;
                  if($total_points < $reach_points && $reach_points >= $added_total){ // user has leveled up
                    $sendmail = new CommonmailController();
                    $userinfo = \DB::table('users')->where('id', '=', $user_id)->first();

                    $image      = \URL::to('uploads/images/'.$leveldata->image);
                    $level      = $leveldata->title;
                    $alink      = \URL::to('/'.$userinfo->username);
                    $toMail     = Auth::user()->email;
                    $fromMail   = CNF_EMAIL;
                    $subject    = CNF_APPNAME." Level Up";
                    $mdata       = array('username'=>$userinfo->username,'level' => $level,'image' => $image,'alink' => $alink,'points' => $points);
                    $tempname   = 'mailtemplates.levelup';
                    $sendmail->getMail($fromMail,$toMail,$subject,$mdata,$tempname);
                  }

                }
              }

              \DB::table('gamification_userpoints')->where('user_id', '=', $user_id)->increment('total_points', $points);
              \DB::table('gamification_userpoints')->where('user_id', '=', $user_id)->increment('points_balance', $points);
            }
          }
        }
      }

      $type = 'free';
      $action_id = 2;
      $gam2 = \DB::table('gamification')->where('action_id', '=',$action_id)->where('course_id', '=',$course_id)->where('user_id', '=',$user_id)->first();
      if(empty($gam2)){

        $lecturescount = 0;
        $sections = \DB::table('curriculum_sections')->where('course_id', '=', $course_id)->get();
        if(!empty($sections)){
          foreach($sections as $section){
            $lecturescount += \DB::table('curriculum_lectures_quiz')->where('section_id', '=', $section->section_id)->where('publish', 1)->orderBy('sort_order', 'asc')->count();
          }
        }
        $completedcount = \DB::table('course_progress')->where('course_id','=', $course_id)->where('user_id','=', $user_id)->where('status','=', '1')->count();

        if($lecturescount == $completedcount){
          $pointdata = \DB::table('gamification_actions')->select('paid_course_points','free_course_points')->where('action_id', '=', $action_id)->where('status', '=', '0')->first();
          if(!empty($pointdata)){   // check if gamification action enabled
            $free_course = DB::table('course')->where('course_id', '=', $course_id)->where('pricing', '=', '0')->count();
            if(!$free_course){
              $type = 'paid';
              $points = $pointdata->paid_course_points;
            } else {
              $type = 'free';
              $points = $pointdata->free_course_points;
            }

            if($type == 'paid' || ($type == 'free' && isset($gamification['apply_for_free_course']) && $gamification['apply_for_free_course'] == 'true')){  // check if paid or free enabled

              $data['user_id'] = $user_id;
              $data['course_id'] = $course_id;
              $data['action_id'] = $action_id;
              $data['points'] = $points;
              $data['created_at'] = date("Y-m-d H:i:s");
              \DB::table('gamification')->insert($data);

              $tpoints = \DB::table('gamification_userpoints')->select('total_points')->where('user_id', '=', $user_id)->first();
              if(isset($tpoints->total_points)){
                $total_points = $tpoints->total_points;
                $leveldata = \DB::table('gamification_levels')->where('reach_points', '>=', $total_points)->orderBy('reach_points', 'asc')->first();
                if($leveldata) {
                  $reach_points = $leveldata->reach_points;
                  $added_total = $total_points+$points;
                  if($total_points < $reach_points && $reach_points >= $added_total){ // user has leveled up
                    $sendmail = new CommonmailController();
                    $userinfo = \DB::table('users')->where('id', '=', $user_id)->first();

                    $image      = \URL::to('uploads/images/'.$leveldata->image);
                    $level      = $leveldata->title;
                    $alink      = \URL::to('/'.$userinfo->username);
                    $toMail     = Auth::user()->email;
                    $fromMail   = CNF_EMAIL;
                    $subject    = CNF_APPNAME." Level Up";
                    $mdata       = array('username'=>$userinfo->username,'level' => $level,'image' => $image,'alink' => $alink,'points' => $points);
                    $tempname   = 'mailtemplates.levelup';
                    $sendmail->getMail($fromMail,$toMail,$subject,$mdata,$tempname);
                  }

                }
              }

              $gamif = \DB::table('gamification')->where('user_id','=', $user_id)->where('course_id', '=',$course_id)->get();
              if(!empty($gamif)){
                $course_points = 0;
                foreach ($gamif as $key => $game) {
                  if($game->points > 0){
                    $course_points += $game->points;
                  }
                }

                $sendmail = new CommonmailController();
                $userinfo = \DB::table('users')->where('id', '=', $user_id)->first();
                $courseinfo = \DB::table('course')->where('course_id', '=', $course_id)->first();

                $image      = \URL::to('uploads/images/'.$leveldata->image);
                $level      = $leveldata->title;
                $course_title      = $courseinfo->course_title;
                $alink      = \URL::to('/'.$userinfo->username);
                $toMail     = Auth::user()->email;
                $fromMail   = CNF_EMAIL;
                $subject    = CNF_APPNAME." Course Points";
                $mdata       = array('username'=>$userinfo->username,'level' => $level,'image' => $image,'alink' => $alink,'course_title' => $course_title,'points' => $course_points);
                $tempname   = 'mailtemplates.course_points';
                $sendmail->getMail($fromMail,$toMail,$subject,$mdata,$tempname);

              }


              \DB::table('gamification_userpoints')->where('user_id', '=', $user_id)->increment('total_points', $points);
              \DB::table('gamification_userpoints')->where('user_id', '=', $user_id)->increment('points_balance', $points);
            }
          }
        }
      }

      \bsetecHelpers::updateGamificationStreak($user_id);

    }
  }

  public static function updateGamification($user_id,$course_id,$action_id,$update_points = 0)
  {
    $points = 0;
    $type = 'none';
    $options = \DB::table('options')->where('code', '=','gamification')->get();
    $gamification = array();
    foreach($options as $results)
    {
      $gamification[$results->option_key] = $results->option;
    }
    $d_course = DB::table('course')->where('course_id', '=', $course_id)->first();
    if($d_course && $d_course->gamification == '0'){
      $gamification['Status'] = 'false';
    }
    if(!isset($gamification['Status']) || $gamification['Status'] != 'true'){   // check if gamification enabled
      return false;
    }

    $pointdata = \DB::table('gamification_actions')->select('paid_course_points','free_course_points')->where('action_id', '=', $action_id)->where('status', '=', '0')->first();
    if(!empty($pointdata)){   // check if gamification action enabled
      $free_course = DB::table('course')->where('course_id', '=', $course_id)->where('pricing', '=', '0')->count();
      if(!$free_course){
        $type = 'paid';
        $points = $pointdata->paid_course_points;
      } else {
        $type = 'free';
        $points = $pointdata->free_course_points;
      }

      if($type == 'paid' || ($type == 'free' && isset($gamification['apply_for_free_course']) && $gamification['apply_for_free_course'] == 'true')){  // check if paid or free enabled
        $data['user_id'] = $user_id;
        $data['course_id'] = $course_id;
        $data['action_id'] = $action_id;
        $data['created_at'] = date("Y-m-d H:i:s");
        if($type == 'paid'){
          $data['points'] = -$update_points;
          \DB::table('gamification')->insert($data);
        }
        $data['points'] = $points;
        \DB::table('gamification')->insert($data);

        if($action_id == '1' && $update_points > 0) {
          $user_points = \DB::table('gamification_userpoints')->select('id')->where('user_id', '=', $user_id)->first();
          if(!empty($user_points)){
            \DB::table('gamification_userpoints')->where('user_id', '=', $user_id)->decrement('points_balance', $update_points);
            \DB::table('gamification_userpoints')->where('user_id', '=', $user_id)->increment('points_spent', $update_points);
              return true;
          }
        }

        if($update_points == 0 && $points != 0){
          $user_points = \DB::table('gamification_userpoints')->select('id')->where('user_id', '=', $user_id)->first();
          if(!empty($user_points)){
            \DB::table('gamification_userpoints')->where('user_id', '=', $user_id)->increment('total_points', $points);
            \DB::table('gamification_userpoints')->where('user_id', '=', $user_id)->increment('points_balance', $points);
            return true;
          } else {
            $updata['user_id'] = $user_id;
            $updata['total_points'] = $points+10; // with default 10 points
            $updata['points_balance'] = $points+10; // with default 10 points
            $updata['points_spent'] = 0;
            \DB::table('gamification_userpoints')->insert($updata);
            return true;
          }
        }

      }
    }

    \bsetecHelpers::updateGamificationStreak($user_id);

  }

  public static function getUserStreak()
  {
    $streak = DB::select("SELECT COUNT( * ) AS streak
                FROM tb_gamification
                WHERE user_id =?
                AND  `created_at` <= NOW( )
                AND  `created_at` >= DATE_SUB(SYSDATE(), Interval 1 Day)
                ORDER BY  `created_at` DESC",
              array(\Session::get('uid')));

    if(isset($streak->streak) && $streak->streak > 0){  // check if user has maintained the streak
      $result = DB::select("SELECT COUNT( streak ) AS streak
      FROM (
      SELECT id, user_id,  `created_at` , DATEDIFF( NOW( ) ,  `created_at` ) , @streak := IF( DATEDIFF( NOW( ) ,  `created_at` ) - @days_diff >1, @streak , IF( @days_diff := DATEDIFF( NOW( ) ,  `created_at` ) , @streak +1, @streak +1 ) ) AS streak
      FROM tb_gamification
      CROSS JOIN (
      SELECT @streak :=0, @days_diff := -1
      ) AS vars
      WHERE user_id =?
      AND  `created_at` <= NOW( )
      GROUP BY DATE(  `created_at` )
      ORDER BY  `created_at` DESC
      ) AS t", array(\Session::get('uid')));
      // check the streak count
      if(isset($result[0]))
        return $result[0]->streak;
      else
        return '0';
    } else {
      return '0';
    }

  }
  
  public static function checkgamification()
  {
    $gamification=\DB::table('options')->where('code','gamification')->get();
    return $gamification;
  }	

  public static function getGroupnameByID($id)
  {
    $group_name = "";
    $group=\DB::table('groups')->where('group_id',$id)->first();
    if(count($group)>0)
        $group_name = $group->name;
    return $group_name;
  } 

   public static function getBlogname($id)
  {
    $blog_name = "";
    $blog=\DB::table('blogcategories')->where('CatID',$id)->first();
    if(count($blog)>0)
        $blog_name = $blog->name;
    return $blog_name;
  } 

  

   public static function onlineuserslist($userid='')
  {
    if($userid==''){
    $lists=\DB::table('onlineusers')->where('user_id','!=',\Session::get('uid'))->get();
    return $lists;
    }
    else
    {
      $lists=\DB::table('onlineusers')->where('user_id','=',$userid)->get();
      if(count($lists)>0)
        return true;
      else
         return false;
    }
  }

    public static function addonlineusers($uid)
  {

    $check=\DB::table('onlineusers')->where('user_id','=',$uid)->first();
    \DB::table('users')->where('id','=',$uid)->update(['active_tutor'=>1]);     
    if(empty($check))
    {
      $check=\DB::table('onlineusers')->insert(['user_id'=>$uid]); 
    }

  }

  public static function getcurrencymethod()
{
    return DB::table('currency')->pluck('name','id');  
 
}

  public static function getGUID(){
    if (function_exists('com_create_guid')){
        return com_create_guid();
    }
    else {
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12);
        return $uuid;
    }
  }

  public static function checkadmincoupon()
{
  $dt = date('Y-m-d');
  $coupons = \DB::table('coupon_discount')->where('coupon_status', '1')->where('coupon_start_date','<=',$dt)->where('coupon_end_date','>=',$dt)->where('usage_limit','>','0')->get();
  return (count($coupons) > 0) ? true : false;
}

public static function getMembershipofuseractive(){
  $uid = \Session::get('uid');
  return \DB::table('transactions')
  ->select('transactions.*','membership_plan.*','transactions.status as status','transactions.updated_at as updated_at','transactions.created_at as created_at')
  ->leftJoin('membership_plan', 'membership_plan.plan_id', '=', 'transactions.course_id')
  ->where('user_id', '=', $uid)
  ->where('transactions.purchase_type', '=', 'membership')
  ->whereIn('transactions.status', array('completed'))
  ->orderBy('transactions.id', 'desc')
  ->first();
}

public static function siteMembership()
{
  return \DB::table('membership_plan')->where(function ($query) {
    $query->where('status', '=', '1')
    ->orwhere('status_y', '=', '1');
  })->orderBy('level')->get();
}

public static function getmaxplandiscount()
{
  $result = DB::select("SELECT max(concat(round(( 100-(plan_amount_y/(plan_amount*12) * 100) ),2),'%')) AS percentage FROM `".\bsetecHelpers::getdbprefix()."membership_plan` WHERE status = 1 AND status_y = 1");
  if(isset($result[0]))
    return $result[0]->percentage;
  else
    return 0;
}

public static function getMembershipofuserallstatus(){
    $uid = \Session::get('uid');
    return \DB::table('transactions')
    ->select('transactions.*','membership_plan.*','transactions.status as status','transactions.updated_at as updated_at','transactions.created_at as created_at')
    ->leftJoin('membership_plan', 'membership_plan.plan_id', '=', 'transactions.course_id')
    ->where('user_id', '=', $uid)
    ->where('transactions.purchase_type', '=', 'membership')
    ->whereIn('transactions.status', array('completed','cancelled','failed'))
    ->orderBy('transactions.id', 'desc')
    ->first();
  }


  public static function getMembershipofuser(){
    $uid = \Session::get('uid');
    return \DB::table('transactions')
    ->select('transactions.*','membership_plan.*','transactions.status as status','transactions.updated_at as updated_at','transactions.created_at as created_at')
    ->leftJoin('membership_plan', 'membership_plan.plan_id', '=', 'transactions.course_id')
    ->where('user_id', '=', $uid)
    ->where('transactions.purchase_type', '=', 'membership')
    ->where(function ($query) {
      $query->where('transactions.status', '=', 'completed')
      ->orwhere(function ($query2) {
        $query2->where('transactions.status', '=', 'cancelled')
        ->where('transactions.purchase_period', '=', 'monthly')
        ->whereRaw(\bsetecHelpers::getdbprefix().'transactions.created_at > now() - interval 1 month');
      })->orwhere(function ($query3) {
        $query3->where('transactions.status', '=', 'cancelled')
        ->where('transactions.purchase_period', '=', 'yearly')
        ->whereRaw(\bsetecHelpers::getdbprefix().'transactions.created_at > now() - interval 1 year');
      });
    })
    ->orderBy('transactions.id', 'desc')
    ->first();
  }
	
  public static function removeonlineusers($uid){
    $check=\DB::table('onlineusers')->where('user_id','=',$uid)->first();
    \DB::table('users')->where('id','=',$uid)->update(['active_tutor'=>0]);     
    if(!empty($check))
    {
      $check=\DB::table('onlineusers')->where('user_id','=',$uid)->delete();
    }
  }

  public static function getadmincouponcode($id){
   return \DB::table('coupon_discount')->select('coupon_code','usage_limit')->where('id','=',$id)->first();

  }

  public static function checkmembership($user_id,$course_id){
    $result = \DB::table('membership_course')
    ->leftJoin('membership_plan','membership_plan.plan_id','=','membership_course.plan_id')
    ->leftJoin('users','users.membership','=','membership_plan.plan_id')
    ->where('users.id','=',$user_id)->select('membership_course.course_id')->get()->toArray();
    if(count($result)>0){
      foreach ($result as $key => $value) {
        if($value->course_id == $course_id){
          return true;
        }
      }
      return false;
    }
    return false;
  }

  public static function getusermembership(){
        $uid = \Session::get('uid');
  if($uid){
    $plan_id = \DB::table('users')->select('membership')->where('id','=',$uid)->first();
    return $plan_id->membership;
    }
  }

  public static function getmembershipstatus(){
    $result = \DB::table('options')->select('option')->where('code','=','membership')->first();
    return $result->option;
  }

  public static function isappliedccr($user_id){
    return \DB::table('custom_course_request')->where('tutor_id','=',$user_id)->where('ccr_status','!=','0')->first();
  }

  public static function getInstitutiontype($id){
    $type = \DB::table('institution_types')->where('id',$id)->first();
    return $type->type;
  }

  public static function getInstitutionTypeId($type){
    return \DB::table('institution_types')->where('type', $type)->value('id');    
  }
  
  public static function getInstitutionName($id){
    return \DB::table('institution')->where('id', $id)->value('name');    
  }

  public static function getDepartmentName($id){
    return \DB::table('departments')->where('id', $id)->value('department');    
  }

  public static function getInstitutionCourseName($id){
    return \DB::table('institution_course')->where('id', $id)->value('name');    
  }

  public static function getSemesterName($id) {
    return \DB::table('institution_semester')->where('id', $id)->value('semester'); 
  }

}

?>
