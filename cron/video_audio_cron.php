<?php 
error_reporting( E_ALL );
$basepath 	= dirname(dirname(__FILE__));

require_once $basepath.'/vendor/autoload.php';
$path 		= $basepath;
$dotenv = new Dotenv\Dotenv($path);
$dotenv->load(__DIR__);
$host   	= getenv('DB_HOST');
$username   = getenv('DB_USERNAME');
$password   = getenv('DB_PASSWORD');
$dbname   	= getenv('DB_DATABASE');
$dbprefix   = getenv('DB_PREFIX');
$link = mysqli_connect($host, $username, $password, $dbname);
//Video process start here
if (!$link) {
	die('Could not connect: ' . mysqli_connect_errno());
} else {
	$sql = "SELECT * FROM ".$dbprefix."course_videos WHERE ".$dbprefix."course_videos.`processed`='0' ORDER BY ".$dbprefix."course_videos.`id` ASC";
	$query = mysqli_query($link, $sql);
	while ($rows = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
		$source = 'raw_'.$rows['created_at'].'_'.$rows['video_title'].'.'.$rows['video_type'];
		$filename = $rows['video_title'];
		$id = $rows['id'];
		processVideo($source,$filename);
		mysqli_query($link, "UPDATE ".$dbprefix."course_videos SET ".$dbprefix."course_videos.`processed` = '1' WHERE ".$dbprefix."course_videos.`id` ='".$id."'");
	}
}

function processVideo($source_filename='',$destination_filename='')
{
	$basepath 	= dirname(dirname(__FILE__));
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {	// check if windows
		echo $ffmpeg_path = $basepath.'\resources\assets\ffmpeg\ffmpeg_win\ffmpeg';
		echo $source_path = $basepath.'\public\uploads\videos\\'.$source_filename;
		echo $destination_path = $basepath.'\public\uploads\videos\\';
		videoConversion($ffmpeg_path,$source_path,$destination_path,$destination_filename);
	} else {
		echo $ffmpeg_path = $basepath.'/resources/assets/ffmpeg/ffmpeg_lin/ffmpeg.exe';
		echo $source_path = $basepath.'/public/uploads/videos/'.$source_filename;
		echo $destination_path = $basepath.'/public/uploads/videos/';
		videoConversion($ffmpeg_path,$source_path,$destination_path,$destination_filename);
	}
}

function videoConversion($ffmpeg_path,$source_path,$destination_path,$destination_filename){
	$ogvcmd = "$ffmpeg_path -i $source_path -acodec libvorbis -b:a 128k -vcodec libtheora -b:v 400k -f ogg $destination_path".$destination_filename.".ogv";
	shell_exec($ogvcmd);
	
	$webmcmd = "$ffmpeg_path -i $source_path -acodec libvorbis -b:a 128k -ac 2 -vcodec libvpx -b:v 400k -f webm $destination_path".$destination_filename.".webm";
	shell_exec($webmcmd);
	
	$mp4cmd = "$ffmpeg_path -i $source_path -acodec aac -strict experimental $destination_path".$destination_filename.".mp4";
	shell_exec($mp4cmd);
}
//Video process end here

//Audio process start here

$sql_audio 	 = "SELECT * FROM ".$dbprefix."course_files WHERE `processed`='0' AND file_extension='wav' ORDER BY ".$dbprefix."course_files.`id` ASC";
$query_audio = mysqli_query($link, $sql_audio);
while ($rows = mysqli_fetch_array($query_audio,MYSQLI_ASSOC)) {
	$source 	= $rows['file_name'].'.'.$rows['file_type'];
	$filename 	= $rows['file_name'];
	$id 		= $rows['id'];
	processAudio($source,$filename);
	mysqli_query($link, "UPDATE ".$dbprefix."course_files SET ".$dbprefix."course_files.`processed` = '1' WHERE ".$dbprefix."course_files.`id` ='".$id."'");
}


function processAudio($source_filename='',$destination_filename='')
{
	$basepath 	= dirname(dirname(__FILE__));
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {	// check if windows
		echo $ffmpeg_path = $basepath.'\resources\assets\ffmpeg\ffmpeg_win\ffmpeg';
		echo $source_path = $basepath.'\public\uploads\files\\'.$source_filename;
		echo $destination_path = $basepath.'\public\uploads\files\\';
		audioConversion($ffmpeg_path,$source_path,$destination_path,$destination_filename);
	} else {
		echo $ffmpeg_path = $basepath.'/resources/assets/ffmpeg/ffmpeg_lin/ffmpeg.exe';
		echo $source_path = $basepath.'/public/uploads/files/'.$source_filename;
		echo $destination_path = $basepath.'/public/uploads/files/';
		audioConversion($ffmpeg_path,$source_path,$destination_path,$destination_filename);
	}
}

function audioConversion($ffmpeg_path,$source_path,$destination_path,$destination_filename){

	$mp3cmd = "$ffmpeg_path -i $source_path -vn -ar 44100 -ac 2 -ab 192 -f mp3 $destination_path".$destination_filename.".mp3";
	shell_exec($mp3cmd);
}
//Audio process end here

mysqli_close($link);
?>