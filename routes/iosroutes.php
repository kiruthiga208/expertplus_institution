<?php 
   //api_ios API for IOS
Route::resource('/api-ios', 'api_ios\WelcomeController');

Route::post('/api-ios/user/register', 'api_ios\UserController@postRegister');
Route::post('/api-ios/user/login', 'api_ios\UserController@postLogin');
Route::post('/api-ios/user/coursedetails', 'api_ios\UserController@postCoursedetails');
Route::post('/api-ios/user/instructor', 'api_ios\UserController@postInstructor');
Route::post('/api-ios/user/adddiscussion', 'api_ios\UserController@postAdddiscussion');
Route::post('/api-ios/user/updatediscussion', 'api_ios\UserController@postUpdatediscussion');
Route::post('/api-ios/user/discussiondelete', 'api_ios\UserController@postDiscussiondelete');
Route::post('/api-ios/user/wishlist', 'api_ios\UserController@postWishlist');
Route::post('/api-ios/user/addreply', 'api_ios\UserController@postAddreply');
Route::post('/api-ios/user/updatereply', 'api_ios\UserController@postUpdatereply');
Route::post('/api-ios/user/replydestroy', 'api_ios\UserController@postReplydestroy');
Route::post('/api-ios/user/forgetrequest', 'api_ios\UserController@postForgetrequest');
Route::post('/api-ios/user/signin', 'api_ios\UserController@postSignin');
Route::post('/api-ios/user/updateinfo', 'api_ios\UserController@postUpdateinfo');
Route::post('/api-ios/user/changepassword', 'api_ios\UserController@postChangepassword');
Route::post('/api-ios/user/accountdestory', 'api_ios\UserController@postAccountdestory');
Route::post('/api-ios/user/lecturedetails', 'api_ios\UserController@postLecturedetails');
Route::post('/api-ios/user/searchdiscussion', 'api_ios\UserController@postSearchdiscussion');
Route::post('/api-ios/user/updateprofile', 'api_ios\UserController@postUpdateprofile');
Route::post('/api-ios/user/addrating', 'api_ios\UserController@postAddrating');
Route::post('/api-ios/user/rating', 'api_ios\UserController@postRating');
Route::post('/api-ios/user/reportabuse', 'api_ios\UserController@postReportabuse');
Route::post('/api-ios/user/signup', 'api_ios\UserController@postSignup');
Route::post('/api-ios/user/quizquestions', 'api_ios\UserController@postQuizquestions');
Route::post('/api-ios/user/quizresult', 'api_ios\UserController@postQuizresult');
Route::post('/api-ios/user/changeemail', 'api_ios\UserController@postChangeemail');
Route::post('/api-ios/user/delreview', 'api_ios\UserController@postDelreview');
Route::post('/api-ios/user/buycourse', 'api_ios\UserController@postBuycourse');
Route::post('/api-ios/user/usermail', 'api_ios\UserController@postUsermail');
Route::post('/api-ios/user/features', 'api_ios\UserController@postFeatures');
Route::post('/api-ios/user/recently', 'api_ios\UserController@postRecently');
Route::post('/api-ios/user/notes', 'api_ios\UserController@postNotes');
Route::post('/api-ios/user/removenotes', 'api_ios\UserController@postRemovenotes');
Route::post('/api-ios/user/announcements', 'api_ios\UserController@postAnnouncements');
Route::post('/api-ios/user/updateannouncement', 'api_ios\UserController@postUpdateannouncement');
Route::post('/api-ios/user/removeannouncement', 'api_ios\UserController@postRemoveannouncement');
Route::post('/api-ios/user/upsertquestion', 'api_ios\UserController@postUpsertquestion');
Route::post('/api-ios/user/uploadqtnfile', 'api_ios\UserController@postUploadqtnfile');
Route::post('/api-ios/user/upsertreply', 'api_ios\UserController@postUpsertreply');
Route::post('/api-ios/user/removequestions', 'api_ios\UserController@postRemovequestions');
Route::post('/api-ios/user/removeanswers', 'api_ios\UserController@postRemoveanswers');
Route::post('/api-ios/user/removeqtnfile', 'api_ios\UserController@postRemoveqtnfile');
Route::post('/api-ios/user/savepayment', 'api_ios\UserController@postSavepayment');
Route::post('/api-ios/user/purchaserestore', 'api_ios\UserController@postPurchaserestore');


Route::get('/api-ios/user/categories', 'api_ios\UserController@getCategories');
Route::get('/api-ios/user/subcategories', 'api_ios\UserController@getSubcategories');
Route::get('/api-ios/user/course', 'api_ios\UserController@getCourse');
Route::get('/api-ios/user/courselist', 'api_ios\UserController@getCourselist');
Route::get('/api-ios/user/coursecurriculum', 'api_ios\UserController@getCoursecurriculum');
Route::get('/api-ios/user/coursereview', 'api_ios\UserController@getCoursereview');
Route::get('/api-ios/user/viewdiscussion', 'api_ios\UserController@getViewdiscussion');
Route::get('/api-ios/user/coursewishlist', 'api_ios\UserController@getCoursewishlist');
Route::get('/api-ios/user/viewreplies', 'api_ios\UserController@getViewreplies');
Route::get('/api-ios/user/mycourselist', 'api_ios\UserController@getMycourselist');
Route::get('/api-ios/user/announcement', 'api_ios\UserController@getAnnouncement');
Route::get('/api-ios/user/usercourse', 'api_ios\UserController@getUsercourse');
Route::get('/api-ios/user/profileinfo', 'api_ios\UserController@getProfileInfo');
Route::get('/api-ios/user/searchcourse', 'api_ios\UserController@getSearchcourse');
Route::get('/api-ios/user/enrolled', 'api_ios\UserController@getEnrolled');
Route::get('/api-ios/user/trending', 'api_ios\UserController@getTrending');
Route::get('/api-ios/user/viewall', 'api_ios\UserController@getViewall');
Route::get('/api-ios/user/currency', 'api_ios\UserController@getCurrency');
Route::get('/api-ios/user/notsubscriber', 'api_ios\UserController@getNotsubscriber');
Route::get('/api-ios/user/subscriber', 'api_ios\UserController@getSubscriber');
Route::get('/api-ios/user/viewlecture', 'api_ios\UserController@getViewlecture');
Route::get('/api-ios/user/lecturediscussion', 'api_ios\UserController@getLecturediscussion');
Route::get('/api-ios/user/coursecertificate', 'api_ios\UserController@getCoursecertificate');
Route::get('/api-ios/user/unreadlecture', 'api_ios\UserController@getUnreadlecture');
Route::get('/api-ios/user/getcoupon', 'api_ios\UserController@getGetcoupon');
Route::get('/api-ios/user/downloadnotes', 'api_ios\UserController@getDownloadnotes');
Route::get('/api-ios/user/questions', 'api_ios\UserController@getQuestions');
Route::get('/api-ios/user/questionview', 'api_ios\UserController@getQuestionview');
Route::get('/api-ios/user/answerslist', 'api_ios\UserController@getAnswerslist');
Route::get('/api-ios/user/membership', 'api_ios\UserController@getMembership');
Route::get('/api-ios/user/membershipinfo', 'api_ios\UserController@getMembershipinfo');
Route::get('/api-ios/user/menus', 'api_ios\UserController@getMenus');


Route::resource('/api-ios/user', 'api_ios\UserController');
?>