<?php 
   //api_droid API for ANDROID

Route::resource('/api-droid', 'api_droid\WelcomeController');
Route::post('/api-droid/user/register', 'api_droid\UserController@postRegister');
Route::post('/api-droid/user/login', 'api_droid\UserController@postLogin');
Route::post('/api-droid/user/coursedetails', 'api_droid\UserController@postCoursedetails');
Route::post('/api-droid/user/coursecurriculum', 'api_droid\UserController@postCoursecurriculum');
Route::post('/api-droid/user/viewdiscussion', 'api_droid\UserController@postViewdiscussion');
Route::post('/api-droid/user/instructor', 'api_droid\UserController@postInstructor');
Route::post('/api-droid/user/adddiscussion', 'api_droid\UserController@postAdddiscussion');
Route::post('/api-droid/user/updatediscussion', 'api_droid\UserController@postUpdatediscussion');
Route::post('/api-droid/user/discussiondelete', 'api_droid\UserController@postDiscussiondelete');
Route::post('/api-droid/user/wishlist', 'api_droid\UserController@postWishlist');
Route::post('/api-droid/user/addreply', 'api_droid\UserController@postAddreply');
Route::post('/api-droid/user/updatereply', 'api_droid\UserController@postUpdatereply');
Route::post('/api-droid/user/replydestroy', 'api_droid\UserController@postReplydestroy');
Route::post('/api-droid/user/viewreplies', 'api_droid\UserController@postViewreplies');
Route::post('/api-droid/user/announcement', 'api_droid\UserController@postAnnouncement');
Route::post('/api-droid/user/forgetrequest', 'api_droid\UserController@postForgetrequest');
Route::post('/api-droid/user/usercourse', 'api_droid\UserController@postUsercourse');
Route::post('/api-droid/user/signin', 'api_droid\UserController@postSignin');
Route::post('/api-droid/user/updateinfo', 'api_droid\UserController@postUpdateinfo');
Route::post('/api-droid/user/changepassword', 'api_droid\UserController@postChangepassword');
Route::post('/api-droid/user/accountdestory', 'api_droid\UserController@postAccountdestory');
Route::post('/api-droid/user/lecturedetails', 'api_droid\UserController@postLecturedetails');
Route::post('/api-droid/user/searchdiscussion', 'api_droid\UserController@postSearchdiscussion');
Route::post('/api-droid/user/updateprofile', 'api_droid\UserController@postUpdateprofile');
Route::post('/api-droid/user/addrating', 'api_droid\UserController@postAddrating');
Route::post('/api-droid/user/rating', 'api_droid\UserController@postRating');
Route::post('/api-droid/user/reportabuse', 'api_droid\UserController@postReportabuse');
Route::post('/api-droid/user/enrolled', 'api_droid\UserController@postEnrolled');
Route::post('/api-droid/user/signup', 'api_droid\UserController@postSignup');
Route::post('/api-droid/user/quizquestions', 'api_droid\UserController@postQuizquestions');
Route::post('/api-droid/user/changeemail', 'api_droid\UserController@postChangeemail');
Route::post('/api-droid/user/quizresult', 'api_droid\UserController@postQuizresult');
Route::post('/api-droid/user/changeemail', 'api_droid\UserController@postChangeemail');
Route::post('/api-droid/user/delreview', 'api_droid\UserController@postDelreview');
Route::post('/api-droid/user/buycourse', 'api_droid\UserController@postBuycourse');
Route::post('/api-droid/user/usermail', 'api_droid\UserController@postUsermail');
Route::post('/api-droid/user/features', 'api_droid\UserController@postFeatures');
Route::post('/api-droid/user/recently', 'api_droid\UserController@postRecently');
Route::post('/api-droid/user/notes', 'api_droid\UserController@postNotes');
Route::post('/api-droid/user/removenotes', 'api_droid\UserController@postRemovenotes');
Route::post('/api-droid/user/announcements', 'api_droid\UserController@postAnnouncements');
Route::post('/api-droid/user/updateannouncement', 'api_droid\UserController@postUpdateannouncement');
Route::post('/api-droid/user/removeannouncement', 'api_droid\UserController@postRemoveannouncement');
Route::post('/api-droid/user/upsertquestion', 'api_droid\UserController@postUpsertquestion');
Route::post('/api-droid/user/uploadqtnfile', 'api_droid\UserController@postUploadqtnfile');
Route::post('/api-droid/user/upsertreply', 'api_droid\UserController@postUpsertreply');
Route::post('/api-droid/user/removequestions', 'api_droid\UserController@postRemovequestions');
Route::post('/api-droid/user/removeanswers', 'api_droid\UserController@postRemoveanswers');
Route::post('/api-droid/user/removeqtnfile', 'api_droid\UserController@postRemoveqtnfile');





Route::get('/api-droid/user/categories', 'api_droid\UserController@getCategories');
Route::get('/api-droid/user/subcategories', 'api_droid\UserController@getSubcategories');
Route::get('/api-droid/user/course', 'api_droid\UserController@getCourse');
Route::get('/api-droid/user/courselist', 'api_droid\UserController@getCourselist');
Route::get('/api-droid/user/coursereview', 'api_droid\UserController@getCoursereview');
Route::get('/api-droid/user/coursewishlist', 'api_droid\UserController@getCoursewishlist');
Route::get('/api-droid/user/mycourselist', 'api_droid\UserController@getMycourselist');
Route::get('/api-droid/user/profileinfo', 'api_droid\UserController@getProfileInfo');
Route::get('/api-droid/user/searchcourse', 'api_droid\UserController@getSearchcourse');
Route::get('/api-droid/user/trending', 'api_droid\UserController@getTrending');
Route::get('/api-droid/user/viewall', 'api_droid\UserController@getViewall');
Route::get('/api-droid/user/currency', 'api_droid\UserController@getCurrency');
Route::get('/api-droid/user/paymentsucess', 'api_droid\UserController@getPaymentsucess');
Route::get('/api-droid/user/notification', 'api_droid\UserController@getNotification');
Route::get('/api-droid/user/notsubscriber', 'api_droid\UserController@getNotsubscriber');
Route::get('/api-droid/user/downloadnotes', 'api_droid\UserController@getDownloadnotes');
Route::get('/api-droid/user/questions', 'api_droid\UserController@getQuestions');
Route::get('/api-droid/user/questionview', 'api_droid\UserController@getQuestionview');
Route::get('/api-droid/user/answerslist', 'api_droid\UserController@getAnswerslist');
Route::get('/api-droid/user/membership', 'api_droid\UserController@getMembership');
Route::get('/api-droid/user/membershipinfo', 'api_droid\UserController@getMembershipinfo');
Route::get('/api-droid/user/cancelmembership', 'api_droid\UserController@getCancelmembership');



Route::resource('/api-droid/user', 'api_droid\UserController');
?>