$(function () {
    favorite();
    deleteComment();
    followUnFollow();
    deleteReply();
    reply();
    postcomment();
    adminImageApprove();
    adminImageUnApprove();
    adminUnApprove();
    $("[data-toggle='tooltip']").tooltip();

    $('#dp1').datepicker({
        format: 'yyyy-mm-dd'
    });
    
    jQuery("abbr.timeago").timeago();

    $('.img-thumbnail').bind('contextmenu', function (e) {
        return false;
    });

});

function reply() {
    var c = $(".replybutton");
    var b = $(".closebutton");
    var a = $(".replytext");

    c.on("click", function () {
        var d = $(this).attr("id");
        $(this).hide();
        $("#open" + d).show();
        a.focus()
    });

    b.on("click", function () {
        var d = $(this).attr("id");
        $("#open" + d).hide();
        c.show()
    });

    $(".replyMainButton").click(function () {
        var e = $(this).attr("id");
        var f = $("#textboxcontent"+e).val();
        var d = "textcontent="+f+"&reply_msgid="+e;
        if (f === "") {
            a.stop().css("background-color", "#FFFF9C")
        } else {
            $.ajax({
                type: "POST",
                url: "../../reply",
                data: d,
                success: function (h) {
                    var g = $(h).css("font-weight", "bold");
                    $("#openbox-" + e).after(g);
                    $("#openbox-" + e).hide();
                    $("#textboxcontent"+e).val('');
                    $("#box-"+e).show();
                    deleteReply();
                }
            })
        }
        return false
    })
}

function postcomment() {
    var c = $(".replybutton");
    var b = $(".closebutton");
    var a = $(".replytext");

    c.on("click", function () {
        var d = $(this).attr("id");
        $(this).hide();
        $("#open" + d).show();
        a.focus()
    });

    b.on("click", function () {
        var d = $(this).attr("id");
        $("#open" + d).hide();
        c.show()
    });

    $(".CommentMainButton").click(function () {
        var f = $('#commenttextboxcontent').val();
        var d = "textcontent="+f+'&courseid='+$('#CourseIDHidden').val();
        if (f === "") {
             $('#commenttextboxcontent').stop().css("background-color", "#FFFF9C")
        }else {
             $.ajax({
                 type: "POST",
                 url: "../../postcomment",
                 data: d,
                 success: function (h) {
                     $("#commenttextboxcontent").val('');
                     //$("#commentPostBox").after(h);
                     $(h).insertAfter(".commentPostBox");
                     deleteReply();
                     deleteComment();
                     reply();
                 }
             })
         }
         return false;
    });
}

function followUnFollow() {
    $(".follow").on("click", function () {
        var c = $(this);
        var uRl = $('#FollowUrl').val();
        var b = "id=" + c.attr("id");
        $.ajax({type: "POST", url: uRl, data: b, success: function (a) {
            var splitdata = a.split('####');
            $.when(c.fadeOut().promise()).done(function () {
                if (c.hasClass("btn")) {
                    c.removeClass("btn-default").addClass("btn-success").text(splitdata[0]).fadeIn()
                } else {
                    c.replaceWith('<span class="notice_mid_link">' + splitdata[0] + "</span>")
                }
                $('.followcount').html(splitdata[1]);
               
            })
        }});
        return false
    })
}

/*function followUnFollow_new() {
    $(".favoritebtn").on("click", function () {
          var c = $(this);
        var b = "id=" + c.attr("id");
        $.ajax({type: "POST", url: $('#getSiteUrl').val()+"/follow", data: b, success: function (a) {
                $.when(c.fadeOut().promise()).done(function () {
                if (c.hasClass("btn")) {
                    var color = c.children("i").css("color");

                    if(color == 'rgb(128, 128, 128)')
                    c.children("i").css('color','#008000');
                    else
                    c.children("i").css('color','grey');

                    c.fadeIn();
                    c.removeClass("btn-default").addClass("btn-success").text(a).fadeIn()
                    $('.viewfavcount').html(a);
                } else {
                     c.replaceWith('<span class="notice_mid_link">' + a + "</span>")
                    
                }
            })
        }});
        return false
    })
}*/
function adminImageApprove() {
    $(".adminImageApprove").on("click", function () {
        var c = $(this);
        var b = "id=" + c.attr("id");
        $.ajax({type: "POST", url: $('#getSiteUrl').val()+"/admin/images/approval", data: b, success: function (a) {
            $.when(c.fadeOut().promise()).done(function () {
                if (c.hasClass("btn")) {
                    c.removeClass("btn-default").addClass("btn-success").text(a).fadeIn();
                } else {
                    c.replaceWith('<button class="notice_mid_link">' + a + "</button>");
                   // c.replaceWith('<button class="btn btn-xs btn-success adminImageApprove" id="'+c.attr("id")+'">' + a + '</button>');
                }
            })
        }});
        return false
    })
}
function adminImageUnApprove() {
    $(".adminImageUnApprove").on("click", function () {
        var c = $(this);
        var b = "id=" + c.attr("id");
        $.ajax({type: "POST", url: "images/approval", data: b, success: function (a) {
            $.when(c.fadeOut().promise()).done(function () {
                if (c.hasClass("btn")) {
                    c.removeClass("btn-default").addClass("btn-success").text(a).fadeIn();
                } else {
                    c.replaceWith('<button class="notice_mid_link">' + a + "</button>");
                   // c.replaceWith('<button class="btn btn-xs btn-success adminImageApprove" id="'+c.attr("id")+'">' + a + '</button>');
                }
            })
        }});
        return false
    })
}
function adminUnApprove() {
    $(".adminUnApprove").on("click", function () {
        var c = $(this);
        var b = "id=" + c.attr("id").replace(/\D/g,'');
        $.ajax({type: "POST", url:"images/unapproval", data: b, success: function (a) {
            $.when(c.fadeOut().promise()).done(function () {
                if (c.hasClass("btn")) {
                    c.removeClass("btn-default").addClass("btn-success").text(a).fadeIn()
                } else {
                    c.replaceWith('<button class="notice_mid_link">' + a + "</button>")
                }
            })
        }});
        return false
    })
}
function deleteComment() {
    var a = $("button.delete-comment");
    a.on("click", function () {
        var ConFirm = confirm("Are you sure want to delete!");
        if (ConFirm == true) {
            var c = $(this);
            var e = c.attr("data-content");
            var b = "id=" + e;
            $.ajax({type: "POST", url: "../../deletecomment", data: b, success: function (d) {
                $("#comment-" + e).hide()
            }});
        }
    })
}

function deleteReply() {
    var a = $("button.delete-reply");
    a.on("click", function () {
        var ConFirm = confirm("Are you sure want to delete!");
        if (ConFirm == true) {
            var c = $(this);
            var e = c.attr("data-content");
            var b = "id=" + e;
            $.ajax({type: "POST", url: "../../deletereply", data: b, success: function (d) {
                $("#reply-" + e).hide()
            }});
        }
    })
}

function favorite() {
    $(".favoritebtn").on("click", function () {
          var c = $(this);
        var b = "id=" + c.attr("id");
        $.ajax({type: "POST", url: $('#getSiteUrl').val()+"/favorite", data: b, success: function (a) {
                $.when(c.fadeOut().promise()).done(function () {
                if (c.hasClass("btn")) {
                    var color = c.children("i").css("color");

                    if(color == 'rgb(128, 128, 128)')
                    c.children("i").css('color','#008000');
                    else
                    c.children("i").css('color','grey');

                    c.fadeIn();
                    $('.viewfavcount'+ c.attr("id")).html(a);
                } else {
                     c.replaceWith('<span class="notice_mid_link">' + a + "</span>")
                    
                }
            })
        }});
        return false
    })
}

function run_pinmarklet() {
    var e = document.createElement('script');
    e.setAttribute('type', 'text/javascript');
    e.setAttribute('charset', 'UTF-8');
    e.setAttribute('src', 'http://assets.pinterest.com/js/pinmarklet.js?r=' + Math.random() * 99999999);
    document.body.appendChild(e);
}