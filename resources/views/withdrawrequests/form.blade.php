@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('withdrawrequests?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	

		 {!! Form::open(array('url'=>'withdrawrequests/save', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
			<fieldset><p><legend> {{ Lang::get('core.withdraw_requests') }} </legend></p>
								
							<div class="form-group" style="display:none">
							<label for="Id" class=" control-label col-md-4 text-left"> {{ Lang::get('core.id') }} </label>
							<div class="col-md-6">
							{!! Form::text('id', $row['id'],array('class'=>'form-control', 'placeholder'=>'',  'readonly'=>'true' )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="User Id" class=" control-label col-md-4 text-left"> {{ Lang::get('core.User_Id') }} </label>
							<div class="col-md-6">

							{!! Form::select('user_id',$users_list,$row['user_id'],array('class'=>'select2','id'=>'user_id','placeholder'=>Lang::get('core.please_select'))) !!}
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Paypal Id" class=" control-label col-md-4 text-left"> {{ Lang::get('core.paypal_id') }} </label>
							<div class="col-md-6">
							{!! Form::text('paypal_id', $row['paypal_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Amount" class=" control-label col-md-4 text-left"> {{ Lang::get('core.amount') }} </label>
							<div class="col-md-6">
							{!! Form::input('number','amount', $row['amount'],array('class'=>'form-control', 'placeholder'=>'','required'=>'true','min'=>'0','step'=>'any')) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 		
							@if($row['requested_on'] !="")			
							<div class="form-group  " >
							<label for="Requested On" class=" control-label col-md-4 text-left"> {{ Lang::get('core.requested_on') }} </label>
							<div class="col-md-6">
							{!! $row['requested_on']!!}
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 	
							@endif
							<div class="form-group  " >
							<label for="Status" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Status') }} </label>
							<div class="col-md-6">
							<select name="status" class="form-control">
									<option value="pending" @if($row['status'] == 'pending') {!! 'selected' !!}@endif>{{ Lang::get('core.pending') }}</option>
									<option value="completed" @if($row['status'] == 'completed') {!! 'selected' !!}@endif>{{ Lang::get('core.completed') }}</option>
								</select>
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> </fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8 btn-section">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('withdrawrequests?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		 
	});
	</script>		 
@stop