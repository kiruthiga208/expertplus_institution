
 @extends('layouts.app')

@section('content')
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3><i class="fa fa-envelope"></i> {{ Lang::get('core.t_blastemail') }}  <small>{{ Lang::get('core.t_blastemailsmall') }}</small></h3>
      </div>
   
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('bsetec/config/email') }}">{{ Lang::get('core.t_blastemail') }}</a></li>
		
      </ul>
	  
	  
    </div>

 <div class="page-content-wrapper">  
	@if(Session::has('message'))
	  
		   {{ Session::get('message') }}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="block-content">
	@include('bsetec.config.tab')	
<div class="tab-content m-t">
	  <div class="tab-pane active use-padding" id="info">	
	 {!! Form::open(array('url'=>'bsetec/config/email/', 'class'=>'form-vertical row')) !!}
	
	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title"> {{ Lang::get('core.registernew') }} <div class="pull-right"><a onclick="customaccordion('1')" href="javascript:void(0);"><span class="plusminus-1"><i class="fa fa-chevron-down"></i></span></a></div>  </div>
			<div class="sbox-content" id="wholecontent-1" style="display:none;"> 	
				  <div class="form-group">
					<label for="ipt" class=" control-label"> {{ Lang::get('core.tab_email') }} </label>		
					<textarea rows="20" id="tiny_mce" name="regEmail" class="form-control input-sm  markItUp mceEditor">{{ $regEmail }}</textarea>		
				  </div>  
				

				<div class="form-group">   
					<button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>	 
				</div>
			
			</div>	
		</div>
		


</div> 


	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title">  {{ Lang::get('core.forgotpassword') }} <div class="pull-right"><a onclick="customaccordion('2')" href="javascript:void(0);"><span class="plusminus-2"><i class="fa fa-chevron-down"></i></span></a></div></div>
			<div class="sbox-content" id="wholecontent-2" style="display:none;">  	
				  <div class="form-group">
					<label for="ipt" class=" control-label ">{{ Lang::get('core.tab_email') }} </label>					
					<textarea rows="20" name="resetEmail" class="form-control input-sm markItUp mceEditor">{{ $resetEmail }}</textarea>					 
				  </div> 

			  <div class="form-group">
					<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }}</button>
				 </div> 
			</div>	 
	  </div>	  
	
 	
 </div>
 <!-- Email activation -->
	
	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title">  {!! Lang::get('core.email_activation')!!} <div class="pull-right"><a onclick="customaccordion('3')" href="javascript:void(0);"><span class="plusminus-3"><i class="fa fa-chevron-down"></i></span></a></div></div>
			<div class="sbox-content" id="wholecontent-3" style="display:none;">	
				  <div class="form-group">
					<label for="ipt" class=" control-label ">{{ Lang::get('core.tab_email') }} </label>					
					<textarea rows="20" name="emailActivation" class="form-control input-sm markItUp mceEditor">{{ $emailActivation }}</textarea>					 
				  </div> 

			  <div class="form-group">
					<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }}</button>
				 </div> 
			</div>	 
	  </div>	  
	
 </div>
	
<!-- E-mail Activation end -->
<!-- reset password -->
	
	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title">  {!! Lang::get('core.reset_password')!!} <div class="pull-right"><a onclick="customaccordion('4')" href="javascript:void(0);"><span class="plusminus-4"><i class="fa fa-chevron-down"></i></span></a></div></div>
			<div class="sbox-content" id="wholecontent-4" style="display:none;"> 	 	
				  <div class="form-group">
					<label for="ipt" class=" control-label ">{{ Lang::get('core.tab_email') }} </label>					
					<textarea rows="20" name="resetPassword" class="form-control input-sm markItUp mceEditor">{{ $resetPassword }}</textarea>					 
				  </div> 

			  <div class="form-group">
					<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }}</button>
				 </div> 
			</div>	 
	  </div>	  
	
 </div>
	
<!-- reset password end -->

<!-- course feedback -->
	
	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title"> {!! Lang::get('core.course_feedback')!!} <div class="pull-right"><a onclick="customaccordion('5')" href="javascript:void(0);"><span class="plusminus-5"><i class="fa fa-chevron-down"></i></span></a></div></div>
			<div class="sbox-content" id="wholecontent-5" style="display:none;"> 	 	
				  <div class="form-group">
					<label for="ipt" class=" control-label ">{{ Lang::get('core.tab_email') }} </label>					
					<textarea rows="20" name="courseFeedback" class="form-control input-sm markItUp mceEditor">{{ $courseFeedback }}</textarea>					 
				  </div> 

			  <div class="form-group">
					<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }}</button>
				 </div> 
			</div>	 
	  </div>	  
	
 </div>
	
<!-- course feedback end -->

<!-- Course Joined -->
	
	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title">  {!! Lang::get('core.course_joined')!!} <div class="pull-right"><a onclick="customaccordion('6')" href="javascript:void(0);"><span class="plusminus-6"><i class="fa fa-chevron-down"></i></span></a></div></div>
			<div class="sbox-content" id="wholecontent-6" style="display:none;"> 	 	
				  <div class="form-group">
					<label for="ipt" class=" control-label ">{{ Lang::get('core.tab_email') }} </label>					
					<textarea rows="20" name="courseJoined" class="form-control input-sm markItUp mceEditor">{{ $courseJoined }}</textarea>					 
				  </div> 

			  <div class="form-group">
					<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }}</button>
				 </div> 
			</div>	 
	  </div>	  
	
 </div>
	
<!-- Course Joined end -->

<!-- Course Rating -->
	
	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title">  {!! Lang::get('core.course_rating')!!} <div class="pull-right"><a onclick="customaccordion('7')" href="javascript:void(0);"><span class="plusminus-7"><i class="fa fa-chevron-down"></i></span></a></div></div>
			<div class="sbox-content" id="wholecontent-7" style="display:none;"> 	 	
				  <div class="form-group">
					<label for="ipt" class=" control-label ">{{ Lang::get('core.tab_email') }} </label>					
					<textarea rows="20" name="courseRating" class="form-control input-sm markItUp mceEditor">{{ $courseRating }}</textarea>					 
				  </div> 

			  <div class="form-group">
					<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }}</button>
				 </div> 
			</div>	 
	  </div>	  
	
 </div>
	
<!-- Course Rating end -->

<!-- Course submitted -->
	
	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title"> {!! Lang::get('core.course_submit')!!} <div class="pull-right"><a onclick="customaccordion('8')" href="javascript:void(0);"><span class="plusminus-8"><i class="fa fa-chevron-down"></i></span></a></div></div>
			<div class="sbox-content" id="wholecontent-8" style="display:none;"> 	
				  <div class="form-group" > 	
					<label for="ipt" class=" control-label ">{{ Lang::get('core.tab_email') }} </label>					
					<textarea rows="20" name="courseSubmit" class="form-control input-sm markItUp mceEditor">{{ $courseSubmit }}</textarea>					 
				  </div> 

			  <div class="form-group">
					<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }}</button>
				 </div> 
			</div>	 
	  </div>	  
	
 </div>
	
<!-- Course submitted end -->

<!-- Course survey -->
	
	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title">  {!! Lang::get('core.course_survey')!!} <div class="pull-right"><a onclick="customaccordion('9')" href="javascript:void(0);"><span class="plusminus-9"><i class="fa fa-chevron-down"></i></span></a></div></div>
			<div class="sbox-content" id="wholecontent-9" style="display:none;"> 	
				  <div class="form-group" > 	
					<label for="ipt" class=" control-label ">{{ Lang::get('core.tab_email') }} </label>					
					<textarea rows="20" name="courseSurvey" class="form-control input-sm markItUp mceEditor">{{ $courseSurvey }}</textarea>					 
				  </div> 

			  <div class="form-group">
					<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }}</button>
				 </div> 
			</div>	 
	  </div>	  
	
 </div>
	
<!-- Course survey end -->

<!-- Course Student -->
	
	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title">{!! Lang::get('core.course_subscriber')!!} <div class="pull-right"><a onclick="customaccordion('10')" href="javascript:void(0);"><span class="plusminus-10"><i class="fa fa-chevron-down"></i></span></a></div></div>
			<div class="sbox-content" id="wholecontent-10" style="display:none;"> 	 	
				  <div class="form-group">
					<label for="ipt" class=" control-label ">{{ Lang::get('core.tab_email') }} </label>					
					<textarea rows="20" name="courseStudjoined" class="form-control input-sm markItUp mceEditor">{{ $courseStudjoined }}</textarea>					 
				  </div> 

			  <div class="form-group">
					<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }}</button>
				 </div> 
			</div>	 
	  </div>	  
	
 </div>
	
<!-- Course Student end -->

<!-- Course Approve -->
	
	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title"> {!! Lang::get('core.course_approval')!!} <div class="pull-right"><a onclick="customaccordion('11')" href="javascript:void(0);"><span class="plusminus-11"><i class="fa fa-chevron-down"></i></span></a></div></div>
			<div class="sbox-content" id="wholecontent-11" style="display:none;"> 	 	
				  <div class="form-group">
					<label for="ipt" class=" control-label ">{{ Lang::get('core.tab_email') }} </label>					
					<textarea rows="20" name="courseApprove" class="form-control input-sm markItUp mceEditor">{{ $courseApprove }}</textarea>					 
				  </div> 

			  <div class="form-group">
					<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }}</button>
				 </div> 
			</div>	 
	  </div>	  
	
 </div>
	
<!-- Course Approve end -->

<!-- Course Unapprove -->
	
	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title">  {!! Lang::get('core.course_unapprove')!!} <div class="pull-right"><a onclick="customaccordion('12')" href="javascript:void(0);"><span class="plusminus-12"><i class="fa fa-chevron-down"></i></span></a></div></div>
			<div class="sbox-content" id="wholecontent-12" style="display:none;"> 	 	
				  <div class="form-group">
					<label for="ipt" class=" control-label ">{{ Lang::get('core.tab_email') }} </label>					
					<textarea rows="20" name="courseUnpprove" class="form-control input-sm markItUp mceEditor">{{ $courseUnpprove }}</textarea>					 
				  </div> 

			  <div class="form-group">
					<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }}</button>
				 </div> 
			</div>	 
	  </div>	  
	
 </div>
	
<!-- Course Unapprove end -->

<!-- Course Delete -->
	
	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title">  {!! Lang::get('core.course_delete')!!} <div class="pull-right"><a onclick="customaccordion('13')" href="javascript:void(0);"><span class="plusminus-13"><i class="fa fa-chevron-down"></i></span></a></div></div>
			<div class="sbox-content" id="wholecontent-13" style="display:none;"> 	
				  <div class="form-group">
					<label for="ipt" class=" control-label ">{{ Lang::get('core.tab_email') }} </label>					
					<textarea rows="20" name="courseDelete" class="form-control input-sm markItUp mceEditor">{{ $courseDelete }}</textarea>					 
				  </div> 

			  <div class="form-group">
					<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }}</button>
				 </div> 
			</div>	 
	  </div>	  
	
 </div>
	
<!-- Course Delete end -->

<!-- Course announcement -->
	
	<div class="col-sm-6 animated fadeInRight">
		<div class="sbox  "> 
			<div class="sbox-title">  {!! Lang::get('core.course_announcement')!!} <div class="pull-right"><a onclick="customaccordion('14')" href="javascript:void(0);"><span class="plusminus-14"><i class="fa fa-chevron-down"></i></span></a></div></div>
			<div class="sbox-content" id="wholecontent-14" style="display:none;"> 	
				  <div class="form-group">
					<label for="ipt" class=" control-label ">{{ Lang::get('core.tab_email') }} </label>					
					<textarea rows="20" name="courseAnnoucement" class="form-control input-sm markItUp mceEditor">{{ $courseAnnoucement }}</textarea>					 
				  </div> 

			  <div class="form-group">
					<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }}</button>
				 </div> 
			</div>	 
	  </div>	  
	
 </div>
	
<!-- Course announcement end -->



 {!! Form::close() !!}
</div>
</div>
</div>
</div>
<script type="text/javascript">

function customaccordion(lid){
  if ($("#wholecontent-"+lid).is(':visible')) { 
    $("#wholecontent-"+lid).slideUp(300);
    $(".plusminus-"+lid).html('<i class="fa fa-chevron-down"></i>'); 
  } else {
    $("#wholecontent-"+lid).slideDown(300); 
    $(".plusminus-"+lid).html('<i class="fa  fa-chevron-up"></i>'); 
  } 
}

  tinyMCE.init({
    theme : "advanced",
    mode: "textareas",
	editor_selector : "mceEditor",
	editor_deselector : "mceNoEditor",
    theme_advanced_toolbar_location : "top",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
    + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
    + "bullist,numlist,outdent,indent",
    theme_advanced_buttons2 : "link,unlink,anchor,image,separator,"
    +"undo,redo,cleanup,code,separator,sub,sup,charmap",
    theme_advanced_buttons3 : "",
    height:"350px",
    width:"600px"
});
  </script>
@stop





