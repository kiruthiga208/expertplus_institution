@extends('layouts.app')


@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3><i class="fa fa-info"></i> {!! Lang::get('core.bse_version')!!}  <sup> 5 </sup>  <small>{{ Lang::get('core.t_generalsettingsmall') }}</small></h3>
      </div>
	  
	 
	  <ul class="breadcrumb">
		<li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('config') }}">{{ Lang::get('core.t_generalsetting') }}</a></li>
	  </ul>	  
	 
    </div>
 	<div class="page-content-wrapper">   
	@if(Session::has('message'))
	  
		   {{ Session::get('message') }}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="block-content">
	@include('bsetec.config.tab')	
<div class="tab-content m-t">
<ul class="nav nav-tabs" style="margin-bottom:10px;">
  	<li class="active"><a href="javascript::">{{ Lang::get('core.t_generalsetting') }}</a></li> 
	<li ><a href="{{ URL::to('bsetec/config/customsettings')}}">{!! Lang::get('core.customized_settings')!!}</a></li> 
</ul>
  <div class="tab-pane active use-padding" id="info">	
  <div class="sbox  "> 
  <div class="sbox-title">{{ Lang::get('core.t_generalsetting') }}</div>
  <div class="sbox-content"> 
  <div id="flogo_error"></div>
		 {!! Form::open(array('url'=>'bsetec/config/save/', 'class'=>'form-horizontal row', 'files' => true)) !!}

		<div class="col-sm-6 animated fadeInRight ">
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.fr_appname') }} </label>
			<div class="col-md-8">
			<input name="cnf_appname" type="text" id="cnf_appname" class="form-control input-sm" required  value="{{ CNF_APPNAME }}" />  
			 </div> 
		  </div>  
		  
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.fr_appdesc') }} </label>
			<div class="col-md-8">
			<input name="cnf_appdesc" type="text" id="cnf_appdesc" class="form-control input-sm" value="{{ CNF_APPDESC }}" /> 
			 </div> 
		  </div>  
		  
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.fr_comname') }} </label>
			<div class="col-md-8">
			<input name="cnf_comname" type="text" id="cnf_comname" class="form-control input-sm" value="{{ CNF_COMNAME }}" />  
			 </div> 
		  </div>      

		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.fr_emailsys') }} </label>
			<div class="col-md-8">
			<input name="cnf_email" type="text" id="cnf_email" class="form-control input-sm" value="{{ CNF_EMAIL }}" /> 
			 </div> 
		  </div>   
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4"> {!! Lang::get('core.fr_multilanguage')!!} <br /> <small> {!! Lang::get('core.layout_interface') !!} </small> </label>
			<div class="col-md-8">
				<div class="checkbox">
					<input name="cnf_multilang" type="checkbox" id="cnf_multilang" value="1"
					@if(CNF_MULTILANG ==1) checked @endif
					  />  {{ Lang::get('core.fr_enable') }} 
				</div>	
			 </div> 
		  </div> 
		     
		   <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.fr_mainlanguage') }} </label>
			<div class="col-md-8">

					<select class="form-control" name="cnf_lang">

					@foreach(SiteHelpers::langOption() as $lang)
						<option value="{{  $lang['folder'] }}"
						@if(CNF_LANG ==$lang['folder']) selected @endif
						>{{  $lang['name'] }}</option>
					@endforeach
				</select>
			 </div> 
		  </div>   
		      
		   <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4"> {!! Lang::get('core.Frontend_Template') !!} </label>
			<div class="col-md-8">

					<select class="form-control" name="cnf_theme">
					@foreach(SiteHelpers::themeOption() as $t)
						<option value="{{  $t['folder'] }}"
						@if(CNF_THEME ==$t['folder']) selected @endif
						>{{  $t['name'] }}</option>
					@endforeach
				</select>
			 </div> 
		  </div> 

		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4"> {!! Lang::get('core.Development_mode')!!}   </label>
			<div class="col-md-8">
				<div class="checkbox">
					<input name="cnf_mode" type="checkbox" id="cnf_mode" value="1"
					@if (defined('CNF_MODE') &&  CNF_MODE =='production') checked @endif
					  />  {!! Lang::get('core.Production') !!}
				</div>
				<small> {!! Lang::get('core.general_msg')!!}</small>	
			 </div> 
		  </div> 

		   <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4"> {!! Lang::get('core.rtl_settings')!!}   </label>
			<div class="col-md-8">
				<div class="checkbox">
					<input name="cnf_rtl" type="checkbox" id="cnf_rtl" value="1"
					@if (defined('CNF_RTL') &&  CNF_RTL == '1') checked @endif
					  />  {!! Lang::get('core.rtl_settings') !!}
				</div>
			 </div> 
		  </div> 
		  <input type="hidden" name="currency_mode" value="{!! CNF_CURRENCY !!}">
		  <!-- <div class="form-group">
		  	<label for="ipt" class=" control-label col-md-4"> {!! Lang::get('core.currency_mode')!!}  </label>
		  	<div class="col-md-8">
				<div class="selectbox">
					<select name="currency_mode" class="form-control">
					@foreach(SiteHelpers::getCurrency() as $currency => $value)
						@if(CNF_CURRENCY == $currency)
							<option value="{!! $currency !!}" selected>{!! $value !!}</option>
						@else
							<option value="{!! $currency !!}">{!! $value !!}</option>
						@endif
					@endforeach
					</select>
				</div>
				<small> {!! Lang::get('core.currency_msg') !!}</small>	
			 </div> 
		  </div> -->		  
		  
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">&nbsp;</label>
			<div class="col-md-8">
				<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }} </button>
			 </div> 
		  </div> 
		</div>

		<div class="col-sm-6 animated fadeInRight ">

		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4"> {{ Lang::get('core.Metakey') }}</label>
			<div class="col-md-8">
				<textarea class="form-control input-sm" name="cnf_metakey">{{ CNF_METAKEY }}</textarea>
			 </div> 
		  </div> 

		   <div class="form-group">
		    <label  class=" control-label col-md-4"> {{ Lang::get('core.Meta_Description') }}</label>
			<div class="col-md-8">
				<textarea class="form-control input-sm"  name="cnf_metadesc">{{ CNF_METADESC }}</textarea>
			 </div> 
		  </div>  

		   <div class="form-group">
		    <label  class=" control-label col-md-4">{!! Lang::get('core.Backend_Logo')!!}</label>
			<div class="col-md-8">
			<div id="logo">
				<input type="file" data-width="195" data-height="50" data-id="blogo_src" name="logo" />
			</div>	
				<p> <i>{!! Lang::get('core.logo_info')!!} </i> </p>
				<div style="padding:5px; border:solid 1px #1ab394; background:#233646; width:auto;">
				 	@if(file_exists(asset('uploads/images/'.CNF_LOGO)) && CNF_LOGO !='')
				 	<img id="blogo_src" src="{{ asset('uploads/images/'.CNF_LOGO)}}" style="max-width:195px;max-height:50px;" alt="{{ CNF_APPNAME }}" />
				 	@else
					<img id="blogo_src" src="{{ asset('uploads/images/'.CNF_LOGO)}}" alt="{{ CNF_APPNAME }}" />
					@endif	
				</div>				
			 </div> 
		  </div>  
		  <div class="form-group">
		    <label  class=" control-label col-md-4">{!! Lang::get('core.Frontend_Logo')!!}</label>
			<div class="col-md-8">
			<div id="flogo">
				<input type="file" name="flogo" data-width="195" data-height="50" data-id="flogo_src"  />
			</div>	
				<p> <i>{!! Lang::get('core.logo_info')!!} </i> </p>
				<div style="padding:5px; border:solid 1px #1ab394;  width:auto;">
				 	@if(file_exists(asset('uploads/images/'.CNF_FLOGO)) && CNF_FLOGO !='')
				 	<img  id="flogo_src" src="{{ asset('uploads/images/'.CNF_FLOGO)}}" style="max-width:195px;max-height:50px;" alt="{{ CNF_APPNAME }}" />
				 	@else
					<img id="flogo_src" src="{{ asset('uploads/images/'.CNF_FLOGO)}}" alt="{{ CNF_APPNAME }}" />
					@endif	
				</div>				
			 </div> 
		  </div> 

		  <div class="form-group">
		    <label  class=" control-label col-md-4">{!! Lang::get('core.Fav_Icon')!!}</label>
			<div class="col-md-8">
			<div id="fav_icon">
				<input type="file" name="fav_icon" data-width="64" data-height="64" data-id="fav" />
			</div>
				<p> <i>{!! Lang::get('core.fav_icon_info')!!} </i> </p>
				<div style="padding:5px; border:solid 1px #1ab394;  width:auto;">
				 	@if(file_exists(asset('/uploads/images/'.CNF_FAV)) && CNF_FAV !='')
				 	<img src="{{ asset('uploads/images/'.CNF_FAV)}}" id="fav" alt="{{ CNF_APPNAME }}" />
				 	@else
					<img src="{{ asset('uploads/images/'.CNF_FAV)}}" id="fav" alt="{{ CNF_APPNAME }}" />
					@endif	
				</div>				
			 </div> 
		  </div>		  

		</div>  
		{!! Form::close() !!}
	</div>
	</div>	 
</div>
</div>
</div>
</div>
<script type="text/javascript">
	$('body').on('change','input:file',function(){
		var _URL = window.URL || window.webkitURL,
		file, img,height,file_html='',
		id = $(this).data('id'),
		height = $(this).data('height'),
		width = $(this).data('width'),
		attrName = $(this).attr('name');
		selector = $('#'+id);
		selector.addClass('hide');
	 if (this.files && this.files[0]) {
	   		img = new Image();
	        img.src = _URL.createObjectURL(this.files[0]);
	        if(id=='fav') image_type = ['gif','png','jpg','jpeg','ico']; else image_type = ['gif','png','jpg','jpeg']; 
	        ext = $(this).val().split('.').pop().toLowerCase();
			if($.inArray(ext, image_type) == -1) {
			   alert("invalid image");
			   file_html = '<input type="file" name="'+attrName+'" data-width="'+width+'" data-height="'+height+'" data-id="'+id+'" />';
			   $('input[data-id="'+id+'"]').remove();
			   $('#'+attrName).html(file_html);
			   selector.attr('src','');
			   return false;
			}else{
				selector.attr('src',img.src);
				img.onload = function(){
					if(this.width > width || this.height > height){
						alert('Your Selected Image size '+this.width+'px * '+this.height+'px.Please use image dimension '+width+'px * '+height+'px');
					   	file_html = '<input type="file" name="'+attrName+'" data-width="'+width+'" data-height="'+height+'" data-id="'+id+'" />';
				   		$('input[data-id="'+id+'"]').remove();
				   		$('#'+attrName).html(file_html);
				   		selector.attr('src','');
					   	return false;
					}
				}
			}
			
		}
		setTimeout(function(){
			selector.removeClass('hide');
		},1000);
	});

</script>
@stop
