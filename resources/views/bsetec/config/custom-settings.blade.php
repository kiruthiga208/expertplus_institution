@extends('layouts.app')
@section('content')
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3><i class="fa fa-info"></i> {!! Lang::get('core.bse_version')!!}  <sup> 5 </sup>  <small>{{ Lang::get('core.t_generalsettingsmall') }}</small></h3>
      </div>
	  <ul class="breadcrumb">
		<li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('config') }}">{{ Lang::get('core.t_generalsetting') }}</a></li>
	  </ul>	  
	 
    </div>
 	<div class="page-content-wrapper">   
	@if(Session::has('message'))
	  
		   {{ Session::get('message') }}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="block-content">
	@include('bsetec.config.tab')	
<div class="tab-content m-t">
<ul class="nav nav-tabs" style="margin-bottom:10px;">
  	<li><a href="{{ URL::to('bsetec/config')}}">{!! Lang::get('core.tab_siteinfo')!!}</a></li> 
	<li class="active" ><a href="Javascript::">{!! Lang::get('core.service_info')!!}</a></li> 
</ul>
  <div class="tab-pane active use-padding" id="info">
  <div class="sbox"> 
  <div class="sbox-title"><h3>{!! Lang::get('core.service_info')!!}</h3></div>
	<div class="sbox-content"> 
		{!! Form::open(array('url'=>'bsetec/config/customsettings/', 'class'=>'form-horizontal row', 'files' => true)) !!}
			<div class="col-sm-12 animated fadeInRight">
	          <div class="form-group">
	           <label class="control-label" for="ipt"> <h5>{!! Lang::get('core.home_page_service')!!} :</h5> </label>
	           <textarea name="page_services_content" rows="15" class="form-control">@if(isset($page_contents['page_services_content'])) {!! $page_contents['page_services_content'] !!} @endif</textarea> 
			  </div> 

			    <div class="form-group">
	           <label class="control-label" for="ipt"> <h5>{!! Lang::get('core.home_page_about')!!} :</h5> </label>
	           <textarea name="page_about_content" rows="15" class="form-control">@if(isset($page_contents['page_about_content'])) {!! $page_contents['page_about_content'] !!} @endif</textarea> 
			  </div> 

			   <div class="form-group">
	           <label class="control-label" for="ipt"> <h5>{!! Lang::get('core.Advertising_Content')!!} :</h5> </label>
	           <textarea name="advertising_footer_content" rows="15" class="form-control">@if(isset($page_contents['advertising_footer_content'])) {!! $page_contents['advertising_footer_content'] !!} @endif</textarea> 
			  </div> 

			  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">&nbsp;</label>
			<div class="col-md-8">
				<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }} </button>
			 </div> 
		  </div> 

	</div>
 {!! Form::close() !!}
</div>	
</div>
</div>
</div>
</div>
@stop