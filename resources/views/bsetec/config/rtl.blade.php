@extends('layouts.app')

@section('content')


<div class="page-content row">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-title">
			<h3><i class="fa fa-gears"></i> {!! Lang::get('core.rtl_settings')!!}</h3>
		</div>

		<ul class="breadcrumb">
			<li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
			<li><a href="{{ URL::to('config') }}">{!! Lang::get('core.rtl_settings')!!}</a></li>
			
		</ul>

	</div>

	<div class="page-content-wrapper">  
		@if(Session::has('message'))

		{{ Session::get('message') }}

		@endif
		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>		
		<div class="block-content">
			@include('bsetec.config.tab')		
			<div class="tab-content m-t">
				<div class="tab-pane active use-padding row" id="info">	

				
					{!! Form::open(array('url'=>'bsetec/config/updatertl/', 'class'=>'form-horizontal')) !!}

					<div class="col-sm-12">
						<div class="sbox   animated fadeInRight"> 
							<div class="sbox-title"> {!! Lang::get('core.rtl_settings')!!} </div>
							<div class="sbox-content"> 	
								
								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4"> {!! Lang::get('core.rtl_settings')!!}  </label>	
									<div class="col-sm-8">
										<div class="checkbox">
											<input name="rtl_approve" type="checkbox" id="rtl_approve" value="1" @if ($CNF == '1') checked @endif  />  
										</div>		
									</div>	
								</div>

								<div class="form-group">
									<label for="ipt" class=" control-label col-md-4">&nbsp;</label>
									<div class="col-md-8">
										<button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>
									</div> 

								</div>	  
							</div>
						</div>
					</div>
					{!! Form::close() !!}

					


				</div>
			</div>
		</div>
	</div>

	@stop




