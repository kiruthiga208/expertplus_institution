@extends('layouts.app')


@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3><i class="fa fa-info"></i> {!! Lang::get('core.Theme_Management')!!}  <small>{!! Lang::get('core.Manage_themes')!!}</small></h3>
      </div>
	  
	 
	  <ul class="breadcrumb">
		<li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('config') }}">{!! Lang::get('core.Theme_Management')!!}</a></li>
	  </ul>	  
	 
    </div>
 	
 	<div class="page-content-wrapper">	 	

		@if(Session::has('message'))
		  
			   {{ Session::get('message') }}
		   
		@endif
		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>		
	<div class="block-content">
		@include('bsetec.config.tab')	
	<div class="tab-content m-t">
	  <div class="tab-pane active use-padding" id="info">	
	  <div class="sbox  animated fadeInRight"> 
	  <div class="sbox-title"></div>
	  <div class="sbox-content"> 	
      <div class="table-responsive"> 
    <table class="table table-striped ">
        <thead>
			<tr>
				<th>{!! Lang::get('core.No')!!}</th>
				<th>{!! Lang::get('core.Theme')!!}</th>
				<th>{!! Lang::get('core.Author')!!}</th>
				<th>{!! Lang::get('core.Preview')!!}</th>
				<th>{!! Lang::get('core.Is_default')!!}</th>

				<th width="70" >{{ Lang::get('core.btn_action') }}</th>
			  </tr>
        </thead>

        <tbody>
			@foreach(SiteHelpers::themeOption() as $key => $value)
			<tr>
				<td>{{ $key+1 }}</td>
				<td>{{ $value['name'] }}</td>
				<td>{{ $value['author'] }}</td>
				@php 
				$base= url('/');
				$url=str_replace('public', '', $base);
				$imgurl=$url.'/resources/views/layouts'.'/'.$value['folder'].'/preview.png' ;
				@endphp
				<td><img src="{{ $imgurl }}" width="100px" height="100px"/></td>
				<td>
					@if($theme ==$value['folder'])
						<span class="label label-info">{!! Lang::get('core.Yes')!!}</span>
					@else
						{!! Lang::get('core.No')!!}
					@endif
				</td>
				<td>
					<a href="{{ URL::to('preview?theme='.$value['folder'])}}" class="tips btn btn-xs btn-info" target="_blank">{!! Lang::get('core.Preview')!!}</a>
					<a href="{{ URL::to('bsetec/config/change/'.$value['folder'])}}" class="tips btn btn-xs btn-success" onclick="return confirm('Are you sure want to make this theme as default?');">{!! Lang::get('core.Make-Default')!!}</a>

				</td>
			</tr>			
            @endforeach
              
        </tbody>
      
    </table>
	</div>
</div>
</div>
</div>
</div>
</div>


</div>	








@stop