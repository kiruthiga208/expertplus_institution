
  <link href="{{ asset('assets/bsetec/themes/theme3/css/theme3.css') }}" rel="stylesheet">

<header class="expert navbar-fixed-top"> 
    <div class="container">
        <div class="row">
            <div class="col-xs-5 col-sm-7">
                <div class="logo">
                    @if(file_exists('./uploads/images/'.CNF_FLOGO) && CNF_FLOGO !='')
                    <a href="{{ URL::to('') }}/" class="site-logo">
                        <img src="{{ asset('uploads/images/'.CNF_FLOGO)}}" alt="{{ CNF_APPNAME }}" class="img-responsive" />
                    </a>
                    @else
                    <a href="{{ URL::to('') }}/" class="site-logo">
                        
                        <img src="{{ asset('assets/bsetec/images/logo.png')}}" alt="{{ CNF_APPNAME }}" />
                    </a>
                    @endif  
                    
                    
                </div>
                @if(Auth::check())  
                @php($sitecategories = \bsetecHelpers::sitecategories() )
<div class="left_block clearfix">
    <div class="browse_block">
        <div class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Lang::get('core.Browse_Course') }}</a>        
            <ul class="dropdown-menu mCustomScrollbar">
              @foreach($sitecategories as $category)
              <li><a href="{{ url('category/'.$category->slug)}}">{!! $category->name !!}</a></li>
              @endforeach
          </ul>
      </div>
  </div>
  <div class="search_s"><form role="search" method="GET" action="{{ url('search') }}"><input type="search" name="q" placeholder="{!! Lang::get('core.search') !!}"/><input type="submit" value="go"></form></div>
</div>
@endif
</div>

<div class="categ">
    <div class="mobile_menu">
        <div class="menu"> 
            <div class="icon-close"> <a>{{ Lang::get('core.close') }}</a></div>
            <div class="sidebar_block clearfix">
               <h3> {{ Lang::get('core.categories') }}</h3>
               <ul>
                
                @foreach(\bsetecHelpers::siteCategories() as $category)
                <li><a href="{{ url( 'category/'.$category->slug) }}">{{ $category->name }}</a></li>
                @endforeach
            </ul>

        </div>
    </div>

    <div class="jumbotron">
        <div class="icon-menu"> <i class="fa fa-bars"></i></div>
    </div>
</div>
</div>

<div class="col-xs-7 col-sm-5">
 
    <ul class="home_menu list-unstyled list-inline pull-right">
        
       @if(!Auth::check() && !\Session::has('uid'))    
       
       <li><a href="{{ URL::to('course') }}">{!! Lang::get('core.course') !!}</a></li>                       
       <li><a href="{{ URL::to('user/login') }}">{{ Lang::get('core.signin') }}</a></li>
       @if(CNF_REGIST !='false')<li><a href="{{ URL::to('user/register') }}">{{ Lang::get('core.signup') }}</a></li>@endif
       @else
       <li><a href="{{ URL::to('/questionanswer/view') }}"><i class="fa fa-question-circle" aria-hidden="true"></i> {!! Lang::get('core.askaquestion') !!} </a></li>
       <li class="create_course"><a href="{{ URL::to('course') }}">{!! Lang::get('core.courses') !!}</a></li>
       <li class="my_course "><a href="{{ URL::to('user/mycourse') }}" class="ignorelink">{!! Lang::get('core.mycourse') !!}</a></li>
<li class="dropdown user_profile"><a class="dropdown-toggle ignorelink" data-toggle="dropdown" href="javascript::"> <span class="profile_img">{!! \SiteHelpers::customavatar(\Auth::user()->email,\Auth::user()->id,'small') !!}</span><b class="caret"></b></a>
    
    <ul class="dropdown-menu">
    @if (defined('CNF_CURRENCY'))
      @php( $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
    @endif
        <li><h5><span class="profile_pic">{!! \SiteHelpers::customavatar(\Auth::user()->email,\Auth::user()->id,'small') !!}</span><span class="user_name">{!! Session::get('fid') !!}</span></h5></li>
            <li><a href="{{ URL::to('profile/'.\bsetecHelpers::getusername()) }}"><i class="fa fa-pencil fa-fw"></i> {!! Lang::get('core.myprofile') !!}</a></li>
            <li><a href="{{ URL::to('user/credits') }}"><i class="fa fa-money"></i> {!! Lang::get('core.my_credits') !!}
             <?php
             $cmtamt=\bsetecHelpers::getUserCredits(); 
             $uid=\Auth::user()->id;

             ?>
             {!! SiteHelpers::getCurrencymethod($uid,$cmtamt) !!}</a></li>
            <li><a href="{{ URL::to('user/profile') }}"><i class="fa fa-pencil fa-fw"></i> {!! Lang::get('core.settings') !!}</a></li>
            <li><a href="{{ URL::to('customcourserequest/view') }}"><i class="icon-book"></i> Custom Course Request</a></li>
            <li class="mobile"><div class="search_s"><form role="search" method="GET" action="{{ url('search') }}"><input type="search" name="q" placeholder="{!! Lang::get('core.search') !!}"/><input type="submit" value="go"></form></div></li>
            <li><a href="{{ URL::to('user/logout') }}" class="ignorelink"><i class="fa  fa-sign-out"></i> {{ Lang::get('core.m_logout') }}</a></li>        
        </ul>

    </li>
    
    @endif 
</ul>
</div>

</div>
</div>
</header>

         