@extends('layouts.app')

@section('content')
{!! Html::script('assets/bsetec/js/plugins/tinymce/jscripts/tiny_mce/jquery.tinymce.js')!!}
{!! Html::script('assets/bsetec/js/plugins/tinymce/jscripts/tiny_mce/tiny_mce.js')!!}
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {!! Lang::get('core.tab_translation')!!}   <small> {!! Lang::get('core.manage_translation')!!} </small></h3>
      </div>

      <ul class="breadcrumb"> 
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('config') }}"> {!! Lang::get('core.error_logs')!!} </a></li>
      </ul>
	</div> 	  


 	<div class="page-content-wrapper m-t">  
 	@include('bsetec.config.tab',array('active'=>'translation'))
	@if(Session::has('message'))
	  
		   {{ Session::get('message') }}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>

	 
<div class="tab-content m-t ">
		<div class="sbox   animated fadeInUp">  
			<div class="sbox-title"> {!! Lang::get('core.Languange_Manager')!!} </div>
			<div class="sbox-content"> 

	<div class="col-sm-8">
	
		<h4> {!! Lang::get('core.Languange_Manager')!!} </h4>
		<hr />
		<ul class="nav nav-tabs" >
		@foreach($files as $f)
			@if($f != "." and $f != ".." and $f != 'info.json')
			<li @if($file == $f) class="active" @endif  >
			<a href="{{ URL::to('bsetec/config/translation?edit='.$lang.'&file='.$f)}}">{{ $f }} </a></li>
			@endif
		@endforeach
		</ul>
		<hr />
		<input type="hidden" name="check" value="true" class="trans" />
		 {!! Form::open(array('url'=>'bsetec/config/savetranslation/', 'class'=>'form-vertical ')) !!}
		<table class="table table-striped">
			<thead>
				<tr>
					<th> {!! Lang::get('core.Pharse')!!} </th>
					<th> {!! Lang::get('core.tab_translation')!!} </th>

				</tr>
			</thead>
			<tbody class="append_key">	
				@foreach($stringLang as $key => $val)  
					@if(!is_array($val)) 
					<tr @if(isset($new)) @if($new != $key) style="display:none;" @endif @endif >	
						<td id="{{ $key }}" class="trans" >{!! $key !!}</td>
						<td><input type="textarea" name="{!! $key !!}" value="{!!  $val !!}" class="form-control" />
						</td>
					</tr>
					@else
					@if(count($val)>0)
						@foreach($val as $k=>$v)
							<tr @if(isset($new)) @if($new != $key) style="visibility:hidden;" @endif @endif>	
								<td id="{{ $key }}" class="trans" >{!! $key .' - '.$k !!}</td>
								@php $keyss= $key[$k] @endphp
								<td><input type="textarea" name="@php  $keyss @endphp" value="@php ($v)" class="form-control" />
								</td>
							</tr>						
				@endforeach
				@endif
				@endif
				@endforeach
			</tbody>
			
		</table>
		@if(isset($new))
			@if($new == "no_record")
			<span class="text-danger">{{ "NO RECORDS FOUND" }}</span> <br>
			@endif
			@endif
		<input type="hidden" name="lang" value="{{ $lang }}"  />
		<input type="hidden" name="file" value="{{ $file }}"  />
		<button type="submit" class="btn btn-info"> {!! Lang::get('core.tr_save')!!}</button>
		{!! Form::close() !!}

	</div> 
	
	
	<div class="clr"></div>
	</div>

</div>
</div></div></div>


<script type="text/javascript">

$(document).ready( function() {

	$('input[class="form-control"]').each(function(index,item){

		var tag = item.value;
		var name = item.name;

		if (tag.match(/<(\w+)((?:\s+\w+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)>/)) {
      var textbox = $(document.createElement('textarea')).attr({
        
        "name" : name,
        value : tag,
        "class" : $(this).attr("class"),
        rows : 6
    }).width('80%');
      textbox = textbox.text(tag);
    $(this).replaceWith(textbox);
   }
    
});
	tinymce.init({	
				selector: 'textarea',
			 });

});

</script>




@endsection