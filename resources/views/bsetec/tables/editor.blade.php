{!! Form::open(array('url'=>'bsetec/tables/mysqleditor', 'class'=>'form-vertical','id'=>'saveform' , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		<div class="result"></div>
		<div class="form-group">
			<label class=""> {{ Lang::get('core.mysql_query') }} <br />
			

			</label>	
			<small> {{ Lang::get('core.query_execute') }} </small>		
				<textarea name="statement" required="true" rows="15" class="form-control"></textarea>
			
		</div>
	
		<div class="form-group">
			<label class="">  </label>			
			<button type="submit" class="btn btn-sm btn-primary"> {{ Lang::get('core.execute') }} </button>			
		</div>
	
{!! Form::close() !!}		

  <script type="text/javascript">
 $(document).ready(function(){
 		var form = $('#saveform');
		form.parsley();
		form.submit(function(){
			
			if(form.parsley('isValid') == true){			
				var options = { 
					dataType:      'json', 
					beforeSubmit :  showRequest,
					success:       showResponse  
				}  
				$(this).ajaxSubmit(options); 
				return false;
							
			} else {
				return false;
			}	
			return false;	
		
		});		

 });
function showRequest()
{
	$('.ajaxLoading').show();
}  
function showResponse(data)  {	

	alert(data.status);
	if(data.status == 'success')
	{
		window.location.href = '{{ URL::to("bsetec/tables") }}';
		
	} else {
		var message = 'Ops Someting Goes Wrong !!<br />' + data.message.errorInfo[2];
		notyMessageError(message);	
		
	}	
	$('.ajaxLoading').hide();
} 

</script>	