@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('forumcomments?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('forumcomments?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
	
				<tr>
				<td width='30%' class='label-view text-right'>{!! Lang::get('core.CommentID')!!}</td>
				<td>{{ $row->comment_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{!! Lang::get('core.User Id')!!}</td>
				<td>{{ $row->user_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{!! Lang::get('core.Forum Id')!!}</td>
				<td>{{ $row->forum_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{!! Lang::get('core.Comment')!!}</td>
				<td>{{ strip_tags($row->comment) }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{!! Lang::get('core.Created At')!!}</td>
				<td>{{ $row->created_at }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{!! Lang::get('core.Updated At')!!}</td>
				<td>{{ $row->updated_at }} </td>

				</tr>
				
		</tbody>	
	</table>    
	
	</div>
</div>	

	</div>
</div>
	  
@stop