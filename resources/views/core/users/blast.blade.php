@extends('layouts.app')

@section('content')

<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3>  {{ Lang::get('core.t_blastemail') }}  <small>{{ Lang::get('core.t_blastemailsmall') }}</small></h3>
      </div>

      <ul class="breadcrumb">
       <li><a href="{{ URL::to('dashboard') }}"> {{ Lang::get('core.dashboard') }} </a></li>
        <li class="active">{{ Lang::get('core.t_blastemail') }}  </li>
      </ul>
    </div>
	
 <div class="page-content-wrapper m-t">  
  <ul class="nav nav-tabs" style="margin-bottom:10px;">
    <li ><a href="{{ URL::to('core/users')}}"><i class="fa fa-user"></i> {{ Lang::get('core.Users') }} </a></li>
    <li class="active"><a href="{{ URL::to('core/users/blast')}}"><i class="fa fa-envelope"></i> {{ Lang::get('core.send_email') }} </a></li>
  </ul> 

  @if(Session::has('message'))    
       {{ Session::get('message') }}
  @endif  
    
    <div class="col-md-12">
              <ul class="parsley-error-list">
                @foreach($errors->all() as $error)
                  <li class="alert alert-danger">{{ $error }}</li>
                @endforeach
              </ul>                
           </div>
<!-- Start blast email -->

{!! Form::open(array('url'=>'core/users/doblast/', 'class'=>'form-horizontal ')) !!}
          <div class="form-group">
          <label for="ipt" class=" control-label col-md-3">  </label>
           
          </div> 
		  
<div class="col-sm-6">
          <div class="form-group  " >
          <label for="ipt" class=" control-label col-md-3">  {{ Lang::get('core.fr_emailsubject') }}   </label>
          <div class="col-md-9">
               {!! Form::text('subject',null,array('class'=>'form-control', 'placeholder'=>'')) !!} 
           </div> 
          </div> 
          <div class="form-group  " >
          <label for="ipt" class=" control-label col-md-3"> {{ Lang::get('core.fr_emailsendto') }}   </label>
          <div class="col-md-9">
          @foreach($groups as $row)
            <label class="checkbox">
                {!! Form::checkbox('groups[]', $row->group_id,0,array()) !!} {{ $row->name }}
            </label>
          @endforeach
           </div> 
          </div>  		  
		  
</div>
<div class="col-sm-6">
          <div class="form-group  " >
          <label for="ipt" class=" control-label col-md-3"> {{ Lang::get('core.Status') }} </label>
          <div class="col-md-9">
          @php( $status_array = array('3'=>'All Status',1=>'Active',0=>'Unconfirmed',2=>'Blocked') )       
         @foreach($status_array as $key=>$value)
            <label class="radio">
                {!! Form::radio('status',$key,0,array()) !!} {!! $value !!}
            </label>
          @endforeach  
          </div> 
        </div>  
</div>
 
 <div class="col-sm-12">


 
          <div class="form-group "  >
         
          <div style=" padding:10px;">
		   <label for="ipt" class=" control-label "> {{ Lang::get('core.fr_emailmessage') }} </label>
          {!! Form::textarea('message',null,array('class'=>'form-control markItUp', 'placeholder'=>'')) !!} 
       </div>
           <p> {{ Lang::get('core.fr_emailtag') }} : </p>
           <small> [fullname] [first_name] [last_name] [email]  </small>
          </div> 
          <div class="form-group" >
          <label for="ipt" class=" control-label col-md-3"> </label>
          <div class="col-md-9">
              <button type="submit" name="submit" class="btn btn-primary">{{ Lang::get('core.sb_send') }} {{ Lang::get('core.mail') }} </button>
           </div> 
          </div> 
	</div>	                   
     {!! Form::close() !!}
    <!-- / blast email -->
</div>
</div>      
@stop