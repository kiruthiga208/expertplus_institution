@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
		  <ul class="breadcrumb">
			<li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.dashboard') }}</a></li>
			<li><a href="{{ URL::to('core/pages') }}">{{ $pageTitle }}</a></li>
			<li class="active"> {{ Lang::get('core.add') }} </li>
		  </ul>	  
    </div>

<div class="page-content-wrapper">
	@if(Session::has('message'))	  
		   {{ Session::get('message') }}
	@endif	
		
		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
		 {!! Form::open(array('url'=>'core/pages/save/'.$row['pageID'], 'class'=>'form-vertical row ','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

			<div class="col-sm-8 ">
				<div class="sbox">
					<div class="sbox-title">{{ Lang::get('core.page_content') }}</div>	
					<div class="sbox-content">		

						<ul class="nav nav-tabs" >
						  <li class="active"><a href="#info" data-toggle="tab"> {{ Lang::get('core.page_content') }} </a></li>
						  <li ><a href="#meta" data-toggle="tab"> {{ Lang::get('core.meta_des') }}</a></li>
						</ul>	

						<div class="tab-content">
						  <div class="tab-pane active m-t" id="info">
							  <div class="form-group  " >
								
								<div class="" style="background:#fff;">
								  <textarea name='content' required rows='35' id='content' class='form-control markItUp' 
									 >@if($row['pageID'] !='') {{htmlentities($content)}}@else {{htmlentities(Input::old('content'))}} @endif</textarea> 
								 </div> 
							  </div> 						  

						  </div>

						  <div class="tab-pane m-t" id="meta">

					  		<div class="form-group  " >
								<label class=""> {{ Lang::get('core.metakey') }} </label>
								<div class="" style="background:#fff;">
								  <textarea name='metakey' rows='5' id='metakey' class='form-control markItUp'>{{ $row['metakey'] }}</textarea> 
								 </div> 
							  </div> 

				  			<div class="form-group  " >
								<label class="">  {{ Lang::get('core.Meta_des') }} </label>
								<div class="" style="background:#fff;">
								  <textarea name='metadesc' rows='10' id='metadesc' class='form-control markItUp'>{{ $row['metadesc'] }}</textarea> 
								 </div> 
							  </div> 							  						  

						  </div>

						</div>  
					 </div>
				</div>	
		 	</div>		 
		 
		 <div class="col-sm-4 ">
			<div class="sbox">
				<div class="sbox-title"> {{ Lang::get('core.page_info') }} </div>	
				<div class="sbox-content">						
				  <div class="form-group hidethis " style="display:none;">
					<label for="ipt" class="">{{ Lang::get('core.page_id') }}</label>
					
					  {!! Form::text('pageID', $row['pageID'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
					
				  </div> 					
				  <div class="form-group  " >
					<label for="ipt" > {{ Lang::get('core.Title') }} </label>
					
					  {!! Form::text('title', $row['title'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
					
				  </div> 					
				  <div class="form-group  " >
					<label for="ipt" > {{ Lang::get('core.Alias') }} </label>
					
					  {!! Form::text('alias', $row['alias'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'   )) !!} 
					 
				  </div> 					
				  <div class="form-group  " >
					<label for="ipt" > {{ Lang::get('core.filename') }} </label>
					
					  <input name="filename" type="text" class="form-control"
					  @if($row['pageID'] !='') value="{{$row['filename']}}" readonly="1" @else value="{{Input::old('filename')}}" @endif required
					  />
					
				  </div> 
				  <div class="form-group  " >
				  <label for="ipt"> {{ Lang::get('core.to_view_page') }} </label>
					@foreach($groups as $group) 
					<label class="checkbox">					
					  <input  type='checkbox' name='group_id[{{ $group['id'] }}]'    value="{{ $group['id'] }}"
					  @if($group['access'] ==1 or $group['id'] ==1)
					  	checked
					  @endif				 
					   /> 
					  {{ $group['name'] }}
					</label>  
					@endforeach	
						  
				  </div> 
				  <div class="form-group  " >
					<label> {{ Lang::get('core.guest_view') }}  </label>
					<label class="checkbox"><input  type='checkbox' name='allow_guest' 
 						@if($row['allow_guest'] ==1 ) checked  @endif	
					   value="1" /> {{ Lang::get('core.allow_guest') }} </lable>
				  </div>


				  <div class="form-group hidethis " style="display:none;">
					<label for="ipt" class=" control-label col-md-4 text-right"> {{ Lang::get('core.Created') }} </label>
					<div class="col-md-8">
					  {!! Form::text('created', $row['created'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
					 </div> 
				  </div> 					
				  <div class="form-group hidethis " style="display:none;">
					<label for="ipt" > {{ Lang::get('core.updaet') }} </label>
			
					  {!! Form::text('updated', $row['updated'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
					
				  </div>	
				  <div class="form-group  " >
					<label> {{ Lang::get('core.Status') }} </label>
					<label class="radio">
						{!! Form::radio('status','enable',($row['status'] =='enable') ? true:false ,array()) !!} {{ Lang::get('core.Enable') }}
					</label> 
					<label class="radio">
						{!! Form::radio('status','disable',($row['status'] =='disable') ? true:false ,array()) !!}  {{ Lang::get('core.disable') }}					
					</label> 					 
				  </div> 

				  <div class="form-group  " >
					<label> {{ Lang::get('core.template') }} </label>
					<label class="radio">	
					{!! Form::radio('template','frontend',($row['template'] =='frontend') ? true:false ,array()) !!} {{ Lang::get('core.frontend') }}					
					</label> 
					<label class="radio">
					{!! Form::radio('template','backend',($row['template'] =='backend') ? true:false ,array()) !!} {{ Lang::get('core.backend') }}									
					</label> 					 
				  </div> 				  
			  <div class="form-group">
				<button type="submit" class="btn btn-primary "> {{ Lang::get('core.sb_submit') }} </button>
			  </div> 
			 </div>
			</div>				  				  
				  		
			</div>

		 {!! Form::close() !!}
	</div>
</div>	

<style type="text/css">
.note-editor .note-editable { height:500px;}
</style>			 	 
@stop