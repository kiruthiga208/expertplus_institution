<section class="slider" >
<div class="container">
	<div class="row">
		<div class="col-md-7">
			<div class="overview">
				<h2 class="title animated fadeInUp delayp1" style="opacity: 0;">BSEtec helps you to build Awesome app</h2>
				<p> Just turn on your current mysql database into great application within minutes using BSEtec </p>
				<ul class="summary animated fadeInUp delayp2" style="opacity: 0;">
					<li><i class="fa fa-dot-circle-o"></i> Complete frontend & backend Platform</li>
					<li><i class="fa fa-dot-circle-o"></i> Create Complete CRUD within a minutes </li>
					<li><i class="fa fa-dot-circle-o"></i> Deliver Work to your clients faster than before</li>
					<li><i class="fa fa-dot-circle-o"></i> Many Updates are comming soon..</li>
				</ul>
				<p>* Discover new experience on building Awesome application </p>
										
			</div>	
		</div>
		
		<div class="col-md-5">
			<div class="laptop">
				<img src="{{ asset('assets/bsetec/themes/theme1/img/dashboard2.png') }}" class="img-responsive" >
				<p class="text-center"> Fully Responsive Design <br />
				<i>bsetec Builder has been designed to work on all device variants </i>
				</p>
			</div>	
		</div>
	</div>
</div>

</section>



	





