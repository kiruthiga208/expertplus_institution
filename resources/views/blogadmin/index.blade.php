@extends('layouts.app')

@section('content')
@php ( usort($tableGrid, "SiteHelpers::_sort") )
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
        <li class="active">{{ $pageTitle }}</li>
      </ul>	  
	  
    </div>

    <div class="page-content-wrapper">	
	
	
	  <div class="toolbar-line">
		
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('blogadmin/add') }}" class="tips btn btn-sm btn-white"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa fa-plus-circle"></i>&nbsp;{{ Lang::get('core.btn_create') }}</a>
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="bsetecDelete();" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-minus-circle"></i>&nbsp;{{ Lang::get('core.btn_remove') }}</a>
			@endif 	
				  
			@if($access['is_excel'] ==1)
			<a href="{{ URL::to('blogadmin/download') }}" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-download"></i>&nbsp;{{ Lang::get('core.btn_download') }} </a>
			@endif		
		 <!-- 	@if(Session::get('gid') ==1)
			<a href="{{ URL::to('bsetec/module/config/blogadmin') }}" class="tips btn btn-sm btn-white"  title="{{ Lang::get('core.btn_config') }}">
			<i class="icon-cog"></i>&nbsp;{{ Lang::get('core.btn_config') }} </a>	
			@endif  
 -->	  
	</div>  
	@include('blogadmin/tab')
		
	@if(Session::has('message'))	  
		   {!! Session::get('message') !!}
	@endif	
		
	{{ $details }}

	

	 <?php echo Form::open(array('url'=>'blogadmin/destroy/', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )); ?>
	 	<div class="table-responsive">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th> {{ Lang::get('core.no') }} </th>
				<th></th>
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th>{{ Lang::get('core.btn_action') }}</th>
			  </tr>
        </thead>

        <tbody>
			<tr id="bsetec-quick-search" >
				<td> # </td>
				<td>  <input type="checkbox" class="checkall" /></td>
				@foreach ($tableGrid as $t)
				@php ($tr=$t['field'])
					@if($t['view'] =='1')
					<td>						
						{!! SiteHelpers::transForm('' , $tableForm) !!}								
					</td>
					@endif
				@endforeach
				<td style="width:130px;">
				<input type="hidden"  value="Search">
				<button type="button"  class=" do-quick-search btn btn-sx btn-info">{!! Lang::get('core.GO') !!} </button></td>
			  </tr>				
            @foreach ($rowData as $row)
                <tr>
					<td width="50"> {{ ++$i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="id[]" value="{{ $row->blogID }}" />  </td>									
				 @foreach ($tableGrid as $field)
				 @php ($fieldname =$field['field'])
					 @if($field['view'] =='1')
					 <td>					 
					 	@if($field['attribute']['image']['active'] =='1')
							{{ SiteHelpers::showUploadedFile($row->$fieldname,$field['attribute']['image']['path']) }}
						@else	
							@php ( $conn = (isset($field['conn']) ? $field['conn'] : array() ) )
							{{ SiteHelpers::gridDisplay($row->$fieldname,$field['field'],array()) }}	
						@endif						 
					 </td>
					 @endif					 
				 @endforeach
				 <td>
				 	
					@php ( $id = SiteHelpers::encryptID($row->blogID) )
				 	@if($access['is_detail'] ==1)
					<a href="{{ URL::to('blogadmin/show/'.$id)}}"  class="tips btn btn-xs btn-white"  title="{{ Lang::get('core.btn_view') }}"><i class="fa fa-search"></i> </a>
					@endif
					@if($access['is_edit'] ==1)
					<a  href="{{ URL::to('blogadmin/add/'.$id)}}"  class="tips btn btn-xs btn-white"  title="{{ Lang::get('core.btn_edit') }}"> <i class="fa fa-edit"></i></a>
					@endif
										
					
				</td>				 
                </tr>
				
            @endforeach
              
        </tbody>
      
    </table>
	<?php echo Form::close(); ?>
	</div>

	@include('footer')
	
	
</div>
	
	</div>	  
	
<script>
$(document).ready(function(){

	$('.do-quick-search').click(function(){
		$('#bsetecTable').attr('action','{{ URL::to("blogadmin/multisearch")}}');
		$('#bsetecTable').submit();
	});
	
});	
</script>		
	@stop