<head>
    <title>Mobile Payment</title>
</head>
<style>
    input[type=number] {
        -moz-appearance:textfield;
    }
</style>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700,700italic,600italic,400italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700,700italic,400italic' rel='stylesheet' type='text/css'>
<link href="{{ asset('assets/bsetec/themes/theme1/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/themes/theme1/css/theme1.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/js/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/fonts/awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/js/plugins/all/common.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/js/plugins/bootstrap-form/css/bootstrap-formhelpers.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/bsetec/rs-plugin/css/settings.css') }}" media="screen" />
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/css/styles.css') }}" rel="stylesheet">

<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/all/common.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/themes/theme1/js/jquery.mixitup.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('assets/bsetec/rs-plugin/js/jquery.themepunch.plugins.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/common.js') }}"></script>
<style type="text/css">
    hr.style14 {
        border-top: 1px dashed #8c8b8b;
        border-bottom: 1px dashed #fff;
    }
  .table-payment table tr th,
  .btn.btn-color,#submit_button{background: {!! CNF_COLOR_CODE !!} !important;color:#fff !important;
  text-shadow:0 1px 2px #000;}
  .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover,.continue_paypal
  {background: {!! CNF_COLOR_CODE !!} !important;border:1px solid {!! CNF_COLOR_CODE !!}  !important;color:#fff !important;
  text-shadow:0 1px 2px #000;}.continue_paypal{padding: 5px 13px;}
  .iradio_square-green.hover { border: 2px solid {!! CNF_COLOR_CODE !!};border-radius: 50%;background: transparent;}
  .iradio_square-green.checked:before{border:2px solid {!! CNF_COLOR_CODE !!}}
</style>
@if (defined('CNF_CURRENCY'))
@php ($currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
@endif
@php( $userMembership = \bsetecHelpers::getMembershipofuseractive() )
@php( $monthly = \bsetecHelpers::siteMembership() )
<div class="confirm-section">

<link href="{{ asset('assets/bsetec/themes/theme3/css/theme3.css') }}" rel="stylesheet">
<div class="register-b">

@if (defined('CNF_CURRENCY'))
@php( $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
@endif
@php( $userMembership = \bsetecHelpers::getMembershipofuseractive() )
@php( $monthly = \bsetecHelpers::siteMembership() )
<section id="pricing" class="top">
  <div class="container">
    <h3 class="pricing_head"> OUR PRICING </h3>
    <h5>We build awesome course & university sites! Don't miss out <font>join us today!</font></h5>

    <ul class="row nav-tabs nav-tabs-plans hide" id="example-two">
      @if(isset($user_id))
      <li id="bill_free" class=""><a data-toggle="tab" href="#billing_free">Free</span></a></li>
      @endif
      <li id="bill_monthly" class="current_page_item active" id="magic"><a data-toggle="tab" href="#billing_monthly">Monthly</a></li>
      <li id="bill_yearly" class=""><a data-toggle="tab" href="#billing_yearly">Yearly <span>save {{ \bsetecHelpers::getmaxplandiscount() }}</span></a></li>
    </ul>

    <div class="tab-content">
      @if(isset($user_id))
      <div class="fade tab-pane active in" id="billing_payment">
        @include('user/payform_mobile')
      </div>

      @endif
      <div id="billing_monthly" class="tab-pane fade @if(!$tab) active @endif in">

        <div class="row wow zoomIn">
          
          @if(count($monthly)>0)
          @php(  $clsarry = array('plan-basic','plan-premium','plan-elite') )
          @php ($skey = 1 )
          @foreach($monthly as $key => $mvalue)
          @if($skey=='4')
          @php(  $skey=1 )
          @endif
          @if($mvalue->status)
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="pricing_content pricing_con_{{ $skey }}">
              <div class="pricing_heading">{!! $mvalue->plan_name !!}</div>
              <div class="pricing_amounts">
                <p><span>{!! $currency !!} {!! $mvalue->plan_amount !!}</span>Monthly</p>
                <p><span>{!! $currency !!} {!! $mvalue->plan_amount_y !!}</span>yearly</p>
                <div class="course_view_price"><a href="{{ URL::to('membership/courselist/'.$mvalue->plan_id) }}">View Courses</a></div>
              </div>
              <div class="pricing_descr">{!! $mvalue->plan_statement !!} </div>
              @if(isset($userMembership) && $mvalue->plan_id == $userMembership->plan_id)
              <p class="term_text term_btn_text"><span>{!! ucfirst($userMembership->purchase_period) !!}</span> <br>{!! Lang::get('core.renews_at') !!}: {!! date("F d, Y h:i:s A",strtotime('+1 '.str_replace('ly', '', $userMembership->purchase_period),strtotime($userMembership->created_at))) !!}</p>
              @elseif(isset($userMembership) && $mvalue->level <= $userMembership->level)
              <div class="status_text">{!! Lang::get('core.downgrade') !!}</div>
              @elseif(isset($userMembership) && $mvalue->level >= $userMembership->level)
              <div class="status_text">{!! Lang::get('core.upgrade') !!}</div>
              @endif
              @if($mvalue->plan_amount!='0')
              @if(isset($user_id)) @php( $btncls = '1' ) @else @php( $btncls = '2') @endif
              <p class="term_btn_text"><a href="javascript:void(0)" class="btn btn-mixed btnopt" data-plan="{!! $mvalue->plan_name !!}" data-amount="{!! $currency !!}{!! $mvalue->plan_amount !!}" data-amount-y="{!! $currency !!}{!! $mvalue->plan_amount_y !!}" data-value="{!! $mvalue->plan_id !!}">{!! Lang::get('core.Continue') !!}</a></p>
              @endif
            </div>
          </div>
          @endif
          @php(  $skey++ )
          @endforeach
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
<input name="registered" type="hidden" value="0">
<input name="selected_plan" type="hidden" value="0">
<input name="selected_term" type="hidden" value="month">
<input name="selected_m" type="hidden" value="$0">
<input name="selected_y" type="hidden" value="$0">


</div>
<script language="javascript" type="text/javascript">
    $(function(){
    $('body').css('padding', 0);
    $('body').removeClass();
    $('body').addClass('sxim-init');
    $('body').addClass('login-b');
    $('#front-header').addClass('front-header');

    @if($tab)
      var $this = $('a[data-value="{{$tab}}"]');
      var $vl = $this.attr('data-value');
      var $pl = $this.attr('data-plan');
      var $am = $this.attr('data-amount');
      var $amy = $this.attr('data-amount-y');
      $('input[name="selected_plan"]').val($pl);
      $('input[name="selected_m"]').val($am);
      $('input[name="selected_y"]').val($amy);
      $('.sel_plan').text($pl);
      $('.sel_plan_title').text($pl);
      $('.sel_plan_price').text($am);
      $('input[name="membership"]').val($vl);
    @endif

    $(document).on('click','#bill_free',function(){
        $('.sel_plan').text('Free');
        $('input[name="membership"]').val(0);
        $('input[name="membership_type"]').val(0);
    });

    $(document).on('click','#bill_monthly,#bill_yearly',function(){
      $('#billing_free').attr('class','tab-pane fade in');
    });

    $(document).on('click','.btnopt',function(){
      // var ty = $(this).attr('data-type');
      // var term = $(this).attr('data-term');
      var vl = $(this).attr('data-value');
      var pl = $(this).attr('data-plan');
      var am = $(this).attr('data-amount');
      var amy = $(this).attr('data-amount-y');
      var registered = $('input[name="registered"]').val();
      $('input[name="selected_plan"]').val(pl);
      $('input[name="selected_m"]').val(am);
      $('input[name="selected_y"]').val(amy);

      if(registered == 0){
        $('#billing_free').attr('class','tab-pane active fade in');
        $('#billing_payment').attr('class','tab-pane fade in');
      } else {
        $('#billing_free').attr('class','tab-pane fade in');
        $('#billing_payment').attr('class','tab-pane active fade in');
      }
      $('#bill_monthly').attr('class','');
      $('#billing_monthly').attr('class','tab-pane fade in');
      $('.sel_plan').text(pl);
      $('.sel_plan_title').text(pl);
      $('.sel_plan_price').text(am);
      $('input[name="membership"]').val(vl);
      // $('.sel_plan_price_term').text('/'+term);
      // $('.sel_plan_price_termname').text(term.toLowerCase());
      // $('input[name="membership_type"]').val(ty);

      /*if(ty == '1'){ // not logged in

      } else if(ty == '2'){ // logged in

      }*/

    });

    });
</script>
</div>