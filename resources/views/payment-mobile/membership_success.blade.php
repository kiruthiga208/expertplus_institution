<head>
    <title>Payment</title>
</head>
<style>
    input[type=number] {
        -moz-appearance:textfield;
    }
</style>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700,700italic,600italic,400italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700,700italic,400italic' rel='stylesheet' type='text/css'>
<link href="{{ asset('assets/bsetec/themes/theme1/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/themes/theme1/css/theme1.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/js/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/fonts/awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/js/plugins/all/common.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/js/plugins/bootstrap-form/css/bootstrap-formhelpers.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/bsetec/rs-plugin/css/settings.css') }}" media="screen" />
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/css/styles.css') }}" rel="stylesheet">

<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/all/common.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/themes/theme1/js/jquery.mixitup.min.js') }}"></script> 
<script type="text/javascript" src="{{ asset('assets/bsetec/rs-plugin/js/jquery.themepunch.plugins.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/common.js') }}"></script>
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<style>
	.nav-bar.learn-course-header li {
		float: left;
		font-size: 14px;
	}
	.nav-bar.learn-course-header li a{
		color: white;
	}
</style>

<div class="description des_marg">

	<div class="container">
		<div class="row-fluid">
			<div class="des_border span12">

				<div class="des_header">
					@if($status == "success")
					<span class="des_bold des_color">{!! Lang::get('core.membership_payment_success')!!}</span>
					@else
					<span class="des_bold des_color">{!! Lang::get('core.membership_payment_fail')!!}</span>
					@endif

				</div>

				<div class="des_con">
					@if($status == "success")
					<div class="success_img"></div>
					@else
					<div class="failure_img"></div>
					@endif

					@if($transId !=0)
					<span class="readytosuccess id">{!! Lang::get('core.t_id')!!} : {{ $transId }}</span>
					@endif


					<!--<span class="readytosuccess  social_all"><span class="like_color">Like us</span> on Facebook and get Exculsive, Fan-only Offers.</span>-->
					<div class="des_ready">
						<span class="desready_p readytosuccess">{!! Lang::get('core.pay_ready')!!}</span>
						<div>
							@if($status == "success")
							<div class="access access_mrg">
								@if(isset($planId))
								<a href="{{url('membership/courselist/'.$planId)}}">{!! Lang::get('core.go_course')!!}</a></div></div>
								@else
								<a href="{{url('course')}}">{!! Lang::get('core.go_course')!!}</a></div></div>
								@endif
							</div>
							@else
							<div class="access access_mrg">
								<!-- <a href="{{url('user/membership')}}">{!! Lang::get('core.back_membership')!!}</a> -->
								</div></div>
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>

		<script language="javascript" type="text/javascript">
    $(function(){
    $('body').css('padding', 0);
    $('body').removeClass();
    $('body').addClass('sxim-init');
    $('body').addClass('login-b');
    $('#front-header').addClass('front-header');

		});
		</script>
