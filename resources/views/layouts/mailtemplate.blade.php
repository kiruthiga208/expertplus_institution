<!-- header start-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>expert</title>
</head>

<body>
<table border="0" cellpadding="0" cellspacing="0" width="600" align="center">
<tr>
<td>
<table cellpadding="0" cellspacing="0" width="100%" border="0">

<tr><!--start-->
<td style="background:#121517;">
<table cellpadding="0" cellspacing="0" width="100%" border="0">
<tr height="25"></tr>
<tr><td width="10"></td><td width="200"><h1 style="margin:0px;">
<a href="{{ URL::to('') }}">
 @if(file_exists('./uploads/images/'.CNF_FLOGO) && CNF_FLOGO !='')
 	<img src="{{ asset('uploads/images/'.CNF_FLOGO)}}" alt="{{ CNF_APPNAME }}"  />
 @else
	<img src="{{ asset('assets/bsetec/images/mailtemplate/logo.png') }}">
 @endif  
</a>
</h1></td><td width="390"></td></tr>
<tr height="25"></tr>
</table>
</td>
</tr><!--end-->

<tr><td><table cellpadding="0" cellspacing="0" width="100%" border="0">
<tr><td width="10"></td>
<td width="580" height="400" valign="top">
<table>
<tr height="20"></tr>
<tr>
<td style="font-size:14px; line-height:25px;">
<!-- header end-->
	@yield('content')
<!-- footer start-->
</td>
</tr>

<tr height="40"></tr>
</table>
</td>
<td width="10"></td></tr></table></td></tr>

<tr><!--start-->
<td>
<table cellpadding="0" cellspacing="0" width="100%" border="0" style="background:#000000;">
<tr height="25"></tr>
<tr><!--start-->
<td width="10"></td>
<td width="220">
<table cellpadding="0" cellspacing="0" width="100%" border="0">
<tr>
<td>
 @if(file_exists('./uploads/images/'.CNF_FLOGO) && CNF_FLOGO !='')
 	<img src="{{ asset('uploads/images/'.CNF_FLOGO)}}" alt="{{ CNF_APPNAME }}"  />
 @else
	<img src="{{ asset('assets/bsetec/images/mailtemplate/logo.png') }}">
 @endif  
</td>
</tr>
<tr height="12"></tr>
<tr><td style="color:#b1b1b1; font-size:13px; text-transform:capitalize;">Copyright © {!! date('Y') !!}  {!! CNF_APPNAME !!}</td></tr>
</table></td>
<td width="20"></td>
<td width="340">

<table cellpadding="0" cellspacing="0" width="100%" border="0">
<tr>
<td width="220"></td>
<td width="30" style="text-align:center;"><a href="javascript::"><img src="{{ asset('assets/bsetec/images/mailtemplate/facebook.png') }}"></a></td>
<td width="30" style="text-align:center;"><a href="javascript::"><img src="{{ asset('assets/bsetec/images/mailtemplate/twitter.png') }}"></a></td>
<td width="30" style="text-align:center;"><a href="javascript::"><img src="{{ asset('assets/bsetec/images/mailtemplate/youtube.png') }}"></a></td>
<td width="30" style="text-align:center;"><a href="javascript::"><img src="{{ asset('assets/bsetec/images/mailtemplate/rss.png') }}"></a></td>
</tr>


</table>

</td>
<td width="10"></td>
</tr><!--end-->
<tr height="25"></tr>
</table>

</td>
</tr><!--end-->
</table>
</td>
</tr>
</table>
</body>
</html>
<!-- footer end-->