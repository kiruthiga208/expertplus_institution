<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> {{ CNF_APPNAME }} </title>
<meta name="keywords" content="">
<meta name="description" content=""/>
<!-- ajax _token passing -->
<meta name="_token" content="{!! csrf_token() !!}"/>
<meta name="_site_name" content="{!! CNF_APPNAME !!}"/>


<link rel="shortcut icon" href="{{ asset('uploads/images/'.CNF_FAV)}}" type="image/x-icon">	



        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700,700italic,600italic,400italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700,700italic,400italic' rel='stylesheet' type='text/css'>
		
	<link href="{{ asset('assets/bsetec/themes/theme1/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/bsetec/js/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{ asset('assets/bsetec/fonts/awesome/css/font-awesome.min.css')}}" rel="stylesheet">
	
	<link href="{{ asset('assets/bsetec/js/plugins/all/common.css')}}" rel="stylesheet">
	<link href="{{ asset('assets/bsetec/themes/theme1/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7') }}" rel="stylesheet">
	<link href="{{ asset('assets/bsetec/themes/theme1/css/theme1.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/bsetec/js/plugins/bootstrap-form/css/bootstrap-formhelpers.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/bsetec/rs-plugin/css/settings.css')}}" media="screen" />
        
        <link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
       <!-- <link href="{{ asset('assets/bsetec/css/bsetec.css')}}" rel="stylesheet">	-->
	<link href="{{ asset('assets/bsetec/css/styles.css') }}" rel="stylesheet">


	<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/all/common.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/bsetec/themes/theme1/js/jquery.mixitup.min.js') }}"></script>
        <script type="text/javascript" src="{{asset('assets/bsetec/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
<noscript><meta http-equiv="refresh" content="0;url={{ URL::TO('nojs.php') }}"></noscript>
        <script src="{{ asset('assets/bsetec/static/js/jquery.raty.min.js') }}"></script>        
    	<script src="{{ asset('assets/bsetec/js/common.js') }}"></script>

    	   


	<!-- <script src="{{ asset('assets/bsetec/js/owl.carousel.js') }}"></script>-->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->		

  	</head>
  	<body class="sxim-init" >

<div id="front-header" class="front-header">
	@include('layouts/header')
</div>
<div id="expert_plus_main">	
<div id="wrapper">
	<div class="gray-bg " id="page-wrapper-full">
		@yield('profile-page')
		<div class="container">
			<div class="row">
			@yield('content')		
			</div>
		</div>
	</div>

</div>
</div>
	
@include('layouts/footer')	

<div class="modal fade" id="bsetec-modal" tabindex="-1" role="dialog">
<div class="modal-dialog">
  <div class="modal-content">
	<div class="modal-header bg-default">
		
		<button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Modal title</h4>
	</div>
	<div class="modal-body" id="bsetec-modal-content">

	</div>

  </div>
</div>
</div>
{{ Sitehelpers::showNotification() }} 
<script type="text/javascript" >
$(".lazy").lazyload({effect:"fadeIn"});
</script>
</body> 
</html>