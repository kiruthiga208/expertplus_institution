@extends('layouts.frontend') 
@section('content')
@php usort($tableGrid, "SiteHelpers::_sort") @endphp
<link href="{{ asset('resources/views/message/resources/style.css') }}" rel="stylesheet" />

<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/tinymce/jscripts/tiny_mce/tiny_mce.js') }}"></script>

<div class="message-c">
<div class="page-title"><h3>{!! Lang::get('core.message')!!}</h3></div>
</div>
<div class="message-block page-content row">
	<div class="col-md-3 m-t">
		<div class="animated fadeInRight sidebar-msg">
		<div class="sbox-title" style="height:65px"><a href="javascript:void(0);" id="composeMessage" class="btn btn-block btn-lg btn-primary">{!! Lang::get('core.compose')!!}</a></div>
			<div class="">
				<section class="panel panel-default mail-categories">
					<ul class="list-group">
						<li class="list-group-item @if($type == 0) active @endif"><a href="{{ url('message?ty=0') }}">
							<i class="fa fa-inbox text-info"></i> &nbsp; {!! Lang::get('core.Inbox')!!}
							<span class="badge badge-info pull-right" data-inbox="{!! $totalType['0'] !!}" id="cinbox">{!! $totalType['0'] !!}</span>
						</a></li>
						<li class="list-group-item @if($type == 1) active @endif"><a href="{{ url('message?ty=1') }}">
							<i class="fa fa-star text-warning"></i> &nbsp;  {!! Lang::get('core.Starred')!!}
							<span class="badge badge-warning pull-right" data-star="{!! $totalType['1'] !!}" id="cstar">{!! $totalType['1'] !!}</span>
						</a></li>
						<li class="list-group-item @if($type == 2) active @endif"><a href="{{ url('message?ty=2') }}">
							<i class="fa fa-envelope-o text-primary"></i> &nbsp; {!! Lang::get('core.Sent')!!}
							<span class="badge badge-primary pull-right" data-sent="{!! $totalType['2'] !!}" id="csent">{!! $totalType['2'] !!}</span>
						</a></li>
						<li class="list-group-item @if($type == 3) active @endif"><a href="{{ url('message?ty=3') }}">
							<i class="fa fa-pencil text-success"></i> &nbsp;  {!! Lang::get('core.Draft')!!}
							<span class="badge badge-success pull-right" data-draft="{!! $totalType['3'] !!}" id="cdraft">{!! $totalType['3'] !!}</span>
						</a></li>
						<li class="list-group-item @if($type == 4) active @endif"><a href="{{ url('message?ty=4') }}">
							<i class="fa fa-trash-o text-danger"></i> &nbsp; {!! Lang::get('core.Trash')!!}
							<span class="badge badge-danger pull-right" data-trash="{!! $totalType['4'] !!}" id="ctrash">{!! $totalType['4'] !!}</span>
						</a></li>
					</ul>
				</section>
			
				<section class="panel panel-default mail-categories add_label">
					<div class="panel-heading clearfix"><a href="javascript:void(0);" class="pull-left" id="AddLabel"><i class="fa fa-plus-circle"></i>{!! Lang::get('core.label')!!}</a></div>
					<ul class="list-group" id="label-list-group">
						@php $labelname = ''; @endphp
 						@foreach($labels as $label_id => $label_name)
						@php if($labelname == '' && $label == $label_id) $labelname = $label_name; @endphp
						<li class="list-group-item @if($type == 5 && $label == $label_id) active @endif"><a href="{{ url('message?ty=5&lb='.$label_id) }}" data-id="{!! $label_id !!}">{!! $label_name !!}</a></li>
						@endforeach
					</ul>
				</section>
			</div>
		</div>
	</div>
	
	<div class="col-md-9 m-t">
		<div class="sbox animated fadeInRight dataMessageDiv">
			<div class="sbox-title">  @if($type == 0) <h5 class="text-info"><i class="fa fa-inbox"></i> {!! Lang::get('core.Inbox') !!}</h5> @elseif($type == 1) <h5 class="text-warning"><i class="fa fa-star"></i> {!! Lang::get('core.Starred') !!} </h5> @elseif($type == 2) <h5 class="text-primary"><i class="fa fa-envelope-o"></i> {!! Lang::get('core.Sent') !!} </h5> @elseif($type == 3) <h5 class="text-success"><i class="fa fa-pencil"></i> {!! Lang::get('core.Draft') !!} </h5> @elseif($type == 4) <h5 class="text-danger"><i class="fa fa-trash-o"></i> {!! Lang::get('core.Trash') !!} </h5> @elseif($type == 5) <h5 class="labelTitleDisp"><i class="fa fa-tag"></i> {!! $labelname !!}</h5> @endif </div>
			<div class="table-responsive" style="min-height:210px;">
				<section class="panel panel-default mail-container">
					<div class="mail-options">
						<label class="ui-checkbox"><input name="checkbox1" type="checkbox" class="checkall" value="option1"><span> {!! Lang::get('core.toggle') !!}</span></label>
						@if($type != 4 && $type != 5)						
						<span class="actbuttons pull-right tips" title="Remove Label">
							<button class="btn btn-primary" type="button" id="removeLabels">
								<i class="fa fa-ban"></i>
							</button>
						</span>	
						<span class="actbuttons dropdown pull-right tips" title="Apply Label">
						  <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-tag"></i>
							<span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" aria-labelledby="dropdownMenu2" id="label-dropdown-menu">
							@foreach($labels as $label_id => $label_name)
								<li><a href="javascript:void(0);" data-id="{!! $label_id !!}">{!! $label_name !!}</a></li>
							@endforeach
						  </ul>
						</span>
						<span class="actbuttons pull-right tips" title="Delete">
							<button class="btn btn-primary" type="button" id="deleteMessage" data-type="{{$type}}">
								<i class="fa fa-trash-o"></i>
							</button>
						</span>						
						<!--span class="actbuttons pull-right tips" title="Mark as Read">
							<button class="btn btn-primary" type="button" id="deleteMessage">
								<i class="fa fa-check"></i>
							</button>
						</span-->
						@endif
						@if($type == 5)
						<span class="actbuttons pull-right tips" title="Delete Label">
							<button class="btn btn-primary" type="button" id="deleteLabel">
								<i class="fa fa-trash-o"></i>
							</button>
						</span>	
						<span class="actbuttons pull-right tips" title="Edit Label">
							<button class="btn btn-primary" type="button" id="editLabel">
								<i class="fa fa-edit"></i>
							</button>
						</span>	
						@endif
						@if($type == 4)
						<span class="actbuttons pull-right tips" title="Delete from Trash">
							<button class="btn btn-primary" type="button" id="deleteFromTrash">
								<i class="fa fa-trash-o"></i>
							</button>
						</span>	
						@endif
					</div>
					<table class="table table-hover">
						<tbody class="dataMessage">
						 @foreach($rowData as $row)
						 <input type="hidden" name="entryby" class="entryby" value="{{ $row->entry_by }}" />
							@php if($row->entry_by == $uid) $isSender = 1; else $isSender = 0; @endphp
							@if($row->read == 0 && ($type == 0 || $type == 1)) <tr class="mail-unread" id="msg_{!! $row->id !!}"> @else <tr class="mail" id="msg_{!! $row->id !!}"> @endif
								<td data-type="{!! $type !!}" data-id="{!! $row->id !!}" data-sender="{!! $isSender !!}" width="10" style="padding-bottom:10px; padding-right:0px;"><label class="ui-checkbox"><input name="checkbox1" type="checkbox" data-sender="{!! $isSender !!}" value="{!! $row->id !!}" class="checkmsg"><span></span></label></td>
								@php if($isSender == 1 && isset($usersNameList[$row->recipient])) { $rowUser = $usersNameList[$row->recipient]; } else if($isSender == 0 && isset($usersNameList[$row->entry_by])) { $rowUser = $usersNameList[$row->entry_by]; } else { $rowUser = ''; } @endphp
								<td class="td-clickable text-success">{!! $rowUser !!}</td>
								@php if($isSender == 0 && isset($labels[$row->label_by_recipient])) { $rowLabel = $labels[$row->label_by_recipient]; $rowLabelId = $row->label_by_recipient; } else if($isSender == 1 && isset($labels[$row->label_by_sender])) { $rowLabel = $labels[$row->label_by_sender]; $rowLabelId = $row->label_by_sender; } else { $rowLabel = ''; $rowLabelId = ''; } @endphp
								<td class="td-clickable text-"><label class="badge badge-inverse labelDisp" data-id="{!! $rowLabelId !!}" id="label_{!! $row->id !!}">{!! $rowLabel !!}</label> {!! substr($row->subject, 0, 30) !!}</td>
								<td class="td-clickable text-light">{!! substr(strip_tags($row->message), 0, 60) !!}</td>
								<td class="td-clickable text-light">{!! date("m/d/y h:i A", strtotime($row->createdOn)) !!}</td>
								<td>
								{!! Form::open(array('url'=>'messageupdate', 'class'=>'form-horizontal ajaxMessageUpdate','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
									@php if($type == 0) $rowStarred = $row->starred_by_recipient; else if($type == 2) $rowStarred = $row->starred_by_sender; else $rowStarred = 0; @endphp
									{!! Form::hidden('starred', $rowStarred )  !!}
									{!! Form::hidden('update_function', '' )  !!}
									{!! Form::hidden('msg_type', $type )  !!}
									{!! Form::hidden('msg_id', $row->id )  !!}
									@if($type == 0 || $type == 2)
									<i class="fa fa-star msgstar @if($rowStarred == 1) active @endif"></i>
									@endif
								{!! Form::close() !!}
								</td>
							</tr>
						 @endforeach
						</tbody>
					</table>
					{!! Form::open(array('url'=>'messagegroupupdate', 'class'=>'form-horizontal ajaxGroupMessageUpdate','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
						{!! Form::hidden('msg_ids', '' )  !!}
						{!! Form::hidden('is_sender', '' )  !!}
						{!! Form::hidden('label_id', '' )  !!}
						{!! Form::hidden('label_name', '' )  !!}
						{!! Form::hidden('group_update_function', '' )  !!}
						{!! Form::hidden('type', $type )!!}
					{!! Form::close() !!}
				</section>
			</div>
			<div style="">	
				<div class="table-footer">
					<div class="row">
					 <div class="col-sm-5">
					  <div class="table-actions" style="">
					 
					   {!! Form::open(array('url'=>$pageModule.'s/mfilter/','class'=>'messagePage')) !!}
							{!! Form::hidden('ty', $type )  !!}
							{!! Form::hidden('lb', $label )  !!}
						   @php $pages = array(5,10,20,30,50) @endphp
						   @php $orders = array('asc','desc') @endphp
						<select name="rows" data-placeholder="{{ Lang::get('core.grid_show') }}" class="select-alt"  >
						  <option value=""> {{ Lang::get('core.grid_page') }} </option>
						  @foreach($pages as $p)
						  <option value="{{ $p }}" 
							@if(isset($pager['rows']) && $pager['rows'] == $p) 
								selected="selected"
							@endif	
						  >{{ $p }}</option>
						  @endforeach
						</select>
						<select name="sort" data-placeholder="{{ Lang::get('core.grid_sort') }}" class="select-alt"  >
						  <option value=""> {{ Lang::get('core.grid_sort') }} </option>	 
						  @foreach($tableGrid as $field)
						   @if($field['view'] =='1' && $field['sortable'] =='1') 
							  <option value="{{ $field['field'] }}" 
								@if(isset($pager['sort']) && $pager['sort'] == $field['field']) 
									selected="selected"
								@endif	
							  >{{ $field['label'] }}</option>
							@endif	  
						  @endforeach
						 
						</select>	
						<select name="order" data-placeholder="{{ Lang::get('core.grid_order') }}" class="select-alt">
						  <option value=""> {{ Lang::get('core.grid_order') }}</option>
						   @foreach($orders as $o)
						  <option value="{{ $o }}"
							@if(isset($pager['order']) && $pager['order'] == $o)
								selected="selected"
							@endif	
						  >{{ ucwords($o) }}</option>
						 @endforeach
						</select>	
						<button type="submit" class="btn btn-primary btn-sm">{!! Lang::get('core.GO')!!}</button>	
						<input type="hidden" name="md" value="{{ (isset($masterdetail['filtermd']) ? $masterdetail['filtermd'] : '') }}" />
					  {!! Form::close() !!}
					  </div>					
					  </div>
					   <div class="col-sm-3">
						<p class="total-c text-center" style="" id="page_total" data-total="{{ $pagination->total() }}">
						{!! Lang::get('core.Total')!!} : <b class="page_total">{{ $pagination->total() }}</b>
						</p>		
					   </div>
						<div class="col-sm-4">	
					  @if($type==5)
					  	{!! $pagination->appends(['ty'=>$type,'lb'=>$label])->render()  !!}
					  @else
					  	{!! $pagination->appends(['ty'=>$type])->render() !!}
					  @endif
					 	 
					  </div>
					  </div>
					</div>	
			</div>
		</div>
		@foreach($rowData as $row)
		@php if($row->entry_by == $uid) $isSender = 1; else $isSender = 0; @endphp
		<div class="sbox animated fadeInRight hidden viewMessageDiv" id="viewMessageDiv_{!! $row->id !!}">
			<div class="sbox-title height60"> 
				<span class="actbuttons pull-left tips" title="Back">
					<button class="btn btn-primary gotodataMessageDiv" type="button">
						<i class="fa fa-arrow-left"></i>
					</button>
				</span>
				@if($type != 4)
				<span class="actbuttons pull-right tips" title="Delete">
					<button class="btn btn-primary singleMessageDelete" type="button" data-id="{!! $row->id !!}" data-sender="{!! $isSender !!}">
						<i class="fa fa-trash-o"></i>
					</button>
				</span>
				@endif
				<input type="hidden" class="senderid" value="{!! $row->entry_by !!}">
				<input type="hidden" class="recipientid" value="{!! $row->recipient !!}">
				<input type="hidden" class="msg_id" value="{!! $row->id !!}">
				<h2 class="viewMessageTitle pull-left">{!! $row->subject !!}</h2>
				@php if($isSender == 0 && isset($labels[$row->label_by_recipient])) { $rowLabel = $labels[$row->label_by_recipient]; $rowLabelId = $row->label_by_recipient; } else if($isSender == 1 && isset($labels[$row->label_by_sender])) { $rowLabel = $labels[$row->label_by_sender]; $rowLabelId = $row->label_by_sender; } else { $rowLabel = ''; $rowLabelId = ''; }@endphp
				<label class="badge badge-inverse labelDisp labelDisp2" data-id="{!! $rowLabelId !!}" id="label_{!! $row->id !!}">{!! $rowLabel !!}</label>
			</div>
			<div class="sbox-content" style="min-height:100px;">
				@php if($isSender == 1 && isset($usersNameList[$row->recipient])) { $rowUser = $usersNameList[$row->recipient]; } else if($isSender == 0 && isset($usersNameList[$row->entry_by])) { $rowUser = $usersNameList[$row->entry_by]; } else { $rowUser = ''; } @endphp
				@php if($isSender == 1 && isset($usersEmailList[$row->recipient])) { $rowUserEmail = $usersEmailList[$row->recipient]; } else if($isSender == 0 && isset($usersEmailList[$row->entry_by])) { $rowUserEmail = $usersEmailList[$row->entry_by]; } else { $rowUserEmail = ''; } @endphp
				@php if($isSender == 1 && isset($usersAvatar[$row->recipient])) { $rowUserAvatar = $usersAvatar[$row->recipient]; } else if($isSender == 0 && isset($usersAvatar[$row->entry_by])) { $rowUserAvatar = $usersAvatar[$row->entry_by]; } else { $rowUserAvatar = ''; } @endphp
				<h5 class="viewMessageDate clearfix"><span class="msg-text pull-left">
				{!! SiteHelpers::customavatar('',$row->entry_by,'small','img-circle') !!}
				{!! $rowUser." (".$rowUserEmail.")" !!}</span><span class="pull-right">{!! date("m/d/y h:i A", strtotime($row->createdOn)) !!}</span></h5><hr />
				<div class="viewMessageContent">{!! $row->message !!}</div>
				
				@if($type != 4 && $type != 3)
				<div class="replyDiv">
					<span class="actbuttons pull-left tips" title="Reply">
						<button class="btn btn-primary sendReplyForward" type="button" data-btn="reply" data-id="{!! $row->id !!}" data-type="{!! $type !!}" data-sender="{!! $isSender !!}">
							<i class="fa fa-mail-reply"></i> {!! Lang::get('core.Reply') !!}
						</button>
					</span>
					<span class="actbuttons pull-right tips" title="Forward">
						<button class="btn btn-primary sendReplyForward" type="button" data-btn="forward" data-id="{!! $row->id !!}" data-type="{!! $type !!}" data-sender="{!! $isSender !!}">
							<i class="fa fa-mail-forward"></i> {!! Lang::get('core.Forward') !!}
						</button>
					</span>
				</div>
				@endif
				
			</div>
		</div>
		@endforeach
	</div>	  
</div>	

<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" style="max-width:800px;width:90%;">
	  <div class="modal-content">
		<div class="modal-header bg-default">
			
			<button type="button " class="close" id="clearcal" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-titles">{!! Lang::get('core.compose') !!} </h4>
		</div>
		<div class="load_notify"></div>
		<div class="modal-body" id="compose-modal-content">
			 {!! Form::open(array('url'=>'messagesend', 'class'=>'form-horizontal sendMessage','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
				<input type="hidden" name="typee" class="type" />
				<div class="form-group">
					<label for="mail_to" class="col-xs-2">{!! Lang::get('core.fr_emailsendto') !!}</label>
					<div class="col-xs-10">
						<div>{!! Form::select('recipient', $users, null, array('class'=>'form-control select2 width100p recipientselect2', 'style'=>'padding:0px;', 'required'=>'' ) ) !!}</div>
					</div>
				</div>
				<div class="form-group">
					<label for="mail_subject" class="col-xs-2">{!! Lang::get('core.fr_emailsubject')!!}</label>
					<div class="col-xs-10">
						<div>{!! Form::text('subject', null, array('class'=>'form-control', 'placeholder'=>Lang::get('core.fr_emailsubject'), 'required'=>'', 'pattern'=>'[a-zA-Z0-9]+')) !!}</div>
					</div>
				</div>
				<div class="form-group">
					<label for="mail_subject" class="col-xs-2">{!! Lang::get('core.message')!!}</label>
					<div class="col-xs-10">
						<div><textarea name='message' rows='3' style="width:100%;" class='form-control mceEditor' placeholder="{{ Lang::get('core.message')}}"></textarea></div>
						<span class="error" style="color:red"></span>
					</div>
				</div>
				<div class="sending_loadergs"></div>
				<div class="form-group">
					<label class="col-xs-2"></label>
					<div class="col-xs-10">
						{!! Form::hidden('draft', '0' )  !!}
						{!! Form::hidden('messageid', '' )  !!}
						<button type="submit" name="submit" data-type="inboxx" class="btn btn-primary btn-sm sendbtn" ><i class="fa  fa-envelope "></i> {!! Lang::get('core.btn_message') !!}</button>
						<button type="submit" name="submitdraft" data-type="draftt" class="btn btn-primary btn-sm savedraft" ><i class="fa  fa-save "></i> {!! Lang::get('core.btn_draft') !!}</button>
					</div>
				</div>
			 {!! Form::close() !!}
		</div>

	  </div>
	</div>
</div>

<div class="modal fade" id="label-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog" style="max-width:800px;width:90%;">
	  <div class="modal-content">
		<div class="modal-header bg-default">
			
			<button type="button " class="close" id="clearcal" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-titles">{!! Lang::get('core.edit_label')!!} </h4>
		</div>
		<div class="load_notify"></div>
		<div class="modal-body" id="label-modal-content">
			 {!! Form::open(array('url'=>'messagelabelsave', 'class'=>'form-horizontal saveLabel','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
				<div class="form-group">
					<label for="label" class="col-xs-2">{!! Lang::get('core.label_name') !!}</label>
					<div class="col-xs-10">
						<div>{!! Form::text('label', null, array('class'=>'form-control', 'placeholder'=>Lang::get('core.label_name'), 'required'=>'')) !!}</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-2"></label>
					<div class="col-xs-10">
						{!! Form::hidden('id', '' )  !!}
						<button type="submit" name="submit" class="btn btn-primary btn-sm savelabelbtn" ><i class="fa  fa-save "></i> {!! Lang::get('core.sb_save') !!}</button>
					</div>
				</div>
			 {!! Form::close() !!}
		</div>

	  </div>
	</div>
</div>
{!! Form::open(array('url'=>'messagelabeldelete', 'class'=>'form-horizontal ajaxLabelDelete','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
	{!! Form::hidden('label_id', '' )  !!}
{!! Form::close() !!}
<input type="hidden" name="urlmodule" id="urlmodule" value="{{ url('message') }}" />
<script>
$(document).ready(function(){

	@if(isset($composeUser) && $composeUser != '0')
	
		$('.sendMessage')[0].reset();
		$('#compose-modal').find('[name="recipient"]').select2('val', {!! $composeUser !!});
		$('#compose-modal').find('[name="messageid"]').val('');
		$('.sendbtn').removeAttr('disabled');
		$('.savedraft').removeAttr('disabled');
		$('#compose-modal').modal({
			backdrop: 'static',
			keyboard: false
		});
	
	@endif

	$('[name="recipient"]').select2();
	
	$(function(){
		tinymce.init({	
			mode : "specific_textareas",
			editor_selector : "mceEditor",
			theme : "advanced",
			theme_advanced_buttons1 : "undo,redo,|,bold,italic,underline,strikethrough,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,justifyfull,outdent,indent,blockquote,bullist,numlist,|,formatselect,fontselect,|,removeformat",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
		 });	
	});
	
	$('.sendbtn').click(function(){
		$('.type').val(0);
	});
	$('.savedraft').click(function(){
		$('.type').val(3);
	});
	function tinyClean(value) {
	value = value.replace(/&nbsp;/ig, ' ');
	value = value.replace(/\s\s+/g, ' ');
	if(value == '<p><br></p>' || value == '<p> </p>' || value == '<p></p>') {
		value = '';
	}
	return value;
}
	$('.sendMessage').submit(function(){

		var value = $.trim(tinyClean(tinyMCE.get('message').getContent()));
		var typee = $('[name="typee"]').val();
		
		if(value==''){
			$('.error').html("{!! Lang::get('core.msg_required')!!}");
		}else{
			$('.error').html('');
		}
		
		if($('[name="recipient"]').val() !='' && $('[name="subject"]').val() !='' && value !=''){
			$('.sendbtn').attr('disabled','disabled');
			$('.savedraft').attr('disabled','disabled');
			var recipient = $('[name="recipient"]').val();
			var url = $(this).attr('action');
			tinyMCE.triggerSave();
			$.ajax({
				type: 'POST',
				url: url,
				data: $(".sendMessage").serialize(),
				beforeSend:function(){
					$('.sendbtn').hide();
					$('.savedraft').hide();
					$('.sending_loadergs').append('<h2><i class="fa fa-refresh fa-spin"></i> Loading...</h2>');					
				},
				success : function(response) {
					var data = jQuery.parseJSON( response );
					if(data.status == '1') {
						toastr.success("{{ Lang::get('core.success_msg') }}");
					} else {
						toastr.error("{{ Lang::get('core.error_msg') }}");
					}
					$('.sendbtn').removeAttr('disabled');
					$('.savedraft').removeAttr('disabled');
					$('#compose-modal').modal("toggle");					
					$('.sendbtn').show();
					$('.savedraft').show();
					if(typee==0){
						var sent =$('#csent').attr('data-sent');
						var sentt =parseInt(sent)+parseInt(1);
						$('#csent').html(sentt);
					}else if(typee==3){
						var draft =$('#cdraft').attr('data-draft');
						var draftt =parseInt(draft)+parseInt(1);
						$('#cdraft').html(draftt);
					}
				},complete: function() {
				$('.sending_loadergs').html('');
			}
			});
		}
		return false;
	});
	$('.ajaxMessageUpdate').submit(function(){
		var starred = $(this).children('[name="starred"]').val();
		if(starred == '1'){
			$(this).children('[name="starred"]').val('0');
		} else {
			$(this).children('[name="starred"]').val('1');
		}
		var url = $(this).attr('action');
			$(this).children('.msgstar').toggleClass('active');
		$.ajax({
			type: 'POST',
			url: url,
			data: $(this).serialize(),
			success : function(response) {
				var star =$('#cstar').attr('data-star');
				var starr;
				if(response==1){
					starr =parseInt(star)+parseInt(response);					
				}else if(response==0){
					 starr =parseInt(star);
				}
				 $('#cstar').html(starr);
				
			}
		});
		return false;
	});
	$('.msgstar').click(function(){
		$(this).parent('form').children('[name="update_function"]').val('starred');
		$(this).parent('form').submit();
	});
	
	$('.sendbtn').click(function(){
		$('[name="draft"]').val('0');
	});
	$('.savedraft').click(function(){
		$('[name="draft"]').val('1');
	});
	
	$('#composeMessage').click(function(){
		$('.sendMessage')[0].reset();
		$('#compose-modal').find('[name="recipient"]').select2('val','');
		$('#compose-modal').find('[name="messageid"]').val('');
		$('.sendbtn').removeAttr('disabled');
		$('.savedraft').removeAttr('disabled');
		$('#compose-modal').modal({
			backdrop: 'static',
			keyboard: false
		});
	});
	$('.gotodataMessageDiv').click(function(){
		$('.viewMessageDiv').addClass('hidden');
		$('.dataMessageDiv').slideDown();
	});
	$('.sendReplyForward').click(function(){
		var msgid = $(this).attr('data-id');
		var msgtype = $(this).attr('data-type');
		var issender = $(this).attr('data-sender');
		var option = $(this).attr('data-btn');
		
		$('.sendMessage')[0].reset();
		$('#compose-modal').find('[name="recipient"]').select2('val','');
		$('#compose-modal').find('[name="messageid"]').val('');
		
		if(option == 'reply'){
			if(msgtype == 0) {
				$('#compose-modal').find('[name="recipient"]').select2('val',$('#viewMessageDiv_'+msgid).find('.senderid').val());
			} else if(msgtype == 2) {
				$('#compose-modal').find('[name="recipient"]').select2('val',$('#viewMessageDiv_'+msgid).find('.recipientid').val());
			} else if(msgtype == 1 || msgtype == 5) {
				if(issender == 1){
					$('#compose-modal').find('[name="recipient"]').select2('val',$('#viewMessageDiv_'+msgid).find('.recipientid').val());
				} else {
					$('#compose-modal').find('[name="recipient"]').select2('val',$('#viewMessageDiv_'+msgid).find('.senderid').val());
				}
			}
		} else if(option == 'forward'){
			$('#compose-modal').find('[name="subject"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitle').text());
			tinyMCE.activeEditor.setContent($('#viewMessageDiv_'+msgid).find('.viewMessageContent').html());
		}
		
		$('.sendbtn').removeAttr('disabled');
		$('.savedraft').removeAttr('disabled');
		$('#compose-modal').modal({
			backdrop: 'static',
			keyboard: false
		});
	});
	
	$('.dataMessage tr .td-clickable').click(function(){
		var msgid = $(this).parent('tr').find('td:nth-child(1)').attr('data-id');
		var msgtype = $(this).parent('tr').find('td:nth-child(1)').attr('data-type');
		if(msgtype == '3'){
			$('.sendMessage')[0].reset();
			$('#compose-modal').find('[name="recipient"]').select2('val','');
			$('#compose-modal').find('[name="messageid"]').val('');
			
			$('#compose-modal').find('[name="subject"]').val($('#viewMessageDiv_'+msgid).find('.viewMessageTitle').text());
			var recipientid = $('#viewMessageDiv_'+msgid).find('.recipientid').val();
			var msg_id = $('#viewMessageDiv_'+msgid).find('.msg_id').val();
			$('#compose-modal').find('[name="recipient"]').select2('val',recipientid);
			$('#compose-modal').find('[name="messageid"]').val(msg_id);
			tinyMCE.activeEditor.setContent($('#viewMessageDiv_'+msgid).find('.viewMessageContent').html());
			
			$('.sendbtn').removeAttr('disabled');
			$('.savedraft').removeAttr('disabled');
			$('#compose-modal').modal({
				backdrop: 'static',
				keyboard: false
			});
		} else {
			if($(this).parent('tr').hasClass('mail-unread')){
				$(this).parent('tr').find('form').children('[name="update_function"]').val('read');
				$(this).parent('tr').find('form').submit();
				$(this).parent('tr').attr('class','mail');
			}
			$('.dataMessageDiv').hide();
			$('#viewMessageDiv_'+msgid).removeClass('hidden');
		}
	});
	$('.ajaxGroupMessageUpdate').submit(function(){
		var url = $(this).attr('action');
		var ids = $(this).children('[name="msg_ids"]').val();
		var message_ids = ids.split(',');
		var typee=$('[name="type"]').val();
		if($(this).children('[name="group_update_function"]').val() == 'delete' || $(this).children('[name="group_update_function"]').val() == 'deleteTrash'){
			jQuery.each(message_ids, function(index, item) {
				$('#msg_'+item).remove();
			});
			$('.gotodataMessageDiv').trigger('click');
		} else if($(this).children('[name="group_update_function"]').val() == 'label'){
			var label_name = $(this).children('[name="label_name"]').val();
			jQuery.each(message_ids, function(index, item) {
				$('#label_'+item).text(label_name);
			});
		} else if($(this).children('[name="group_update_function"]').val() == 'removelabel'){
			jQuery.each(message_ids, function(index, item) {
				$('#label_'+item).text('');
			});
		}
		
		$.ajax({
			type: 'POST',
			url: url,
			data: $(this).serialize(),
			success : function(response) {
				$('.ajaxGroupMessageUpdate')[0].reset();
				if(response!='label')
				{
					var trash = $('#ctrash').attr('data-trash');
					var total =$('#page_total').attr('data-total');
					var res = parseInt(trash)+parseInt(response);
					//$('#page_total').html(res);
					$('#ctrash').html(res);
					if(typee==0){
						var inbox =$('#cinbox').attr('data-inbox');
						var inboxx = parseInt(inbox)-parseInt(response);					
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#cinbox').attr('data-inbox',inboxx).text(inboxx);
					}else if(typee==1){
						var star =$('#cstar').attr('data-star');
						var starr =parseInt(star)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#cstar').attr('data-star',starr).text(starr);
						
					}else if(typee==2){
						var sent =$('#csent').attr('data-sent');
						var sentt =parseInt(sent)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#csent').attr('data-sent',sentt).text(sentt);
						
					}else if(typee==3){
						var draft =$('#cdraft').attr('data-draft');
						var draftt =parseInt(draft)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#cdraft').attr('data-draft',draftt).text(draftt);
					}else if(typee==4){
						trash = $('#ctrash').attr('data-trash');
						res = parseInt(trash)-parseInt(response);
						total = parseInt(total)-parseInt(response);
						$('#page_total').attr('data-total',total);
						$('.page_total').text(total);
						$('#ctrash').attr('data-trash',res).text(res);
					}
				}
				
			}
		});
		return false;
	});
	$('#deleteMessage').click(function(){
		var msg_ids = '';
		var is_sender = '';
		$('.checkmsg').each(function(){
			if($(this).parent('div').hasClass('checked')){
				if(msg_ids == ''){
					msg_ids = $(this).val();
					is_sender = $(this).attr('data-sender');
				} else {
					msg_ids = msg_ids+','+$(this).val();
					is_sender = is_sender+','+$(this).attr('data-sender');
				}
			}
		});
		if (msg_ids != '' && is_sender != '') {
			if (confirm("{{ Lang::get('core.to_remove_msg') }}")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="group_update_function"]').val('delete');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
	$('#deleteFromTrash').click(function(){
		var msg_ids = '';
		var is_sender = '';
		$('.checkmsg').each(function(){
			if($(this).parent('div').hasClass('checked')){
				if(msg_ids == ''){
					msg_ids = $(this).val();
					is_sender = $(this).attr('data-sender');
				} else {
					msg_ids = msg_ids+','+$(this).val();
					is_sender = is_sender+','+$(this).attr('data-sender');
				}
			}
		});
		if (msg_ids != '' && is_sender != '') {
			if (confirm("{{ Lang::get('core.permanently_delete') }}")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="group_update_function"]').val('deleteTrash');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
	$('.singleMessageDelete').click(function(){
		var msg_ids = $(this).attr('data-id');
		var is_sender = $(this).attr('data-sender');
		if (msg_ids != '' && is_sender != '') {
			if (confirm("{{ Lang::get('core.to_remove_msg') }}")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="group_update_function"]').val('delete');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
		
	$('#removeLabels').click(function(){
		var msg_ids = '';
		var is_sender = '';
		$('.checkmsg').each(function(){
			if($(this).parent('div').hasClass('checked')){
				if(msg_ids == ''){
					msg_ids = $(this).val();
					is_sender = $(this).attr('data-sender');
				} else {
					msg_ids = msg_ids+','+$(this).val();
					is_sender = is_sender+','+$(this).attr('data-sender');
				}
			}
		});
		if (msg_ids != '' && is_sender != '') {
			if (confirm("{{ Lang::get('core.remove_label') }}")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="group_update_function"]').val('removelabel');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
	
	$(document).on('click','ul#label-dropdown-menu li',function(){
		var msg_ids = '';
		var is_sender = '';
		$('.checkmsg').each(function(){
			if($(this).parent('div').hasClass('checked')){
				if(msg_ids == ''){
					msg_ids = $(this).val();
					is_sender = $(this).attr('data-sender');
				} else {
					msg_ids = msg_ids+','+$(this).val();
					is_sender = is_sender+','+$(this).attr('data-sender');
				}
			}
		});
		if (msg_ids != '' && is_sender != '') {
			if (confirm("{{ Lang::get('core.apply_label') }}")) {
				$('[name="msg_ids"]').val(msg_ids);
				$('[name="is_sender"]').val(is_sender);
				$('[name="label_id"]').val($(this).children('a').attr('data-id'));
				$('[name="label_name"]').val($(this).children('a').text());
				$('[name="group_update_function"]').val('label');
				$('.ajaxGroupMessageUpdate').submit();
			}
		}
		return false;
	});
	
	$('#AddLabel').click(function(){
		$('.saveLabel')[0].reset();
		$('.savelabelbtn').removeAttr('disabled');
		$('#label-modal').modal({
			backdrop: 'static',
			keyboard: false
		});
	});
	
	$('#editLabel').click(function(){
		$('.saveLabel')[0].reset();
		$('.savelabelbtn').removeAttr('disabled');
		$('.saveLabel').find('[name="id"]').val($('ul#label-list-group').find('li.active').children('a').attr('data-id'));
		$('.saveLabel').find('[name="label"]').val($('ul#label-list-group').find('li.active').children('a').text());
		$('#label-modal').modal({
			backdrop: 'static',
			keyboard: false
		});
	});
	
	$('#deleteLabel').click(function(){
		var currentLabel = $('ul#label-list-group').find('li.active').children('a').attr('data-id');
		if (currentLabel != '') {
			if (confirm("{{ Lang::get('core.con_delete') }}")) {
				$('.ajaxLabelDelete').find('[name="label_id"]').val(currentLabel);
				$('.ajaxLabelDelete').submit();
			}
		}
		return false;
	});
	
	$('.ajaxLabelDelete').submit(function(){
		var url = $(this).attr('action');
		var urlmodule = $('[name="urlmodule"]').val();
		
		$.ajax({
			type: 'POST',
			url: url,
			data: $(this).serialize(),
			beforeSend:function(){
				toastr.info("{{ Lang::get('core.delete_label') }}");
			},
			success : function(response) {
				window.location.href = urlmodule;
			}
		});
		return false;
	});
	
	$('.saveLabel').submit(function(){
		if($.trim($('[name="label"]').val()).length>1){
			$('.savelabelbtn').attr('disabled','disabled');
			var url = $(this).attr('action');
			var labelname = $('.saveLabel').find('[name="label"]').val();
			var labelid = $('.saveLabel').find('[name="id"]').val();
			var urlmodule = $('[name="urlmodule"]').val();
			$.ajax({
				type: 'POST',
				url: url,
				data: $(".saveLabel").serialize(),
				beforeSend:function(){
					// toastr.info('Sending Message');
				},
				success : function(response) {
					var data = jQuery.parseJSON( response );
					if(data.status == '1') {
						if(labelid == ''){
							$('ul#label-list-group').append('<li class="list-group-item"><a href="'+urlmodule+'?ty=5&lb='+data.newid+'" data-id="'+data.newid+'">'+labelname+'</a></li>');
							$('ul#label-dropdown-menu').append('<li><a href="javascript:void(0);" data-id="'+data.newid+'">'+labelname+'</a></li>');
						} else {
							$('.labelTitleDisp').text(labelname);
							$('.labelDisp').each(function(){
								if($(this).attr('data-id') == labelid) {
									$(this).text(labelname);
								}
							});
							$('ul#label-dropdown-menu li a').each(function(){
								if($(this).attr('data-id') == labelid) {
									$(this).text(labelname);
								}
							});
							$('ul#label-list-group li a').each(function(){
								if($(this).attr('data-id') == labelid) {
									$(this).text(labelname);
								}
							});
						}
						
						toastr.success("{{ Lang::get('core.update_label') }}");
					} else {
						toastr.error("{{ Lang::get('core.error_msg') }}");
					}
					$('.savelabelbtn').removeAttr('disabled');
					$('#label-modal').modal("toggle");
				}
			});
		}
		return false;
	});
});	
</script>		
@stop