@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('institution?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('institution?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('institution/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	

				<tr>
				<td width='30%' class='label-view text-right'>Logo</td>
				<td>{!! SiteHelpers::institutionlogo($row->logo,$row->email,'small') !!} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Name</td>
				<td>{{ $row->name }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Type</td>
				<td>{{ bsetecHelpers::getInstitutiontype($row->type) }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Email</td>
				<td>{{ $row->email }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Contact No</td>
				<td>{{ $row->contact_no }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Address</td>
				<td>{{ $row->address }} </td>

				</tr>

				<tr>
				<td width='30%' class='label-view text-right'>Status</td>
				<td>
				@if($row->status =='1')
					<span class="">{{ Lang::get('core.fr_mactive') }}</span>
				@else
					<span class="">{{ Lang::get('core.fr_minactive') }}</span>
				@endif
				</td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Created At</td>
				<td>{{ $row->created_at }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Updated At</td>
				<td>{{ $row->updated_at }} </td>

				</tr>
				
		</tbody>	
	</table>    
	
	</div>
</div>	

	</div>
</div>
	  
@stop