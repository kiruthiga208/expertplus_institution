@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('institution?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	

		 {!! Form::open(array('url'=>'institution/save/'.$row['id'].'?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		 {!! Form::hidden('id', $row['id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
		<div class="col-md-12">
			<fieldset>
				<legend> Institution</legend>

				<div class="form-group  " >
				<label for="Name" class=" control-label col-md-4 text-left"> Name </label>
				<div class="col-md-6">
				{!! Form::text('name', $row['name'],array('class'=>'form-control', 'placeholder'=>'', 'readonly')) !!} 
				</div> 
				<div class="col-md-2">
				</div>
				</div> 		

				<div class="form-group  " >
				<label for="Type" class=" control-label col-md-4 text-left"> Type </label>
				<div class="col-md-6">
				{!! Form::text('type', bsetecHelpers::getInstitutiontype($row->type), array('class'=>'form-control', 'placeholder'=>'',  'readonly')) !!} 
				</div> 
				<div class="col-md-2">
				</div>
				</div> 	

				<div class="form-group  " >
				<label for="Status" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Status') }} <span class="asterisk "> * </span></label>
				<div class="col-md-6">
			   	<label class='radio radio-inline'>
	    		{!! Form::radio('status',1,($row['status']==1) ? true:false,array()) !!} Approve
	    		</label>
	    		<label class='radio radio-inline'>
				{!! Form::radio('status',0,($row['status']==0) ? true:false,array()) !!} Unapprove
	 			</label>
				</div> 
				<div class="col-md-2">
				</div>
				</div>			

			</fieldset>
		</div>

		
		<div style="clear:both"></div>	
				
					
		<div class="form-group">
			<label class="col-sm-4 text-right">&nbsp;</label>
			<div class="col-sm-8">	
			<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
			<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
			<button type="button" onclick="location.href='{{ URL::to('institution?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
			</div>	  
		</div> 
		 
		{!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
<script type="text/javascript">
$(document).ready(function() { 
	 
});
</script>		 
@stop