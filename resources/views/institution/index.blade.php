@extends('layouts.app')

@section('content')
@php usort($tableGrid, "SiteHelpers::_sort") @endphp
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}"> Dashboard </a></li>
        <li class="active">{{ $pageTitle }}</li>
      </ul>	  
	  
    </div>
	
	
	<div class="page-content-wrapper m-t">	 	

<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5>
<div class="sbox-tools" >
		@if(Session::get('gid') ==1)
			<a href="{{ URL::to('bsetec/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
		@endif 
		</div>
	</div>
	<div class="sbox-content"> 	
	    <div class="toolbar-line ">
			@if($access['is_add'] ==1)
	   	<!-- 	<a href="{{ URL::to('institution/update') }}" class="tips btn btn-sm btn-white"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa fa-plus-circle "></i>&nbsp;{{ Lang::get('core.btn_create') }}</a> -->
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="bsetecDelete();" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-minus-circle "></i>&nbsp;{{ Lang::get('core.btn_remove') }}</a>
			@endif 		
			@if($access['is_excel'] ==1)
			<!-- <a href="{{ URL::to('institution/download') }}" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_download') }}"> -->
			<!-- <i class="fa fa-download"></i>&nbsp;{{ Lang::get('core.btn_download') }} </a> -->
			@endif
			<a href="javascript://ajax" data-type='Approved' class="tips btn btn-sm btn-white updatestatus" title="Approve">
			&nbsp;Approve</a>
			<a href="javascript://ajax" data-type='Unapproved' class="tips btn btn-sm btn-white updatestatus" title="Unapprove">
			&nbsp;Unapprove</a>			
		 
		</div> 		

	
	
	 {!! Form::open(array('url'=>'institution/delete/', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"> No </th>
				<th> <input type="checkbox" class="checkall" /></th>
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th width="70" >{{ Lang::get('core.btn_action') }}</th>
			  </tr>
        </thead>

        <tbody>
			<tr id="bsetec-quick-search" >
				<td class="number"> # </td>
				<td> </td>
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
					<td>			
						@if($t['field'] != 'logo')			
						{!! SiteHelpers::transForm($t['field'] , $tableForm) !!}		
						@endif
					</td>
					@endif
				@endforeach
				<td >
				<input type="hidden"  value="Search">
				<button type="button"  class=" do-quick-search btn btn-xs btn-info"> GO</button></td>
			 </tr>	        
						
            @foreach ($rowData as $row)
                <tr>
					<td width="30"> {{ ++$i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="id[]" value="{{ $row->id }}" />  </td>									
				 @foreach ($tableGrid as $field)
				 @php ($fieldname=$field['field'])
					 @if($field['view'] =='1')
					 <td>					 
					 	@if($field['attribute']['image']['active'] =='1')
							{!! SiteHelpers::showUploadedFile($row->$fieldname,$field['attribute']['image']['path']) !!}
						@else
							@if($fieldname == 'logo')	
								{!! SiteHelpers::institutionlogo($row->logo,$row->email,'small') !!}

							@elseif($fieldname == 'type')
								{!! bsetecHelpers::getInstitutiontype($row->type) !!}
							@elseif($fieldname == 'status')
								<?php if($row->$fieldname =='1'){ $row->$fieldname = '<span class="label label-success">Approved</span>'; } else{ $row->$fieldname  = '<span class="label label-danger">Unapproved</span>'; }?>
							{!! SiteHelpers::gridDisplay($row->$fieldname ,$field['field'],array()) !!}	

							@else
							@php ( $conn = (isset($field['conn']) ? $field['conn'] : array() ) )
							{!! SiteHelpers::gridDisplay($row->$fieldname,$field['field'],$conn) !!}
							@endif	
						@endif						 
					 </td>
					 @endif					 
				 @endforeach
				 <td>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('institution/show/'.$row->id.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
						@if($access['is_edit'] ==1)
						<a  href="{{ URL::to('institution/update/'.$row->id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif
												
					
				</td>				 
                </tr>
				
            @endforeach
              
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	@include('footer')
	</div>
</div>	
	</div>	  
</div>	
<script>
$(document).ready(function(){

	$('.do-quick-search').click(function(){
		$('#bsetecTable').attr('action','{{ URL::to("institution/multisearch")}}');
		$('#bsetecTable').submit();
	});

	$('.updatestatus').click(function(){
		type = $(this).data('type');
		if($(".ids:checkbox:checked").length==0){
			alert('Please select atleast one record');
			return false;
		}
		var ids = $(".ids:checkbox:checked").map(function(){
	      return this.value;
	    }).toArray();
	    $.ajax({
	    	type : 'POST',
	    	url : '{!! url("/") !!}/institution/updateapproval',
	    	data : { type: type, ids: ids},
	    	success: function(data){
	    		toastr.success("Success", data.success);
	    		// $(".ids").prop("checked", false);
	    		// $(".icheckbox_square-green").removeClass('checked');
                location.reload();
	    	},
	    	error: function(error){
	    		console.log(error);
	    	}
	    });
	});
	
});	

</script>		
@stop