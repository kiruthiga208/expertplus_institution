
@foreach($courses as $course) 
	<div class="col-sm-4">

	<div class="course_b "><div class="img_hover"><a href="{!! url('profile/'.$course->username)!!}">
	<img src="{{asset('assets/bsetec/images/spacer.gif')}}" style="background-image:url('{{ \bsetecHelpers::getImage($course->image) }}');" > 
    <span class="bg"></span>
	<div class="user-img-b">
	{!! SiteHelpers::customavatar('',$course->user_id, 'small') !!}
	<p>{!! $course->first_name.' '.$course->last_name !!}</p>
	<p>{!! bsetecHelpers::studentscount($course->course_id) !!} {!! Lang::get('core.students') !!}</p>
	</div>

	</a></div>
	<div class="detail_block">
	<h4><a href="{{ url('courseview/'.$course->course_id.'/'.$course->slug) }}" title="{!! $course->course_title !!}">{!! substr(ucfirst($course->course_title),0,30) !!}</a></h4>
	<div class="clearfix">
<!--	<div class="star_rating">
	<ul class="star_one clearfix">
	<li class="late_star ReadonlyRating startvalue" data-score=""></li>
	</ul>
	</div>-->
    <div class="ratings">
      <div class="star_rating">
            <ul class="star_one clearfix">
            <li class="late_star ReadonlyRating startvalue" data-score="{{\bsetecHelpers::checkReview($course->course_id)}}"></li>
            </ul>
            </div>
             </div>

	<p class="rate">
	    @if($course->pricing != 0)
	        {!! SiteHelpers::getCurrencyid($course->user_id) !!} {!! SiteHelpers::getCurrencymethod($course->user_id,$course->pricing,'1') !!}
	    @else
	        {!! Lang::get('core.free') !!}
	    @endif
	</p>
	</div>

	</div>

	</div>
	</div>
@endforeach

<script>
$(function(){
  $('.ReadonlyRating').raty({score: function() {return $(this).attr('data-score');},readOnly: true,starOff : "{{url('').'/assets/bsetec/static/img/star-off.png'}}",starOn: "{{url('').'/assets/bsetec/static/img/star-on.png'}}",starHalf : "{{url('').'/assets/bsetec/static/img/star-half.png'}}"});
});
</script>
