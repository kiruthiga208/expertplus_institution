@extends('layouts.frontend')

@section('content')

<div class="register-b">

<div class="login-s animated fadeInDown delayp1">
<h3>{{ Lang::get('core.Register') }}</h3>
  {!! Form::open(array('url'=>'user/create', 'class'=>'form-signup', 'parsley-validate', 'novalidate')) !!}

	@if(Session::has('message'))
		{!! Session::get('message') !!}
	@endif

  <ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li class="alert alert-danger">{{ $error }}</li>
		@endforeach
	</ul>

  <div class="clearfix">
    <ul class="signup-b">

      <li class="">
        <label>{{ Lang::get('core.firstname') }}<span class="req">*</span></label>
        <div class="form-group animated fadeInLeft">
        <i class="fa fa-user"></i>		
      	{!! Form::text('firstname', $first_name, array('placeholder'=>Lang::get('core.firstname'), 'required'=>'', 'parsley-minlength'=>'2' )) !!}
      	</div>
      </li>

      <li>
        <label>{{ Lang::get('core.lastname') }}<span class="req">*</span></label>  
        <div class="form-group animated fadeInRight">
        <i class="fa fa-user"></i>		
        {!! Form::text('lastname', $last_name, array('placeholder'=>Lang::get('core.lastname'), 'required'=>'', 'parsley-minlength'=>'2')) !!}
        </div>
      </li>

      <li>
        <label>{{ Lang::get('core.username') }}<span class="req">*</span></label>    
        <div class="form-group animated fadeInLeft">
        <i class="fa fa-user"></i>		
        {!! Form::text('username', $username, array('placeholder'=>Lang::get('core.username'), 'required'=>'', 'parsley-minlength'=>'2' )) !!}
        </div>
      </li>

      <li>
        <label>{{ Lang::get('core.email') }}<span class="req">*</span></label>
        <div class="form-group animated fadeInRight">
        <i class="fa fa-envelope"></i>	
        {!! Form::text('email', $email, array('placeholder'=>Lang::get('core.email'), 'required'=>'email', 'parsley-type'=>'email')) !!}
        </div>
      </li>

      <li>
        <label>{{ Lang::get('core.password') }}<span class="req">*</span></label>
        <div class="form-group animated fadeInLeft">
        <i class="fa fa-lock"></i>		
        {!! Form::password('password', array('placeholder'=>Lang::get('core.password'), 'required'=>'', 'id'=>'password', 'parsley-minlength'=>'6', 'parsley-maxlength'=>'12')) !!}
        </div>
      </li>

      <li>
        <label>{{ Lang::get('core.repassword') }}<span class="req">*</span></label>
        <div class="form-group animated fadeInRight">
        <i class="fa fa-lock"></i>			
        {!! Form::password('password_confirmation', array('placeholder'=>Lang::get('core.conewpassword'), 'required'=>'', 'parsley-equalto'=>'#password')) !!}
        </div>
      </li> 

      <li class="">
        <label>Institution<span class="req">*</span></label>
        <div class="form-group animated fadeInLeft">
        <i class="far fa-university"></i> 
        {!! Form::select('institution_id', $institutions,'' , array('placeholder'=>Lang::get('core.please_select') ,'class'=>'institution','required'=>'true' )) !!}
        </div>
      </li>

      <li class="">
        <label>Department<span class="req">*</span></label>
        <div class="form-group animated fadeInLeft">
        <i class="far fa-university"></i> 
        {!! Form::select('department_id', array(), null, array('class'=>'department', 'placeholder'=>Lang::get('core.please_select'), 'required'  )) !!}
        </div>
      </li>

      <li class="">
        <label>Course<span class="req">*</span></label>
        <div class="form-group animated fadeInLeft">
        <i class="far fa-university"></i> 
        {!! Form::select('institution_course_id', array(), null, array('class'=>'institution_course', 'placeholder'=>Lang::get('core.please_select'), 'required'  )) !!}
        </div>
      </li>

      <li class="">
        <label>Semester<span class="req">*</span></label>
        <div class="form-group animated fadeInLeft">
        <i class="far fa-university"></i> 
        {!! Form::select('semester_id', array(), null, array('class'=>'semester', 'placeholder'=>Lang::get('core.please_select'), 'required'  )) !!}
        </div>
      </li>

      <li>
      @if(CNF_RECAPTCHA =='true') 
        <label class="text-left"> {!! Lang::get('core.captcha')!!} <span class="req">*</span></label>    
        {{ Lang::get('core.case_sensitive') }}
        <div class="captcha-block">
        {!! captcha_img() !!} 
        <div class="form-group animated fadeInRight">
        <i class="fa fa-lock"></i>
        <input type="text" name="recaptcha_response" placeholder="{!! Lang::get('core.security_code')!!}" class="form-control" required/>
        </div>
        </div>
      @endif  
      </li>
    </ul>  
  </div>

  <div class="clearfix">
    <div class="row form-actions">
      <div class="col-sm-12">
      {!! Form::hidden('social_id', $social_id); !!}
      {!! Form::hidden('social_type', $social_type); !!}
      {!! Form::hidden('social_avatar' , $socialavatar); !!}
      {!! Form::hidden('user_type', 1); !!}
      <button type="submit" style="" class="btn btn-color animated fadeInUp">{{ Lang::get('core.signup') }}	</button>
      </div>
    </div>
  </div>      
 {!! Form::close() !!}
</div>



</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('body').removeClass();
		$('body').addClass('sxim-init');
		$('body').addClass('login-b');
		$('#front-header').addClass('front-header');	
	});

  $(document).ready(function() { 

    $('.institution').change(function(){
      institution_id = $(this).val();
      $('.department option').remove();
      $('.department').append('<option selected="selected" value="">Please Select</option>');
      $('.institution_course option').remove();
      $('.institution_course').append('<option selected="selected" value="">Please Select</option>');
      $('.semester option').remove();
      $('.semester').append('<option selected="selected" value="">Please Select</option>');
      $.ajax({
        url  : '{{ url("user/getdepartments") }}',
        data : {institution_id : institution_id},
        success : function(data){
          $.each(data, function(key,value){
            $('.department').append('<option class="'+value['id']+'" value="'+value['id']+'">'+value['department']+'</option>');
          });
        },
        error : function(error){
          console.log(error);
        } 
      });
    });

    $('.department').change(function(){
      department_id = $(this).val();
      $('.institution_course option').remove();
      $('.institution_course').append('<option selected="selected" value="">Please Select</option>');
      $('.semester option').remove();
      $('.semester').append('<option selected="selected" value="">Please Select</option>');
      $.ajax({
        url  : '{{ url("user/getcourses") }}',
        data : {department_id : department_id},
        success : function(data){
          $.each(data, function(key,value){
            $('.institution_course').append('<option class="'+value['id']+'" value="'+value['id']+'">'+value['name']+'</option>');
          });
        },
        error : function(error){
          console.log(error);
        } 
      });
    });

    $('.institution_course').change(function(){
      institution_course_id = $(this).val();
      $('.semester option').remove();
      $('.semester').append('<option selected="selected" value="">Please Select</option>');
      $.ajax({
        url  : '{{ url("user/getsemesters") }}',
        data : {institution_course_id : institution_course_id},
        success : function(data){
          $.each(data, function(key,value){
            $('.semester').append('<option class="'+value['id']+'" value="'+value['id']+'">'+value['semester']+'</option>');
          });
        },
        error : function(error){
          console.log(error);
        } 
      });
    });

  });
</script> 


@stop

