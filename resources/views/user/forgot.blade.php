@extends('layouts.frontend')

@section('content')


<div class="fgt-pwd animated fadeInDown">
<div class="login-s">
{!! Form::open(array('url'=>'user/request', 'class'=>'form-vertical')) !!}
<h3>{!! Lang::get('core.forgotpassword')!!}</h3>
@if(Session::has('message'))
		{!! Session::get('message') !!}
	@endif

  <label>{{ Lang::get('core.enteremailforgot') }}</label>
<div class="form-group animated fadeInLeft">
<i class="fa fa-user"></i>
{!! Form::text('credit_email', null, array('placeholder'=>Lang::get('core.email') ,'required'=>'email')) !!}

</div>
<ul class="parsley-error-list">
	@foreach($errors->all() as $error)
		<li class="alert alert-danger">{{ $error }}</li>
	@endforeach
</ul>
<button type="submit" class="btn btn-color animated fadeInLeft"> {{ Lang::get('core.sb_submit') }} </button> 

<a href="{!! url('user/login') !!}">{!! Lang::get('core.return_login')!!}</a>

{!! Form::close() !!}	
    </div>
</div>


<script language="javascript" type="text/javascript">
	$(function(){
		$('body').removeClass();
		$('body').addClass('sxim-init');
		$('body').addClass('login-b');
		$('#front-header').addClass('front-header');
		$('#or').click(function(){
		$('#fr').toggle();
		});
		$('input[type="checkbox"],input[type="radio"]').iCheck({
			checkboxClass: 'icheckbox_square-green',
			radioClass: 'iradio_square-green',
		});
	});
</script>
<script>

</script>
@stop