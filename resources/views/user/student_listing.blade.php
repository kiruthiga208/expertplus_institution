@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}"> {{ Lang::get('core.dashboard') }} </a></li>
        <li class="active">{{ $pageTitle }}</li>
      </ul>	  
	  
    </div>
	
	
	<div class="page-content-wrapper m-t">	 	

<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h5> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h5>

	</div>
	<div class="sbox-content scroll-sec"> 	
	    <div class="toolbar-line ">
			@if(\Session::has('iid'))

			<a href="{{ URL::to('student/update/0') }}" class="tips btn btn-sm btn-white"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa fa-plus-circle "></i>&nbsp;{{ Lang::get('core.btn_create') }}</a>

			<a href="javascript://ajax" class="tips btn btn-sm btn-white remove" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-minus-circle "></i>&nbsp;{{ Lang::get('core.btn_remove') }}</a>
		 	@endif
		</div> 		

	
	<div class="table-responsive" style="min-height:300px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"> {{ Lang::get('core.no') }} </th>
				<th> <input type="checkbox" class="checkall" /></th>
				<th> Avatar </th>
				@if(\Session::has('iid'))
				<th> Firstname </th>
				<th> Lastname </th>
				@endif
				<th> Username </th>
				<th> Email </th>
				@if(\Session::has('iid'))
				<th> Department </th>
				<th> Course </th>
				<th> Semester </th>
				@endif
				<th> Status </th>
				<th width="70" >{{ Lang::get('core.btn_action') }}</th>
			  </tr>
        </thead>

        <tbody>
       
			<tr id="bsetec-quick-search">
				{!! Form::open(array('url'=>'students', 'class'=>'form-horizontal', 'method' => 'get')) !!}
				<td class="number"> # </td>
				<td></td>
				<td></td>

				@if(\Session::has('iid'))
				<td></td>
				<td></td>
				@endif

				<td>{!! Form::select('username', $usernames, null, array('class'=>'form-control', 'placeholder'=>'--Select--')) !!}</td>
				<td>{!! Form::select('email', $emails, null, array('class'=>'form-control', 'placeholder'=>'--Select--')) !!}</td>

				@if(\Session::has('iid'))
				<td>{!! Form::select('department', $departments, null, array('class'=>'form-control', 'placeholder'=>'--Select--')) !!}</td>
				<td>{!! Form::select('institution_course', $institution_courses, null, array('class'=>'form-control', 'placeholder'=>'--Select--')) !!}</td>
				<td>{!! Form::select('semester', $semesters, null, array('class'=>'form-control', 'placeholder'=>'--Select--')) !!}</td>
				@endif

				<td>{!! Form::text('status', '', array('class'=>'form-control input-sm')) !!}</td>
				<td>
				{!! Form::hidden('search', 'search') !!}
				<button type="submit" class=" do-quick-search btn btn-xs btn-info"> GO</button></td>
				{!! Form::close() !!}
			 </tr>

			@if(count($students)>0)
			@php ($i = ($students->currentPage() - 1) * $students->perPage() + 1)
            @foreach ($students as $student)
                <tr>
					<td width="30"> {{ $i++ }} </td>
					<td width="50"><input type="checkbox" class="ids" name="id[]" value="{{ $student->id }}" />  </td>									
					<td>					 
					 	<a href="{{ URL::to('profile/'.$student->username) }}" target="_blank">{!! SiteHelpers::customavatar($student->email,$student->id,'small') !!}</a>
					</td>

					@if(\Session::has('iid'))
					<td>{{ $student->first_name }}</td>
					<td>{{ $student->last_name }}</td>
					@endif

					<td>		
						<a href="{{ URL::to('profile/'.$student->username) }}" target="_blank">{{ $student->username }}</a>
					</td>
					<td>{{ $student->email }}</td>

					@if(\Session::has('iid'))
					<td>{!! bsetecHelpers::getDepartmentName($student->department_id) !!}</td>
					<td>{!! bsetecHelpers::getInstitutionCourseName($student->institution_course_id) !!}</td>
					<td>{!! bsetecHelpers::getSemesterName($student->semester_id) !!}</td>
					@endif

					<td>
						@if($student->active =='1') 
							<span class="label label-success">{{ Lang::get('core.fr_mactive') }}</span>
						@else
							<span class="label label-danger">{{ Lang::get('core.fr_minactive') }}</span>
						@endif						 
					</td>
				 <td>	
				 		<a href="{{ URL::to('student/view/'.$student->id)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>

						<a  href="{{ URL::to('student/update/'.$student->id) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>							
					
				</td>				 
                </tr>
				
            @endforeach
            @endif
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	</div>
	<div class="text-center">{!! str_replace('/?', '?', $students->appends(request()->query())->links()) !!}</div>

	<!-- <div class="table-footer">
	<div class="row">
	 <div class="col-sm-5">
	  <div class="table-actions" style=" padding: 10px 0">
	  	@if(\Session::has('iid'))
	  		@php($sort = array('Sort', 'username'=>'Username', 'email'=>'Email', 'department_id'=>'Department', 'course_id'=>'Course', 'semester_id'=>'Semester', 'active'=>'Status'))
	  	@else
	  		@php($sort = array('Sort', 'username'=>'Username', 'email'=>'Email', 'active'=>'Status'))
	  	@endif

	  	{!! Form::open(array('url'=>'institution_students', 'class'=>'form-horizontal', 'method' => 'get')) !!}
	  	{!! Form::select('rows', array('Page', '5'=>'5', '10'=>'10', '20'=>'20', '30'=>'30', '50'=>'50'), null, array('class'=>'select-alt')) !!}
	  	{!! Form::select('sort', $sort, null, array('class'=>'select-alt')) !!}
	  	{!! Form::select('order', array('Order', 'asc'=>'Asc', 'desc'=>'Desc'), null, array('class'=>'select-alt')) !!}
		<button type="submit" class="btn btn-primary btn-sm">GO</button>	
	  	{!! Form::close() !!}
	  </div>					
	  </div>
	   <div class="col-sm-3">
		<p class="text-center" style=" padding: 25px 0">
		Total : <b>{{ count($students) }}</b>
		</p>		
	   </div>
		<div class="col-sm-4"> 
		
	  </div>
	  </div>
	</div> -->
	</div>
</div>	
	</div>	  
</div>	
<script>
$('.remove').click(function(){
	if($(".ids:checkbox:checked").length==0){
		alert('Please select atleast one record');
		return false;
	}
	var ids = $(".ids:checkbox:checked").map(function(){
      return this.value;
    }).toArray();
    $.ajax({
    	type : 'POST',
    	url : '{!! url("/") !!}/student/delete',
    	data : { ids: ids},
    	success: function(data){
            location.reload();
    	},
    	error: function(error){
    		console.log(error);
    	}
    });
});
</script>		
@stop