

<div class="course_info premium_inst">
<div class="container">

<div class="row">
<div class="col-xs-12 col-sm-3">
<div class="mobile_menu">
@include('user.user_menu')

</div>
</div>




<div class="col-xs-12 col-sm-9">
<div class="block2 clearfix">



    <div class="promo_block">
<h2>{!! Lang::get('core.Premium_instructor')!!} </h2>
<p>{!! Lang::get('core.premium_instructor_txt')!!}</p>
<div class="steps-bar">
<ul class="">
	@if($request_page=='personal_info')
		<li class="on"> <span>{!! Lang::get('core.personal')!!}</span></li>
		<li class="trans"><span>{!! Lang::get('core.profile_picture')!!}</span></li>
	  	<li><span>{!! Lang::get('core.terms_of_service')!!} </span></li>
	  	<li><span>{!! Lang::get('core.paypal_information')!!}</span></li>
	@elseif($request_page=='profile_picture')
		<li class="off"> <span>{!! Lang::get('core.personal')!!}</span></li>
		<li class="on"><span>{!! Lang::get('core.profile_picture')!!}</span></li>
	  	<li class="trans"><span>{!! Lang::get('core.terms_of_service')!!} </span></li>
	  	<li><span>{!! Lang::get('core.paypal_information')!!}</span></li>
	@elseif($request_page=='terms_service')
		<li class="off"> <span>{!! Lang::get('core.personal')!!}</span></li>
		<li class="off"><span>{!! Lang::get('core.profile_picture')!!}</span></li>
	  	<li class="on"><span>{!! Lang::get('core.terms_of_service')!!} </span></li>
	  	<li class="trans"><span>{!! Lang::get('core.paypal_information')!!}</span></li>
	@elseif($request_page=='paypal_info')
	  	<li class="off"> <span>{!! Lang::get('core.personal')!!}</span></li>
		<li class="off"><span>{!! Lang::get('core.profile_picture')!!}</span></li>
	  	<li class="off"><span>{!! Lang::get('core.terms_of_service')!!} </span></li>
	  	<li class="on"><span>{!! Lang::get('core.paypal_information')!!}</span></li>
	@endif  	
	</ul>
    </div>
</div>
    
    
		@foreach($errors->all() as $error)
				<p class="alert alert-danger">{!! $error !!}</p>
		@endforeach
	
	@if($request_page != '')
	<div class="tab-content">
	  <div class="tab-pane active m-t" id="info">
		{!! Form::open(array('url'=>'user/instructorinfo/'.$page_edit, 'method'=>'POST', 'class'=>'form-horizontal ' ,'files' => true)) !!}
    {{ Form::hidden('instructor_id',$instructor->instructor_id) }}
	@endif	
		@if($request_page=='personal_info')
        <div class="instructor_block">
		<div class="form-group">
			{!! Form::text('first_name', $info->first_name ,array('class'=>'form-control input-sm', 'placeholder'=>Lang::get('core.firstname'))) !!} 
		  </div>  

		  <div class="form-group">
			{!! Form::text('last_name', $info->last_name ,array('class'=>'form-control input-sm', 'placeholder'=>Lang::get('core.lastname'))) !!} 
		  </div>  

		  <div class="form-group">
			{!! Form::text('headline',$instructor->headline,array('class'=>'form-control input-sm', 'placeholder'=>Lang::get('core.headline'))) !!} 
		  </div>

		  <div class="form-group">
			{!! Form::text('email',$info->email ,array('class'=>'form-control input-sm', 'placeholder'=>Lang::get('core.Email'))) !!} 
		  </div> 
		  
		  <label class="sm_title">{!! Lang::get('core.biography')!!}:</label>
		  <div class="form-group" style="display:none;">
			{!! Form::textarea('biography',$instructor->biography ,array('class'=>'redactor')) !!}
		  </div> 

		  <label class="sm_title">{!! Lang::get('core.billing_info')!!} :</label>

		  <div class="form-group" style="display:none;">
			{!! Form::textarea('b_address',$instructor->b_address,array('class'=>'form-control redactor','id'=>'address')) !!} 
		  </div> 

 			<div class="form-group">
			{!! Form::text('b_city',$instructor->b_city,array('class'=>'form-control input-sm', 'placeholder'=>Lang::get('core.city'))) !!} 
		  </div> 
		   <div class="form-group">
			{!! Form::text('b_zip',$instructor->b_zip,array('class'=>'form-control input-sm', 'placeholder'=>Lang::get('core.zip_code'))) !!} 
		  </div> 
           <label class="sm_title">{!! Lang::get('core.Country') !!} :</label>
		   <div class="form-group">
   			<select class="form-control input-sm bfh-countries" name="b_country"  data-country="{{ $instructor->b_country }}"></select>
		  </div>
		<div class="form-group">
			{!! Form::text('b_phone',$instructor->b_phone,array('class'=>'form-control input-sm', 'placeholder'=>Lang::get('core.Telephone'))) !!} 
		  </div> 
		<!-- end personal information -->
		@elseif($request_page == 'profile_picture')

<div class="image_sec subblock">
<div class="video_block">
<label>{!! Lang::get('core.img_preview') !!}:*</label>
<div class=" block3">

	<div id="image_up">
			<div id="image_prograss"></div>
	</div>
	<div class="image_container" id="cropContainerPreload"></div>

   <div class="tip"><p>{!! Lang::get('core.img_preview_txt')!!}</p></div>
</div>
<div class="upload_type clearfix">
<label>{!! Lang::get('core.ad_image')!!}:</label>
<div class="format_type clearfix">
<div class="format"><span id="test_error">{!! Lang::get('core.use')!!}</span>
<div class="progress progress-striped progress_upload" id="progress_bar" style="display:none;">
<div class="progress-bar progress-bar-warning active" id="progress_bar_p"> </div>
<p id="file_name"></p>
</div>
</div> 
</div>
<div class="button-block clearfix">
	<div id="image_div" class="change_btn button_block">
		<div class="fileUpload btn btn-primary"> <span>{!! Lang::get('core.choose')!!}</span> 
			<input id="cImage" type="file" name="cImage" data-url="{!! url('').'/user/image' !!}" class="upload"/> 
			<input type="hidden" name="image_id" id="image_id" />
		</div>
	</div> 

	<div class="change_btn" id="profile_delete_btn" style="display:none;">
	    <button id='cancel_btn' style="display:none;">{!! Lang::get('core.cancel') !!}</button>
	    <input type="button" onClick='imagep_delete();' value="Change" /> 
	</div>
</div>

<input type='hidden' id='image_name' />
<input type='hidden' id='image_path' />
</div>
</div>
</div>

            
		<!-- start terms_service -->
		@elseif($request_page == 'terms_service')
        <div class="instructor_terms">
        <label>{!! Lang::get('core.instructor_terms')!!}:</label>

     {!! Lang::get('core.instructor_terms_msg')!!}


			
<label for="is_terms" class="conditions"> {!! Form::checkbox('is_terms', 1, $checked , ['id'=>'is_terms']) !!}  {!! Lang::get('core.terms_condition')!!} </label>

</div>
		<!-- end paypal information -->
		@elseif($request_page == 'paypal_info')
        <div class="instructor_block">
        <label>{!! Lang::get('core.paypal')!!}</label>
		 <div class="form-group">
			{!! Form::text('p_email',$instructor->p_email,array('class'=>'form-control input-sm', 'placeholder'=>Lang::get('core.email'))) !!} 
		  </div> 

		<div class="form-group">
			{!! Form::textarea('p_address',$instructor->p_address ,array('class'=>'form-control redactor','id'=>'address')) !!} 
		  </div> 

 			<div class="form-group">
			{!! Form::text('p_city',$instructor->p_city ,array('class'=>'form-control input-sm', 'placeholder'=>Lang::get('core.city'))) !!} 
		  </div> 
		   <div class="form-group">
			{!! Form::text('p_zip',$instructor->p_zip ,array('class'=>'form-control input-sm', 'placeholder'=>Lang::get('core.zip_code'))) !!} 
		  </div> 
          <label>{!! Lang::get('core.Country') !!}:</label>
		   <div class="form-group">
		   <select class="form-control input-sm bfh-countries" name="p_country" data-country="{{$instructor->p_country}}"></select>
		  </div>
          </div>
		@else
<div class="update_account">
			<h2> {!! Lang::get('core.update_premium') !!}</h2>
</div>
		@endif	
           </div> 
		@if($request_page != '')
          <div class="button_footer">
          <button class="btn btn-color" type="submit" id="submit"> {{ Lang::get('core.sb_savechanges') }}</button>	
		</div>
		{!! Form::close() !!}	
		@endif
	 
</div>

</div>

</div>
</div>
</div>
</div>

<!-- Cropic plugin files -->
<link href="{{ asset('assets/bsetec/js/plugins/croppic/css/croppic.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/croppic/croppic.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/croppic/jquery.mousewheel.min.js') }}"></script>
<!-- file upload plugin files -->
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.ui.widget.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-ui.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-process.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-validate.js') }}"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $('.redactor').redactor({
        buttons: ['bold', 'italic']
    });

    $('.form-group').show();
});
@if($request_page == 'profile_picture')

var error = $("#test_error");
var progress_bar = $("#progress_bar");
var cancel_btn = $("#cancel_btn");
var progress_bar_p = $('#progress_bar_p');
var delete_btn = $("#profile_delete_btn");
var image_div = $("#image_div");
var image_prograss = $('#image_prograss');
var submit_button = $('#submit');
submit_button.attr("disabled","disabled");
$(function () {
   $('#cImage').fileupload({
   	 	dataType:'json',
        add:function(e,data){
            var file=data.files[0];
            var uploadErrors = false;
            var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
            if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                error.text('{!! Lang::get("core.image_file_type_error") !!}').addClass('error_msg');
                return false;
            }

            if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000) {
                error.text('{!! Lang::get("core.image_big_size") !!}').addClass('error_msg');
                return false;
            }
            data.submit();
            error.hide();
            progress_bar.show();
            progress_bar_p.css('width','0%');
            $("#file_name").text(file.name); 
        },
		progress: function (e, data) {
            error.removeClass('error_msg').hide();
            var percentage = parseInt((data.loaded / data.total) * 100,10);
			progress_bar_p.css('width',percentage+'%');
            image_prograss.html("{!! Lang::get('core.img_uploading') !!} "+ percentage + '%');
        },
        done: function(e, data){
            error.removeClass('error_msg');
            return_data = data.result;
            v_html = "";
            if(return_data.status){
            	image_div.hide();
                delete_btn.show();
                progress_bar.show();
                progress_bar_p.show();
          		$("#image_name").val(return_data.image_name);
          		$("#image_path").val(return_data.image_path);    
            }else{
              image_prograss.html("");
              $("#file_name").text("");  
              delete_btn.hide();
              image_div.show();
              progress_bar.hide();
              progress_bar_p.hide();
              error.addClass('error_msg').css('font-size','10px').text(return_data.message).show();
              return false;
            }
            image_prograss.html("{!! Lang::get('core.uploading_full') !!}");
            progress_bar_p.removeClass('active');
            $("#file_name").text("{{ Lang::get('core.photo_crop') }}"); 
            imageCrop();
        },
    });
});

function imageCrop(){
	image_prograss.html("{!! Lang::get('core.please_wait')!!}");
    image_path = $("#image_path").val();
    image_name = $("#image_name").val();
    var croppicContainerPreloadOptions = {
        cropUrl:'{{url('')}}/user/image',
        loadPicture: image_path,
        doubleZoomControls:false,
        rotateControls:false,
        enableMousescroll:true,
        cropData:{
            "image_name":image_name,
        },
        onReset:function(){
        	$("#cropContainerPreload").html(""); 
        	imagep_delete();
    	},
    	onAfterImgCrop:function(res){ 
          $("#image_id").val(res.image_id);
      		$('#cropContainerPreload img').attr({
              	width:'100%',
              	height:'500px'
          	});
          $("#file_name").text("{{ Lang::get('core.photo_save') }}");
          submit_button.removeAttr('disabled');
    	},
    }
    var cropContainerPreload = new Croppic('cropContainerPreload', croppicContainerPreloadOptions);
     $('#cropContainerPreload img').hide();
     imageLoading();
}

function imageLoading(){
    var imgLoad = new imagesLoaded('#cropContainerPreload');
    imgLoad.on( 'done', function( instance ) {
        setTimeout(function(){
          $("#image_up").hide();
          image_prograss.html(" ");
          $('#cropContainerPreload img').show();
        },1000);
    });
}

function imagep_delete() {
    image_name = $('#image_name').val();
    $.ajax({
        url:'{!! url('').'/user/image'!!}',
        data:{image_file:image_name},
        type:"DELETE",
        success:function(res){
	        if(res == "success"){
	          $("#imageCrop").html("");
              $("#image_up,#image_div").toggle();
              $("#cropContainerPreload").html("");
              image_prograss.html("");
              error.text("{{ Lang::get('core.use') }}").css( 'font-size', '').show();
              progress_bar.hide();
              delete_btn.toggle();
              submit_button.attr("disabled","disabled");
	        } 
        },
        fail:function(){
            alert("{{ Lang::get('core.error') }}");
        }
    });    
}
@endif
@if(count($image_info))
// alert('{{ url('/') }}'+'/user/image/'+'{{$image_info->image_hash}}');
              $("#image_id").val("{{$instructor->image_id}}");
              $("#image_name").val("{{$image_info->image_title}}"+".{{$image_info->image_type}}");
              $("#image_path").val("{{ url('/') }}"+"/uploads/tmp/"+"{{$image_info->image_title}}"+".{{$image_info->image_type}}"); 
              $("#cropContainerPreload").prepend('<img/>');
              $('#cropContainerPreload img').attr({
                src : '{{ url('/') }}'+'/user/image/'+'{{$image_info->image_hash}}',
                width:'100%',
                height:'500px'
            }); 
                   $('#cropContainerPreload img').hide();
          submit_button.removeAttr('disabled');
      imageLoading();
@endif


</script>




<script>
$('input[type="checkbox"],input[type="radio"]').iCheck({
   checkboxClass: 'icheckbox_square-green',
   radioClass: 'iradio_square-green',
  });
@if($instructor->is_terms == 1)
$(".icheckbox_square-green").addClass('checked');
@endif

// 
</script>