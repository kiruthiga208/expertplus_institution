@extends('layouts.frontend')
@section('content')
<style type="text/css">
body #page-wrapper-full.gray-bg {
    background: url(../bsetec/images/banner2.png) center center no-repeat fixed;
    background-size: cover;
}
</style>
<link href="{{ asset('assets/bsetec/themes/theme3/css/theme3.css') }}" rel="stylesheet">
<div class="register-b">

@if (defined('CNF_CURRENCY'))
@php ($currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
@endif
@php( $userMembership = \bsetecHelpers::getMembershipofuseractive() )
@php( $monthly = \bsetecHelpers::siteMembership() )
<section id="pricing" class="top">
  <div class="container">
    <h3 class="pricing_head"> OUR PRICING </h3>
    <h5>We build awesome course & university sites! Don't miss out <font>join us today!</font></h5>

    <ul class="row nav-tabs nav-tabs-plans hide" id="example-two">
      @if(Auth::check() == false )
      <li id="bill_free" class=""><a data-toggle="tab" href="#billing_free">Free</span></a></li>
      @endif
      <li id="bill_monthly" class="current_page_item active" id="magic"><a data-toggle="tab" href="#billing_monthly">Monthly</a></li>
      <li id="bill_yearly" class=""><a data-toggle="tab" href="#billing_yearly">Yearly <span>save {{ \bsetecHelpers::getmaxplandiscount() }}</span></a></li>
    </ul>

    <div class="tab-content">
      @if(Auth::check() == false )
   
      <div class="fade tab-pane in" id="billing_payment">
        @include('user/payform')
      </div>
      @else
      <div class="fade tab-pane @if($tab) active @endif in" id="billing_free">
        @include('user/payform')
      </div>
      @endif
      <div id="billing_monthly" class="tab-pane fade @if(!$tab) active @endif in">

        <div class="row wow zoomIn">
          
          @if(count($monthly)>0)
          @php ($clsarry = array('plan-basic','plan-premium','plan-elite') )
          @php ($skey = 1 )
          @foreach($monthly as $key => $mvalue)
          @if($skey=='4')
          @php (  $skey=1 )
          @endif
          @if($mvalue->status || $mvalue->status_y)
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="pricing_content pricing_con_{{ $skey }}">
              <div class="pricing_heading">{!! $mvalue->plan_name !!}</div>
              <div class="pricing_amounts">
                <p><span>{!! $currency !!} {!! $mvalue->plan_amount !!}</span>Monthly</p>
                <p><span>{!! $currency !!} {!! $mvalue->plan_amount_y !!}</span>yearly</p>
                <div class="course_view_price"><a href="{{ URL::to('membership/courselist/'.$mvalue->plan_id) }}">View Courses</a></div>
              </div>
              <div class="pricing_descr">{!! $mvalue->plan_statement !!} </div>
              @if(isset($userMembership) && $mvalue->plan_id == $userMembership->plan_id)
              <p class="term_text term_btn_text"><span>{!! ucfirst($userMembership->purchase_period) !!}</span> <br>{!! Lang::get('core.renews_at') !!}: {!! date("F d, Y h:i:s A",strtotime('+1 '.str_replace('ly', '', $userMembership->purchase_period),strtotime($userMembership->created_at))) !!}</p>
              @elseif(isset($userMembership) && $mvalue->level <= $userMembership->level)
              <div class="status_text">{!! Lang::get('core.downgrade') !!}</div>
              @elseif(isset($userMembership) && $mvalue->level >= $userMembership->level)
              <div class="status_text">{!! Lang::get('core.upgrade') !!}</div>
              @endif
              @if($mvalue->plan_amount!='0')
              @if(Auth::check() == false ) @php ($btncls = '1' ) @else @php ( $btncls = '2') @endif
              @if( $mvalue->plan_id != bsetecHelpers::getusermembership() )
              <p class="term_btn_text"><a href="javascript:void(0)" class="btn btn-mixed btnopt" data-plan="{!! $mvalue->plan_name !!}" data-amount="{!! $currency !!}{!! $mvalue->plan_amount !!}" data-amount-y="{!! $currency !!}{!! $mvalue->plan_amount_y !!}" data-value="{!! $mvalue->plan_id !!}" data-status="{!! $mvalue->status !!}" data-statusy="{!! $mvalue->status_y !!}">{!! Lang::get('core.continue') !!}</a></p>
              @endif
              @endif
            </div>
          </div>
          @endif
          @php (  $skey++ )
          @endforeach
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
<input name="registered" type="hidden" value="0">
<input name="selected_plan" type="hidden" value="0">
<input name="selected_term" type="hidden" value="month">
<input name="selected_m" type="hidden" value="$0">
<input name="selected_y" type="hidden" value="$0">


</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('body').removeClass();
		$('body').addClass('sxim-init');
		$('body').addClass('login-b');
		$('#front-header').addClass('front-header');

    @if($tab)
      var $this = $('a[data-value="{{$tab}}"]');
      var $vl = $this.attr('data-value');
      var $pl = $this.attr('data-plan');
      var $am = $this.attr('data-amount');
      var $amy = $this.attr('data-amount-y');
      var  $sm = $this.attr('data-status');
      var $sy = $this.attr('data-statusy');
      $('input[name="selected_plan"]').val($pl);
      $('input[name="selected_m"]').val($am);
      $('input[name="selected_y"]').val($amy);
      $('.sel_plan').text($pl);
      $('.sel_plan_title').text($pl);
      $('.sel_plan_price').text($am);
      $('input[name="membership"]').val($vl);
      if($sm==0)
      {
        

        $('#monthly_term').prop('disabled','true');
        $('#monthly_term').iCheck('uncheck');
        var $amy = $this.attr('data-amount-y');
        $('input[name="selected_y"]').val($amy);
        $('.sel_plan_price').text($amy);
        $('input[name="membership_type"]').val('y');
        
       
      }
      else if($sy==0)
      {
        
        $('#yearly_term').prop('disabled','true');
        $('#yearly_term').iCheck('uncheck');
        var $amy = $this.attr('data-amount');
        $('input[name="selected_y"]').val($amy);
         $('.sel_plan_price').text($amy);
        $('input[name="membership_type"]').val('m');
        
      }
      else
      { 
        $('#yearly_term').iCheck('uncheck');
        $('#monthly_term').iCheck('check');
      }
     

    @endif

    $(document).on('click','#bill_free',function(){
        $('.sel_plan').text('Free');
        $('input[name="membership"]').val(0);
        $('input[name="membership_type"]').val(0);
    });

    $(document).on('click','#bill_monthly,#bill_yearly',function(){
      $('#billing_free').attr('class','tab-pane fade in');
    });

    $(document).on('click','.btnopt',function(){
      // var ty = $(this).attr('data-type');
      // var term = $(this).attr('data-term');
      var vl = $(this).attr('data-value');
      var pl = $(this).attr('data-plan');
      var am = $(this).attr('data-amount');
      var amy = $(this).attr('data-amount-y');
      var $sm = $(this).attr('data-status');
      var $sy = $(this).attr('data-statusy');

      var registered = $('input[name="registered"]').val();
      $('input[name="selected_plan"]').val(pl);
      $('input[name="selected_m"]').val(am);
      $('input[name="selected_y"]').val(amy);

      if(registered == 0){
        $('#billing_free').attr('class','tab-pane active fade in');
        $('#billing_payment').attr('class','tab-pane fade in');
      } else {
        $('#billing_free').attr('class','tab-pane fade in');
        $('#billing_payment').attr('class','tab-pane active fade in');
      }
      $('#bill_monthly').attr('class','');
      $('#billing_monthly').attr('class','tab-pane fade in');
      $('.sel_plan').text(pl);
      $('.sel_plan_title').text(pl);
      $('.sel_plan_price').text(am);
      $('input[name="membership"]').val(vl);
      
      // $('.sel_plan_price_term').text('/'+term);
      // $('.sel_plan_price_termname').text(term.toLowerCase());
      // $('input[name="membership_type"]').val(ty);

      /*if(ty == '1'){ // not logged in

      } else if(ty == '2'){ // logged in

      }*/

    });

	});
</script>


@stop

