@extends('layouts.frontend')
@section('content')


<div class="credit-history">
<div class="head-block clearfix">
<div class="title-s">
<h2>{!! Lang::get('core.withdraw_requests') !!}</h2>
</div>
<div class="right-block">
<a href="{!! url('user/credits')!!}" class="btn btn-success pull-right">{!! Lang::get('core.my_credits') !!}</a>
</div>
</div>
<div class="table-block">
<table class="table">
	<tr>
		<th>#</th>
		<th>{!! Lang::get('core.paypal_id') !!}</th>
		<th>{!! Lang::get('core.Amount') !!}</th>
		<th>{!! Lang::get('core.Requested_On') !!}</th>
		<th>{!! Lang::get('core.Status') !!}</th>
	</tr>
	@if(count($requests)>0)
	@foreach ($requests as $key => $request)
		<tr>
			<td>{!! $key+1 !!}</td>
			<td>{!! $request->paypal_id !!}</td>
			<td>{!! $request->amount !!}</td>
			<td>{!! $request->requested_on !!}</td>
			<td>{!! $request->status !!}</td>
		</tr>
	@endforeach
	@else
		<tr>
			<td colspan="6" align="center">{!! Lang::get('core.no_withdraws') !!}</td>
		</tr>
	@endif
	
</table>
</div>
</div>
<script>
$(function()
{
	$('body').removeClass();
	$('body').addClass('bsetec-init');
	$('#front-header').addClass('front-header');
});
</script>
@stop