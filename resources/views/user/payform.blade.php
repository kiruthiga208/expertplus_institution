<style type="text/css">
hr.style14 {
    border-top: 1px dashed #8c8b8b;
    border-bottom: 1px dashed #fff;
}
</style>
<div class="register-b">
  <div class="payform fade active in">
 
    @if(Session::has('message'))
    {!! Session::get('message') !!}
    @endif


    <h3>Selected Plan: <span class="sel_plan"></span></h3>
    <div class="col-md-12 m-b-xl text-center">
        <div class="col-md-4"></div>
        <div class="col-md-2">
            <div class="eo-button-box m-0-top-bottom text-center">
                <h5> Monthly </h5>
                <input type="radio" id="monthly_term" name="type" class="hidden" value="monthly_term" >
            </div>
        </div>
        <div class="col-md-2">
            <div class="eo-button-box m-0-top-bottom text-center">
                <h5> Yearly </h5>
                <input type="radio" id="yearly_term" name="type" class="hidden" value="yearly_term">
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="clearfix">
          <div class="table-payment">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                <tr>
                <!-- <th width="10%">Sl No:</th> -->
                <th width="80%">{!! Lang::get('core.name_content') !!}</th>
                <th width="20%">{!! Lang::get('core.price')!!}</th>
                </tr>
                <tr>
               @if (defined('CNF_CURRENCY'))
                        @php ( $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
                @endif
                <!-- <td>1</td> -->
                <td><span class="sel_plan_title"></span></td>
                <td><span class="sel_plan_price"></span><span class="sel_plan_price_term"></span></td>
                </tr>
                <tr id="coupon_discount_div" style="display:none;">
                <!-- <td>3</td> -->
                <td>{!! Lang::get('core.coupon_discount') !!}<span class="coupon_code_app"></span></td>
                <td>{!! $currency !!}<span id="coupon_discount"></span><span class="sel_plan_price_term"></span></td>
                </tr>
                <tr>@if(\bsetecHelpers::checkadmincoupon())
                <td colspan="2">

                  <div class="hide_block clearfix" id="coupon_code_div">
                    <div id="couponError"></div>
                    <h4 class="redeem m-l-md">{!! Lang::get('core.redeem') !!}</h4>
                    <div class="form-group clearfix">
                      <div class="col-sm-8">
                        <i class="fa fa-tag"></i>
                        <input type="text" id="couponCode" onClick="$('#checkCoupon').removeAttr('disabled');" class="couponCode" autocomplete="off" placeholder="{!! Lang::get('core.enter_coupon')!!}">
                        <button class="btn btn-mixed btn-applycoupon" type="button" id="checkCoupon">{!! Lang::get('core.Apply')!!}</button>
                      </div>
                    </div>

                  </div>

                </td>@endif
                </tr>
                <tr>
                <td style="text-align:right;">{!! Lang::get('core.you_pay') !!}</td>
                <td><div class="you_pay_btn"><span id="you_pay" class="sel_plan_price"></span><span id="coupon_price"></span>
                    <span class="sel_plan_price_term"></span></div>
                </td>
                </tr>
                </table>
                </div>

            <ul id="tabs" class="nav nav-tabs nav-tabs-payment m-t-md" data-tabs="tabs">

            @if($stripe['payment'] == '1')
            <li class="active"><a href="#stripe" data-toggle="tab">{!! Lang::get('core.card_payment') !!}</a></li>
            @endif

            @if($paypal_express_checkout['payment'] == '1')
            <li><a href="#paypal_ex" data-toggle="tab">{!! Lang::get('core.PayPal')!!}</a></li>
            @endif

            </ul>

            <div id="my-tab-content" class="tab-content m-t-md">

            @if($paypal_express_checkout['payment'] == '1')
            <div id="paypal_ex" class="tab-pane">
            <div class="well">
            {!! Form::open(array('url' => url('payment/paypalform'))) !!}
            <input type="hidden" name="membership" value="" />
            <input type="hidden" name="membership_type" value="m" />
            <input type="hidden" name="coupon_code" value="" />
            <input type="hidden" name="payment_method" value="paypal_express_checkout" />

            <div class="form-group clearfix">
            <div class="col-sm-12">
              <h2>{!! Lang::get('core.Connect_to_PayPal') !!}</h2>
              <p>{!! Lang::get('core.By_Clicking') !!} "{!! Lang::get('core.Continue_to_PayPal') !!}", <span class="sel_plan_price_termname"></span>{!! Lang::get('core.plan_will_start_paypal') !!}<span class="sel_plan_price"></span><span class="sel_plan_price_term"></span>{!! Lang::get('core.Upgrade_downgrade') !!}</p>
            </div>
            </div>

            <div class="form-group clearfix">
            <div class="col-sm-12 text-center">
            <input type="submit" class="continue_paypal"  value="{!! Lang::get('core.Continue_to_PayPal') !!}" class="btn btn-mixed animated fadeInUp">
            </div>
            </div>
            <p class="stripe-desc text-center">{!! Lang::get('core.paypal_terms') !!}</p>
            {!! Form::close() !!}
            </div>
            </div>
            @endif

            @if($stripe['payment'] == '1')
            <div id="stripe" class="tab-pane active">
            {!! Form::open(array('url' => url('payment/stripemembership'),'id'=>'payment-stripe' )) !!}
            <div class="text-center">
              <span class="payment-errors text-danger"></span>
            </div>
            <input type="hidden" name="membership" value="" />
            <input type="hidden" name="membership_type" value="m" />
            <input type="hidden" name="user_email" value="" />
            <input type="hidden" name="user_id" value="" />
            <input type="hidden" name="coupon_code" value="" />
            <input type="hidden" name="payment_method" value="stripe" />
            <input name='stripeToken' id="stripeToken" type="hidden" />

            <div class="well">
            <div class="form-group clearfix m-b-xl">
            <div class="col-sm-12">
              <!-- <h2>{!! Lang::get('core.card_payment') !!}</h2> -->

            </div>
            </div>

            <fieldset>

            <div class="clearfix m-b-sm">
            <div class="col-sm-12">
            <h4>{!! Lang::get('core.billing_info') !!}</h4>
            <p>{!! Lang::get('core.By_Clicking') !!} "{!! Lang::get('core.pay_now') !!}", <span class="sel_plan_price_termname"></span>{!! Lang::get('core.plan_will_start') !!}<span class="sel_plan_price"></span><span class="sel_plan_price_term"></span>{!! Lang::get('core.Upgrade_downgrade') !!}</p>
            </div>
            </div>

            <div class="form-group clearfix">
            <!-- <label for="{!! Lang::get('core.billingAddress1')!!}" class="control-label col-sm-4 text-left animated fadeInLeft"> {!! Lang::get('core.billingAddress1')!!} <span class="asterisk"> * </span></label> -->
            <div class="col-sm-12 animated fadeInRight">
            <i class="fa fa-map-signs"></i>
            {!! Form::text('billingAddress1', '' , array('class'=>'form-control','id'=>'billingAddress1','required'=>true,'placeholder'=>Lang::get('core.billingAddress1'),'autocomplete'=>'off')) !!}
            </div>
            </div>

            <div class="form-group clearfix">
            <!-- <label for="{!! Lang::get('core.billingCity')!!}" class="control-label col-sm-4 text-left animated fadeInLeft"> {!! Lang::get('core.billingCity')!!} <span class="asterisk"> * </span></label> -->
            <div class="col-sm-6 animated fadeInRight">
            <i class="fa fa-building-o"></i>
            {!! Form::text('billingCity', '' , array('class'=>'form-control','id'=>'billingCity','required'=>true,'placeholder'=>Lang::get('core.billingCity'),'autocomplete'=>'off')) !!}
            </div>

            <!-- <label for="{!! Lang::get('core.billingState')!!}" class="control-label col-sm-4 text-left animated fadeInLeft"> {!! Lang::get('core.billingState')!!} <span class="asterisk"> * </span></label> -->
            <div class="col-sm-6 animated fadeInRight">
            <i class="fa fa-map-o"></i>
            {!! Form::text('billingState', '' , array('class'=>'form-control','id'=>'billingState','required'=>true,'placeholder'=>Lang::get('core.billingState'),'autocomplete'=>'off')) !!}
            </div>
            </div>

            <div class="form-group clearfix">
            <!-- <label for="{!! Lang::get('core.billingCountry') !!}" class="control-label col-sm-4 text-left animated fadeInLeft">{!! Lang::get('core.billingCountry') !!} <span class="asterisk"> * </span></label> -->
            <div class="col-sm-6 animated fadeInRight">
            <div class="select-style_block">

            <select class="form-control required input-sm bfh-countries" name="billingCountry" id="billingCountry" data-country="US"></select>
            </div>
            </div>

            <!-- <label for="{!! Lang::get('core.billingPostcode')!!}" class="control-label col-sm-4 text-left animated fadeInLeft"> {!! Lang::get('core.billingPostcode')!!} <span class="asterisk"> * </span></label> -->
            <div class="col-sm-6 animated fadeInRight">
            <i class="fa fa-map-marker"></i>
            {!! Form::text('billingPostcode', '' , array('class'=>'form-control','id'=>'billingPostcode','required'=>true,'placeholder'=>Lang::get('core.billingPostcode'),'autocomplete'=>'off')) !!}
            </div>
            </div>
            </fieldset>

            <div class="clearfix">
            <div class="col-sm-12">
            <hr class="style14">
            </div>
            </div>

            <fieldset>

            <div class="clearfix m-b-sm">
            <div class="col-sm-12">
            <h4>{!! Lang::get('core.card_info') !!}</h4>
            </div>
            </div>


            <div class="form-group clearfix">
            <!-- <label for="{!! Lang::get('core.Name_on_Card')!!}" class="control-label col-sm-4 text-left animated fadeInLeft"> {!! Lang::get('core.Name_on_Card')!!} <span class="asterisk"> * </span></label> -->
            <div class="col-sm-12 animated fadeInRight">
            <i class="fa fa-address-card-o"></i>
            {!! Form::text('nameOnCard', '' , array('class'=>'form-control','id'=>'nameOnCard','required'=>true,'placeholder'=>Lang::get('core.Name_on_Card'),'autocomplete'=>'off')) !!}
            </div>
            </div>

            <div class="form-group clearfix">
            <!-- <label for="Card Number" class="control-label col-sm-4 text-left animated fadeInLeft"> {!! Lang::get('core.card_number')!!} <span class="asterisk"> * </span></label> -->
            <div class="col-sm-12 animated fadeInRight">
            <i class="fa fa-credit-card"></i>
            {!! Form::text('card-number', '' , array('class'=>'form-control','id'=>'card-number','required'=>true,'placeholder'=>Lang::get('core.enter_card'),'autocomplete'=>'off')) !!}
            </div>
            </div>

            <div class="form-group clearfix">
            <!-- <label for="card-cvc" class="control-label col-sm-4 text-left animated fadeInLeft"> {!! Lang::get('core.card_cvc') !!} <span class="asterisk"> * </span></label> -->
            <!-- <label for="expire" class="control-label col-sm-4 text-left animated fadeInLeft">{!! Lang::get('core.Expiration') !!} <span class="asterisk"> * </span></label> -->
            <div class="clearfix animated fadeInRight">
            <div class="col-xs-4 col-sm-4">
            <div class="select-style_block">
            <select class="stripe-sensitive required" name="card-expiry-month" id="card-expiry-month">
            <option value="" selected="selected">{!! Lang::get('core.mm') !!}</option>
            </select>
            </div>
            <script type="text/javascript">
            var select = $("#card-expiry-month"),
            month = new Date().getMonth() + 1;
            for (var i = 1; i <= 12; i++) {
            select.append($("<option value='"+i+"'>"+i+"</option>"))
            }
            </script>
            </div>
            <div class="col-xs-4 col-sm-4">
            <div class="select-style_block">
            <select class="stripe-sensitive required" name="card-expiry-year" id="card-expiry-year">
            <option value="" selected="selected">{!! Lang::get('core.year') !!}</option>
            </select>
            </div>
            <script type="text/javascript">
            var select = $("#card-expiry-year"),
            year = new Date().getFullYear();
            for (var i = 0; i < 30; i++) {
            select.append($("<option value='"+(i + year)+"'>"+(i + year)+"</option>"))
            }
            </script>
            </div>
            <div class="col-sm-4 col-xs-4 animated fadeInRight">
            <i class="fa fa-question-circle tips know_cvv" data-original-title="Know about CVV" style="cursor: pointer;"></i>
            {!! Form::input('number','card-cvc', '' , array('class'=>'form-control','maxlength'=>'3','id'=>'card-cvc','required'=>true,'placeholder'=>Lang::get('core.enter_cvc'),'autocomplete'=>'off')) !!}
            </div>
            </div>
            </div>

            <div class="form-group clearfix">
            <div class="col-sm-12 text-center">
            <input type="submit" id="submit_button" value="{!! Lang::get('core.pay_now') !!}" class="btn btn-mixed animated fadeInUp">
            <!-- <input type="button" value="Buy with Credit balance $ {{ \bsetecHelpers::getUserCredit()}}" id="BuyWithCredit" class="btn btn-primary btn_pay"> -->
            </div>
            </div>
            </div>

            </fieldset>

            {!! Form::close() !!}
            </div>
            @endif

            @if($stripe['payment'] != '1' && $paypal_standard['payment'] != '1' && $paypal_express_checkout['payment'] != '1')
            <h2>{!! Lang::get('core.no_payment')!!}</h2>
            @endif
            </div>
    </div>
    <div class="clearfix">
      <div class="row">
        <div class="col-sm-12">
          <ul class="parsley-error-list error-lists" style="display: none;"></ul>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="CVVinformation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="btn_close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{!! Lang::get('core.CVV_infotitle')!!}</h4>
            </div>
            <div class="modal-body" style="max-height: 700px;">
                <p class="confirmation-content">{!! Lang::get('core.CVV_information')!!}</p>
                <div class="text-center m-t-xl">
                   <img src="{{asset('assets/bsetec/images/cvc.png')}}" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- payment plugins -->
<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
<script type="text/javascript">
// payment options script start
Stripe.setPublishableKey("{{ $stripe['publishable_key'] }}");
var payment_error = $('.payment-errors');
$(document).ready(function() {

    $(document).on('click','.know_cvv',function(){
        $('#CVVinformation').modal({
            backdrop: 'static',
            keyboard: false
        });

    });

    $('#monthly_term').on('ifChecked', function(event){
        $('.sel_plan_price').text($('input[name="selected_m"]').val());
        $('input[name="membership_type"]').val('m');
    });

    $('#yearly_term').on('ifChecked', function(event){
        $('.sel_plan_price').text($('input[name="selected_y"]').val());
        $('input[name="membership_type"]').val('y');
    });

    function submit(form) {
        // given a valid form, submit the payment details to stripe
        $(form['submit-button']).attr("disabled", "disabled")
        Stripe.createToken({
            number: $('#card-number').val(),
            cvc: $('#card-cvc').val(),
            exp_month: $('#card-expiry-month').val(),
            exp_year: $('#card-expiry-year').val()
        },function(status, response) {
            if (response.error) {
                // re-enable the submit button
                $(form['submit-button']).removeAttr("disabled")
                // show the error
               payment_error.html(response.error.message);
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the stripe token
                $('#stripeToken').val(token);
                // and submit
                form.submit();
                $('#submit_button').prop('disabled', true);
                $('#submit_button').addClass('disabled');
            }
        });

        return false;
    }

    // add custom rules for credit card validating
    jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber, "Please enter a valid card number");
    jQuery.validator.addMethod("cardCVC", Stripe.validateCVC, "Please enter a valid security code");
    // We use the jQuery validate plugin to validate required params on submit
    $("#payment-stripe").validate({
        submitHandler: submit,
        rules: {
            "card-cvc" : {
                cardCVC: true,
            },
            "card-number" : {
                cardNumber: true,
            },
        },
        errorPlacement: function(error, element){
            payment_error.html(" ");
            payment_error.html(error);
        }
    });
});


// COUPON PROCESS
$('body').on('click','#checkCoupon',function(){
  var couponCode = $('#couponCode');
  var couponError = $('#couponError');
  var couponprice = $('#coupon_price');
  var coupon_discount = $('#coupon_discount');
  var coupon_div =  $('#coupon_discount_div');
  var you_pay = $('#you_pay');
  var coupon_code_div =  $('#coupon_code_div');
  $(this).attr('disabled','disabled');
  var plan_id = $('input[name="membership"]').val();
  var plan_type = $('input[name="membership_type"]').val();
  couponError.html("");
  if(couponCode.val().length > 4){
    $.ajaxSetup({
          headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
    });
    $.ajax({
      url: '{{ \URL::to("membership/couponcheck") }}',
      type: 'POST',
      dataType:'json',
      data:{code:couponCode.val(),plan_id:plan_id,plan_type:plan_type},
      }).done(function(res){
           if(res.success_message){
              couponError.html('<div class="alert alert-success">'+res.success_message+'</div>');
              coupon_div.show();
              coupon_discount.text(res.discount_amount);
              you_pay.hide();
              couponprice.text("{!! $currency !!}"+res.amount);
              // coupon_code_div.slideToggle();
              $('input[name="coupon_code"]').val(couponCode.val());
              $('.coupon_code_app').text(' ('+couponCode.val()+')');
              couponCode.val("");
            }
        }).error(function(data) {
            couponprice.text("");
            you_pay.show();
            coupon_div.hide();
            var errors = data.responseText;
            res = $.parseJSON(errors);
            couponError.html('<div class="alert alert-danger">'+res.errors+'</div>');
            $(this).removeAttr('disabled');
            couponCode.val("");
       });

  }else{
    couponError.html('<div class="alert alert-danger">{!! Lang::get("core.coupon_code_error") !!}</div>');
  }
  setTimeout(function(){
    $('.alert').fadeOut();
  },6000);
});

</script>

