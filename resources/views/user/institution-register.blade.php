@extends('layouts.frontend')
@section('content')
<style type="text/css">
.address .form-group,.logos  .form-group {
	border: none;
}
.address textarea.form-control {
	padding-left: 35px;
	font-size: 15px !important;
}

</style>

<div class="register-b">
	<div class="login-s animated fadeInDown delayp1">
		<h3>{{ Lang::get('core.institute_reg') }}</h3>
		{!! Form::open(array('url'=>'institutioncreate', 'class'=>'form-signup','files' => true, 'parsley-validate', 'novalidate' )) !!}
			@if(Session::has('message'))
				{!! Session::get('message') !!}
			@endif
            <ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li class="alert alert-danger">{{ $error }}</li>
			@endforeach
			</ul>
					<div id="image_error"></div>	

	        <div class="clearfix">
	        	<ul class="signup-b">
					<li class="">
						<label>{{ Lang::get('core.insti_name') }}<span class="req">*</span></label>
						<div class="form-group animated fadeInLeft">
							<i class="far fa-university"></i>	
	  						{!! Form::text('institution_name', $institution_name , array('placeholder'=>Lang::get('core.insti_name') ,'required'=>'', 'parsley-minlength'=>'2' )) !!}
						</div>
					</li>
					<li class="">
						<label>{{ Lang::get('core.insti_type') }}<span class="req">*</span></label>
						<div class="form-group animated fadeInLeft">
							<i class="far fa-university"></i>	
	  					{!! Form::select('institution_type', $institution_type,'' , array('placeholder'=>'Select Institution type' ,'class'=>'','required'=>'true' )) !!}
						</div>
					</li>
					<li>
						<label>{{ Lang::get('core.username') }}<span class="req">*</span></label>    
						<div class="form-group animated fadeInLeft">
						<i class="fa fa-user"></i>		
						{!! Form::text('username', '', array('placeholder'=>Lang::get('core.username') ,'required'=>'', 'parsley-minlength'=>'2' )) !!}
						</div>
					</li>
					<li class="">
						<label>{{ Lang::get('core.email_id') }}<span class="req">*</span></label>
						<div class="form-group animated fadeInLeft">
							<i class="fa fa-envelope"></i>	
	  						{!! Form::text('email', $email , array('placeholder'=>Lang::get('core.email_id'), 'required'=>'', 'parsley-type'=>'email' )) !!}
						</div>
					</li>
					<li>
						<label>{{ Lang::get('core.password') }}<span class="req">*</span></label>
						<div class="form-group animated fadeInLeft">
						<i class="fa fa-lock"></i>		
						{!! Form::password('password', array('placeholder'=>Lang::get('core.password'), 'required'=>'', 'id'=>'password', 'parsley-minlength'=>'6', 'parsley-maxlength'=>'12')) !!}
						</div>
					</li>
					<li>
						<label>{{ Lang::get('core.repassword') }}<span class="req">*</span></label>
						<div class="form-group animated fadeInRight">
						<i class="fa fa-lock"></i>			
						{!! Form::password('password_confirmation', array('placeholder'=>Lang::get('core.conewpassword'), 'required'=>'', 'parsley-equalto'=>'#password')) !!}
						</div>
					</li>
					<li class="">
						<label>{{ Lang::get('core.contact_no') }}<span class="req">*</span></label>
						<div class="form-group animated fadeInLeft">
							<i class="fa fa-university"></i>	
	  						{!! Form::text('contact_no', $contact_no , array('placeholder'=>Lang::get('core.contact_no') ,'required'=>'true', 'parsley-type'=>'phone' )) !!}
						</div>
					</li>
					
					<li class="address">
						<label>{{ Lang::get('core.address') }}<span class="req">*</span></label>
						<div class="form-group animated fadeInLeft">
							<i class="fa fa-university"></i>	
	  						 {!! Form::textarea('address',null,array('class'=>'form-control', 'placeholder'=>'Address', 'required'=>'true','rows'=>'5'   )) !!}	
						</div>
					</li>
					<li class="logos">
						<label>{{ Lang::get('core.logo') }}<span class="req">*</span></label>
						<div class="form-group animated fadeInLeft">
	  						<input type='file' name='logo' id='logo' class='' required=""  />
	  						
						</div>
						<div>
	  						<img class="img-circle user_image_selector" id="logo-circle" border="2" width="100" />
							
	  						 </div>
					</li>
				</ul>
	        </div>

	          <div class="clearfix">
          <div class="row form-actions">
        <div class="col-sm-12">
        {!! Form::hidden('user_type', 4); !!}
          <button type="submit" style="" class="btn btn-color animated fadeInUp">{{ Lang::get('core.signup') }}	</button>
          </div>
          </div>
    </div>  
		{!! Form::close() !!}
	</div>
</div>
<script type="text/javascript">
var _URL = window.URL || window.webkitURL;
var avatar_image = $('.user_image_selector');
var image_error = $('#image_error');
$("#logo").change(function(e) {
	var file, img,height;
	image_error.html("");
	avatar_image.removeAttr('src');
    if ((file = this.files[0])) {
        img = new Image();
        img.src = _URL.createObjectURL(file);
        ext = $(this).val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
		   html_error ='<div class="alert alert-danger">invalid file type!</div>';
		   image_error.html(html_error);
		   avatar_image.removeAttr('src');
		   $(this).val("");
		   return false;
		}else{
			avatar_image.attr('src',img.src);
			img.onload = function(){
				if(this.width < 500 || this.height < 500){
					html_error ='<div class="alert alert-danger">The selected image size is '+this.width+'px * '+this.height+'px. The Minimum allowed image size is 500px * 500px.</div>';
				   	image_error.html(html_error);
				   	$("#logo").val("");
				   	avatar_image.removeAttr('src');
				   	return false;
				}
			}
			
		}
    }


});

</script>
@endsection