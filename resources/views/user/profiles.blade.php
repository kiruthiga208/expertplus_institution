@extends('layouts.frontend')
@section('profile-page')

<section class="user-profile-block">
<div class="user-info">
<div class="container">
<h3>{{ $info->first_name ." ". $info->last_name }}</h3> 
<p>{{ $info->designation }}</p>
</div>
</div>
<div class="content-section">
<div class="container">
<div class="row">

<aside class="col-sm-3">
<div class="sidebar clearfix">
<div class="user_image">
	<a href="">
        {!! SiteHelpers::customavatar('',$info->id,'normal','') !!}
    </a>
</div>
@if(Auth::check() == TRUE)
    @if($info->id==\Session::get('uid'))
        <a href="{!! url('message') !!}" class="msg">{!! Lang::get('core.messages') !!}</a>
    @else
        <a href="{!! url('message?ctu='.$info->id) !!}" class="msg">{!! Lang::get('core.send_message') !!}</a>
    @endif
@else
     <a href="{!! url('user/login') !!}" class="msg">{!! Lang::get('core.send_message') !!} </a>   
@endif

<meta property="og:type" content="expert_com:course" />
<meta property="og:url" content="{!! Request::url() !!}" />
<meta property="og:title" content="{{ $info->first_name ." ". $info->last_name }}" />
<meta property="og:description" content="{{ strip_tags($info->designation) }}" />
<meta property="og:image" content="{!! \bsetecHelpers::getImageUser($info->avatar); !!}" />
<meta property="og:site_name" content="Expertplus" />
<meta property="og:locale" content="en_US" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:url" content="{!! Request::url() !!}" />
<meta name="twitter:title" content="{{ $info->first_name ." ". $info->last_name }}" />
<meta name="twitter:description" content="{{ strip_tags($info->designation) }}" />
<meta name="twitter:image" content="{!! \bsetecHelpers::getImageUser($info->avatar); !!}" />
<meta name="twitter:site" content="@expertsite" />

<ul class="social-b clearfix">
<li><a target="_blank" href="https://plusone.google.com/_/+1/confirm?hl=en&amp;url={{ Request::url() }}"> {!! Lang::get('core.site') !!}</a></li>
<li><a target="_blank" href="https://www.facebook.com/dialog/feed?app_id={!! \Config::get('services.facebook.client_id') !!}&display=popup&link={{ Request::url() }}&redirect_uri={{ Request::url() }}&picture={!! \bsetecHelpers::getImageUser($info->avatar); !!}&name={{ $info->first_name ." ". $info->last_name }}&description={{ urlencode(strip_tags($info->designation)) }}"> {!! Lang::get('core.f') !!}</a></li>
<li><a target="_blank" href="https://twitter.com/share"> {!! Lang::get('core.t') !!}</a></li>
<li><a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url={{ Request::url() }}"> {!! Lang::get('core.l') !!}</a></li>
</ul>
</div>
</aside>

<article class="col-sm-9">
<div class="content-right">
@if($info->biography)
<h5>{!! Lang::get('core.instructor') !!}</h5>
@endif
{!! $info->biography !!}
</div>
	@if(!empty($courses))
	<div class="student-course clearfix">
	<ul>
	<li><p>{!! Lang::get('core.students') !!}</p><h4>{!! $studentscount !!}</h4></li>
	<li><p>{!! Lang::get('core.courses') !!}</p><h4>{!! $coursecount !!}</h4></li>
	<li><p>{!! Lang::get('core.reviews') !!}</p><h4>{!! $ratings !!}</h4></li>
	</ul>
	</div>
	@endif
</article>
</div>
</div>
</div>

@if(!empty($courses))
<h3 class="title-l">{!! Lang::get('core.course_taught') !!} {{ $info->first_name ." ". $info->last_name }}</h3>
<div class="courses_block">
<div class="container">
		<div class="row courses-list">
        	@include('user/ajax-courses')
        </div>
        <div class="course_load">
        <div id="loadmoreajaxloader" style="display:none;"><img src="{{asset('assets/bsetec/images/ajax-loader.gif')}}"></div>
        @if($next_courses)
            <a class="btn btn-color load-more" data-userid="{!! $info->id !!}">{!! Lang::get('core.load_more') !!} </a>
        @endif
		</div>
</div>
</div>
@endif
</section>

<script type="text/javascript">
$(function()
{
	$('body').removeClass();
	$('body').addClass('bsetec-init');
	$('#front-header').addClass('front-header');
    $('.load-more').click(function()
    {
        var user_id = $('.load-more').attr('data-userid');
        $('.load-more').hide();
        var offset = $(".courses-list").children().length;
        $('div#loadmoreajaxloader').show();
        $.ajax({
        type: "POST",	
        data: {offset:offset,user_id:user_id},
        url: "{!! url('user/ajax-courses')!!}",
        success: function(html)
        {
            $('.load-more').show();
            if(html)
            {
                $(".courses-list").append(html);
                $('div#loadmoreajaxloader').hide();
            }else
            {
                $('div#loadmoreajaxloader').html("{!! Lang::get('core.no_course')!!}");
            }
        }
        });
    });
});
</script>
@stop

