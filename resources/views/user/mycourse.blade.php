@extends('layouts.frontend')
@section('content')
<div class="reg_form new_reg_form mycourse_block">
<div id="mainwrapper">
<div class="head_block clearfix">
         <h2 class="title">{!! Lang::get('core.mycourses')!!}</h2>
            @if (defined('CNF_CURRENCY'))
            @php ( $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
          @endif 
         <div class="TeachingCourse_create">
               
                <p class="discover_courses"><a class="btn course_create" id="createcourseid" href="javascript:void(0);">{!! Lang::get('core.Create_Course')!!}</a></p>
            </div>
            </div>
            
            <p class="mycourse-p">{!! Lang::get('core.mycourse_text') !!}</p>
           
           <div class="col-sm-12"> 
         <div class="tab-block-mycourse clearfix">
         <ul class="nav nav-tabs usernavbar price_align post">
            <li ><a href="{{url('user/learning')}}" > {{ Lang::get('core.Learning') }}</a></li>
            <li class="current_page_item  active"><a href="#TeachingCourse" data-toggle="tab">{{ Lang::get('core.Teaching')}}</a></li>
            <li><a href="{{url('user/wishlist')}}">{{ Lang::get('core.Wishlist') }}</a></li>

        </ul>
</div>
    </div>
   

 @if(count($course) == 0)
                <p class="empty_courses">{!! Lang::get('core.teach') !!}</p>
                @endif

    <div id='content' class="HomeScroll 1 tab-content  new_tabs ">

        <div class="usergallery tab-pane active MyCourseDiv " id="TeachingCourse">

            

            @php( $i=0 )
            <div class="my_course_section clearfix">
            @foreach($course as $course)
           
            <div class="col-xs-6 col-sm-3">
               <div class="block_course clearfix">
                <div class="business_office">

                    <div class="image">
                         <a href="{{url('course/create/'.$course->course_id.'/1') }}"><img id="image-3" src="{{ asset('assets/bsetec/images/spacer.gif') }}" class="thumb lazy" alt="{{{ $course->course_title }}}" data-original="{{ \bsetecHelpers::getImage($course->image) }}" data-src="{{ \bsetecHelpers::getImage($course->image) }}" style="background-image: url('{{ \bsetecHelpers::getImage($course->image) }}');"/>
                        </a>
                        @if($course->approved == 1)
                        <span class="Publish"> {!! Lang::get('core.published') !!}</span>
                        @else
                        <span class="DraftCourse">{!! Lang::get('core.Draft') !!}</span>
                        @endif
                    </div>

                   
                    <div class="course_detail clearfix">
                <h4>
                           <a href="{{url('course/create/'.$course->course_id.'/1') }}" title="{{$course->course_title}}"> {{{ str_limit(ucfirst($course->course_title),30) }}}</a>
                           </h4>
                           
                       <div class="star_rating">
                            <ul class="star_one clearfix">
                               <li class="late_star ReadonlyRating startvalue star_rating" data-score="{{\bsetecHelpers::checkReview($course->course_id, '')}}"></li>
                           </ul>
                        </div>    
                      
                       <p class="rate">

                        @if($course->pricing != 0)
                        {!! SiteHelpers::getCurrencymethod($course->user_id,$course->pricing) !!}
                        @else
                        {!! Lang::get('core.free') !!}
                        @endif
                    </p>
             
                
               </div>
          
               
               
           </div>
       </div>
       </div>


       @endforeach
       </div>
   </div>                 

</div>
</div>

</div>
<div class="modal fade" id="createMycourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog course_popup" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" id="btn_close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">{!! Lang::get('core.Create_Course')!!}</h4>
    </div>
    <div class="modal-body">
     <?php echo Form::open(array('url' => 'course/createcourse', 'method' => 'post','id'=>'coursecreateform')); ?>
     <div class="form-group">
        <label for="coursename">{!! Lang::get('core.create_course_title')!!}</label>
        <input type="text" name="coursename" class="form-control" id="coursename" placeholder="{!! Lang::get('core.course_name')!!}">
    </div>
    
   
    <button type="submit" id="createcourse" class="btn btn-color">{!! Lang::get('core.btn_create')!!}</button>
    <?php  echo Form::close(); ?>
    <br>
  
</div>
</div>
</div>
</div>

<style>
.error{
    color:red;
}
</style>
<script type="text/javascript">
$(function(){
    $('body').removeClass();
    $('body').addClass('bsetec-init');
    $('#front-header').addClass('front-header');
    $('.ReadonlyRating').raty({score: function() {return $(this).attr('data-score');},readOnly: true,starOff : "{{url('').'/assets/bsetec/static/img/star-off.png'}}",starOn: "{{url('').'/assets/bsetec/static/img/star-on.png'}}",starHalf : "{{url('').'/assets/bsetec/static/img/star-half.png'}}"});

    $('.datetimenosec').datetimepicker({format: 'yyyy-mm-dd hh:ii:00'});
    $('.timenosec').datetimepicker({
        format: 'hh:ii:00',
        autoclose: true,
        startView: 'day',
        minView: 'hour',
        maxView:'day',
        pickDate: false,
        startDate: new Date

    });
    $("#btn_close").click(function () {
       $("#coursename").val('');
    });
    $(document).on('click','#createcourseid',function(){
        $('#createMycourse').modal({
            backdrop: 'static',
            keyboard: false
        });

    });
    $.validator.addMethod("customwhitespace", function(value, element) {
        return (this.optional(element) || $.validator.methods.required.call(this, $.trim(value), element));
    }, "Please enter a valid course name");

   
   

    $("#coursecreateform").validate({
        ignore: [],
        rules: {
            coursename: {
                 required: true,
                // minlength: 5,
                maxlength: 60,
                customwhitespace: true,
                remote: {
                    url: "{{ URL::to('course/coursenameexits') }}",
                    type: "GET",
                    data: {
                        coursename: function() {
                            return $( "#coursename" ).val();
                        }
                    }
                }
            },
           
          
        },
        messages: {
            coursename: {
                required: "{{ Lang::get('core.enter_course') }}",
                // minlength: "Course name must consist of at least 5 characters",
                maxlength: "{{ Lang::get('core.no_length') }}",
                remote: "{{ Lang::get('core.course_taken') }}"
            },
           
        },submitHandler: function() {
            $('#createcourse').prop("disabled", true);
            return true;
        }
    });


});
</script>
@stop
