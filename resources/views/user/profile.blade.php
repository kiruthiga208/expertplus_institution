<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.inputlimiter.1.3.1.js') }}"></script>

 <div class="course_info profile_block profile">
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-3">
@include('user.user_menu')
</div>

<div class="col-xs-12 col-sm-9">
<div class="block2 account_section clearfix">
<div class="promo_block">
        <h2> {!! Lang::get('core.profile') !!}</h2>
        <p>{!! Lang::get('core.profile_msg') !!}</p>
</div>
	<div class="page-content account_block">
	@if(Session::has('message'))	  
		   {!! Session::get('message') !!}
	@endif	

		@foreach($errors->all() as $error)
			<p class="alert alert-danger">{{ $error }}</p>
		@endforeach
        
	<div class="tab-content">
	  <div class="tab-pane active m-t" id="info">
		{!! Form::open(array('url'=>'user/saveprofile/','files' => true,'id'=>'profileForm')) !!}  
		  <div class="form-group"> 
		  {!! Form::text('designation', $info->designation ,array('placeholder'=>Lang::get('core.designation'))) !!} 
		 </div> 
          <div class="form-group">	 
		  <div class="input-group head_line">
			{!! Form::text('headline', $info->headline ,array('id'=>'headline','placeholder'=>Lang::get('core.headline'))) !!} 
			<span id="headline_count" class="input-group-addon">{{ 60-strlen($info->headline) }}</span>
		  </div> 
          </div>	  
	  
		  <div class="form-group">
		  {!! Form::text('first_name', $info->first_name ,array('placeholder'=>Lang::get('core.firstname'))) !!} 
		 </div>  
		  
		  <div class="form-group">
		  {!! Form::text('last_name', $info->last_name ,array('placeholder'=>Lang::get('core.lastname'))) !!} 
		  </div>

		  <div class="form-group">
		   <label for="ipt" class=" control-label">{!! Lang::get('core.biography') !!}</label>			
			{!! Form::textarea('biography',$info->biography ,array('class'=>'redactor')) !!}			
		  </div> 
		   <div class="form-group">
		   <label for="ipt" class=" control-label">{!! Lang::get('core.language') !!}</label>
           <div class="select">
			{!! Form::select('lang', array(
				'' => Lang::get('core.select_language'),
			    'de_DE' => 'Deutsch',
			    'en_US' => 'English (US)',
				'es_ES' => 'Español (España)',
				'en_TA' => 'Tamil',
				),$info->lang,array())
    		!!}
            </div>
		  </div>
		   <div class="form-group">
		   <label for="ipt" class=" control-label">{!! Lang::get('core.select_currency') !!}</label>
           <div class="select">
           		{!! Form::select('currency',$currency,$info->currency ? $info->currency : '',array())!!}
            </div>
		  </div>
		<div class="link_block">
		  <label class="title">{!! Lang::get('core.links') !!}</label>
		
		 <div class="input-group">
		<span class="input-group-addon">http://</span>{!! Form::text('link_web', $info->link_web ,array('class' =>'form-control', 'placeholder'=>Lang::get('core.website'))) !!} 
		</div>
		<div class="input-group">
		<span class="input-group-addon">https://plus.google.com/</span>{!! Form::text('link_google_plus', $info->link_google_plus ,array('class' =>'form-control','placeholder'=>Lang::get('core.g_profile'))) !!} 
		</div>
		<div class="input-group">
		<span class="input-group-addon">http://twitter.com/</span>{!! Form::text('link_twitter', $info->link_twitter ,array('class' =>'form-control','placeholder'=>Lang::get('core.t_profile'))) !!} 
		</div>
		<div class="input-group">
		<span class="input-group-addon">http://www.facebook.com/</span>{!! Form::text('link_facebook', $info->link_facebook ,array('class' =>'form-control','placeholder'=>Lang::get('core.f_profile'))) !!} 
		</div>
		<div class="input-group">
		<span class="input-group-addon">http://www.linkedin.com/</span>{!! Form::text('link_linkedin', $info->link_linkedin ,array('class' =>'form-control','placeholder'=>Lang::get('core.l_profile'))) !!} 
		</div>
		<div class="input-group">
		<span class="input-group-addon">http://www.youtube.com/</span>{!! Form::text('link_youtube', $info->link_youtube ,array('class' =>'form-control','placeholder'=>Lang::get('core.y_profile'))) !!} 
		</div>
		   	</div>
		
	  </div>

</div>
</div>

<div class="button_footer">
				<button class="btn btn-color" type="submit" onclick="$('#profileForm').submit();"> {{ Lang::get('core.sb_savechanges') }}</button>
		  </div>
          {!! Form::close() !!}	

</div>
</div>

</div>
</div>
</div>
<script>
$(function()
{
		$('body').removeClass();
	$('body').addClass('bsetec-init');
	$('#front-header').addClass('front-header');
    $('.redactor').redactor({
        buttons: ['bold', 'italic']
    });

$('#headline').inputlimiter({
    limit: 60,
    boxId: 'headline_count'
});
});
</script>
</section>