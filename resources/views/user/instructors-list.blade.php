@extends('layouts.frontend')
@section('content')

<!--slider-->
<div class="develpoment_this_sec categ_list instructor-b ">
    <div class="container">
        <div class="row">
            <!-- category sidebar start-->
            @include('course.category-sidebar')
            <!-- category sidebar end-->
         <div class="col-sm-9 trendieng search">   
            <div class="course-list-section instructor-list-section clearfix" id="course-list-section">          
            <div class="mycourse_block">   
            <div class="leak_coursing clearfix">
                <div class="newest_coursing clearfix">
                    <div class="trebt_ot">
                        <h2 class="list_title">Instructors List</h2>  
                    </div> 
                </div>
                <div class="business_trend clearfix">
                    @if(count($instructors) == 0 )
                    <p class="empty_courses">Currently No Instructors. </p>
                    @else
                    @foreach($instructors as $instructor)
                    <div class="col-xs-6 col-sm-3">
                     <div class="block_course clearfix">
                      <div class="business_office">
                           <div class="image">
                               <a href="{{url('profile/'.$instructor->username) }}">
                                {!! \SiteHelpers::customavatar('',$instructor->user_id,'normal','thumbs lazy root') !!}
                               </a>
                            </div>
                            <div class="course_detail clearfix">
                            <h4><a href="{{url('profile/'.$instructor->username) }}" title="{{$instructor->first_name}}"> {{ substr(ucfirst($instructor->first_name.' '.$instructor->last_name),0,30) }}</a></h4>
                        </div>
                        </div>
                        </div>
                    </div>
                    @endforeach
                     @endif
                </div>
            </div>
              <div class="pull-right"> 
               @if(count($instructors)>0)
                    {!! str_replace('/?', '?', $instructors->render()) !!}
               @endif
              </div>
              </div>
            </div>
        </div>   
    </div>
</div>
</div>  
            <script>
$(function(){
  $('body').removeClass();
  $('body').addClass('course_body');
  $('#front-header').addClass('front-header');	
});
  </script>
@stop