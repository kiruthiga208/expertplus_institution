@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('students') }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('students') }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			
	   		<a href="{{ URL::to('student/update/'.$id) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
	
					<tr>
						<td width='30%' class='label-view text-right'>{{ Lang::get('core.avatar') }}</td>
						<td>{!! SiteHelpers::customavatar($student->email,$student->id,'small') !!}</td>
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ Lang::get('core.Username') }} </td>
						<td>{{ $student->username }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ Lang::get('core.firstname') }} </td>
						<td>{{ $student->first_name }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'> {{ Lang::get('core.lastname') }} </td>
						<td>{{ $student->last_name }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'> {{ Lang::get('core.email') }} </td>
						<td>{{ $student->email }} </td>
						
					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>Department </td>
						<td>{{ bsetecHelpers::getDepartmentName($student->department_id) }} </td>
						
					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>Course </td>
						<td>{{ bsetecHelpers::getInstitutionCourseName($student->institution_course_id) }} </td>
						
					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>Semester </td>
						<td>{{ bsetecHelpers::getSemesterName($student->semester_id) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ Lang::get('core.Created At') }}</td>
						<td>{{ $student->created_at }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ Lang::get('core.lastlogin') }}</td>
						@if($student->last_login != '')
						<td>{{ $student->last_login }} </td>
						@else
						<td>{{ $student->updated_at }} </td>
						@endif
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ Lang::get('core.Updated At') }}</td>
						<td>{{ $student->updated_at }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Status</td>
						@if($student->active == 1)
						<td>{!! "Active" !!} </td>
						@else
						<td>{!! "Inactive" !!}
						@endif
						
					</tr>
				
		</tbody>	
	</table>    
	
	</div>
</div>



	</div>
</div>
	  
@stop