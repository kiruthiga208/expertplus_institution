<style type="text/css">
.mycourse_block .block_course .business_office{
  height: auto;
}
.mycourse_block .block_course .business_office .img_div a img {
  width: 82px;
  height: 82px;
  max-width:82px;
  max-height:82px;
}
</style>
<div class="business_trend clearfix">
    @if(count($instructors) == 0 )
    <h4 class="empty_courses" style="margin-top: 70px;">Currently No Tutors are Online. </h4>
    @else
    @foreach($instructors as $instructor)
    <div class="col-md-12 col-sm-12">
      <div class="block_course clearfix">
        <div class="business_office">

          <div class="row">
          <div class="col-sm-2 img_div nopadding">
            <div class="image">
              <a href="{{url('profile/'.$instructor->username) }}">
                {!! \SiteHelpers::customavatar('',$instructor->id,'normal','thumbs lazy root') !!}
              </a>
            </div>
          </div>
          <div class="col-sm-10 nopadding">
            <div class="course_detail clearfix">
            <div class="row">
                <div class="col-sm-8">
                  <h3>
                    <a href="{{url('profile/'.$instructor->username) }}" title="{!! $instructor->first_name !!}"> {{ substr(ucfirst($instructor->first_name.' '.$instructor->last_name),0,30) }}</a>
                  </h3>
                </div>
                <div class="col-sm-4">
                  @if(!empty($instructor->hourly_rate) && $instructor->hourly_rate != 0)<h3> {!! $currency.' '.$instructor->hourly_rate !!}/Hour</h3>@endif
                </div>
              </div>
              <div class="row">
                <div class="col-sm-8">
                    <span class="m-r-sm" style="width: 180px;">
                      @if($instructor->ratings > 0)
                          <img src="{{ asset('assets/bsetec/static/img/star-on.png') }}" alt="1" title="bad">
                      @endif
                      @if($instructor->ratings > 1)
                          <img src="{{ asset('assets/bsetec/static/img/star-on.png') }}" alt="2" title="poor">
                      @endif
                      @if($instructor->ratings > 2)
                          <img src="{{ asset('assets/bsetec/static/img/star-on.png') }}" alt="3" title="regular">
                      @endif
                      @if($instructor->ratings > 3)
                          <img src="{{ asset('assets/bsetec/static/img/star-on.png') }}" alt="4" title="good">
                      @endif
                      @if($instructor->ratings > 4)
                          <img src="{{ asset('assets/bsetec/static/img/star-on.png') }}" alt="5" title="best">
                      @endif
                    </span>
                    @if($instructor->ratings > 0)<span>({!! $instructor->totalratings !!})</span>@endif
                </div>
                <div class="col-sm-4">
                  <h3>@if(\Cache::has('user-is-online-' . $instructor->id)) <span class="text-info"><i class="fa fa-check-circle"></i> Online</span> @elseif(\Cache::has('user-is-away-' . $instructor->id)) <span class="text-warning"><i class="fa fa-clock-o"></i> Away</span> @else <i class="fa fa-dot-circle-o"></i> Offline @endif</h3>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  @php  $skills = json_decode($instructor->skills); @endphp
                  @if(!empty($skills))<h3> @foreach($skills as $key => $skill) @if($key < 15) {!! ucfirst($skill) !!}@if(count($skills) != ($key+1)), @endif @endif @endforeach </h3>@endif
                </div>
              </div>
            </div>
          </div>
          </div>

        </div>
      </div>
    </div>
    @endforeach
     @endif
</div>
