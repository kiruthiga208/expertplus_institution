@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li>
		@if(!Session::has('iid'))
			<a href="{{ URL::to('students') }}">{{ $pageTitle }}</a>
		@else
			<a href="{{ URL::to('institution_students') }}">{{ $pageTitle }}</a>
		@endif
		</li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper m-t">


<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	
		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li class="alert alert-danger">{{ $error }}</li>
			@endforeach
		</ul>
		<div id="image_error"></div>	

		<?php
			if($student['id']!=''){
				$pgurl = 'student/save/'.$student['id'];
			} else {
				$pgurl = 'student/save/0';
			}
		?>

		 {!! Form::open(array('url'=>$pgurl, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		 {!! Form::hidden('id', $student['id']) !!}
		 {!! Form::hidden('return', $previous_url) !!}
		<div class="col-md-6">			
								  	
		  <div class="form-group  " >
			<label for="Username" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Username') }} <span class="asterisk "> * </span></label>
			<div class="col-md-6">
			  {!! Form::text('username', $student['username'], array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'disabled'=>$disabled  )) !!} 
			 </div> 
			 <div class="col-md-2">
			 	
			 </div>
		  </div> 					
		  					
		  <div class="form-group  " >
			<label for="Email" class=" control-label col-md-4 text-left"> {{ Lang::get('core.email') }} <span class="asterisk "> * </span></label>
			<div class="col-md-6">
			  {!! Form::text('email', $student['email'], array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'email', 'disabled'=>$disabled   )) !!} 
			 </div> 
			 <div class="col-md-2">
			 	
			 </div>
		  </div> 	

		  @if(\Session::has('iid'))
		  	<div class="form-group  " >
			<label for="First Name" class=" control-label col-md-4 text-left"> {{ Lang::get('core.firstname') }}  <span class="asterisk "> * </span></label>
			<div class="col-md-6">
			  {!! Form::text('first_name', $student['first_name'], array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-minlength'=>'2'  )) !!} 
			 </div> 
			 <div class="col-md-2">
			 	
			 </div>
		  </div> 

		  <div class="form-group  " >
			<label for="Last Name" class=" control-label col-md-4 text-left"> {{ Lang::get('core.lastname') }} <span class="asterisk "> * </span> </label>
			<div class="col-md-6">
			  {!! Form::text('last_name', $student['last_name'], array('class'=>'form-control', 'placeholder'=>'',  'required'=>'true', 'parsley-minlength'=>'2' )) !!} 
			 </div> 
			 <div class="col-md-2">
			 	
			 </div>
		  </div> 


		  
		@endif				

		  <div class="form-group  " >
			<label for="Status" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Status') }} <span class="asterisk "> * </span></label>
			<div class="col-md-6">
			   	<label class='radio radio-inline'>
	    		{!! Form::radio('active',1,($student['active']==1) ? true:false,array()) !!} Active
	    		</label>
	    		<label class='radio radio-inline'>
				{!! Form::radio('active',0,($student['active']==0) ? true:false,array()) !!} Inactive
	 			</label>
			 </div> 
			 <div class="col-md-2">
			 </div>
		  </div>  

		  @if(\Session::has('iid'))
		  <div class="form-group  " >
			<label for="Avatar" class=" control-label col-md-4 text-left"> {{ Lang::get('core.avatar') }} </label>
			<div class="col-md-6">
			<input  type='file' name='avatar' id='avatar' @if($student['avatar'] =='') class='required' @endif   />
				<div>
				@if($student['id'] !="" && $student['avatar'] != 0)
					{!! SiteHelpers::customavatar($student['email'], $student['id'], 'small', 'img-circle user_image_selector') !!}
				@else
					<img class="img-circle user_image_selector" id="avatar-circle" border="2" width="100" />
				@endif
				</div>					

			</div> 
			<div class="col-md-2"> 	</div>
		  </div>
		  @endif
		</div>		
		<div class="col-md-6">	  
			@if(\Session::has('iid'))
			<div class="form-group">
				<label for="ipt" class=" control-label col-md-4"> {{ Lang::get('core.password') }}@if($student['id'] =='')<span class="asterisk ">*</span>@endif</label>
				@if($student['id'] =='')
					@php($required='')
				@else
					@php($required=false)
				@endif
				<div class="col-md-8">
				{!! Form::password('password', array('class'=>'form-control input-sm password', 'required'=>$required , 'id'=>'password', 'parsley-minlength'=>'6', 'parsley-maxlength'=>'12' )) !!}
				 </div> 
			</div>  
		  
			  <div class="form-group">
				<label for="ipt" class=" control-label col-md-4"> {{ Lang::get('core.conewpassword') }} 
				@if($student['id'] =='')
				<span class="asterisk ">*</span>
				@endif
				</label>
				<div class="col-md-8">
				{!! Form::password('password_confirmation', array('class'=>'form-control input-sm', 'required'=>$required, 'parsley-equalto'=>'#password', 'data-parsley-iff'=>'#password', 'data-parsley-iff-message'=>'Passwords must match')) !!}	 
				 </div> 
			  </div>

			  <div class="form-group  " >
				<label for="Department" class=" control-label col-md-4 text-left"> Department <span class="asterisk "> * </span></label>
				<div class="col-md-6">
				{!! Form::select('department_id', $departments, $student['department_id'], array('class'=>'form-control department', 'placeholder'=>Lang::get('core.please_select'), 'required'  )) !!} 
				</div> 
				<div class="col-md-2">
				</div>
			  </div> 				

			  <div class="form-group  " >
				<label for="Institution Course" class=" control-label col-md-4 text-left"> Institution Course <span class="asterisk "> * </span></label>
				<div class="col-md-6">
				{!! Form::select('institution_course_id', $courses, $student['institution_course_id'], array('class'=>'form-control institution_course', 'placeholder'=>Lang::get('core.please_select'), 'required'  )) !!} 
				</div> 
				<div class="col-md-2">
				</div>
			  </div> 				

			  <div class="form-group  " >
				<label for="Semester" class=" control-label col-md-4 text-left"> Semester <span class="asterisk "> * </span></label>
				<div class="col-md-6">
				{!! Form::select('semester_id', $semesters, $student['semester_id'], array('class'=>'form-control semester', 'placeholder'=>Lang::get('core.please_select'), 'required'  )) !!} 
				</div> 
				<div class="col-md-2">
				</div>
			  </div>  				  
		 	@endif
		 </div>	
			
		<div style="clear:both"></div>	
		<div class="form-group">
			<label class="col-sm-4 text-right">&nbsp;</label>
			<div class="col-sm-8 btn-section">	
			<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
			<button type="button" onclick="location.href='{{ URL::to('students') }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
			</div>
		</div>  
		{!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			
<script type="text/javascript">
$(document).ready(function() { 

	

	department_id = $('.department').val();
	getCourses(department_id);

	setTimeout( function(){
		institution_course_id = $('.institution_course').val();
		getSemesters(institution_course_id);
	}, 1000);
	
	
	$('.department').change(function(){
		department_id = $(this).val();
		$('.institution_course option').remove();
		$('.institution_course').append('<option selected="selected" value="">Please Select</option>');
		$('.semester option').remove();
		$('.semester').append('<option selected="selected" value="">Please Select</option>');
		getCourses(department_id);
	});

	$('.institution_course').change(function(){
		institution_course_id = $(this).val();
		$('.semester option').remove();
		$('.semester').append('<option selected="selected" value="">Please Select</option>');
		getSemesters(institution_course_id);
    });
});

function getCourses(department_id){
	$.ajax({
		url  : '{{ url("user/getcourses") }}',
		data : {department_id : department_id},
		success : function(data){
			$.each(data, function(key,value){
				$('.institution_course').append('<option class="'+value['id']+'" value="'+value['id']+'">'+value['name']+'</option>');
				if(value['id']=="{{ $student['institution_course_id'] }}"){
					$('.institution_course option').removeProp('selected');
					$('.institution_course option.'+value['id']).prop("selected", "selected");
					getSemesters(value['id']);
				}
			});
		},
		error : function(error){
			console.log(error);
		} 
	});
}

function getSemesters(institution_course_id){
	$.ajax({
        url  : '{{ url("user/getsemesters") }}',
        data : {institution_course_id : institution_course_id},
        success : function(data){
			$.each(data, function(key,value){
				$('.semester').append('<option class="'+value['id']+'" value="'+value['id']+'">'+value['semester']+'</option>');
				if(value['id']=="{{ $student['semester_id'] }}"){
					$('.semester option').removeProp('selected');
					$('.semester option.'+value['id']).prop("selected", "selected");
				}
			});
        },
        error : function(error){
			console.log(error);
    	} 
  	});
}

var _URL = window.URL || window.webkitURL;
var avatar_image = $('.user_image_selector');
var image_error = $('#image_error');
var remove_btn = $('#removeImage');
$("#avatar").change(function(e) {
	var file, img,height;
	image_error.html("");
	avatar_image.removeAttr('src');
    if ((file = this.files[0])) {
        img = new Image();
        img.src = _URL.createObjectURL(file);
        ext = $(this).val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
		   html_error ='<div class="alert alert-danger">invalid file type!</div>';
		   image_error.html(html_error);
		   avatar_image.removeAttr('src');
		   remove_btn.hide();
		   $(this).val("");
		   return false;
		}else{
			avatar_image.attr('src',img.src);
			remove_btn.show();
			img.onload = function(){
			}
			
		}
    }
});

</script>	 
@stop