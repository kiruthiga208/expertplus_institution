@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('clientbanner?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	

		 {!! Form::open(array('url'=>'clientbanner/save/'.$row['id'], 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
			<fieldset>
			<legend> {!! Lang::get('core.client_banner') !!}</legend>
								
							<div id="image_error"></div>
							{!! Form::hidden('id', $row['id'],array('class'=>'form-control','id'=>'banner_id', 'placeholder'=>'',   )) !!} 
							<div class="form-group  " >
							<label for="Banner Title" class=" control-label col-md-4 text-left"> {!! Lang::get('core.client_title') !!} </label>
							<div class="col-md-6">
							{!! Form::text('banner_title', $row['banner_title'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Banner Image" class=" control-label col-md-4 text-left"> {!! Lang::get('core.client_image') !!} </label>
							<div class="col-md-6">
							<input type='file' name='banner_image' id='banner_image' />
							@if($row['banner_image'] !="")	
								@php ( $banner_path = 'uploads/banner/'.$row['banner_image'] )
								@if(file_exists($banner_path))
                                <div style="margin-top:15px;">
								<img class="img-circle" id="banner-circle" border="2" width="100" src="{{ url('').'/uploads/banner/'.$row['banner_image'] }}" />                                
								<span id="removeImage" style="margin-top:35px;color:red;cursor:pointer; margin-left:10px;">{!! Lang::get('core.x_remove') !!}</span>
                                </div>
								@endif
							@else
                            <div style="margin-top:15px;">
							<img class="img-circle" id="banner-circle" border="2" width="100" />                            
							<span id="removeImage" style="margin-top:35px;color:red;cursor:pointer;display:none; margin-left:10px;">{!! Lang::get('core.x_remove') !!}</span>
                            </div>
							@endif
							</div> 
							<!--<div class="col-md-2">
							
							</div>-->
							</div> 					
							<div class="form-group  " >
							<label for="Banner Status" class=" control-label col-md-4 text-left"> {!! Lang::get('core.banner_status') !!} </label>
							<div class="col-md-6">
							{!! Form::checkbox('banner_status','1', $row['banner_status'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							</fieldset>
		</div>

		
			<div style="clear:both"></div>	
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('clientbanner?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
<script type="text/javascript">
var _URL = window.URL || window.webkitURL;
var banner_image = $('#banner-circle');
var image_error = $('#image_error');
var remove_btn = $('#removeImage');
$("#banner_image").change(function(e) {
	var file, img,height;
	image_error.html("");
	banner_image.removeAttr('src');
    if ((file = this.files[0])) {
        img = new Image();
        img.src = _URL.createObjectURL(file);
        ext = $(this).val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
		   html_error ='<div class="alert alert-danger">{!! Lang::get("core.banner_invalid")!!}</div>';
		   image_error.html(html_error);
		   banner_image.removeAttr('src');
		   remove_btn.hide();
		   $(this).val("");
		   return false;
		}else{
			banner_image.attr('src',img.src);
			remove_btn.show();
			img.onload = function(){
				if(this.width != 145 || this.height != 125){
					html_error ='<div class="alert alert-danger">{!! Lang::get("core.selected_image") !!} '+this.width+'px * '+this.height+'px. {!! Lang::get("core.exact_image") !!}</div>';
				   	image_error.html(html_error);
				   	$("#banner_image").val("");
				   	banner_image.removeAttr('src');
				   	remove_btn.hide();
				   	return false;
				}
			}
			
		}
    }
});
remove_btn.on('click',function(){
	var banner_id = $('#banner_id').val();
	if(banner_id != ""){
		if(confirm('{!! Lang::get("core.delete_image")!!}'))
		{
			$.ajax({
				url:'{!! url('').'/clientbanner/clientbanner'!!}',
		        data:{id:banner_id},
		        type:"DELETE",
		        success:function(res){
		        	$("#banner_image").val("");
		        	banner_image.removeAttr('src');
		        	remove_btn.hide();
		        },
		        fail:function(){
		            alert("error");
		        }
			});
		}
	}else{
		$("#banner_image").val("");
		banner_image.removeAttr('src');
		remove_btn.hide();	
	}
});
</script>		 
@stop