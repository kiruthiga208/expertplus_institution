@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('advertisement?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('advertisement?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('advertisement/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
	
				<tr>
				<td width='30%' class='label-view text-right'>Id</td>
				<td>{{ $row->id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Name</td>
				<td>{{ $row->name }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Code</td>
				<td>
					@php ( $code = str_replace('[','',$row->code))
					{{ str_replace(']','',$code) }}
				</td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Script</td>
				<td>{{ $row->script }} </td>

				</tr>

				<tr>
				<td width='30%' class='label-view text-right'>Status</td>
				<td> @if($row->status==1)<span class="label label-primary">Active</span>@else <span class="label label-danger">Inactive </span> @endif</td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Created At</td>
				<td>{{ $row->created_at }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Updated At</td>
				<td>{{ $row->updated_at }} </td>

				</tr>
				
				
				
		</tbody>	
	</table>    
	
	</div>
</div>	

	</div>
</div>
	  
@stop