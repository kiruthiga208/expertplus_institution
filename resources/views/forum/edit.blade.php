@extends('layouts.app')

@section('content')
@if( Auth::check() )
	@php( $auth=Auth::user()->id )
@endif
<link rel="stylesheet" href="{{asset('assets/bsetec/css/styles.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/bsetec/css/redactor.css') }}" />
<script type="text/javascript" src="{{ asset('assets/bsetec/js/redactor.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(
	function()
	{
		$('#redactor').redactor();
	}
);
</script>
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('forum?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> {{ $pageTitle }} <small></small></h4></div>
	<div class="sbox-content"> 	
		<ul class="parsley-error-list">
			
		</ul>	
		@foreach($row as $row)
		@php ( $id=$row->forum_id )
		@php ( $title=$row->forum_topic )
		@php ( $cat_id=$row->category_id )
		@php ( $content=$row->forum_content )
		@php ( $status=$row->status )
		
		@endforeach
		
		 {!! Form::open(array('url'=>'forum/forumsave/', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		 	
		 	<div class="form-group hidethis " style="display:none;">
				<label for="Id" class=" control-label col-md-4 text-left"> Id </label>
				<div class="col-md-6">
				  {!! Form::hidden('id', $id,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
				 </div> 
				 <div class="col-md-2">
				 	
				 </div>
			 </div> 

			 <div class="form-group  " >
					<label for="Title" class=" control-label col-md-4 text-left"> {!! Lang::get('core.Title') !!} <span class="text-danger"> * </span></label>
					<div class="col-md-6">
					  {!! Form::text('title', $title,array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
					 </div> 
					 <div class="col-md-2">
					 	
					 </div>
			</div> 
			
			 <div class="form-group  " >
				<label for="Group / Level" class=" control-label col-md-4 text-left"> {!! Lang::get('core.forum_category')!!} <span class="text-danger"> * </span></label>
				<div class="col-md-6">
				  <select name='cat_id' rows='5' id='cat_id' code='' 
					class='select2 '  required  >
					<option value="">{!! Lang::get('core.select_category_name') !!}</option>
					@foreach($category as $cat)
						@if($cat_id==$cat->forum_cat_id)
							@php( $sel='selected' )
							<option value='{{$cat->forum_cat_id}}' selected="{{'selected'}}"  >{{$cat->name}}</option>
						@else
							<option value='{{$cat->forum_cat_id}}'   >{{$cat->name}}</option>
						@endif
						
					@endforeach
						
				</select> 
				 </div> 
				 <div class="col-md-2">
				 	
				 </div>
			  </div>
			   <div class="form-group  " >
					<label for="editor" class=" control-label col-md-4 text-left"> {!! Lang::get('core.Description')!!} <span class="text-danger"> * </span></label>
					<div class="col-md-6">
					  <!-- {!! Form::textarea('content', $content,array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!}  -->
						<textarea id="redactor" name="content" class="form-control" value="{{$content}}" required>{{ strip_tags($content)}}</textarea>
					 </div> 
					 <div class="col-md-2">
					 	
					 </div>
				</div> 

				<div class="form-group  " >
					<label for="editor" class=" control-label col-md-4 text-left"> {!! Lang::get('core.Enable')!!} <span class="text-danger">  </span></label>
					<div class="col-md-6">
					
					@if($status==1)
					 <input type="checkbox" name="status"  checked='checked'/>
					@else
					 <input type="checkbox" name="status"  />
					@endif
					 </div> 
					 <div class="col-md-2">
					 	
					 </div>
				</div> 
				<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<!-- <button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button> -->
					<button type="button" onclick="location.href='{{ URL::to('forum') }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					
					
					</div>	  
			
				  </div> 
		 	
		 {!! Form::close() !!}
		
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		 
	});
	</script>		 
@stop