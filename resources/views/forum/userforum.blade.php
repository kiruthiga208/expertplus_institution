
<script>
	$(document).ready(function(){
		$("#error").fadeOut(3000);
		$('body').addClass('forum-p');
	
    $(".editt").click(function(){
        var title=$(this).attr('data-ti');
        var category=$(this).attr('data-cat');
        var category_id=$(this).attr('data-catid');
        var des=$(this).attr('data-des');
        var status=$(this).attr('data-st');
        var forum_id=$(this).attr('data-id');
        $("#edit_cat #cat_id").val(category_id);
        //$('.cat_id').html(category);
        $('.title').val(title);
        $('.content').val(des);
        $('.forum_id').val(forum_id)
        if(status==1){
        	$('.estatus').prop('checked',true)
        }
       	
    });


    $(".delete").click(function(){
		var res=window.confirm('{!! Lang::get("core.forum_del") !!}')
		var forum_id=$(this).attr('data-id')
		if(res==true)
			window.location.href="user-forum/delete/"+forum_id;
		else
			window.location.href="";
	});
});
</script>
@if(Auth::check())
	@php  $user_id=Auth::user()->id @endphp
@else
	@php  $user_id =''  @endphp
@endif
     
   <div class="breadcrumb-c">
                  <div class="container">
                   <ul>
        <li><a href="{{ url('') }}">{{ Lang::get('core.home') }}</a></li>
         @if($pageTitle != "Forum")
        <li><a href="{!! url('user-forum') !!}">{{ Lang::get('core.Forum') }}</a></li>
        @endif
		<li class="active"> <span>{!! $pageTitle !!} </span></li>
     </ul>		
                  </div>
                  </div>
                    
     
	<section class="forum_block" id="why">
	<div class="container">
	<div class="row">
	<div class="col-sm-9">
 	<h3 class="title">{!! $pageTitle !!}</h3>
     
     @if(!empty($message))
	 <p class="no-record"> {{ $message }}</p>
     @endif
     @php $i=0; $forums = $forum;  @endphp
     
	  @foreach($forum as $forum)
	  <div class="subblock_forum clearfix">
      <div class="block3 clearfix">
      <div class="forum-b">
		 <p class="forum_topic"> <a href="{{url('user-forum/'.$forum->forum_id	)}}" >{{ $forum->forum_topic }}  </a> </p>
       
		 @php $content=$forum->forum_content @endphp
	<p class="description">	 {{ strip_tags($content) }} </p>
      </div>
      @if(count($anscount)>0)
		@foreach($anscount as $c)
      		@if( $forum->forum_id == $c->forum_id)
      		@php $cc=1 @endphp
      		<div class="ans_block"><p class="count">{!! $c->total !!}</p><p class="ans">{!!Lang::get('core.Answers')!!}</p></div>
      		@php break @endphp
      		@else
      		@php $cc=0 @endphp
      		@endif
      	@endforeach
      	@if($cc==0)
      	<div class="ans_block"><p class="count">0</p><p class="ans">{!!Lang::get('core.Answers')!!}</p></div>
      	@endif
      @endif
      	
</div>
   <div class="created-block clearfix">
		<p class="created">{!! Lang::get('core.Created_By') !!} &nbsp; :&nbsp; <a href="{{url('profile/'.$forum->username)}}"> {{$forum->username}}</a> &nbsp; - &nbsp;<a href="{{url('topic/'.$forum->category_slug)}}">{{$forum->name}}</a> &nbsp; -&nbsp;  <span class="timeago" title="{{ date('Y - m - d',strtotime($forum->created_at)) }}">{!! SiteHelpers::changeFormat($forum->created_at) !!} </span></p>
        	@if( Auth::check() )
			@if(Auth::user()->id == $forum->created_by )
			<div class="blog_editblocks">
		    <a title="{!! Lang::get('core.btn_edit') !!}" class="btn btn-info padding_width editt" data-catid="{{$forum->category_id}}" data-id="{{$forum->forum_id}}" data-ti="{{ $forum->forum_topic }}" data-cat="{{$forum->name}}" data-des="{{ strip_tags($content) }}" data-st='{{$forum->status}}' data-toggle="modal" data-target="#myModal1">
		        <i class="fa fa-pencil-square-o "></i>
		    </a>
		     <a title="{!! Lang::get('core.btn_remove') !!}"  data-id="{{$forum->forum_id}}" class="btn btn-danger padding_width delete" >
		        <i class="fa fa-trash-o"></i>
		    </a>
		</div>
		@endif
		@endif
        </div>
		</div>
	  @endforeach
       @if(count($forums)>0){!! str_replace('/?', '?', $forums->render()) !!}  @endif	
	 </div>
     <div class="col-sm-3">
     <div class="categories_block">
		<h4>{!! Lang::get('core.Categories')!!}</h4>
		<ul>
		<li><a href="{{url('user-forum')}}"><i class="fa fa-angle-right icon_forum"></i> {!! Lang::get('core.ALL')!!}</a></li>
		@foreach($category as $cat)
		<li><a href="{{url('topic/'.$cat->slug)}}"><i class="fa fa-angle-right icon_forum"> </i> {{ $cat->name }}</a></li>
		@endforeach
        </ul>
	</div>
     <div class="add_forum_block">
      @if( Auth::check() )
     <ul><li class="add_forum">
     <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> {!! Lang::get('core.Add_Forum')!!}</button></li>
	 <li class="disable_forum"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal3"> {!! Lang::get('core.View_Disable_Forum')!!}</button></li></ul>
      @endif
       </div>
     </div>
	  </div>
	  </div>	
	</section>
<div id="myModal3" class="modal fade" role="dialog">
  <div class="modal-dialog add_forum">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{!! Lang::get('core.View_Disable_Forum')!!}</h4>
      </div>
      <div class="modal-body">
      	@if(count($disable)>0)
		{!! Form::open(array('url'=>'user-forum/enable', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		 	
		 	<div class="form-group hidethis " style="display:none;">
				<label for="Id" class=" control-label col-md-4 text-left"> Id </label>
				<div class="col-md-6">
				{!! Form::hidden('user_id',$user_id,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
				</div> 
			</div> 
			<div class="form-group">
            <div class="row">
            <div class="col-xs-4">
				<label for="editor" class=" control-label text-left txt_head"> {!! Lang::get('core.Forum_Topic')!!} <span class="asterix">  </span></label></div>
				<div class="col-xs-8">
				 <p class="txt_head">{!! Lang::get('core.Status')!!}</p>
				 </div> 
                 </div>
				 
			</div> 
			@foreach($disable as $dis)
			<div class="form-group">
            <div class="row">
            <div class="col-xs-4">
				<label for="editor" class=" control-label text-left"> {{$dis->forum_topic}} </label>
                </div>
				<div class="col-xs-8">
				 <input type="checkbox" name="status[]" value="{{$dis->forum_id}}"  />
				</div> 
                </div>
				
			</div> 
			@endforeach

				<div style="clear:both"></div>	
			  <div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">	
				<button type="submit" name="submit" class="btn btn-color" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				<button type="button" onclick="location.href='{{ URL::to('user-forum') }}' " class="btn btn-color "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>	  
		
			  </div> 
		 {!! Form::close() !!}
		 @else
			<span>{{ Lang::get('core.no_forum') }}</span>
		 @endif

      </div>
    </div>
  </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog add_forum">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{!! Lang::get('core.Add_Forum')!!}</h4>
      </div>
      <div class="modal-body">
        @php ( $id='' )
		@php ( $title='' )
		@php ( $cat_id='' )
		@php ( $desc='' )
		@php ( $status='' )
		{!! Form::open(array('url'=>'user-forum/foruminsert/', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		 	
		 	<div class="form-group hidethis " style="display:none;">
				<label for="Id" class=" control-label text-left"> Id </label>
				<div class="col-md-6">
				  {!! Form::hidden('id', $id,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
				  {!! Form::hidden('user_id',$user_id,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
				 </div> 
				 <div class="col-md-2">
				 	
				 </div>
			 </div> 

			 <div class="form-group">
             <div class="row">
             <div class="col-sm-4">
					<label for="Title" class=" control-label text-left"> {!! Lang::get('core.Title')!!} <span class="asterix"> * </span></label>
                    </div>
					<div class="col-sm-8">
					  {!! Form::text('title', $title,array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
					 </div> 
                     </div>
					 
			</div> 
			
			 <div class="form-group">
             <div class="row">
             <div class="col-sm-4">
				<label for="Group / Level" class=" control-label text-left">  {!! Lang::get('core.forum_category')!!}<span class="asterix"> * </span></label>
                </div>
				<div class="col-sm-8">
				  <select name='cat_id' rows='5' id='cat_id' code='' 
					class='select2 '  required  >
					<option value=''>{!! Lang::get('core.select_category_name')!!}</option>
					@foreach($cforum as $cat)
						<option value='{{$cat->forum_cat_id}}'>{{$cat->name}}</option>
					@endforeach
				</select> 
				 </div> 
                 </div>
				 
			  </div>
			   <div class="form-group">
               <div class="row">
                <div class="col-sm-4">
					<label for="editor" class=" control-label text-left">  {!! Lang::get('core.Description')!!}<span class="asterix"> * </span></label>
                    </div>
					<div class="col-sm-8">
					<div class="adjoined-bottom">
						<div class="grid-container">
							<div class="grid-width-50">
								<textarea id="editor" name="content" required></textarea>
							</div>
						</div>
					</div>
					  
					 </div> 
                     </div>
				</div> 

				<div class="form-group">
                <div class="row">
                <div class="enable_chk col-xs-4">
					<label for="editor" class=" control-label text-left">  {!! Lang::get('core.Enable')!!}</label>
                    </div>
                    
					<div class="col-xs-8">
					@if(!empty($status))
					 <input type="checkbox" name="status"  checked='checked'/>
					@else
					 <input type="checkbox" name="status"  />
					@endif
					 </div> 
                     </div>
					 
				</div> 
				<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
                    
					<button type="submit" name="submit" class="btn btn-color" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('user-forum') }}' " class="btn btn-color"><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 	
		 {!! Form::close() !!}
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>-->
    </div>
  </div>
</div>

<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog add_forum">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{!! Lang::get('core.Edit_Forum')!!}</h4>
      </div>
      <div class="modal-body">
        {{---*/ $id='' /*---}}
		{{---*/ $title='' /*---}}
		{{---*/ $cat_id='' /*---}}
		{{---*/ $desc='' /*---}}
		{{---*/ $status='' /*---}}
		{!! Form::open(array('url'=>'user-forum/forumupdate/', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		 	
		 	<div class="form-group hidethis " style="display:none;">
				<label for="Id" class=" control-label col-md-4 text-left"> {{ Lang::get('core.id') }} </label>
				<div class="col-md-6">
				  <input type="hidden" class="forum_id" value="" name="id" />
				  {!! Form::hidden('user_id',$user_id,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
				 
				 </div> 
				 
			 </div> 

			 <div class="form-group">
             <div class="row">
             <div class="col-sm-4">
					<label for="Title" class=" control-label text-left">  {!! Lang::get('core.Title')!!}<span class="asterix"> * </span></label>
                    </div>
					<div class="col-sm-8">
					  <input type="text" class="title"  name="title" required />
					 </div> 
                     </div>
					 
			</div>
			 <div class="form-group">
             <div class="row">
          <div class="col-sm-4">
				<label for="Group / Level" class=" control-label text-left">  {!! Lang::get('core.Category Name')!!}<span class="asterix"> * </span></label>
                </div>
				<div class="col-sm-8" id="edit_cat">
				  <select name='cat_id' rows='5' id='cat_id' code='' 
					class=''  required  class="cat_id">
					<option value="0"> Select a Category </option>
					@foreach($cforum as $cat)
						<option value='{{$cat->forum_cat_id}}'>{{$cat->name}}</option>
					@endforeach
				</select> 
				 </div>
				 </div>
			  </div>
			   <div class="form-group">
               <div class="row">
               <div class="col-sm-4">
					<label for="editor" class=" control-label text-left">  {!! Lang::get('core.Description')!!}<span class="asterix"> * </span></label>
                    </div>
					<div class="col-sm-8">
					<div class="adjoined-bottom">
						<div class="grid-container">
							<div class="grid-width-50">
								<textarea id="editor" name="content"  class="content"></textarea>
							</div>
						</div>
					</div>
					  
					 </div> 
                     </div>
					 
				</div> 

				<div class="form-group">
                <div class="row">
                <div class="enable_chk col-sm-4 col-xs-4">
					<label for="editor" class=" control-label text-left">{!! Lang::get('core.Enable')!!} <span class="asterix">  </span></label>
                    </div>
					<div class="col-xs-8">
                    <input type="checkbox" name="status" class="estatus" checked />
					 </div> 
                     </div>
					 
				</div> 
				<div style="clear:both"></div>	
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="submit" class="btn btn-color" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					</div>	  
				  </div> 
		 	
		 {!! Form::close() !!}
      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>-->
    </div>
  </div>
</div>

<script>
$('input[type="checkbox"],input[type="radio"]').iCheck({
   checkboxClass: 'icheckbox_square-green',
   radioClass: 'iradio_square-green',
  })
  </script>
