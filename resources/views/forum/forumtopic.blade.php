<link rel="stylesheet" href="{{asset('assets/bsetec/css/style.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/bsetec/css/redactor.css') }}" />
<script type="text/javascript" src="{{ asset('assets/bsetec/js/redactor.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(
	function()
	{
		$('#redactor').redactor();
		$("#error").fadeOut(3000);
	}
);
$(document).ready(function(){
	$('body').addClass('forum-p');
    $(".editt").click(function(){
        var comment_id=$(this).attr('data-id');
        var content=$(this).attr('data-des');
        var forum_id=$(this).attr('data-forumid');
        $('.comment').val(comment_id);
        $('.content').val(content);
        $('.content').html(content);
       	$('.forum_id').val(forum_id);
    });

    $(".delete").click(function(){
		var res=window.confirm('{!! Lang::get("core.forum_comments_del") !!}')
		var comment_id=$(this).attr('data-id')
		if(res==true)
			window.location.href="comments/"+comment_id;
	});

	function tinyClean(value) {
		value = value.replace(/&nbsp;/ig, "");
		value = value.replace(/\s\s+/g, "");
		value = value.replace(/(<([^>]+)>)/ig,"");
		if(value == '<p><br></p>' || value == '<p> </p>' || value == '<p></p>') {
			value = '';
		}
		return value;
	}
	$('.answers').submit(function(){
		var value = $.trim(tinyClean($('#redactor').val()));
		if(value.length == 0)
		{
			$('#com_error').html("{!! Lang::get('core.comment_required')!!}");
			return false;
		}else{
			var url = $(this).attr('action');
			$.ajax({
				type: 'POST',
				url: url,
				data:'comments='+value+'&user_id='+user_id+'&forum_id='+forum_id,
				success : function(data) {	
					}
			 });	
		     }
		});
   });
</script>
     
 <div class="breadcrumb-c">
                  <div class="container">
                   <ul>
        <li><a href="{{ url('') }}">{{ Lang::get('core.home') }}</a></li>
        <li><a href="{!! url('user-forum') !!}">{{ Lang::get('core.Forum') }}</a></li>
		<li class="active"> <span>{!! $pageTitle !!} </span></li>
     </ul>		
                  </div>
                  </div>
                      
    <div id="error">
	    @if(Session::has('message'))
			{!! Session::get('message') !!}
		@endif                 
     </div>
	<section class="services editor_block" id="why">
	<div class="container">
	  <div class="row">
	  <div class="col-sm-9">
	  <h3 class="title">{!! Lang::get('core.Forum_Comments') !!}</h3>
	 @if(Auth::check())
     	@php  $user_id=Auth::user()->id; @endphp
     @else	
     	@php   $user_id=''; @endphp
     @endif
     	
	  @foreach($forum as $forum)
		 <h4 class="forum-topic">{{ $forum->forum_topic }}  </h4>
		 		@php $content=$forum->forum_content @endphp
		 <p class="content-b">{{ strip_tags($content) }}</p>
         <div class="created-block clearfix">
         <p class="create-b"><span>{!! Lang::get('core.Created_By') !!} : <a href="{{url('profile/'.$forum->username)}}"> {{$forum->username}}</a> </span> - <span><a href="{{url('topic/'.$forum->category_slug)}}">{{$forum->name}}</a> </span></p>
		</div>
	  @endforeach
	  		@php $i=0; $lists = $list;  @endphp
	  		@foreach($list as $list)
	  		<div class="avatar_img clearfix">
            <div class="row">
            <div class="col-xs-2">
           	<a href="{{url('profile/'.$list->username)}}">{!! SiteHelpers::customavatar($list->email,$list->user_id,'small','') !!}</a>
            </div>
            <div class="col-xs-10">
            <div class="usercomment-block">
            <span class="user-name"><a href="{{url('profile/'.$list->username)}}">{{$list->username}}</a></span>
            <span class="time-user">{!! SiteHelpers::changeFormat($list->created_at) !!}</span>
            <p class="desc-p">{{ strip_tags($list->comment)}} </p>
            <div class="desc-block clearfix">
	  			@if( Auth::check())
					@if(Auth::user()->id == $list->user_id )
					<div class="blog_editblocks">
				    <a title="{!! Lang::get('core.btn_edit') !!}" class="btn btn-info padding_width editt"  data-id="{{$list->comment_id}}" data-des="{{ strip_tags($list->comment)}}" data-forumid="{{$list->forum_id}}" data-toggle="modal" data-target="#myModal1">
				        <i class="fa fa-pencil-square-o"></i>
				    </a>
				     <a title="{!! Lang::get('core.btn_remove') !!}"  data-id="{{$list->comment_id}}" class="btn btn-danger padding_width delete" >
				        <i class="fa fa-trash-o"></i>
				    </a>
					</div>
                    @endif
				@endif
				</div>
              </div>
              </div>
            </div>
			</div>		
        @endforeach
	  	<div class="pull-right">
          @if(count($lists)>0){!! str_replace('/?', '?', $lists->render()) !!}  @endif
        </div>
        <div class="text_area-b clearfix">
	 	@if( Auth::check())
	 	{!! Form::open(array('url'=>'user-forum/insertt/', 'class'=>'form-horizontal answers','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		
		<div id="page" style="width:100%;" class="editor_page">
			<label>{!! Lang::get('core.YourAnswer')!!} :<span class="text-danger"> * </span></label>
			<textarea id="redactor" name="comments">
			</textarea>
		</div>
		@php ( $forum_id=$forum->forum_id )
		@php ( $slug = $forum->slug )
		{!! Form::hidden('slug', $slug,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
		{!! Form::hidden('user_id', $user_id,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
		{!! Form::hidden('forum_id', $forum_id,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
		<div id="com_error" class="text-danger"></div>
		<button type="submit" name="submit" class="btn btn-color" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
	 	
	 	{!! Form::close() !!}
		@endif
</div>
      </div>
        <div class="col-sm-3">
        <div class="categories_block">
		<h4> {!! Lang::get('core.Categories')!!}</h4>
		<ul>
		<li><a href="{{url('user-forum')}}"><i class="fa fa-angle-right icon_forum"></i>  {!! Lang::get('core.ALL')!!}</a></li>
		@foreach($category as $cat)
		<li><a href="{{url('topic/'.$cat->slug)}}"><i class="fa fa-angle-right icon_forum"> </i> {{ $cat->name }}</a></li>
		@endforeach
		</ul>
		</div>
			</div>
      
      </div>	
      </div>   
	</section>


<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog add_forum">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> {!! Lang::get('core.Edit_Comments')!!} </h4>
      </div>
      <div class="modal-body">
        @php ( $id='' )
		@php ( $title='' )
		@php ( $cat_id='')
		@php ( $desc='' )
		@php ( $status='' )
		{!! Form::open(array('url'=>'user-forum/comments', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		 	<div class="form-group hidethis " style="display:none;">
				<label for="Id" class=" control-label col-md-4 text-left"> Id </label>
				<div class="col-md-6">
				  <input type="hidden" class="comment" value="" name="id" />
				  <input type="hidden" class="forum_id" value="" name="forum_id" />
				  {!! Form::hidden('user_id',$user_id,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
				 
				 </div> 
			 </div> 
			   <div class="form-group">
               <div class="row">
               <div class="col-sm-4">
					<label for="editor" class=" control-label text-left">  {!! Lang::get('core.Description')!!} <span class="asterix"> * </span></label>
                    </div>
					<div class="col-sm-8">
					<div class="adjoined-bottom">
						<div class="grid-container">
							<div class="grid-width-50">
								<textarea id="editor" name="content"  class="content"></textarea>
							</div>
						</div>
					</div>
					 </div> 
                     </div>
				</div> 
				
				<div style="clear:both"></div>	
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="submit" class="btn btn-color" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					</div>	  
				 </div> 
		 	
		 {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>




