@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('invoice?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li class="alert alert-danger">{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	
		 {!! Form::open(array('url'=>'/invoice/save', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
			<fieldset><p><legend> {{ Lang::get('core.invoice') }}</legend></p>
							{!! Form::hidden('id', $row['id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							<div class="form-group  " >
							<label for="Transaction Id" class=" control-label col-md-4 text-left"> {{ Lang::get('core.transaction_id') }} <span class="text-danger">*</span></label>
							<div class="col-md-6">
							{!! Form::text('transaction_id', $row['transaction_id'],array('class'=>'form-control', 'placeholder'=>'', 'required'=> ''  )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Customer Name" class=" control-label col-md-4 text-left"> {{ Lang::get('core.customer_name') }} <span class="text-danger">*</span></label>
							<div class="col-md-6">
							{!! Form::text('customer_name', $row['customer_name'],array('class'=>'form-control', 'placeholder'=>'', 'required'=> ''  )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Email" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Email') }} <span class="text-danger">*</span></label>
							<div class="col-md-6">
							{!! Form::text('email', $row['email'],array('class'=>'form-control', 'placeholder'=>'', 'required'=> ''   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Course Title" class=" control-label col-md-4 text-left"> {{ Lang::get('core.course_title') }} <span class="text-danger">*</span></label>
							<div class="col-md-6">
							{!! Form::text('course_title', $row['course_title'],array('class'=>'form-control', 'placeholder'=>'', 'required'=> ''  )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Amount" class=" control-label col-md-4 text-left"> {{ Lang::get('core.amount') }} <span class="text-danger">*</span></label>
							<div class="col-md-6">
							{!! Form::text('amount', $row['amount'],array('class'=>'form-control', 'placeholder'=>'', 'required'=> ''  )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Status" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Status') }} <span class="text-danger">*</span></label>
							<div class="col-md-6">
							@php ( $statuss = array('completed', 'pending') )
							<select name="status" class="form-control" required>
							<option value="">{{ Lang::get('core.select')}}</option>
							@foreach($statuss as $status)
								<option value="{!! $status !!}" {!! $row['status']==$status ? 'selected' : '' !!}>
									{!! ucwords(str_replace('_', ' ', $status)) !!}
								</option>
							@endforeach	 
							</select>							
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Payment Method" class=" control-label col-md-4 text-left"> {{ Lang::get('core.payment_method') }} <span class="text-danger">*</span></label>
							<div class="col-md-6">
							@php (  $payment_methods = $method )
							<select name="payment_method" class="form-control" required>
							<option value="">{{ Lang::get('core.select')}}</option>
							@foreach($payment_methods as $payment_method)
								<option value="{!! $payment_method !!}" {!! $row['payment_method']==$payment_method ? 'selected' : '' !!}>
									{!! ucwords(str_replace('_', ' ', $payment_method)) !!}
								</option>
							@endforeach	 
							</select>
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Invoice Number" class=" control-label col-md-4 text-left"> {{ Lang::get('core.invoice_no') }} <span class="text-danger">*</span></label>
							<div class="col-md-6">
							{!! Form::text('invoice_number', $row['invoice_number'],array('class'=>'form-control', 'placeholder'=>'', 'required'=> ''   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Ordered On" class=" control-label col-md-4 text-left"> {{ Lang::get('core.ordered_on') }} <span class="text-danger">*</span></label>
							<div class="col-md-6">
							{!! Form::text('ordered_on', $row['ordered_on'],array('class'=>'form-control datetime', 'placeholder'=>'',  'required'=> '' )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> </fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8 btn-section">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('invoice?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		 
	});
	</script>		 
@stop