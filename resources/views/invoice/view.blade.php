@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('invoice?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('invoice?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('invoice/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
	
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.id') }}</td>
				<td>{{ $row->id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.transaction_id') }}</td>
				<td>{{ $row->transaction_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.customer_name') }}</td>
				<td>{{ $row->customer_name }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.Email') }}</td>
				<td>{{ $row->email }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.course_title') }}</td>
				<td>{{ $row->course_title }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.amount') }}</td>
				<td>{{ $row->amount }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.Status') }}</td>
				<td>{{ $row->status }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.payment_method') }}</td>
				<td>{{ $row->payment_method }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.invoice_no') }}</td>
				<td>{{ $row->invoice_number }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.ordered_on') }}</td>
				<td>{{ $row->ordered_on }} </td>

				</tr>
				
		</tbody>	
	</table>    
	
	</div>
</div>	

	</div>
</div>
	  
@stop