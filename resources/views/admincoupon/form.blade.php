@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('admincoupon?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	

		 {!! Form::open(array('url'=>'admincoupon/save/'.$row['id'], 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
			<fieldset><legend> {{ Lang::get('core.admin_coupon') }} </legend>
			{!! Form::hidden('user_id', $row['user_id'],array('class'=>'form-control')) !!}
							<div class="form-group  " >
							<label for="Coupon Type" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Coupon_Type') }} </label>
							<div class="col-md-6">
							{!! Form::select('coupon_type', array('' => 'Select Type', '1' => 'Percentage','2'=>'Fixed Price'), $row['coupon_type'] ,array('class'=>'form-controller','required'=>true,'id'=>'coupon_type')); !!}
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group" >
							<label for="Coupon Value" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Coupon_Value') }} </label>
							<div class="col-md-6">
							{!! Form::input('number','coupon_value', $row['coupon_value'],array('class'=>'form-control','required'=>true,'placeholder'=>'0','step'=>'any','min'=>'1','autocomplete'=>'off','id'=>'coupon_value')) !!}
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 	
							<div class="form-group  " >
							<label for="Coupon Desc" class=" control-label col-md-4 text-left"> {{ Lang::get('core.coupon_desc') }} </label>
							<div class="col-md-6">
							   <textarea name='coupon_desc' rows='2' id='coupon_desc' class='form-control'
							>@if(Input::old('coupon_desc')==''){{ $row['coupon_desc'] }}@else{{ Input::old('coupon_desc')}}@endif</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Coupon Start Date" class=" control-label col-md-4 text-left"> {{ Lang::get('core.coupon_Sdate') }} </label>
							<div class="col-md-6">
							@if($row['coupon_start_date'] !='0000-00-00')
							{!! Form::text('coupon_start_date', $row['coupon_start_date'],array('class'=>'form-control date', 'placeholder'=>'')) !!} 
							@else
							{!! Form::text('coupon_start_date',null,array('class'=>'form-control date', 'placeholder'=>'')) !!} 
							@endif
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Coupon End Date" class=" control-label col-md-4 text-left"> {{ Lang::get('core.coupon_Edate') }} </label>
							<div class="col-md-6">
							@if($row['coupon_end_date'] !='0000-00-00')
							{!! Form::text('coupon_end_date', $row['coupon_end_date'],array('class'=>'form-control date', 'placeholder'=>'')) !!} 
							@else
							{!! Form::text('coupon_end_date',null,array('class'=>'form-control date', 'placeholder'=>'')) !!} 
							@endif
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Coupon Status" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Coupon_Status') }} </label>
							   <div class="col-sm-8">
	             		    <label for="coupon_status">
	             		    {!! Form::radio('coupon_status',1,"",['id'=>'active']) !!} {{ Lang::get('core.fr_mactive') }}
	             		    </label>
	             		    <label for="coupon_status">
	             		    {!! Form::radio('coupon_status',0,"",['id'=>'inactive']) !!} {{ Lang::get('core.fr_minactive') }}
	             		    </label>
	                	    </div>
							</div> 					
							</fieldset>
		</div>

		
			<div style="clear:both"></div>	
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('admincoupon?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>	
<script type="text/javascript">
	$(document).ready(function() { 
		@if($row['coupon_status'] == 1)
    		$('#active').iCheck('check');
    	@else
    		$('#inactive').iCheck('check');
    	@endif
	});
	</script>		 
@stop