@extends('layouts.app')
@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
        <div class="page-title">
            <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
        </div>
        <ul class="breadcrumb">
            <li><a href="{{ URL::to('dashboard') }}"> Dashboard </a></li>
            <li class="active">{{ $pageTitle }}</li>
        </ul>
    </div>
    <div class="page-content-wrapper m-t">
        <div class="sbox animated fadeInRight">
            <div class="sbox-title"> <h5> <i class="fa fa-table"></i> Course Request Settings  </h5>
            </div>
            <div class="sbox-content">
                {!! Form::open(array('url'=>'customcourserequest/settingssave','method' => 'post','id'=>'customcoursesettings', 'class'=>'form-horizontal row','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
                    <div class="col-sm-12 animated fadeInRight ">
                        <fieldset>
                          <div class="form-group">
                            <label for="taccr" class=" control-label col-md-4"> Default Description Content </label>
                            <div class="col-md-8">
                                <textarea class="form-control input-sm" rows="10" name="taccr"><?php echo stripslashes($taccr); ?></textarea>
                            </div>
                        </fieldset>
                        <div style="clear:both"></div>
                        <div class="form-group">
                            <label for="save" class=" control-label col-md-4"> </label>
                            <div class="col-sm-8">
                                <button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@stop