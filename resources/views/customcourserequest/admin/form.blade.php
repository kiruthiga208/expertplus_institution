@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('customcourserequests?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	

		 {!! Form::open(array('url'=>'customcourserequests/save/'.$row['ccr_id'].'?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
			<fieldset><legend> Custom Course Requests</legend>
								
							<div class="form-group  " >
							<label for="Ccr Id" class=" control-label col-md-4 text-left"> Ccr Id </label>
							<div class="col-md-6">
							{!! Form::text('ccr_id', $row['ccr_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Tutor Id" class=" control-label col-md-4 text-left"> Tutor Id </label>
							<div class="col-md-6">
							{!! Form::text('tutor_id', $row['tutor_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Level" class=" control-label col-md-4 text-left"> Level </label>
							<div class="col-md-6">
							{!! Form::text('level', $row['level'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Category Id" class=" control-label col-md-4 text-left"> Category Id </label>
							<div class="col-md-6">
							{!! Form::text('category_id', $row['category_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Ccr Name" class=" control-label col-md-4 text-left"> Ccr Name </label>
							<div class="col-md-6">
							<textarea name='ccr_name' rows='2' id='ccr_name' class='form-control '  
			  >{{ $row['ccr_name'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Ccr Description" class=" control-label col-md-4 text-left"> Ccr Description </label>
							<div class="col-md-6">
							<textarea name='ccr_description' rows='2' id='ccr_description' class='form-control '  
			  >{{ $row['ccr_description'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Skills" class=" control-label col-md-4 text-left"> Skills </label>
							<div class="col-md-6">
							{!! Form::text('skills', $row['skills'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Payment Type" class=" control-label col-md-4 text-left"> Payment Type </label>
							<div class="col-md-6">
							{!! Form::text('payment_type', $row['payment_type'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Budget" class=" control-label col-md-4 text-left"> Budget </label>
							<div class="col-md-6">
							{!! Form::text('budget', $row['budget'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Ccr Status" class=" control-label col-md-4 text-left"> Ccr Status </label>
							<div class="col-md-6">
							{!! Form::text('ccr_status', $row['ccr_status'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Approved" class=" control-label col-md-4 text-left"> Approved </label>
							<div class="col-md-6">
							{!! Form::text('approved', $row['approved'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Tutor Count" class=" control-label col-md-4 text-left"> Tutor Count </label>
							<div class="col-md-6">
							{!! Form::text('tutor_count', $row['tutor_count'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="CreatedOn" class=" control-label col-md-4 text-left"> CreatedOn </label>
							<div class="col-md-6">
							
			{!! Form::text('createdOn', $row['createdOn'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="UpdatedOn" class=" control-label col-md-4 text-left"> UpdatedOn </label>
							<div class="col-md-6">
							
			{!! Form::text('updatedOn', $row['updatedOn'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> </fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('customcourserequests?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		 
	});
	</script>		 
@stop