@extends('layouts.app')
@section('content')
@php usort($tableGrid, "SiteHelpers::_sort") @endphp
<div class="page-content row">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-title">
			<h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
		</div>
		<ul class="breadcrumb">
			<li><a href="{{ URL::to('dashboard') }}"> Dashboard </a></li>
			<li class="active">{{ $pageTitle }}</li>
		</ul>
	</div>
	<div class="page-content-wrapper m-t">
		<div class="sbox animated fadeInRight">
			<div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5>
			</div>
			<div class="sbox-content">
				<div class="toolbar-line ">
					<a href="javascript://ajax" class="tips btn btn-sm btn-white updatestatus" title="Approve" data-type="1"><i class="fa fa-check"></i>&nbsp;{{ Lang::get('core.approve') }}</a>
					<a href="javascript://ajax" class="tips btn btn-sm btn-white updatestatus" title="Unapprove" data-type="2"><i class="fa fa-times"></i>&nbsp;{{ Lang::get('core.unapprove') }}</a>
					<a href="javascript://ajax" class="tips btn btn-sm btn-white updatestatus" title="{{ Lang::get('core.btn_remove') }}" data-type="0"><i class="fa fa-minus-circle "></i>&nbsp;{{ Lang::get('core.btn_remove') }}</a>
				</div>
				{!! Form::open(array('url'=>'customcourserequest/delete/', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )) !!}
				{!! Form::hidden('types', '',array('id'=>'actiontype'))  !!}
				<div class="table-responsive" style="min-height:300px;">
					<table class="table table-striped ">
						<thead>
							<tr>
								<th class="number"> No </th>
								<th> <input type="checkbox" class="checkall" /></th>
								@foreach ($tableGrid as $t)
								@if($t['view'] =='1')
								<th>{{ $t['label'] }}</th>
								@endif
								@endforeach
								<th width="70" >{{ Lang::get('core.btn_action') }}</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($rowData as $row)
							<tr>
								<td width="30"> {{ ++$i }} </td>
								<td width="50"><input type="checkbox" class="ids" name="id[]" value="{{ $row->ccr_id }}" />  </td>
								@foreach ($tableGrid as $field)
								@if($field['view'] =='1')
								@php ($fieldname =$field['field'])
								<td>
									@if($field['attribute']['image']['active'] =='1')
									{!! SiteHelpers::showUploadedFile($row->$fieldname,$field['attribute']['image']['path']) !!}
									@else
									@php ( $conn = (isset($field['conn']) ? $field['conn'] : array() ) )
									@if($field['field'] == 'entry_by')
									<?php
										$usrinfo = \bsetecHelpers::getuserinfobyid($row->entry_by);
										$uname = '';
										if(!empty($usrinfo)){
											$uname = $usrinfo->first_name.' '.$usrinfo->last_name;
										}
										$row->$fieldname = $uname;
										
									?>

									@elseif($field['field'] == 'tutor_id' && !empty($row->tutor_id))
									<?php
										$usrinfo = \bsetecHelpers::getuserinfobyid($row->tutor_id);
										$uname = '';
										if(!empty($usrinfo)){
											$uname = $usrinfo->first_name.' '.$usrinfo->last_name;
										}
										$row->$fieldname = $uname;
									?>

									@elseif($field['field'] == 'ccr_status')
									@if($row->$fieldname == '0')
									<?php $row->$fieldname = '<span class="label label-info">'.Lang::get('core.opened').'</span>'; ?>
									@elseif($row->$fieldname == '1' || $row->$fieldname == '2')
									<?php $row->$fieldname = '<span class="label label-warning">'.Lang::get('core.in_progress').'</span>'; ?>
									@elseif($row->$fieldname == '3')
									<?php $row->$fieldname = '<span class="label label-success">'.Lang::get('core.video_completed').'</span>'; ?>
									@elseif($row->$fieldname == '4')
									<?php $row->$fieldname= '<span class="label label-danger">'.Lang::get('core.Cancelled').'</span><a target="_blank" href="'.URL::to('customcourserequest/show/'.$row->ccr_id.'?return='.$return).'" class="tips btn btn-xs btn-white" title="'.Lang::get('core.dispute').'"><i class="fa  fa-comments-o "></i></a>'; ?>
									@elseif($row->$fieldname == '5')
									<?php $row->$fieldname = '<span class="label label-success">'.Lang::get('core.dispute').' '.Lang::get('core.video_completed').'</span>'; ?>
									@endif

									@elseif($field['field'] == 'approved')
									@if($row->$fieldname == '0')
									<?php $row->$fieldname = '<span class="label label-warning">'.Lang::get('core.waiting_for_approval').'</span>'; ?>
									@elseif($row->$fieldname == '1')
									<?php $row->$fieldname = '<span class="label label-success">'.Lang::get('core.approved').'</span>'; ?>
									@else
									<?php $row->$fieldname = '<span class="label label-danger">'.Lang::get('core.unapproved').'</span>'; ?>
									@endif

									@elseif($field['field'] == 'category_id')
									<?php $row->$fieldname = bsetecHelpers::siteCategories($row->$fieldname)->name; ?>

									@elseif($field['field'] == 'ccr_description')
									<?php $row->$fieldname = substr($row->ccr_description,0,50); ?>

									@endif
									{!! SiteHelpers::gridDisplay($row->$fieldname,$field['field'],$conn) !!}
									@endif
								</td>
								@endif
								@endforeach
								<td>
									@if($access['is_detail'] ==1)
									<a href="{{ URL::to('customcourserequest/show/'.$row->ccr_id.'?return='.$return)}}" target="_blank" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<input type="hidden" name="md" value="" />
				</div>
				{!! Form::close() !!}
				@include('footer')
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$(document).on('click','.updatestatus',function(){
		var status = $(this).data('type');
		$('#actiontype').val(status);
		if($("input[name='id[]']:checked").length==0){
			alert('Please select atleast one checkbox!');
		}else if($("input[name='id[]']:checked").length>0){
			if(confirm('Are you sure?'))
			{
				$('#bsetecTable').submit();// do the rest here
			}
		}else{
			alert('{{ Lang::get('core.select_checkbox') }}');
		}
	});
});
</script>
@stop