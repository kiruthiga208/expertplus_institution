@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('customcourserequests?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('customcourserequests?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('customcourserequests/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
	
				<tr>
				<td width='30%' class='label-view text-right'>Ccr Id</td>
				<td>{{ $row->ccr_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Entry By</td>
				<td>{{ $row->entry_by }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Tutor Id</td>
				<td>{{ $row->tutor_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Level</td>
				<td>{{ $row->level }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Category Id</td>
				<td>{{ $row->category_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Ccr Name</td>
				<td>{{ $row->ccr_name }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Ccr Description</td>
				<td>{{ $row->ccr_description }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Skills</td>
				<td>{{ $row->skills }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Payment Type</td>
				<td>{{ $row->payment_type }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Budget</td>
				<td>{{ $row->budget }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Ccr Status</td>
				<td>{{ $row->ccr_status }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Approved</td>
				<td>{{ $row->approved }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Tutor Count</td>
				<td>{{ $row->tutor_count }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>CreatedOn</td>
				<td>{{ $row->createdOn }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>UpdatedOn</td>
				<td>{{ $row->updatedOn }} </td>

				</tr>
				
		</tbody>	
	</table>    
	
	</div>
</div>	

	</div>
</div>
	  
@stop