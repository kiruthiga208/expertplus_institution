<div class="sbox-content">
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-1 no-padding">
                    @php  $usrinfo = \bsetecHelpers::getuserinfobyid($row->entry_by); @endphp
                    <a href="{!! \URL::to('profile/'.$usrinfo->username) !!}">
                        <center>
                            {!! \SiteHelpers::customavatar($usrinfo->email,$row->entry_by) !!}
                            <h4 class="m-t-sm small">{!! $usrinfo->first_name.' '.$usrinfo->last_name !!}</h4>
                            <h3 class="text-center">@if(\Cache::has('user-is-online-' . $row->entry_by)) <span class="text-info"><i class="fa fa-check-circle"></i> Online</span> @elseif(\Cache::has('user-is-away-' . $row->entry_by)) <span class="text-warning"><i class="fa fa-clock-o"></i> Away</span> @else <i class="fa fa-dot-circle-o"></i> Offline @endif</h3>
                        </center>
                    </a>
                </div>
                <div class="col-sm-8">
                    <a href="{{ URL::to('customcourserequest/show/'.$row->ccr_id.'?return='.$return) }}">
                        <h3 class="inline">{!! stripslashes($row->ccr_name) !!}</h3>
                    </a>
                    <div>{!! stripslashes($row->ccr_description) !!}</div>
                    <div class="row m-t-sm">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h4>{!! Lang::get('core.Type_of_Assistance') !!}: <em class="text-warning">{!! ucfirst($row->type) !!}</em></h4>
                                </div>
                                <div class="col-sm-6">
                                    <?php
                                        $skillset = '';
                                        if(!empty($row->skills)){
                                            $skillset = implode(', ',json_decode($row->skills,true));
                                        }
                                     ?>
                                    <h4>{!! Lang::get('core.Skills_Required') !!}: <em class="text-warning" style="text-transform: capitalize;">{!! $skillset !!}</em></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 text-right">
                    <h3>
                        <span>{!! $currency !!} <strong>{!! $row->budget !!}</strong></span>
                        <span>{!! Lang::get('core.horz_sep')!!}</span>
                        <span>{!! ucfirst($row->payment_type) !!}</span>
                    </h3>

                    @if(!isset($row->useraccess) || $row->useraccess)
                        <div>
                            @if($row->approved ==0)
                            <span class="text-warning"><strong>{!! Lang::get('core.Approval') !!}</strong></span>
                            @elseif($row->approved ==1)
                            <span class="text-success"><strong>{!! Lang::get('core.approved') !!}</strong></span>
                            @elseif($row->approved ==2)
                            <span class="text-danger"><strong>{!! Lang::get('core.unapproved') !!}</strong></span>
                            @endif
                        </div>
                        @if($row->ccr_status ==0)
                            <div class="m-t-sm">
                                @if($access['is_edit'] ==1)
                                <a href="{{ URL::to('customcourserequest/update?id='.$row->ccr_id) }}" data-id="{!! $row->ccr_id !!}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-pencil "></i></a>
                                @endif
                                @if($access['is_remove'] ==1)
                                <a href="{{ URL::to('customcourserequest/delete?id='.$row->ccr_id) }}" class="tips btn btn-xs btn-white remove_ccr" title="{{ Lang::get('core.btn_remove') }}"><i class="fa fa-trash-o "></i></a>
                                @endif
                            </div>
                        @elseif($row->ccr_status ==1 || $row->ccr_status ==2)
                            <h3 class="text-warning"><i class="fa fa-spinner"></i> {!! Lang::get('core.ongoing') !!}</h3>
                        @elseif($row->ccr_status ==3)
                            <h3 class="text-info"><i class="fa fa-check-circle"></i> {!! Lang::get('core.Completed') !!}</h3>
                        @elseif($row->ccr_status ==4 || $row->ccr_status ==5)
                            <h4 class="text-danger">{!! Lang::get('core.Cancelled') !!}</h4>
                        @endif
                    @else
                        @if($applied && $row->ccr_status ==0)
                            <h4 class="text-info">{!! Lang::get('core.applied_success') !!}</h4>
                            <a href="javascript:void(0)" data-aid="{!! $applied_id !!}" data-uid="{!! $row->entry_by !!}" class="btn btn-color tips messageBoard m-b-sm" title="{!! Lang::get('core.message_ccr')!!}">{!! Lang::get('core.message')!!} ({!! $applied_msg_count or '' !!})</a>
                        @elseif($row->ccr_status ==0)
                            @if(\Session::get('gid') != "1")
                            <a @if(isset($loggedid) && $loggedid) href="javascript:void(0)" class="btn btn-color applyccr" @else href="{{ URL::to('user/login') }}" class="btn btn-color" @endif>{!! Lang::get('core.applyccr')!!}</a>
                            @endif
                        @elseif($row->ccr_status ==1)
                            <h4 class="text-info">{!! Lang::get('core.awarded_requests') !!}</h4>
                        @elseif($row->ccr_status ==3)
                            <h3 class="text-info"><i class="fa fa-check-circle"></i> {!! Lang::get('core.Completed') !!}</h3>
                        @elseif(($row->ccr_status ==4 || $row->ccr_status ==5) && $loggedid == $row->tutor_id)
                            <h4 class="text-danger">{!! Lang::get('core.Cancelled') !!}</h4>
                        @endif
                    @endif

                    @if($row->ccr_status ==1 && isset($loggedid) && $loggedid && (\Session::get('uid') == $row->entry_by || \Session::get('uid') == $row->tutor_id))
                    <div class="m-t-sm">

                        @if((!isset($row->useraccess) || $row->useraccess) && $row->type == 'online' && $show_meeting)
                            <a href="{{ URL::to('customcourserequest/start/'.$row->ccr_id) }}" class="btn btn-color tips start_ccr m-b-sm" title="{!! Lang::get('core.start_ccr')!!}">{!! Lang::get('core.join_meeting')!!}</a>
                        @elseif(\Session::get('uid') == $row->tutor_id)
                            @if(isset($application_id) && !empty($application_id))
                                <a href="javascript:void(0)" data-aid="{!! $application_id !!}" data-uid="{!! $row->entry_by !!}" class="btn btn-color tips messageBoard m-b-sm" title="{!! Lang::get('core.message_ccr')!!}">{!! Lang::get('core.message')!!}</a>
                            @endif
                            @if($row->type == 'online' && $show_meeting)
                                <a href="{{ URL::to('customcourserequest/start/'.$row->ccr_id) }}" class="btn btn-color tips start_ccr m-b-sm" title="{!! Lang::get('core.start_ccr')!!}">{!! Lang::get('core.join_meeting')!!}</a>
                            @endif
                        @endif
                    </div>
                    @endif

                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-1 no-padding"></div>
                <div class="col-sm-11">
                    <div class="row">

                        <div class="form-group">
                            <div class="col-sm-6">
                                <strong>{!! bsetecHelpers::siteCategories($row->category_id)->name !!} 
                                @if($row->sub_cat_id!=0)
                                / {!! bsetecHelpers::sitesubcategories($row->category_id,$row->sub_cat_id)->sub_name !!}
                                @endif</strong>
                                <span>{!! Lang::get('core.vert_sep')!!}</span>
                                <strong>{!! ucfirst($row->level) !!}</strong>
                                <span>{!! Lang::get('core.vert_sep')!!}</span>
                                <h5 class="inline"><em>{!! bsetecHelpers::timeago(strtotime($row->createdOn)) !!}</em></h5>
                            </div>
                            @if($row->ccr_status !=0 && !empty($row->tutor_id))
                                <div class="col-sm-6">
                                    @php $usrinfo = \bsetecHelpers::getuserinfobyid($row->tutor_id); @endphp
                                    <h4 class="text-right">{!! Lang::get('core.this_ccr_awarded') !!} @if(isset($loggedid) && $loggedid == $row->tutor_id) {!! Lang::get('core.you')!!} @else <a href="{!! \URL::to('profile/'.$usrinfo->username) !!}" class="text-info">{!! $usrinfo->first_name.' '.$usrinfo->last_name !!}</a> @endif</h4>
                                </div>
                            @endif
                        </div>

                        @if(($row->ccr_status !=0) && !empty($row->tutor_id) && isset($loggedid) && $loggedid == $row->tutor_id)
                            <div class="form-group m-b-none">
                                <div class="row">
                                    <div class="col-md-12 show-grid">
                                        <div class="col-md-12">
                                            <h3>{!! Lang::get('core.ccrprice') !!} : {!! $currency !!} {!! $row->final_price !!}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if(($row->ccr_status !=0) && !empty($row->tutor_id) && isset($loggedid) && $loggedid == $row->entry_by && (!$enough_credits))
                            <div class="form-group m-b-none">
                                <div class="row">
                                    <div class="col-md-12 show-grid">
                                        <div class="col-md-12">
                                            <h3 class="text-danger"><i class="fa fa-warning"></i> {!! Lang::get('core.credits_failure_you') !!} </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @elseif(($row->ccr_status !=0) && !empty($row->tutor_id) && isset($loggedid) && $loggedid == $row->tutor_id && (!$enough_credits))
                            <div class="form-group m-b-none">
                                <div class="row">
                                    <div class="col-md-12 show-grid">
                                        <div class="col-md-12">
                                            <h3 class="text-info"><i class="fa fa-info-circle"></i> {!! Lang::get('core.credits_failure_student') !!} </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if(!empty($row->file_ids))
                            @php ( $file_list = json_decode($row->file_ids) )
                             <div class="form-group show-grid">
                                <div class="col-md-12">
                                @foreach($file_list as $file_id)
                                    @php $file = $model->getFiledata($file_id); 
                                    $resourceid = \SiteHelpers::encryptID($file_id);@endphp
                                    <div class="file_id_{!! $file_id !!}">
                                        <div>
                                            @if(!empty($file))
                                            {!! $file->file_title !!}
                                            @endif
                                        <span style="cursor:pointer;" class="arrow"><a href="{!! URL::to('customcourserequest/downloadresource/'.$resourceid) !!}"><i class="fa fa-download"></i></a></span>
                                        </div>
                                    </div>
                                @endforeach
                                </div>
                            </div>
                          @php  $file_data = implode(",",$file_list);@endphp
                            {!! Form::hidden('file_ids', $file_data,array('id'=>'file_ids')) !!}
                        @else
                            {!! Form::hidden('file_ids', '',array('id'=>'file_ids')) !!}
                        @endif

                        @if(!empty($shared_course) && ($loggedid == $row->tutor_id))
                            <div class="show-grid">
                                <div class="col-md-12">
                                    <h3>{!! Lang::get('core.shared_course')!!}: <a href="{!! $shared_course['link'] !!}">{!! $shared_course['title'] !!}</a>
                                        @if($row->ccr_status ==3 && 1==2)
                                            <div class="arrow">
                                                {!! Lang::get('core.Completed')!!}
                                            </div>
                                        @endif
                                    </h3>
                                </div>
                            </div>
                        @endif

                        @if($row->type == 'offline' && ($row->ccr_status ==1 || $row->ccr_status ==2) && !empty($row->tutor_id) && isset($loggedid) && $loggedid == $row->tutor_id)
                            <div class="m-b-md" @if(empty($shared_course)) style="display: none" @endif>
                                 <a class="btn btn-color m-t-sm" id="change_course" href="javascript:void(0);">{!! Lang::get('core.Change_Course')!!}</a>
                                 <a class="btn btn-color m-t-sm" style="display: none" id="change_course_cancel" href="javascript:void(0);">{!! Lang::get('core.cancel')!!}</a>
                            </div>
                            <div @if(!empty($shared_course)) style="display: none" @endif  id="create_course_div">
                            <div class="form-group show-grid">
                                <div class="col-md-12">
                                <h3>{!! Lang::get('core.create_and_share_course')!!}</h3>
                                <a class="btn btn-color" id="createcourseid" href="javascript:void(0);">{!! Lang::get('core.Create_Course')!!}</a>
                                </div>
                            </div>
                            {{---*/ $courses_list = $user_model->getcourse($row->tutor_id); /*---}}
                            @if(!empty($courses_list))
                            <div class="form-group show-grid">
                                <div class="col-md-12">
                                    <h2 class="text-center m-t-md"> OR </h2>
                                </div>
                                <?php echo Form::open(array('url' => 'customcourserequest/courseshare', 'method' => 'post','id'=>'share_courseform')); ?>
                                <input name="user_id" id="user_id" type="hidden" value="{!! $row->entry_by !!}">
                                <input name="ccr_id" id="ccr_id" type="hidden" value="{!! $row->ccr_id !!}">
                                <div class="col-md-12">
                                    <h3> {!! Lang::get('core.course_select')!!} </h3>
                                    <select name="course_id" id="course_id" class="form-control required" required>
                                        <option value="">{!! Lang::get('core.select_course')!!}</option>
                                        @foreach($courses_list as $courses)
                                        <option value="{!!  $courses->course_id  !!}">{!!  $courses->course_title  !!}</option>
                                        @endforeach
                                    </select>
                                    <br>
                                    <button type="submit" id="share_course" class="btn btn-color">{!! Lang::get('core.Share_Course')!!}</button>
                                </div>
                                <?php  echo Form::close(); ?>
                            </div>
                            @endif
                            </div>
                        @endif

                        @if(($row->ccr_status ==1 || $row->ccr_status ==2) && !empty($row->tutor_id) && isset($loggedid) && $loggedid == $row->entry_by)
                        <div class="row">
                            <div class="col-md-12">
                                <span>
                                    <a class="btn btn-color tips m-t-sm" id="completed_ccr" href="{{ URL::to('customcourserequest/complete/'.$row->ccr_id) }}" title="{!! Lang::get('core.completed_ccr')!!}">{!! Lang::get('core.complete_ccr')!!}</a>
                                </span>
                                <span>
                                    <a class="btn btn-color tips cancel_ccr m-t-sm" href="javascript:void(0);" title="{!! Lang::get('core.cancel_ccr')!!}" data-id="{!! $row->ccr_id !!}">{!! Lang::get('core.cancel')!!}</a>
                                </span>
                            </div>
                        </div>
                        @endif

                        @if(($row->ccr_status ==1 || $row->ccr_status ==2) && !empty($row->tutor_id) && isset($loggedid) && $loggedid == $row->tutor_id)
                        <div class="row">
                            <div class="col-md-12">
                                <span>
                                    <a class="btn btn-color tips m-t-sm" id="request_completion_ccr" href="{{ URL::to('customcourserequest/completerequest/'.$row->ccr_id) }}" title="{!! Lang::get('core.request_completed_ccr')!!}">{!! Lang::get('core.request_completed')!!}</a>
                                </span>
                                <span>
                                    <a class="btn btn-color tips cancel_ccr m-t-sm" href="javascript:void(0);" title="{!! Lang::get('core.cancel_ccr')!!}" data-id="{!! $row->ccr_id !!}">{!! Lang::get('core.cancel')!!}</a>
                                </span>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
