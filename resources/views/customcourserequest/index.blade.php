@extends('layouts.frontend')
@section('content')
<style>
.btn.btn-xxl{
    border-radius: 0;
    font-family: open sans;
    font-size: 18px;
	text-transform: uppercase;
    font-weight: 600;
    height: 50px;
}
.search-block{
	padding:0;
	margin-bottom:20px;
}
.search-block .course_searching{
	width:100%;
}
.course_searching .more_one_portiion{
	width:90%;
}
.course_searching .btn{
	width:6%;
	text-align:center;
	padding:0px;
}
.ccr_titles{
	margin:0 !important;
}
@media (max-width: 768px) {
	.course_searching .btn{
		width:15%;
	}
	.course_searching .more_one_portiion{
		width:85%;
	}
	.TeachingCourse_create {
	    float: none !important;
	    width: 100% !important;
	}
}
</style>
@if (defined('CNF_CURRENCY'))
@php  $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) @endphp
@else
@php $currency = 'USD'; @endphp
@endif
<div class="page-content row">
	<div class="reg_form new_reg_form mycourse_block">
		<div id="mainwrapper">
			<div class="col-sm-12">
				<div class="head_block clearfix">
					 <h2 class="title col-sm-9">{!! Lang::get('core.custom_course_request'); !!}</h2>
					 @if(\Session::get('ut') != "tutor")
					 <div class="TeachingCourse_create col-sm-3">
						<p class="discover_courses"><a class="btn course_create" href="{{ URL::to('customcourserequest/update') }}">{!! Lang::get('core.create_course_request'); !!}</a></p>
					</div>
					@endif
				</div>
				<div class="mycourse-p">
					<div class="row">
						<div class="col-sm-9">
							{!! Lang::get('core.ccr_text'); !!}
						</div>
						<div class="col-sm-3">
							<a class="btn btn-color arrow" href="{{ URL::to('customcourserequest/payments') }}"><strong>{!! Lang::get('core.payment_history'); !!}</strong></a>
						</div>
					</div>
				</div>
				<div class="tab-block-mycourse clearfix ccr_titles">
					<ul class="nav nav-tabs usernavbar price_align post">
						<li class="@if($type == '1'): current_page_item active @endif !!}"><a href="{!! e(url('customcourserequest/'.$view.'?t=1'.'&qt='.$qtype)); !!}" > {!! Lang::get('core.upcoming'); !!} </a></li>
						<li class="@if($type == '2'): current_page_item active @endif !!}"><a href="{!! e(url('customcourserequest/'.$view.'?t=2'.'&qt='.$qtype)); !!}" > {!! Lang::get('core.ongoing'); !!} </a></li>
						<li class="@if($type == '3'): current_page_item active @endif !!}"><a href="{!! e(url('customcourserequest/'.$view.'?t=3'.'&qt='.$qtype)); !!}" > {!! Lang::get('core.past'); !!} </a></li>
						<li class="@if($type == '4'): current_page_item active @endif !!}"><a href="{!! e(url('customcourserequest/'.$view.'?t=4'.'&qt='.$qtype)); !!}" > {!! Lang::get('core.all'); !!} </a></li>
						<li class="@if($type == '5'): current_page_item active @endif !!}"><a href="{!! e(url('customcourserequest/'.$view.'?t=5'.'&qt='.$qtype)); !!}" > Custom Course Requests  </a></li>
					</ul>
				</div>
				<div class="arrow m-b-sm m-r-sm @if(!empty($loggedid)) m-t-n-md @else m-t-sm @endif">
					@if(\Session::get('ut') != "tutor")
					<!--a class="tips" title="{!! e(Lang::get('core.my_requests_note')); !!}" href="{!! e(url('customcourserequest/'.$view.'?t='.$type.'&qt=1')); !!}">@if($qtype == '1') <strong>{!! e(Lang::get('core.my_requests')); !!}</strong> @else {!! e(Lang::get('core.my_requests')); !!} @endif </a>
					<span>{!! Lang::get('core.horz_sep'); !!}</span-->
					@elseif(\Session::get('ut') == "tutor")
					<!--a class="tips" title="{!! e(Lang::get('core.applied_requests_note')); !!}" href="{!! e(url('customcourserequest/'.$view.'?t='.$type.'&qt=2')); !!}">@if($qtype == '2') <strong>{!! e(Lang::get('core.applied_requests')); !!}</strong> @else {!! e(Lang::get('core.applied_requests')); !!} @endif</a>
					<span>{!! Lang::get('core.horz_sep'); !!}</span>
					<a class="tips" title="{!! e(Lang::get('core.awarded_requests_note')); !!}" href="{!! e(url('customcourserequest/'.$view.'?t='.$type.'&qt=3')); !!}">@if($qtype == '3') <strong>{!! e(Lang::get('core.awarded_requests')); !!}</strong> @else {!! e(Lang::get('core.awarded_requests')); !!} @endif</a-->
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

@include('customcourserequest/result_block')
@include('customcourserequest/scripts')
@stop