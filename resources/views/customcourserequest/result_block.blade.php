<div class="page-content m-l-xl m-r-xl">
    <div class="row">
        @if(!empty($rowData))
            @foreach($rowData as $row)
                @include('customcourserequest/request_block')
            @endforeach
        @else
            <h3><center>{!! Lang::get('core.noccr') !!}</center></h3>
        @endif
    </div>
    <div class="table-footer">
        <div class="row">
            <div class="col-sm-12">
                <div class="arrow m-t-md">
                    {!! $pagination->appends(['ty'=>1,'t'=>$type,'qt'=>$qtype])->render(); !!}
                </div>
            </div>
        </div>
    </div>
</div>