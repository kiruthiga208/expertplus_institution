@extends('layouts.mailtemplate')
@section('content')

<h2>Hello {!! $authorname !!} , </h2>
<p>A tutor has applied for your {!! $item !!}</p>
<p>
	{!! $item !!} Title : {!! $title !!}
</p>
click <a href="{!! $link !!}"> here </a> to view your {!! $item !!}.</p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!}

@stop
