@extends('layouts.mailtemplate')
@section('content')

<h2>Hello {!! $authorname !!} , </h2>
<p>You have got a new {!! $type !!} for a {!! $item !!}</p>
<p>
	{!! $type !!} : {!! $title !!}
</p>
click <a href="{!! $link !!}"> here </a> to view the {!! $type !!} and the {!! $item !!}.</p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!}

@stop
