@extends('layouts.mailtemplate')
@section('content')

<h2>Hello {!! $authorname !!}, </h2>
<p>Here is the latest {!! $item !!} matching your skills</p>
<p>
	{!! $item !!} Title : {!! $title !!}
</p>
<p>Apply quickly to get this offer!! <br>
Click <a href="{!! $link !!}"> here </a> to view this {!! $item !!}.</p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!}

@stop
