@extends('layouts.mailtemplate')
@section('content')
<h2>Hello {!! $authorname !!} , </h2>
<p>Sorry!!! Your {!! $item !!} has not been unapproved by Admin...</p>
<p>{!! $item !!} Details<br>
	{!! $item !!} Title : {!! $title !!} <br>
	click <a href="{!! $link !!}"> here </a> to view your {!! $item !!}.
</p>
<p> Try to post another {!! $item !!}!!  Congrats in Advance!!!</p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!}

@stop