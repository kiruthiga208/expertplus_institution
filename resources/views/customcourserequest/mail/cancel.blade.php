@extends('layouts.mailtemplate')
@section('content')

<h2>Hello {!! $authorname !!} , </h2>
<p>A {!! $item !!} you have {!! $type !!} has been cancelled</p>
<p>
	{!! $item !!} Title : {!! $title !!}
</p>
click <a href="{!! $link !!}"> here </a> to view the {!! $item !!} and start dispute.</p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!}

@stop
