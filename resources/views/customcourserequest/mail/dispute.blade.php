@extends('layouts.mailtemplate')
@section('content')

<h2>Hello {!! $authorname !!} , </h2>
<p>The Dispute over a {!! $item !!} you have {!! $type !!} has been closed</p>
<p>
    {!! $item !!} Title : {!! $title !!}
</p>
<p>
	Refund Accepted : {!! $currency.' '.$price !!} @if($payment_type == 'hour') (Hourly Rate) @endif
</p>
<p>
    Revised Total : {!! $currency.' '.$revised !!}
</p>
<p>
    Total Refund : {!! $currency.' '.$return !!}
</p>
click <a href="{!! $link !!}"> here </a> to view the {!! $item !!}.</p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!}

@stop
