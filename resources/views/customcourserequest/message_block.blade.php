<div class="row m-b-md">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-2 no-padding">
                <a href="{!! $user_link; !!}" class="col-sm-12">
                    <center>
                        {!! $imgpath ; !!}
                    </center>
                </a>
            </div>
            <div class="col-sm-10" style="background-color: #f6f6f6;">
                <a href="{!! $user_link; !!}">
                    <strong @if($is_admin) class="text-warning" @else class="text-success" @endif>{!! $display_name ; !!} @if($is_admin)(Administrator)@endif</strong>
                </a>
                <p>{!! $message ; !!}</p>
            </div>
        </div>
    </div>
</div>