@extends('layouts.frontend')
@section('content')
<style>
.request_block h4 {
    background: #158d8e;
    color: #fff;
    font-size: 16px;
    padding: 14px 20px;
    margin: 20px 0 20px;
}
.dispute_block h4 {
    background: #f8ac59;
    color: #fff;
    font-size: 16px;
    padding: 14px 20px;
    margin: 20px 0 20px;
}
</style>
@if (defined('CNF_CURRENCY'))
@php  $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) @endphp
@else
@php $currency = 'USD'; @endphp
@endif
<div class="page-header">
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('') }}">{{ Lang::get('core.home') }}</a></li>
        <li><a href="{{ URL::to('customcourserequest/view') }}">{!! Lang::get('core.custom_course_request') !!}</a></li>
        <li class="active"> </li>
      </ul>
</div> 
<div class="page-content row">
    <div class="reg_form new_reg_form mycourse_block">
        <div id="mainwrapper">
            <div class="col-sm-12">
                <div class="head_block clearfix">
                     <h2 class="title col-sm-9">{!! Lang::get('core.custom_course_request'); !!}</h2>
                     <!-- @if(\Session::get('ut') != "tutor") -->
                     <div class="TeachingCourse_create col-sm-3">
                        <p class="discover_courses"><a class="btn course_create" href="{{ URL::to('customcourserequest/update') }}">{!! Lang::get('core.create_course_request'); !!}</a></p>
                    </div>
                    <!-- @endif -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-content m-l-xl m-r-xl">
    <div class="row animated fadeInRight">
        @if($row->approved ==1 || $row->useraccess || \Session::get('gid') == "1")
            @include('customcourserequest/request_block')
            @include('customcourserequest/scripts')
        @else
            <h4><center>{!! Lang::get('core.ccr_no') !!}</center></h4>
        @endif
    </div>

    @if(($row->ccr_status ==4 && !empty($dispute)) && !empty($row->tutor_id) && isset($loggedid) && ($loggedid == $row->entry_by || $loggedid == $row->tutor_id || \Session::get('gid') == 1))
    <div class="row">
        <div class="dispute_block">
            <h4>{!! Lang::get('core.dispute')!!}</h4>
        </div>

        <div class="sbox-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-2">
                            <h4> Cancelled By: <span class="text-info">{!! ucfirst($dispute->cancelled_by) !!}</span></h4>
                        </div>
                        <div class="col-md-7">
                            <h4> Reason: {!! $dispute->reason !!}</h4>
                        </div>
                        <div class="col-md-3">
                            @if(\Session::get('gid') == 1)
                            <a href="javascript:void(0)" class="btn btn-color tips disputeFinish arrow" title="{!! Lang::get('core.finish_dispute')!!}">{!! Lang::get('core.finish_dispute')!!}</a>
                            @elseif($loggedid == $row->tutor_id)
                            <a href="javascript:void(0)" class="btn btn-color tips disputeFinish arrow" title="{!! Lang::get('core.finish_dispute_tutor')!!}">{!! Lang::get('core.finish_dispute')!!}</a>
                            @endif
                            <a href="javascript:void(0)"  data-aid="{!! $applications->application_id !!}" data-uid="{!! $applications->user_id !!}" class="btn btn-color tips messageBoard arrow m-r-sm" title="{!! Lang::get('core.Discussions')!!}">{!! Lang::get('core.Discussions')!!}</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 show-grid">
                            <div class="col-md-4">
                                <h3>Refund Proposed by Student: {!! $currency.' '.$dispute->price_proposed_by_student !!} @if($loggedid == $row->entry_by) <a href="javascript:void(0)"><i class="fa fa-edit proposeprice"></i></a> @endif</h3>
                            </div>
                            <div class="col-md-4">
                                <h3>Refund Proposed by Tutor: {!! $currency.' '.$dispute->price_proposed_by_tutor !!} @if($loggedid == $row->tutor_id) <a href="javascript:void(0)"><i class="fa fa-edit proposeprice"></i></a> @endif</h3>
                            </div>
                            <div class="col-md-4">
                                <h3>Refund Proposed by Admin: {!! $currency.' '.$dispute->price_proposed_by_admin !!} @if(\Session::get('gid') == 1) <a href="javascript:void(0)"><i class="fa fa-edit proposeprice"></i></a> @endif</h3>
                            </div>
                        </div>
                        <!--div class="col-md-12">
                        <strong><i>{!! Lang::get('core.Note')!!}: {!! Lang::get('core.refund_note')!!}</i></strong>
                        </div-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    @elseif(($row->ccr_status ==5 && !empty($dispute)) && !empty($row->tutor_id) && isset($loggedid) && ($loggedid == $row->entry_by || $loggedid == $row->tutor_id || \Session::get('gid') == 1))
    <div class="row">
        <div class="dispute_block">
            <h4>{!! Lang::get('core.dispute')!!}</h4>
        </div>

        <div class="sbox-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-2">
                            <h4> Cancelled By: <span class="text-info">{!! ucfirst($dispute->cancelled_by) !!}</span></h4>
                        </div>
                        <div class="col-md-7">
                            <h4> Reason: {!! $dispute->reason !!}</h4>
                        </div>
                        <div class="col-md-3">
                            <h2 class="text-right text-warning"> {!! Lang::get('core.closed')!!}</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 show-grid">
                            <div class="col-md-6">
                                <h3>Refund Proposed: {!! $currency.' '.$amount_refund !!} </h3>
                            </div>
                            <div class="col-md-6">
                                <h3>Revised Final Total: {!! $currency.' '.$revised_total !!}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if(($row->useraccess || \Session::get('gid') == 1) && $row->ccr_status != '0')
    <div class="row">
        <div class="request_block">
            <h4>{!! Lang::get('core.awarded_tutors')!!}</h4>
        </div>

        <div class="sbox-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-1 no-padding">
                        <a href="{!! $applications->user_link !!}">
                            <center>
                                {!! $applications->imgpath !!}
                            </center>
                        </a>
                    </div>
                    <div class="col-sm-11">
                        <div class="row">
                            <div class="col-sm-10">
                                <a href="{!! $applications->user_link !!}">
                                    <h4 class="inline">{!! $applications->display_name !!}</h4>
                                </a>
                                <span>{!! Lang::get('core.vert_sep')!!}</span>
                                <h5 class="inline"><em>{!! bsetecHelpers::timeago(strtotime($applications->createdOn)) !!}</em></h5>
                                <span>{!! Lang::get('core.vert_sep')!!}</span>
                                <h5 class="inline"><em>@if(\Cache::has('user-is-online-' . $applications->user_id)) <span class="text-info"><i class="fa fa-check-circle"></i> Online</span> @elseif(\Cache::has('user-is-away-' . $applications->user_id)) <span class="text-warning"><i class="fa fa-clock-o"></i> Away</span> @else <i class="fa fa-dot-circle-o"></i> Offline @endif</em></h5>
                            </div>
                            <div class="col-sm-2">
                                <h3 class="text-right">{!! $currency !!} <strong>{!! $applications->price !!}</strong></h3>
                            </div>
                        </div>
                        <div>
                            <a href="{!! $applications->user_link !!}" class="btn btn-color">{!! Lang::get('core.btn_view')!!} {!! Lang::get('core.m_profile')!!}</a>
                            @if($row->ccr_status != '4' && $row->ccr_status != '5')
                            <a href="javascript:void(0)" data-aid="{!! $applications->application_id !!}" data-uid="{!! $row->entry_by !!}" class="btn btn-color tips messageBoard" title="{!! Lang::get('core.message_ccr')!!}">{!! Lang::get('core.message')!!}</a>
                            @endif
                            <h2 class="text-info arrow"><strong><i class="fa fa-check-circle"></i> {!! Lang::get('core.awarded_tutors')!!}</strong></h2>
                        </div>
                    </div>
                </div>
            </div>

            @if(!empty($shared_course) && ($loggedid == $row->tutor_id || $loggedid == $row->entry_by))
                <div class="show-grid">
                    <div class="col-md-12">
                            <h3>{!! Lang::get('core.shared_course')!!}: <a href="{!! $shared_course['link'] !!}">{!! $shared_course['title'] !!}</a>
                                @if($row->ccr_status ==3 && 1==2)
                                    <div class="arrow">
                                        {!! Lang::get('core.Completed')!!}
                                    </div>
                                @endif
                            </h3>
                    </div>
                </div>
            @endif

            @if(($row->ccr_status ==1 || $row->ccr_status ==2) && !empty($row->tutor_id) && isset($loggedid) && $loggedid == $row->entry_by && ($row->request_completion))
                <div class="form-group m-b-none">
                    <div class="row">
                        <div class="col-md-12 show-grid">
                            <div class="col-md-12">
                                <h3 class="text-warning"><i class="fa fa-warning"></i> {!! Lang::get('core.tutor_requested_completion') !!} </h3>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if($row->type == 'online' && $row->payment_type == 'hour' && isset($loggedid) && ($loggedid == $row->tutor_id || $loggedid == $row->entry_by || \Session::get('gid') == 1))
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12 show-grid">
                            <div class="col-md-6">
                                <h3>{!! Lang::get('core.total_meeting_time') !!} : {!! $total_meeting_time !!}</h3>
                            </div>
                            <div class="col-md-6">
                                <h3>{!! Lang::get('core.amount_calc') !!} : {!! $currency !!} {!! $amount_calc !!}</h3>
                            </div>
                            <div class="col-md-12">{!! Lang::get('core.note') !!} : {!! Lang::get('core.note_total_time') !!}. @if($loggedid == $row->entry_by) {!! Lang::get('core.note_total_time_student') !!} @endif</div>
                        </div>
                    </div>
                </div>
            @endif

            @if(!empty($recordings) && ($row->ccr_status !=0) && !empty($row->tutor_id) && isset($loggedid) && $loggedid == $row->entry_by)
                <div class="show-grid">
                @foreach($recordings as $key => $recording)
                    <?php //$recording = preg_replace("/^http:/i", "https:", $recording); ?>
                    <div class="col-md-12">
                        <div>  {!! Lang::get('core.recording_for') !!} {!! $key+1 !!}
                        <span style="cursor:pointer;" class="arrow"><a class="viewrecords" href="javascript:void(0);" data-href="{!! $recording !!}">{!! Lang::get('core.view') !!}</a></span>
                        </div>
                    </div>
                @endforeach
                </div>
            @endif
        </div>

    </div>
    @elseif($row->useraccess && !$row->ccr_status)
    <div class="row">
        <div class="request_block">
            <h4>{!! Lang::get('core.applied_tutors')!!} ({!! $row->tutor_count !!})</h4>
        </div>

        @foreach($applications as $application)
        <div class="sbox-content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-1 no-padding">
                        <a href="{!! $application->user_link !!}">
                            <center>
                                {!! $application->imgpath !!}
                            </center>
                        </a>
                    </div>
                    <div class="col-sm-11">
                        <div class="row">
                            <div class="col-sm-10">
                                <a href="{!! $application->user_link !!}">
                                    <h4 class="inline">{!! $application->display_name !!}</h4>
                                </a>
                                <span>{!! Lang::get('core.vert_sep')!!}</span>
                                <h5 class="inline"><em>{!! bsetecHelpers::timeago(strtotime($application->createdOn)) !!}</em></h5>
                                <span>{!! Lang::get('core.vert_sep')!!}</span>
                                <h5 class="inline"><em>@if(\Cache::has('user-is-online-' . $application->user_id)) <span class="text-info"><i class="fa fa-check-circle"></i> Online</span> @elseif(\Cache::has('user-is-away-' . $application->user_id)) <span class="text-warning"><i class="fa fa-clock-o"></i> Away</span> @else <i class="fa fa-dot-circle-o"></i> Offline @endif</em></h5>
                            </div>
                            <div class="col-sm-2">
                                <h3 class="text-right">{!! $currency !!} <strong>{!! $application->price !!}</strong></h3>
                            </div>
                        </div>
                        <div>
                            <a href="{!! $application->user_link !!}" class="btn btn-color">{!! Lang::get('core.btn_view')!!} {!! Lang::get('core.m_profile')!!}</a>
                            <a href="javascript:void(0)" data-uid="{!! $application->user_id !!}"  data-aid="{!! $application->application_id !!}" class="btn btn-color messageBoard">{!! Lang::get('core.message')!!}</a>
                            <a href="{{ URL::to('customcourserequest/award?id='.$application->application_id) }}" class="btn btn-color arrow award" style="background:#f8ac59">{!! Lang::get('core.award_course_request')!!}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach

    </div>
    @endif

</div>
@include('customcourserequest/apply_block')
@include('customcourserequest/cancel_block')
@include('customcourserequest/message')
<script>
$(document).ready(function(){
    $('.award').click(function(e){
        if (!confirm("{{ Lang::get('core.sure_award_edit') }} \n"+"{{ Lang::get('core.sure_award_amount') }} \n\n"+"{{ Lang::get('core.sure_award') }} ")) {
            e.preventDefault();
        }
    });
});
</script>
@if($row->type == 'offline' && ($row->ccr_status ==1 || $row->ccr_status ==2) && !empty($row->tutor_id) && isset($loggedid) && $loggedid == $row->entry_by)
<!-- <script>
$(document).ready(function(){
    $('#completed_ccr').click(function(e){
        if (!confirm("{{ Lang::get('core.sure_complete') }}")) {
            e.preventDefault();
        }
    });
});
</script> -->
@endif
@if($row->type == 'offline' && ($row->ccr_status ==1 || $row->ccr_status ==2) && !empty($row->tutor_id) && isset($loggedid) && $loggedid == $row->tutor_id)
<div class="modal fade" id="createMycourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog course_popup" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="btn_close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{!! Lang::get('core.Create_Course')!!}</h4>
            </div>
            <div class="modal-body">
                <?php echo Form::open(array('url' => 'course/createcourse', 'method' => 'post','id'=>'coursecreateform')); ?>
                <div class="form-group">
                    <label for="coursename">{!! Lang::get('core.create_course_title')!!}</label>
                    <input type="text" name="coursename" class="form-control" id="coursename" placeholder="{!! Lang::get('core.course_name')!!}">
                </div>
                <div class="form-group">
                     <input type="hidden" name="coursetype_sel" id="coursetype_no" value="offline">
                    <input type="hidden" name="coursetype" id="coursetype" value="offline">
                </div>
                <div class="hide online_part">
                    <div class="form-group">
                        <label for="coursefreq">{!! Lang::get('core.Course_Frequency')!!}</label>
                        <div class="col-md-12">
                            <div class="col-md-3 m-b-sm">
                                <input type="radio" name="coursefreq_sel" id="coursetype_once" class="form-control" value="once" checked /> Once
                            </div>
                            <div class="col-md-3 m-b-sm">
                                <input type="radio" name="coursefreq_sel" id="coursetype_daily" class="form-control" value="daily" /> Daily
                            </div>
                            <div class="col-md-3 m-b-sm">
                                <input type="radio" name="coursefreq_sel" id="coursetype_weekly" class="form-control" value="weekly" /> Weekly
                            </div>
                        </div>
                        <input type="hidden" name="coursefreq" id="coursefreq" value="once">
                    </div>
                    <div class="form-group hide weekly_part">
                        <label for="coursetype">{!! Lang::get('core.Course_Frequency_Days')!!}</label>
                        <div class="col-md-12">
                            <div class="col-md-3 m-b-sm">
                                <input type="checkbox" name="coursefreq_days" id="coursetype_monday" class="form-control" value="Monday" checked /> Monday
                            </div>
                            <div class="col-md-3 m-b-sm">
                                <input type="checkbox" name="coursefreq_days" id="coursetype_tuesday" class="form-control" value="Tuesday" /> Tuesday
                            </div>
                            <div class="col-md-3 m-b-sm">
                                <input type="checkbox" name="coursefreq_days" id="coursetype_wednesday" class="form-control" value="Wednesday" /> Wednesday
                            </div>
                            <div class="col-md-3 m-b-sm">
                                <input type="checkbox" name="coursefreq_days" id="coursetype_thursday" class="form-control" value="Thursday" /> Thursday
                            </div>
                            <div class="col-md-3 m-b-sm">
                                <input type="checkbox" name="coursefreq_days" id="coursetype_friday" class="form-control" value="Friday" /> Friday
                            </div>
                            <div class="col-md-3 m-b-sm">
                                <input type="checkbox" name="coursefreq_days" id="coursetype_saturday" class="form-control" value="Saturday" /> Saturday
                            </div>
                            <div class="col-md-3 m-b-sm">
                                <input type="checkbox" name="coursefreq_days" id="coursetype_sunday" class="form-control" value="Sunday" /> Sunday
                            </div>
                            <input type="hidden" name="coursefreqdays" id="coursefreqdays" value="Monday">
                        </div>
                    </div>
                    <div class="form-group once_part">
                        <label for="coursedate">{!! Lang::get('core.create_course_date')!!}</label>
                        <input type="text" name="coursedate" class="form-control datetimenosec" id="coursedate" placeholder="{!! Lang::get('core.create_course_date')!!}">
                    </div>
                    <div class="form-group hide daily_part weekly_part">
                        <label for="coursedatefrom">{!! Lang::get('core.create_course_date_from')!!}</label>
                        <input type="text" name="coursedatefrom" class="form-control date" id="coursedatefrom" placeholder="{!! Lang::get('core.create_course_date_from')!!}">
                    </div>
                    <div class="form-group hide daily_part weekly_part">
                        <label for="coursedateto">{!! Lang::get('core.create_course_date_to')!!}</label>
                        <input type="text" name="coursedateto" class="form-control date" id="coursedateto" placeholder="{!! Lang::get('core.create_course_date_to')!!}">
                    </div>
                    <div class="form-group hide daily_part weekly_part">
                        <label for="coursetime">{!! Lang::get('core.create_course_time')!!}</label>
                        <input type="text" name="coursetime" class="form-control timenosec" id="coursetime" placeholder="{!! Lang::get('core.course_time')!!}">
                    </div>
                    <div class="form-group">
                        <label for="coursetime_duration">{!! Lang::get('core.create_course_time_duration')!!}</label>
                        <select name="coursetime_duration" class="form-control">
                            <option value="">{!! Lang::get('core.select_duration')!!}</option>
                            <option value="30">30 Minutes</option>
                            <option value="60">1 Hour</option>
                            <option value="90">1 Hour 30 Minutes</option>
                            <option value="120">2 Hour</option>
                            <option value="150">2 Hour 30 Minutes</option>
                            <option value="180">3 Hour</option>
                            <option value="210">3 Hour 30 Minutes</option>
                            <option value="240">4 Hour</option>
                            <option value="270">4 Hour 30 Minutes</option>
                            <option value="300">5 Hour</option>
                            <option value="330">6 Hour 30 Minutes</option>
                            <option value="360">6 Hour</option>
                            <option value="390">6 Hour 30 Minutes</option>
                            <option value="420">7 Hour</option>
                            <option value="450">7 Hour 30 Minutes</option>
                            <option value="480">8 Hour</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="courselastdate">{!! Lang::get('core.create_course_lastenrolldate')!!}</label>
                        <input type="text" name="courselastdate" class="form-control datetimenosec" id="courselastdate" placeholder="{!! Lang::get('core.create_course_lastdate')!!}">
                    </div>
                </div>
                <button type="submit" id="createcourse" class="btn btn-color">{!! Lang::get('core.btn_create')!!}</button>
                <?php  echo Form::close(); ?>

                <p class="m-t-md"><strong>NOTE: </strong><br> 1. {!! Lang::get('core.create_course_free')!!} <br> 2. {!! Lang::get('core.create_course_private')!!}<br> 3. {!! Lang::get('core.create_course_share')!!} <br> 4. <strong>Fees and Charges:</strong> Please note that a fee of {!! \bsetecHelpers::get_options('commision_percentage')['commision_percentage']; !!}% with the transaction charge of {!! \bsetecHelpers::get_options('transaction_charges_percentage')['transaction_charges_percentage']; !!}% will be deducted from the course price</p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function(){
    $(document).on('click','#createcourseid',function(){
        $('#createMycourse').modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    $.validator.addMethod("customwhitespace", function(value, element) {
    return (this.optional(element) || $.validator.methods.required.call(this, $.trim(value), element));
    }, "Please enter a valid course name");

    $('#coursetype_yes').on('ifChecked', function(event){
        $('.online_part').removeClass('hide');
        $('input[name="coursetype"]').val('online');
    });
    $('#coursetype_no').on('ifChecked', function(event){
        $('.online_part').addClass('hide');
        $('input[name="coursetype"]').val('offline');
    });
    $('#coursetype_once').on('ifChecked', function(event){
        $('.daily_part').addClass('hide');
        $('.weekly_part').addClass('hide');
        $('.once_part').removeClass('hide');
        $('input[name="coursefreq"]').val('once');
    });
    $('#coursetype_daily').on('ifChecked', function(event){
        $('.once_part').addClass('hide');
        $('.weekly_part').addClass('hide');
        $('.daily_part').removeClass('hide');
        $('input[name="coursefreq"]').val('daily');
    });
    $('#coursetype_weekly').on('ifChecked', function(event){
        $('.daily_part').addClass('hide');
        $('.once_part').addClass('hide');
        $('.weekly_part').removeClass('hide');
        $('input[name="coursefreq"]').val('weekly');
    });
    var freqdays = ["Monday"];
    $('input[name="coursefreq_days"]').on('ifChecked', function(event){
        var cval = $(this).val();
        freqdays.push(cval);
        $('input[name="coursefreqdays"]').val(freqdays.join(","));
    });
    $('input[name="coursefreq_days"]').on('ifUnchecked', function(event){
        var cval = $(this).val();
        freqdays = jQuery.grep(freqdays, function(value) {
          return value != cval;
        });
        $('input[name="coursefreqdays"]').val(freqdays.join(","));
    });

    $("#coursecreateform").validate({
        ignore: [],
        rules: {
            coursename: {
                 required: true,
                // minlength: 5,
                maxlength: 60,
                customwhitespace: true,
                remote: {
                    url: "{{ URL::to('course/coursenameexits') }}",
                    type: "GET",
                    data: {
                        coursename: function() {
                            return $( "#coursename" ).val();
                        }
                    }
                }
            },
            coursetype: {
                required: true,
            },
            coursefreqdays: {
                required: function(element){
                    return $("#coursefreq").val()=="weekly" && $("#coursetype").val()=="online";
                }
            },
            coursedate: {
                required: function(element){
                    return $("#coursefreq").val()=="once" && $("#coursetype").val()=="online";
                }
            },
            coursedatefrom: {
                required: function(element){
                    return $("#coursefreq").val()!="once" && $("#coursetype").val()=="online";
                }
            },
            coursedateto: {
                required: function(element){
                    return $("#coursefreq").val()!="once" && $("#coursetype").val()=="online";
                }
            },
            coursetime: {
                required: function(element){
                    return $("#coursefreq").val()!="once" && $("#coursetype").val()=="online";
                }
            },
            coursetime_duration: {
                required: function(element){
                    return $("#coursetype").val()=="online";
                }
            },
            courselastdate: {
                required: function(element){
                    return $("#coursetype").val()=="online";
                }
            }
        },
        messages: {
            coursename: {
                required: "{{ Lang::get('core.enter_course') }}",
                // minlength: "Course name must consist of at least 5 characters",
                maxlength: "{{ Lang::get('core.no_length') }}",
                remote: "{{ Lang::get('core.course_taken') }}"
            },
            coursetype: {
                required: "{{ Lang::get('core.enter_coursetype') }}",
            },
            coursefreqdays: {
                required: "{{ Lang::get('core.enter_coursefreq_days') }}",
            },
            coursedate: {
                required: "{{ Lang::get('core.enter_coursedate') }}",
            },
            coursedatefrom: {
                required: "{{ Lang::get('core.enter_coursedatefrom') }}",
            },
            coursedateto: {
                required: "{{ Lang::get('core.enter_coursedateto') }}",
            },
            coursetime: {
                required: "{{ Lang::get('core.enter_coursetime') }}",
            },
            coursetime_duration: {
                required: "{{ Lang::get('core.enter_courseduration') }}",
            },
            courselastdate: {
                required: "{{ Lang::get('core.enter_courselastdate') }}",
            }
        },submitHandler: function() {
            $('#createcourse').prop("disabled", true);
            return true;
        }
    });

    $("#share_courseform").validate({
        rules: {
            course_id: {
                required: true
            }
        },
        messages: {
            course_id: {
                required: "{{ Lang::get('core.select_a_course') }}"
            }
        },submitHandler: function() {
            $('#share_course').prop("disabled", true);
            return true;
        }
    });

    $(document).on('click','#change_course',function(){
        $('#change_course').hide();
        $('#change_course_cancel').show();
        $('#create_course_div').show();
    });

    $(document).on('click','#change_course_cancel',function(){
        $('#change_course').show();
        $('#change_course_cancel').hide();
        $('#create_course_div').hide();
    });
});
</script>
@endif

@if(($row->ccr_status ==4 && !empty($dispute)) && !empty($row->tutor_id) && isset($loggedid) && ($loggedid == $row->entry_by || $loggedid == $row->tutor_id || \Session::get('gid') == 1))
@if(\Session::get('gid') == 1)
@php $type = '3'; $pprice = $dispute->price_proposed_by_admin; @endphp
@elseif($loggedid == $row->entry_by)
@php   $type = '1'; $pprice = $dispute->price_proposed_by_student; @endphp
@elseif($loggedid == $row->tutor_id)
@php $type = '2'; $pprice = $dispute->price_proposed_by_tutor; @endphp
@endif
<div class="modal fade" id="editproposed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1">
    <div class="modal-dialog course_popup" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="btn_close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel1">{!! Lang::get('core.prop_price')!!}</h4>
            </div>
            <div class="modal-body">
                <?php echo Form::open(array('url' => 'customcourserequest/proposalprice', 'method' => 'post','id'=>'editproposedform')); ?>
                <input type="hidden" name="ccr_id" value="{!! $row->ccr_id !!}" />
                <input type="hidden" name="final_price" id="final_price" value="{!! $row->final_price !!}" />
                <input type="hidden" name="dispute_id" value="{!! $dispute->dispute_id !!}" />
                <input type="hidden" name="type" value="{!! $type !!}" />
                <div class="form-group">
                    <label for="price">{!! Lang::get('core.price')!!} @if($row->payment_type == 'hour') ({!! Lang::get('core.note_hourly')!!}) @endif</label>
                    <input type="text" name="price" class="form-control" id="price" placeholder="{!! Lang::get('core.price')!!}" value="{!! $pprice !!}">
                </div>
                <button type="submit" id="submitprice" class="btn btn-color">{!! Lang::get('core.submit')!!}</button>
                <?php  echo Form::close(); ?>
            </div>
        </div>
    </div>
</div>
@if(\Session::get('gid') == 1 || $loggedid == $row->tutor_id)
<div class="modal fade" id="finishdispute" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog course_popup" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="btn_close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel2">{!! Lang::get('core.finish_dispute')!!}</h4>
            </div>
            <div class="modal-body">
                <?php echo Form::open(array('url' => 'customcourserequest/finishdispute', 'method' => 'post','id'=>'finishdisputeform')); ?>
                <input type="hidden" name="ccr_id" value="{!! $row->ccr_id !!}" />
                <input type="hidden" name="final_price2" id="final_price2" value="{!! $row->final_price !!}" />
                <input type="hidden" name="dispute_id" value="{!! $dispute->dispute_id !!}" />

                @if(\Session::get('gid') == 1)

                <div class="form-group">
                    <label for="price">Refund Proposed by Admin @if($row->payment_type == 'hour') ({!! Lang::get('core.note_hourly')!!}) @endif</label>
                    <input type="text" name="price" class="form-control" id="price" placeholder="{!! Lang::get('core.price')!!}" value="{!! $pprice !!}">
                </div>
                <button type="submit" id="submitfinish" class="btn btn-color">{!! Lang::get('core.finish_dispute')!!}</button>
                @elseif($loggedid == $row->tutor_id)
                <h4>Refund Proposed by Student: {!! $currency.' '.$dispute->price_proposed_by_student !!}</h4>
                <input type="hidden" name="price" value="{!! $dispute->price_proposed_by_student !!}" />
                <button type="submit" id="submitfinish" class="btn btn-color">{!! Lang::get('core.finish_dispute_tutor')!!}</button>
                @endif
                <?php  echo Form::close(); ?>
            </div>
        </div>
    </div>
</div>
@endif

<script type="text/javascript">
$(function(){
    $(document).on('click','.proposeprice',function(){
        $('#editproposed').modal({
            backdrop: 'static',
            keyboard: false
        });
    });
    $(document).on('click','.disputeFinish',function(){
        $('#finishdispute').modal({
            backdrop: 'static',
            keyboard: false
        });
    });
    $.validator.addMethod('lessThanEqual', function(value, element, param) {
        return this.optional(element) || parseFloat(value) <= parseFloat($(param).val());
    }, "{!! Lang::get('core.choose_proposeprice')!!}");
    $("#editproposedform").validate({
        rules: {
            price: {
                required: true,
                lessThanEqual: "#final_price"
            }
        },
        messages: {
            price: {
                required: "{{ Lang::get('core.enter_prop_price') }}"
            }
        },submitHandler: function() {
            $('#submitprice').prop("disabled", true);
            return true;
        }
    });
    $("#finishdisputeform").validate({
        rules: {
            price: {
                required: true,
                lessThanEqual: "#final_price2"
            }
        },
        messages: {
            price: {
                required: "{{ Lang::get('core.enter_prop_price') }}"
            }
        },submitHandler: function() {
            $('#submitfinish').prop("disabled", true);
            return true;
        }
    });
});
</script>
@endif
@if(!empty($recordings) && ($row->ccr_status !=0) && !empty($row->tutor_id) && isset($loggedid) && $loggedid == $row->entry_by)
<div class="modal fade" id="recordingshow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
    <div class="modal-dialog course_popup" role="document" style="width:90% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="btn_close" class="close removeframe" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel3">{!! Lang::get('core.recording_for') !!}</h4>
            </div>
            <div class="modal-body" style="max-height: none !important;">
                <div class="row">
                    <iframe id="recordview" src="" style="width: 100%; height: 100%; min-height: 820px; border: none;"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(function(){
    $(document).on('click','.viewrecords',function(){
        var elem = $(this);
        var href = elem.attr('data-href');
        $('#recordview').attr('src',href);
        $('#recordingshow').modal({
            backdrop: 'static',
            keyboard: false
        });
    });
    $(document).on('click','.removeframe',function(){
        $('#recordview').attr('src','');
    });
});
</script>
@endif

@stop
