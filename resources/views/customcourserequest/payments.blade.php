@extends('layouts.frontend')
@section('content')
<style>
.btn.btn-xxl{
    border-radius: 0;
    font-family: open sans;
    font-size: 18px;
	text-transform: uppercase;
    font-weight: 600;
    height: 50px;
}
.search-block{
	padding:0;
	margin-bottom:20px;
}
.search-block .course_searching{
	width:100%;
}
.course_searching .more_one_portiion{
	width:90%;
}
.course_searching .btn{
	width:6%;
	text-align:center;
	padding:0px;
}
.ccr_titles{
	margin:0 !important;
}
@media (max-width: 768px) {
	.course_searching .btn{
		width:15%;
	}
	.course_searching .more_one_portiion{
		width:85%;
	}
	.TeachingCourse_create {
	    float: none !important;
	    width: 100% !important;
	}
}
</style>
@if (defined('CNF_CURRENCY'))
@php ( $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
@else
@php ($currency = 'USD' )
@endif
<div class="page-content row">
	<div class="reg_form new_reg_form mycourse_block">
		<div id="mainwrapper">
			<div class="col-sm-12">
				<div class="head_block clearfix">
					 <h2 class="title col-sm-9">{!! Lang::get('core.custom_course_request'); !!} {!! Lang::get('core.payment_history'); !!}</h2>
					 @if(\Session::get('ut') != "tutor")
					 <div class="TeachingCourse_create col-sm-3">
						<p class="discover_courses"><a class="btn course_create" href="{{ URL::to('customcourserequest/update') }}">{!! Lang::get('core.create_course_request'); !!}</a></p>
					</div>
					@endif
				</div>
				<div class="credit-history">
					<div class="table-block">
						<table class="table">
							<tr>
								<th>#</th>
								<th>{!! Lang::get('core.custom_course_request') !!}</th>
								<th>{!! Lang::get('core.amount') !!} ({!! $currency !!})</th>
								<th>{!! Lang::get('core.Payment_Type') !!}</th>
								<!-- <th>{!! Lang::get('core.total_meeting_time') !!}</th> -->
								<th>{!! Lang::get('core.debit') !!} ({!! $currency !!})</th>
								<th>{!! Lang::get('core.u_credit') !!} ({!! $currency !!})</th>
								<!-- <th>{!! Lang::get('core.refund') !!} ({!! $currency !!})</th> -->
								<th>{!! Lang::get('core.payment_status') !!}</th>
								<th>{!! Lang::get('core.custom_course_request') !!} {!! Lang::get('core.status') !!}</th>
							</tr>
							@if(isset($payments) && count($payments)>0)
								@foreach ($payments as $key => $payment)
									@if(isset($payment->status))
										<tr>
											<td>{!! $numbe = $numbe+1 !!}</td>
											<td><a style="text-decoration: none;" href="{{ URL::to('customcourserequest/show/'.$payment->ccr_id.'?return='.$return) }}"><h4>{!! $payment->ccr_name !!}</h4></a></td>
											<td>{!! $payment->final_price !!}</td>
											<td style="text-transform: capitalize;">{!! $payment->payment_type !!}</td>
											<!-- <td>{!! $payment->total_meeting_time !!}</td> -->
											<td>{!! $payment->debit !!}</td>
											<td>{!! $payment->credit !!}</td>
											<!-- <td>{!! $payment->refund !!}</td> -->
											<td>{!! $payment->status !!}</td>
											<td>{!! $payment->ccrstatus !!}</td>
										</tr>
									@endif
								@endforeach
							@else
							<tr>
								<td colspan="9" align="center"> {!! Lang::get('core.no_record')!!} </td>
							</tr>
							@endif
						</table>
					</div>
					<div class="table-footer">
				        <div class="row">
				            <div class="col-sm-12">
				                <div class="arrow m-t-md">
				                    <ul class="pagination">
				                    	@if($payments_count>=$take)
				                    		@if($page_no == 1)
				                    			<li class="disabled"><span>«</span></li>
					                    	@else
				                    			<li><a href="{{ URL::to('customcourserequest/payments'.'?page=1') }}">«</a></li>
				                    		@endif
					                    	@for($i=1; $i<=ceil($payments_count/$take); $i++)
						                    	@if($i == $page_no)
					                    			<li class="active"><span>{!! $i !!}</span></li>
						                    	@else
						                    		<li><a href="{{ URL::to('customcourserequest/payments'.'?page='.$i) }}">{!! $i !!}</a></li>
						                    	@endif
					                    	@endfor
				                    		@if($page_no == ceil($payments_count/$take))
				                    			<li class="disabled"><span>»</span></li>
					                    	@else
				                    			<li><a href="{{ URL::to('customcourserequest/payments'.'?page='.$i) }}">»</a></li>
				                    		@endif
			                    		@endif
				                    </ul>
				                </div>
				            </div>
				        </div>
				    </div>
					<div class="m-t-sm">{!! Lang::get('core.note'); !!}: {!! Lang::get('core.outstanding_pay'); !!}. {!! Lang::get('core.note_total_time_student') !!}</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop