<div class="modal fade" id="askMessageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog course_popup" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="btn_close" class="close msgmodalclose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">@if($row->ccr_status == '4') {!! Lang::get('core.Discussions')!!} @else {!! Lang::get('core.Messages')!!} @endif</h4>
            </div>
            <div class="modal-body">
                <div class="message_block">
                    <div class="message_loader"><h2><center><i class="fa fa-refresh fa-spin"></i> Loading...</center></h2></div>
                </div>
                {!! Form::open(array('url'=>'customcourserequest/messageslist', 'class'=>'form-horizontal getMessages','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
                    {!! Form::hidden('application_id', '' )  !!}
                {!! Form::close() !!}


                {!! Form::open(array('url'=>'customcourserequest/messagesend', 'class'=>'form-horizontal sendMessage','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
                <input type="hidden" name="typee" class="type" />
                <input type="hidden" name="recipient" />
                <input type="hidden" name="subject" value="{!! Lang::get('core.custom_course_request').' '.Lang::get('core.Messages') !!}" />
                <div class="form-group">
                    <div class="col-xs-12">
                        <div>
                            <textarea name='message' rows='3' style="width:100%;" class='form-control messagebox' placeholder="{{ Lang::get('core.type_message')}}"></textarea>
                        </div>
                        <span class="error" style="color:red"></span>
                    </div>
                </div>
                <div class="sending_loadergs"></div>
                <div class="form-group">
                    <div class="col-xs-10">
                        {!! Form::hidden('draft', '0' )  !!}
                        {!! Form::hidden('messageid', '' )  !!}
                        {!! Form::hidden('applicationid', '' )  !!}
                        <button type="submit" name="submit" data-type="inboxx" class="btn btn-primary btn-sm sendbtn" ><i class="fa  fa-envelope "></i> {!! Lang::get('core.btn_message') !!}</button>
                    </div>
                </div>
             {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
       toastr.options={ "positionClass": "toast-top-right" };
    $('.messageBoard').click(function(){
        var elem = $(this);
        //console.log(elem)
        var applicationid = elem.attr('data-aid');
        var recipient = elem.attr('data-uid');
        $('#askMessageModal').find('[name="applicationid"]').val(applicationid);
        $('#askMessageModal').find('[name="application_id"]').val(applicationid);
        $('#askMessageModal').find('[name="recipient"]').val(recipient);
        $('#askMessageModal').find('[name="message"]').val('');
        $('.sendbtn').removeAttr('disabled');

        refreshMessages();
        interval = setInterval(function(){ refreshMessages(); },5000);

        $('#askMessageModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    });


    function refreshMessages(ty){
        var url = $(".getMessages").attr('action');
        $.ajax({
            type: 'POST',
            url: url,
            data: $(".getMessages").serialize(),
            success : function(data) {
                if(data != '') {
                    $('.message_block').html(data);
                    $('.message_loader').hide();
                }
            }
        });
    }
    var interval = null;
    $('.msgmodalclose').click(function(){ clearInterval(interval); });

    $('.messagebox').keydown(function(event) {
        if (event.keyCode == 13) {
            $(this.form).submit()
            return false;
         }
    });

    $('.sendMessage').submit(function(){

        var value = $('#askMessageModal').find('[name="message"]').val();
        var typee = $('[name="typee"]').val();

        if(value==''){
            $('.error').html("{!! Lang::get('core.msg_required')!!}");
        }else{
            $('.error').html('');
        }

        if($('[name="recipient"]').val() !='' && $('[name="subject"]').val() !='' && value !=''){
            $('.sendbtn').attr('disabled','disabled');
            var url = $(this).attr('action');
            $.ajax({
                type: 'POST',
                url: url,
                data: $(".sendMessage").serialize(),
                beforeSend:function(){
                    $('.sendbtn').hide();
                    $('.sending_loadergs').append('<h2><i class="fa fa-refresh fa-spin"></i> Sending...</h2>');
                },
                success : function(data) {
                    if(data != '') {
                        toastr.success("{{ Lang::get('core.success_msg') }}");
                        $('#askMessageModal').find('[name="message"]').val('');
                        $('.message_block').append(data);
                    } else {
                        toastr.error("{{ Lang::get('core.error_msg') }}");
                    }
                    $('.sendbtn').removeAttr('disabled');
                    $('.sendbtn').show();
                },complete: function() {
                    $('.sending_loadergs').html('');
                }
            });
        }
        return false;
    });

});
</script>