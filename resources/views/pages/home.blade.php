            <section class="banner_section">            
            <div class="banner">
            <div class="tp-banner-container">
          <div class="tp-banner">
            <ul>
            @if(count($banners) > 0)
                @foreach($banners as $banner)
                 <li data-masterspeed="1500" data-slotamount="7" data-transition="fade">
                   <img data-bgrepeat="no-repeat" data-bgposition="left top" data-bgfit="cover" alt="slidebg1" src="{!! url('uploads/banner/'.$banner->banner_image) !!}">
        					<div data-easing="Power4.easeOut" data-start="1200" data-speed="500" data-y="420" data-x="20" class="tp-caption  skewfromrightshort fadeout">
        					</div>
                  </li>
                @endforeach
            @else
               <li data-masterspeed="1500" data-slotamount="7" data-transition="fade">	
                   <img src="{{ url('bsetec/images/banner.png') }}" />
                   <div data-easing="Power4.easeOut" data-start="1200" data-speed="500" data-y="420" data-x="20" class="tp-caption  skewfromrightshort fadeout">
    					     </div>
               </li>
            @endif
                </ul>
            </div>
            </div>
            <div class="easier_course">
            <h2 class="animated fadeInDown">{!! Lang::get('core.learning')!!} <span>{!! Lang::get('core.online')!!}</span> {!! Lang::get('core.easier')!!}</h2>
            <p class="animated fadeInLeft">{!! Lang::get('core.banner_hint')!!}</p>

             @if(!Auth::check())
            <div class="learn_remeber edit_tutorial animated fadeInDown"><a href="{{ url('user/register') }}" class="btn btn-primary"> {!! Lang::get('core.member')!!}</a></div>
             @endif
            </div>
            <!-- services dynamic contents -->

            @php ( $page_contents = \bsetecHelpers::get_options('page_contents') )
            
            @if(count($page_contents)>0)
                @if(isset($page_contents['page_services_content']))
                    {!! $page_contents['page_services_content'] !!}
                @endif
            @endif

            <!-- services dynamic contents -->
            </div>
            
            </section>
            
            <div class="seach_alone_courses clearfix">
            <div class="container">
            <div class="search-block clearfix">
            <h3>{!! Lang::get('core.heree') !!}</h3>
            <div class="course_searching"><form role="search" id="search-form" method="GET" action="{{ url('search') }}"><input type="text" class="more_one_portiion" placeholder="{!! Lang::get('core.find_course') !!}" name="q" id="srch-term">
            </form></div>
            <form id="myform" method="post" name="myform" action="">
            <div class="dropdown">
            
            <a href="#" data-toggle="dropdown">{!! Lang::get('core.categories') !!}<b class="caret"></b><span class="select_arrow"> </span></a>
            <ul class="dropdown-menu ul_width mCustomScrollbar">
            <li><a href="">{!! Lang::get('core.categories') !!}</a></li>
            @foreach(\bsetecHelpers::siteCategories() as $category)
            <li><a href="{{ url( 'category/'.$category->slug) }}">{{ $category->name }}</a></li>
            @endforeach
            </ul>
            <span class="right_drop"></span>
            <span class="left_drop"></span>
            </div>        
            </form>
            <button class="btn btn-primary" id="search"  type="submit">{!! Lang::get('core.se') !!}</button>
            </div>
            </div>
            </div>
          
            <div class="tab_block">
            <div class="container">        
            <ul class="row nav-tabs" id="example-one">
                  <li class="current_page_item active" id="magic" >
                  	<a data-toggle="tab" href="#sectionA">{!! Lang::get('core.latest') !!} <span>{!! Lang::get('core.courses') !!}</span></a>
                  </li>
                  <li class=""><a data-toggle="tab" href="#sectionB">{!! Lang::get('core.featured') !!} <span>{!! Lang::get('core.courses') !!}</span></a></li>
                  <li class=""><a data-toggle="tab" href="#sectionC">{!! Lang::get('core.most') !!}<span>{!! Lang::get('core.viewed') !!}</span></a></li>
   
            </ul>
            
            <p class="txt">{!! Lang::get('core.msg') !!}</p>
            
            
            <div class="tab-content">
            
            <div id="sectionA" class="tab-pane fade in active">
            <div class="courses_block">
            <div class="row">
            
            @foreach($course as $course)
            <div class="col-sm-4">
            <div class="course_b"><div class="img_hover">
            <a href="{{ URL::to('courseview/'.$course->course_id.'/'.$course->slug) }}" >
            <img src="{{asset('assets/bsetec/images/spacer.gif')}}" class="lazy" data-src="{{ \bsetecHelpers::getImage($course->image) }}" data-original="{{ \bsetecHelpers::getImage($course->image) }}"  > 
            <span class="bg"></span>
            <div class="user-img-b">
            <a href="{{ URL::to('profile/'.$course->username,'') }}" title="{!! $course->first_name !!}">
              {!! SiteHelpers::customavatar($course->email,$course->user_id,'small','') !!}
              <p>{!! $course->first_name !!}</p>
            </a>
            <!-- <p>3 likes</p> -->
            @foreach($coursestudents as $students)
            @if( $course->course_id == $students->course_id)
                   <p> {{ $students->total }} students</p>
            @endif

            @endforeach
            </div>
            
            </a></div>
            <div class="detail_block">
            <h4><a href="{{ URL::to('courseview/'.$course->course_id.'/'.$course->slug) }}">{!! $course->course_title !!}</a></h4>
            <div class="clearfix">
            <div class="star_rating">
            <ul class="star_one clearfix">
            <li class="late_star ReadonlyRating startvalue" data-score="{{\bsetecHelpers::checkReview($course->course_id)}}"></li>
            </ul>
            </div>
            <p>

                  @if($course->pricing != 0)
                      <div class="business_amount"><p class="rate"> 
                       {!! SiteHelpers::getCurrencymethod($course->user_id,$course->pricing) !!}
                      </p></div>
                  @else
                      <div class="business_free"><p class="rate">{!! Lang::get('core.free') !!}</p></div>
                  @endif
            </p>
            </div>
            </div>
            </div>
            </div>
            @endforeach
            </div>
            </div>
            </div>
            
            <div id="sectionB" class="tab-pane fade">
            
            <div class="courses_block">
            <div class="row">
            @foreach($feature as $feature)
            <div class="col-sm-4">
            <div class="course_b"><div class="img_hover"><a href="{{ URL::to('courseview/'.$feature->course_id.'/'.$feature->slug) }}">
            <img class="lazy" data-src="{{ \bsetecHelpers::getImage($feature->image) }}" data-original="{{ \bsetecHelpers::getImage($feature->image) }}" src="{{asset('assets/bsetec/images/spacer.gif')}}" > <span class="bg"></span>
            <div class="user-img-b">
            <a href="{{ URL::to('profile/'.$feature->username) }}" title="{!! $feature->first_name !!}">
            {!! SiteHelpers::customavatar($feature->email,$feature->user_id,'small','') !!}
            <p>{!! $feature->first_name !!}</p>
            </a>
           <!--  <p>3 likes</p> -->
            @foreach($coursestudents as $students)
            @if( $feature->course_id == $students->course_id)
                   <p> {{ $students->total }} students</p>
            @endif

            @endforeach
            </div>
            
            </a></div>
            <div class="detail_block">
            <h4><a href="{{ URL::to('courseview/'.$feature->course_id.'/'.$feature->slug) }}">{!! $feature->course_title !!}</a></h4>
            <div class="clearfix">
            <div class="star_rating">
            <ul class="star_one clearfix">
            <li class="late_star ReadonlyRating startvalue" data-score="{!! $feature->rating_count !!}"></li>
            </ul>
            </div>
            <p class="rate">
                  @if($feature->pricing != 0)
                      <div class="business_amount"><p class="rate">{!! SiteHelpers::getCurrencymethod($feature->user_id,$feature->pricing) !!}</p></div>
                  @else
                      <div class="business_free"><p class="rate">{!! Lang::get('core.free') !!}</p></div>
                  @endif
            </p>
            </div>
            </div>
            </div>
            </div>
            @endforeach
            </div>
            </div>
            </div>
      
            <div id="sectionC" class="tab-pane fade">
            
            <div class="courses_block">
            <div class="row">
            @foreach($viwed as $view)
            <div class="col-sm-4">
            <div class="course_b"><div class="img_hover"><a href="{{ URL::to('courseview/'.$view->course_id.'/'.$view->slug) }}">
            <img class="lazy" data-src="{{ \bsetecHelpers::getImage($view->image) }}" data-original="{{ \bsetecHelpers::getImage($view->image) }}" src="{{asset('assets/bsetec/images/spacer.gif')}}" > <span class="bg"></span>
            <div class="user-img-b">
            <a href="{{ URL::to('profile/'.$view->username) }}" title="{!! $view->first_name !!}">
            {!! SiteHelpers::customavatar($view->email,$view->user_id,'small','') !!}
            <p>{!! $view->first_name !!}</p>
            </a>
            @foreach($coursestudents as $students)
            @if( $view->course_id == $students->course_id)
                   <p> {{ $students->total }} students</p>
            @endif
            @endforeach
            </div>
            
            </a></div>
            <div class="detail_block">
            <h4><a href="{{ URL::to('courseview/'.$view->course_id.'/'.$view->slug) }}">{!! $view->course_title !!}</a></h4>
            <div class="clearfix">
            <div class="star_rating">
            <ul class="star_one clearfix">
            <li class="late_star ReadonlyRating startvalue" data-score="{!! $view->rating_count !!}"></li>
            </ul>
            </div>
            <p class="rate">
                  @if($view->pricing != 0)
                      <div class="business_amount"><p class="rate">{!! SiteHelpers::getCurrencymethod($view->user_id,$view->pricing) !!}</p></div>
                  @else
                      <div class="business_free"><p class="rate">{!! Lang::get('core.free') !!}</p></div>
                  @endif
            </p>
            </div>
            </div>
            </div>
            </div>
            @endforeach
            </div>
            </div>
            </div>

            </div>
            </div>
            </div>
            
            <div class="watch_listen_practice_section">
            <div class="container">
            <!-- about dynamic contents -->
            
            @if(count($page_contents)>0)
                @if(isset($page_contents['page_about_content']))
                    {!! $page_contents['page_about_content'] !!}
                @endif    
            @endif

            <!-- about dynamic contents -->
            </div>
            </div>
            
            <div class="our_clients_section">
            <div class="container">
            <div class="row">
            <div class="post_only">
            <h3>{!! Lang::get('core.our') !!} <span>{!! Lang::get('core.client') !!}</span></h3>
            </div>
            <p>{!! Lang::get('core.client_msg') !!}</p>
            </div>
            </div>
            </div>
            <div class="demo_slides_one">
            <div class="container">
            <ul class="bxslider">
            @php ( $clientAds = \bsetecHelpers::getClientAds() )
             @foreach($clientAds as $client)
              <li>
              <a href="#" title="{{ $client->banner_title }}">
                <img class="lazy" src="{{asset('assets/bsetec/images/spacer.gif')}}" data-src="{{ asset( url('').'/uploads/banner/'.$client->banner_image) }}"  data-original="{{ asset( url('').'/uploads/banner/'.$client->banner_image) }}"/>
              </a>
              </li>
             @endforeach
            </ul>
            </div>
            </div>
@if(bsetecHelpers::getmembershipstatus())
             @php ( $userMembership = \bsetecHelpers::getMembershipofuseractive() )
    @php ( $monthly_s = \bsetecHelpers::siteMembership() )
    <div class="pricing_container section">
      <div class="container">
        <h3>OUR PRICING</h3>
        <p>We build awesome course & university sites! Don't miss out <font>join us today!</font></p>
        <div class="row wow zoomIn">
          @if(count($monthly_s)>0)
          @php(  $clsarry = array('plan-basic','plan-premium','plan-elite'))
          @php ( $skey = 0)
          @foreach($monthly_s as $key => $mvalue)
          @if($skey=='4')
          @php(  $skey=0 )
          @endif
          @if($mvalue->status || $mvalue->status_y)
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="pricing_content pricing_con_{{ $skey }}">
              <div class="pricing_heading">{!! $mvalue->plan_name !!}</div>
              <div class="pricing_amounts">
                <p><span>@if($mvalue->status==1) @if(!empty($currency)) {!!  $currency !!} @endif {!! $mvalue->plan_amount !!}@else No @endif</span>Monthly @if(!$mvalue->plan_amount) Costs @endif</p>
                <p><span>@if($mvalue->status_y==1)@if(!empty($currency)) {!!  $currency !!} @endif {!! $mvalue->plan_amount_y !!}@else No @endif</span>yearly @if(!$mvalue->plan_amount_y) Costs @endif</p>
                @if($mvalue->plan_id != 4)
                <div class="course_view_price"><a href="{{ URL::to('membership/courselist/'.$mvalue->plan_id) }}">View Courses</a></div>
                @else
                <div class="course_view_price"><a href="{{ URL::to('freeCourses') }}">View Courses</a></div>
                @endif
              </div>
              <div class="pricing_descr">{!! $mvalue->plan_statement !!} </div>
              @if(isset($userMembership) && $mvalue->plan_id == $userMembership->plan_id)
              <p class="term_text term_btn_text"><span>{!! ucfirst($userMembership->purchase_period) !!}</span> <br>{!! Lang::get('core.renews_at') !!}: {!! date("F d, Y h:i:s A",strtotime('+1 '.str_replace('ly', '', $userMembership->purchase_period),strtotime($userMembership->created_at))) !!}</p>
              @elseif(isset($userMembership) && $mvalue->level <= $userMembership->level)
              <div class="status_text">{!! Lang::get('core.downgrade') !!}</div>
              @elseif(isset($userMembership) && $mvalue->level >= $userMembership->level)
              <div class="status_text">{!! Lang::get('core.upgrade') !!}</div>
              @endif
              @if($mvalue->plan_id != 4)
              @if(Auth::check() == false ) @php( $btncls = '1' ) @else @php( $btncls = '2' ) @endif
              @if( $mvalue->plan_id != bsetecHelpers::getusermembership() )
              <p class="term_btn_text"><a href="{{ URL::to('user/membership/'.$mvalue->plan_id) }}" class="btn btn-mixed btnopt">{!! Lang::get('core.continue') !!}</a></p>
              @endif
              @else
              <p class="term_btn_text"><a href="javascript:void(0)" class="btn btn-mixed btnopt">{!! Lang::get('core.continue') !!}</a></p>
              @endif
            </div>
          </div>
          @endif
          @php(  $skey++ )
          @endforeach
          @endif
        </div>
      </div>
    </div>
    @endif
<script type="text/javascript">
   
$(function(){
    $('.tp-banner').revolution({
            delay: 5000,
            startwidth: 1170,
            startheight: 650,
            hideThumbs: 10,
            fullWidth: "on",
        });
	$('#front-header').removeClass();	
    $('body').removeClass();
	$('body').addClass('home');
     //serach form submit
	   @if(count($clientAds) > 1)
  	$('.bxslider').bxSlider({
      minSlides: 3,
      maxSlides: 4,
      slideWidth: 146,
      slideMargin: 0
    });
  @endif 
      $('#search').on('click',function(){
          if($('#srch-term').val() != "")
           $('#search-form').submit();  
      }); 
	  
	   $('header.expert').addClass('nobackground');
			 var div = $('.easier_course');
			 var start = 100;
			 console.log(start);
			 $.event.add(window, "scroll", function() {
                var p = $(window).scrollTop();
				if(p>start) {
	          $('header.expert').removeClass('nobackground');
					  $('header.expert').addClass('hasbackground');
				} else {
			
					  $('header.expert').removeClass('hasbackground');
					  $('header.expert').addClass('nobackground');
				}
       });
	  
	});
</script> 
