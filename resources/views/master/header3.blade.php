<script type="text/javascript">  @php ( $count++ ) @php ( $count++ )
function chgcolor()
{
    $('#notifyarea').removeClass('badge-danger');
}
</script>
<style type="text/css">
.req_title:before {color: #729F34;font-size: 26px;left: 0;position: absolute;top: 21px;}
.req_details .req_title {border-bottom: 1px solid #DEDEDE;border-radius: 0;cursor: pointer;margin: 0;padding: 10px 10px 10px 0px;position: relative;}
.bx-default-pager{display: none;}
.notification-container {
    overflow:auto;
    max-height:500px;  
}
</style>
<div class="menu navbar navbar-inverse navbar-fixed-top">
    <div class="header clsClearfix">    
        <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>  
            <div class="nav-collapse collapse clearfix">
                <ul class="header_menu nav pull-right clearfix">
                <li><a href="{{ url('course') }}">{{ t('Course') }}</a></li>
                @if(Auth::check() == false)
                        <li><a href="{{ url('login') }}">{{ t('Login') }}</a></li>
                        <li><a href="{{ url('registration') }}">{{ t('Register') }}</a></li>
                        @else
                        <li><a href="{{ url(Auth::user()->username.'/myCourse') }}">My Courses</a></li>
                        <li class="bell_top dropdown"><a href="#" class="menudroplist dropdown-toggle" data-toggle="dropdown"><!-- {{ t('Notifications') }}--><i class="icon-bell"></i>
                        @if(count(unreadNotifications()) > 0)
                        <span class="badge badge-danger" onclick="chgcolor()" id="notifyarea">{{ count(unreadNotifications()) }}</span>
                        @elseif(numberOfNotifications() > 0)
                        <span class="badge" id="notifyarea">{{ numberOfNotifications() }}</span>
                        @endif
                        </a>
                            <ul class="dropdown-menu NotificationMenu span4">
                                <li class="notificationHeader">Notifications</li>
                                                    <li class="divider"></li>
                                @if(numberOfNotifications() > 0)
                                    <div class="notification-container">
                                    @foreach(unreadNotifications() as $notice)
                                        <li><div class="media">
                                            <a class="pull-left" href="#">
                                                <img class="media-object" alt="{{{ $notice->user->fullname }}}" src="{{ avatar($notice->user->avatar,30,30) }}">
                                            </a>
                                            <div class="media-body">
                                                <a href="{{ url('user/'.$notice->user->username) }}">{{{ $notice->user->fullname }}}</a>
                                                    <span class="msg-time pull-right"><i class="glyphicon glyphicon-time"></i>
                                                        <span><abbr class="timeago" title="{{ date(DATE_ISO8601,strtotime($notice->created_at)) }}">{{ date(DATE_ISO8601,strtotime($notice->created_at)) }}</abbr>&nbsp;</span>
                                                    </span>
                                                @if($notice->type == 'follow')
                                                    <p class="NotificationMsg">Started Following you</p>
                                                @elseif($notice->type == 'comment')
                                                    <p class="span3 NotificationMsg">Commented on your course <a href="{{ url('course/'.$notice->course->course_id.'/'.$notice->course->slug) }}">{{{ ucfirst($notice->course->course_title) }}}</a>
                                                    </p>
                                                    <a class="pull-right" href="#">
                                                        <img src="{{ asset(zoomCrop('uploads/course/'.$notice->course->image,75,75))}}" alt="{{ $notice->course_title }}">
                                                    </a>
                                                @elseif($notice->type == 'forum')
                                                    <p class="span3 NotificationMsg">Anwsered on your question <a href="{{ url('forum/'.$notice->forum->forum_id.'/'.$notice->forum->slug) }}">{{{ ucfirst($notice->forum->forum_topic) }}}</a></p>
                                                @elseif($notice->type == 'like')
                                                    <p class="span3 NotificationMsg">Liked your course <a href="{{ url('course/'.$notice->course->course_id.'/'.$notice->course->slug) }}">{{{ ucfirst($notice->course->course_title) }}}</a>
                                                    </p>
                                                    <a class="pull-right" href="#">
                                                        <img src="{{ asset(zoomCrop('uploads/course/'.$notice->course->image,75,75))}}" alt="{{ $notice->course_title }}">
                                                    </a>
                                                @elseif($notice->type == 'reply')
                                                    <p class="span3 NotificationMsg">Replied on your comment <a href="{{ url('course/'.$notice->course->course_id.'/'.$notice->course->slug) }}">{{{ ucfirst($notice->course->course_title) }}}</a>
                                                    </p>
                                                    <a class="pull-right" href="#">
                                                        <img src="{{ asset(zoomCrop('uploads/course/'.$notice->course->image,75,75))}}" alt="{{ $notice->course_title }}">
                                                    </a>
                                               <!--  @elseif($notice->type == 'follow')
                                                    <p>Started Following Your</p> -->
                                                @endif
                                            </div>
                                        </div></li>
                                        <li class="divider"></li>
                                    @endforeach
                                    </div>
                                @endif
                                <li><a class="AllNotification" target="_blank" href="{{ url('notifications') }}">View all notifications</a></li>
                            </ul>
                        </li>
                    <li class="dropdown">
                        <a href="#" class="menudroplist dropdown-toggle" data-toggle="dropdown">
                            <span><img src="{{ avatar(Auth::user()->avatar,30,30) }}"></span>
                            <b class="subject_arrow caret"></b>
                        </a>
                            
                        <ul class="dropdown-menu menu_wd">
                            <li><span class="userimg_width"><img src="{{ avatar(Auth::user()->avatar,90,90) }}"></span>
                                <span class="userimg_right">
                                    <span class="" >
                                    {{{ Auth::user()->fullname }}} </span>
                                    <div class="clearfix"></div>
                                    <div class="user_icons clearfix">
                                        <a href="{{ url('user/'.Auth::user()->username) }}"><i class="icon-user" title="View Profile"></i></a>
                                    <a href="{{ url('settings') }}"><i class="icon-cog" title="Edit Profile"></i></a>
                                    </div>
                                </span>
                            </li>
                            <li><a href="{{ url(Auth::user()->username.'/myCourse') }}"><i class="fa fa-home fa-fw"></i><span>My Courses</span></a></li>
                            <li><a href="{{ url('user/'.Auth::user()->username) }}"><i class="fa fa-pencil fa-fw"></i><span>{{ t('My Profile') }}</span></a></li>
                            <li class="edit_tutorial"><a href="{{ url('credits') }}"><i class="fa fa-money"></i><span>{{ siteSettings('siteName') }}</span> {{ t('credits') }} $ {{getUserCredit()}}</a></li>
                            <li><a href="{{ url('settings') }}"><i class="fa fa-pencil fa-fw"></i><span>{{ t('Profile Settings') }}</span></a></li>
                            <li class="signoutblocks"><a href="{{ url('logout') }}"><i class="fa fa-power-off"></i><span>{{ t('Logout') }}</span></a></li>
                        </ul>
                    </li>
                    @endif
                </ul>
                
                <ul class="header_menu nav pull-left">
                    <li class="dropdown">
                        <a href="#" class="menudroplist dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-list bell_top"></i>
                        {{ t('Categories') }}</a>
                        <ul class="dropdown-menu ul_width">
                            @foreach(siteCategories() as $category)
                            <li><a href="{{ url( 'category/'.$category->slug) }}">{{ $category->name }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
                
                <form class="search_right span3" role="search" method="GET" action="{{ url('search') }}">
                        <div class="search clsClearfix">
                        <div class="search_text">
                            <input class="stext" type="text" placeholder="search" name="q" id="srch-term"> 
                        </div>
                        <div class="search_button">
                        <button class="sbutton" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
                
                <div class="logo">
                  <h1><a href="{{url()}}">  <img src="{{ url('/') }}/static/img/logo.png" alt=""/> </a></h1>
                </div>
            </div>
        </div>
    </div>
</div> 
<!--header End-->

<!--slider-->
@if(Request::segment(1) == '')

{{ siteSettings('Tutorial-Home') }}

@elseif(Request::segment(1) == 'login')

<div class="reg_banner">
    <div class="container">
        <div class="row-fluid">
            <div class="span12 align_reg">
                <div class="signup">{{ t('Login') }}
                    <div class="join_tutorial"> Join the Tutorial Plus Community  </div>
                </div>
            </div>
        </div>
    </div>
</div>

@elseif(Request::segment(1) == 'password')

<div class="reg_banner">
    <div class="container">
        <div class="row-fluid">
            <div class="span12 align_reg">
                <div class="signup">Reset pwd
                    <div class="join_tutorial"> Join the Tutorial Plus Community</div>
                </div>
            </div>
        </div>
    </div>
</div>

@elseif(Request::segment(1) == 'registration')

<div class="reg_banner">
    <div class="container">
    <div class="row-fluid">
        <div class="span12 align_reg">
            <div class="signup">Signup
                <div class="join_tutorial"> Join the Tutorial Plus Community  </div>
            </div>
        </div>
        </div>
    </div>
</div>

@elseif(Request::segment(1) == 'course' AND Request::segment(2) != '')

<div class="banner_bg">
    <div class="container">
        <div class="row-fluid">
        @if(checkPurchase($course->course_id) == 0)
            <div class="span12 align_course">
                <div class="image_course"><img src="{{ url().'/uploads/course/'.$course->image }}" alt="{{ $course->course_title }}" class="img-thumbnail"></div>
                <div class="video_course">
                @if(file_exists(public_path()."/uploads/videos/".$course->video) && $course->video!='')
                <video id='my_video' class='video-js vjs-default-skin' controls preload='auto' width='470' height='264' data-setup='{}'>
                <source src='{{url("/uploads/videos/".$course->video)}}' type='{{$course->video_type}}'></video>
                @endif
                </div>
            </div>
        @endif
        </div>
    </div>
</div>

@elseif(Request::segment(1) == 'CreateCourses')
 
<div class="createcourse_banner">
    <div class="container">
        <div class="row-fluid">
            <div class="span12 align_courseimage">
                <div class="teacher_details">
                    <div class="teacher_image">
                        <img src="{{ asset(zoomCrop('uploads/course/'.$course->image,150,90))}}" alt="{{ $course->course_title }}" class="img-thumbnail my_coursesblockss">
                    </div>
                </div>
                <div class="teacher_course">
                    <div class="course_name">
                        <a>{{ $course->course_title }}</a>
                    </div>            
                </div>
                <div class="course_button pull-right">
                    <div class="course_pub preview_btnblocks">
                        <a class="PreviewButton course_preview" target="_blank" href="{{url('course').'/'.$course->course_id.'/'.$course->slug}}">Preview</a>
                    </div>
                    <div class="course_pub">
                        <input type="button" onclick="return validate_click($(this));" class="course_publish" id="{{ $course->course_id }}" value="PUBLISH">
                        <a data-toggle="modal" class="course_publish_trigger course_publish" style="display:none" href="#PublisCourse">PUBLISH</a>
                    </div>
                </div>
            </div>
        </div> 
    </div>   
</div>

@else
<div class="reg_banner_empty"></div>
@endif
<!--slider ends-->