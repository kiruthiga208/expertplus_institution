<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{{ $title }}} - {{ siteSettings('siteName') }}</title>
    @yield('metaDescription')
    <link rel="shortcut icon" href="{{ siteSettings('favIcon') }}" type="image/x-icon"/>
 <!-- css styles -->
   {{ HTML::style('static/css/front/bootstrap.css') }}
    <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css"/>
   
     <!--IE-->
    <!--[if lte IE 8]>
        {{ HTML::style('static/css/front/ie8.css') }}
        {{ HTML::style('static/css/ie8.css') }}
    <![endif]-->

    {{ HTML::style('static/css/front/bootstrap-responsive.css',array('id'=>"colors")) }}
    {{ HTML::style('static/css/front/style.css') }}
    {{ HTML::style('static/css/front/common.css') }}
    {{ HTML::style('static/css/front/media.css') }}
    {{ HTML::style('static/css/front/tooltip.css') }}
    {{ HTML::style('static/css/video-js.css') }}
    {{ HTML::style('static/css/dasky.min.css') }}
    {{ HTML::style('http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic') }}
    {{ HTML::style('http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic') }}
    {{ HTML::style('http://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700') }}
    <!--css-->

  <!--   HTML5 shim, for IE6-8 support of HTML5 elements 
    [if lt IE 9]>
      {{ HTML::script('static/js/html5shiv.js') }}
    <![endif]
   Fav and touch icons -->

</head>
<style type="text/css">
    .pagination > li.active:next{
        padding: 20px;
    }
    #infscr-loading {
    left: 50%;padding: 0 0 20px;position: relative;
    }
    .ReadonlyRating.startvalue > img {
    width: 15px;
}
</style>
<!-- Header-->
<body>
@include('master/header')
@yield('carousel')

@include('master/notices')
@yield('content')

@include('master/footer')
{{ HTML::script('static/js/jquery.js') }}
{{ HTML::script('static/js/jquery.min.js') }}

{{ HTML::script('static/js/bootstrap.min.js') }}
{{ HTML::script('static/js/front/bootstrap-collapse.js') }}

{{ HTML::script('static/js/jquery-ui.min.js') }}
{{ HTML::script('static/js/jquery.jscroll.min.js') }}
{{ HTML::script('static/js/jquery.timeago.js') }}
{{ HTML::script('static/js/front/tooltip.js') }}

{{ HTML::script('static/js/video.js') }}
{{ HTML::script('static/js/front/jquery.bxslider.min.js') }}

{{ HTML::script('static/js/dasky.eval.js') }}
{{ HTML::script('static/js/jmpress.min.js') }}
{{ HTML::script('static/js/jquery.raty.min.js') }}

{{ HTML::script('static/js/custom.js') }}
{{ HTML::script('static/js/bootstrap-datepicker.js') }}
{{ HTML::script('static/js/jquery.lazyload.min.js') }}
{{ HTML::script('static/js/multiupload.js') }}

<input type="hidden" id="getSiteUrl" value="{{url()}}">
<input type="hidden" id="FollowUrl" value="{{url('follow')}}">
<noscript><meta http-equiv="refresh" content="0;url={{ URL::TO('nojs.php') }}"></noscript>
<script type="text/javascript">
    $(document).ready(function(){
        $("img.lazy").lazyload({ effect : "fadeIn" });
        $('.MoreFromUser, .MoreFromSite').bxSlider({slideWidth: 75,minSlides: 2,maxSlides: 3,slideMargin: 10});
        $('.LatestCourse').bxSlider();
        $('.req_details .req_title').click(function(){
        if($(this).hasClass('faqopen')){$(this).removeClass('faqopen');$(this).addClass('faqclose');}
        else{$(this).addClass('faqopen');$(this).removeClass('faqclose');}$(this).children('.req_ans').slideToggle('slow');});
        $('.HomeScroll').infinitescroll({navSelector  : "ul.pagination",nextSelector : "ul.pagination a:first",itemSelector : ".HomeScroll .business_content",loading: {msgText: "<em>Loading...</em>", finishedMsg: 'That\'s It :)',img: '{{url().'/static/img/loading.gif'}}'}},function() {$("img.lazy").lazyload();$('.ReadonlyRating').raty({score: function() {return $(this).attr('data-score');},readOnly: true,starOff : "{{url().'/static/img/star-off.png'}}",starOn: "{{url().'/static/img/star-on.png'}}",starHalf : "{{url().'/static/img/star-half.png'}}"});});
        $('#rating-div').raty({score: function() {return $(this).attr('data-score');},width: 150,target : '#review_hint',starOff : "{{url().'/static/img/star-off.png'}}",starOn: "{{url().'/static/img/star-on.png'}}",starHalf : "{{url().'/static/img/star-half.png'}}"});
        $('.ReadonlyRating').raty({score: function() {return $(this).attr('data-score');},readOnly: true,starOff : "{{url().'/static/img/star-off.png'}}",starOn: "{{url().'/static/img/star-on.png'}}",starHalf : "{{url().'/static/img/star-half.png'}}"});

        // Rating 

                $('#review-save').click(function(){
                var title = $('.review-box #review-title').val();
                var description = $('.review-box #review-description').val();
                var rating = $('.review-box input[name=score]').val();
                var Course_id = $('.review-box #rating-courseId').val();
                if(title == ''){
                    $('.review-box #review-title').css('border','1px solid red');
                }
                else{
                    $('.review-box #review-title').css('border','1px solid #CCCCCC');
                    if(rating == 0){alert('Please Give Rating');}else{
                        $.ajax({
                        type: "POST",
                        url: "{{url('review')}}",
                        data: {title:title,description:description,rating:rating,Course_id:Course_id},
                        success: function (data) {
                            location.reload();
                        }
                        });
                    }
                }
                });
        // Rating

        // My Course

            $('#mycourseFilter').change(function(){
                $(this).parent('form').submit();
            });
        });

        //My Course

        // Tap

            $('#Course_Tabs a').click(function (e) {
              e.preventDefault()
              $(this).tab('show')
            });

        // Tap

        // Take Course

            eval(function(p,a,c,k,e,d){e=function(c){return c};if(!''.replace(/^/,String)){while(c--){d[c]=k[c]||c}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('$(0(){$(\'#1\').2()});',3,3,'function|dasky|Dasky'.split('|'),0,{}))
            $(".dsk-content .video-js").bind("ended", function() {
               $('.dsk-next').trigger('click');
            });

        // Take Course 

    function resetTabs(){
        $("#content_new > div,#content_news > div").hide(); //Hide all content
        $("#tabs_new a,#tabs_news a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content_new > div,#content_news > div").hide(); // Initially hide all content
        $("#tabs_new li:first a,#tabs_news li:first a").attr("id","current"); // Activate first tab
        $("#content_new > div:first,#content_news > div:first").fadeIn(); // Show first tab content
        
        $("#tabs_new a,#tabs_news a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs_new li,#tabs_news li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })();
</script>
@yield('extrafooter')

</body>
</html>