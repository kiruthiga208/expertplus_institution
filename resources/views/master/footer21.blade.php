 <!--Footer-->
<div class="footer clsClearfix">
<div class="about_company container">
<div class="row-fluid">
<div class="span12">
<div class="company span3">
<h3>ABOUT OUR COMPANY</h3>
{{ siteSettings('about') }}
</div>

<div class="course span3">
<h3>LATEST COURSE</h3>

<ul class="course_menus">
<?php $i=1; ?>
@foreach(getLastestCourse() as $course)
	<li class="clearfix latest_coursess">
	<div class="course_image">
	<a href="{{url('course/'.$course->course_id.'/'.$course->slug) }}" class="footerimgsize"><img id="image-3" data-original="{{ asset(zoomCrop('uploads/course/'.$course->image,44,44)) }}" class="thumb lazy" alt="{{{ $course->course_title }}}"/>
        <noscript><img src="{{ asset(zoomCrop('uploads/course/'.$course->image,44,44))}}"></noscript></a>
	</div>

	<div class="course_top">
	<a href="{{url('course/'.$course->course_id.'/'.$course->slug) }}" title="{{$course->course_title}}"> {{{ Str::limit(ucfirst($course->course_title),30) }}}</a><div class="jobs"><a href="#">by {{ ucfirst($course->user->fullname) }}</a></div>
	</div>
	</li>
	<?php if($i==3) { ?>
	<li class="clearfix latest_coursess"><div class="course_top" style="padding-left:50px"><a href="{{url() .'/latestcourse'}}">More...</a></div></li>
	<?php break; } $i++; ?>
@endforeach
</ul>
</div>

<div class="course span3">
<h3>MOST VIEWED</h3>
<ul class="course_menus">
<?php $m=1; ?>
@foreach(getMostViewedCourse() as $course)

	<li class="clearfix latest_coursess">
	<div class="course_image">
	<a href="{{url('course/'.$course->course_id.'/'.$course->slug) }}" class="footerimgsize"><img id="image-3" data-original="{{ asset(zoomCrop('uploads/course/'.$course->image,44,44)) }}" class="thumb lazy" alt="{{{ $course->course_title }}}"/>
        <noscript><img src="{{ asset(zoomCrop('uploads/course/'.$course->image,44,44))}}"></noscript></a>
	</div>

	<div class="course_top">
	<a href="{{url('course/'.$course->course_id.'/'.$course->slug) }}" title="{{$course->course_title}}"> {{{ Str::limit(ucfirst($course->course_title),30) }}}</a><div class="jobs"><a href="#">by {{ ucfirst($course->user->fullname) }}</a></div>
	</div>
	</li>
	<?php if($m==3) { ?>
	<li class="clearfix latest_coursess"><div class="course_top" style="padding-left:50px"><a href="{{url() .'/mostViewed'}}">More...</a></div></li>
	<?php break; } $m++; ?>
@endforeach
</ul>

</div>

<div class="new_course span3">
<h3>Featured Course</h3>

<ul class="course_menus">
<?php $f=1;?>
@foreach(getFeaturedCourse() as $course)
		
		<li class="clearfix latest_coursess">
		<div class="course_image">
		<a href="{{url('course/'.$course->course_id.'/'.$course->slug) }}" class="footerimgsize"><img id="image-3" data-original="{{ asset(zoomCrop('uploads/course/'.$course->image,44,44)) }}" class="thumb lazy" alt="{{{ $course->course_title }}}"/>
	        <noscript><img src="{{ asset(zoomCrop('uploads/course/'.$course->image,44,44))}}"></noscript></a>
		</div>

		<div class="course_top">
		<a href="{{url('course/'.$course->course_id.'/'.$course->slug) }}" title="{{$course->course_title}}"> {{{ Str::limit(ucfirst($course->course_title),30) }}}</a><div class="jobs"><a href="#">by {{ ucfirst($course->user->fullname) }}</a></div>
		</div>
		</li>
		<?php if($f==3) { ?>
		<li class="clearfix latest_coursess"><div class="course_top" style="padding-left:50px"><a href="{{url() .'/featured'}}">More...</a></div></li>
		<?php break; } $f++; ?>
@endforeach
</ul>
</div>
</div>
</div>
</div>

		{{ siteSettings('Footer') }}
   </div>
  
   <div class="copyright">
		<div class="container">
			<div class="all_rights">&copy; {{ date("Y") }} {{ siteSettings('siteName') }}, All rights reserved.</div>
			<div class="policy">	 
			   	<ul class="policy_menu"> 
					<li><a href="{{ url('privacy') }}">{{ t('Privacy Policy') }}</a> </li>
					<li><a href="{{ url('tos') }}">{{ t('Terms') }}</a></li>
					<li><a href="{{ url('faq') }}">{{ t('FAQ') }}</a></li>
					<li><a href="{{ url('blog') }}">{{ t('blog') }}</a></li>
					<li><a href="{{ url('forum') }}">{{ t('forum') }}</a></li>
					<li><a href="{{ url('about') }}">{{ t('About Us') }}</a></li>
		        	@include('master/language')
			   	</ul>
			</div>  
		</div>
	</div>
<!--End Footer-->

</body>
</html>
