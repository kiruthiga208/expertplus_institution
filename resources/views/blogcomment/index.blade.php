@extends('layouts.app')

@section('content')
@php usort($tableGrid, "SiteHelpers::_sort") @endphp
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}"> {{ Lang::get('core.dashboard') }} </a></li>
        <li class="active">{{ $pageTitle }}</li>
      </ul>	  
	  
    </div>
	
	
	<div class="page-content-wrapper m-t">	 	

<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5>
<!-- <div class="sbox-tools" >
		@if(Session::get('gid') ==1)
			<a href="{{ URL::to('bsetec/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
		@endif 
		</div> -->
	</div>
	<div class="sbox-content blog-comment"> 	
	    <div class="toolbar-line ">
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('blogcomment/update') }}" class="tips btn btn-sm btn-white"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa fa-plus-circle "></i>&nbsp;{{ Lang::get('core.btn_create') }}</a>
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="bsetecDelete();" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-minus-circle "></i>&nbsp;{{ Lang::get('core.btn_remove') }}</a>
			@endif 		
			@if($access['is_excel'] ==1)
			<a href="{{ URL::to('blogcomment/download') }}" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-download"></i>&nbsp;{{ Lang::get('core.btn_download') }} </a>
			@endif			
		 
		</div> 		

	
	@include('blogadmin/tab')
	 {!! Form::open(array('url'=>'blogcomment/delete/', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"> {{ Lang::get('core.no') }} </th>
				<th></th>
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th width="70" >{{ Lang::get('core.btn_action') }}</th>
			  </tr>
        </thead>

        <tbody>
			<tr id="bsetec-quick-search" >
				<td class="number"> # </td>
				<td> <input type="checkbox" class="checkall" /> </td>
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
					@php ($tfiled =$t['field'])
					<td>						
						{!! SiteHelpers::transForm('', $tableForm) !!}								
					</td>
					@endif
				@endforeach
				<td >
				<input type="hidden"  value="Search">
				<button type="button"  class=" do-quick-search btn btn-xs btn-info"> {{ Lang::get('core.GO') }}</button></td>
			 </tr>	        
						
            @foreach ($rowData as $row)
                <tr>
					<td width="30"> {{ ++$i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="id[]" value="{{ $row->commentID }}" />  </td>									
				 @foreach ($tableGrid as $field)
				 @php ($fieldname=$field['field'])
					 @if($field['view'] =='1')
					 <td>					 
					 	@if($field['attribute']['image']['active'] =='1')
							{!! SiteHelpers::showUploadedFile($row->$fieldname,$field['attribute']['image']['path']) !!}
						@else	
							@php ( $conn = (isset($field['conn']) ? $field['conn'] : array() ) )
							@if($field['field']=='comment')
							<p style="display: block;overflow: hidden;text-overflow: ellipsis;width: 300px;">{!! $row->comment !!}</p>
							@else
							{!! SiteHelpers::gridDisplay($row->$fieldname,$field['field'],array()) !!}	
							@endif
						@endif						 
					 </td>
					 @endif					 
				 @endforeach
				 <td>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('blogcomment/show/'.$row->commentID.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
						@if($access['is_edit'] ==1)
						<a  href="{{ URL::to('blogcomment/update/'.$row->commentID.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif
												
					
				</td>				 
                </tr>
				
            @endforeach
              
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	@include('footer')
	</div>
</div>	
	</div>	  
</div>	
<script>
$(document).ready(function(){
    $('body').addClass('invoice-block');
	$('.do-quick-search').click(function(){
		$('#bsetecTable').attr('action','{{ URL::to("blogcomment/multisearch")}}');
		$('#bsetecTable').submit();
	});
	
});	
</script>		
@stop