@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('blogcomment?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('blogcomment?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('blogcomment/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	


	@php $prefix = \bsetecHelpers::getdbprefix();   @endphp
	<table class="table table-striped table-bordered" >
		<tbody>	
	
					<tr>
						<td width='30%' class='label-view text-right'> {!! Lang::get('core.Post_By') !!}</td>
						@php $userdetails = \bsetecHelpers::getuserinfobyid($row->user_id); @endphp 

						<td>{{ $userdetails->username }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'> {!! Lang::get('core.Post_Title') !!}</td>

							@php $blogdetails = \bsetecHelpers::getbloginfobyid($row->blogID); @endphp 
						
						<td>{{ $blogdetails->title }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! Lang::get('core.Comment') !!}</td>
						<td>{{ $row->comment }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'> {!! Lang::get('core.Created') !!}</td>
						<td>{{ $row->created }} </td>
						
					</tr>
				
		</tbody>	
	</table>    
	
	</div>
</div>	

	</div>
</div>
	  
@stop