@extends('layouts.app')
 
@section('content')
@php usort($tableGrid, "SiteHelpers::_sort") @endphp
@if (defined('CNF_CURRENCY'))
        @php $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) @endphp
@endif
<style>
.view_block {
  list-style: outside none none;
  margin: 0;
  padding: 0;
}
.view_block li.dropdown i, .view_block li.dropdown b.caret {
  color: #676a6c;
  font-size: 12px;
}
.view_block li.dropdown > a {
  border: 1px solid #eee;
  display: inline-block;
  margin: 2px 0;
  padding: 2px 6px;
}
.view_block li a {
  margin-bottom: 3px;
  width: 37px;
}
.table-responsive.course-listing-admin .view_block li.dropdown ul {
  top: -40px;
}

.view_block li.dropdown ul {
  left: auto;
  right: 0;
}
</style>
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.dashboard') }}</a></li>
        <li class="active">{{ $pageTitle }}</li>
      </ul>	  
	  
    </div>
	
	
	<div class="page-content-wrapper m-t">	 	

<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5>
<!-- <div class="sbox-tools" >
		@if(Session::get('gid') ==1)
			<a href="{{ URL::to('bsetec/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
		@endif 
		</div> -->
	</div>
	<div class="sbox-content"> 	
	    <div class="toolbar-line ">
			<!-- @if($access['is_add'] ==1)
	   		<a href="{{ URL::to('courselisting/update') }}" class="tips btn btn-sm btn-white"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa fa-plus-circle "></i>&nbsp;{{ Lang::get('core.btn_create') }}</a>
			@endif  --> 
			<a href="javascript://ajax" class="tips btn btn-sm btn-white updatestatus" title="Approve" data-type="1">
			<i class="fa fa-check"></i>&nbsp;{{ Lang::get('core.approve') }}</a>

			<a href="javascript://ajax" class="tips btn btn-sm btn-white updatestatus" title="Unapprove" data-type="2">
			<i class="fa fa-times"></i>&nbsp;{{ Lang::get('core.unapprove') }}</a>

			@if($access['is_remove'] ==1)
			<a href="javascript://ajax" class="tips btn btn-sm btn-white updatestatus" title="{{ Lang::get('core.btn_remove') }}" data-type="0">
			<i class="fa fa-minus-circle "></i>&nbsp;{{ Lang::get('core.btn_remove') }}</a>
			@endif 		
			<!-- @if($access['is_excel'] ==1)
			<a href="{{ URL::to('courselisting/download') }}" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-download"></i>&nbsp;{{ Lang::get('core.btn_download') }} </a>
			@endif		 -->	
		 
		</div> 		

	
	
	 {!! Form::open(array('url'=>'courselisting/delete/', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )) !!}
	 {!! Form::hidden('types', '',array('id'=>'actiontype'))  !!}
	 <div class="table-responsive course-listing-admin" style="min-height:300px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"> {{ Lang::get('core.no') }} </th>
				<th> </th>
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
					@if($t['label'] == 'Description')
					<th>{{ "No of students" }} </th>
					@else
					<th>{{ $t['label'] }}</th>
					@endif
						
					@endif
				@endforeach
				<!-- <th>{{ Lang::get('core.logo') }}</th>
				<th>{{ Lang::get('core.Title') }}</th>
				<th>{{ Lang::get('core.Created_By') }}</th>
				<th>{{ Lang::get('core.pricing') }}</th>
				<th>{{ Lang::get('core.students') }}</th>
				<th>{{ Lang::get('core.approved') }}</th>
				<th>{{ Lang::get('core.Created_At') }}</th> -->

				<th width="70" >{{ Lang::get('core.btn_action') }}</th>
			  </tr>
        </thead>

        <tbody>
			<tr id="bsetec-quick-search" >
				<td class="number"> # </td>
				<td> <input type="checkbox" class="checkall" /> </td>
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
					<td>	
					@if($t['field'] == "user_id")
					<select name="user_id" class="form-control">
						<option value=""> {{ Lang::get('core.select_user') }} </option>
						@foreach ($user_lists as $search)
							<option value="{{ $search->userid }}">{{ $search->user_id }}</option>
						@endforeach
					</select>
					@elseif($t['field'] == "image")

					@elseif($t['field'] == "approved")
					{!! Form::select('approved', array('' => '--select--','0' => 'Waiting','1' => 'Approved','2'=>'Unapproved'),'',array('class'=>'form-control')); !!}
					@elseif($t['field'] == "pricing")
					
					@else
						{!! SiteHelpers::transForm($t['field'] , $tableForm) !!}
					@endif							
					</td>
					@endif
				@endforeach
				<td >
				<input type="hidden"  value="Search">
				<button type="button"  class=" do-quick-search btn btn-xs btn-info"> {{ Lang::get('core.GO') }} </button></td>
			 </tr>	        
						
            @foreach ($rowData as $row)
                <tr>
					<td width="30"> {{ ++$i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="id[]" value="{{ $row->course_id }}" />  </td>									
				 @foreach ($tableGrid as $field)
				 @php 
					$fname=$field['field'];
				  @endphp
					 @if($field['view'] =='1')
					 <td>					 
					 	@if($field['attribute']['image']['active'] =='1')
							{!! SiteHelpers::showUploadedFile($row->$fname,$field['attribute']['image']['path']) !!}
						@else	
							@php $conn = (isset($field['conn']) ? $field['conn'] : array() ) @endphp
							@if($field['field']=='image')
							 <img src="{{ \bsetecHelpers::getImage($row->image,'small') }}" width="50" border="0" class="img-thumbnail">
							
							@elseif($field['field']=='pricing')
							<?php if($row->$fname==NULL ||$row->$fname=='0'){ $row->$fname = '<span class="label label-success">'.Lang::get('core.free').'</span>'; } else{ $row->$fname = $currency.' '.number_format($row->$fname,2); }?>
							{!! SiteHelpers::gridDisplay($row->$fname,$field['field'],$conn) !!}
							@elseif($field['field']=='description')
							<?php $students = \SiteHelpers::getstudentslist($row->course_id);
								  $row->$fname= $students; ?>
							{!! SiteHelpers::gridDisplay($row->$fname,$field['field'],$conn) !!}	
							@elseif($field['field']=='approved')
							<?php if($row->$fname=='1'){ $row->$fname = '<span class="label label-success">'.Lang::get('core.approved').'</span>'; } else if($row->$fname=='2'){$row->$fname = '<span class="label label-danger">'.Lang::get('core.unapproved').'</span>';} else if($row->$fname=='0'){ $row->$fname = '<span class="label label-danger">'.Lang::get('core.waiting_for_approval').'</span>'; }?>
							{!! SiteHelpers::gridDisplay($row->$fname,$field['field'],$conn) !!}		
							@else
							{!! SiteHelpers::gridDisplay($row->$fname,$field['field'],$conn) !!}	
							@endif

				
						@endif						 
					 </td>
					 @endif					 
				 @endforeach
				 <td>
                 <ul class="view_block clearfix">
                 <li>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('courseview/'.$row->course_id.'/'.$row->slug)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
                        </li>
                        <li>
						<a href="{{ URL::to('courselisting/feedback/'.$row->course_id)}}" class="tips btn btn-xs btn-white" title="Feedback"><i class="icon-bubble6"></i></a>
                        </li>
                        <li>
                      
						<a href="#" data-toggle="modal" data-target="#myModal" @if($row->test_video != '') data-video="{{ $row->test_video }}" @else data-video="{{ $row->youtube_id }}" @endif class="tips btn btn-xs btn-white test-video" title="Test Video Preview"><i class="fa fa-play-circle"></i></a>
						<!-- @if($access['is_edit'] ==1)
						<a  href="{{ URL::to('courselisting/update/'.$row->course_id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif -->
                        </li>
						<li class="dropdown"><a href="javascript::" data-toggle="dropdown" class="dropdown-toggle"> <i class="fa fa-eye"></i><b class="caret"></b></a>
				    		<ul class="dropdown-menu" style="display: none;">
					        	<li><a target="_blank" href="{!! url('course-preview/'.$row->course_id.'/'.$row->slug.'?PreviewMode=instructor') !!}">{{ Lang::get('core.as_nstructor') }}</a></li>
					            <li><a target="_blank" href="{!! url('course-preview/'.$row->course_id.'/'.$row->slug.'?PreviewMode=guest') !!}">{{ Lang::get('core.as_guest') }}</a></li>        
					        </ul>
						</li>
                        </ul>
					
				</td>				 
                </tr>
				
            @endforeach
              
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	@include('footer')
	</div>
</div>	
	</div>	  
</div>	
<script>
$(document).ready(function(){
	$('.test-video').click(function(){
		var video_id =$(this).attr('data-video');
		$('.video_preview').html("");
		$('.video_id').val(video_id);
		$.ajax({
            url:"{!! url('courselisting/testvideo') !!}",
            type:'GET',
            data: {id:video_id},
            success: function(data)
            {   
            	if(data.length > 0){
            	var title = data[0].video_title;
            	var  titlee = "uploads/videos/"+data[0].video_title+".ogv";
                var video = '<video width="400" controls><source src="{{asset("uploads/videos")}}/'+data[0].video_title+'.mp4" type="video/mp4">\
  				<source src="{{asset("uploads/videos")}}/'+data[0].video_title+'.ogv" type="video/ogg"><source src="{{asset("uploads/videos")}}/'+data[0].video_title+'.webm" type="video/webm"></video>';
				}else if(video_id != '') {
					var video = '  <div id="div_video"> </div>';
                                $.post( "{!! url('api_youtube/getvideo.php') !!}", { videoid: video_id, type: "Download" })
                                .done(function( data ) {
                                   var json = $.parseJSON(data);
                                   var urls = "{!! url('api_youtube') !!}/"+json.url;
                                   document.getElementById('div_video').innerHTML = '<video autoplay controls id="video_ctrl" width="400px"><source src="'+urls+'" type="video/mp4"></video>';
                                   document.getElementById('video_ctrl').play();
                                });
                }else{                

					video ='<center>File Not Found</center>';
				}
				$('.video_preview').html(video);
            }
        });
	});

	$('.do-quick-search').click(function(){
		$('#bsetecTable').attr('action','{{ URL::to("courselisting/multisearch")}}');
		$('#bsetecTable').submit();
	});

	$(document).on('click','.updatestatus',function(){
		var status = $(this).data('type');
		$('#actiontype').val(status);

		if(status=='0' && $("input[name='id[]']:checked").length==0){
			alert('Please select atleast one checkbox!');
		}else if(status=='0' && $("input[name='id[]']:checked").length>0){
			if(confirm('{{ Lang::get('core.to_remove') }}'))
			{
					$('#bsetecTable').submit();// do the rest here	
			}
		}else if($("input[name='id[]']:checked").length>0){
			$('#bsetecTable').submit();
		}else{
			alert('{{ Lang::get('core.select_checkbox') }}');
		}
		
	});

});	
</script>	
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content test_vid">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('core.test_video_preview') }}</h4>
     	<input type="hidden" name="vid" class="video_id" value="" />
      	</div>
        <div class="modal-body">
      <div class="video_preview">
      	</div>
        </div>
    </div>
  </div>
</div>	
@stop