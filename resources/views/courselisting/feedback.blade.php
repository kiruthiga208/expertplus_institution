@extends('layouts.app')

@section('content')
<link href="{{ URL::to('resources/views/courselisting/resources/style.css') }}" rel="stylesheet" />
<div class="page-content row">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-title">
			<h3> {{ Lang::get('core.course_feedback') }} </h3>
		</div>
		<ul class="breadcrumb">
			<li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
			<li><a href="{{ URL::to('courselisting?return='.$return) }}">{{ Lang::get('core.courselisting') }}</a></li>
			<li class="active"> {{ Lang::get('core.course_feedback') }} </li>
		</ul>

	</div>

	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
		<div class="sbox animated fadeInRight">
			<div class="sbox-title"> <h4> <i class="fa fa-table"></i> {{ Lang::get('core.course_feedback') }} </h4></div>
			<div class="sbox-content"> 	

				{!! Form::open(array('url'=>'courselisting/savefeedback/'.$courseid, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
				{!! Form::hidden('course_id', $courseid )  !!}
				{!! Form::hidden('redirects', URL::to('courselisting/removefeedback') )  !!}
				<div class="col-md-12">
					<fieldset>
						<!-- <legend> Feedback</legend> -->
						<div class="form-group  " >
							<label for="Course Id" class=" control-label col-md-2 text-left"> {{ Lang::get('core.comments') }} </label>
							<div class="col-md-10">
								{!! Form::textarea('comments', '',array('class'=>'form-control feedbackeditor', 'placeholder'=>'',   )) !!} 
							</div> 
						</div> 					
					</fieldset>
				</div>

				<div style="clear:both"></div>	
				
				<div class="form-group">
					<label class="col-sm-2 text-right">&nbsp;</label>
					<div class="col-sm-10">	
						<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
						<button type="button" onclick="location.href='{{ URL::to('courselisting?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  

				</div> 

				{!! Form::close() !!}
			</div>
		</div>	


		<div class="table-responsive">

			@php  $k = 0; @endphp
			@foreach($results as $key => $value)
			@if($value->feedback_type=='0') @php  $class=''; $clear='details'; @endphp @else @php $clear='details user-block clearfix'; $class='sec2'; @endphp @endif 

			<div class="usercomments {{$class}}">
				<div class="container">
					@if($value->feedback_type=='0')
					<div class="pull-right">
						<a href="javascript:void(0);" data-fid="{!! $value->feedback_id !!}" class="removecomments"><i class="fa fa-trash-o"></i></a>
					</div>
					@endif 
					<div class="row">
						<div class="col-sm-2 col-xs-12">
							<a href="{{ URL::to('profile/'.$value->username) }}">
							{!! SiteHelpers::customavatar($value->email,$value->user_id,'small') !!}
							</a>
						</div>
						<div class="col-sm-10 col-xs-12">
							<div class="{{$clear}}">
								<span class="user_name"><a href="{{ URL::to('profile/'.$value->username) }}">{!! $value->username !!}</a></span>
								<span class="time"> {!! SiteHelpers::changeFormat($value->created_at) !!} </span>
							</div>
							<div class=""><?php echo $value->comments; ?></div>
						</div>
					</div>
				</div>
			</div>
			@php  $k++; @endphp
			@endforeach
		</div>
	</div>	
</div>	
<script type="text/javascript" src="<?php echo e(URL::to('assets/bsetec/js/plugins/tinymce/jscripts/tiny_mce/tiny_mce.js')); ?>"></script>		 
<script type="text/javascript">
$(document).ready(function() { 
	tinymce.init({	
		mode : "specific_textareas",
		editor_selector : "feedbackeditor",
		theme : "advanced",
		theme_advanced_buttons1 : "bold,italic,underline,bullist,numlist",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		width : "100%",
		plugins : "paste",
		paste_text_sticky : true,
		setup : function(ed) {
			ed.onInit.add(function(ed) {
				ed.pasteAsPlainText = true;
			});
		}
	});

	$(document).on('click','.removecomments',function(){
		var datas = $(this).data('fid');
		var redirects = $('[name="redirects"]').val();
		var courseid   = $('[name="course_id"]').val();
		var r = confirm("{{ Lang::get('core.comment_remove') }}");
		if (r == true) {
			window.location.href = redirects+'/'+datas+'/'+courseid;
		}
	});

});
</script>		 
@stop