@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('courselisting?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('courselisting?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('courselisting/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
	
				<tr>
				<td width='30%' class='label-view text-right'> {{ Lang::get('core.course_id') }} </td>
				<td>{{ $row->course_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.User_Id') }}</td>
				<td>{{ $row->user_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.CatID') }}</td>
				<td>{{ $row->cat_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.lang_id') }}</td>
				<td>{{ $row->lang_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.Slug') }}</td>
				<td>{{ $row->slug }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.course_title') }}</td>
				<td>{{ $row->course_title }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.subtitle') }} </td>
				<td>{{ $row->subtitle }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.Description') }}</td>
				<td>{{ $row->description }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.Curriculum') }}</td>
				<td>{{ $row->curriculum }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.course_goal') }}</td>
				<td>{{ $row->course_goal }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.int_audience') }}</td>
				<td>{{ $row->int_audience }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.course_req') }}</td>
				<td>{{ $row->course_req }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.type') }}</td>
				<td>{{ $row->type }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.lang') }}</td>
				<td>{{ $row->lang }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.keywords') }}</td>
				<td>{{ $row->keywords }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.tags') }}</td>
				<td>{{ $row->tags }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.Image') }}</td>
				<td>{{ $row->image }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.video') }}</td>
				<td>{{ $row->video }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.test_video') }}</td>
				<td>{{ $row->test_video }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.video_type') }}</td>
				<td>{{ $row->video_type }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.Privacy') }}</td>
				<td>{{ $row->privacy }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.pricing') }}</td>
				<td>{{ $row->pricing }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.Survey') }}</td>
				<td>{{ $row->survey }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.course_status') }}</td>
				<td>{{ $row->course_status }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.course_level') }}</td>
				<td>{{ $row->course_level }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.manage_ins') }}</td>
				<td>{{ $row->manage_ins }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.is_featured') }}</td>
				<td>{{ $row->is_featured }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.rating_count') }}</td>
				<td>{{ $row->rating_count }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.approved') }}</td>
				<td>{{ $row->approved }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.con_analytic') }}</td>
				<td>{{ $row->con_analytic }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.eng_analytic') }}</td>
				<td>{{ $row->eng_analytic }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.Created_At') }}</td>
				<td>{{ $row->created_at }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.Updated_At') }}</td>
				<td>{{ $row->updated_at }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.Deleted_At') }}</td>
				<td>{{ $row->deleted_at }} </td>

				</tr>
				
		</tbody>	
	</table>    
	
	</div>
</div>	

	</div>
</div>
	  
@stop