@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('instructors?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	

		 {!! Form::open(array('url'=>'instructors/save/'.$row['id'].'?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		 {!! Form::hidden('id', $row['id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
		 {!! Form::hidden('user_id', $row['user_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
<div class="col-md-12">
			<fieldset><legend> instructors</legend>

							<div class="form-group  " >
								<label for="Username" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Username') }} <span class="asterisk "> * </span></label>
								<div class="col-md-6">
								  {!! Form::text('username', $row['username'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
								 </div> 
								 <div class="col-md-2">
								 	
								 </div>
							</div> 

							<div class="form-group  " >
								<label for="First Name" class=" control-label col-md-4 text-left"> {{ Lang::get('core.firstname') }}  <span class="asterisk "> * </span></label>
								<div class="col-md-6">
								  {!! Form::text('first_name', $row['first_name'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
								</div> 
								<div class="col-md-2">
									
								</div>
							</div> 					
							<div class="form-group  " >
								<label for="Last Name" class=" control-label col-md-4 text-left"> {{ Lang::get('core.lastname') }} <span class="asterisk "> * </span> </label>
								<div class="col-md-6">
								  {!! Form::text('last_name', $row['last_name'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
								</div> 
								<div class="col-md-2">
								 	
								</div>
							</div> 					
							<div class="form-group  " >
								<label for="Email" class=" control-label col-md-4 text-left"> {{ Lang::get('core.email') }} <span class="asterisk "> * </span></label>
								<div class="col-md-6">
								  {!! Form::text('email', $row['email'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'email'   )) !!} 
								</div> 
								<div class="col-md-2">
								 	
								</div>
							</div> 		

							<div class="form-group  " >
								<label for="Institution" class=" control-label col-md-4 text-left"> Institution <span class="asterisk "> * </span></label>
								<div class="col-md-6">

								{!! Form::select('institution', $institutions, $row['institution_id'], array('class'=>'select2', 'id'=>'group_id', 'placeholder'=>Lang::get('core.please_select'))) !!}
								</div> 
								<div class="col-md-2">
								 	
								</div>
							</div> 				

							<div class="form-group  " >
								<label for="Status" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Status') }} <span class="asterisk "> * </span></label>
								<div class="col-md-6">
								   	<label class='radio radio-inline'>
						    		{!! Form::radio('status',1,($row['status']==1) ? true:false,array()) !!} Active
						    		</label>
						    		<label class='radio radio-inline'>
									{!! Form::radio('status',0,($row['status']==0) ? true:false,array()) !!} Inactive
						 			</label>
								</div> 
								<div class="col-md-2">
								</div>
							</div> 


							<div class="form-group  " >
								<label for="Avatar" class=" control-label col-md-4 text-left"> {{ Lang::get('core.avatar') }} </label>
								<div class="col-md-6">
								<input  type='file' name='avatar' id='avatar' @if($row['avatar'] =='') class='required' @endif   />
								<div>
								@if($row['id'] !="" && $row['avatar'] != 0)
									{!! SiteHelpers::customavatar($row['email'],$row['id'],'small','img-circle user_image_selector') !!}
									<span class="pull-right" id="removeImage" style="margin-top:35px;color:red;cursor:pointer;">{{ Lang::get('core.x_remove') }}</span>
								@else
									<img class="img-circle user_image_selector" id="avatar-circle" border="2" width="100" />
									<span class="pull-right" id="removeImage" style="margin-top:35px;color:red;cursor:pointer;display:none;">{{ Lang::get('core.x_remove') }}</span>
								@endif
								</div>					
			 
								</div> 
								<div class="col-md-2">
							 	
								</div>
							</div> 	
							</fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('instructors?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		$("#user_id").jCombo("{{ URL::to('instructors/comboselect?filter=bse_users:id:id') }}",
			{  selected_value : '{{ $row["user_id"] }}' });

		$("#institution_id").jCombo("{{ URL::to('instructors/comboselect?filter=bse_institution:id:id') }}",
			{  selected_value : '{{ $row["institution_id"] }}' });
 
	});
	</script>		 
@stop