<div class="sbox-content">
	<div class="row">
		<div class="col-sm-12">
			<div class="col-sm-1 no-padding">
				<a href="{!! $an->user_link !!}">
					<center>
						{!! $an->imgpath !!}
					</center>
				</a>
			</div>
			<div class="col-sm-11">
				<div class="row">
					<div class="col-sm-10">
						<a href="{!! $an->user_link !!}">
							<h4 class="inline">{!! $an->display_name !!}</h4>
							@if($loggedid == $an->user_id)
								<h6 class="inline btn btn-xs btn-warning btn-rounded small m-t-none"> {!! Lang::get('core.YOU')!!}</h6>
							@endif
						</a>
						<span>{!! Lang::get('core.vert_sep')!!}</span>
						<h5 class="inline"><em>{!! bsetecHelpers::timeago(strtotime($an->createdOn)) !!}</em></h5>
					</div>
					<div class="col-sm-2">
						<div class="arrow">
							@if($loggedid == $an->user_id)
								<a href="javascript:void(0)" id="answer_block_ans_{!! $an->answer_id !!}_click" data-id="answer_block_ans_{!! $an->answer_id !!}" class="tips btn btn-xs btn-white answerEdit" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-pencil "></i></a>
							@else

							@endif
						</div>
					</div>
				</div>
				<div>{!! $an->answer_text !!}</div>
				@if($loggedid == $an->user_id)
				<div class="row show-grid hidden" id="answer_block_ans_{!! $an->answer_id !!}">
					<div class="col-sm-12">
						{!! Form::open(array('url'=>'questionanswer/answer', 'class'=>'','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
						{!! Form::hidden('question_id',$row->question_id) !!}
						{!! Form::hidden('answer_id',$an->answer_id) !!}
						{!! Form::hidden('user_id',$loggedid) !!}
						<div class="form-group">
							<label for="answer_text">{!! Lang::get('core.youranswer')!!}</label>
							<textarea name="answer_text" required class="form-control required" placeholder="{!! Lang::get('core.youranswer')!!}">{!! $an->answer_text !!}</textarea>
						</div>
						<div class="form-group m-b-sm">
							<button type="submit" class="btn btn-color answerSubmit">{!! Lang::get('core.submit')!!}</button>
							<button type="button" data-id="answer_block_ans_{!! $an->answer_id !!}" class="btn btn-color btn-default answer_ans_Cancel">{!! Lang::get('core.cancel')!!}</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>