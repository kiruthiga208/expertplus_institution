@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('questionanswer?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	
          
		 {!! Form::open(array('url'=>'questionanswer/save/'.$row['question_id'], 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
			<fieldset><legend> Questions Admin</legend>
								
							<div class="form-group hidethis " style="display:none;" >
							<label for="Question Id" class=" control-label col-md-4 text-left"> Question Id </label>
							<div class="col-md-6">
							{!! Form::text('question_id', $row['question_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group " >
							<label for="Category Id" class=" control-label col-md-4 text-left"> Category Name </label>
							<div class="col-md-6">
									{!! Form::select('category_id',[null=>Lang::get('core.please_select')] + $categories,$row['category_id'],array('class'=>'select2','id'=>'category_id')) !!}
							</div> 
							<!-- <div class="col-md-6">
							{!! Form::text('category_id', $row['category_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div>  -->
							<div class="col-md-2">
							
							</div>
							</div> 	
							 <div class="form-group">
							 <label for="category_id" class=" control-label col-md-4 text-left"> {!! Lang::get('core.Sub_Category')!!}</label>
							 <div class="col-md-6">
                    	        <select name="sub_cat_id" id="sub_cats" class="select2" required>
                        		 <option value="">{!! Lang::get('core.select_subcategory')!!}</option>
                                 </select>
                             </div>
                			 </div>			

							<div class="form-group  " >
							<label for="Question Text" class=" control-label col-md-4 text-left"> Question Text </label>
							<div class="col-md-6">
							<textarea name='question_text' rows='2' id='question_text' class='form-control ' required >{{ $row['question_text'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Question Description" class=" control-label col-md-4 text-left"> Question Description </label>
							<div class="col-md-6">
							<textarea name='question_description' rows='2' id='question_description' class='form-control' required >{{ $row['question_description'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Question Status" class=" control-label col-md-4 text-left"> Question Status </label>
							<div class="col-md-6">
							   	<label class='radio radio-inline'>
							   		{!! Form::radio('active',1,($row['question_status']==1) ? true:false,array()) !!} Active
							    </label>
							    <label class='radio radio-inline'>
									{!! Form::radio('active',0,($row['question_status']==0) ? true:false,array()) !!} Inactive
							 	</label>
							</div>
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Approved" class=" control-label col-md-4 text-left"> Approved </label>
							<div class="col-md-6">
							   	<label class='radio radio-inline'>
							   		{!! Form::radio('approved',2,($row['approved']==2) ? true:false,array()) !!} Approve
							    </label>
							    <label class='radio radio-inline'>
									{!! Form::radio('approved',1,($row['approved']==1) ? true:false,array()) !!} Unapprove
							 	</label>
							</div>
						    <div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group hidethis " style="display:none;" >
							<label for="Views" class=" control-label col-md-4 text-left"> Views </label>
							<div class="col-md-6">
							{!! Form::text('views', $row['views'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group hidethis " style="display:none;" >
							<label for="Answer Count" class=" control-label col-md-4 text-left"> Answer Count </label>
							<div class="col-md-6">
							{!! Form::text('answer_count', $row['answer_count'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group hidethis " style="display:none;" >
							<label for="CreatedOn" class=" control-label col-md-4 text-left"> CreatedOn </label>
							<div class="col-md-6">
							
			{!! Form::text('createdOn', $row['createdOn'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group hidethis " style="display:none;" >
							<label for="UpdatedOn" class=" control-label col-md-4 text-left"> UpdatedOn </label>
							<div class="col-md-6">
							
			{!! Form::text('updatedOn', $row['updatedOn'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> </fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('questionanswer?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	// $(document).ready(function() { 
		 $(document).on('change','#category_id',function(){
      var cats = $(this).val();
      $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
      });
      $.ajax({
        type: 'POST',
        url: '<?php echo e(\URL::to("course/subcategory")); ?>',
        data:'cats='+cats+'&subcat=0',
        success : function(data) {
          $('#sub_cats').html(data);
        }
      });

    });
	// });
	</script>		 
@stop