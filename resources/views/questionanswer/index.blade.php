@extends('layouts.frontend')
@section('content')
<style>
.btn.btn-warning{
    font-size: 12px;
    color: #fff;
    padding: 7px 20px;
    text-transform: capitalize;
    border-radius: 3px;
    border: none;
}
.btn.btn-xxl{
    border-radius: 0;
    font-family: open sans;
    font-size: 18px;
	text-transform: uppercase;
    font-weight: 600;
    height: 50px;
}
.search-block{
	padding:0;
	margin-bottom:20px;
}
.search-block .course_searching{
	width:100%;
}
.course_searching .more_one_portiion{
	width:90%;
}
.course_searching .btn{
	width:6%;
	text-align:center;
	padding:0px;
}
@media (max-width: 768px) {
	.course_searching .btn{
		width:15%;
	}
	.course_searching .more_one_portiion{
		width:85%;
	}
}
</style>
<div class="page-header">
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('') }}">{{ Lang::get('core.home') }}</a></li>
        <li><a href="{{ URL::to('questionanswer/view') }}">{!! Lang::get('core.questions') !!}</a></li>
        <li class="active"> </li>
      </ul>
</div> 
<div class="page-content row">
	<div class="reg_form new_reg_form mycourse_block">
		<div id="mainwrapper">
			<div class="col-sm-12">
				<div class="head_block clearfix">
					 <h2 class="title">{!! Lang::get('core.questions'); !!}</h2>
					
					 <div class="TeachingCourse_create">
						<p class="discover_courses"><a class="btn course_create" @if(!empty($loggedid)) id="askQuestion" href="javascript:void(0)" @else href="{!! e(url('user/login')); !!}" @endif>{!! Lang::get('core.Ask_Questions'); !!}</a></p>
					</div>
				</div>
				<p class="mycourse-p">{!! Lang::get('core.question_text') !!} {!! Lang::get('core.question_text_ans') !!}</p>
				<div class="search-block clearfix">
					<div class="course_searching">
						<form role="search" id="search-form" method="GET" action="{!! e(url('questionanswer/'.$view)); !!}">
							<input type="text" class="more_one_portiion inline" placeholder="{!! Lang::get('core.search_questions') !!}" id="search_question" name="search" value="{!! $search !!}" autocomplete="off">
							<input type="hidden" name="t" value="{!! $type; !!}" >
							<input type="hidden" name="qt" value="{!! $qtype; !!}" >
							<button class="btn btn-color btn-xxl inline" id="search" type="submit"><i class="fa fa-search"></i></button>
						</form>
					</div>
				</div>
				<h3 class="help_question" @if(empty($search)) style="display:none" @endif><center>{!! Lang::get('core.question_found'); !!} <a @if(!empty($loggedid)) href="javascript:void(0)" id="postQuestion" @else href="{!! e(url('user/login')); !!}" @endif>{!! Lang::get('core.question_post'); !!}</a></center></h3>
				@if(!empty($loggedid))
				<div class="tab-block-mycourse clearfix">
					<ul class="nav nav-tabs usernavbar price_align post">
						<li class="@if($type == '1'): current_page_item active @endif !!}"><a href="{!! url('questionanswer/'.$view) !!}" > {!! e(Lang::get('core.allquestions')); !!} </a></li>
						<li class="@if($type == '2'): current_page_item active @endif !!}"><a href="{!! url('questionanswer/'.$view.'?t=2') !!}" > {!! e(Lang::get('core.my_qna')); !!} </a></li>
					</ul>
				</div>
				@endif
				<div class="arrow m-b-sm m-r-sm @if(!empty($loggedid)) m-t-n-md @else m-t-sm @endif">
					<a href="{!! e(url('questionanswer/'.$view.'?t='.$type.'&qt=1')); !!}">@if($qtype == '1') <strong style="color:red">{!! e(Lang::get('core.all')); !!}</strong> @else {!! e(Lang::get('core.all')); !!} @endif </a>
					<span>{!! Lang::get('core.horz_sep'); !!}</span>
					<a href="{!! e(url('questionanswer/'.$view.'?t='.$type.'&qt=2')); !!}">@if($qtype == '2') <strong style="color:red">{!! e(Lang::get('core.Answered')); !!}</strong> @else {!! e(Lang::get('core.Answered')); !!} @endif</a>
					<span>{!! Lang::get('core.horz_sep'); !!}</span>
					<a href="{!! e(url('questionanswer/'.$view.'?t='.$type.'&qt=3')); !!}">@if($qtype == '3') <strong style="color:red">{!! e(Lang::get('core.Unanswered')); !!}</strong> @else {!! e(Lang::get('core.Unanswered')); !!} @endif</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="result_block">
	@include('questionanswer/result_block')
</div>
@include('questionanswer/add_block')

<script>
$(document).ready(function(){

    $('body').attr('class','course_body');

    var request = '';

    $(document).on('click','.logbackurl',function(e){
    	var elem = $(this);
    	var value = elem.attr('href');
		var date = new Date();
		date.setTime(date.getTime()+(60*60*1000));
		var expires = "; expires="+date.toGMTString();
		document.cookie = "last_visited_url_quest="+value+expires+"; path=/";
    });

    $(document).on('input','#search_question',function(e){
    	// if((e.type == 'keyup' && (e.which == 8 || e.which == 46 || e.KeyCode == 8 || e.KeyCode == 46)) || e.type == 'keypress'){
	    // 	if(request) request.abort();
	    	var search = $(this).val();
	    	if(search != '') $('.help_question').show(); else $('.help_question').hide();
	    	$('.result_block').html('<center><h3><i class="fa fa-refresh fa-spin"></i>&nbsp;&nbsp; Searching </h3></center>');
	    	request = $.ajax({
			    type: 'GET',
			    url: "{!! url('questionanswer/search') !!}",
			    data : {'search': search,'t': "{!! $type; !!}",'qt': "{!! $qtype; !!}",},
			    success: function(response){
			    	$('.result_block').html(response);
			    	$(".lazy").lazyload({effect:"fadeIn"});
			    }
			});
	    // }
    });

    $('.pagination a').click(function(){
    	window.location = $(this).attr('href');
    });

});
</script>
@stop