@extends('layouts.mailtemplate')
@section('content')
<h2>Hello {!! $authorname !!} , </h2>
<p>{!! $item !!} Details<br>
	{!! $item !!} Title : {!! $title !!} <br>
	Answer : {!! $answer !!} <br>
	click <a href="{!! $link !!}"> here </a> to view your {!! $item !!}.
</p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!}

@stop