@extends('layouts.mailtemplate')
@section('content')
<h2>Hello {!! $username !!} , </h2>
<p>Your {!! $item !!} has been deleted and the details are given below:</p>
<p>{!! $item !!} deleted by <b> {!! $authorname !!} </b> </p>
<p>
	Author Name : {!! $authorname !!}
</p>
<p>
	{!! $item !!} Title : {!! $title !!}
</p>

<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!}
@stop