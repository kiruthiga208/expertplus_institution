<style>
#chartdivcategory-wise-course {
  width: 100%;
  height: 500px;
  font-size: 11px;
}

.amcharts-pie-slice {
  transform: scale(1);
  transform-origin: 50% 50%;
  transition-duration: 0.3s;
  transition: all .3s ease-out;
  -webkit-transition: all .3s ease-out;
  -moz-transition: all .3s ease-out;
  -o-transition: all .3s ease-out;
  cursor: pointer;
  box-shadow: 0 0 30px 0 #000;
}

.amcharts-pie-slice:hover {
  transform: scale(1.1);
  filter: url(#shadow);
} 
a[title="JavaScript charts"]{visibility: hidden;}
</style>

@if($category_wise_course == '[]')
  {!! 'No courses'!!}
@endif
<div id="category-wise-course" style="height:500px;width:100%"></div>

<script src="http://www.amcharts.com/lib/3/pie.js"></script>
<script>
var cat_chart = AmCharts.makeChart("category-wise-course", {
  "type": "pie",
  "startDuration": 0,
   "theme": "light",
  "addClassNames": true,
  "legend":{
    "position":"right",
    "marginRight":100,
    "autoMargins":false
  },
  "innerRadius": "30%",
  "defs": {
    "filter": [{
      "id": "shadow",
      "width": "200%",
      "height": "200%",
      "feOffset": {
        "result": "offOut",
        "in": "SourceAlpha",
        "dx": 0,
        "dy": 0
      },
      "feGaussianBlur": {
        "result": "blurOut",
        "in": "offOut",
        "stdDeviation": 5
      },
      "feBlend": {
        "in": "SourceGraphic",
        "in2": "blurOut",
        "mode": "normal"
      }
    }]
  },
  "dataProvider": {!! $category_wise_course !!},
  "valueField": "total",
  "titleField": "category",
  "export": {
    "enabled": true
  }
});

cat_chart.addListener("init", handleInit);

cat_chart.addListener("rollOverSlice", function(e) {
  handleRollOver(e);
});

function handleInit(){
  cat_chart.legend.addListener("rollOverItem", handleRollOver);
}

function handleRollOver(e){
  var wedge = e.dataItem.wedge.node;
  wedge.parentNode.appendChild(wedge);  
}
</script>