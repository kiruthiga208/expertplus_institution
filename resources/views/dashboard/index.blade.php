@extends('layouts.app')


@section('content')
<style>
#registered-users {
	width	: 100%;
	height	: 500px;
}	

#transactions {
	width: 100%;
	height: 500px;
}						
</style>
<script src="http://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="http://www.amcharts.com/lib/3/themes/light.js"></script>
 <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>
<div class="page-content dashboard-b row">

	  <div class="page-title">
		<h3 class="admin_title">{!! Lang::get('core.dashboard') !!}</h3>
	  </div>
		  

	<div class="page-content-wrapper">  
	
	
	@if(Session::get('gid') ==1)

<section>
	<div class="row m-l-none m-r-none m-t  white-bg shortcut dashboard-m" >
		<div class="col-sm-6 col-md-3">
        <div class="module-b cmn-t">
			<span class="pull-left m-r-sm text-navy"><i class="fa fa-plus-circle"></i></span> 
			<a href="{{ URL::to('bsetec/module') }}" class="clear">
				<span class="h3 block m-t-xs"><strong>  {{ Lang::get('core.dash_i_module') }}  </strong>
				</span> <small class="text-muted text-uc">  {{ Lang::get('core.dash_module') }}</small>
			</a>
		</div>
        </div>
		<div class="col-sm-6 col-md-3">
        <div class="settings-b cmn-t">
			<span class="pull-left m-r-sm text-info">	<i class="fa fa-cogs"></i></span>
			<a href="{{ URL::to('bsetec/config') }}" class="clear">
				<span class="h3 block m-t-xs"><strong> {{ Lang::get('core.dash_i_setting') }}</strong>
				</span> <small class="text-muted text-uc">   {{ Lang::get('core.dash_setting') }} </small> 
			</a>
            </div>
		</div>
		<div class="col-sm-6 col-md-3">
        <div class="site-menu cmn-t">
			<span class="pull-left m-r-sm text-warning">	<i class="fa fa-sitemap"></i></span>
			<a href="{{ URL::to('bsetec/menu') }}" class="clear">
			<span class="h3 block m-t-xs"><strong>  {{ Lang::get('core.dash_i_sitemenu') }} </strong></span>
			<small class="text-muted text-uc">  {{ Lang::get('core.dash_sitemenu') }}  </small> </a>
		</div>
        </div>
		<div class="col-sm-6 col-md-3 b-r  p-sm">
        <div class="user-group cmn-t">
			<span class="pull-left m-r-sm ">	<i class="fa fa-users"></i></span>
			<a href="{{ URL::to('core/users') }}" class="clear">
			<span class="h3 block m-t-xs"><strong> {{ Lang::get('core.dash_i_usergroup') }}</strong>
			</span> <small class="text-muted text-uc">  {{ Lang::get('core.dash_usergroup') }} </small> </a>
		</div>
        </div>
	</div> 
</section>	

	
	<div class="row m-t">
		<div class="col-md-12">
			<div class="sbox">
				<div class="sbox-title"> <h3 class="course-title"> {!! Lang::get('core.course_list') !!} <small> {!! Lang::get('core.category_wise') !!} </small> </h3> <div class="filter-b"> <input type="hidden" name="_token" value="{{ csrf_token() }}">              
                <label>{!! Lang::get('core.filters') !!}</label>
                <select class="filter">
                  <option value="0">{!! Lang::get('core.Overall')!!}</option>
                  <option value="7">{!! Lang::get('core.last_week') !!}</option>
                  <option value="30">{!! Lang::get('core.last_month') !!}</option>
                  <option value="90">{!! Lang::get('core.last_three_month') !!}</option>
                </select></div>
                </div>
				<div class="sbox-content">
					<div class="row">
						<div class="col-md-11">
               
              <div class="course">  
                @include('dashboard.ajax-course')	
              </div>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</div>

	<div class="row m-t">
		<div class="col-md-12">
			<div class="sbox">
				<div class="sbox-title"> <h3> {!! Lang::get('core.reg_users') !!} <small> {!! Lang::get('core.date_wise') !!} </small> </h3> </div>
				<div class="sbox-content">
					<div class="row">
						<div class="col-md-11">
							<div id="registered-users"></div>	
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</div>

	<div class="row m-t">
		<div class="col-md-12">
			<div class="sbox">
				<div class="sbox-title"> <h3> {!! Lang::get('core.Transactions') !!} <small> {!! Lang::get('core.last_three_month') !!} </small> </h3> </div>
				<div class="sbox-content">
					<div class="row">
						<div class="col-md-11">
							<div id="transactions"></div>	
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</div>
	@endif
</div>	
	
</div>
<script>

$(document).ready(function()
{
  $('body').on('change', '.filter', function()
  {
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
    });
    $.ajax({
        type: 'POST',
        url: '{!! url("dashboard/ajax-course") !!}',
        data: {value:$(this).val()},
        success : function(data) 
        {
          $('.course').html(data);
        }
      });
  });
  
});
  
</script>




<script src="http://www.amcharts.com/lib/3/serial.js"></script>
<script>
var users_chart = AmCharts.makeChart("registered-users", {
    "type": "serial",
    "theme": "light",
    "marginRight": 80,
    "autoMarginOffset": 20,
    "dataDateFormat": "YYYY-MM-DD",
    "valueAxes": [{
        "id": "v1",
        "axisAlpha": 0,
        "position": "left"
    }],
    "balloon": {
        "borderThickness": 1,
        "shadowAlpha": 0
    },
    "graphs": [{
        "id": "g1",
        "bullet": "round",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "bulletSize": 5,
        "hideBulletsCount": 50,
        "lineThickness": 2,
        "title": "red line",
        "useLineColorForBulletBorder": true,
        "valueField": "value",
        "balloonText": "<div style='margin:5px; font-size:19px;'><span style='font-size:13px;'>[[category]]</span><br>[[value]]</div>"
    }],
    "chartScrollbar": {
        "graph": "g1",
        "oppositeAxis":false,
        "offset":30,
        "scrollbarHeight": 80,
        "backgroundAlpha": 0,
        "selectedBackgroundAlpha": 0.1,
        "selectedBackgroundColor": "#888888",
        "graphFillAlpha": 0,
        "graphLineAlpha": 0.5,
        "selectedGraphFillAlpha": 0,
        "selectedGraphLineAlpha": 1,
        "autoGridCount":true,
        "color":"#AAAAAA"
    },
    "chartCursor": {
        "pan": true,
        "valueLineEnabled": true,
        "valueLineBalloonEnabled": true,
        "cursorAlpha":0,
        "valueLineAlpha":0.2
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true,
        "dashLength": 1,
        "minorGridEnabled": true
    },
    "export": {
        "enabled": true
    },
    "dataProvider": {!! $registered_users !!}
});

users_chart.addListener("rendered", zoomChart);

zoomChart();

function zoomChart() {
    users_chart.zoomToIndexes(users_chart.dataProvider.length - 40, users_chart.dataProvider.length - 1);
}
</script>

<script>
var chart = AmCharts.makeChart("transactions", {
    "type": "serial",
	"theme": "light",
    "legend": {
        "horizontalGap": 10,
        "maxColumns": 1,
        "position": "right",
		"useGraphSettings": true,
		"markerSize": 10
    },
    "dataProvider": {!! $transactions !!},
    "valueAxes": [{
        "stackType": "regular",
        "axisAlpha": 0.3,
        "gridAlpha": 0
    }],
    "graphs": [{
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Commision Amount",
        "type": "column",
		"color": "#000000",
        "valueField": "commisions"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Course Purchase Amount",
        "type": "column",
		"color": "#000000",
        "valueField": "purchases"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Withdrawal Requests",
        "type": "column",
      "color": "#000000",
        "valueField": "requests"
    }],
    "categoryField": "month",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left"
    },
    "export": {
    	"enabled": true
     }

});
</script>
@stop