@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('semester?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	

		 {!! Form::open(array('url'=>'semester/save/'.$row['id'].'?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		 {!! Form::hidden('id', $row['id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
<div class="col-md-12">
			<fieldset>
				<legend> Semester</legend>
												
				<div class="form-group  " >
				<label for="Institution" class=" control-label col-md-4 text-left"> Institution </label>
				<div class="col-md-6">
				{!! Form::text('institution_id', bsetecHelpers::getInstitutionName(Session::get('iid')), array('class'=>'form-control', 'placeholder'=>'',  'disabled' )) !!} 
				</div> 
				<div class="col-md-2">
				</div>
				</div> 				

				<div class="form-group  " >
				<label for="Department" class=" control-label col-md-4 text-left"> Department <span class="asterisk "> * </span></label>
				<div class="col-md-6">
				{!! Form::select('department_id', $departments, $row['department_id'], array('class'=>'form-control department', 'placeholder'=>Lang::get('core.please_select'), 'required'  )) !!} 
				</div> 
				<div class="col-md-2">
				</div>
				</div> 				

				<div class="form-group  " >
				<label for="Institution Course" class=" control-label col-md-4 text-left"> Institution Course <span class="asterisk "> * </span></label>
				<div class="col-md-6">
				{!! Form::select('institution_course_id', $courses, $row['institution_course_id'], array('class'=>'form-control institution_course', 'placeholder'=>Lang::get('core.please_select'), 'required'  )) !!} 
				</div> 
				<div class="col-md-2">
				</div>
				</div> 				

				<div class="form-group  " >
				<label for="Semester" class=" control-label col-md-4 text-left"> Semester <span class="asterisk "> * </span></label>
				<div class="col-md-6">
				{!! Form::text('semester', $row['semester'],array('class'=>'form-control', 'placeholder'=>'',  'required' )) !!} 
				</div> 
				<div class="col-md-2">
				</div>
				</div> 				

				<div class="form-group  " >
				<label for="Status" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Status') }} <span class="asterisk "> * </span></label>
				<div class="col-md-6">
				   	<label class='radio radio-inline'>
		    		{!! Form::radio('status',1,($row['status']==1) ? true:false,array()) !!} Active
		    		</label>
		    		<label class='radio radio-inline'>
					{!! Form::radio('status',0,($row['status']==0) ? true:false,array()) !!} Inactive
		 			</label>
				</div> 
				<div class="col-md-2">
				</div>
				</div> 				

			</fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('semester?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
<script type="text/javascript">
$(document).ready(function() { 

	department_id = $('.department').val();
	getCourses(department_id);

	$('.department').change(function(){
		department_id = $(this).val();
		getCourses(department_id);
		$('.institution_course option').remove();
		$('.institution_course').append('<option selected="selected" value="">Please Select</option>');
	});
});

function getCourses(department_id){
	$.ajax({
		url  : '{{ url("user/getcourses") }}',
		data : {department_id : department_id},
		success : function(data){
			$.each(data, function(key,value){
				$('.institution_course').append('<option class="'+value['id']+'" value="'+value['id']+'">'+value['name']+'</option>');
				if(value['id']=="{{ $row['institution_course_id'] }}"){
					$('.institution_course option').removeProp('selected');
					$('.institution_course option.'+value['id']).prop("selected", "selected");
				}
			});
		},
		error : function(error){
			console.log(error);
		} 
	});
}
</script>		 
@stop