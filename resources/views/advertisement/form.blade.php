@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('advertisement?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	
	@if($row['id'])
		@php ( $url = 'advertisement/save/'.$row['id'].'?return='.$return  )
	@else
		@php ( $url = 'advertisement/save?return='.$return )
	@endif
		 {!! Form::open(array('url'=>$url, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
			<fieldset><legend> Advertisement</legend>
								
							<div class="form-group  " >
							<!-- <label for="Id" class=" control-label col-md-4 text-left"> Id </label> -->
							<div class="col-md-6">
							{!! Form::hidden('id', $row['id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Name" class=" control-label col-md-4 text-left"> Name </label>
							<div class="col-md-6">
							{!! Form::text('name', $row['name'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Script" class="hidethis control-label col-md-4 text-left"> Script </label>
							<div class="col-md-6">
							<textarea name='script' rows='2' id='script' class='form-control '  
			  >{{ $row['script'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<!-- <div class="form-group  " >
							<label for="Created At" class="hidethis control-label col-md-4 text-left"> Created At </label>
							<div class="col-md-6">
							
			{!! Form::text('created_at', $row['created_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div>  -->					
							<!-- <div class="form-group  " >
							<label for="Updated At" class=" control-label col-md-4 text-left"> Updated At </label>
							<div class="col-md-6">
							
			{!! Form::text('updated_at', $row['updated_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> --> 					
							<div class="form-group  " >
							<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
							<div class="col-md-6">
							{!! Form::checkbox('status','1', $row['status'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> </fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('advertisement?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		 
	});
	</script>		 
@stop