@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('subscriber?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	
	@foreach($subscriber as $subscriber)

		{{---*/ $id=$subscriber->id /*---}}
		{{---*/ $user_id=$subscriber->user_id /*---}}
		{{---*/ $group_idd=$subscriber->group_id /*---}}
	@endforeach
		 {!! Form::open(array('url'=>'subscriber/update/'.$row['id'].'?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
			<fieldset><legend> subscriber</legend>
								
							<div class="form-group  " >
							<!-- <label for="Id" class=" control-label col-md-4 text-left"> Id </label> -->
							<div class="col-md-6">
							{!! Form::hidden('id', $id,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 	
							<div class="form-group  " >
									<label for="Group / Level" class=" control-label col-md-4 text-left"> Group / Level <span class="asterix"> * </span></label>
									<div class="col-md-6">
									  <select name='group_id' rows='5' id='group_id' code='' 
							class='select2 '  required  >
							<option value="">Select group</option>
							@foreach($groups as $gr)
								@if($group_idd==$gr->id)
								<option value="{!! $gr->id !!}" selected="selected">{!! $gr->group_name !!}</option>
								@else
								<option value="{!! $gr->id !!}" >{!! $gr->group_name !!}</option>
								@endif
							@endforeach
							</select> 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
							</div> 
							<div class="form-group  " >
									<label for="User" class=" control-label col-md-4 text-left"> Group / Level <span class="asterix"> * </span></label>
									<div class="col-md-6">
									  <select name='user_id' rows='5' id='user_id' code='' 
							class='select2 '  required  >
							<option value="">Select user</option>
							@foreach($users as $user)
								@if($user_id==$user->id)
								<option value="{!! $user->id !!}" selected="selected">{!! $user->first_name !!}</option>
								@else
								<option value="{!! $user->id !!}" >{!! $user->first_name !!}</option>
								@endif
							@endforeach
							</select> 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
							</div> 	
								<input type="hidden" id="email" name="email" code="email" />
							 </fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<!-- <button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button> -->
					<button type="button" onclick="location.href='{{ URL::to('subscriber?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		 
	});
	</script>		 
@stop