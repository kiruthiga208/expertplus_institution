@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('usergroup?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	
	{{---*/$id='' /*---}}
	{{---*/$name='' /*---}}
	<!-- 'url'=>'usergroup/searchlist', -->
		 {!! Form::open(array('url'=>'usergroup/searchlist','class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','method'=>'get')) !!}
<div class="col-md-12">
			<fieldset><legend> newsletter user group</legend>
				<input type="hidden" name="group_name" value="{{$row['id']}}" />
			<div class="form-group  " >
			<label for="Id" class=" control-label col-md-4 text-left"> Search Users  <span class="asterix"> </span></label>
			<div class="col-md-6">
			 <select name='user_id' rows='5' id='user_id' code='' 
			class='select2 userid '>
			</select>
			
			</div> 
				<div class="col-md-2">
				</div>
			</div> 	
			<div class="form-group  " >
			<label for="Id" class=" control-label col-md-4 text-left"> User Type </label>
			<div class="col-md-6">
			<input type="radio" name="usertype" value="student" class="form-control"/>Student
			<input type="radio" name="usertype" value="instructor" class="form-control" />Instructor
			<!-- {!! Form::radio('usertype', 'student','',array('class'=>'form-control usertype'   )) !!} {!! Form::label('radio', 'Student', array('class' => 'awesome'))!!}
			{!! Form::radio('usertype', 'instructor','',array('class'=>'form-control usertype'   )) !!} {!! Form::label('radio', 'Instructor', array('class' => 'awesome'))!!} -->
			</div> 
			<div class="col-md-2">
			</div>
			</div> 					
			<div class="form-group  " >
					<label for="Group / Level" class=" control-label col-md-4 text-left"> Country <span class="asterix"> </span></label>
					<div class="col-md-6">
					  <select name='group_id' rows='5' id='group_id' code='' 
			class='select2 country'>
			</select> 
					 </div> 
					 <div class="col-md-2">
					 </div>
			</div> 
			</fieldset>
		</div>		
			<div style="clear:both"></div>	
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<!-- <button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button> -->
					<button type="submit" name="submit" class="btn btn-primary btn-sm searchuser" ><i class="fa  fa-save "></i> {{ 'Search' }}</button>
					<button type="button" onclick="location.href='{{ URL::to('usergroup?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
				  </div> 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#group_id").jCombo("{{ URL::to('core/users/comboselect?filter=instructor:b_country:b_country') }}",
		{  selected_value : '{{ $row["group_id"] }}' });
		
		$("#user_id").jCombo("{{ URL::to('core/users/comboselect?filter=users:first_name:first_name:email') }}",
		{  selected_value : '{{ $row["user_id"] }}' },
		{  selected_value : '{{ $row["email"] }}' }
		);

		$('.searchuser').click(function(){
			var usertype= $('input[name=usertype]:checked').val();
			var user = $('.userid option:selected').val();
			var country = $('.country option:selected').val();
			if((typeof usertype === "undefined")&&(user=='')&&(country==''))
			{
				alert('Select Any one search field')
				return false;
			}
			else{	return true;
				
			}
		
		});
	});
	</script>		
	</script>		 
@stop