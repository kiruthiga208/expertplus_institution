@extends('layouts.mailtemplate')
@section('content')
<h2>Hello {!! $username !!} , </h2>
<p>Your Course has been deleted and the details are given bellow:</p>
<p>Course deleted by <b> {!! $authorname !!} </b> </p>
<p>
	Author Name : {!! $authorname !!} 
</p>
<p>
	Course Title : {!! $title !!}
	click <a href="{!! $link !!}"> here </a> to view your course.
</p>

<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!} 
@stop