@extends('layouts.mailtemplate')
@section('content')
	<h2>Hello {!! $authorname !!} , </h2>
<p>You have an new follower for your Secured course!! <br>The student details are given bellow:</p>
	<p>
		Student Name : {!! $studentname !!} 
	</p>
	<p>
		Course Title : {!! $title !!} <br>
		Click <a href="{!! $link !!}">here</a> To view the Course.
	</p>
	<p>
		Date of Joined : {!! $joindate !!} 
	</p>

	<br /><br /><p> Thank You </p>
	
	{!! CNF_APPNAME !!} 
@stop