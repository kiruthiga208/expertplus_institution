@extends('layouts.mailtemplate')
@section('content')

<h2>Hello {!! $authorname !!} , </h2>
<p>Your Course has been approved by Admin</p>
<p>
	Course Title : {!! $title !!} 
</p>
<p>Now <b> {!! $title !!} </b> course has been published!! <br>
click <a href="{!! $link !!}"> here </a> to view your course.</p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!} 

@stop
