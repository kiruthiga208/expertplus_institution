@extends('layouts.mailtemplate')
@section('content')
<h2>Hello {!! $authorname !!} , </h2>
<p>A new guy joined your course</p>

<p><b>{!! $instructorname !!}</b> joined in the bellow course:</p>

<p>
	Course Title : {!! $title !!} <br>                        
	Click <a href="{!! $link !!}"> here</a> to view course.
</p>
	
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!} 
@stop