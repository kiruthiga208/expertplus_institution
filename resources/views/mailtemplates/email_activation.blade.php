@extends('layouts.mailtemplate')
@section('content')
<h2>Hello {!! $username !!} , </h2>

<p>
	Your account has been activated successfully.
</p>
<p>Now you can enjoy our service.</p>
<p> Please Click <a href="{{ URL::to('course') }}"> here </a>to continue.</p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!} 
@stop