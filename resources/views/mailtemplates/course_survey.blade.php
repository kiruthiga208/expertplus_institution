@extends('layouts.mailtemplate')
@section('content')
		<h2>Hello Admin , </h2>
		
		<p>
			Title : {!! $title !!} submitted by {!! $username !!}
		</p>
		<h5>Planning Your Course</h5>
		<p>
			What language do you want to teach in : {!! $language !!}
		</p>
		<p>
			What category best describes your course topic? : {!! $topic !!}
		</p>
		<p>
			Which of the following best describes your primary goal of teaching? : {!! $goal !!}
		</p>
		<h5>Producing Your Course</h5>
		<p>
			Which of the following best describes your ideal course creation experience? : {!! $ideal !!}
		</p>

		<p>
			Do you have an existing email subscriber list? {!! $subscribers !!}
		</p>

		<p>
			Do you have subscribers on YouTube? : {!! $title !!} submitted by {!! $youtubes !!}
		</p>
	
		<br /><br /><p> Thank You</p>
		
		{!! CNF_APPNAME !!} 
@stop