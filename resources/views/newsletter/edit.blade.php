@extends('layouts.app')

@section('content')
@if( Auth::check() )
	{{---*/ $auth=Auth::user()->id /*---}}
@endif
<link rel="stylesheet" href="{{asset('assets/bsetec/css/style.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/bsetec/css/redactor.css') }}" />
<script type="text/javascript" src="{{ asset('assets/bsetec/js/redactor.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(
	function()
	{
		$('#redactor').redactor();
	}
);
</script>
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('newsletter?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	
		@foreach($subscriber as $row)
		{{---*/ $id=$row->id /*---}}
		{{---*/ $subject=$row->subject /*---}}
		{{---*/ $gr=$row->user_group /*---}}
		{{---*/ $desc=$row->message /*---}}
		{{---*/ $status=$row->status /*---}}
		@endforeach
		{!! Form::open(array('url'=>'newsletter/insert/'.$row->id, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		 	
		 	<div class="form-group hidethis " style="display:none;">
				<label for="Id" class=" control-label col-md-4 text-left"> Id </label>
				<div class="col-md-6">
				  {!! Form::hidden('id', $id,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
				  {!! Form::hidden('user_id',$auth,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
				 </div> 
				 <div class="col-md-2">
				 	
				 </div>
			 </div> 
			  <div class="form-group  " >
				<label for="Group / Level" class=" control-label col-md-4 text-left"> Users <span class="asterix"> * </span></label>
				<div class="col-md-6">
				<select name='users' rows='5' id='users' code='' 
					class='select2 '  required  >

					@foreach($groupusers as $group)
					@if($gr==$group->id)
					<option value="{!! $group->id !!}" selected="{!!'selected'!!}">{!! $group->group_name !!}</option>
					@else
					<option value="{!! $group->id !!}" >{!! $group->group_name !!}</option>
					@endif
					
					@endforeach
					
				</select> 
				 </div> 
				 <div class="col-md-2">
				 	
				 </div>
			  </div>
			 <div class="form-group  " >
					<label for="Title" class=" control-label col-md-4 text-left"> Subject <span class="asterix"> * </span></label>
					<div class="col-md-6">
					  {!! Form::text('subject', $subject,array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
					 </div> 
					 <div class="col-md-2">
					 	
					 </div>
			</div> 
			
			
			   <div class="form-group  " >
					<label for="editor" class=" control-label col-md-4 text-left"> Description <span class="asterix"> * </span></label>
					<div class="col-md-6">
					<div class="adjoined-bottom">
						<div class="grid-container">
							<div class="grid-width-50">
								<textarea id="redactor" name="content" >{{$desc}}</textarea>
							</div>
						</div>
					</div>
					  
					 </div> 
					 <div class="col-md-2">
					 	
					 </div>
				</div> 

				<div class="form-group  " >
					<label for="editor" class=" control-label col-md-4 text-left"> Enable <span class="asterix"> * </span></label>
					<div class="col-md-6">
					@if($status==1)
					 <input type="checkbox" name="status"  checked/>
					@else
						<input type="checkbox" name="status" />
					@endif
					 </div> 
					 <div class="col-md-2">
					 </div>
				</div> 
				<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('newsletter') }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
				  </div> 
		 	
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		 
	});
	</script>		 
@stop