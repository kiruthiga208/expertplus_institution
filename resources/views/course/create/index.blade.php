@extends('admin/master')

@section('container')
<div class="title_bg clearfix">
<div class="double_line"></div>
<div class="title_inner_bg clearfix">
<h2 class="title_heading">{{ $title }}</h2>
</div>
<div class="double_line"></div>
</div>
<style type="text/css">
    td {
        vertical-align: middle!important;
    }
</style>
<div class="clsboxes clear">
<div class="table-responsive">
    <table class="table table-bordered table-hover table-striped tablesorter">
        <thead>
        <tr>
            <th>#</th>
            <th>Logo</th>
            <th>Title <i class="fa fa-sort"></i></th>
            <th>Created By <i class="fa fa-sort"></i></th>
            <th>Students <i class="fa fa-sort"></i></th>
            <th>price <i class="fa fa-sort"></i></th>
            <th>Reviews <i class="fa fa-sort"></i></th>
            <th>Status <i class="fa fa-sort"></i></th>
            <th>Created Date <i class="fa fa-sort"></i></th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php $i=1; ?>
        @foreach($course as $courses)
        <tr>
            <td>{{ $i }}</td>
            <td><a href="{{ url($courses->slug) }}"><img width="150" src="{{ asset('uploads/course/'.$courses->image) }}"/></a></td>
            <td>{{ $courses->course_title }}</td>
            <td>{{ $courses->course_title }}</td>
            <td>{{ $courses->course_title }}</td>
            <td>{{ $courses->pricing }}</td>
            <td>{{ $courses->course_title }}</td>
             @if($courses->approved == 1)
            <td><span class="label label-success">Approved</span></td>
            @else
            <td><span class="label label-danger">Waiting for approval</span></td>
            @endif
            <td><abbr class="timeago" title="{{ date(DATE_ISO8601,strtotime($courses->created_at)) }}">
            {{ date(DATE_ISO8601,strtotime($courses->created_at)) }}</abbr></td>
            <td>
                <a class="btn btn-success" href="{{ url('image/'.$courses->course_id.'/'.$courses->slug) }}">
                    <i class="glyphicon glyphicon-zoom-in"></i>
                </a>
                <a class="btn btn-info" href="{{ url('admin/courses/edit/'.$courses->course_id) }}">
                    <i class="glyphicon glyphicon-edit"></i>
                </a></td>
        </tr>
        <?php $i++; ?>
        @endforeach
        </tbody>
    </table>
</div>
</div>
{{ $course->links() }}
@stop