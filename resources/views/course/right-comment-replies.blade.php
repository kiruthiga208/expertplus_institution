<li id="reply-block{!!$reply->reply_id!!}">
	<div>
		<div class="comment-box">
			<span class="thumb-wrapper">
			<div class="profile_image">
			<a href="{{ URL::to('profile/'.$reply->username) }}">
				{!! SiteHelpers::customavatar($reply->email,$reply->user_id,'small') !!}
			</a>
			</div>
			</span>

			<div class="activity-box">
				<div class="activity-header">
					<div class="header-right clearfix pull-right">
						@if($reply->user_id==\Session::get('uid'))
						<div class="hidden-btns">
							<a href="javascript:void(0)" class="activity-edit-link mini-act-btn mini-tooltip2 editreply" data-rcommentid="{!!$reply->reply_id!!}">
								<i class="fa fa-pencil"></i>
							</a>
							<a href="javascript:void(0)" class="activity-delete-link mini-act-btn mini-tooltip2 removereply" data-rcommentid="{!!$reply->reply_id!!}">
								<i class="fa fa-trash-o"></i>
							</a>
						</div>
						@endif
					</div>
					<div class="header-left">
						<a class="activity-header-link" href="{{ URL::to('profile/'.$reply->username) }}">{{$reply->first_name}} {{$reply->last_name or ''}}</a>
						<time class="activity-time"> &middot; {!! SiteHelpers::changeFormat($reply->created_at) !!}</time>
					</div>
				</div>
				<div class="comment-body w3c-default" id="replytext-{!!$reply->reply_id!!}"><p><?php echo $reply->reply_comment; ?></p></div>
				<div style="display:none;" id="replyopenbox-{!!$reply->reply_id!!}">
					<input type="hidden" name="pid" value="{!!$reply->reply_id!!}">
					<textarea id="edittextboxcontent-{!!$reply->reply_id!!}" class="form-control lectureeditor edittextboxcontent-{!!$reply->reply_id!!}" rows="2" placeholder="Comment" name="comment" cols="50"><?php echo $reply->reply_comment; ?></textarea>                
					<button class="btn btn-orange updatereplybtn" type="button" data-commentid="{!!$reply->lecture_comment_id!!}" data-replyid="{!!$reply->reply_id!!}">{!! Lang::get('core.sb_save')!!}</button>
					<button class="btn btn-danger editcancelbtn" data-commentid="{!!$reply->reply_id!!}" type="button">{!! Lang::get('core.cancel')!!}</button>
				</div>
			</div>
		</div>
	</div>
</li>

<script type="text/javascript">
$('.lectureeditor').redactor({
				    buttons: ['format', 'bold', 'italic','image']
				});
</script>