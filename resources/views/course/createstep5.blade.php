@extends('layouts.frontend')
@section('content')
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<style type="text/css">
.form-group{ margin:25px 0px; }
.leftnopad{padding-left:0px !important;}
</style>
<div class="create-course">
<div class="container">
	<div class="row">
    <div class="col-sm-12">
            @include('course.courseheader')
        </div>
		<div class="col col-sm-3 multi_development">
			@include('course.createsidebar')
		</div>

<div class="col-xs-12 col-sm-9">
<div class="block2 clearfix">
<div class="promo_block">
<h2>{!! Lang::get('core.Promo_Video')!!}</h2>
<p>{!! Lang::get('core.promo_tip')!!}</p>
<!-- <a href="#">Promo Video Guide</a> -->
</div>
<div class="subblock">
<div class="video_block">
<label>{!! Lang::get('core.Video_Preview')!!}:</label>
<div class="block3">

<div class="text_block">
<?php
	$filename = '';
	$filetype = '';
	if(count($videos)>0) {
	if(!empty($videos['0']->video_title)){
		$filename = $videos['0']->video_title;
		$filetype = $videos['0']->video_type; 
	}
	}
?>
<strong id="_link" style="display:none;">{!! Lang::get('core.promo_text')!!} »</strong>

<div id="video_div" class="video_b">
<div id="video_prograss"></div>
<div id='_video'>
@if(count($videos)>0)
@if($course->video != NULL && $course->video != 0 && $videos['0']->processed !=0 )

<video id='my_video' class='video-js vjs-default-skin' controls preload='auto' width='570' height='264' data-setup='{}'>
<source src="{{ asset('/uploads/videos/'.$filename.'.mp4') }}" type="video/mp4"><source src="{{ asset('/uploads/videos/'.$filename.'.webm') }}" type="video/webm"><source src="{{ asset('/uploads/videos/'.$filename.'.ogv') }}" type="video/ogg"><source src="{{ asset('/uploads/videos/'.$filename.'.avi') }}" type="video/avi"></video>
@endif
@endif
@if(count($videos)>0)
@if($videos['0']->processed == 0 )
<div class="video_notification">{!! Lang::get('core.video_process')!!}</div>
@endif
@endif
</div>
</div>


</div>
   <p><b>{!! Lang::get('core.promo_hint')!!}</b></p>
   <p>{!! Lang::get('core.promo_msg') !!}</p>
   </div>
   <div class="upload_type clearfix">
<label>{!! Lang::get('core.edit_video')!!}:</label>
<div class="format_type clearfix">
  <div class="format"> <span id="test_error">{!! Lang::get('core.video_use')!!}</span>
    <div class="progress progress-striped progress_upload" id="progress_bar" style="display:none;">
    <div class="progress-bar progress-bar-warning active" id="progress_bar_p"></div>
    <p id="file_name"></p>
    </div>
  </div> 
</div>
<div class="button-block clearfix">
  <div class="change_btn">
  <button id='cancel_btn' style="display:none;">{!! Lang::get('core.cancel')!!}</button>
    <input type="button" id="promo_delete" style="display:none;" onClick='promo_delete();' value="Change" />
  </div>

  <div id="video_up"> 
    <div class="fileUpload btn btn-primary"> 
      <span>{!! Lang::get('core.Upload_video') !!}</span>  
      <input id="promoVideo" type="file" name="promoVideo" data-url="{!! url('').'/course/uploadvideo' !!}" class="upload"/>
    </div> 
  </div>
  {!! Form::hidden('course_id', $course->course_id,array('id'=>'course_id') )  !!}
  {!! Form::hidden('video_name', $filename ,array('id'=>'video_name') )  !!}
  {!! Form::hidden('step', $step,array('id'=>'step') )  !!}
  <input type='hidden' id='video_type' name='video_type' />

</div>
@php( $youtube = \bsetecHelpers::ifInstalled('youtube') )
@if($youtube>0)
 <div class="youtube_class">
    <label>{!! Lang::get('core.youtube_id') !!} </label>
    <span>{!! Lang::get('core.youtube_note')  !!}</span>
       <div class="you_btn"> <input type="button" value="ADD" id="click"> </div>
       <div class="you_txt">
          <input type="text"  value="{!! $course->youtube_preview_id !!}" name="id" id="url_id">
        </div> 
      <div id="div_video"> </div>
</div>
@endif
</div>
</div>
</div>
</div>
</div>

  

</div>
</div>
</div>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.ui.widget.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-ui.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-process.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-validate.js') }}"></script>
<script type="text/javascript">
var uploadProcess = null;
var error = $("#test_error");
var progress_bar = $("#progress_bar");
var cancel_btn = $("#cancel_btn");
var progress_bar_p = $('#progress_bar_p');
var delete_btn = $("#promo_delete");
var video_div = $("#video_div");
var video_prograss = $('#video_prograss');
var courseID = $('#course_id').val();
var courseStep = $('#step').val();
var link_tag =  $('#_link');




$(document).ready(function(){
  @if($course->video != NULL && $course->video != 0)
    delete_btn.show();
    $("#video_up").hide();
    link_tag.hide();
    video_div.addClass('video_upload');
    @else
    delete_btn.hide();
    $("#video_up").show();
    link_tag.show();
    @endif
  });


$(function () {
$('#promoVideo').fileupload({
        dataType:'json',
        formData : {course_id:courseID,step:courseStep},
        add:function(e,data){
            var file=data.files[0];
            var uploadErrors = false;
            var acceptFileTypes = /(\.|\/)(mp4|flv|ogg|ogv|avi|wmv|x-ms-wmv|webm|ogx|x-msvideo)$/i;
            if((data.originalFiles[0]['type'].length || data.originalFiles[0]['type'].length==0) && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                error.text('{!! Lang::get("core.image_file_type_error")!!}').addClass('error_msg');
                return false;
            }
            if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000) {
                error.text('{!! Lang::get("core.image_big_size") !!}').addClass('error_msg');
                return false;
            }
            uploadProcess = data.submit();
            link_tag.hide();
            video_prograss.show();
            progress_bar_p.css('width','0%');
			      $("#progress_bar").show(); 
            $("#video_up").hide();
            $("#file_name").text(file.name);
            cancel_btn.show();
        },
        progress: function (e, data) {
            error.removeClass('error_msg').hide();
            var percentage = parseInt(data.loaded / data.total * 100,10);
			      progress_bar_p.css('width',percentage+'%');
            video_prograss.html("{!! Lang::get('core.img_uploading')!!} "+ percentage + '%');
        },
        processfail: function (e, data) {
            file_name = data.files[data.index].name;
            error.text(data.files[data.index].error).addClass('error_msg');
        },
        done: function(e, data){
            error.removeClass('error_msg').hide();
            return_data = data.result;
            v_html = "";
            if(return_data.status){
                v_html +="<div class='video_notification'>"+return_data.video_path+"</div></div>";
                $('#video_name').val(return_data.video_name);
                $('#video_type').val(return_data.video_type);
            }else{
              error.addClass('error_msg').text("uploading failed!");
            }

            video_prograss.html("{!! Lang::get('core.uploading_full')!!}");
            $("#video_up,#video_prograss").hide();
            $('#_video').html(v_html);
            video_div.addClass('video_upload');
            progress_bar_p.removeClass('active');
            $("#file_name").text("{!! Lang::get('core.video_completed')!!}");
            cancel_btn.hide();
            delete_btn.show();
		},
    });
    $('body').on('click','#cancel_btn', function (e) {
       uploadProcess.abort();
       $("#video_div").removeClass('video_upload');
       error.toggle();
       cancel_btn.hide();
       $('#_video').html("");
       link_tag.show();
       video_prograss.hide();
       $("#video_up,#progress_bar").toggle();
    });
});

function promo_delete() {
    video_name = $('#video_name').val();
    courseID = $('#course_id').val();
    //alert(video_name);
    $.ajax({
        url:'{!! url('').'/course/uploadvideo'!!}',
        data:{video_file:video_name,course_id:courseID},
        type:"DELETE",
        success:function(res){
            if(res == "success"){ 
             delete_btn.hide();
           video_prograss.hide();
		       $("#video_div").removeClass('video_upload');
		       $("#video_up,.text_block strong").toggle();
           $('#progress_bar').hide();
             error.show();

             $('#_video').html("");
             link_tag.show();
            } 
        },
        fail:function(){
            alert("error");
        }
    });    
}


$( "#click" ).click(function() {

var video_id = $('#url_id').val();
if(video_id == '')
{
  toastr.error("Error", "Please give valid youtube id");
  msg()
  return;
}

var cid = $('#course_id').val();
get_url = "{!! \URL::to('') !!}";
  $.get(get_url+"/course/youtubepriupload/"+video_id+"/"+cid)
  .done(function( data ) {
    if(data == 1)
    {
      toastr.success("Sucess", "Saved successfully");
      msg()
      location.reload();
    }
    else
    {
      toastr.error("Error", "Something went wrong please check your Yotube id and try again !");
      msg()
     
    } 
  });

});

function msg(){
        toastr.clear();
        toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "300",
        "timeOut": "300",
        "extendedTimeOut": "300",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
}



 $( document ).ready(function() {
  var check_url = $('#url_id').val();
  get_url = "{!! \URL::to('') !!}";
  
  if(check_url != ''){

      var url = $('#url_id').val();
  $.post(get_url+"/api_youtube/getvideo.php", { videoid: url, type: "Download" })
  .done(function( data ) {
     var json = $.parseJSON(data);
     document.getElementById('div_video').innerHTML = '<video controls id="video_ctrl" width="400px" height="300px"><source src="'+get_url+'/api_youtube/'+json.url+'" type="video/mp4"></video>';    
  });
  }
  });
</script>
<style>
.youtube_class {
    clear: both;
    padding-top: 10px;
}

.you_txt {
    float: left;
    margin-left: 11px;
    margin-top: 9px;
    width: 70.32%;
}
.youtube_class video {
    border: 3px solid #444;
    border-radius: 3px;
    height: auto;
    margin-top: 10px;
    padding: 0;
    width: 100%;
}
.you_btn > input {
    background-color: #428bca;
    border: medium none;
    border-radius: 3px;
    color: #fff;
    font-size: 16px;
    padding: 6px 10px;
    text-align: center;
    width: 80%;
}
.you_txt > input {
    border: 1px solid #e1e1e1;
    margin: 0;
    padding: 8px 10px;
    width: 100%;
}
</style>
@php( $cnt = \bsetecHelpers::ifInstalled('rtl') )
@if($cnt!=1)
  <style>
  .you_btn {
    float: right;
    margin-top: 9px;
    width: 28%;
    margin-right: -9px;
}
  </style>
@endif
@stop