@extends('layouts.frontend')
@section('content')
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600italic,700,800,800italic,700italic,600,400italic,300,300italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,900italic,700italic,900,700,500italic,400italic,500,300,300italic,100italic,100' rel='stylesheet' type='text/css'>
<link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/view.css') }}" rel="stylesheet">
<script src="{{ asset('assets/bsetec/static/js/jquery.raty.min.js') }}"></script>
<!-- <script src="{{ asset('assets/bsetec/js/infinitescroll.js') }}"></script>  -->
<!--<script src="https://raw.githubusercontent.com/pklauzinski/jscroll/master/jquery.jscroll.min.js"></script>-->
@php $curdate = date('Y-m-d H:i:s') @endphp
<!--Description-->
<div class="course_view_page">
  <div class="container">
    <div class="row">

      <div class="col-md-9">
        <div class="course_view_section">
          <div class="course_view_header">

            <h1>{{$course->course_title}}</h1>
            <p> {{$course->subtitle}}</p>
            @if($t_return == "SUCCESS")
            <h4>{!! Lang::get('core.transaction_success') !!}</h4>
            @else
            <div class="star_rating">
              <ul class="star_one clearfix">
                <li class="late_star ReadonlyRating startvalue star_rating" data-score="{{\bsetecHelpers::checkReview($course->course_id)}}"></li>
                <li class="late_star last">{!! $allreviews !!} ratings, {!! $allstudents !!} students enrolled </li>
              </ul>
            </div>
            @endif

          </div>
          <div class="course_view_video">
            <div class="banner_watch">

              @if($course->video != NULL && $course->video != 0)
              <video id='my_video' class='video-js vjs-default-skin' controls preload='auto' width='100%' height='100%' data-setup='{}'><source src="{{ asset('/uploads/videos/'.$filename.'.mp4') }}" type="video/mp4" ><source src="{{ asset('/uploads/videos/'.$filename.'.webm') }}" type="video/webm" ><source src="{{ asset('/uploads/videos/'.$filename.'.ogv') }}" type="video/ogg" ></video>
              @else 
                              @if($course->youtube_preview_id != NULL)

                @php  $youtube = \bsetecHelpers::ifInstalled('youtube') @endphp
                        @if($youtube>0)
                              <div class="youtube_video">
                              <div id="div_video"> </div>
                                <script type="text/javascript">
                               $( document ).ready(function() {
                                var check_url = '{!! $course->youtube_preview_id or '' !!}';
                                if(check_url != ''){
                                    var url = '{!! $course->youtube_preview_id or '' !!}';
                                $.post( "{!! url('api_youtube/getvideo.php') !!}", { videoid: url, type: "Download" })
                                .done(function( data ) {
                                   var json = $.parseJSON(data);
                                   var urls = "{!! url('api_youtube') !!}/"+json.url;
                                   document.getElementById('div_video').innerHTML = '<video autoplay controls id="video_ctrl" width="800px" height="400px"><source src="'+urls+'" type="video/mp4"></video>';
                                   document.getElementById('video_ctrl').play();
                                });
                                } 
                                });
                              </script>
                              </div>
                          @endif
                      @endif
              @endif 
                       </div>
            </div>
            <div class="course_view_overview">
              <div class="over_view">
                <h2>{!! Lang::get('core.course_overview') !!}</h2>
                <p>{!! $course->description !!}</p>
                <div class="course_view_requirement">
                  <h4>{!! Lang::get('core.requirements') !!}</h4>
                  @php $rexplodes = explode('--txt--',$course->course_req) @endphp
                  <ul>
                    @if(count($rexplodes)>1)
                    @foreach($rexplodes as $rlists)
                    <li>{!! $rlists !!}</li>
                    @endforeach 
                    @else
                    <li>{!! $course->course_req !!}</li>
                    @endif
                  </ul>
                </div>
                <div class="course_view_goal">
                  <h4>{!! Lang::get('core.goal') !!}</h4>
                  @php  $gexplodes = explode('--txt--',$course->course_goal) @endphp
                  <ul>
                    @if(count($gexplodes)>1)
                    @foreach($gexplodes as $glists)
                    <li>{!! $glists !!}</li>
                    @endforeach 
                    @else
                    <li> {!! $course->course_goal !!}</li>
                    @endif
                  </ul>
                </div>
                <div class="course_view_audience">
                  <h4>{!! Lang::get('core.audience') !!}</h4>
                  @php $aexplodes = explode('--txt--',$course->int_audience) @endphp
                  <ul>
                    @if(count($aexplodes)>1)
                    @foreach($aexplodes as $alists)
                    <li>{!! $alists !!}</li>
                    @endforeach 
                    @else
                    <li>{!! $course->int_audience !!}</li>
                    @endif
                  </ul>
                </div>
              </div>
            </div>
            <div class="course_view_author">

              <h2>{!! Lang::get('core.abt_author') !!}</h2>
              @if(count($author)>0)
              <div class="course_view_author_inner">
                <div class="course_view_author_img">
                 <a href="{!! URL::to('profile/'.$author['0']->username) !!}"> {!! SiteHelpers::customavatar($author['0']->email,$author['0']->id) !!} </a>
               </div>
               <div class="course_view_author_bio">
                {!! $author['0']->biography or '' !!}
              </div>
            </div>
            @endif
          </div>

          <div class="course_view_curriculam">
           <div class="course_curriculum">
            <h2>{!! Lang::get('core.course_curriculum')!!}</h2>

            <?php if(count($curriculum)>0){ 
              $no = 1;
              for($i=0;$i<count($curriculum);$i++){
                $lectures = \SiteHelpers::getlectures($curriculum[$i]->section_id);
                ?>
                <div class="section_one_inroduction">
                  <h4> <label>Section {{$no}}:</label> {{ $curriculum[$i]->title }}</h4>
                  <ul class="section_introduction_lasting clearfix">
                    <?php if(count($lectures)>0) { 
                      for($j=0;$j<count($lectures);$j++){
                        ?>
                        <li class="course_view_lecture_list">
                          <div class="course_lecture_curriculam_list">
                            <div class="course_lecture_title course_media_{!! $lectures[$j]->media_type !!}"><h6>{{$lectures[$j]->title}}</h6>
                              @if($lectures[$j]->description!='')
                              <a href="javascript:void(0);" onclick="customaccordion('{{$lectures[$j]->lecture_quiz_id}}')"><span class="plusminus-{{$lectures[$j]->lecture_quiz_id}}"><i class="fa fa-chevron-down"></i></span></a>
                              @endif
                              <div class="clear"></div>
                            </div>
                          </div>
                          @if($lectures[$j]->description!='')
                          <div id="wholecontent-{{$lectures[$j]->lecture_quiz_id}}" class="course_lecture_curriculam_list_desc" style="display:none;">{!! $lectures[$j]->description !!}</div>
                          @endif

                          @if($lectures[$j]->media_type=='0')
                          <div class="course_view_lecture_details">{!! $lectures[$j]->duration or '' !!}</div>
                          @elseif($lectures[$j]->media_type=='1')
                          <div class="course_view_lecture_details">{!! $lectures[$j]->duration or '' !!}</div>
                          @elseif($lectures[$j]->media_type=='2')
                          @php $pdf = \SiteHelpers::getlecturesfiles($lectures[$j]->lecture_quiz_id);  @endphp
                          <div class="course_view_lecture_details">{!! $pdf['0']->duration or '' !!} Page</div>
                          @elseif($lectures[$j]->media_type=='5')
                          @php $pdf = \SiteHelpers::getlecturesfiles($lectures[$j]->lecture_quiz_id);  @endphp
                          <div class="course_view_lecture_details">{!! $pdf['0']->duration or '' !!} Page</div>
                          @else
                          <div class="course_view_lecture_details">&nbsp;</div>
                          @endif
                        </li>

                        <?php  
                      } 

                    }
                    ?>
                  </ul>
                </div>
                <?php  $no++;
              } 

            }
            ?>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
          </div>
        </div>

        <div class="course_view_feedback">
         <h2>{!! Lang::get('core.reviews') !!}</h2>
         <div class="course_view_feedback_lists" id="reviewitems">
           <ul class="clearfix" id="ritems">
             @if(count($reviews)>0) 
             @foreach($reviews as $results)
             @php ( $rateid = $results->ratings_id )
             <li class="litems">
               <div class="course_view_feedback_author_img"><a href="{!! URL::to('profile/'.$results->username) !!}">{!! SiteHelpers::customavatar($results->email,$results->user_id) !!}</a></div>
               <div class="course_view_feedback_content">
                 <div class="course_view_feedback_author_name"><a href="{!! URL::to('profile/'.$results->username) !!}">{!! $results->first_name or '' !!}   {!! $results->last_name or '' !!}</a></div>
                 <div class="course_view_feedback_desc">{!! $results->review_description or '' !!}</div>
                 <!-- <div class="course_view_feedback_rating"></div> -->
                 <div class="star_rating course_view_feedback_rating">
                  <ul class="star_one clearfix">
                    <li class="late_star ReadonlyRating startvalue star_rating" data-score="{{\bsetecHelpers::checkReview($results->course_id,$results->user_id)}}"></li>

                  </ul>
                </div>
                <div class="course_view_feedback_date">{!! SiteHelpers::changeFormat($results->created_at) !!}</div>
              </div>
            </li>
            @endforeach

            @else
            <li>{!! Lang::get('core.no_review') !!}</li>
            @endif
          </ul>

          <div class="reviewloader"></div>
          <div class="course_view_feedback_btn">
            @if(!empty($rateid) && count($pnext)>0)
            <ul id="rpagination-{!! $rateid !!}" class="pagination" >
              <li class="next"><a href="javascript:void(0);" data-next="{!! $rateid or '' !!}" data-cid="{!! $courseid !!}" class="btn btn-primary seereviews">{!! Lang::get('core.see_more') !!}</a></li>
            </ul>
            @endif
            <!-- <a href="#" class="btn btn-primary">SEE MORE COMMENTS</a> -->
          </div>

        </div>
      </div>

    </div>
  </div>


  <div class="col-md-3 ">
    <div class="course_view_sidebar">

     <div class="select_price">
      <ul class="price_dollar clearfix">
        <li class="first">{!! Lang::get('core.price') !!}</li>
        <li class="second">
        @if (defined('CNF_CURRENCY'))
                @php ($currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
        @endif 
         @if($course->pricing != 0) {!! $currency !!} {{$course->pricing}} @else {!! Lang::get('core.free') !!} @endif
       </li>
       <li class="last">
        @if(Auth::check() == false)
        <div class="controls"><a class="btn btn-primary" href="{{url('subscribe-course/'.$course->course_id.'/'.$course->slug)}}">{!! Lang::get('core.take') !!}</a></div>
        @else
        @if(\bsetecHelpers::checkPurchase($course->course_id))
        <div class="modal fade" id="CoursePurchase" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog" style="border:none;">
            <div class="modal-content">
              <div class="modal-header header_bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">{!! Lang::get('core.already') !!}</h2>
              </div>
              <div class="modal-body">
                <h4>{{$course->course_title}}</h4>
                <a href="{{url('learn-course').'/'.$course->course_id.'/'.$course->slug}}">{!! Lang::get('core.click_here_course') !!}</a>
              </div>
            </div>
          </div>
        </div>
        @endif
        @if($course->pricing == 0) 
        <div class="controls"><button type="button" class="btn btn-primary" disabled> {!! Lang::get('core.take') !!} </button></div>
        @else
        <div class="controls"><div class="joinloader"></div><a href="javascript:void(0);" id="jointhis" data-mid="{!! $insstatus['0']->manage_instrctor_id or '' !!}" data-cid="{!! $insstatus['0']->course_id or '' !!}" class="btn btn-primary" disabled>{!! Lang::get('core.join') !!}</a></div>
        @endif
        @endif

      </li>
    </ul>
  </div>
  <div class="wish_list_section">
    <ul class="sidebarli clearfix">
      @if(\bsetecHelpers::checkFavorite($course->course_id) == true)
      <li><a href="javascript:void(0);" data-cid="{!! $courseid !!}" id="changeicon-{!! $courseid !!}" class="" data-type="0" ><i class="fa fa-heart"></i><span>{!! Lang::get('core.Wishlisted')!!}</span></a></li>
      @else
      <li><a href="javascript:void(0);" data-cid="{!! $courseid !!}" id="changeicon-{!! $courseid !!}" class="" data-type="1" ><i class="fa fa-heart-o"></i><span>{!! Lang::get('core.add_wishlist') !!}</span></a></li>
      @endif
    </ul>
    
    <ul class="social_icons sidebarli last clearfix">
      <li><a href="javascript:void(0);" class="twitter"><i class="fa fa-twitter"></i><span class="entypo-twitter">{!! Lang::get('core.share_twitter')!!}</span></a></li>
      <li><a href="javascript:void(0);" class="facebook"><i class="fa fa-facebook"></i><span class="entypo-facebook">{!! Lang::get('core.share_facebook')!!}</span></a></li>
      <li><a href="javascript:void(0);" class="gplus"><i class="fa fa-google-plus"></i><span class="entypo-gplus">{!! Lang::get('core.share_google')!!}</span></a></li>
      <li><a href="javascript:void(0);"  class="pintrest"><i class="fa fa-pinterest"></i><span class="entypo-pinterest">{!! Lang::get('core.share_pin')!!}</span></a></li>
      <li><a href="javascript:void(0);"><i class="fa fa-linkedin"></i><span class="entypo-linkedin">{!! Lang::get('core.share_linkedin')!!}</span></a></li>

    </ul>
    <div class="money_back_section">

    </div>
  </div>

  <div class="course_view_sidebar_box clearfix">
    <div class="course_view_sidebar_box_header">
      <h3>{!! Lang::get('core.course_instructor') !!}</h3>
    </div>
    <div class="course_view_sidebar_content">
     @if(count($instructor)>0)
     <div class="recent_viewed_course_main">
       @foreach($instructor as $results)
       <div class="recent_viewed_course clearfix">
        <div class="recent_viewed_course_img">
          <a href="{!! URL::to('profile/'.$results->username) !!}">
            {!! SiteHelpers::customavatar($results->email,$results->id) !!}
          </a>
        </div>
        <div class="recent_viewed_course_content">
          <h6><a href="{!! URL::to('profile/'.$results->username) !!}">{!! $results->username or '' !!}</a></h6>
          <div class="recent_viewed_course_desc">{!! $results->designation or '' !!}</div>

        </div>
      </div>
      @endforeach
    </div>      
    @endif
    @if(count($instructor)=='0' && count($author)>0)

    <div class="recent_viewed_course_main">
      <div class="recent_viewed_course clearfix">
        <div class="recent_viewed_course_img">
          <a href="{!! URL::to('profile/'.$author['0']->username) !!}">{!! SiteHelpers::customavatar($author['0']->email,$author['0']->id) !!}</a>
        </div>
        <div class="recent_viewed_course_content">
          <h6><a href="{!! URL::to('profile/'.$author['0']->username) !!}"> {!! $author['0']->username or '' !!}</a></h6>
          <div class="recent_viewed_course_desc"> {!! $author['0']->designation or '' !!}</div>
        </div>
      </div>
    </div>

    @endif

    @if(count($morecourse)>0)
    <div class="morecourse_heading">
      <h3> {!! Lang::get('core.course_by') !!} {!! $author['0']->first_name !!} {!! $author['0']->last_name !!}   <i class="fa fa-plus-circle"></i></h3>
    </div>
    <div class="recent_viewed_course_main">
      @foreach($morecourse as $mycourse)
      <div class="recent_viewed_course clearfix">
        <div class="recent_viewed_course_img">
          <a href="{!! URL::to('courseview/'.$mycourse->course_id.'/'.$mycourse->slug) !!}">
            @php ( $img = \bsetecHelpers::getImage($mycourse->image) )
            <img src="{!! $img !!}" />
          </a>
        </div>
        <div class="recent_viewed_course_content">
          <h6><a href="{!! URL::to('courseview/'.$mycourse->course_id.'/'.$mycourse->slug) !!}">{!! $mycourse->course_title or '' !!}</a></h6>
          <div class="recent_viewed_course_rating">
            <div class="star_rating">
              <ul class="star_one clearfix">
                <li class="late_star ReadonlyRating startvalue" data-score="{{\bsetecHelpers::checkReview($mycourse->course_id)}}"></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      @endforeach
    </div>
    @endif



  </div>

</div>


@if(count($students)>0)
<div class="course_view_sidebar_box clearfix">
  <div class="course_view_sidebar_box_header">
    <h3>{!! Lang::get('core.students') !!}</h3>
  </div>
  <div class="course_view_sidebar_content" id="studentitems">
    <ul class="attend_one_stud clearfix" id="sitems">
      @if(count($students)>0)
      @foreach($students as $results)
      @php ($sid = $results->taken_id )
      <li class="slitems">
        <a href="{!! URL::to('profile/'.$results->username) !!}">
          {!! SiteHelpers::customavatar($results->email,$results->user_id) !!}
        </a>
      </li>
      @endforeach
      @endif

    </ul>
    <div class="studentloader"></div>
    <div class="course_view_sidebar_students_more">
      @if(!empty($sid) && count($snext)>0)
      <ul id="spagination-{!! $sid !!}" class="pagination">
        <li class="next"><a href="javascript:void(0);" data-cid="{!! $courseid !!}" data-next="{!! $sid !!}" class="seestudents" >{!! Lang::get('core.view_students') !!}</a></li>
      </ul>
      @endif
    </div>
  </div>
</div>
@endif
@if(count($mostviewed)>0)
<div class="course_view_sidebar_box clearfix">
  <div class="course_view_sidebar_box_header">
    <h3>{!! Lang::get('core.recently_view') !!}</h3>
  </div>
  <div class="course_view_sidebar_content">
    <div class="recent_viewed_course_main">
      @foreach($mostviewed as $mostlikes)
      <div class="recent_viewed_course clearfix">
        <div class="recent_viewed_course_img">
          <a href="{!! URL::to('courseview/'.$mostlikes->course_id.'/'.$mostlikes->slug) !!}">
           @php ( $img = \bsetecHelpers::getImage($mostlikes->image) )
            <img src="{!! $img !!}" />
          </a>
        </div>
        <div class="recent_viewed_course_content">
          <h6> <a href="{!! URL::to('courseview/'.$mostlikes->course_id.'/'.$mostlikes->slug) !!}">{!! $mostlikes->course_title or '' !!}</a></h6>
          <div class="recent_viewed_course_rating">
            <div class="star_rating">
              <ul class="star_one clearfix">
               <li class="late_star ReadonlyRating startvalue" data-score="{{\bsetecHelpers::checkReview($mostlikes->course_id)}}"></li>
             </ul>
           </div>
         </div>
         <div class="recent_viewed_course_pricing">
          @if($mostlikes->pricing!='0' && $mostlikes->pricing!=NULL)
          <span class="easy_teruns"> $ {!! $mostlikes->pricing or '' !!}  </span>
          @elseif($mostlikes->pricing=='0' || $mostlikes->pricing==NULL)
          <span class="easy_teruns">{!! Lang::get('core.free')!!}</span>
          @endif
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>

</div>
@endif


</div>
</div>


</div>
</div>
</div>




<!--End Description-->
<script type="text/javascript">
$(function(){
  $(document).on('click','.replybutton',function () { 
    var cid = $(this).data('commentid');
    if ($('#openbox-'+cid).is(':visible')) { 
      $("#openbox-"+cid).slideUp(300); 
    } 
    if ($("#openbox-"+cid).is(':visible')) { 
      $("#openbox-"+cid).slideUp(300);
    } else {
      $("#openbox-"+cid).slideDown(300); 

    } 
  });

  $(document).on('click','.cancelbtn',function () { 
    var cid = $(this).data('commentid');
    if ($('#openbox-'+cid).is(':visible')) { 
      $("#openbox-"+cid).slideUp(300); 
    } 
    if ($("#openbox-"+cid).is(':visible')) { 
      $("#openbox-"+cid).slideUp(300);
    } else {
      $("#openbox-"+cid).slideDown(300); 

    } 
  });

  $(document).on('click','.replybtn',function(e){
    var cid = $(this).data('commentid');
    var courseid = $(this).data('courseid');
    var textvalues = $('#textboxcontent-'+cid).val();

    if($.trim(textvalues).length===0){
      toastr.error("Error", "{!! Lang::get('core.enter_valid_comments') !!}");
      toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
    }else if ($.trim(textvalues).length>0){
      var me = $(this);
      e.preventDefault();
      if ( me.data('requestRunning') ) {
        return;
      }
      me.data('requestRunning', true);

      $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
      });
      $.ajax({
        type: 'POST',
        url: '<?php echo e(\URL::to("course/insertreply")); ?>',
        data:'commentid='+cid+'&comments='+textvalues+'&courseid='+courseid,
        success : function(data) {
          var obj = jQuery.parseJSON(data);
          if(obj.status=='1'){
            $('#openbox-'+cid).hide();
            $('#textboxcontent-'+cid).val('');

            $('#replyies-'+obj.results.commentid).append('<div id="reply-block" class="media"><hr><a href="" class="pull-right bla">'+obj.results.imgpath+'</a><div class="media-body"><h4 class="media-heading comment"><a href="">'+obj.results.fname+'</a><span class="msg-time pull-right"><i class="glyphicon glyphicon-time"></i><span><abbr class="timeago">Just Now</abbr>&nbsp;</span></span></h4>'+obj.results.comments+'</div></div>');


            toastr.success("Success", "{!! Lang::get('core.comment_thanks') !!}");
            toastr.options = {
              "closeButton": true,
              "debug": false,
              "positionClass": "toast-bottom-right",
              "onclick": null,
              "showDuration": "300",
              "hideDuration": "1000",
              "timeOut": "5000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
            }
          }else if(obj.status=='2'){
            toastr.error("Error", "{!! Lang::get('core.comments_error') !!}");
            toastr.options = {
              "closeButton": true,
              "debug": false,
              "positionClass": "toast-bottom-right",
              "onclick": null,
              "showDuration": "300",
              "hideDuration": "1000",
              "timeOut": "5000",
              "extendedTimeOut": "1000",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
            }
          }

          return false;
        },complete: function() {
          me.data('requestRunning', false);
        }
      });
}

});


$(document).on('click','.postbtn',function(e){
  var courseid = '{!! Request::segment(2) !!}';
  var textvalues = $('#postcomments').val();

  if($.trim(textvalues).length===0){
    toastr.error("Error", "{!! Lang::get('core.enter_valid_comments') !!}");
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "positionClass": "toast-bottom-right",
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
  }else if ($.trim(textvalues).length>0){
    var me = $(this);
    e.preventDefault();
    if ( me.data('requestRunning') ) {
      return;
    }
    me.data('requestRunning', true);

    $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
    });
    $.ajax({
      type: 'POST',
      url: '<?php echo e(\URL::to("course/insertcomments")); ?>',
      data:'comments='+textvalues+'&courseid='+courseid,
      success : function(data) {
        var obj = jQuery.parseJSON(data);
        if(obj.status=='1'){
          $('#postcomments').val('');


          $('#allcomments').append('<div id="comment-'+obj.results.commentid+'" class="media profile clearfix"><div class="profile_image"><a href=""> '+obj.results.imgpath+'</a></div><div class="profile_title"> <a href="">'+obj.results.fname+' </a></div><div class="profile_content">'+obj.results.comments+'</div><div class="about_hours"><abbr class="timeago">Just now</abbr>&nbsp;<a data-commentid="'+obj.results.commentid+'" id="openreplybox" class="replybutton">Reply</a></div><div id="openbox-'+obj.results.commentid+'" style="display:none;" class="commentReplyBox"><input type="hidden" value="'+obj.results.commentid+'" name="pid"><textarea cols="50" name="comment" placeholder="Comment" rows="2" class="form-control" id="textboxcontent-'+obj.results.commentid+'"></textarea><button data-courseid="7" data-commentid="'+obj.results.commentid+'" type="button" class="btn btn-info replybtn">Reply</button><button type="button" data-commentid="'+obj.results.commentid+'" class="btn btn-danger cancelbtn">Cancel</button></div></div><div id="replyies-'+obj.results.commentid+'"></div>')
          toastr.success("Success", "{!! Lang::get('core.comment_thanks') !!}");
          toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }
        }else if(obj.status=='2'){
          toastr.error("Error", "{!! Lang::get('core.comments_error')!!}");
          toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }
        }

        return false;
      },complete: function() {
        me.data('requestRunning', false);
      }
    });
}

});



$('.ReadonlyRating').raty({score: function() {return $(this).attr('data-score');},readOnly: true,starOff : "{{url('').'/assets/bsetec/static/img/star-off.png'}}",starOn: "{{url('').'/assets/bsetec/static/img/star-on.png'}}",starHalf : "{{url('').'/assets/bsetec/static/img/star-half.png'}}"});



$(document).on('click','.addtowishlist',function(){
  var cid = $(this).data('cid');
  var type = $(this).attr('data-type');

  var me = $(this);
  if ( me.data('requestRunning') ) {
    return;
  }
  me.data('requestRunning', true);

  $.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
  });
  $.ajax({
    type: 'POST',
    url: '<?php echo e(\URL::to("course/wishlist")); ?>',
    data:'cid='+cid+'&type='+type,
    success : function(data) {
      var obj = jQuery.parseJSON(data);
      if(obj.status=='0'){
        if(type=='0'){
          $('#changeicon-'+cid).html('<i class="fa fa-heart-o"></i><span>{!! Lang::get("core.add_wishlist")!!}</span>');
          $('#changeicon-'+cid).attr('data-type',1);
        }else if(type=='1'){
          $('#changeicon-'+cid).html('<i class="fa fa-heart"></i><span>{!! Lang::get("core.Wishlisted")!!}</span>');
          $('#changeicon-'+cid).attr('data-type',0);
        }

        toastr.success("Success", "{!! Lang::get('core.new_add_wishlist')!!}");
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "positionClass": "toast-bottom-right",
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
      }else if(obj.status=='1'){
        toastr.error("Error", "{!! Lang::get('core.login_error') !!}");
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "positionClass": "toast-bottom-right",
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
      }else if(obj.status=='2'){
        toastr.error("Error", "{!! Lang::get('core.comments_error') !!}");
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "positionClass": "toast-bottom-right",
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }
      }

      return false;

    },complete: function() {
      me.data('requestRunning', false);
    }
  });


});

$(document).on('click','.seereviews',function(){

  var next = $(this).data('next');
  var cid  = $(this).data('cid');
  var me = $(this);
  if ( me.data('requestRunning') ) {
    return;
  }
  me.data('requestRunning', true);

  $.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
  });
  $.ajax({
    type: 'POST',
    url: '<?php echo e(\URL::to("course/reviews")); ?>',
    data:'cid='+cid+'&next='+next,
    beforeSend: function() {
      $('.reviewloader').html('<h5><i class="fa fa-refresh fa-spin"></i> {!! Lang::get("core.loading") !!}</h5>');
    },
    success : function(data) {
     $('#ritems').append(data);
     $("#rpagination-"+next).remove();
     return false;

   },complete: function() {
    $('.reviewloader').html("");
    me.data('requestRunning', false);
  }
});


});

$(document).on('click','.seestudents',function(){

  var next = $(this).data('next');
  var cid  = $(this).data('cid');
  var me = $(this);
  if ( me.data('requestRunning') ) {
    return;
  }
  me.data('requestRunning', true);

  $.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
  });
  $.ajax({
    type: 'POST',
    url: '<?php echo e(\URL::to("course/students")); ?>',
    data:'cid='+cid+'&next='+next,
    beforeSend: function() {
      $('.studentloader').html('<h5><i class="fa fa-refresh fa-spin"></i> {!! Lang::get("core.loading") !!}</h5>');
    },
    success : function(data) {
     $('#sitems').append(data);
     $("#spagination-"+next).remove();
     return false;

   },complete: function() {
    $('.studentloader').html("");
    me.data('requestRunning', false);
  }
});


});

$(document).on('click','#jointhis',function(){
  var mid = $(this).data('mid');
  var cid = $(this).data('cid');
  if (typeof(mid) != 'undefined' && mid != null){
    // alert(mid);  
    var me = $(this);
    if ( me.data('requestRunning') ) {
      return;
    }
    me.data('requestRunning', true);

    $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
    });
    $.ajax({
      type: 'POST',
      url: '<?php echo e(\URL::to("course/updateinsstatus")); ?>',
      data:'mid='+mid+'&cid='+cid,
      beforeSend: function() {
        $('.joinloader').html('<h5><i class="fa fa-refresh fa-spin"></i> {!! Lang::get("core.loading") !!}</h5>');
      },
      success : function(data) {
        var obj = jQuery.parseJSON(data);
        if(obj.status=='0'){
          toastr.success("Success`", "{!! Lang::get('core.course_joining_msg') !!}");
          toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }
          window.location.reload();
        }else if(obj.status=='1'){
          toastr.error("Error", "{!! Lang::get('core.login_error') !!}");
          toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }
        }else if(obj.status=='2'){
          toastr.error("Error", "{!! Lang::get('core.comments_error')!!}");
          toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }
        }

        return false;

      },complete: function() {
        $('.joinloader').html("");
        me.data('requestRunning', false);
      }
    });

}

});


});
function customaccordion(lid){
    // if ($('#wholecontent-'+lid).is(':visible')) { 
    //     $("#wholecontent-"+lid).slideUp(300); 
    //     $(".plusminus-"+lid).html('<i class="fa fa-chevron-down"></i>'); 
    // } 
    if ($("#wholecontent-"+lid).is(':visible')) { 
      $("#wholecontent-"+lid).slideUp(300);
      $(".plusminus-"+lid).html('<i class="fa fa-chevron-down"></i>'); 
    } else {
      $("#wholecontent-"+lid).slideDown(300); 
      $(".plusminus-"+lid).html('<i class="fa  fa-chevron-up"></i>'); 
    } 
  }
  </script>
  @stop

