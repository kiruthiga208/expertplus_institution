<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> {!! Lang::get('core.Take_Course')!!} </title>
<meta name="keywords" content="">
<meta name="description" content=""/>
<link href="{{ asset('assets/bsetec/themes/theme1/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/themes/theme1/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">   
<link href="{{ asset('assets/bsetec/css/icons.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/css/vertical.css') }}" rel="stylesheet">
<link rel="shortcut icon" href="{{ asset('uploads/images/'.CNF_FAV)}}" type="image/x-icon">	
<link href="{{ asset('assets/bsetec/static/css/video-js.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/js/plugins/toastr/toastr.css')}}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/coursepreview.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/js/plugins/iCheck/skins/square/green.css')}}" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600italic,700,800,800italic,700italic,600,400italic,300,300italic' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/easing.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/themes/theme1/js/bootstrap.min.js') }}"></script>	
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/toastr/toastr.js') }}"></script>
<script type="text/javascript" src="<?php echo e(URL::to('assets/bsetec/js/plugins/tinymce/jscripts/tiny_mce/tiny_mce.js')); ?>"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/sly.min.js') }}"></script>
<script type="text/javascript">
jQuery(function($){
	'use strict';

	// -------------------------------------------------------------
	//   Non Item Based Navigation
	// -------------------------------------------------------------
	(function () {
		var $frame = $('#scroller');
		var $wrap  = $frame.parent();

		// Call Sly on frame
		$frame.sly({
			speed: 300,
			easing: 'easeOutExpo',
			pagesBar: $wrap.find('.pages'),
			activatePageOn: 'click',
			scrollBar: $wrap.find('.scrollbar'),
			scrollBy: 100,
			dragHandle: 1,
			dynamicHandle: 1,
			clickBar: 1,
		});
	}());
});
</script>
<style>
body{background:#333333;height: 100% !important; color:#FFF;}
html, body, .container-fluid, .row { height: 100%;}

</style>
</head>
<body>
<div class="taken-course">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-8 contentView nopadding">
			
				<div class="blackhead">
					<a href="{{ URL::to('course-preview/'.$courseid.'/'.$slug.'?PreviewMode=instructor') }}"><i class="fa fa-angle-left"></i> {!! Lang::get('core.back_course') !!}</a>
		
					<span class="pull-right righthide"><i class="fa fa-angle-right"></i></span>
					<span class="pull-right use-padding">
						
					</span>
				</div>
			<div class="clr-b clearfix">
				<div class="titlehead">
					@if(count($curriculum) > 0)
						@if(count($getdata)> 0) 
							@php  $sec_count = 1; @endphp
							@for($i=0;$i < count($curriculum);$i++)
								@if($curriculum[$i]->section_id == $getdata['0']->section_id)
									@php $section = $sec_count.' - '.$curriculum[$i]->title;@endphp
								@endif
								@php  $sec_count++; @endphp
							@endfor
							<h2>SECTION {!! $section !!}</h2>
							@if($quiz_page)
								<h2><span>{!! Lang::get('core.quiz_error') !!}</span></h2>
							@else
								<h2><span>{!! $getdata['0']->title !!}</span></h2>
							@endif				
						@endif
					@endif
				</div>
                
            <!--     <div class="arw-nxt">
                 <a href=""><i class="fa fa-angle-left"></i></a>
                 <a href=""><i class="fa fa-angle-right"></i></a>
                 
                 </div>-->
                
                <div class="arw-nxt">
                			@if($prev)
					<?php $combine = $prev;  $encryptid = \SiteHelpers::encryptID($combine); ?>
					<a href="{{ URL::to('learn-course-preview/'.$courseid.'/'.$slug.'/'.$encryptid) }}"><i class="fa fa-angle-left"></i></a>
					@endif 
					@if($next)
					<?php $combine = $next;  $encryptid = \SiteHelpers::encryptID($combine); ?>
					<a href="{{ URL::to('learn-course-preview/'.$courseid.'/'.$slug.'/'.$encryptid) }}"><i class="fa fa-angle-right"></i></a>
					@endif 
					
                </div>
                </div>
				
				
				<div class="contenthead text-center">

					{!! Form::open(array('url'=>'course/updatecourse', 'method' => 'post', 'class'=>'form-horizontal saveLabel','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
					{!! Form::hidden('courseid', $courseid )  !!}
					<?php
					if(count($getdata)>0 && isset($getdata['0']->media_type) && $getdata['0']->media_type=='0'){ 
						$filename1 = $getdata['0']->video_title.'.mp4'; 
						$filename2 = $getdata['0']->video_title.'.webm'; 
						$filename3 = $getdata['0']->video_title.'.ogv'; 
						?>
						<video id='lecture_video' class='video-js vjs-default-skin' controls preload='auto' width='720' height='480' data-setup='{}'><source src="{{ asset('/uploads/videos/'.$filename1) }}" type="video/mp4" id="videosource"><source src="{{ asset('/uploads/videos/'.$filename2) }}" type="video/webm" id="videosource"><source src="{{ asset('/uploads/videos/'.$filename3) }}" type="video/ogg" id="videosource"></video>
						{!! Form::hidden('mediatype', $getdata['0']->media_type )  !!}
						{!! Form::hidden('lectureid', $getdata['0']->lecture_quiz_id )  !!}
						{!! Form::hidden('sectionid', $getdata['0']->section_id )  !!}
						<?php 
					}elseif(count($getdata)>0 && isset($getdata['0']->media_type) && $getdata['0']->media_type=='1'){ 
						$filename = $getdata['0']->file_name.'.mp3'; ?>
						<audio controls id='lecture_audio'>
							<source src="{{ asset('/uploads/files/'.$filename) }}" type="audio/mpeg">
								{!! Lang::get('core.browser_support')!!}
							</audio>
							{!! Form::hidden('mediatype', $getdata['0']->media_type )  !!}
							{!! Form::hidden('lectureid', $getdata['0']->lecture_quiz_id )  !!}
							{!! Form::hidden('sectionid', $getdata['0']->section_id )  !!}
							<?php 
						}elseif(count($getdata)>0 && isset($getdata['0']->media_type) && $getdata['0']->media_type=='2'){
							$encryptid = \SiteHelpers::encryptID($getdata['0']->id);
							$filepath = URL::to('course/toread/'.$encryptid); ?>
							{{---*/ $filepath = $filepath/*---}}
							
							<iframe src="{!! $filepath !!}" width="100%" height="400px" top="100" class="pdfiframe"></iframe>
							{!! Form::hidden('mediatype', $getdata['0']->media_type )  !!}
							{!! Form::hidden('lectureid', $getdata['0']->lecture_quiz_id )  !!}
							{!! Form::hidden('sectionid', $getdata['0']->section_id )  !!}
						<?php }elseif(count($getdata)>0 && isset($getdata['0']->media_type) && $getdata['0']->media_type=='3'){ ?>
							<div class="text-left">{!! $getdata['0']->contenttext or '' !!}</div>
							{!! Form::hidden('mediatype', $getdata['0']->media_type )  !!}
							{!! Form::hidden('lectureid', $getdata['0']->lecture_quiz_id )  !!}
							{!! Form::hidden('sectionid', $getdata['0']->section_id )  !!}
						<?php
						} 
						?>
						
						{!! Form::close() !!}

						<?php

						$path =  url('')."/uploads/files/chart_14437659073265.pdf";
						// echo '<iframe src="http://localhost/eplus/uploads/files/1_14437658697565.pdf" width="100%" height="400px" top="100" class="pdfiframe"></iframe>';

						?>

					</div>
				</div>
                
				<div class="col-sm-4 nopadding lsidebar">
					<div class="tab-container">
						<ul class="nav nav-tabs side-tabs">
							<li class="nopadding active"><a data-toggle="tab" href="#lectures"><i class="icon-list"></i></a></li>
							<li class="nopadding"><a data-toggle="tab" href="#description"><i class="icon-download4"></i></a></li>
							<li class="nopadding"><a data-toggle="tab" href="#comments"><i class="icon-bubbles2"></i></a></li>
							<li class="nopadding"><a data-toggle="tab" href="#notes"><i class="icon-file4"></i></a></li>
						</ul>
						<div class="tab-content nomargin">
							<div id="lectures" class="tab-pane active">
                              <div class="wrap">
                              <div class="scrollbar">
<div class="handle">
<div class="mousearea"></div>
</div>
</div>
                              <div id="scroller" class="frame">
                              <div class="slidee">
								@if(count($curriculum)>0)
									@php $no = 1; 
									 $lno = 1;@endphp 
									@for($i=0;$i < count($curriculum);$i++)
									@php $lectures = \SiteHelpers::getlectures($curriculum[$i]->section_id); @endphp
									<ul class="curriculum-items-list-dark">
										<li class="curriculum-section-container-dark">
											<ul>
												<li class="section-title-dark">
													<h5>
														<span translate=""><span class="caps">{!! Lang::get('core.section')!!} </span></span>{!! $no !!} - {!! $curriculum[$i]->title !!}
													</h5>
												</li>
									@for($j=0;$j<count($lectures);$j++)
									@php $combine = $lectures[$j]->lecture_quiz_id;  $encryptid = \SiteHelpers::encryptID($combine); @endphp
												@if($lectures[$j]->type != '1')
												<li class="curriculum-item-container-dark">
												
														<a class="curriculum-item-dark clearfix" href="{{ URL::to('learn-course-preview/'.$courseid.'/'.$slug.'/'.$encryptid) }}">
															<!--<div class="ci-progress-container-dark">
																<span class="ci-progress-mask-dark perc-100-dark" style="width: 100%;"></span>
															</div>-->

															<div class="ci-info-dark">
																<div class="ci-title-dark">
                                                                
																	<span><span class="">{!! Lang::get('core.Lecture')!!}</span></span>
																	<span class="">{!! $lno !!}</span>:
																	<span class="title-dark">{{$lectures[$j]->title}}</span>
																</div>
																@if($lectures[$j]->media_type=='0')
																@if(isset($lectures[$j]->duration))
																<div class="ci-details-container-dark clearfix">
																	<span class="ci-details-dark">
																		<i class="fa fa-play-circle"></i>
																		<span>{!! $lectures[$j]->duration !!}</span>
																	</span>
																</div>
																@endif
																@endif
																@if($lectures[$j]->media_type=='1' || $lectures[$j]->media_type=='2' || $lectures[$j]->media_type=='5')
																@php $durations = \SiteHelpers::getlecturesfiles($lectures[$j]->lecture_quiz_id); @endphp
																@if(isset($lectures[$j]->duration))
																<div class="ci-details-container-dark clearfix">
																	<span class="ci-details-dark">
																		@if($lectures[$j]->media_type=='1')<i class="fa fa-audio"></i>@endif
																		@if($lectures[$j]->media_type=='2')<i class="fa fa-book"></i>@endif
																		@if($lectures[$j]->media_type=='5')<i class="fa fa-picture-o"></i>@endif
																		<span>{!! $durations['0']->duration !!}</span>
																	</span>
																</div>
																@endif
																@endif
																@if($lectures[$j]->media_type=='3')
																<div class="ci-details-container-dark clearfix">
																	<span class="ci-details-dark">
																		<i class="fa fa-file-text"></i>
																		<span>{!! Lang::get('core.text') !!}</span>
																	</span>
																</div>
																@endif
															</div>
														</a>
													
												</li>
												@endif
									@php $lno++; @endphp
									@endfor
												
											</ul>
										</li>
									</ul>
									@php $no++; @endphp
									@endfor
									@endif
                                    <ul class="pages"></ul>
                               </div>
                              </div>      
                              </div>
							</div>
							<div id="description" class="tab-pane use-padding description-b">
								<?php if(count($getdata)>0){
									if(isset($getdata['0']->description)){ ?>
									<h2 class="txt-orange">Lecture Description</h2>
									<p><?php echo $getdata['0']->description; ?></p>
									<?php } if(isset($getdata['0']->resources)){ 
										echo '<h4>Downloadable Resources</h4>
										<table class="table table-bordered">';
										$jsondecode = json_decode($getdata['0']->resources,true);
										foreach ($jsondecode as $resource) { 
											$getfiles 	= \SiteHelpers::getcoursefiles($resource);
											$resourceid = \SiteHelpers::encryptID($getfiles['0']->id);
											echo '<tr><td><a href="'.URL::to('download-resource/'.$resourceid).'"><i class="fa fa-file-o"></i> '.$getfiles['0']->file_title.'</a></td><td width="30"><a href="'.URL::to('download-resource/'.$resourceid).'"><i class="fa fa-download"></i></a></td></tr>';
										}
										echo '</table>';
									}
								} ?>
								
								<!--<h4 class="txt-orange">Downloadable Resources</h4>
								<table class="table table-bordered">
								<tr><td><a href="#"><i class="fa fa-file-o"></i> asdf </a></td><td width="30"><a href="#"><i class="fa fa-download"></i></a></td></tr>
								</table>-->
								
							</div>
							<div id="comments" class="tab-pane use-padding">
								<h2 class="txt-orange">{!! Lang::get('core.comments')!!}</h2>

													
									@if(count($comments)>0)
                                    <div class="para_lack_hut" id="allcomments">
									@for($i=0;$i < count($comments);$i++)
									<div class="media profile clearfix" id="comment-{{$comments[$i]->lecture_comment_id}}">
									
										<div class="activity-post lecture-preview">
											<div class="profile_image">
                                            <a href="{{ URL::to('profile/'.$comments[$i]->username) }}">
												{!! SiteHelpers::customavatar($comments[$i]->email,$comments[$i]->user_id) !!}</a>
											</div>
											<div class="activity-box discussion-activity-box">
												<div  class="activity-header clearfix" action-icon="icon-comments" action-text="posted a discussion">
												<div class="header-right clearfix pull-right">
														<div class="hidden-btns">
															
															
														</div>
														
													</div>
													<div class="header-left clearfix">
														<a href="{{ URL::to('profile/'.$comments[$i]->username) }}"><span class="activity-header-link"><span><strong>{{$comments[$i]->first_name}} {{$comments[$i]->last_name or ''}}</strong></span></span></a>
														<a>{!! Lang::get('core.post_discussion')!!}</a>
														<!-- <span class="activity-details-flex-wrapper"><a><span class=""><strong>in Lecture </strong></span></a></span> -->
														<span class="activity-actions-separator"> &middot; </span>
														<time class="activity-time nowrap"><a>{!! SiteHelpers::changeFormat($comments[$i]->created_at) !!}</a></time>
													</div>
												</div>
												<div>
													<div class="">
														<div class="activity-body" id="wholewrapper-{{$comments[$i]->lecture_comment_id}}">
															<div class="activity-title text-primary titletext" id="titletext-{{$comments[$i]->lecture_comment_id}}">{!! $comments[$i]->lecture_comment_title !!}</div>
															<div class="activity-content w3c-default profile_content" id="bodytext-{{$comments[$i]->lecture_comment_id}}">{!! $comments[$i]->lecture_comment !!}</div>
														</div>

														<div style="display:none;" id="commentopenbox-{{$comments[$i]->lecture_comment_id}}">
															<div class="form-group">
																<input type="hidden" name="pid" value="{{$comments[$i]->lecture_comment_id}}">
																<input type="text" name="posttitle" class="form-control" id="edittitle-{{$comments[$i]->lecture_comment_id}}" value="{{$comments[$i]->lecture_comment_title}}" placeholder="{!! Lang::get('core.start_discussion')!!}">
															</div>
															<div class="form-group">
																<textarea id="editctextboxcontent-{{$comments[$i]->lecture_comment_id}}" class="form-control editctextboxcontent-{{$comments[$i]->lecture_comment_id}}" rows="2" placeholder="{!! Lang::get('core.comment')!!}" name="comment" cols="50"><?php echo $comments[$i]->lecture_comment; ?></textarea> 
															</div> 
															<div class="form-group">              
																<button class="btn btn-orange updatecommentbtn" type="button" data-commentid="{{$comments[$i]->lecture_comment_id}}" >{!! Lang::get('core.sb_save')!!}</button>
																<button class="btn btn-danger editccancelbtn" data-commentid="{{$comments[$i]->lecture_comment_id}}" type="button">{!! Lang::get('core.sb_cancel')!!}</button>
															</div>
														</div>

														@php ($replys = SiteHelpers::getlecturereplies($comments[$i]->lecture_comment_id) )
														@if(count($replys)>0)
														<div class="activity-actions clearfix">
															<a href="javascript:void(0)" class="show-reply">
																<span>
																	<span><span class="text-primary">{!! Lang::get('core.grid_show')!!} {!! count($replys)!!} {!! Lang::get('core.replies')!!}</span></span>
																	
																</span>
															</a>
														</div>
														@endif
														

														<div class="commentReplyBox" style="display:none;" id="openbox-{{$comments[$i]->lecture_comment_id}}">
															<input type="hidden" name="pid" value="{{$comments[$i]->lecture_comment_id}}">
															<textarea id="textboxcontent-{{$comments[$i]->lecture_comment_id}}" class="form-control lectureeditor textboxcontent-{{$comments[$i]->lecture_comment_id}}" rows="2" placeholder="{!! Lang::get('core.comment')!!}" name="comment" cols="50"></textarea>                
															<button class="btn btn-orange replybtn" type="button" data-commentid="{{$comments[$i]->lecture_comment_id}}">{!! Lang::get('core.Reply')!!}</button>
															<button class="btn btn-danger cancelbtn" data-commentid="{{$comments[$i]->lecture_comment_id}}" type="button">{!! Lang::get('core.sb_cancel')!!}</button>
														</div>
														
														<div class="discussion-comments-block" style="display:none;" id="repc-{{$comments[$i]->lecture_comment_id}}">
															<div class="discussion-comments-container-dark" id="repcd-{{$comments[$i]->lecture_comment_id}}">
																@if(count($replys)>0)

																@for($j=0;$j < count($replys);$j++)
																<li id="reply-block{!!$replys[$j]->reply_id!!}">
																	<div>
																		<div class="comment-box">
																			<span class="thumb-wrapper"><a href="{{ URL::to('profile/'.$replys[$j]->username) }}">
																				{!! SiteHelpers::customavatar($replys[$j]->email,$replys[$j]->user_id, '', 'thumb') !!}
																			</a></span>

																			<div class="activity-box">
																				<div class="activity-header">
																					<div class="header-right clearfix pull-right">
																						
																					</div>
																					<div class="header-left">
																						<a class="activity-header-link" href="{{ URL::to('profile/'.$replys[$j]->username) }}">{{$replys[$j]->first_name}} {{$replys[$j]->last_name or ''}}</a>
																						<time class="activity-time"> &middot; {!! SiteHelpers::changeFormat($replys[$j]->created_at) !!}</time>
																					</div>
																				</div>
																				<div class="comment-body w3c-default" id="replytext-{!!$replys[$j]->reply_id!!}"><p><?php echo $replys[$j]->reply_comment; ?></p></div>
																				<div style="display:none;" id="replyopenbox-{!!$replys[$j]->reply_id!!}">
																					<input type="hidden" name="pid" value="{!!$replys[$j]->reply_id!!}">
																					<textarea id="edittextboxcontent-{!!$replys[$j]->reply_id!!}" class="form-control lectureeditor edittextboxcontent-{!!$replys[$j]->reply_id!!}" rows="2" placeholder="{!! Lang::get('core.comment')!!}" name="comment" cols="50"><?php echo $replys[$j]->reply_comment; ?></textarea>                
																					<button class="btn btn-info updatereplybtn" type="button" data-commentid="{!!$replys[$j]->lecture_comment_id!!}" data-replyid="{!!$replys[$j]->reply_id!!}">{!! Lang::get('core.sb_save')!!}</button>
																					<button class="btn btn-danger editcancelbtn" data-commentid="{!!$replys[$j]->reply_id!!}" type="button">{!! Lang::get('core.sb_cancel')!!}</button>
																				</div>
																			</div>
																		</div>
																	</div>
																</li>
																@endfor
																@endif
																
															</div>
														</div>
														
													</div>
												</div>
											</div>
										</div>
									
									</div>

									
										
                                        
										@endfor
                                        </div>
										@endif

							   </div>
                            <div id="notes" class="tab-pane">
									<h2 class="txt-orange use-padding">{!! Lang::get('core.Notes') !!}</h2>
									
									<div style="display:block; ">
										<table class="table" id="listnotes">
											<?php
											if(count($notes)>0){
												for ($k=0; $k < count($notes); $k++) { 
													echo '<tr><td>'.$notes[$k]->notes.'</td></tr>';
												}
											}

											?>										
										</table>

									</div>
									
									</div>      
						</div>
					</div>
				</div>
                
		</div>
       </div>
</div>
		

				<script type="text/javascript">
				$('body').on('click', '.show-reply', function()
				{
					$(this).parents('.discussion-activity-box').find('.discussion-comments-block').slideToggle();
				});

				$(function(){
				
					$('.righthide').click(function(){
						if($(this).hasClass('fullshow')) {
							$(this).find('i').addClass('fa-angle-right');
							$(this).find('i').removeClass('fa-angle-left');
							$(this).removeClass('fullshow');
							$('.contentView').removeClass('col-md-12');
							$('.contentView').addClass('col-md-8');
							$('.lsidebar').show();
						} else {
							$(this).find('i').removeClass('fa-angle-right');
							$(this).find('i').addClass('fa-angle-left');
							$(this).addClass('fullshow');
							$('.contentView').removeClass('col-md-8');
							$('.contentView').addClass('col-md-12');
							$('.lsidebar').hide();
						}
					});

					$('input[type="checkbox"],input[type="radio"]').iCheck({
						checkboxClass: 'icheckbox_square-green',
						radioClass: 'iradio_square-green',
					});	

					$('.updatelstatus').on('ifChecked', function(event){
						var lid 		= $(this).data('lid');
						var courseid 	= $(this).data('courseid');
						updatestatus(lid,courseid,1);
					});

					$('.updatelstatus').on('ifUnchecked', function(event){
						var lid 		= $(this).data('lid');
						var courseid 	= $(this).data('courseid');
						updatestatus(lid,courseid,0);
					});

					tinymce.init({	
						mode : "specific_textareas",
						editor_selector : "lectureeditor",
						theme : "advanced",
						theme_advanced_buttons1 : "bold,italic,underline,bullist,numlist,link,image",
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "bottom",
						width : "100%",
						plugins : "paste",
						paste_text_sticky : true,
						setup : function(ed) {
							ed.onInit.add(function(ed) {
								ed.pasteAsPlainText = true;
							});
						}
					});

					$('#takenotes').keypress(function(e) {
						if (e.keyCode == '13') {
							e.preventDefault();

							var gettype 	= $('[name="mediatype"]').val();
							var lectureid 	= $('[name="lectureid"]').val();
							var sectionid 	= $('[name="sectionid"]').val();
							var notes 		= $(this).val();
							$('#takenotes').val('');
						// alert(lectureid);
						// alert(notes);
						if(gettype=='0'){
							var vid = document.getElementById("lecture_video");
							var length  = parseInt(vid.currentTime);
						}else if(gettype=='1'){
							var vid = document.getElementById("lecture_audio");
							var length  = parseInt(vid.currentTime);
						}else{
							var length  = 0;
						}
						
						var converts   = secondsTimeSpanToHMS(length);
						// alert(converts);
						

						if($.trim(notes).length>0){

							var me = $(this);
							if ( me.data('requestRunning') ) {
								return;
							}
							me.data('requestRunning', true);

							$.ajaxSetup({
								headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
							});
							$.ajax({
								type: 'POST',
								url: '<?php echo e(\URL::to("course/takenotes")); ?>',
								data:'lid='+lectureid+'&sectionid='+sectionid+'&duration='+converts+'&notes='+notes,
								success : function(data) {
									$('#takenotes').val('');
									$('#listnotes').append('<tr><td width="70"><span class="label label-warning">'+converts+'</span></td><td>'+notes+' <a href="javascript:void(0);" class="pull-right removenotes" data-noteid="'+data+'"><i class="fa fa-times"></i></a></td></tr>');
									$('#downloadlink').html('<a href="{{ \URL::to("download-notes") }}/'+lectureid+'" class="btn btn-orange">Download Your Notes</a>');

								},complete: function() {
									me.data('requestRunning', false);
								}
							});
						}

					}
				}); //end keypress

$(document).on('click','.removenotes',function(){
	var removeid = $(this).data('noteid');
	var removelength = $('.removenotes').length;
					// alert(removelenght);
					var r = confirm("{!! Lang::get('core.notes_delete') !!}");
					if (r == true) {
						$(this).closest('tr').remove();
						if(removelength=='1'){
							$('#downloadlink').html('');
						}
						var me = $(this);
						if ( me.data('requestRunning') ) {
							return;
						}
						me.data('requestRunning', true);

						$.ajaxSetup({
							headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
						});
						$.ajax({
							type: 'POST',
							url: '<?php echo e(\URL::to("course/removenotes")); ?>',
							data:'noteid='+removeid,
							success : function(data) {
								
							},complete: function() {
								me.data('requestRunning', false);
							}
						});

					}
				});


				//comments start here

				$(document).on('click','.replybutton',function () { 
					var cid = $(this).data('commentid');
					if ($('#openbox-'+cid).is(':visible')) { 
						$("#openbox-"+cid).slideUp(300); 
					} 
					if ($("#openbox-"+cid).is(':visible')) { 
						$("#openbox-"+cid).slideUp(300);
					} else {
						$("#openbox-"+cid).slideDown(300); 
						ajaxtinymce('textboxcontent-'+cid);
					} 
				});

				$(document).on('click','.cancelbtn',function () { 
					var cid = $(this).data('commentid');
					if ($('#openbox-'+cid).is(':visible')) { 
						$("#openbox-"+cid).slideUp(300); 
					} 
					if ($("#openbox-"+cid).is(':visible')) { 
						$("#openbox-"+cid).slideUp(300);
					} else {
						$("#openbox-"+cid).slideDown(300); 

					} 
				});

				$(document).on('click','.editcancelbtn',function () { 
					var cid = $(this).data('commentid');
					if ($('#replyopenbox-'+cid).is(':visible')) { 
						$("#replyopenbox-"+cid).slideUp(300); 
						$('#replytext-'+cid).show();
					} 
					if ($("#replyopenbox-"+cid).is(':visible')) { 
						$("#replyopenbox-"+cid).slideUp(300);
						$('#replytext-'+cid).show();
					} else {
						$("#replyopenbox-"+cid).slideDown(300); 
						$('#replytext-'+cid).show();
					} 
				});

				$(document).on('click','.editccancelbtn',function () { 
					var cid = $(this).data('commentid');
					if ($('#commentopenbox-'+cid).is(':visible')) { 
						$("#commentopenbox-"+cid).slideUp(300); 
						$('#wholewrapper-'+cid).show();
					} 
					if ($("#commentopenbox-"+cid).is(':visible')) { 
						$("#commentopenbox-"+cid).slideUp(300);
						$('#wholewrapper-'+cid).show();
					} else {
						$("#commentopenbox-"+cid).slideDown(300); 
						$('#wholewrapper-'+cid).show();
					} 
				});
				
				$(document).on('click','.replybtn',function(e){
					var cid = $(this).data('commentid');
					var textvalues = tinyMCE.get('textboxcontent-'+cid).getContent();//$('#textboxcontent-'+cid).val();
					if($.trim(textvalues).length===0){
						toastr.error("Error", "{!! Lang::get('core.enter_valid_comments')!!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}
					}else if ($.trim(textvalues).length>0){
						var me = $(this);
						e.preventDefault();
						if ( me.data('requestRunning') ) {
							return;
						}
						me.data('requestRunning', true);
						var lectureid 	= $('[name="lectureid"]').val();
						$.ajaxSetup({
							headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
						});
						$.ajax({
							type: 'POST',
							url: '<?php echo e(\URL::to("course/insertlecturereply")); ?>',
							data:'commentid='+cid+'&comments='+textvalues+'&lid='+lectureid+'&render_file=true',
							success : function(data) {
								$('#openbox-'+cid).hide();
									// $('#textboxcontent-'+cid).val('');
									tinyMCE.get('textboxcontent-'+cid).setContent('');
									$('#repc-'+cid).show();
									$('#repcd-'+cid).append(data);
									
									//$('.discussion-comments-container-dark').append(data);

									// ajaxtinymce();

									toastr.success("Success", "{!! Lang::get('core.comment_thanks')!!}");
									toastr.options = {
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-bottom-right",
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"timeOut": "5000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut"
									}

								// var obj = jQuery.parseJSON(data);
								// if(obj.status=='1'){
								// 	$('#openbox-'+cid).hide();
								// 	// $('#textboxcontent-'+cid).val('');
								// 	tinyMCE.get('textboxcontent-'+cid).setContent('');

								// 	$('.discussion-comments-container-dark').append(data);

								// 	ajaxtinymce();

								// 	toastr.success("Success", "Thank you for reply this comment.");
								// 	toastr.options = {
								// 		"closeButton": true,
								// 		"debug": false,
								// 		"positionClass": "toast-bottom-right",
								// 		"onclick": null,
								// 		"showDuration": "300",
								// 		"hideDuration": "1000",
								// 		"timeOut": "5000",
								// 		"extendedTimeOut": "1000",
								// 		"showEasing": "swing",
								// 		"hideEasing": "linear",
								// 		"showMethod": "fadeIn",
								// 		"hideMethod": "fadeOut"
								// 	}
								// }else if(obj.status=='2'){
								// 	toastr.error("Error", "Sorry! Something went wrong please try again later.");
								// 	toastr.options = {
								// 		"closeButton": true,
								// 		"debug": false,
								// 		"positionClass": "toast-bottom-right",
								// 		"onclick": null,
								// 		"showDuration": "300",
								// 		"hideDuration": "1000",
								// 		"timeOut": "5000",
								// 		"extendedTimeOut": "1000",
								// 		"showEasing": "swing",
								// 		"hideEasing": "linear",
								// 		"showMethod": "fadeIn",
								// 		"hideMethod": "fadeOut"
								// 	}
								// }

								return false;
							},complete: function() {
								me.data('requestRunning', false);
							}
						});
}

});


$(document).on('click','.postbtn',function(e){

	var texttitles = $('#posttitle').val();
	// var textvalues = $('#postcomments').val();
	var textvalues = tinyMCE.get('postcomments').getContent();

	// alert(textvalues);

	if($.trim(texttitles).length===0){
		toastr.error("Error", "{!! Lang::get('core.discussion_valid_title')!!}");
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
	}else if($.trim(textvalues).length===0){

		toastr.error("Error", "{!! Lang::get('core.enter_valid_comments') !!}");
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}

	}else if ($.trim(textvalues).length>0 && $.trim(texttitles).length>0){
		var me = $(this);
		var lectureid 	= $('[name="lectureid"]').val();
		var sectionid 	= $('[name="sectionid"]').val();
		var courseid    = $('[name="courseid"]').val();
		e.preventDefault();
		if ( me.data('requestRunning') ) {
			return;
		}

		me.data('requestRunning', true);

		$.ajaxSetup({
			headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
		});
		$.ajax({
			type: 'POST',
			url: '<?php echo e(\URL::to("course/insertlecturecomments")); ?>',
			data:'comments='+textvalues+'&title='+texttitles+'&lid='+lectureid+'&sid='+sectionid+'&cid='+courseid+'&render_file=true',
			success : function(data) {
				$('#posttitle').val('');
					tinyMCE.get('postcomments').setContent('');
					$('.textareablock').css('display','none');
					$('#allcomments').append(data);
					// ajaxtinymce();
					toastr.success("Success", "{!! Lang::get('core.disccusion_comment_success') !!}");
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}

				// var obj = jQuery.parseJSON(data);
				// if(obj.status=='1'){
				// 	$('#posttitle').val('');
				// 	tinyMCE.get('postcomments').setContent('');
				// 	$('.textareablock').css('display','none');
				// 	$('#allcomments').append('<div id="comment-'+obj.results.commentid+'" class="media profile clearfix"><div class="profile_image"><a href=""> '+obj.results.imgpath+'</a></div><div class="profile_title"> <a href="">'+obj.results.fname+' </a> <a href="javascript:void(0);" data-commentid="'+obj.results.commentid+'" class="pull-right removecomments"><i class="fa fa-trash-o"></i></a> <a href="javascript:void(0);" style="padding-right: 10px;" data-commentid="'+obj.results.commentid+'" class="pull-right editcomments"><i class="fa fa-edit"></i></a> </div><div id="wholewrapper-'+obj.results.commentid+'"><h4 id="titletext-'+obj.results.commentid+'">'+obj.results.texttitles+'</h4><div class="profile_content" id="bodytext-'+obj.results.commentid+'">'+obj.results.comments+'</div></div><div style="display:none;" id="commentopenbox-'+obj.results.commentid+'"><div class="form-group"><input type="hidden" name="pid" value="'+obj.results.commentid+'"><input type="text" name="posttitle" class="form-control" id="edittitle-'+obj.results.commentid+'" value="'+obj.results.texttitles+'" placeholder="Start a new discussion"></div><div class="form-group"><textarea id="editctextboxcontent-'+obj.results.commentid+'" class="form-control lectureeditor" rows="2" placeholder="Comment" name="comment" cols="50">'+obj.results.comments+'</textarea></div> <div class="form-group"><button class="btn btn-info updatecommentbtn" type="button" data-commentid="'+obj.results.commentid+'" >Save</button><button class="btn btn-danger editccancelbtn" data-commentid="'+obj.results.commentid+'" type="button">Cancel</button></div></div><div class="about_hours"><abbr class="timeago">Just now</abbr>&nbsp;<a data-commentid="'+obj.results.commentid+'" id="openreplybox" class="replybutton">Reply</a></div><div id="openbox-'+obj.results.commentid+'" style="display:none;" class="commentReplyBox"><input type="hidden" value="'+obj.results.commentid+'" name="pid"><textarea cols="50" name="comment" placeholder="Comment" rows="2" class="form-control lectureeditor" id="textboxcontent-'+obj.results.commentid+'"></textarea><button  data-commentid="'+obj.results.commentid+'" type="button" class="btn btn-info replybtn">Reply</button><button type="button" data-commentid="'+obj.results.commentid+'" class="btn btn-danger cancelbtn">Cancel</button></div></div><div id="replyies-'+obj.results.commentid+'"></div>');
				// 	ajaxtinymce();
				// 	toastr.success("Success", "Thank you for post this comment.");
				// 	toastr.options = {
				// 		"closeButton": true,
				// 		"debug": false,
				// 		"positionClass": "toast-bottom-right",
				// 		"onclick": null,
				// 		"showDuration": "300",
				// 		"hideDuration": "1000",
				// 		"timeOut": "5000",
				// 		"extendedTimeOut": "1000",
				// 		"showEasing": "swing",
				// 		"hideEasing": "linear",
				// 		"showMethod": "fadeIn",
				// 		"hideMethod": "fadeOut"
				// 	}
				// }else if(obj.status=='2'){
				// 	toastr.error("Error", "Sorry! Something went wrong please try again later.");
				// 	toastr.options = {
				// 		"closeButton": true,
				// 		"debug": false,
				// 		"positionClass": "toast-bottom-right",
				// 		"onclick": null,
				// 		"showDuration": "300",
				// 		"hideDuration": "1000",
				// 		"timeOut": "5000",
				// 		"extendedTimeOut": "1000",
				// 		"showEasing": "swing",
				// 		"hideEasing": "linear",
				// 		"showMethod": "fadeIn",
				// 		"hideMethod": "fadeOut"
				// 	}
				// }

				return false;
			},complete: function() {
				me.data('requestRunning', false);
			}
		});
}

});

$('#posttitle').focus(function(){
	$('.textareablock').css('display','block');

});

$(document).on('click','.removecomments',function(){
	var removeid  = $(this).data('commentid');
				// alert(removeid);

				var r = confirm("{!! Lang::get('core.co_delete_confirm') !!}");
				if (r == true) {
					$('#comment-'+removeid).remove();
					
					var me = $(this);
					if ( me.data('requestRunning') ) {
						return;
					}
					me.data('requestRunning', true);

					$.ajaxSetup({
						headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
					});
					$.ajax({
						type: 'POST',
						url: '<?php echo e(\URL::to("course/removelecturecomments")); ?>',
						data:'cid='+removeid,
						success : function(data) {
							
						},complete: function() {
							me.data('requestRunning', false);
						}
					});

				}

			});

$(document).on('click','.removereply',function(){
	var removeid  = $(this).data('rcommentid');
				// alert(removeid);

				var r = confirm("{!! Lang::get('core.co_delete_confirm') !!}");
				if (r == true) {
					$('#reply-block'+removeid).remove();
					
					var me = $(this);
					if ( me.data('requestRunning') ) {
						return;
					}
					me.data('requestRunning', true);

					$.ajaxSetup({
						headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
					});
					$.ajax({
						type: 'POST',
						url: '<?php echo e(\URL::to("course/removelecturereply")); ?>',
						data:'rid='+removeid,
						success : function(data) {
							
						},complete: function() {
							me.data('requestRunning', false);
						}
					});

				}

			});


$(document).on('click','.editreply',function(){
	var replyid = $(this).data('rcommentid');
				// alert(replyid);
				$('#replytext-'+replyid).css('display','none');
				$('#replyopenbox-'+replyid).css('display','block');
				ajaxtinymce('edittextboxcontent-'+replyid);
			});

$(document).on('click','.editcomments',function(){
	var cid = $(this).data('commentid');
				// alert(cid);
				$('#wholewrapper-'+cid).css('display','none');
				$('#commentopenbox-'+cid).css('display','block');
				ajaxtinymce('editctextboxcontent-'+cid);
			});

$(document).on('click','.updatereplybtn',function(e){
	var rid = $(this).data('replyid');
	var cid = $(this).data('commentid');
					var textvalues = tinyMCE.get('edittextboxcontent-'+rid).getContent();//$('#textboxcontent-'+cid).val();

					if($.trim(textvalues).length===0){
						toastr.error("Error", "{!! Lang::get('core.enter_valid_comments') !!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}
					}else if ($.trim(textvalues).length>0){
						var me = $(this);
						e.preventDefault();
						if ( me.data('requestRunning') ) {
							return;
						}
						me.data('requestRunning', true);
						var lectureid 	= $('[name="lectureid"]').val();
						$.ajaxSetup({
							headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
						});
						$.ajax({
							type: 'POST',
							url: '<?php echo e(\URL::to("course/updatelecturereply")); ?>',
							data:'commentid='+cid+'&comments='+textvalues+'&lid='+lectureid+'&rid='+rid,
							success : function(data) {
								var obj = jQuery.parseJSON(data);
								if(obj.status=='1'){
									$('#replytext-'+rid).show();
									$('#replyopenbox-'+rid).hide();
									// tinyMCE.get('edittextboxcontent-'+rid).setContent('');
									$('#replytext-'+rid).html(obj.results.comments);
									// $('#replyies-'+obj.results.commentid).append('<div id="reply-block'+obj.results.replyid+'" class="media"><hr><a href="" class="pull-right bla">'+obj.results.imgpath+'</a><a href="javascript:void(0);" data-rcommentid="'+obj.results.replyid+'" class="pull-right removereply"><i class="fa fa-trash-o"></i></a><div class="media-body"><h4 class="media-heading comment"><a href="">'+obj.results.fname+'</a><span class="msg-time pull-right"><span><abbr class="timeago">Just Now</abbr>&nbsp;</span></span></h4>'+obj.results.comments+'</div></div>');
									toastr.success("Success", "{!! Lang::get('core.comment_thanks') !!}");
									toastr.options = {
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-bottom-right",
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"timeOut": "5000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut"
									}
								}else if(obj.status=='2'){
									toastr.error("Error", "{!! Lang::get('core.comments_error') !!}");
									toastr.options = {
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-bottom-right",
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"timeOut": "5000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut"
									}
								}

								return false;
							},complete: function() {
								me.data('requestRunning', false);
							}
						});
}

});



$(document).on('click','.updatecommentbtn',function(e){
	var cid  = $(this).data('commentid');
	var texttitles = $('#edittitle-'+cid).val();
					// var textvalues = $('#editctextboxcontent-'+cid).val();
					var textvalues = tinyMCE.get('editctextboxcontent-'+cid).getContent();

					// alert(textvalues);

					if($.trim(texttitles).length===0){
						toastr.error("Error", "{!! Lang::get('core.discussion_valid_title') !!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}
					}else if($.trim(textvalues).length===0){

						toastr.error("Error", "{!! Lang::get('core.enter_valid_comments')!!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}

					}else if ($.trim(textvalues).length>0 && $.trim(texttitles).length>0){
						var me = $(this);
						var lectureid 	= $('[name="lectureid"]').val();
						var sectionid 	= $('[name="sectionid"]').val();
						e.preventDefault();
						if ( me.data('requestRunning') ) {
							return;
						}

						me.data('requestRunning', true);

						$.ajaxSetup({
							headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
						});
						$.ajax({
							type: 'POST',
							url: '<?php echo e(\URL::to("course/updatelecturecomments")); ?>',
							data:'comments='+textvalues+'&title='+texttitles+'&lid='+lectureid+'&cid='+cid,
							success : function(data) {
								var obj = jQuery.parseJSON(data);
								if(obj.status=='1'){
									// $('#posttitle').val('');
									// tinyMCE.get('postcomments').setContent('');
									// $('.textareablock').css('display','none');
									$('#commentopenbox-'+cid).css('display','none');
									$('#wholewrapper-'+cid).css('display','block');
									$('#titletext-'+cid).html(texttitles);
									$('#bodytext-'+cid).html(textvalues);
									toastr.success("Success", "{!! Lang::get('core.disccusion_comment_success') !!}");
									toastr.options = {
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-bottom-right",
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"timeOut": "5000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut"
									}
								}else if(obj.status=='2'){
									toastr.error("Error", "{!! Lang::get('core.comments_error') !!}");
									toastr.options = {
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-bottom-right",
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"timeOut": "5000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut"
									}
								}

								return false;
							},complete: function() {
								me.data('requestRunning', false);
							}
						});
}

});



			}); //end functions
function secondsTimeSpanToHMS(s) {
			    var h = Math.floor(s/3600); //Get whole hours
			    s -= h*3600;
			    var m = Math.floor(s/60); //Get remaining minutes
			    s -= m*60;
			    if(h>0){
			    	return h+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
			    }else{
			    	return (m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
			    }
			}

			function ajaxtinymce(dynamic_class = ''){
				
				if(dynamic_class == '')
				{
					var selector = 'lectureeditor';
				}
				else
				{
					var selector = dynamic_class;
				}
				
				if ($("#"+selector+'_parent').length > 0) 
				{
					    
				}
				else
				{
					tinymce.init({	
						mode : "specific_textareas",
						editor_selector : selector,
						theme : "advanced",
						theme_advanced_buttons1 : "bold,italic,underline,bullist,numlist,link,image",
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "bottom",
						width : "100%",
						plugins : "paste",
						paste_text_sticky : true,
						setup : function(ed) {
							ed.onInit.add(function(ed) {
								ed.pasteAsPlainText = true;
							});
						}
					});
				}
				
			}

			function updatestatus(lid,cid,sid){

				$.ajaxSetup({
					headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
				});
				$.ajax({
					type: 'POST',
					url: '<?php echo e(\URL::to("course/updatecoursestatus")); ?>',
					data:'lid='+lid+'&cid='+cid+'&sid='+sid,
					success : function(data) {

					}
				});
			}

			$(document).ready(function(e){
				@if(!$completion_status)
					var lid 		= $('.updatelstatus').data('lid');
					var courseid 	= $('.updatelstatus').data('courseid');
					updatestatus(lid,courseid,0);
				@endif
			});
			</script>


		</body> 
		</html>