<option value="">{!! Lang::get('core.select_subcategory')!!}
@if(!empty($results))
@foreach($results as $subcats)
@if($subcats->sub_cat_id == $sid)
<option value="{!!  $subcats->sub_cat_id  !!}" selected="selected">{!!  $subcats->sub_name  !!}</option>
@else
<option value="{!!  $subcats->sub_cat_id  !!}" >{!!  $subcats->sub_name  !!}</option>
@endif
@endforeach
@endif