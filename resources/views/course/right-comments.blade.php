<div class="media profile clearfix" id="comment-{{$comment->lecture_comment_id}}">
<div class="activity-post lecture-preview">
	<div class="profile_image"><a href="{{ URL::to('profile/'.$comment->username) }}">
		{!! SiteHelpers::customavatar($comment->email,$comment->user_id,'small') !!}</a>
	</div>
	<div class="activity-box discussion-activity-box">
		<div  class="activity-header clearfix" action-icon="icon-comments" action-text="posted a discussion">
		<div class="header-right clearfix pull-right">
				<div class="hidden-btns">
					<a href="javascript:void(0);" data-commentid="{!!$comment->lecture_comment_id!!}" class="pull-right removecomments"><i class="fa fa-trash-o"></i></a> &nbsp;&nbsp;&nbsp;
					<a href="javascript:void(0);" style="padding-right: 10px;" data-commentid="{!!$comment->lecture_comment_id!!}" class="pull-right editcomments"><i class="fa fa-pencil"></i></a>
				</div>
		</div>
			<div class="header-left clearfix">
				<a href="{{ URL::to('profile/'.$comment->username) }}"><span class="activity-header-link"><span><strong>{{$comment->first_name}} {{$comment->last_name or ''}}</strong></span></span></a>
				<a>{!! Lang::get('core.post_discussion')!!}</a>
				<span class="activity-actions-separator"> &middot; </span>
				<time class="activity-time nowrap"><a>{!! SiteHelpers::changeFormat($comment->created_at) !!}</a></time>
			</div>
		</div>
		<div>
			<div class="">
				<div class="activity-body" id="wholewrapper-{{$comment->lecture_comment_id}}">
					<div class="activity-title text-primary titletext" id="titletext-{{$comment->lecture_comment_id}}">{!! $comment->lecture_comment_title !!}</div>
					<div class="activity-content w3c-default profile_content" id="bodytext-{{$comment->lecture_comment_id}}">{!! $comment->lecture_comment !!}</div>
				</div>

				<div style="display:none;" id="commentopenbox-{{$comment->lecture_comment_id}}">
					<div class="form-group">
						<input type="hidden" name="pid" value="{{$comment->lecture_comment_id}}">
						<input type="text" name="posttitle" class="form-control" id="edittitle-{{$comment->lecture_comment_id}}" value="{{$comment->lecture_comment_title}}" placeholder="Start a new discussion">
					</div>
					<div class="form-group">
						<textarea id="editctextboxcontent-{{$comment->lecture_comment_id}}" class="form-control lectureeditor editctextboxcontent-{{$comment->lecture_comment_id}}" rows="2" placeholder="Comment" name="comment" cols="50"><?php echo $comment->lecture_comment; ?></textarea> 
					</div> 
					<div class="form-group">              
						<button class="btn btn-orange updatecommentbtn" type="button" data-commentid="{{$comment->lecture_comment_id}}" >{!! Lang::get('core.sb_save')!!}</button>
						<button class="btn btn-danger editccancelbtn" data-commentid="{{$comment->lecture_comment_id}}" type="button">{!! Lang::get('core.cancel')!!}</button>
					</div>
				</div>

				<a class="replybutton" id="openreplybox" data-commentid="{{$comment->lecture_comment_id}}" href="javascript::">{!! Lang::get('core.Reply')!!}</a>
				
				<div class="commentReplyBox" style="display:none;" id="openbox-{{$comment->lecture_comment_id}}">
					<input type="hidden" name="pid" value="{{$comment->lecture_comment_id}}">
					<textarea id="textboxcontent-{{$comment->lecture_comment_id}}" class="form-control lectureeditor textboxcontent-{{$comment->lecture_comment_id}}" rows="2" placeholder="Comment" name="comment" cols="50"></textarea>                
					<button class="btn btn-orange replybtn" type="button" data-commentid="{{$comment->lecture_comment_id}}"> {!! Lang::get('core.Reply')!!}</button>
					<button class="btn btn-danger cancelbtn" data-commentid="{{$comment->lecture_comment_id}}" type="button">{!! Lang::get('core.cancel')!!}</button>
				</div>

				<div class="discussion-comments-block" style="display:none;" id="repc-{{$comment->lecture_comment_id}}">
					<div class="discussion-comments-container-dark" id="repcd-{{$comment->lecture_comment_id}}">

					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>

	
</div>

<script type="text/javascript">
$('.lectureeditor').redactor({
				    buttons: ['format', 'bold', 'italic','image']
				});
</script>