@extends('layouts.frontend')
@section('content')
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/css/taginputs/jquery.tagsinput.css') }}" rel="stylesheet">
<style type="text/css">
.form-group{ margin:25px 0px; }
.leftnopad{padding-left:0px !important;}
</style>
<div class="create-course">
<div class="container">
	<div class="row">
		<div class="col-sm-12">
            @include('course.courseheader')
        </div>
		<div class="col col-sm-3 multi_development">
			@include('course.createsidebar')
		</div>
	

	</div>
</div>
</div>
<script type="text/javascript">
$(function(){

    $("#coursecreateform").validate({
        ignore: [],
        rules: {
            coursename: {
                 required: true,
                // minlength: 5,
                maxlength: 60,
                customwhitespace: true,
                remote: {
                    url: "{{ URL::to('course/coursenameexits') }}",
                    type: "GET",
                    data: {
                        coursename: function() {
                            return $( "#coursename" ).val();
                        }
                    }
                }
            },
            coursetype: {
                required: true,
            },
       
        },
        messages: {
            coursename: {
                required: "{{ Lang::get('core.enter_course') }}",
                // minlength: "Course name must consist of at least 5 characters",
                maxlength: "{{ Lang::get('core.no_length') }}",
                remote: "{{ Lang::get('core.course_taken') }}"
            },           
          
        },submitHandler: function() {
            $('#createcourse').prop("disabled", true);
            return true;
        }
    });


});
</script>
@stop