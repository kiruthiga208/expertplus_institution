<link href="{{ asset('assets/bsetec/css/jquery.custom-scrollbar.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('assets/bsetec/js/jquery.custom-scrollbar.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/redactor/js/redactor.js') }}"></script>
<style type="text/css">
        html, body {
            padding: 0;
            margin: 0;
        }
.tab-content > .tab-pane { position:relative;  }

        .overview {
            height: auto;
        }
    </style>

    {!! Form::hidden('lectureid', $getdata['0']->lecture_quiz_id )  !!}
	{!! Form::hidden('sectionid', $getdata['0']->section_id )  !!}

<div class="col-sm-4 nopadding lsidebar">
					<div class="tab-container">
						<ul class="nav nav-tabs side-tabs">
							<li class="nopadding active"><a data-toggle="tab" href="#lectures"><i class="icon-list"></i></a></li>
							<li class="nopadding"><a data-toggle="tab" href="#description"><i class="icon-download4"></i></a></li>
							<li class="nopadding"><a data-toggle="tab" href="#comments"><i class="icon-bubbles2"></i></a></li>
							<li class="nopadding"><a data-toggle="tab" href="#notes"><i class="icon-file4"></i></a></li>
						</ul>
						<div class="tab-content nomargin">
							<div id="lectures" class="tab-pane active">
                               <div class="cum_scrollbar default-skin">
								@if(count($curriculum)>0)
									@php  $no = 1; 
									 $lno = 1; 
									$qno = 1; @endphp
									@for($i=0;$i < count($curriculum);$i++)
									@php $lectures = \SiteHelpers::getlectures($curriculum[$i]->section_id); @endphp
									<ul class="curriculum-items-list-dark">
										<li class="curriculum-section-container-dark">
											<ul>
												<li class="section-title-dark">
													<h5>
														<span translate=""><span class="caps">Section </span></span>{!! $no !!} - {!! $curriculum[$i]->title !!}
													</h5>
												</li>
									@for($j=0;$j<count($lectures);$j++)
									@php $combine = $lectures[$j]->lecture_quiz_id;  $encryptid = \SiteHelpers::encryptID($combine); @endphp
									
												<li class="curriculum-item-container-dark">
												
														<a class="curriculum-item-dark clearfix" href="{{ URL::to('learn-course/'.$courseid.'/'.$slug.'/'.$encryptid) }}">
															<!--<div class="ci-progress-container-dark">
																<span class="ci-progress-mask-dark perc-100-dark" style="width: 100%;"></span>
															</div>-->

															<div class="ci-info-dark">
																<div class="ci-title-dark">
                                                                @if($lectures[$j]->type==0)
																<span><span class=""> {!! Lang::get('core.Lecture')!!} </span></span>
																<span class="">{!! $lno !!}</span>:
																@else
																<span><span class=""> {!! Lang::get('core.Quiz')!!} </span></span>
																<span class="">{!! $qno !!}</span>:
																@php $lno--;$qno++; @endphp
																@endif
																<span class="title-dark">{{$lectures[$j]->title}}</span>
																</div>
																@if($lectures[$j]->media_type=='0')
																@if(isset($lectures[$j]->duration))
																<div class="ci-details-container-dark clearfix">
																	<span class="ci-details-dark">
																		<i class="fa fa-play-circle"></i>
																		<span>{!! $lectures[$j]->duration !!}</span>
																	</span>
																</div>
																@endif
																@endif
																@if($lectures[$j]->media_type=='1' || $lectures[$j]->media_type=='2' || $lectures[$j]->media_type=='5')
																@php $durations = \SiteHelpers::getlecturesfiles($lectures[$j]->lecture_quiz_id); @endphp
																@if(isset($lectures[$j]->duration))
																<div class="ci-details-container-dark clearfix">
																	<span class="ci-details-dark">
																		@if($lectures[$j]->media_type=='1')<i class="fa fa-audio"></i>@endif
																		@if($lectures[$j]->media_type=='2')<i class="fa fa-book"></i>@endif
																		@if($lectures[$j]->media_type=='5')<i class="fa fa-picture-o"></i>@endif
																		<span>{!! $durations['0']->duration !!}</span>
																	</span>
																</div>
																@endif
																@endif
																@if($lectures[$j]->media_type=='3')
																<div class="ci-details-container-dark clearfix">
																	<span class="ci-details-dark">
																		<i class="fa fa-file-text"></i>
																		<span>{!! Lang::get('core.text')!!}</span>
																	</span>
																</div>
																@endif
															</div>
														</a>
													
												</li>
												
									@php $lno++; @endphp
									@endfor
												
											</ul>
										</li>
									</ul>
									@php $no++; @endphp
									@endfor
									@endif
                                    </div>
							</div>
							<div id="description" class="tab-pane  description-b">
 <div class="cum_scrollbar default-skin">
                              <div class="use-padding">
								<?php if(count($getdata)>0){
									if(isset($getdata['0']->description)){ ?>
									<h2 class="txt-orange">{!! Lang::get('core.lecture_description') !!}</h2>
									<p><?php echo $getdata['0']->description; ?></p>
									<?php }
									else
									{?>
												<h2 class="txt-orange">{!! Lang::get('core.lecture_description') !!}</h2>
												{!! Lang::get('core.no_description') !!}												
											<?php  
									}

									 if(isset($getdata['0']->resources)){ 
										echo '<h4 class="txt-orange">'.Lang::get('core.download_resource').'</h4>
										<table class="table table-bordered">';
										$jsondecode = json_decode($getdata['0']->resources,true);
										foreach ($jsondecode as $resource) { 
											$getfiles 	= \SiteHelpers::getcoursefiles($resource);
											$resourceid = \SiteHelpers::encryptID($getfiles['0']->id);
											if($getfiles['0']->file_type=='link'){
												echo '<tr><td><a href="'.$getfiles['0']->file_name.'" target="_blank"><i class="fa fa-file-o"></i> '.$getfiles['0']->file_title.'</a></td><td width="30"><a href="'.$getfiles['0']->file_name.'" target="_blank"><i class="fa fa-download"></i></a></td></tr>';
											}else{
												echo '<tr><td><a href="'.URL::to('download-resource/'.$resourceid).'"><i class="fa fa-file-o"></i> '.$getfiles['0']->file_title.'</a></td><td width="30"><a href="'.URL::to('download-resource/'.$resourceid).'"><i class="fa fa-download"></i></a></td></tr>';
											}
										}
										echo '</table>';
									}
									else
								{?>
									<h2 class="txt-orange">{!! Lang::get('core.download_resource') !!}</h2>
									
									{!! Lang::get('core.no_downloadresources') !!}
									<?php
								}
								} ?>
								
								<!--<h4 class="txt-orange">Downloadable Resources</h4>
								<table class="table table-bordered">
								<tr><td><a href="#"><i class="fa fa-file-o"></i> asdf </a></td><td width="30"><a href="#"><i class="fa fa-download"></i></a></td></tr>
								</table>-->
								</div>
                                </div>
							</div>
							<div id="comments" class="tab-pane">
                             <div class="cum_scrollbar default-skin">
                              <div class="use-padding">
								<h2 class="txt-orange">{!! Lang::get('core.comments') !!}</h2>

								@if(Auth::check() == TRUE)
								
								<div class="commentbox" >
									<div class="form-group">
										<input type="hidden" name="courseid" value="">
										<input type="text" name="posttitle" class="form-control" id="posttitle" placeholder="{!! Lang::get('core.start_discussion')!!}">
									</div>
									<div class="form-group textareablock" style="<?php if(count($comments)=='0'){ echo "display:block"; }else{ echo "display:none"; }?>;">
										<textarea id="postcomments" class="form-control lectureeditor" rows="2" placeholder="Discuss the course and topics related to its field. No promotions please." name="comment" cols="50"></textarea> 
									</div> 
									<div class="form-group textareablock" <?php if(count($comments)=='0'){ ?> style="display:block;" <?php }else{ ?>style="display:none;"<?php } ?>>              
										<button class="btn btn-orange postbtn" type="button" >{!! Lang::get('core.Post')!!}</button>
									</div>
								</div>
								@endif
									
								
									<div class="ajax-comments">
										@include('course.ajax-comments-learn')
									</div>
								</div> 
                              </div>
                             </div>
                            <div id="notes" class="tab-pane">
 							<div class="cum_scrollbar default-skin">
                              <div class="use-padding">
									<h2 class="txt-orange">{!! Lang::get('core.Notes')!!}</h2>
									{!! Form::open(array('method' => 'post', 'class'=>'form-horizontal saveLabel','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
									<div class="col-sm-12">
										{!! Form::textarea('takenotes','',array('class'=>'form-control typenotes','placeholder'=>'Start typing to take your note','id' => 'takenotes','rows'=>'3')); !!}
									</div>

									{!! Form::close() !!}
									<div class="clearfix"></div>
									<div style="display:block; ">
										<table class="table" id="listnotes">
											<?php
											if(count($notes)>0){
												for ($k=0; $k < count($notes); $k++) { 
													echo '<tr><td><p class="list_note">'.$notes[$k]->notes.'</p> <a href="javascript:void(0);" class="pull-right removenotes" data-noteid="'.$notes[$k]->note_id.'"><i class="fa fa-times"></i></a></td></tr>';
												}
											}

											?>										
										</table>

									</div>
									<div id="downloadlink" class="use-padding">
										<?php
										if(count($notes)>0){ 
											echo '<a href="'.URL::to("download-notes/".$notes["0"]->lecture_id).'" class="btn btn-orange">'.Lang::get('core.download_notes').'</a>';
										}
										?></div>
                                        </div>
                                        </div>
									</div>      
						</div>
					</div>
				</div>

<script type="text/javascript">
				// ajaxtinymce();
				function initscrollbar(id) 
				{
					if(id == '#comments')
					{
						$(id+ ' .cum_scrollbar').customScrollbar();
					}	
					else
					{
						$(id+ ' .cum_scrollbar').customScrollbar();
					}

				}

				$('.lectureeditor').redactor({
				    buttons: ['format', 'bold', 'italic','image']
				});

				function ajaxtinymce(dynamic_class){
				if(dynamic_class == 'undefined' || dynamic_class == null)
				{
					var selector = 'lectureeditor';
				}
				else
				{
					var selector = dynamic_class;
				}
				
				if ($("#"+selector+'_parent').length > 0) 
				{
					    
				}
				else
				{
					//alert(selector);
					tinymce.init({	
						mode : "specific_textareas",
						editor_selector : selector,
						theme : "advanced",
						theme_advanced_buttons1 : "bold,italic,underline,bullist,numlist,link,image",
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "bottom",
						width : "100%",
						plugins : "paste",
						paste_text_sticky : true,
						setup : function(ed) {
							ed.onInit.add(function(ed) {
								ed.pasteAsPlainText = true;
							});
						}
					});
				}
				
			}

				$(function() {
					initscrollbar('#lectures');
					$('.nav-tabs li a').click(function(){
						var currentid=$(this).attr('href');
						setTimeout(function(){ initscrollbar(currentid);}, 500);
					});
				});

				$('body').on('click', '.ajax-comments-pagination a', function()
				{
					$.ajax({
						type: 'GET',
						url: $(this).attr('href'),
						success : function(data) 
						{
							$('.ajax-comments').html(data);
						}
					});	
					return false;						
				});
				$('body').on('click', '.show-reply', function()
				{
					$(this).parents('.discussion-activity-box').find('.discussion-comments-block').slideToggle();
					$("#comments .cum_scrollbar").customScrollbar();
				});

				$(function(){
				
					$('.righthide').click(function(){
						if($(this).hasClass('fullshow')) {
							$(this).find('i').addClass('fa-angle-right');
							$(this).find('i').removeClass('fa-angle-left');
							$(this).removeClass('fullshow');
							$('.contentView').removeClass('col-md-12');
							$('.contentView').addClass('col-md-8');
							$('.lsidebar').show();
						} else {
							$(this).find('i').removeClass('fa-angle-right');
							$(this).find('i').addClass('fa-angle-left');
							$(this).addClass('fullshow');
							$('.contentView').removeClass('col-md-8');
							$('.contentView').addClass('col-md-12');
							$('.lsidebar').hide();
						}
					});

					$('input[type="checkbox"],input[type="radio"]').iCheck({
						checkboxClass: 'icheckbox_square-green',
						radioClass: 'iradio_square-green',
					});	

					$('.updatelstatus').on('ifChecked', function(event){
						var lid 		= $(this).data('lid');
						var courseid 	= $(this).data('courseid');
						updatestatus(lid,courseid,1);
					});

					$('.updatelstatus').on('ifUnchecked', function(event){
						var lid 		= $(this).data('lid');
						var courseid 	= $(this).data('courseid');
						updatestatus(lid,courseid,0);
					});

				

					$('#takenotes').keypress(function(e) {
						if (e.keyCode == '13') {
							e.preventDefault();

							var gettype 	= $('[name="mediatype"]').val();
							var lectureid 	= $('[name="lectureid"]').val();
							var sectionid 	= $('[name="sectionid"]').val();
							var notes 		= $(this).val();
							$('#takenotes').val('');
						// alert(lectureid);
						// alert(notes);
						if(gettype=='0'){
							var vid = document.getElementById("lecture_video");
							var length  = parseInt(vid.currentTime);
						}else if(gettype=='1'){
							var vid = document.getElementById("lecture_audio");
							var length  = parseInt(vid.currentTime);
						}else{
							var length  = 0;
						}
						
						var converts   = secondsTimeSpanToHMS(length);
						// alert(converts);
						

						if($.trim(notes).length>0){

							var me = $(this);
							if ( me.data('requestRunning') ) {
								return;
							}
							me.data('requestRunning', true);

							$.ajaxSetup({
								headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
							});
							$.ajax({
								type: 'POST',
								url: '<?php echo e(\URL::to("course/takenotes")); ?>',
								data:'lid='+lectureid+'&sectionid='+sectionid+'&duration='+converts+'&notes='+notes,
								beforeSend: function() {
									$('#takenotes').prop('disabled',true);
								},
								success : function(data) {
									$('#takenotes').val('');
									$('#listnotes').append('<tr><td><p class="list_note">'+notes+'</p><a href="javascript:void(0);" class="pull-right removenotes" data-noteid="'+data+'"><i class="fa fa-times"></i></a></td></tr>');
									$('#downloadlink').html('<a href="{{ \URL::to("download-notes") }}/'+lectureid+'" class="btn btn-orange">Download Your Notes</a>');
									$("#notes .cum_scrollbar").customScrollbar();
								},complete: function() {
									$('#takenotes').prop('disabled',false);
									me.data('requestRunning', false);
								}
							});
						}

					}
				}); //end keypress

$(document).on('click','.removenotes',function(){
	var removeid = $(this).data('noteid');
	var removelength = $('.removenotes').length;
					// alert(removelenght);
					var r = confirm("{!! Lang::get('core.notes_delete') !!}");
					if (r == true) {
						$(this).closest('tr').remove();
						if(removelength=='1'){
							$('#downloadlink').html('');
						}
						var me = $(this);
						if ( me.data('requestRunning') ) {
							return;
						}
						me.data('requestRunning', true);

						$.ajaxSetup({
							headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
						});
						$.ajax({
							type: 'POST',
							url: '<?php echo e(\URL::to("course/removenotes")); ?>',
							data:'noteid='+removeid,
							success : function(data) {
								$("#notes .cum_scrollbar").customScrollbar();
							},complete: function() {
								me.data('requestRunning', false);
							}
						});

					}
				});


				//comments start here

				$(document).on('click','.replybutton',function () { 
					var cid = $(this).data('commentid');
					if ($('#openbox-'+cid).is(':visible')) { 
						$("#openbox-"+cid).slideUp(300); 
					} 
					if ($("#openbox-"+cid).is(':visible')) { 
						$("#openbox-"+cid).slideUp(300);
					} else {
						$("#openbox-"+cid).slideDown(300); 
						//ajaxtinymce('textboxcontent-'+cid);
					} 
					$("#comments .cum_scrollbar").customScrollbar();
				});

				$(document).on('click','.cancelbtn',function () { 
					var cid = $(this).data('commentid');
					$('.redactor_textboxcontent-'+cid+' p').html('<br/>');
					if ($('#openbox-'+cid).is(':visible')) { 
						$("#openbox-"+cid).slideUp(300); 
					} 
					if ($("#openbox-"+cid).is(':visible')) { 
						$("#openbox-"+cid).slideUp(300);
					} else {
						$("#openbox-"+cid).slideDown(300); 

					} 
					$("#comments .cum_scrollbar").customScrollbar();
				});

				$(document).on('click','.editcancelbtn',function () { 
					var cid = $(this).data('commentid');
					if ($('#replyopenbox-'+cid).is(':visible')) { 
						$("#replyopenbox-"+cid).slideUp(300); 
						$('#replytext-'+cid).show();
					} 
					if ($("#replyopenbox-"+cid).is(':visible')) { 
						$("#replyopenbox-"+cid).slideUp(300);
						$('#replytext-'+cid).show();
					} else {
						$("#replyopenbox-"+cid).slideDown(300); 
						$('#replytext-'+cid).show();
					} 
					$("#comments .cum_scrollbar").customScrollbar();
				});

				$(document).on('click','.editccancelbtn',function () { 
					var cid = $(this).data('commentid');
					if ($('#commentopenbox-'+cid).is(':visible')) { 
						$("#commentopenbox-"+cid).slideUp(300); 
						$('#wholewrapper-'+cid).show();
					} 
					if ($("#commentopenbox-"+cid).is(':visible')) { 
						$("#commentopenbox-"+cid).slideUp(300);
						$('#wholewrapper-'+cid).show();
					} else {
						$("#commentopenbox-"+cid).slideDown(300); 
						$('#wholewrapper-'+cid).show();
					} 
					$("#comments .cum_scrollbar").customScrollbar();
				});
				
				$(document).on('click','.replybtn',function(e){
					var cid = $(this).data('commentid');
					var textvalues = $('#textboxcontent-'+cid).val();//$('#textboxcontent-'+cid).val();
					var textvaluescount = $(textvalues).text();
					if($.trim(textvaluescount).length===0){
						toastr.error("Error", "{!! Lang::get('core.enter_valid_comments') !!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}
					}else if ($.trim(textvaluescount).length>0){
						var me = $(this);
						e.preventDefault();
						if ( me.data('requestRunning') ) {
							return;
						}
						me.data('requestRunning', true);
						var lectureid 	= $('[name="lectureid"]').val();
						$.ajaxSetup({
							headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
						});
						$.ajax({
							type: 'POST',
							url: '<?php echo e(\URL::to("course/insertlecturereply")); ?>',
							data:'commentid='+cid+'&comments='+textvalues+'&lid='+lectureid+'&render_file=true',
							success : function(data) {
									$('#openbox-'+cid).hide();
									$('#openbox-'+cid).find('.redactor_editor').html('<p><br/></p>');
									$('#textboxcontent-'+cid).val('');
								
									$('#repc-'+cid).show();
									$('#repcd-'+cid).append(data);
									
									//$('.discussion-comments-container-dark').append(data);

									// ajaxtinymce();

									toastr.success("Success", "{!! Lang::get('core.comment_thanks') !!}");
									toastr.options = {
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-bottom-right",
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"timeOut": "5000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut"
									}

								// var obj = jQuery.parseJSON(data);
								// if(obj.status=='1'){
								// 	$('#openbox-'+cid).hide();
								// 	// $('#textboxcontent-'+cid).val('');
								// 	tinyMCE.get('textboxcontent-'+cid).setContent('');

								// 	$('.discussion-comments-container-dark').append(data);

								// 	ajaxtinymce();

								// 	toastr.success("Success", "Thank you for reply this comment.");
								// 	toastr.options = {
								// 		"closeButton": true,
								// 		"debug": false,
								// 		"positionClass": "toast-bottom-right",
								// 		"onclick": null,
								// 		"showDuration": "300",
								// 		"hideDuration": "1000",
								// 		"timeOut": "5000",
								// 		"extendedTimeOut": "1000",
								// 		"showEasing": "swing",
								// 		"hideEasing": "linear",
								// 		"showMethod": "fadeIn",
								// 		"hideMethod": "fadeOut"
								// 	}
								// }else if(obj.status=='2'){
								// 	toastr.error("Error", "Sorry! Something went wrong please try again later.");
								// 	toastr.options = {
								// 		"closeButton": true,
								// 		"debug": false,
								// 		"positionClass": "toast-bottom-right",
								// 		"onclick": null,
								// 		"showDuration": "300",
								// 		"hideDuration": "1000",
								// 		"timeOut": "5000",
								// 		"extendedTimeOut": "1000",
								// 		"showEasing": "swing",
								// 		"hideEasing": "linear",
								// 		"showMethod": "fadeIn",
								// 		"hideMethod": "fadeOut"
								// 	}
								// }
								$("#comments .cum_scrollbar").customScrollbar();
								return false;
								
							},complete: function() {
								me.data('requestRunning', false);
							}
						});
}

});


$(document).on('click','.postbtn',function(e){

	var texttitles = $('#posttitle').val();
	var textvalues = $('#postcomments').val();
	var regX = /(<([^>]+)>)/ig;
	textvalues= textvalues.replace(regX, "");
	var textvaluescount = textvalues;
	//var textvaluescount = $(textvalues).text();
	// var textvalues = $('.lectureeditor').redactor('code.get');
	// console.log(textvalues);

	if($.trim(texttitles).length===0){
		toastr.error("Error", "{!! Lang::get('core.discussion_valid_title') !!}");
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut",
			"preventDuplicates": true
		}
	}else if($.trim(textvaluescount).length===0){

		toastr.error("Error", "{!! Lang::get('core.enter_valid_comments') !!}");
		toastr.clear();
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "2000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut",
			"preventDuplicates": true
		}

	}else if ($.trim(textvaluescount).length>0 && $.trim(texttitles).length>0){
		var me = $(this);
		var lectureid 	= $('[name="lectureid"]').val();
		var sectionid 	= $('[name="sectionid"]').val();
		var courseid    = $('[name="courseid"]').val();
		e.preventDefault();
		if ( me.data('requestRunning') ) {
			return;
		}

		me.data('requestRunning', true);

		$.ajaxSetup({
			headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
		});
		$.ajax({
			type: 'POST',
			url: '<?php echo e(\URL::to("course/insertlecturecomments")); ?>',
			data:'comments='+textvalues+'&title='+texttitles+'&lid='+lectureid+'&sid='+sectionid+'&cid='+courseid+'&render_file=true',
			success : function(data) {
					$('#posttitle').val('');
					// tinyMCE.get('postcomments').setContent('');
					$('.textareablock').find('.redactor_editor').html('<p><br/></p>');
					$('#postcomments').val('');	
					$('.textareablock').css('display','none');
					
					seclector_div = $('#allcomments');
					if(seclector_div.length < 1)
						$('.ajax-comments').prepend('<div id="allcomments">'+data+'</div>');	
					else
						seclector_div.prepend('<div id="allcomments">'+data+'</div>');	
					
					// ajaxtinymce();
					toastr.success("Success", "{!! Lang::get('core.disccusion_comment_success')!!}");
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}

				
                $("#comments .cum_scrollbar").customScrollbar();
                // alert('dd');
				return false;
			},complete: function() {
				me.data('requestRunning', false);
			}
		});
}

});

$('#posttitle').focus(function(){
	$('.textareablock').css('display','block');
});

$(document).on('click','.removecomments',function(){
	var removeid  = $(this).data('commentid');
				// alert(removeid);

				var r = confirm("{!! Lang::get('core.co_delete_confirm') !!}");
				if (r == true) {
					$('#comment-'+removeid).remove();
					
					var me = $(this);
					if ( me.data('requestRunning') ) {
						return;
					}
					me.data('requestRunning', true);

					$.ajaxSetup({
						headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
					});
					$.ajax({
						type: 'POST',
						url: '<?php echo e(\URL::to("course/removelecturecomments")); ?>',
						data:'cid='+removeid,
						success : function(data) {
							$("#comments .cum_scrollbar").customScrollbar();
						},complete: function() {
							me.data('requestRunning', false);
						}
					});

				}

			});

$(document).on('click','.removereply',function(){
	var removeid  = $(this).data('rcommentid');
				// alert(removeid);

				var r = confirm("{!! Lang::get('core.co_delete_confirm')!!}");
				if (r == true) {
					$('#reply-block'+removeid).remove();
					
					var me = $(this);
					if ( me.data('requestRunning') ) {
						return;
					}
					me.data('requestRunning', true);

					$.ajaxSetup({
						headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
					});
					$.ajax({
						type: 'POST',
						url: '<?php echo e(\URL::to("course/removelecturereply")); ?>',
						data:'rid='+removeid,
						success : function(data) {
							$("#comments .cum_scrollbar").customScrollbar();
						},complete: function() {
							me.data('requestRunning', false);
						}
					});

				}

			});


$(document).on('click','.editreply',function(){
	var replyid = $(this).data('rcommentid');
				// alert(replyid);
				$('#replytext-'+replyid).css('display','none');
				$('#replyopenbox-'+replyid).css('display','block');
				$('.lectureeditor').redactor();
				$("#comments .cum_scrollbar").customScrollbar();
			});


$(document).on('click','.editcomments',function(){
	var cid = $(this).data('commentid');
				
				$('#wholewrapper-'+cid).css('display','none');
				$('#commentopenbox-'+cid).css('display','block');
				$('.lectureeditor').redactor();
				//ajaxtinymce('editctextboxcontent-'+cid);
				$("#comments .cum_scrollbar").customScrollbar();
			});

$(document).on('click','.updatereplybtn',function(e){
	var rid = $(this).data('replyid');
	var cid = $(this).data('commentid');
					var textvalues = $('#edittextboxcontent-'+rid).val();

					if($.trim(textvalues).length===0){
						toastr.error("Error", "{!! Lang::get('core.enter_valid_comments')!!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut",
							"preventDuplicates": true
						}
					}else if ($.trim(textvalues).length>0){
						var me = $(this);
						e.preventDefault();
						if ( me.data('requestRunning') ) {
							return;
						}
						me.data('requestRunning', true);
						var lectureid 	= $('[name="lectureid"]').val();
						$.ajaxSetup({
							headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
						});
						$.ajax({
							type: 'POST',
							url: '<?php echo e(\URL::to("course/updatelecturereply")); ?>',
							data:'commentid='+cid+'&comments='+textvalues+'&lid='+lectureid+'&rid='+rid,
							success : function(data) {
								var obj = jQuery.parseJSON(data);
								if(obj.status=='1'){
									$('#replytext-'+rid).show();
									$('#replyopenbox-'+rid).hide();
									// tinyMCE.get('edittextboxcontent-'+rid).setContent('');
									$('#replytext-'+rid).html(obj.results.comments);
									// $('#replyies-'+obj.results.commentid).append('<div id="reply-block'+obj.results.replyid+'" class="media"><hr><a href="" class="pull-right bla">'+obj.results.imgpath+'</a><a href="javascript:void(0);" data-rcommentid="'+obj.results.replyid+'" class="pull-right removereply"><i class="fa fa-trash-o"></i></a><div class="media-body"><h4 class="media-heading comment"><a href="">'+obj.results.fname+'</a><span class="msg-time pull-right"><span><abbr class="timeago">Just Now</abbr>&nbsp;</span></span></h4>'+obj.results.comments+'</div></div>');
									toastr.success("Success", "{!! Lang::get('core.comment_thanks') !!}");
									toastr.options = {
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-bottom-right",
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"timeOut": "5000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut",
										"preventDuplicates": true
									}
								}else if(obj.status=='2'){
									toastr.error("Error", "{!! Lang::get('core.comments_error')!!}");
									toastr.options = {
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-bottom-right",
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"timeOut": "5000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut",
										"preventDuplicates": true
									}
								}
                                $("#comments .cum_scrollbar").customScrollbar();
								return false;
							},complete: function() {
								me.data('requestRunning', false);
							}
						});
}

});



$(document).on('click','.updatecommentbtn',function(e){
	var cid  = $(this).data('commentid');
	var texttitles = $('#edittitle-'+cid).val();
					var textvalues = $('#editctextboxcontent-'+cid).val();
					// var textvalues = tinyMCE.get('editctextboxcontent-'+cid).getContent();

					// alert(textvalues);

					if($.trim(texttitles).length===0){
						toastr.error("Error", "{!! Lang::get('core.discussion_valid_title') !!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut",
							"preventDuplicates": true
						}
					}else if($.trim(textvalues).length===0){

						toastr.error("Error", "{!! Lang::get('core.enter_valid_comments')!!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut",
							"preventDuplicates": true
						}

					}else if ($.trim(textvalues).length>0 && $.trim(texttitles).length>0){
						var me = $(this);
						var lectureid 	= $('[name="lectureid"]').val();
						var sectionid 	= $('[name="sectionid"]').val();
						e.preventDefault();
						if ( me.data('requestRunning') ) {
							return;
						}

						me.data('requestRunning', true);

						$.ajaxSetup({
							headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
						});
						$.ajax({
							type: 'POST',
							url: '<?php echo e(\URL::to("course/updatelecturecomments")); ?>',
							data:'comments='+textvalues+'&title='+texttitles+'&lid='+lectureid+'&cid='+cid,
							success : function(data) {
								var obj = jQuery.parseJSON(data);
								if(obj.status=='1'){
									// $('#posttitle').val('');
									// tinyMCE.get('postcomments').setContent('');
									// $('.textareablock').css('display','none');
									$('#commentopenbox-'+cid).css('display','none');
									$('#wholewrapper-'+cid).css('display','block');
									$('#titletext-'+cid).html(texttitles);
									$('#bodytext-'+cid).html(textvalues);
									toastr.success("Success", "{!! Lang::get('core.disccusion_comment_success') !!}");
									toastr.options = {
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-bottom-right",
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"timeOut": "5000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut",
										"preventDuplicates": true
									}
								}else if(obj.status=='2'){
									toastr.error("Error", "{!! Lang::get('core.comments_error') !!}");
									toastr.options = {
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-bottom-right",
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"timeOut": "5000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut",
										"preventDuplicates": true
									}
								}
                                $("#comments .cum_scrollbar").customScrollbar();
								return false;
							},complete: function() {
								me.data('requestRunning', false);
							}
						});
}

});



			}); //end functions
function secondsTimeSpanToHMS(s) {
			    var h = Math.floor(s/3600); //Get whole hours
			    s -= h*3600;
			    var m = Math.floor(s/60); //Get remaining minutes
			    s -= m*60;
			    if(h>0){
			    	return h+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
			    }else{
			    	return (m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
			    }
			}

			

			function updatestatus(lid,cid,sid){

				$.ajaxSetup({
					headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
				});
				$.ajax({
					type: 'POST',
					url: '<?php echo e(\URL::to("course/updatecoursestatus")); ?>',
					data:'lid='+lid+'&cid='+cid+'&sid='+sid,
					success : function(data) {

					}
				});
			}

			
			</script>