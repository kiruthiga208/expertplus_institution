@extends('layouts.frontend')
@section('content')
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ URL::to('assets/bsetec/static/js/croppic.min.js') }}"></script>
<style type="text/css">
.form-group{ margin:25px 0px; }
.leftnopad{padding-left:0px !important;}
</style>
<div class="create-course">
<div class="container">
	<div class="row">
         <div class="col-lg-12">
            @include('course.courseheader')
        </div>
		<div class="col col-sm-3 multi_development">
			@include('course.createsidebar')
		</div>
		
<div class="col col-sm-9">
<div class="block2 clearfix">
<div class="promo_block">
<h2>{!! Lang::get('core.Image')!!}</h2>
</div>
<div class="image_sec subblock">
<div class="video_block">
<label>{!! Lang::get('core.cover_preview') !!}: <span class="asterisk">*</span></label>
<div class="block3">
<?php
    	$filename = '';
    	$filetype = '';
    	if(!empty($courseImage['0']->image_title)){
    		$image_name = $courseImage['0']->id.".".$courseImage['0']->image_type;
			$image_title = $courseImage['0']->image_title.".".$courseImage['0']->image_type;
    	}else{
            $image_name = "";
            $image_title = "";
        }
    ?>
<div id="image_up">
  <div id="image_prograss"></div>
</div>
<div id="cropContainerPreloadCourse" class="image_container">
 @if($course->image != NULL && $course->image != 0 && is_numeric($course->image))
    <img class="croppedImg" src="{!! \bsetecHelpers::getImage($course->image)!!}" width="100%" height="250px"/>
 @endif 
</div>
<div class="tip"><p>{!! Lang::get('core.image_tip')!!}</p></div>
</div>
<div class="upload_type clearfix">
<label>{!! Lang::get('core.ad_image') !!} ( {!! Lang::get('core.course_image')!!} ):</label>
<div class="format_type clearfix">
<div class="format">
  <span id="test_error">{!! Lang::get('core.use')!!}</span>
  <div class="progress progress-striped progress_upload" id="progress_bar" style="display:none;">
  <div class="progress-bar progress-bar-warning active" id="progress_bar_p"> </div>
  <p id="file_name"></p>
</div>
</div> 
</div>
@if($course->image != NULL && $course->image != 0 && is_numeric($course->image))
 <input type='hidden' id='image_title' value='{!! $image_title !!}' />
 <input type='hidden' id='image_name' value='{!! $image_name !!}' />
   <input type='hidden' id='image_path' />
 @else
  <input type='hidden' id='image_title' />
   <input type='hidden' id='image_name' />
   <input type='hidden' id='image_path' />
 @endif
{!! Form::hidden('course_id', $course->course_id,array('id'=>'course_id') )  !!}
{!! Form::hidden('step', $step,array('id'=>'step') )  !!}
<div class="button-block clearfix">
<div id="image_div">
  <div class="fileUpload btn btn-primary"> <span>{!! Lang::get('core.choose')!!}</span>  
    <input id="cImage" type="file" name="cImage" data-url="{!! url('').'/course/image' !!}" class="upload"/>
  </div>
</div>

<div class="change_btn" id="profile_delete_btn" style="display:none;">
    <button id='cancel_btn' style="display:none;">{!! Lang::get('core.cancel')!!}</button>
    <input type="button" onClick='course_picture_delete();' value="{!! Lang::get('core.Change')!!}" /> 
</div>

</div>

</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div></div>


<!-- Cropic plugin files -->
<link href="{{ asset('assets/bsetec/js/plugins/croppic/css/croppic.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/croppic/croppic.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/croppic/jquery.mousewheel.min.js') }}"></script>
<!-- image load plugin -->
        <script type="text/javascript" src="{{ asset('assets/bsetec/static/js/imagesloaded.pkgd.min.js') }}"></script>
<!-- file upload plugin files -->
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.ui.widget.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-ui.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-process.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-validate.js') }}"></script>
<script type="text/javascript">
var error = $("#test_error");
var progress_bar = $("#progress_bar");
var cancel_btn = $("#cancel_btn");
var progress_bar_p = $('#progress_bar_p');
var delete_btn = $("#profile_delete_btn");
var image_div = $("#image_div");
var image_prograss = $('#image_prograss');

$(document).ready(function(){
		@if($course->image != NULL && $course->image != 0 && is_numeric($course->image))
    delete_btn.show();
    $("#image_up,#image_div").hide();
    @else
    delete_btn.hide();
    $("#image_up,#image_div").show();
    @endif
});
$(function () {
   $('#cImage').fileupload({
        dataType:'json',
        add:function(e,data){
            var file=data.files[0];
            var uploadErrors = false;
            var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
            if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                error.text('{!! Lang::get("core.image_file_type_error") !!}').addClass('error_msg');
                return false;
            }

            if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000) {
                error.text('{!! Lang::get("core.image_big_size") !!}').addClass('error_msg');
                return false;
            }
            data.submit();

            image_div.hide();
            delete_btn.toggle();
            progress_bar.show();
            error.removeClass('error_msg').hide();
            progress_bar_p.css('width','0%');
            $("#file_name").text(file.name); 
        },
        progress: function (e, data) {
            error.removeClass('error_msg').hide();
            var percentage = parseInt((data.loaded / data.total) * 100,10);
            progress_bar_p.css('width',percentage+'%');
            image_prograss.html("{!! Lang::get('core.img_uploading')!!} "+ percentage + '%');
        },
        done: function(e, data){
            return_data = data.result;
            v_html = "";
            if(return_data.status){
                image_div.hide();
                delete_btn.show();
                progress_bar.show();
                progress_bar_p.show();
                     $("#image_name,#image_title").val(return_data.image_name); 
               $("#image_path").val(return_data.image_path);
            }else{
              image_prograss.html("");
              $("#file_name").text("");  
              delete_btn.hide();
              image_div.show();
              progress_bar_p.hide();
              error.addClass('error_msg').text(return_data.message).css('font-size','10px').show(); 
              return false;  
            }
            error.removeClass('error_msg');
            image_prograss.html("{!! Lang::get('core.uploading_full')!!}");
            progress_bar_p.removeClass('active');
            $("#file_name").text("{!! Lang::get('core.img_crop_process')!!}"); 
            imageCrop();
        },
    });
});

function course_picture_delete(){
    image_name = $('#image_title').val();
    courseID = $('#course_id').val();
    $.ajax({
        url:'{!! url('').'/course/image'!!}',
        data:{image_file:image_name,course_id:courseID},
        type:"DELETE",
        success:function(res){
            if(res == "success"){
              $("#imageCrop").html("");
              $("#image_up,#image_div").toggle();
              $("#cropContainerPreloadCourse").html("");
              error.text("{!! Lang::get('core.image_use') !!}").css('font-size','').show();
              progress_bar.hide();
              image_prograss.html("");
              progress_bar_p.css('width','0%');
              delete_btn.toggle();

              var image_url = '{!! url("course/image/default") !!}';
              $('.img-thumbnail').css('background-image','url('+image_url+')');
            }
        },
        fail:function(){
            alert("error");
        }
    });    
}

function imageCrop(){
    image_prograss.html("{!! Lang::get('core.wait') !!}");
    image_path = $("#image_path").val();
    image_name = $("#image_name").val();
      courseID = $('#course_id').val();

    var croppicContainerPreloadOptions = {
        onReset:function(){ 
         $("#cropContainerPreloadCourse").html("");
          course_picture_delete();
        },
        cropUrl:'{{url('')}}/course/image',
        loadPicture: image_path,
        doubleZoomControls:false,
        rotateControls:false,
        enableMousescroll:true,
        cropData:{
            "image_name":image_name,
            "course_id":courseID
        },
        modal:false,
        onAfterImgCrop:function(res){ 
          $('.img-thumbnail').css('background-image','url('+res.url+'/_small)');
        $('#cropContainerPreloadCourse img').attr({
            width:'100%',
            height:'250px'
        });
        $("#image_id").val(res.image_id);
        $("#image_path").val(" ");
          $("#file_name").text("{!! Lang::get('core.image_save') !!}");
        },
    }

    var cropContainerPreloadCourse = new Croppic('cropContainerPreloadCourse', croppicContainerPreloadOptions);
    $('#cropContainerPreloadCourse img').hide();
    imageLoading();
}
function imageLoading(){
    var imgLoad = new imagesLoaded('#cropContainerPreloadCourse');
    imgLoad.on( 'done', function( instance ) {
          $("#image_up").hide();
          image_prograss.html(" ");
          $('#cropContainerPreloadCourse img').show();
    });
}
</script>
@stop