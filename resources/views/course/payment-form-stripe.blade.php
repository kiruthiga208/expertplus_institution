<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>

<div class="container">
			<div class="row text-center">
				<h3 class="text-center">{!! Lang::get('core.stripe_payment_form') !!}</h3>
				<br>
				<br>
				<div class="col-lg-12">
					{!! Form::open(array('url'=>'payment/stripe', 'class'=>'form-horizontal','id'=>'payment-form', 'parsley-validate'=>'','novalidate'=>' ' )) !!}
					{!! Form::hidden('course_id', $course->course_id) !!}
					{!! Form::hidden('course_title', $course->course_title) !!}
					
					<div class="form-group" >
						<label for="First Name" class=" control-label col-md-4 text-left"> {!! Lang::get('core.course_title') !!}: </label>
						<div class="col-md-6">
						  	{!! $course->course_title !!}
						 </div> 
					</div>
					<input type="hidden" name="payment_method" value="stripe" />
					<div class="form-group" >
						<label for="First Name" class="control-label col-md-4 text-left"> {!! Lang::get('core.amount') !!} <span class="asterix"> * </span></label>
						<div class="col-md-6">
						  {!! Form::text('amount', '', array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
						 </div> 
					</div>
		   <div class="form-row">
               <label for="Email" class="control-label col-md-4 text-left"> {!! Lang::get('core.Email') !!} <span class="asterix"> * </span></label>
                {!! Form::email('email', '' , array('class'=>'form-control','id'=>'email','required'=>true, 'placeholder'=>'')) !!} 
            </div>            
    
   			<div class="form-row">
               <label for="Card Number" class="control-label col-md-4 text-left"> {!! Lang::get('core.card_number') !!} <span class="asterix"> * </span></label>
                {!! Form::text('card-number', '' , array('class'=>'form-control','id'=>'card-number','placeholder'=>'')) !!} 
            </div>

            <div class="form-row">
               <label for="card-cvc" class="control-label col-md-4 text-left"> {!! Lang::get('core.card_cvc') !!} <span class="asterix"> * </span></label>
                {!! Form::text('card-cvc', '' , array('class'=>'form-control','id'=>'card-cvc', 'placeholder'=>'')) !!} 
            </div>
            <div class="form-row">
                <label>{!! Lang::get('core.Expiration') !!}</label>
                <div class="expiry-wrapper">
                    <select class="stripe-sensitive required" id="card-expiry-month">
                    </select>
                    <script type="text/javascript">
                        var select = $("#card-expiry-month"),
                            month = new Date().getMonth() + 1;
                        for (var i = 1; i <= 12; i++) {
                            select.append($("<option value='"+i+"' "+(month === i ? "selected" : "")+">"+i+"</option>"))
                        }
                    </script>
                    <span> / </span>
                    <select class="stripe-sensitive required" id="card-expiry-year"></select>
                    <script type="text/javascript">
                        var select = $("#card-expiry-year"),
                            year = new Date().getFullYear();
                        for (var i = 0; i < 12; i++) {
                            select.append($("<option value='"+(i + year)+"' "+(i === 0 ? "selected" : "")+">"+(i + year)+"</option>"))
                        }
                    </script>
                </div>
            </div>
 <span class="payment-errors"></span>
 <input name='stripeToken' id="stripeToken" type="hidden" />
 </div>
				<div class="form-group">
					<div class="col-sm-6 pull-right">				
						<button type="submit" name="submit-button">{!! Lang::get('core.sb_submit')!!}</button>
					</div>
				</div>
		 {!! Form::close() !!}
				</div>	
			</div>
		</div>

<script type="text/javascript">
var stripe_div = $("#stripe");
var form_tag = $('#payment-form');
Stripe.setPublishableKey( "<?php echo $publishable_key; ?>" );
$(document).ready(function() {
	function submit(form) {
        // given a valid form, submit the payment details to stripe
        $(form['submit-button']).attr("disabled", "disabled")
        Stripe.createToken({
            number: $('#card-number').val(),
            cvc: $('#card-cvc').val(),
            exp_month: $('#card-expiry-month').val(), 
            exp_year: $('#card-expiry-year').val()
        },function(status, response) {
            if (response.error) {
                // re-enable the submit button
                $(form['submit-button']).removeAttr("disabled")

                // show the error
                $(".payment-errors").html(response.error.message);
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the stripe token
                $('#stripeToken').val(token);
                //form.appendChild(input[0])
                // and submit
                form.submit();
            }
        });
        
        return false;
    }
            
    // add custom rules for credit card validating
    jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber, "Please enter a valid card number");
    jQuery.validator.addMethod("cardCVC", Stripe.validateCVC, "Please enter a valid security code");
    jQuery.validator.addMethod("cardExpiry", function() {
        return Stripe.validateExpiry($("#card-expiry-month").val(), 
                                     $("#card-expiry-year").val())
    }, "Please enter a valid expiration");
    // We use the jQuery validate plugin to validate required params on submit
    $("#payment-form").validate({
        submitHandler: submit,
        rules: {
            "card-cvc" : {
                cardCVC: true,
                required: true
            },
            "card-number" : {
                cardNumber: true,
                required: true
            },
            "card-expiry-year" : "cardExpiry" // we don't validate month separately
        }
    });
});
// }
</script>