@extends('layouts.frontendfullwidth')
@section('content')
<!-- course privacy start-->
<div class="modal fade" id="passwordmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title ellipsis" id="infoModalLabel">{!! $courseinfo->course_title !!}</h4>
			</div>

			<div class="modal-body" id="passwordbody">

				<div class="form-group">
					<label for="recipient-name" class="control-label">{!! Lang::get('core.Password') !!}: {!! \Session::get('cplogin') !!}</label>
					<input type="password" name="cpassword" class="form-control" placeholder="{!! Lang::get('core.enter_pwd') !!}" id="cpassword">
				</div>
				<div class="form-group">
					<input type="button" name="submits" data-cid="{!! $courseinfo->course_id or '' !!}" id="submitpassword" class="btn btn-orange btn-bg" value="{!! Lang::get('core.sb_submit') !!}"><div class="passwordloader"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- course privacy end -->

<script type="text/javascript">
$(function(){

	$(document).on('click','#submitpassword',function(e){
		
		var courseid = $(this).data('cid');
		var password = $('#cpassword').val();
		if($.trim(password).length===0){
			toastr.error("Error", "{!! Lang::get('core.valid_pwd')!!}");
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
		}else if ($.trim(password).length>0){
			var me = $(this);
			e.preventDefault();
			if ( me.data('requestRunning') ) {
				return;
			}
			me.data('requestRunning', true);

			$.ajaxSetup({
				headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
			});
			$.ajax({
				type: 'POST',
				url: '<?php echo e(\URL::to("course/checkcoursepassword")); ?>',
				data:'password='+password+'&courseid='+courseid,
				beforeSend: function() {
					$('.passwordloader').html('<h4><i class="fa fa-refresh fa-spin"></i> {!! Lang::get("core.loading") !!}</h4>');
				},
				success : function(data) {
					var obj = jQuery.parseJSON(data);
					if(obj.status=='0'){
						$('#cpassword').val('');
						toastr.success("Success", "{!! Lang::get('core.pwd_success')!!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}
						window.location.reload();
					}else if(obj.status=='1'){

						toastr.error("Error", "{!! Lang::get('core.pwd_invalid') !!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}
					}else if(obj.status=='2'){
						toastr.error("Error", "{!! Lang::get('core.discussion_wrong') !!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}
					}

					return false;
				},complete: function() {
					$('.passwordloader').html("");
					me.data('requestRunning', false);
				}
			});
}

});

$('#passwordmodel').modal({
	keyboard: false
});

});
</script>
@stop