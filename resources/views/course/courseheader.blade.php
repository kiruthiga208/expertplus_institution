{!! Form::open(array('method' => 'post', 'class'=>'form-horizontal saveLabel','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
{!! Form::hidden('checkcoursereview', URL::to('course/submitreview') )  !!}

@if(count($course)>0)
@php $img = \bsetecHelpers::getImage($course->image, 'small'); $coursename = $course->course_title;  @endphp
@if($course->approved=='1')
@php $text = Lang::get('core.Approved'); $ids = ''; $infotext = 'Your course is <span class="publish_txt ctext">published.</span> '; @endphp
@elseif($course->approved=='2')
@php $text = Lang::get('core.Approval'); $ids = 'submitcourse'; $infotext = 'Your course is <span class="not_publish ctext">not published.</span> ';  @endphp
@elseif($course->approved=='0' || !$course->approved)
@php $text = Lang::get('core.sb_review'); $ids = 'submitcourse';  $infotext = 'Your course is <span class="not_publish ctext">not published.</span> ';  @endphp
@endif
@endif


<div class="well course-header">

	<div class="row">
		<div class="col-sm-3"><div class="image"><img src="{{ asset('assets/bsetec/images/spacer.gif') }}" alt="{!! $coursename or '' !!}" class="img-thumbnail" style="background-image:url('{!! $img !!}');"></div></div>
		<div class="col-md-9 clearfix"><div class="detail"><h4>{!! $coursename or '' !!}</h4>
        	<p >{!! $infotext or '' !!}</p>
            </div>
            <div class="btn-block">
            <ul>
            <li>
			    <div class="dropdown prev-btn">
		            <a href="javascript::" data-toggle="dropdown" class="dropdown-toggle"> {!! Lang::get('core.Preview') !!}<b class="caret"></b></a>
		    		<ul class="dropdown-menu" style="display: none;">
			        	<li><a target="_blank" href="{!! url('course-preview/'.$course->course_id.'/'.$course->slug.'?PreviewMode=instructor') !!}"> {!! Lang::get('core.As_Instructor') !!} </a></li>
			            <li><a target="_blank" href="{!! url('course-preview/'.$course->course_id.'/'.$course->slug.'?PreviewMode=guest') !!}"> {!! Lang::get('core.As_Guest') !!}</a></li>        
			        </ul>
				</div>
    		</li>
    <li>
            <p class="lead"><a href="javascript:void(0);" class="btn btn-primary" data-courseid="{!! $courseid !!}" id="{!! $ids !!}">{!! $text !!}</a></p></li></ul></div>
        </div>
		<!-- <a target="_blank" class="pull-right btn btn-success" href="{!! url('course-preview/'.$course->course_id.'/'.$course->slug.'?PreviewMode=instructor') !!}">Instructor</a>
		<a target="_blank" class="pull-right btn btn-success" href="{!! url('course-preview/'.$course->course_id.'/'.$course->slug.'?PreviewMode=guest') !!}">Guest</a> -->
	
		
            
       
	</div>
	
</div>


{!! Form::close() !!}

<div class="modal fade" id="submitmodal" tabindex="-1" role="dialog" aria-labelledby="submitmodalLabel">
	<div class="modal-dialog course_popup" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">{!! Lang::get('core.submit_course_review')!!}</h4>
			</div>
			<div class="modal-body" id="bodycontents">

			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
$(function(){
	$(document).on('click','#submitcourse',function(){
		$('#submitmodal').modal({
			keyboard: false
		});
		$('#bodycontents').html('');
		$('#bodycontents').html('<div class="sending_loadergs"></div>');
		var courseid = $(this).data('courseid');
		var checkcoursereview =$('[name="checkcoursereview"]').val();
		$.ajaxSetup({
			headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
		});
		$.ajax ({
			type: "POST",
			url: checkcoursereview,
			data: "courseid="+courseid,
			beforeSend: function() {
				$('.sending_loadergs').html('<h2><i class="fa fa-refresh fa-spin"></i> {!! Lang::get("core.loading")!!}</h2>');
			},
			success: function (msg){
				var return_data = $.parseJSON(msg);
				if(return_data.status=='1'){
					$('#bodycontents').append('<ul>'+return_data.message+'</ul>');
				}else if(return_data.status=='0'){
					$('#submitcourse').text('{!! Lang::get("core.Approved")!!}');
					$('#submitcourse').removeAttr('id');
					$('.ctext').removeClass('not_publish');
					$('.ctext').addClass('publish_txt');
					$('.ctext').text('{!! Lang::get("core.published")!!}');
					$('#bodycontents').append('<ul>'+return_data.message+'</ul>');
				}else if(return_data.status=='2'){
					$('#submitcourse').text('{!! Lang::get("core.Approval")!!}');
					$('#submitcourse').removeAttr('id');
					$('#bodycontents').append('<ul>'+return_data.message+'</ul>');
				}
			},
			complete: function() {
				$('.sending_loadergs').html("");
			}
		});
	});
});
</script>