@extends('layouts.frontend')
@section('content')
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
         <style>
.nav-bar.learn-course-header li {
  float: left;
  font-size: 14px;
}
.nav-bar.learn-course-header li a{
  color: white;
}
</style>

<div class="description des_marg">

<div class="container">
<div class="row-fluid">
<div class="des_border span12">

<div class="des_header">
@if($status == "success")
<span class="des_bold des_color">{!! Lang::get('core.payment_success')!!}</span>
 @else
 <span class="des_bold des_color">{!! Lang::get('core.payment_fail')!!}</span>
 @endif

</div>

 <div class="des_con">
<div class="success_img"></div>

 @if($transId !=0)
 <span class="readytosuccess id">{!! Lang::get('core.t_id')!!} : {{ $transId }}</span>
 @endif


 <!--<span class="readytosuccess  social_all"><span class="like_color">Like us</span> on Facebook and get Exculsive, Fan-only Offers.</span>-->
 <div class="des_ready">
  <span class="desready_p readytosuccess">{!! Lang::get('core.pay_ready')!!}</span>
 <div>
 @if($status == "success")
 <div class="access access_mrg">
<a href="{{url('courseview/'.$course->course_id.'/'.$course->slug.'?return=success')}}">{!! Lang::get('core.go_course')!!}</a></div></div>
</div>
 @else
  <div class="access access_mrg">
<a href="{{url('courseview/'.$course->course_id.'/'.$course->slug)}}">{!! Lang::get('core.back_course')!!}</a></div></div>
</div>
 @endif
</div>
</div>
</div>
</div>
</div>

<!--End Description-->
@stop
