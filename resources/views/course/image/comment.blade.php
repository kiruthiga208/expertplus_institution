@if(Auth::check() == true)
<div class="commentBlock">
    {{ Form::open(array('files'=> TRUE,'role'=>'form')) }}
    <div class="form-group">
        {{ Form::textarea('comment','',array('class'=>"form-control",'rows'=>2,'placeholder'=>t('Comment'))) }}
    </div>
    {{ Form::submit(t('Comment'),array('class'=>'btn btn-info')) }}
    {{ Form::close() }}
</div>
@endif
@foreach($comments as $comment)

<div class="media" id="comment-{{ $comment->id }}">
    <span class="pull-left">
    <a href="{{ url('user/'.$comment->user->username) }}">
        <img class="media-object" alt="{{ $comment->user->fullname }}" src="{{ avatar($comment->user->avatar,75,75) }}">
    </a>
        @if(Auth::check())
        @if(checkFollow($comment->user->id))
     <button class="btn btn-default btn-xs replyfollow follow" id="{{ $comment->user->id }}">{{ t('Un-Follow') }}</button>
        @else
    <button class="btn btn-default btn-xs replyfollow follow" id="{{ $comment->user->id }}">{{ t('Follow Me') }}</button>
        @endif
        @endif
        </span>

    <div class="media-body">
        <h4 class="media-heading comment"><a href="{{ url('user/'.$comment->user->username) }}">{{ $comment->user->fullname }}</a>
             <span class="msg-time pull-right">
                  @if(Auth::check() == TRUE)
            @if($comment->user_id == Auth::user()->id || $image->id == Auth::user()->id)
            <span class="pull-right"><button data-content="{{ $comment->id }}" type="button" class="close delete-comment"
                                             aria-hidden="true">&times;</button></span>
            @endif
            @endif
				<i class="glyphicon glyphicon-time"></i>
				<span><abbr class="timeago" title="{{ date(DATE_ISO8601,strtotime($comment->created_at)) }}">{{ date(DATE_ISO8601,strtotime($comment->created_at)) }}</abbr>&nbsp;</span>
				</span>
        </h4>
        <p>{{ Smilies::parse(e($comment->comment)) }}</p>

        @if(Auth::check() == TRUE)
        <a class="replybutton" id="box-{{ $comment->id }}">{{ t('Reply') }}</a>

        <div class="commentReplyBox" id="openbox-{{ $comment->id }}">
            <input type="hidden" name="pid" value="19">
            {{ Form::textarea('comment','',array('id'=>'textboxcontent'.$comment->id,'class'=>"form-control",'rows'=>2,'placeholder'=>t('Comment'))) }}
            </br>
            <button class="btn btn-info replyMainButton" id="{{ $comment->id }}">{{ t('Reply') }}</button>
            <a class="closebutton" id="box-{{ $comment->id }}">{{ t('Cancel') }}</a>
        </div>
        @endif

        @foreach($comment->reply as $reply)
        <div class="media" id="reply-{{ $reply->id }}">
            <hr>
            <a class="pull-left bla" href="{{ url('user/'.$reply->user->username) }}">
                <img class="media-object" alt="{{ $reply->user->fullname }}"
                     src="{{ avatar($reply->user->avatar,64,64) }}">
            </a>

            <div class="media-body">
                <h4 class="media-heading comment"><a href="{{ url('user/'.$reply->user->username) }}">{{ $reply->user->fullname }}</a>

                    @if(Auth::check() == TRUE)
                    @if($reply->user_id == Auth::user()->id || $image->id == Auth::user()->id || $reply->comment->user->id == Auth::user()->id)
            <span class="right"><button data-content="{{ $reply->id }}" type="button" class="close delete-reply"
                                        aria-hidden="true">&times;</button></span>
                    @endif
                    @endif

                    <span class="msg-time pull-right">
				<i class="glyphicon glyphicon-time"></i>
				<span><abbr class="timeago" title="{{ date(DATE_ISO8601,strtotime($reply->created_at)) }}">{{ date(DATE_ISO8601,strtotime($reply->created_at)) }}</abbr>&nbsp;</span>
				</span>

                </h4>
                {{ Smilies::parse(e($reply->reply)) }}
            </div>
        </div>
        @endforeach

    </div>
</div>
<hr>
@endforeach


{{ $comments->links(array('class'=>'pagination')) }}