@extends('layouts.frontendfullwidth')
@section('content')
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/coursepreview.css') }}" rel="stylesheet">
<script src="{{ asset('assets/bsetec/static/js/jquery.raty.min.js') }}"></script>
<style>
.back-to-course-btn {
    position: absolute;
    top: 0px;
    display: block;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    max-width: 215px;
}
.static-content-wrapper {
    overflow: visible!important;
    margin: 20px auto;
    border: 1px solid #ccc;
}
.users-popup-list {
    width: 600px;
}
.static-content-wrapper h2 {
    background: #EDEDED;
    font-size: 16px;
    padding: 2px 15px;
    text-transform: uppercase;
    border-bottom: 1px solid #bebebe;
    box-shadow: 0 1px #edede7;
    line-height: 2.6;
    margin: auto;
}
.static-content-wrapper .mask {
    overflow: auto!important;
    width: auto!important;
    background: #FFFFFF;
    max-width: 600px;
    margin: auto;
}
.search-input{
	position: absolute; 
	right: 5px; 
	top: 5px;
	height:35px;
	padding:0px 5px;
}
.table-striped>tbody>tr:nth-child(odd) {
    background-color: #f9f9f9;
}
.table-striped>tbody>tr{
	height:40px;
}
.ud-userlist tr td {
    overflow: hidden;
    /*position: relative;*/
    border-bottom: 1px solid #dbdbdb;
    padding: 1px;
}
.ud-userlist tr td a img.img-circle {
    display: block;
    overflow: hidden;
    width: 45px;
    height: 45px;
    background-position: center;
    background-size: cover;
	padding:5px;
}
.ud-userlist tr.item-empty td {
	padding:10px;
}
.ud-userlist tr td .bordered-thumb {
    float: left;
    margin-right: 1px;
}
</style>
<!--Description-->
<div id="wrapper">
	<div class="course-header clearfix">
		<div class="course-header-content">
			<div class="container">
				<div class="col col-lg-12 fxacw course-header-wrapper">
					<div class="flex">
						@php $page_url = (Request::get('PreviewMode') == 'instructor') ? URL::to('course-preview/'.$courseinfo->course_id.'/'.$courseinfo->slug.'?PreviewMode='.Request::get('PreviewMode')) : URL::to('courseview/'.$courseinfo->course_id.'/'.$courseinfo->slug); @endphp
						<a href="{{ $page_url }}" class="back-to-course-btn btn btn-orange btn-sm"><i class="icon-chevron-left"></i>{!! Lang::get('core.back_course')!!}</a>
						<center><h1 class="course-title ellipsis">{!! $courseinfo->course_title !!}</h1></center>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="gray-bg" id="page-wrapper-full">
		<div class="container">
			<div class="row">
				<div class="main_second_section">
					<div class="container">
						<div class="row">
								<div id="students" class="users-popup-list static-content-wrapper" style="position: relative;">
								  <h2>{!! Lang::get('core.course_students_subscriber')!!}</h2><input type="text" placeholder="{!! Lang::get('core.search_by_name')!!}" autocomplete="off" name="search" class="search-input" />
								  <div class="ud-userlist mask">
									<table class="users-container table-striped table-hover" style="width: 100%;">
										<tbody>
											@if(!empty($subscribers))
											@foreach($subscribers as $subscriber)
											<tr class="student-list-item" data-userid="{!! $subscriber->id !!}">
												<td style="width: 50px;">
													<a href="{{ URL::to('profile/'.$subscriber->username) }}" target="_blank" style="display: inline-block;">
														{!! SiteHelpers::customavatar($subscriber->email,$subscriber->id) !!}</a>
													</a>
												</td>
												<td style="width: 150px;">
													<div class="ellipsis w150" style="text-align: left; display: inline-block;">
														<a href="{{ URL::to('profile/'.$subscriber->username) }}" target="_blank">{!! $subscriber->first_name.' '.$subscriber->last_name !!}</a>
													</div>
												</td><td></td><td></td>
											</tr>
											@endforeach
											@else
											<tr class="student-list-item item-empty">
												<td colspan="4"><center>{!! Lang::get('core.no_subscriber') !!}<center></td>
											</tr>
											@endif
										</tbody>
									</table>
									@if(count($subscribers) > 11)
									<div class="more">
										<center><a class="btn none load_more" href="javascript:void(0)" data-skip="{!! $skip !!}">{!! Lang::get('core.load_more')!!}</a></center>
										<input type="hidden" name="_token" value="{{ csrf_token() }}">
									</div>
									@endif
									
								  </div>
								</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(function(){
		$(".search-input").keyup(function(){
			var _token = $('[name="_token"]').val();
			var search = $(this).val();
			
			$.ajax({
				type: 'POST',
				url: '<?php echo e(\URL::to("courseusers/".$courseinfo->course_id)); ?>',
				data:'skip=0&search='+search+'&_token='+_token,
				beforeSend: function() {
					$('.item-empty').remove();
					$('.load_more').show();
					$('.load_more').html('<i class="fa fa-refresh fa-spin"></i> Loading...');
				},
				success : function(result) {
					$('.load_more').html('{!! Lang::get("core.load_more")!!}');
					var data = jQuery.parseJSON(result);
					if(data.status == '1'){
						$('.load_more').attr('data-skip',data.skip);
						$('.users-container tbody').html(data.result);
					} else if(data.status == '2'){
						$('.load_more').hide();
						$('.users-container tbody').html('<tr class="student-list-item item-empty"><td colspan="4"><center>{!! Lang::get("core.no_subscriber")!!}<center></td></tr>');
					}
				}
			});
		});
		$(document).on('click','.load_more',function(){ 
			var skip = $('.load_more').attr('data-skip');
			var _token = $('[name="_token"]').val();
			var search = $(".search-input").val();
			$.ajax({
				type: 'POST',
				url: '<?php echo e(\URL::to("courseusers/".$courseinfo->course_id)); ?>',
				data:'skip='+skip+'&search='+search+'&_token='+_token,
				beforeSend: function() {
					$('.item-empty').remove();
					$('.load_more').show();
					$('.load_more').html('<i class="fa fa-refresh fa-spin"></i> Loading...');
				},
				success : function(result) {
					$('.load_more').html('{!! Lang::get("core.load_more")!!}');
					var data = jQuery.parseJSON(result);
					if(data.status == '1'){
						$('.load_more').attr('data-skip',data.skip);
						$('.users-container tbody').append(data.result);
					} else if(data.status == '2'){
						$('.load_more').hide();
						$('.users-container tbody').append('<tr class="student-list-item item-empty"><td colspan="4"><center>{!! Lang::get("core.no_subscriber")!!}<center></td></tr>');
					}
				}
			});
		});
	}); //end functions
</script>
@stop