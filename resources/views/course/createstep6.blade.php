@extends('layouts.frontend')
@section('content')
<link href="<?php echo e(asset('assets/bsetec/static/css/front/style.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('assets/bsetec/static/css/front/sub-style.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('assets/bsetec/static/css/front/common.css')); ?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo e(asset('assets/bsetec/static/css/jquery-ui.css')); ?>">
<style type="text/css">
.form-group{ margin:25px 0px; }
.leftnopad{padding-left:0px !important;}
</style>
<div class="create-course privacy">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				@include('course.courseheader')
			</div>
			<div class="col col-sm-3 multi_development">
				@include('course.createsidebar')
			</div>
			<div class="col col-sm-9">

				@if($course->privacy!='' && $course->privacy==3)
					@php $default = 3 @endphp
				@else
					@php  $default = $course->privacy @endphp
				@endif

				<div class="lach_dev resp-tab-content course_tab"> 
					<div class="slider_divsblocks">
						<div>
							{!! Form::open(array('url'=>'course/updatecourse', 'method' => 'post', 'class'=>'form-horizontal saveLabel','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=>'privacyform')) !!}

							{!! Form::hidden('course_id', $course->course_id ) !!}

							{!! Form::hidden('step', $step ) !!}

							{!! Form::hidden('privacyhidden',$default,array('id'=>'privacyhidden')) !!}

							<div class="course_basic course_newbasic"><h4>{!! Lang::get('core.Privacy')!!}</h4><p>{!! Lang::get('core.privacy_tip')!!}</p></div>
							
							@if($course->privacy=='1' || $course->privacy=='')
							@php $first = 'active'; $firsttab = 'in active'; @endphp
							@endif

							@if($course->privacy!='1' && $course->privacy!='')
							@php $second = 'active'; $secondtab = 'in active'; @endphp
							@endif
							

							<ul id="myTab" class="privacy_tabs nav nav-tabs tab_public">
								<li class="{!! $first or '' !!}"><a class="selected" href="#public" data-toggle="tab" id="publicprivacy">{!! Lang::get('core.fr_mpublic')!!}</a></li>
								<li class="{!! $second or ''  !!}"><a class="selected" href="#private" data-toggle="tab" id="privateprivacy">{!! Lang::get('core.Private')!!}</a></li>
							</ul> 
							<div class="tab-content privacy_block">
								<div class="tab-pane fade {!! $firsttab or ''  !!}" id="public">
									<!-- <?php echo Form::radio('privacy', '1', ($course->privacy != 2) ? true : false, array('class'=>'privacy_pub','id'=>'privacy_1')); ?> -->
								{!! Lang::get('core.public_course')!!}
								</div>

								<div class="tab-pane fade {!! $secondtab or '' !!}" id="private">
									<ul class="tab_ul clearfix">
										<li>
											{!! Form::radio('privacy', '2', ($course->privacy == 2) ? true : true, array('class'=>'privacy_inv ','id'=>'privacy_2')) !!} {!! Lang::get('core.invitation_only') !!}
										</li>
										<li>{!! Form::radio('privacy', '3', ($course->privacy == 3) ? true : false, array('class'=>'privacy_pswd','id'=>'privacy_3')) !!} {!! Lang::get('core.password_protected') !!}

										</li> 
									</ul>
									@if($course->privacy != 2)
									@php $types = 'style="display:none;"'; @endphp
									@endif
									<p class="invitation" {!! $types or '' !!} >
										{!! Form::text('username','',array('class'=>'form-control','placeholder'=>Lang::get('core.private_email'),'id' => 'searchusers')) !!}
										<input type="button" value="{!! Lang::get('core.add_user')!!}" class="btn btn-color" id="addusers">
									</p>

									@if($course->privacy != 3)
									@php $stypes = 'style="display:none;"'; @endphp
									@endif
									<p class="password" {!! $stypes or '' !!}>
										@if($course->privacy == 3)
										@php $psw = explode('--pwd--',$course->privacy); @endphp
										<input type="text" name="privacy_password" id="privacy_password" class="password_text form-control password_newmrg" value="{!! $psw[1] !!}" />
										@else
										<input type="text" name="privacy_password" disabled id="privacy_password" class="password_text form-control password_newmrg" value="" />
										@endif</p>

										<table class="table table-bordered" id="utable" {!! $types or '' !!}>
											<thead>
												<tr>
													<th>{!! Lang::get('core.Name')!!}</th>
													<th>{!! Lang::get('core.Delete')!!}</th>
												</tr>
											</thead>
											<tbody>
												@if(count($inviteusers)>0)
												@foreach($inviteusers as $invites)
												@php  $userdetails = \bsetecHelpers::getuserinfobyid($invites->user_id);
												if(!empty($userdetails->first_name) && !empty($userdetails->last_name)){
												$userinfo   = $userdetails->first_name.' '.$userdetails->last_name;
											}elseif (!empty($userdetails->first_name)) {
											$userinfo   = $userdetails->first_name;
										}
										@endphp
										<tr><td>{!! $userinfo or '' !!}<input type="hidden" name="user_id[]" value="{!! $invites->user_id or '' !!}"></td><td><a href="javascript:void(0);" class="btn btn-danger removetr" data-userid="{!! $invites->user_id or '' !!}"><i class="fa fa-trash-o"></i></a></td></tr>
										@endforeach
										@else
										<tr class="nothing">
											<td colspan="2">{!! Lang::get('core.no_record')!!}</td>
										</tr>

										@endif

									</tbody>
								</table>


							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<p class="tip_privacy">
									<span id="tip">
										<strong>{!! Lang::get('core.TIP')!!}:</strong>
										{!! Lang::get('core.public_tip')!!} {!! CNF_APPNAME !!}. {!! Lang::get('core.private_tip')!!} {!! CNF_APPNAME !!}'{!! Lang::get('core.private_tips')!!}
									</span></p>
								</div>
							</div>

							<div class="form-group">
								<input type="submit" name="" value="{!! Lang::get('core.sb_save')!!}" class="btn btn-color">

							</div>
							{!! Form::close() !!}


						</div>
					</div>
				</div> 

			</div>

		</div>
	</div>
</div>
<script src="http://cdn.jsdelivr.net/jquery.validation/1.14.0/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script>
$(function(){

	$( "#searchusers" ).autocomplete({
		source: '<?php echo e(\URL::to("course/allusers/".$course->course_id)); ?>',
		minLength: 2,
		open: function() { $('#searchusers .ui-menu').width(300) }  

	});


	$(document).on('click','#addusers',function(e){
		var me = $(this);
		e.preventDefault();
		if ( me.data('requestRunning') ) {
			return;
		}
		me.data('requestRunning', true);
		var sdata = $('#searchusers').val();
		var courseid = $('[name="course_id"]').val();
		var lens     = $('.visisblecheck').length;
		$.ajaxSetup({
			headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
		});
		$.ajax({
			type: 'POST',
			url: '<?php echo e(\URL::to("course/addusers")); ?>',
			data:'emails='+sdata+'&courseid='+courseid,
			success : function(data) {
				var obj = jQuery.parseJSON(data);
				if(obj.status=='1'){
					$('.nothing').remove();
					$('#utable').append('<tr><td>'+obj.results.username+'<input type="hidden" name="user_id[]" value="'+obj.results.user_id+'"></td><td><a href="javascript:void(0);" class="btn btn-danger removetr" data-userid="'+obj.results.user_id+'"><i class="fa fa-trash-o"></i></a></td></tr>');
					
				}else if(obj.status=='2'){
					toastr.error("Error", "Sorry! This user is already added.");
					toastr.options = {
						"preventDuplicates": true,
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}
				}else if(obj.status=='3'){
					toastr.error("Error", "Please enter valid email");
					toastr.options = {
						"preventDuplicates": true,
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}
				}
				return false;
			},complete: function() {
				$('#searchusers').val('');
				me.data('requestRunning', false);
			}
		});
});

$(document).on('click','.removetr',function(e){
	$(this).closest('tr').remove();
	var userid = $(this).data('userid');
	var me = $(this);
	e.preventDefault();
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	var courseid = $('[name="course_id"]').val();

	$.ajaxSetup({
		headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
	});
	$.ajax({
		type: 'POST',
		url: '<?php echo e(\URL::to("course/deleteinviteusers")); ?>',
		data:'userid='+userid+'&courseid='+courseid,
		success : function(data) {
			return false;
		},complete: function() {
			me.data('requestRunning', false);
		}
	});

});

$('#privacy_3').on('ifChecked', function(event){
	$('.password').show();
	$('#privacy_password').removeAttr('disabled',false);
	$('.invitation,#utable').hide();
	$('#privacyhidden').val('3');
});

$('#privacy_2').on('ifChecked', function(event){
	$('.password').hide();
	$('#privacy_password').attr('disabled',true);
	$('.invitation,#utable').show();
	$('#privacyhidden').val('2');
});

$(document).on('click','#publicprivacy',function(){
	$('#privacyhidden').val('1');
});
$(document).on('click','#privateprivacy',function(){
	if($('#privacy_2').is(':checked')) { 
		$('.password').hide();
		$('#privacy_password').attr('disabled',true);
		$('.invitation,#utable').show();
		$('#privacyhidden').val('2');
	}else{
		$('.password').show();
		$('#privacy_password').removeAttr('disabled',false);
		$('.invitation,#utable').hide();
		$('#privacyhidden').val('3');
	}
});

jQuery.validator.addMethod("noSpace", function(value, element)
	{ return value.indexOf(" ") < 0 && value != ""; }, "No space please and don't leave it empty");

$("#privacyform").validate({
	rules: {
		privacy_password: {
			required: {
				depends:function(){
					return $('#privacy_3').is(':checked');
				}
			},
			noSpace: true
		}
	},
	messages: {
		privacy_password: {
			required: "Please enter password",
		}
	},submitHandler: function() {
		return true;
	}
});

	// alert('<?php echo e($course->privacy); ?>');

});
</script>
@stop