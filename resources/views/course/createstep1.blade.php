@extends('layouts.frontend')
@section('content')
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<style type="text/css">
.leftnopad{padding-left:0px !important;}
</style>
<div class="create-course get-start">
<div class="container">
	<div class="row">
        <div class="col-sm-12">
            @include('course.courseheader')
        </div>
		<div class="col-sm-3 multi_development">
			@include('course.createsidebar')
		</div>
		<div class="col-sm-9">
		     @php($sitename = \bsetecHelpers::get_options('sitename'))
			 <div class="lach_dev resp-tab-content"> 
                <div class="slider_divsblocks">
                    <h2>{!! Lang::get('core.getting_started') !!}</h2>
                    <h3 class="GetStartedTitle">{!! Lang::get('core.create_course') !!} <span class="arr"></span></h3>

                    <div class="GetStartedContent">
                        <div class="status-tabs-content">
                        {!! Lang::get('core.create_course_txt')!!}
                           <!-- <h4 class="fxac">{!! Lang::get('core.create_prepare')!!} {!! $sitename['sitename'] !!} {!! Lang::get('core.create_instructor')!!}</h4>
                            <p>{!! Lang::get('core.create_join_the') !!} {!! $sitename['sitename'] !!} {!! Lang::get('core.create_studio') !!}, {!! $sitename['sitename'] !!} {!! Lang::get('core.create_join')!!}</p>
                            <p>{!! Lang::get('core.create_how_to_create_course')!!} {!! $sitename['sitename'] !!} {!! Lang::get('core.course') !!}. </p>
                            {!! Lang::get('core.create_course_subtext')!!}-->
                        </div>
                    </div>

                    <h3 class="GetStartedTitle">{!! Lang::get('core.create_course_review')!!}<span class="arr"></span></h3>

                    <div class="GetStartedContent">
                        <div class="status-tabs-content">
                        {!! Lang::get('core.course_review_txt')!!}
                           <!-- <h4 class="fxac">{!! Lang::get('core.submit_prepare')!!} {!! $sitename['sitename'] !!} {!! Lang::get('core.submit_instructor')!!}</h4>
                            <p>{!! Lang::get('core.submit_join_the') !!} {!! $sitename['sitename'] !!} {!! Lang::get('core.submit_studio') !!}, {!! $sitename['sitename'] !!} {!! Lang::get('core.submit_join')!!}</p>
                            <p>{!! Lang::get('core.submit_how_to_create_course')!!} {!! $sitename['sitename'] !!} {!! Lang::get('core.course') !!}. </p>
                            {!! Lang::get('core.submit_course_subtext')!!}-->
                        </div>
                    </div>

                    <h3 class="GetStartedTitle">{!! Lang::get('core.course_publish') !!} <span class="arr"></span></h3>

                    <div class="GetStartedContent">
                        <div class="status-tabs-content">
                        {!! Lang::get('core.course_publish_txt')!!}
<!--                        
                             <h4 class="fxac">{!! Lang::get('core.after_prepare')!!} {!! $sitename['sitename'] !!} {!! Lang::get('core.after_instructor')!!}</h4>
                            <p>{!! Lang::get('core.after_join_the') !!} {!! $sitename['sitename'] !!} {!! Lang::get('core.after_studio') !!}, {!! $sitename['sitename'] !!} {!! Lang::get('core.after_join')!!}</p>
                            <p>{!! Lang::get('core.after_how_to_create_course')!!} {!! $sitename['sitename'] !!} {!! Lang::get('core.course') !!}. </p>
                            {!! Lang::get('core.after_course_subtext')!!}-->
                        </div>
                    </div>
                </div>
            </div> 
			
		</div>
		
	</div>
</div>
</div>

<script>
$(function(){
    $(".GetStartedTitle").click(function(){$(this).next(".GetStartedContent").slideToggle( "slow" );});
});
</script>
@stop