@extends('master/index')
@section('metaDescription')
@if(strlen($course->description) > 2)
<meta name="description" content="">
@else
<meta name="description" content="{{ $course->course_title}} {{ siteSettings('description') }}">
@endif
<meta property="og:title" content="{{ ucfirst($course->course_title) }} - {{ siteSettings('siteName') }}"/>
<meta property="og:image" content="{{ asset(zoomCrop('uploads/course/'.$course->image)) }}"/>
@stop
@section('content')

<!--Description-->

<div class="bg_container">
    <ul class="slider_menu container">
        <li><a href="{{url()}}" class="blog_color">Home</a></li>
        <li><a href="{{url('category/'.$course->categories->slug)}}" class="blog_color"><i class="fa fa-angle-right new_icons"></i>{{ $course->categories->name }} </a></li>
        <li><span><i class="fa fa-angle-right new_icons"></i>{{$course->course_title}}</span></li>
    </ul>
 </div>
 
<div class="description container">

<div class="row-fluid">
<div class="span12">

<div class="description_content span9">
<h2 class="course_descp">{{$course->course_title}}</h2>
<div class="btn-group course_descpbtns">
						  @if(Auth::check()==true)
                    @if(checkFavorite($course->course_id) == true)
                    <button type="button" class="btn btn-success favoritebtn" id="{{ $course->course_id }}"> <i class="fa fa-heart" style="color:#008000"></i>
                    </button>
                    @else
                    <button type="button" class="btn  btn-success favoritebtn" id="{{ $course->course_id }}"> <i class="fa fa-heart" style="color:grey"></i> </button>
                    @endif
                    @endif


                     {{-- <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        More
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        @if(siteSettings('allowDownloadOriginal') == 1 || siteSettings('allowDownloadOriginal') == 'leaveToUser' && $course->allow_download == 1)
                        <li>
                            <a href="{{ url('download/'.Crypt::encrypt($course->course_id).'/'.Crypt::encrypt($course->slug)) }}">Download Original</a>
                        </li>
                        @endif
                        <li><a href="{{ url('report/image/'.$course->course_id) }}">Report</a></li>
                        @if(Auth::check() == true)
                        @if(Auth::user()->id == $course->user_id)
                        <li><a href="{{ url('delete/image/'.$course->course_id) }}">Delete</a></li>
                        @endif
                        @endif
                    </ul> --}}
                </div>
<h3 class="course_descpsubtitle">{{$course->subtitle}}</h3>
<hr/>
<p>{{$course->description}}</p>
<h4>Category : {{ $course->categories->name }} </h4>

<div class="req_details">
    <div class="req_title">
<h5><a href="javascript:void(0);">What are the requirements?</a></h5>
<ul class="description_list req_ans" style="display: none;">
{{--*/ $courseReq =  explode('--txt--', $course->course_req) /*--}}
@foreach($courseReq as $courseReq)
 <li>{{$courseReq}}</li>
@endforeach
</ul>
    </div>
</div>

<div class="req_details">
    <div class="req_title">
<h5><a href="javascript:void(0);">What am I going to get from this course?</a></h5>
<ul class="description_list req_ans" style="display: none;">
{{--*/ $courseGoal =  explode('--txt--', $course->course_goal) /*--}}
@foreach($courseGoal as $courseGoal)
 <li>{{$courseGoal}}</li>
@endforeach
</ul>
    </div>
</div>

<div class="req_details">
    <div class="req_title">
<h5><a href="javascript:void(0);">What is the target audience?</a></h5>
<ul class="description_list req_ans" style="display: none;">
{{--*/ $courseTar =  explode('--txt--', $course->int_audience) /*--}}
@foreach($courseTar as $courseTar)
 <li>{{$courseTar}}</li>
@endforeach
</ul>

    </div>
</div>

    <div class="price clsClearfix">

        @if($course->pricing != 0)
        <div class="price_label">PRICE: <div class="dollar">$ {{$course->pricing}}</div> </div>
        @else
        <div class="price_label">PRICE: <div class="dollar">FREE</div> </div>
        @endif

          <div class="price_right">

            @if(Auth::check() == false)
            <div class="controls"><a data-toggle="modal" class="take_course" href="#LoginBox">TAKE THIS COURSE</a></div>
            @else
            @if(checkPurchase($course->course_id) == 0)
                <div class="modal fade model_width" id="CoursePurchase" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header header_bg">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h2 class="modal-title">Confirm purchase</h2>
                        </div>
                        <div class="modal-body cont">
                          <h4>{{$course->course_title}}</h4> <div class="dollar dollar_font">$ {{$course->pricing}}</div>
                            {{ Form::open(array('url' => url('payment'))) }}
                            <input type="hidden" name="itemnumber" value="{{$course->course_id}}" id="itemnumber" /> 
                            <input type="hidden" name="itemslug" value="{{$course->slug}}" id="itemnumber" /> 
                            <div class="controls control">
                            <input type="submit" value="Go to PayPal" class="take_course btn_pay">
                            <input type="button" value="Buy with Credit balance $ {{getUserCredit()}}" id="BuyWithCredit" class="take_course btn_pay">
                            </div> 
                            <p> You will be redirected to Paypal's payment page and then sent back once you complete your purchase.
                            By clicking the "Pay" button, you agree to these    <a href="{{url('tos')}}" target="_blank"> Terms of Service. </a></p>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
            @else
                <div class="modal fade" id="CoursePurchase" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="border:none;">
                    <div class="modal-content">
                        <div class="modal-header header_bg">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h2 class="modal-title">Already purchase</h2>
                        </div>
                        <div class="modal-body">
                          <h4>{{$course->course_title}}</h4>
                          <a href="{{url('learn-course').'/'.$course->course_id.'/'.$course->slug}}">Click here view course</a>
                        </div>
                    </div>
                </div>
                </div>
            @endif
            @if($course->pricing == 0) 
		    @if($course->user_id != Auth::user()->id)
            <div class="controls"><a class="take_course" href="{{url('subscribe-course/'.$course->course_id.'/'.$course->slug)}}">TAKE THIS COURSE</a></div>
            @else
            <div class="controls"><a class="take_course">It's your Course</a></div>
            @endif
            @else
	    @if($course->user_id != Auth::user()->id)
            <div class="controls"><a data-toggle="modal" class="take_course" href="#CoursePurchase">TAKE THIS COURSE</a></div>
	    @else
            <div class="controls"><a class="take_course">It's your Course</a></div>
	    @endif
            @endif 
            
            @endif
        </div>

    </div>

 <div class="remove_margin">
                <h2>Curriculum</h2>
                </hr>
                    <ul id="curriculum_ul" class="box_shadow">
                        <?php $curriculum = json_decode($course->curriculum); 
                            $curri_count = @count($curriculum->MainTitle); 
                            $count = 1;
                            ?>

                            @for ($j=0; $j < $curri_count; $j++)
                                @if($curriculum->MainTitle[$j] == 'Section')
                                <?php $class = 'main_li_curriculum new_curspace';?>
                                @else
                                <?php $class = 'sub_li_curriculum new_subcurr sub_bg';?>
                                @endif
                            <li class="ui-state-default curriculum_ul_li {{$class}}"> 
                             @if($curriculum->MainTitle[$j] == 'Section')
                                {{--*/ $count = 1 /*--}}
                                <span class="main_title">{{$curriculum->MainTitle[$j]}}  : </span>
                                <span><h4> {{$curriculum->title[$j]}}</h4></span>
                                @else
                                <p id="list_count">{{$count}}</p>
                                {{--*/ $count++ /*--}}
                                <span class="product_reviews"><h4> {{$curriculum->title[$j]}}</h4></span>
                                @endif

                            @if($curriculum->MainTitle[$j] == 'Lecture')
                            <div class="section_detections">
                                <?php 
                                $file_count = count($curriculum->file_type[$j]); ?>
                                @for($k=0; $k < $file_count; $k++)
                                @if($curriculum->file_type[$j][$k] != '')
                                <div class="file_divss">
                                    <a class="pull-right glyphicon glyphicon-trash DeleteContent" href="javascript:void(0)"></a>
                                    @if($curriculum->file_type[$j][$k] == 'audio/mpeg')
                                        <span class="audio_icon icon-headphones"></span>
                                    @elseif($curriculum->file_type[$j][$k] == 'video/mp4' || $curriculum->file_type[$j][$k] == 'video/x-flv')
                                        <span class="video_icon icon-play-circle"></span>
                                    @elseif($curriculum->file_type[$j][$k] == 'text/plain')
                                        <span class="text_icon icon-file"></span>
                                    @endif
                                </div>
                                @endif
                                @endfor
                            </div>
                            @elseif($curriculum->MainTitle[$j] == 'Quiz')
                            <div class="Add{{@$curriculum->MainTitle[$j]}}ContentDiv">
                                {{count($curriculum->file_type[$j])}} {{$curriculum->MainTitle[$j]}}
                                
                            </div>
                            @endif
                            </li>

                            @endfor
                    </ul>
           </div>

            <div class="profile_view">

            @if(ReviewCount($course->course_id)==0)
            <div class="response" style="text-align:center">No Reviews Yet</div>
            @else
            <div class="response">{{ReviewCount($course->course_id)}} Reviews</div>
            @endif

                @foreach($ratings as $rating)

                    <div class="media profile clsClearfix">
                        <div class="span2"><a href="{{ url('user/'.$rating->username) }}">
                            <img class="media-object" alt="{{ $rating->fullname }}" src="{{ avatar($rating->avatar,100,100) }}"></a>
							
						  <div class="ReadonlyRating startvalue star_rating" data-score="{{checkReview($course->course_id, $rating->id)}}"></div> 
							
                        </div>
                        <div class="profile_title clearfix user_content"> <a href="{{ url('user/'.$rating->username) }}">{{ $rating->fullname }}</a>
						
							<div class="profile_title" >{{ Smilies::parse(e($rating->review_title)) }}</div>
                        <div class="profile_content">{{ Smilies::parse(e($rating->review_description)) }}</div>
						
						</div>
                        
                        <div class="about_hours align_hours"><abbr class="timeago" title="{{ date(DATE_ISO8601,strtotime($rating->created_at)) }}">{{ date(DATE_ISO8601,strtotime($rating->created_at)) }}</abbr>&nbsp;</div> 
                        
                    </div>
                @endforeach                  
            </div>
    </div>


<div class="about_manager span3">
    <div class="manager clsClearfix">
        <div class="manager_image">
            <a href="{{url('user').'/'.$course->user->username}}"><img class="media-object imgfix thumbnail" alt="{{ $course->user->fullname }}" src="{{ avatar($course->user->avatar, 52,55) }}"></a>
        </div>
  
        <div class="manager_name">
            <div class="name">
                <a href="{{ url('user/'.$course->user->username) }}">{{$course->user->fullname }}</a>
            </div>
            <div class="work">{{$course->user->designation}}</div>
            <div><strong>Course Created</strong> 
                <abbr class="timeago" title="{{ date(DATE_ISO8601,strtotime($course->created_at)) }}">{{date(DATE_ISO8601,strtotime($course->created_at)) }}</abbr>
            </div>
            @if(Auth::check()==true)
                @if(Auth::user()->id == $course->user->id)
                    <a class="btn btn-default btn-xs edit_profiles" href="{{ url('settings') }}">Edit Profile</a>
                @else
                    @if(checkFollow($course->user->id))
                        <a type="button" class="btn btn-default btn-lg btn-block follow" id="{{ $course->user->id }}">{{ t('Un Follow') }}</a>
                    @else
                        <a type="button" class="btn btn-default btn-lg btn-block follow" id="{{ $course->user->id }}">{{ t('Follow Me') }}</a>
                    @endif
                @endif
            @endif
        </div>
    </div>
    <div class="manager_content ">{{Str::limit($course->user->about_me,200)}}</div>
    <ul class="listup clsClearfix"> 
        <li><div class="like clsClearfix"><div class="list_image"><i class="icon-heart"></i></div><div class="viewfavcount">{{ $numberOfFavorites }}</div><div class="list_text"> Likes</div></div></li>
        <li><div class="like clsClearfix"><div class="list_image"><i class="icon-shopping-cart"></i></div><div class="list_text">{{count($purchase)}} Students Enrolled</div></div></li>
        <li><div class="like clsClearfix"><div class="list_image"><i class="icon-eye-open"></i></div><div class="list_text">{{ReviewCount($course->course_id)}} Reviews</div></div></li>
        <!--<li><div class="like clsClearfix"><div class="list_image"><i class="icon-share"></i></div><div class="list_text">http://bit.ly/1eeul6r</div></div></li>-->
    </ul>
    <div class="about clsClearfix">
        <div class="about_title clsClearfix">
        <div class="about_more">{{ t('Share This') }}</div></div>
            <ul class="action-bar student_image clearfix social_iconss">
                <li><a href="https://twitter.com/intent/tweet?url={{ Request::url() }}
		@if(Auth::check()==true){{'?ref='.Auth::user()->username}}@endif" class="twitter" target="_blank"><span class="entypo-twitter"></span><i class="fa fa-twitter"></i></a></li>
                <li><a href="http://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}
		@if(Auth::check()==true){{'?ref='.Auth::user()->username}}@endif" class="facebook" target="_blank"><span class="entypo-facebook"><i class="fa fa-facebook"></i></span></a></li>
                <li><a href="https://plusone.google.com/_/+1/confirm?hl=en&url={{ Request::url() }}
		@if(Auth::check()==true){{'?ref='.Auth::user()->username}}@endif" class="gplus" target="_blank"><span class="entypo-gplus"></span><i class="fa fa-google-plus"></i></a></li>
                <li><a href="javascript:void(run_pinmarklet())"  class="pintrest"><span class="entypo-pinterest"></span><i class="fa fa-pinterest"></i></a></li>
            </ul>
    </div>    

    @if($course->user->course()->where('approved','=',1)->count() > 2)
        <div class="about clsClearfix">
            {{--*/ $morefromUser = $course->user->course()->where('approved','=',1)->where('course_id', '!=' ,$course->course_id )->take(12)->get() /*--}}
            <div class="about_title clsClearfix">
                <div class="about_more"> {{ t('More From') }} {{ $course->user->fullname }} ( {{count($morefromUser)}} )</div>
            </div>
            <div class="more_images MoreFromUser">
                @foreach($morefromUser as $sidebarImage)
        		<a href="{{ url('course/'.$sidebarImage->course_id.'/'.$sidebarImage->slug) }}">
                    <img src="{{ asset(zoomCrop('uploads/course/'.$sidebarImage->image ,75,75)) }}" alt="{{ $sidebarImage->course_title }}">
                </a>       
                @endforeach
            </div>
        </div>
    @endif

    <div class="about clsClearfix">
        <div class="about_title clsClearfix">
            <div class="about_more">{{ t('More From') }} {{ siteSettings('siteName') }}</div>
        </div>
        <div class="more_images MoreFromSite clearfix">
            @foreach(moreFromSite() as $sidebarImage)
            <a class="slide" href="{{ url('course/'.$sidebarImage->course_id.'/'.$sidebarImage->slug) }}" class="pull-left userimage">
            <img src="{{ asset(zoomCrop('uploads/course/'.$sidebarImage->image ,69,69)) }}" alt="{{ $sidebarImage->title }}" class="thumbnail">
            </a>
            @endforeach
        </div>
    </div>

    @if(count($purchase) > 0)
    <div class="student">
        <div class="student_title"><div class="enrolled">Students Enrolled ( {{count($purchase)}} )</div></div>
        <ul class="student_image clsClearfix">
            @foreach($purchase as $purchased)
            <li><a href="{{ url('user/'.$purchased->username) }}">
                <img alt="{{ $purchased->fullname }}" src="{{ avatar($purchased->avatar, 47,54) }}" class="thumbnail">
            </a></li>
            @endforeach
        </ul> 
    </div> 
    @endif
 
    <div class="tags">
        <div class="tags_title"><div class="tag">Tags</div></div>
        <ul class="tags_category">
            {{--*/ $courseTag =  explode(',', $course->keywords) /*--}}
            @foreach($courseTag as $courseTag)
             <li class="clsClearfix"><div class="tags_left"><a href="{{ url('tag/'.$courseTag) }}">{{$courseTag}}</a></div><!-- <div class="tags_right">10</div> --></li>
            @endforeach
        </ul>
    </div>
</div>

</div>
</div>
</div>

<!--End Description-->

@stop

@section('extrafooter')

<script type="text/javascript">
$('#BuyWithCredit').click(function(){
    var itemCost = {{$course->pricing}};
    var CreditBalance = {{getUserCredit()}};
    var calc = itemCost - CreditBalance;
    var itemslug = $("input[name=itemslug]").val();

	 if (CreditBalance < itemCost) {
        alert('Sorry!! Your account balance is ${{getUserCredit()}} Still you need $'+calc+' to purchase this course')
    }
	else {
        var ConFirm = confirm("Buy clicking Ok you will purchase the course!");
        if (ConFirm == true) {
            var course_id = $('#itemnumber').val();          
            $.ajax({
                type: "POST",
                url: "{{url('pay-course-payment')}}",
                data: {Course_id:course_id},
                success: function (data) {
                    window.location.href = "{{url('paymentsuccess')}}/"+course_id+'/'+itemslug
                }
            });
        } 
    }
});    
</script>

<style>
@media (min-width: 980px) and (max-width: 1199px)
{
	.banner_bg {height:368px !important;}	
}
</style>
    
{{-- @include('course/comment') --}}

{{-- @include('course/detailed_sidebar') --}}
@stop
