@extends('master/index')

@section('content')

<!--Description-->

<style>
.menu {display: none;margin: 0;}
.reduce{margin:0px;}
.footer{display:none!important;}
.copyright{display:none!important;}
.vjs-default-skin .vjs-big-play-button {left:273px;top:132px;}
body{overflow:hidden; background:#333333;}
.ui-state-default.curriculum_ul_li.main_li_curriculum h4 {color: #fff;}
.ui-state-default.curriculum_ul_li.change_color.sub_li_curriculum p {color: #fff;padding: 0 15px;float: left;}
.ui-state-default.curriculum_ul_li.sub_li_curriculum h4 {margin: 25px 0 0;padding: 0 20px 0 0;color:#fff;width: 250px;word-break: break-all;}
.fa-circle-new{line-height: 68px;padding: 0 10px;}
.reg_banner_empty{margin: 0px;}
</style>
 

<div class="description reduce">
<div class="row-fluid">
<div class="span12 bg_span">

<div class="span9 add_width">
   <div class="dasky_header "></div>

    <div id="dasky" class="add_width">
    <span id="timeinsecs" style="display:none">{{$timeinsecs}}</span>
	    <ul class="dasky_ul"><li><a href="{{url('course/'.$course->course_id.'/'.$course->slug)}}"> <i class="fa fa-chevron-left font_size"></i>Back to Course</a></li><!--<li><a href="#">Previous Course <i class="fa fa-chevron-right font_size"></i></a></li>--></ul>
   
		
        <?php 
        $curriculum = json_decode($course->curriculum);
        $curri_count = @count($curriculum->MainTitle); 
        ?>
            @for ($j=0; $j < $curri_count; $j++)

                @if($curriculum->MainTitle[$j] == 'Section')
                <?php $class = 'step year';?>
                @else
                <?php $class = 'step';?>
                @endif

            <div class="{{$class}}">

             @if($curriculum->MainTitle[$j] == 'Section')
                {{--*/ $count = 1 /*--}}
                <div class="dsk-titlenode"> {{$curriculum->MainTitle[$j]}}</div>
                <div class="dsk-content"><p class="dsk-year-info">{{$curriculum->title[$j]}}</p></div>
                @else
                <div class="dsk-circle">{{$curriculum->MainTitle[$j]}}</div>
                 <h2 class="dsk-circle-title">{{$curriculum->title[$j]}}</h2>
                @endif

            @if($curriculum->MainTitle[$j] == 'Lecture')
            <div class="dsk-content">

             <?php $file_count = count($curriculum->file_type[$j]); ?>
                    @for($k=0; $k < $file_count; $k++)
                    @if($curriculum->file_type[$j][$k] != '')
                    <div class="dsk-info">
                    <h2>{{$curriculum->title[$j]}} </h2>
                   
                    </div>
                        @if($curriculum->file_type[$j][$k] == 'audio/mpeg')
                            <audio controls name='{{$curriculum->file_name[$j][$k]}}' class='video-js'><source src='{{url()."/uploads/videos/".$curriculum->file_name[$j][$k]}}' type='{{$curriculum->file_type[$j][$k]}}'></audio>
                            <p class="clearfix text_blocks">{{ $curriculum->file_description[$j][$k] }}</p>
                        @elseif($curriculum->file_type[$j][$k] == 'video/mp4' || $curriculum->file_type[$j][$k] == 'video/x-flv')
                            <video id='my_video_lecture' name='{{$curriculum->file_name[$j][$k]}}' class='video-js  vjs-default-skin v_left' controls preload='auto' width='500' height='350' data-setup='{}'>
                            <source src='{{url()."/uploads/videos/".$curriculum->file_name[$j][$k]}}' type='{{$curriculum->file_type[$j][$k]}}'></video>
                             <p class="clearfix text_blocks">{{ $curriculum->file_description[$j][$k] }}</p>
                        @elseif($curriculum->file_type[$j][$k] == 'application/pdf')
                           <?php  
                            $file = url()."/uploads/videos/".$curriculum->file_name[$j][$k];
                            $shownfile1 = public_path()."/uploads/videos/".$curriculum->file_name[$j][$k];
                            exec("identify -format %n $shownfile1",$totpages1);
                            $elapse_timeinsecs1 = $totpages1[0]*$timeinsecs*1000;
                            echo '<p class="clearfix"><input type="hidden" name="elapsepdftime" id="elapsepdftime" value="'.$elapse_timeinsecs1.'"><iframe src="'.$file.'" width="100%" height="400px" top="100" class="pdfiframe"></iframe><br><a href="javascript:void(0)" onclick="completereading(\''.$curriculum->file_name[$j][$k].'\',\''.$elapse_timeinsecs1.'\')">Click Here to start Reading</a></p>';
                           ?>
                        @elseif($curriculum->file_type[$j][$k] == 'text/plain')
                            <?php $file = @file_get_contents(url()."/uploads/videos/".$curriculum->file_name[$j][$k],true);
                                if($file){
                                    $shownfile = public_path()."/uploads/videos/".$curriculum->file_name[$j][$k];
                                    exec("identify -format %n $shownfile",$totpages);
                                    $elapse_timeinsecs = $totpages[0]*$timeinsecs*1000;

                                    $file_content = substr($file, 0, 100).'...'; 
                                    echo '<p class="fileContent" class="txtframe"><input type="hidden" name="elapsetxttime" id="elapsetxttime" value="'.$elapse_timeinsecs.'">'.$file.'<br><a href="javascript:void(0)"  onclick="completereading(\''.$curriculum->file_name[$j][$k].'\',\''.$elapse_timeinsecs.'\')">Click Here to start Reading</a></p>'; 
                                } ?>
                        @endif
                    @endif
                    @endfor
                    
            </div>
            @elseif($curriculum->MainTitle[$j] == 'Quiz')
            <div class="Add{{@$curriculum->MainTitle[$j]}}ContentDiv">
                {{count($curriculum->file_type[$j])}} {{$curriculum->MainTitle[$j]}}
                
            </div>
            @endif
            </div>
            @endfor
   </div>
</div>
    <div class="about_manager total_width span3 reduce_space" id="main" style="">
	
	  <ul id="tabs_new">
      <li><a href="#" name="#tab1"><i class="fa fa-list icon_font"></i></a></li>
      <li><a href="#" name="#tab2"><i class="fa fa-comments icon_font"></i></a></li>
      <li><a href="#" name="#tab3"><i class="fa fa-file-text-o icon_font"></i></a></li>   
  </ul>

  <div id="content_new" class="sortable_ul sortable_bg courses_takensblades">
      <div id="tab1" class="course_takenn">
<ul id="curriculum_ul" class="taken_curriculumblocks">

                        <?php 
                        $curriculum = json_decode($course->curriculum);
                        $curri_count = @count($curriculum->MainTitle); 
                        ?>
                            @for ($j=0; $j < $curri_count; $j++)

                                @if($curriculum->MainTitle[$j] == 'Section')
                                <?php $class = 'main_li_curriculum';?>
                                @else
                                <?php $class = 'sub_li_curriculum';?>
                                @endif

                            <li class="ui-state-default curriculum_ul_li change_color {{$class}}"> 
                              <a href="{{url('learn-course/'.$course->course_id.'/'.$course->slug.'#/step-'.($j+1))}}">
                                @if($curriculum->MainTitle[$j] == 'Section')
                                    {{--*/ $count = 1 /*--}}
                                    <span class="main_title change_font">{{$curriculum->MainTitle[$j]}}  : </span>
                                    <span><h4> {{$curriculum->title[$j]}}</h4></span>
                                @else
                                    <p id="list_count"> <i class="fa fa-circle" style="color:grey"></i> {{--*/$count/*--}}</p>
                                    {{--*/ $count++ /*--}}
                                    <span><h4> {{$curriculum->title[$j]}}</h4></span>
                                @endif

                            @if($curriculum->MainTitle[$j] == 'Lecture')
                            <div>
                            <?php $file_count = count($curriculum->file_type[$j]); ?>

                                    @for($k=0; $k < $file_count; $k++)
                                    @if($curriculum->file_type[$j][$k] != '')
                                    @if (strpos(CompletedPercentage($course->course_id),$curriculum->file_name[$j][$k]))
                                        <!-- <i class="fa fa-circle fa-circle-new" style="color:green"></i> -->
                                        <button class="MakeItInCompleted MakeItButton" style="color:green" onclick="return MakeItCompleted($(this),'remove')" getname="{{$curriculum->file_name[$j][$k]}}"><i class="fa fa-check-square-o"></i></button>
                                    @else
                                        <!-- <i class="fa fa-circle fa-circle-new"></i> -->
                                        <button class="MakeItCompleted MakeItButton" style="color:grey" onclick="return MakeItCompleted($(this),'add')" getname="{{$curriculum->file_name[$j][$k]}}"><i class="fa fa-check-square-o"></i></button>
                                    @endif
                                 <!--   <div class="file_div">                                    
                                        @if($curriculum->file_type[$j][$k] == 'audio/mpeg')
                                            <i class="fa fa-file-audio-o"></i>
                                        @elseif($curriculum->file_type[$j][$k] == 'video/mp4' || $curriculum->file_type[$j][$k] == 'video/x-flv')
                                            <i class="fa fa-play-circle-o icon_fonts"></i>
                                        @elseif($curriculum->file_type[$j][$k] == 'text/plain')
                                            <i class="fa fa-file-text"></i>
                                        @endif
                                    </div>-->
                                    @endif
                                    @endfor
                            </div>
                            @endif
                            </a>
                            </li>

                            @endfor
                    </ul>
      </div>

<div id="tab2" class="course_takenn"> 
    <div class="profile_view tabs_space">
        <div class="response remove_margin response_color">{{ $numberOfComments }} Comments</div>         
    @if(Auth::check() == true)
        <div class="commentPostBox replymargin">
            {{ Form::textarea('comment','',array('id'=>'commenttextboxcontent','class'=>"form-control text_top textarea_height",'placeholder'=>t('Comment'))) }}
            <input type="hidden" id="CourseIDHidden" value="{{$course->course_id}}">
            <button class="btn btn-info CommentMainButton">{{ t('Comment') }}</button>
        </div>
    @endif   

    @foreach($comments as $comment)
        <div class="media profile response_color clsClearfix" id="comment-{{ $comment->comment_id }}">
    	    <span class="msg-time pull-right replymargin">
                @if(Auth::check() == TRUE)
                    @if($comment->user_id == Auth::user()->id || $course->id == Auth::user()->id)
                        <span class="pull-right"><button data-content="{{ $comment->comment_id }}" type="button" class="close delete-comment close_padd close_new" aria-hidden="true">&times;</button></span>
                    @endif
                @endif
            </span>
    	
            <div class="profile_image"><a href="{{ url('user/'.$comment->user->username) }}">
                <img class="media-object" alt="{{ $comment->user->fullname }}" src="{{ avatar($comment->user->avatar,75,75) }}"></a>
            </div>
                
            <div class="profile_title name_title hours new_hours"> <a href="{{ url('user/'.$comment->user->username) }}">{{ $comment->user->fullname }}</a> <div class="about_hours hours_mrg newhours_mrg span8"> <i class="fa fa-clock-o opt_class"></i> <abbr class="timeago" title="{{ date(DATE_ISO8601,strtotime($comment->created_at)) }}">{{ date(DATE_ISO8601,strtotime($comment->created_at)) }}</abbr>&nbsp;</div></div>
            <div class="profile_content font_media name_cont span9">{{ Smilies::parse(e($comment->comment)) }}</div>

            @if(Auth::check() == TRUE)
            <a class="replybutton reply_top" id="box-{{ $comment->comment_id }}">{{ t('Reply') }}</a>
            <div class="commentReplyBox replymargin" id="openbox-{{ $comment->comment_id }}">
                <input type="hidden" name="pid" value="19">
                {{ Form::textarea('comment','',array('id'=>'textboxcontent'.$comment->comment_id,'class'=>"form-control text_top textarea_height",'rows'=>0,'placeholder'=>t('Comment'))) }}
                <button class="btn btn-info replyMainButton" id="{{ $comment->comment_id }}">{{ t('Reply') }}</button>
                <a class="closebutton color_btn close_new" id="box-{{ $comment->comment_id }}">{{ t('Cancel') }}</a>
            </div>
            @endif

            @foreach($comment->reply as $reply)
            <div class="media media_space" id="reply-{{ $reply->id }}">
                <hr class="hr_margin hr_border">
                <a class="pull-left bla profile_image" href="{{ url('user/'.$reply->user->username) }}">
                    <img class="media-object" alt="{{ $reply->user->fullname }}" src="{{ avatar($reply->user->avatar,64,64) }}">
                </a>

                <div class="media-body">
                    <h4 class="media-heading comment comments"><a href="{{ url('user/'.$reply->user->username) }}" class="profile_title">{{ $reply->user->fullname }}</a>

                        @if(Auth::check() == TRUE)
                            @if($reply->user_id == Auth::user()->id || $course->id == Auth::user()->id || $reply->comment->user->id == Auth::user()->id)
                                <span class="right"><button data-content="{{ $reply->id }}" type="button" class="close delete-reply close_padd close_new" aria-hidden="true">&times;</button></span>
                            @endif
                        @endif
    					
    					<span class="about_hours about_space space_hours span6"> <i class="fa fa-clock-o opt_class"></i> <abbr class="timeago" title="{{ date(DATE_ISO8601,strtotime($reply->created_at)) }}">{{ date(DATE_ISO8601,strtotime($reply->created_at)) }}</abbr></span>

                        <span class="msg-time">
                            <i class="glyphicon glyphicon-time"></i>
                            <span>{{ Smilies::parse(e($reply->reply)) }}&nbsp;</span>
                        </span>

                    </h4>
                    <div class="about_hours about_space">
    				</div>
                </div>
            </div>
            @endforeach
            </div>
            @endforeach      
            {{ $comments->links() }}    
        </div>

        </div>
        <div id="tab3" class="course_takenn">
            <input type="text" onKeydown="Javascript: if (event.keyCode==13) addNotes();" class="file_text new_file" id="NotesText" placeholder="Start typing to take your note"/>
	        <input type="button" value="add" onclick="return addNotes()" class="add_newbtn" />
    	    <ul class="underline" style="color:white">
                {{getNotes($course->course_id)}}
            </ul> 
        </div>
    </div>
    </div>      
</div>
</div>
</div>

@stop

@section('extrafooter')

<!-- Read un Read -->

<script type="text/javascript">
    $(".video-js").bind("ended", function() {
        var file_name = $(this).attr('name');
        var user_id = {{Auth::user()->id}};
        var Course_id = {{$course->course_id}};
        $.ajax({
        type: "POST",
        url: "{{url('completion')}}",
        data: {file_name:file_name,user_id:user_id,Course_id:Course_id},
        success: function (data) {
          if(data=='update') {
           $('#curriculum_ul').find('button[getname="'+file_name+'"]').attr("onclick","return MakeItCompleted($(this),'remove')");
           $('#curriculum_ul').find('button[getname="'+file_name+'"] i').css('color','green');
          } 
        }
        });
    });

function completereading(filename,timeout) {
    setTimeout( function() {
        var file_name = filename;
        var user_id = {{Auth::user()->id}};
        var Course_id = {{$course->course_id}};
        $.ajax({
            type: "POST",
            url: "{{url('completion')}}",
            data: {file_name:file_name,user_id:user_id,Course_id:Course_id},
            success: function (data) {
              if(data=='update') {
               $('#curriculum_ul').find('button[getname="'+file_name+'"]').attr("onclick","return MakeItCompleted($(this),'remove')");
               $('#curriculum_ul').find('button[getname="'+file_name+'"] i').css('color','green');
              } 
            }
        });
    },timeout)
}   

function MakeItCompleted(ThisContent,Remove){
        var file_name = ThisContent.attr('getname');
        var user_id = {{Auth::user()->id}};
        var Course_id = {{$course->course_id}};
        //alert(Remove);
        if(Remove == 'remove'){
           SendData = {file_name:file_name,user_id:user_id,Course_id:Course_id,remove:'remove'}; 
        }else{
           SendData = {file_name:file_name,user_id:user_id,Course_id:Course_id};
        }
        $.ajax({
        type: "POST",
        url: "{{url('completion')}}",
        data: SendData,
        success: function (data) {
            if(Remove == 'remove'){
                ThisContent.children('i').css('color','grey');
                ThisContent.attr("onclick","return MakeItCompleted($(this),'')");
            }else{
                ThisContent.children('i').css('color','green');
                ThisContent.attr("onclick","return MakeItCompleted($(this),'remove')");
            }
        }
        });
}

    // $(".MakeItCompleted").click(function() {
    //     var thiss = $(this);
    //     var file_name = $(this).attr('getname');
    //     var user_id = {{Auth::user()->id}};
    //     var Course_id = {{$course->course_id}};
    //     $.ajax({
    //     type: "POST",
    //     url: "{{url('completion')}}",
    //     data: {file_name:file_name,user_id:user_id,Course_id:Course_id},
    //     success: function (data) {
    //       thiss.prev('i').css('color','green');
    //       //location.reload();
    //     }
    //     });
    // });

    // $(".MakeItInCompleted").click(function() {
    //     var thiss = $(this);
    //     var file_name = $(this).attr('getname');
    //     var user_id = {{Auth::user()->id}};
    //     var Course_id = {{$course->course_id}};
    //     var remove = 'r';
    //     $.ajax({
    //     type: "POST",
    //     url: "{{url('completion')}}",
    //     data: {file_name:file_name,user_id:user_id,Course_id:Course_id,remove:remove},
    //     success: function (data) {
    //       $(".MakeItCompleted").prev('i').css('color','green');
    //       thiss.prev('i').css('color','grey');
    //       //location.reload();
    //     }
    //     });
    // });    
</script>

<!-- Read un Read -->

<script type="text/javascript">
  function addNotes(){
    var notes = $('#NotesText').val();
    if(notes != '' ){
    var user_id = {{Auth::user()->id}};
    var Course_id = {{$course->course_id}};
      $.ajax({
      type: "POST",
      url: "{{url('addNotes')}}",
      data: {notes:notes,user_id:user_id,Course_id:Course_id},
      success: function (data) {
          $('#NotesText').val('');
          $('.underline').append('<li>'+notes+'<i class="fa fa-trash-o pull-right" onclick="return DeleteNotes($(this))"></i></li>');
      }
      });
    }
  }

  function DeleteNotes(thiss){
        var confirsm = confirm('This note will be removed permanently.');
        if(confirsm == true){
        thiss.parent('li').remove();
        var notes = $('.underline').html();
        var user_id = {{Auth::user()->id}};
        var Course_id = {{$course->course_id}};
        var remove = 'remove';
          $.ajax({
          type: "POST",
          url: "{{url('addNotes')}}",
          data: {remove:remove,notes:notes,user_id:user_id,Course_id:Course_id},
            success: function (data) {

            }
          });
        }
  }
</script>
@stop