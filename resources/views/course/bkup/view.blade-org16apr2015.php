@extends('master/index')
@section('metaDescription')
@if(strlen($course->description) > 2)
<meta name="description" content="">
@else
<meta name="description" content="{{ $course->course_title}} {{ siteSettings('description') }}">
@endif
<meta property="og:title" content="{{ ucfirst($course->course_title) }} - {{ siteSettings('siteName') }}"/>
@stop
@section('content')

<!--Description-->

<div class="main_second_section">
<div class="container">
<div class="row">

<div class="col-lg-9 photography_section">
<div class="auto">
<h1>{{$course->course_title}}</h1>
<p> {{$course->subtitle}}</p>
<div class="star_rating">
<ul class="star_one clearfix">
<li class="late_star"><a href=""></a></li>
<li class="late_star"><a href=""></a></li>
<li class="late_star"><a href=""></a></li>
<li class="late_star"><a href=""></a></li>
<li class="late_star"><a href=""></a></li>
<li class="late_star last">{{$course->rating_count}} ratings, {{count($purchase)}} students enrolled </li>
</ul>
</div>
</div>
<div class="banner_watch">
<a href=""><img src="{{ asset(zoomCrop('uploads/course/'.$course->image,840,376)) }}"></a>
</div>
</div>

<div class="col-lg-3 price_second_section">
<div class="select_price">
<ul class="price_dollar clearfix">
<li class="first">PRICE</li>
<li class="second">
 @if($course->pricing != 0) $ {{$course->pricing}} @else FREE @endif
</li>
<li class="last">
    @if(Auth::check() == false)
    <div class="controls"><a data-toggle="modal" class="btn btn-primary" href="#LoginBox">TAKE THIS COURSE</a></div>
    @else
        @if(checkPurchase($course->course_id) == 0)
            <div class="modal fade model_width" id="CoursePurchase" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header header_bg">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h2 class="modal-title">Confirm purchase</h2>
                    </div>
                    <div class="modal-body cont">
                      <h4>{{$course->course_title}}</h4> <div class="dollar dollar_font">$ {{$course->pricing}}</div>
                        {{ Form::open(array('url' => url('payment'))) }}
                        <input type="hidden" name="itemnumber" value="{{$course->course_id}}" id="itemnumber" /> 
                        <input type="hidden" name="itemslug" value="{{$course->slug}}" id="itemnumber" /> 
                        <div class="controls control">
                        <input type="submit" value="Go to PayPal" class="btn btn-primary btn_pay">
                        <input type="button" value="Buy with Credit balance $ {{getUserCredit()}}" id="BuyWithCredit" class="btn btn-primary btn_pay">
                        </div> 
                        <p> You will be redirected to Paypal's payment page and then sent back once you complete your purchase.
                        By clicking the "Pay" button, you agree to these    <a href="{{url('tos')}}" target="_blank"> Terms of Service. </a></p>
                        </form>
                    </div>
                </div>
            </div>
            </div>
        @else
            <div class="modal fade" id="CoursePurchase" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="border:none;">
                <div class="modal-content">
                    <div class="modal-header header_bg">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h2 class="modal-title">Already purchase</h2>
                    </div>
                    <div class="modal-body">
                      <h4>{{$course->course_title}}</h4>
                      <a href="{{url('learn-course').'/'.$course->course_id.'/'.$course->slug}}">Click here view course</a>
                    </div>
                </div>
            </div>
            </div>
        @endif
        @if($course->pricing == 0) 
            @if($course->user_id != Auth::user()->id)
            <div class="controls"><a class="btn btn-primary" href="{{url('subscribe-course/'.$course->course_id.'/'.$course->slug)}}">TAKE THIS COURSE</a></div>
            @else
            <div class="controls"><a class="btn btn-primary">It's your Course</a></div>
            @endif
        @else
            @if($course->user_id != Auth::user()->id)
                <div class="controls"><a data-toggle="modal" class="btn btn-primary" href="#CoursePurchase">TAKE THIS COURSE</a></div>
            @else
                <div class="controls"><a class="btn btn-primary">It's your Course</a></div>
            @endif
        @endif 
    
    @endif
</li>
</ul>
</div>
<div class="wish_list_section">
<ul class="listing_wish_luist clearfix">
<li class="first"><a href="" class="cupon">Redeem a Coupon</a></li>
<li><a href="" class="wisth_list">Add To Wishlist</a></li>
<li><a href="" class="gift">Gift This Course</a></li>
<li><a href="" class="bulk">Bulk Purchase</a></li>
<li class="last"><a href="" class="preview">Free Preview </a></li>
</ul>
<div class="money_back_section">
<ul class="life_time_back clearfix">
<li class="first"><a href="" class="money_back_policy">30 day money back guarantee!</a></li>
<li><a href="" class="acess_my_life_time">Lifetime Access. No Limits!</a></li>
<li class="last"><a href="" class="completeion">Certificate of Completion</a></li>
</ul>
</div>
</div>
</div>


</div>
</div>
</div>


<div class="main_third_section">
<div class="container">
<div class="row">

<div class="col-lg-9 dit_view">
<div class="over_view">
<h1>Course <span>Overview</span></h1>
</div>
<div class="likes_share">
<div class="keys_one">
<ul class="like_dislikes clearfix">
<li class="fb"><a href="">like</a></li>
<li class="lakhs_alone">3k <span class="one_port"></span></li>
<li class="tweet"><a href="">tweet</a></li>
<li class="lakhs_alone">316<span class="one_port"></span></li>
<li class="google_plus"><a href="">+1</a></li>
<li class="lakhs_alone">56<span class="one_port"></span></li>
<li class="share"><a href="">share</a></li>
<li class="lakhs_alone">4.6k<span class="one_port"></span></li>
</ul>
</div>
</div>
<div class="para_lack_hut">
<p>
{{$course->description}}
{{$course->course_goal}}
{{$course->int_audience}}
</p>
</div>
<div class="author_lack_hut">
<h1>About <span>the Author</span></h1>
<span class="over_autor"><a href="{{url('user').'/'.$course->user->username}}"><img class="media-object imgfix thumbnail" alt="{{ $course->user->fullname }}" src="{{ avatar($course->user->avatar, 76,79) }}"></a></span>
<p>{{$course->user->about_me }}</p>
</div>

<div class="course_curriculum">
<h1>Course <span>Curriculum</span></h1>
<div class="section_one_inroduction">

<?php $curriculum = json_decode($course->curriculum); 
$curri_count = @count($curriculum->MainTitle); 
$count = 1;
?>

@for ($j=0; $j < $curri_count; $j++)
   
    @if($curriculum->MainTitle[$j] == 'Section')
        @if($count>1) </ul></div><div class="section_one_inroduction"> @endif
        <h4> {{$curriculum->MainTitle[$j].' '.$count.': '.$curriculum->title[$j]}}</h4>
        <ul class="section_introduction_lasting clearfix">
        {{--*/ $count++ /*--}}
    
    @elseif($curriculum->MainTitle[$j] == 'Lecture')
        <li class="icon-play-circle first col-lg-10">{{$curriculum->title[$j]}}</li>
        <?php $file_count = count($curriculum->file_type[$j]); ?>
        @for($k=0; $k < $file_count; $k++)
        @if($curriculum->file_type[$j][$k] != '')
        <li class="minutes col-lg-2">
            <a class="pull-right glyphicon glyphicon-trash DeleteContent" href="javascript:void(0)"></a>
            @if($curriculum->file_type[$j][$k] == 'audio/mpeg')
                <span class="audio_icon icon-headphones"></span>
            @elseif($curriculum->file_type[$j][$k] == 'video/mp4' || $curriculum->file_type[$j][$k] == 'video/x-flv')
                <span class="video_icon icon-play-circle"></span>
            @elseif($curriculum->file_type[$j][$k] == 'text/plain')
                <span class="text_icon icon-file"></span>
            @endif
        </li>
        @endif
        @endfor
    @elseif($curriculum->MainTitle[$j] == 'Quiz')
    <li class="play first col-lg-10"><div class="Add{{@$curriculum->MainTitle[$j]}}ContentDiv">
        {{count($curriculum->file_type[$j])}} {{$curriculum->MainTitle[$j]}}
        
    </div></li>
    @endif
@endfor

@if($curri_count>0) </ul> @endif
</div>
<!-- End of Curriculum -->
<div class="section_reviews_inroduction">
<h1>Reviews</h1>
</div>
@if(ReviewCount($course->course_id)==0)
    <div class="click_one clearfix">No Reviews Yet</div>
@else
    @foreach($ratings as $rating)
    <div class="click_one clearfix">
        <div class="first_pasrt col-lg-2">
            <a href="{{ url('user/'.$rating->username) }}">
             <img class="media-object" alt="{{ $rating->fullname }}" src="{{ avatar($rating->avatar,86,86) }}"></a>
        </div>
        <div class="second_pasrt clearfix">
            <div class="second_on_part col-lg-5"><h5>{{ $rating->fullname }}</h5></div>
            <div class="third_pasrt col-lg-5 clearfix"><div class="star_rating">
                <ul class="star_one clearfix">
                    <li class="late_star ReadonlyRating startvalue star_rating" data-score="{{checkReview($course->course_id, $rating->id)}}"></li>
                    <li class="late_star last"><abbr class="timeago" title="{{ date(DATE_ISO8601,strtotime($rating->created_at)) }}">{{ date(DATE_ISO8601,strtotime($rating->created_at)) }}</abbr></li>
                </ul>
            </div>
        </div>
        <div class="four_pasrt col-lg-10"><p>{{ Smilies::parse(e($rating->review_description)) }}</p></div>
    </div>
    @endforeach 
@endif
</div>



<div class="see_comments"><button class="btn btn-primary">SEE MORE COMMENTS</button></div>
</div>



<div class="col-lg-3 instructor_one">
<div class="distruction clearfix">
<div class="instruction">
<h4>Instructor</h4>
</div>
<div class="first_time_return clearfix">
<div class="col-lg-5 samrt_man">
<a href="{{url('user').'/'.$course->user->username}}"><img class="media-object imgfix thumbnail" alt="{{ $course->user->fullname }}" src="{{ avatar($course->user->avatar, 76,79) }}"></a>
</div>
<div class="col-lg-7 richard_man">
<p><a href="{{ url('user/'.$course->user->username) }}">{{$course->user->fullname }}</a></p>
<span>{{$course->user->designation}}</span>
</div>
</div>
@if($course->user->course()->where('approved','=',1)->count() > 0)
{{--*/ $morefromUser = $course->user->course()->where('approved','=',1)->where('course_id', '!=' ,$course->course_id )->take(12)->get() /*--}}

<div class="more_coursers_at_here clearfix">
<p>More Courses by {{$course->user->fullname }}</p>
<?php if(count($morefromUser)>3) { ?> <p class="mot"></p><?php }?>
</div>
<?php $mu=1; ?>
@foreach($morefromUser as $sidebarImage)
<div class="first_time_return return_last clearfix">
<div class="col-lg-5 samrt_man">
    <a href="{{ url('course/'.$sidebarImage->course_id.'/'.$sidebarImage->slug) }}">
        <img src="{{ asset(zoomCrop('uploads/course/'.$sidebarImage->image ,76,79)) }}" alt="{{ $sidebarImage->course_title }}">
    </a>
</div>
<div class="col-lg-7 richard_man">
<p>{{$sidebarImage->course_title}}</p>
<div class="star_rating">
<ul class="star_one clearfix">

<li class="late_star last">({{$sidebarImage->rating_count}})</li>
</ul>
</div>
</div>
</div>
<?php if($mu==3) { break;} ?>
{{--*/ $mu++ /*--}}
@endforeach
@endif
</div>
<!-- Students Start -->
<div class="students_fix clearfix">
<div class="instruction">
<h4>Students</h4>
</div>
<div class="nine_students">
<ul class="attend_one_stud clearfix">
@if(count($purchase) > 0)
@foreach($purchase as $purchased)
<li class="col-lg-4 mini_stud">
    <a href="{{ url('user/'.$purchased->username) }}">
        <img alt="{{ $purchased->fullname }}" src="{{ avatar($purchased->avatar, 58,58) }}" class="thumbnail">
    </a>
</li>
 @endforeach
 @endif
</ul>
@if(count($purchase) > 3)
<div class="more_opt_stu">
<a href="">View more student</a>
</div>
@endif
</div>
</div>

<div class="user_view_fix clearfix">
<div class="instruction">
<h4>User View this Courses</h4>
</div>
<?php $mv=1; ?>
@foreach(moreFromSite() as $sidebarImage)
<div class="luck_time_return clearfix">
<div class="col-lg-5 samrt_man">
<a class="slide" href="{{ url('course/'.$sidebarImage->course_id.'/'.$sidebarImage->slug) }}" class="pull-left userimage">
            <img src="{{ asset(zoomCrop('uploads/course/'.$sidebarImage->image ,76,79)) }}" alt="{{ $sidebarImage->course_title }}" class="thumbnail">
            </a></div>
<div class="col-lg-7 richard_man">
<p>{{$sidebarImage->course_title}}</p>
<div class="star_rating">
<ul class="star_one clearfix">
</ul>
</div>
<span class="easy_teruns"><a href="">@if($sidebarImage->pricing != 0) $ {{$sidebarImage->pricing}} @else FREE @endif</a></span>
</div>
</div>
<?php if($mv==3) { break;} ?>
{{--*/ $mv++ /*--}}
@endforeach

</div>



</div>

</div>
</div>
</div>
<!--End Description-->

@stop

@section('extrafooter')

<script type="text/javascript">
$('#BuyWithCredit').click(function(){
    var itemCost = {{$course->pricing}};
    var CreditBalance = {{getUserCredit()}};
    var calc = itemCost - CreditBalance;
    var itemslug = $("input[name=itemslug]").val();

	 if (CreditBalance < itemCost) {
        alert('Sorry!! Your account balance is ${{getUserCredit()}} Still you need $'+calc+' to purchase this course')
    }
	else {
        var ConFirm = confirm("Buy clicking Ok you will purchase the course!");
        if (ConFirm == true) {
            var course_id = $('#itemnumber').val();          
            $.ajax({
                type: "POST",
                url: "{{url('pay-course-payment')}}",
                data: {Course_id:course_id},
                success: function (data) {
                    window.location.href = "{{url('paymentsuccess')}}/"+course_id+'/'+itemslug
                }
            });
        } 
    }
});    
</script>

<style>
@media (min-width: 980px) and (max-width: 1199px)
{
	.banner_bg {height:368px !important;}	
}
</style>
    
{{-- @include('course/comment') --}}

{{-- @include('course/detailed_sidebar') --}}
@stop
