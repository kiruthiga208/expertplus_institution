@if(count($reviews)>0) 
@foreach($reviews as $results)
@php  $rateid = $results->ratings_id;  @endphp
<li class="litems">
 <div class="course_view_feedback_author_img"><a href="{!! URL::to('profile/'.$results->username) !!}">{!! SiteHelpers::customavatar($results->email,$results->user_id) !!}</a></div>
 <div class="course_view_feedback_content">
   <div class="course_view_feedback_author_name"><a href="{!! URL::to('profile/'.$results->username) !!}">{!! $results->first_name or '' !!}   {!! $results->last_name or '' !!}</a></div>
   <div class="course_view_feedback_desc">{!! $results->review_description or '' !!}</div>
   <!-- <div class="course_view_feedback_rating"></div> -->
   <div class="star_rating course_view_feedback_rating">
    <ul class="star_one clearfix">
      <li class="late_star ReadonlyRating startvalue star_rating" data-score="{{\bsetecHelpers::checkReview($results->course_id,$results->user_id)}}"></li>

    </ul>
  </div>
  <div class="course_view_feedback_date">{!! SiteHelpers::changeFormat($results->created_at) !!}</div>
</div>
</li>
@endforeach

@else
<li>{!! Lang::get('core.no_more_reviews') !!}</li>
@endif

@if(!empty($rateid) && count($nextrec)>0)
<ul id="rpagination-{!! $rateid !!}" class="pagination" >
  <li class="next"><a href="javascript:void(0);" data-next="{!! $rateid or '' !!}" data-cid="{!! $courseid !!}" class="btn btn-primary seereviews">{!! Lang::get('core.see_more_comments') !!}</a></li>
</ul>
@endif

<script type="text/javascript">
$(function(){
  $('.ReadonlyRating').raty({score: function() {return $(this).attr('data-score');},readOnly: true,starOff : "{{url().'/assets/bsetec/static/img/star-off.png'}}",starOn: "{{url().'/assets/bsetec/static/img/star-on.png'}}",starHalf : "{{url().'/assets/bsetec/static/img/star-half.png'}}"});
});
</script>

