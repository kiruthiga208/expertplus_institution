{!! Form::open(array('method' => 'post','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="commentbox" >
	<div class="form-group">
		<input type="hidden" name="courseid" value="">
		<input type="text" name="posttitle" class="form-control" id="posttitle" placeholder="{!! Lang::get('core.start_discussion')!!}">
	</div>
	<div class="form-group" >
		<textarea id="postcomments" class="form-control lectureeditor lectureeditoradd" rows="2" placeholder="Discuss the course and topics related to its field. No promotions please." name="comment" cols="50"></textarea> 
	</div> 

	<div class="form-group" >
		@if(count($curriculum)>0)
		<select name="lectureid" id="lectureid" class="form-control">
		<option value="">--{!! Lang::get('core.select_lecture') !!}--</option>
			@php  $no = 1; 
			 $lno = 1; 
			 $qno = 1; @endphp
			@for($i=0;$i<count($curriculum);$i++)
			@php  $lectures = \SiteHelpers::getlectures($curriculum[$i]->section_id); @endphp
			@for($j=0;$j<count($lectures);$j++)
			@if($lectures[$j]->type==0)
			<option value="{!! $lectures[$j]->lecture_quiz_id !!}" data-sectionid="{!! $curriculum[$i]->section_id !!}">{!! Lang::get('core.Lecture')!!} {!! $lno !!} {{$lectures[$j]->title}}</option>
			@php $lno++; @endphp
			@else
			<option value="{!! $lectures[$j]->lecture_quiz_id !!}" data-sectionid="{!! $curriculum[$i]->section_id !!}">{!! Lang::get('core.Quiz') !!} {!! $qno !!} {{$lectures[$j]->title}}</option>
			@php $qno++; @endphp
			@endif
			@endfor
			@php $no++; @endphp
			@endfor
		</select>
		@endif
	</div> 


	<div class="form-group">              
		<button class="btn btn-orange btn-bg postbtn" type="button" >{!! Lang::get('core.Post')!!}</button>
	</div>
</div>
{!! Form::hidden('course_id', $courseid)  !!}
{!! Form::close() !!}
<script type="text/javascript">
$(function(){
	var align = 'ltr';
	@if(CNF_RTL==1)
 		var align = 'rtl';
	@endif
	tinymce.init({	
				mode : "specific_textareas",
				editor_selector : 'lectureeditoradd',
				theme : "advanced",
				theme_advanced_buttons1 : "bold,italic,underline,bullist,numlist,link,image",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				width : "100%",
				plugins : "paste",
				paste_text_sticky : true,
					directionality : align,
				setup : function(ed) {
					ed.onInit.add(function(ed) {
						ed.pasteAsPlainText = true;
					});
				}
			});
	});

	$(document).on('click','.postbtn',function(e){

		var texttitles = $('#posttitle').val();
		var lectureid = $('#lectureid').val();
		var  con = tinyMCE.activeEditor.getContent({format : 'text'});
		var textvalues =con.trim();

	if($.trim(texttitles).length===0){
		toastr.error("Error", "{!! Lang::get('core.discussion_valid_title') !!}");
		toastr.clear();
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "200",
			"hideDuration": "1000",
			"timeOut": "3000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut",
			"preventDuplicates": true
		}
	}else if($.trim(textvalues).length===0){

		toastr.error("Error", "{!! Lang::get('core.discussion_valid_comments')!!}");
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "3000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut",
			"preventDuplicates": true
		}
	}
	else if($.trim(lectureid).length===0){
		toastr.error("Error", "{!! Lang::get('core.lecture_error') !!}");
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "3000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut",
			"preventDuplicates": true
		}
}
	else if ($.trim(textvalues).length>0 && $.trim(texttitles).length>0){
		var me = $(this);
		var lectureid 	= $('#lectureid').val();
		var sectionid 	= $('#lectureid').find('option:selected').attr('data-sectionid');
		var courseid    = $('[name="courseid"]').val();

		e.preventDefault();
		if ( me.data('requestRunning') ) {
			return;
		}

		me.data('requestRunning', true);

		$.ajaxSetup({
			headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
		});
		$.ajax({
			type: 'POST',
			url: '<?php echo e(\URL::to("course/insertlecturecomments")); ?>',
			data:'comments='+textvalues+'&title='+texttitles+'&lid='+lectureid+'&sid='+sectionid+'&cid='+courseid,
			success : function(data) {
				var obj = jQuery.parseJSON(data);
				if(obj.status=='1'){
					
					$('#discussionmodel').modal('hide');
					$('#posttitle').val('');
					tinyMCE.get('postcomments').setContent('');
					var stype = obj.results.stype;
					if(stype == 1){
					$('#allcomments').prepend('<div class="media profile clearfix" id="comment-'+obj.results.commentid+'"> <div class="activity-post"> <div class="profile_image"><a href="">'+obj.results.imgpath+'</a> </div> <div class="activity-box discussion-activity-box"> <div class="activity-header clearfix" action-icon="icon-comments" action-text="posted a discussion"> <div class="header-right clearfix pull-right"> <div class="hidden-btns"> <a href="javascript:void(0);" data-commentid="'+obj.results.commentid+'" class="editcomments"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0);" data-commentid="'+obj.results.commentid+'" class="removecomments"><i class="fa fa-trash-o"></i></a> </div> </div> <div class="header-left clearfix"> <a><span class="activity-header-link"><span><strong>'+obj.results.fname+'</strong></span></span></a> <a>posted a discussion</a> <span class="activity-details-flex-wrapper"><a><span class=""><strong>in Quiz '+obj.results.lecture+' </strong></span></a></span> <span class="activity-actions-separator"> &middot; </span> <time class="activity-time nowrap"><a>Just Now</a></time> </div> </div> <div> <div class=""> <div class="activity-body" id="wholewrapper-'+obj.results.commentid+'"> <div class="activity-title" id="titletext-'+obj.results.commentid+'">'+obj.results.texttitles+'</div> <div class="activity-content w3c-default" id="bodytext-'+obj.results.commentid+'">'+obj.results.comments+'</div> </div> <div style="display:none;" id="commentopenbox-'+obj.results.commentid+'"> <div class="form-group"> <input type="hidden" name="pid" value="'+obj.results.commentid+'"> <input type="text" name="posttitle" class="form-control" id="edittitle-'+obj.results.commentid+'" value="'+obj.results.texttitles+'" placeholder="Start a new discussion"> </div> <div class="form-group"> <textarea id="editctextboxcontent-'+obj.results.commentid+'" class="form-control lectureeditor" rows="2" placeholder="Comment" name="comment" cols="50">'+obj.results.comments+'</textarea>  </div>  <div class="form-group">               <button class="btn btn-orange btn-bg updatecommentbtn" type="button" data-commentid="'+obj.results.commentid+'" >Update</button> <button class="btn btn-orange btn-bg editccancelbtn" data-commentid="'+obj.results.commentid+'" type="button">Cancel</button> </div> </div> <div class="activity-actions clearfix"> <a href="javascript:void(0)"> <span> <span><span class="txt-orange replybutton" id="openreplybox" data-commentid="'+obj.results.commentid+'"><strong>Reply</strong></span></span> </span> </a> </div> <div class="commentReplyBox" style="display:none;" id="openbox-'+obj.results.commentid+'"> <input type="hidden" name="pid" value="'+obj.results.commentid+'"> <div class="form-group"> <textarea id="textboxcontent-'+obj.results.commentid+'" class="form-control lectureeditor" rows="2" placeholder="Comment" name="comment" cols="50"></textarea> </div> <div class="form-group"> <button class="btn btn-orange btn-bg replybtn" type="button" data-commentid="'+obj.results.commentid+'">Reply</button> <button class="btn btn-orange btn-bg cancelbtn" data-commentid="'+obj.results.commentid+'" type="button">Cancel</button> </div> </div> <div class="discussion-comments-block" id="replyies-'+obj.results.commentid+'"> </div> </div> </div> </div> </div> </div>');
				    }
				    else{
					$('#allcomments').prepend('<div class="media profile clearfix" id="comment-'+obj.results.commentid+'"> <div class="activity-post"> <div class="profile_image"><a href="">'+obj.results.imgpath+'</a> </div> <div class="activity-box discussion-activity-box"> <div class="activity-header clearfix" action-icon="icon-comments" action-text="posted a discussion"> <div class="header-right clearfix pull-right"> <div class="hidden-btns"> <a href="javascript:void(0);" data-commentid="'+obj.results.commentid+'" class="editcomments"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0);" data-commentid="'+obj.results.commentid+'" class="removecomments"><i class="fa fa-trash-o"></i></a> </div> </div> <div class="header-left clearfix"> <a><span class="activity-header-link"><span><strong>'+obj.results.fname+'</strong></span></span></a> <a>posted a discussion</a> <span class="activity-details-flex-wrapper"><a><span class=""><strong>in Lecture '+obj.results.lecture+' </strong></span></a></span> <span class="activity-actions-separator"> &middot; </span> <time class="activity-time nowrap"><a>Just Now</a></time> </div> </div> <div> <div class=""> <div class="activity-body" id="wholewrapper-'+obj.results.commentid+'"> <div class="activity-title" id="titletext-'+obj.results.commentid+'">'+obj.results.texttitles+'</div> <div class="activity-content w3c-default" id="bodytext-'+obj.results.commentid+'">'+obj.results.comments+'</div> </div> <div style="display:none;" id="commentopenbox-'+obj.results.commentid+'"> <div class="form-group"> <input type="hidden" name="pid" value="'+obj.results.commentid+'"> <input type="text" name="posttitle" class="form-control" id="edittitle-'+obj.results.commentid+'" value="'+obj.results.texttitles+'" placeholder="Start a new discussion"> </div> <div class="form-group"> <textarea id="editctextboxcontent-'+obj.results.commentid+'" class="form-control lectureeditor" rows="2" placeholder="Comment" name="comment" cols="50">'+obj.results.comments+'</textarea>  </div>  <div class="form-group">               <button class="btn btn-orange btn-bg updatecommentbtn" type="button" data-commentid="'+obj.results.commentid+'" >Update</button> <button class="btn btn-orange btn-bg editccancelbtn" data-commentid="'+obj.results.commentid+'" type="button">Cancel</button> </div> </div> <div class="activity-actions clearfix"> <a href="javascript:void(0)"> <span> <span><span class="txt-orange replybutton" id="openreplybox" data-commentid="'+obj.results.commentid+'"><strong>Reply</strong></span></span> </span> </a> </div> <div class="commentReplyBox" style="display:none;" id="openbox-'+obj.results.commentid+'"> <input type="hidden" name="pid" value="'+obj.results.commentid+'"> <div class="form-group"> <textarea id="textboxcontent-'+obj.results.commentid+'" class="form-control lectureeditor" rows="2" placeholder="Comment" name="comment" cols="50"></textarea> </div> <div class="form-group"> <button class="btn btn-orange btn-bg replybtn" type="button" data-commentid="'+obj.results.commentid+'">Reply</button> <button class="btn btn-orange btn-bg cancelbtn" data-commentid="'+obj.results.commentid+'" type="button">Cancel</button> </div> </div> <div class="discussion-comments-block" id="replyies-'+obj.results.commentid+'"> </div> </div> </div> </div> </div> </div>');
				    }
					tinyMCE.execCommand('mceAddControl', false, 'editctextboxcontent-'+obj.results.commentid); 
					tinyMCE.execCommand('mceAddControl', false, 'textboxcontent-'+obj.results.commentid); 
					
					toastr.success("Success", "{!! Lang::get('core.disccusion_comment_success')!!}");
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut",
						"preventDuplicates": true
					}
				}else if(obj.status=='2'){
					toastr.error("Error", "{!! Lang::get('core.discussion_wrong')!!}");
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut",
						"preventDuplicates": true
					}
				}

				return false;
			},complete: function() {
				me.data('requestRunning', false);
			}
		});
}

});



</script>