<style>
input[type=number] {
    -moz-appearance:textfield;
}
.payment-block .form-group label.error{
    color:red;
}
</style>
<div class="confirm-section">
                <div class="container">                
                  <h2 class="confirm-title">{!! Lang::get('core.confirm_purchase') !!}</h2>    
                   <div class="table-payment">  
                    <div id="couponError"></div>       
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                <tr>
                <!-- <th width="10%">Sl No:</th> -->
                <th width="90%">{!! Lang::get('core.name_content') !!}</th>
                <th width="10%">{!! Lang::get('core.price')!!}</th>
                </tr>
                <tr>
               @if (defined('CNF_CURRENCY'))
                        @php( $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
                @endif
                <!-- <td>1</td> -->
                <td>{{$course->course_title}}</td>
                <td> {{$currency .' '. $course->pricing}}</td>
                </tr>
                @if($admin_discount)
                <tr>
               
                <td>{!! Lang::get('core.admin_discount') !!}</td>
                <td> {{ $currency.' '.$discount_amount }}</td>
                </tr>
                @endif    
                <tr id="coupon_discount_div" style="display:none;">
                <!-- <td>3</td> -->
                <td>{!! Lang::get('core.coupon_discount') !!}</td>
                <td>{!! $currency !!} <span id="coupon_discount"></span></td>
                </tr>
                <tr>
                <td colspan="2">

                               @if(\bsetecHelpers::checkcoupon($course->course_id) == 1)
                <h4 class="redeem">{!! Lang::get('core.redeem') !!}</h4>
                <div class="hide_block clearfix" id="coupon_code_div">
                  
                    <input type="text" id="couponCode" onClick="$('#checkCoupon').removeAttr('disabled');" class="" autocomplete="off" placeholder="{!! Lang::get('core.enter_coupon')!!}">
                    
                        <button class="btn btn-success" type="button" id="checkCoupon">{!! Lang::get('core.Apply')!!}</button>
         
             
                </div>
                @endif
                </td>
                </tr>
                <tr>
                <td style="text-align:left;">{!! Lang::get('core.you_pay') !!}</td>
                <td>{!! $currency !!} <span id="you_pay">{{$you_pay}}</span><span id="coupon_price"></span></td>
                </tr>
                </table>     
                </div>      
                <div class="payment-block clearfix">
                @if($errors->any())
                    <div class="alert alert-danger">
                       <a class="close" data-dismiss="alert">×</a>
                       {!! Lang::get('core.payment_error') !!}
                    </div>
                @endif
                
                <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                @php( $i=1 ) 
                @if(count($payment)>0)  
                    @php(  $me = array('ccavenue','bank','cod') )
                   @foreach ($payment as $key => $method)
                    @php( $kk = in_array($key, $me) )
                        @if($kk==1)
                            @php( $cc = \bsetecHelpers::ifInstalled($key) )
                        @endif
                        @if($i==1)
                            @if($key=='ccavenue' || $key == 'bank' || $key=='cod')
                                @if($cc>0)
                                     <li class="active"><a href="{!! '#'.$key !!}" data-toggle="tab">{!! Lang::get('core.'.$key) !!}</a></li>
                                @endif
                            @else
                                 <li class="active"><a href="{!! '#'.$key !!}" data-toggle="tab">{!! Lang::get('core.'.$key) !!}</a></li>
                            @endif
                            <script>
                                $(document).ready(function(){
                                    var active = $('#tabs').find('.active').children().attr('href');
                                    $(active).addClass('active');
                                });
                            </script>
                        @else
                            @if($key=='ccavenue' || $key == 'bank' || $key=='cod')
                                @if($cc>0)
                                     <li class=""><a href="{!! '#'.$key !!}" class="{!! $key.'-tab' !!}" data-toggle="tab">{!! Lang::get('core.'.$key) !!}</a></li>
                                @endif
                            @else
                                <li class=""><a href="{!! '#'.$key !!}" class="{!! $key.'-tab' !!}" data-toggle="tab">{!! Lang::get('core.'.$key) !!}</a></li>
                            @endif
                        @endif
                        @php( $i++ )
                    @endforeach
                @else
                     <h2>{!! Lang::get('core.no_payment')!!}</h2>
                @endif
                </ul>
                
                <div id="my-tab-content" class="tab-content">

    @foreach ($payment as $key => $method)
                @if($key == 'paypal_express_checkout' && $method['payment']=='1')
                <div id="paypal_express_checkout" class="tab-pane ">
                {!! Form::open(array('url' => url('payment/form'))) !!}
                <input type="hidden" name="course_id" value="{{$course->course_id}}" /> 
                <input type="hidden" name="course_title" value="{{$course->slug}}" /> 
                <input type="hidden" name="payment_method" value="paypal_express_checkout" /> 
                
                <div class="form-group clearfix">
                <div class="col-sm-4 submit_block"><label style="visibility:hidden;">{!! Lang::get('core.sb_submit') !!}</label></div>
                <div class="col-sm-8">
                <input type="submit" value="{!! Lang::get('core.payment_option') !!}" class="btn btn-color">
                </div>
                </div> 
                {!! Form::close() !!}
                </div>
                @endif

                @if($key == 'paypal_standard' && $method['payment']=='1' )
                <div id="paypal_standard" class="tab-pane">
                {!! Form::open(array('url' => url('payment/form'),'parsley-validate'=>'','novalidate'=>' ' )) !!}
                <input type="hidden" name="course_id" id="course_id" value="{{$course->course_id}}" /> 
                <input type="hidden" name="course_title" value="{{$course->slug}}" /> 
                <input type="hidden" name="payment_method" value="paypal_standard" /> 
                
                <div class="form-group clearfix">
                <div class="col-sm-4 submit_block"><label style="visibility:hidden;">{!! Lang::get('core.sb_submit') !!}</label></div>
                <div class="col-sm-8">
                <input type="submit" value="{!! Lang::get('core.payment_option') !!}" class="btn btn-color">
                </div>
                </div> 
                {!! Form::close() !!}
                </div>
                @endif

                @if($key == 'stripe' && $method['payment']=='1')
                <div id="stripe" class="tab-pane col-md-6 ">
                {!! Form::open(array('url' => url('payment/stripe'),'id'=>'payment-stripe' )) !!}
                <span class="payment-errors"></span>
                <input name='stripeToken' id="stripeToken" type="hidden" />
                <input type="hidden" name="course_id" value="{{$course->course_id}}" /> 
                <input type="hidden" name="course_title" value="{{$course->slug}}" /> 
                <input type="hidden" name="payment_method" value="stripe" /> 
                
                <div class="form-group clearfix">
                <label for="Card Number" class="control-label col-sm-4 text-left"> {!! Lang::get('core.card_number')!!} <span class="asterix"> * </span></label>
                <div class="col-sm-8">
                {!! Form::text('card-number', '' , array('class'=>'form-control','id'=>'card-number','required'=>true,'placeholder'=>Lang::get('core.enter_card'),'autocomplete'=>'off')) !!} 
                </div>
                </div>
                
                <div class="form-group clearfix">
                <label for="card-cvc" class="control-label col-sm-4 text-left"> {!! Lang::get('core.card_cvc') !!} <span class="asterix"> * </span></label>
                <div class="col-sm-3">
                {!! Form::input('number','card-cvc', '' , array('class'=>'form-control','maxlength'=>'3','id'=>'card-cvc','required'=>true,'placeholder'=>Lang::get('core.enter_cvc'),'autocomplete'=>'off')) !!} 
                </div>
                </div>
                <div class="form-group clearfix">
                <label for="expire" class="col-sm-4">{!! Lang::get('core.Expiration') !!} <span class="asterisk"> * </span></label>
                <div class="expiry-wrapper clearfix">
                <div class="col-xs-6 col-sm-4">
                <div class="select-style_block">
                <select class="stripe-sensitive required" id="card-expiry-month">
                <option value="" selected="selected">{!! Lang::get('core.mm') !!}</option>
                </select>
                </div>
                <script type="text/javascript">
                var select = $("#card-expiry-month"),
                month = new Date().getMonth() + 1;
                for (var i = 1; i <= 12; i++) {
                select.append($("<option value='"+i+"'>"+i+"</option>"))
                }
                </script>
                </div>
                <div class="col-xs-6 col-sm-4">
                <div class="select-style_block">
                <select class="stripe-sensitive required" id="card-expiry-year">
                <option value="" selected="selected">{!! Lang::get('core.year') !!}</option>
                </select>
                </div>
                <script type="text/javascript">
                var select = $("#card-expiry-year"),
                year = new Date().getFullYear();
                for (var i = 0; i < 12; i++) {
                select.append($("<option value='"+(i + year)+"'>"+(i + year)+"</option>"))
                }
                </script>
                </div>
                </div>
                </div>
                <div class="form-group clearfix">
                <div class="col-sm-4 submit_block"><label style="visibility:hidden;">{!! Lang::get('core.sb_submit') !!}</label></div>
                <div class="col-sm-8">
                <input type="hidden" value="{!! $method['publishable_key'] !!}" class="stripe_key" />
                <input type="submit" id="submit_button" value="{!! Lang::get('core.payment_option') !!}" class="btn btn-color">
                <!-- <input type="button" value="Buy with Credit balance $ {{ \bsetecHelpers::getUserCredit()}}" id="BuyWithCredit" class="btn btn-primary btn_pay"> -->
                </div>
                </div> 
                
                {!! Form::close() !!}
                </div>
                @endif
                 @if($key == 'ccavenue' && $method['Status']=='true')
                     <div id="ccavenue" class="tab-pane">
                     <h1>{!! Lang::get('core.ccavenue-payment') !!}</h1>
                        <form method="post" id="checkout" name="customerData" action="{!! url('ccavenue/ccavenuerequest') !!}">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <input type="hidden" name="course_id" id="course_id" value="{{$course->course_id}}" /> 
                            <input type="hidden" name="tid" id="tid" readonly />
                           <?php 
                           if(!empty($method['merchant_id']))
                                $mech=$method['merchant_id'];
                            else
                                $mech='';
                            ?>
                            <input type="hidden" name="merchant_id" value="{!! $mech !!}"/>
                            <input type="hidden" id="order_id" name="order_id" value=""/>
                            <input type="hidden" name="currency" value="INR"/>
                            <input type="hidden" name="amount" class="pay-amount" value="{!! $course->pricing !!}"/>
                            <input type="hidden" name="redirect_url" value="{!! \URL::to($method['redirect_url']) !!}"/>
                            <input type="hidden" name="cancel_url" value="{!! \URL::to($method['cancel_url']) !!}"/>
                            <input type="hidden" name="language" value="EN"/>
                            <input type="hidden" name="billing_name" value="{!! $user_info->username !!}"/>
                            <input type="hidden" name="billing_address" value="{!! CNF_APPNAME !!}"/>
                            <input type="hidden" name="billing_city" value="{!! CNF_APPNAME !!}"/>
                            <input type="hidden" name="billing_state" value="{!! CNF_APPNAME !!}"/>
                            <input type="hidden" name="billing_zip" value="425001"/>
                            <input type="hidden" name="billing_country" value="{!! CNF_APPNAME !!}"/>
                            <input type="hidden" name="billing_tel" value="7896541236"/>
                            <input type="hidden" name="billing_email" value="{!! $user_info->email !!}"/>
                            <input type="hidden" name="delivery_name" value="{!! $user_info->username !!}"/>
                            <input type="hidden" name="delivery_address" value="{!! CNF_APPNAME !!}"/>
                            <input type="hidden" name="delivery_city" value="{!! CNF_APPNAME !!}"/>
                            <input type="hidden" name="delivery_state" value="{!! CNF_APPNAME !!}"/>
                            <input type="hidden" name="delivery_zip" value="425001"/>
                            <input type="hidden" name="delivery_country" value="{!! CNF_APPNAME !!}"/>
                            <input type="hidden" name="delivery_tel" value="7896541236"/>
                            <input type="submit" value="{!! Lang::get('core.payment_option') !!}" class="btn btn-color events-submit">
                        </form>
                        <script type="text/javascript">
                         window.onload = function() {
                            var d = new Date().getTime();
                            document.getElementById("tid").value = d;
                            document.getElementById("order_id").value = d+"{{$course->course_id}}";
                        };
                        </script>
                     </div>
                @endif
                @if($key == 'cod' && $method['Status']=='true')
                     <div id="cod" class="tab-pane cashpi">
                        <div class="cash">
                        {!! Lang::get('core.cod_msg') !!}
                        </div>
                        {!! Form::open(array('url' => url('bank/cashondelivery'),'id' => 'cod_form')) !!}
                            <input type="hidden" name="course_id" value="{{$course->course_id}}" /> 
                            <input type="hidden" name="price" value="{{$course->pricing}}" />
                            <input type="hidden" name="devicetypes" value="web" />
                            <input type="hidden" name="paymenttype" value ="cod" />
                            <input type="hidden" name="coupon_price" value="" id="cou_price_cod"/>

                        <div class="form-group clearfix">
                            <label for="Card Number" class="control-label col-sm-2 text-left"> {!! Lang::get('core.Name') !!} <span class="asterix"> * </span></label>
                            <div class="col-sm-8">
                            <input type="text" name="name" class="form-control"  placeholder="Enter Your Name"  autocomplete="false"  />
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label for="Card Number" class="control-label col-sm-2 text-left"> {!! Lang::get('core.Telephone') !!} <span class="asterix"> * </span></label>
                            <div class="col-sm-8">
                            <input type="text"  name="phone" class="form-control"  placeholder="Enter Mobile Number" autocomplete="false" />
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label for="Card Number" class="control-label col-sm-2 text-left">{!! Lang::get('core.address') !!} <span class="asterix"> * </span></label>
                            <div class="col-sm-8">
                           <textarea class="form-control" name="address" rows="5"  >  </textarea>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <label for="Card Number" class="control-label col-sm-2 text-left"></label>
                            <div class="col-sm-8">
                           <input type="submit" value="submit" size="10" class="btn btn-color">
                            </div>
                        </div>
                        {!! Form::close() !!}         
                        </div>
                @endif
                @if($key == 'bank' && $method['Status']=='true')
                    <div id="bank" class="tab-pane banking">
                        <div class="banks">
                        {!! Lang::get('core.bank_msg') !!}
                        </div>
                        <div class="ban_det">
                        <h3>{!! Lang::get('core.bank_details') !!}</h3>
                        <ul>
                        {!! $method['info'] !!}
                        </ul>
                        </div>
                        {!! Form::open(array('url' => url('bank/cashondelivery') , 'id' => 'bank_form')) !!}
                            <input type="hidden" name="devicetypes" value="web" />
                            <input type="hidden" name="course_id" value="{{$course->course_id}}" /> 
                            <input type="hidden" name="price" value="{{$you_pay}}" />
                            <input type="hidden" name="paymenttype" value ="bank" />
                            <input type="hidden" name="coupon_price" value="" id="cou_price_bank"/>
                            
                         <div class="form-group clearfix">
                            <label for="Card Number" class="control-label col-sm-2 text-left">{!! Lang::get('core.Name') !!} <span class="asterix"> * </span></label>
                            <div class="col-sm-8">
                           <input type="text" name="name" class="form-control"  autocomplete="false" />
                            </div>
                         </div>

                        <div class="form-group clearfix">
                            <label for="Card Number" class="control-label col-sm-2 text-left">{!! Lang::get('core.Telephone') !!} <span class="asterix"> * </span></label>
                            <div class="col-sm-8">
                            <input type="text" name="phone" class="form-control"  autocomplete="false" />
                            </div>
                        </div>
                        
                        <div class="form-group clearfix">
                            <label for="Card Number" class="control-label col-sm-2 text-left">{!! Lang::get('core.bank_details') !!} <span class="asterix"> * </span></label>
                            <div class="col-sm-8">
                            <textarea class="form-control" name="bankdetails" rows="5"  >  </textarea> 
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <label for="Card Number" class="control-label col-sm-2 text-left"></label>
                            <div class="col-sm-8">
                            <input type="submit" value="submit" size="10" class="btn btn-color">
                            </div>
                        </div>
                        {!! Form::close() !!}  
                    </div>
                @endif
                @endforeach
                </div>
                </div>
                </div>
                </div>
<!-- payment plugins -->
<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
<script>
    $('#cod_form').validate({
        rules:{
            name:{
                required:true,
            },
            phone:{
                required : true,
                number: true
            },
            address:{
                required : true,
            }

        },
        messages:{
            name:{
                required : "{{ Lang::get('core.name_cod_required') }}"
            },
            phone:{
                required : "{{ Lang::get('core.phone_cod_required') }}",
                number : "{{ Lang::get('core.phone_cod_number') }}"

            },
            address:{
                required : "{{ Lang::get('core.address_cod_required') }}"
            }
        },
        submitHandler:function(form){
            form.submit();
        }
});
$('#bank_form').validate({
        rules:{
            name:{
                required:true,
            },
            phone:{
                required : true,
                number: true
            },
            bankdetails:{
                required : true,
            }

        },
        messages:{
            name:{
                required : "{{ Lang::get('core.name_cod_required') }}"
            },
            phone:{
                required : "{{ Lang::get('core.phone_cod_required') }}",
                number : "{{ Lang::get('core.phone_cod_number') }}"

            },
            bankdetails:{
                required : "{{ Lang::get('core.details_bank_required') }}"
            }
        },
        submitHandler:function(form){
            form.submit();
        }
});
</script>
<script type="text/javascript">
// payment options script start
var keyy = $('.stripe_key').val();
Stripe.setPublishableKey(keyy);
var payment_error = $('.payment-errors');
$(document).ready(function() {
    function submit(form) {
        // given a valid form, submit the payment details to stripe
        $(form['submit-button']).attr("disabled", "disabled")
        Stripe.createToken({
            number: $('#card-number').val(),
            cvc: $('#card-cvc').val(),
            exp_month: $('#card-expiry-month').val(), 
            exp_year: $('#card-expiry-year').val()
        },function(status, response) {
            if (response.error) {
                // re-enable the submit button
                $(form['submit-button']).removeAttr("disabled")
                // show the error
               payment_error.html(response.error.message);
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the stripe token
                $('#stripeToken').val(token);
                // and submit
                form.submit();
            }
        });
        
        return false;
    }
            
    // add custom rules for credit card validating
    jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber, "Please enter a valid card number");
    jQuery.validator.addMethod("cardCVC", Stripe.validateCVC, "Please enter a valid security code");
    // We use the jQuery validate plugin to validate required params on submit
    $("#payment-stripe").validate({
        submitHandler: submit,
        rules: {
            "card-cvc" : {
                cardCVC: true,
            },
            "card-number" : {
                cardNumber: true,
            },
        },
        errorPlacement: function(error, element){
            payment_error.html(" ");
            payment_error.html(error);
        }
    });
});


// COUPON PROCESS
$('body').on('click','#checkCoupon',function(){
  var couponCode = $('#couponCode');
  var couponError = $('#couponError');
  var couponprice = $('#coupon_price');
  var coupricecod = $('#cou_price_cod');
  var coupricebank = $('#cou_price_bank');

  var coupon_discount = $('#coupon_discount');
  var coupon_div =  $('#coupon_discount_div');
  var you_pay = $('#you_pay');
  var coupon_code_div =  $('#coupon_code_div');
  $(this).attr('disabled','disabled');
  courseID = "{{$course->course_id}}";
  couponError.html("");
  if(couponCode.val().length > 4){
    $.ajaxSetup({
          headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
    });
    $.ajax({
      url: '{{ \URL::to("course/couponcheck") }}',
      type: 'POST',
      dataType:'json',
      data:{code:couponCode.val(),course_id:courseID},
      }).done(function(res){
           if(res.success_message){
              couponError.html('<div class="alert alert-success">'+res.success_message+'</div>');
              coupon_div.show();
              coupon_discount.text(res.discount_amount);
              you_pay.hide();
               couponprice.text(res.amount);
              coupricecod.val(res.amount);
              coupricebank.val(res.amount);

             
              coupon_code_div.slideToggle();
              couponCode.val("");
              $('.redeem').addClass('hide');
            }
        }).error(function(data) {
            couponprice.text("");
            you_pay.show();
            coupon_div.hide();
            var errors = data.responseText;
            res = $.parseJSON(errors);
            couponError.html('<div class="alert alert-danger">'+res.errors+'</div>');
            $(this).removeAttr('disabled');
            couponCode.val("");
            $('.redeem').removeClass('hide');
       });

  }else{
    couponError.html('<div class="alert alert-danger">{!! Lang::get("core.coupon_code_error") !!}</div>');
    $('.redeem').removeClass('hide');
  }
  setTimeout(function(){
    $('.alert').fadeOut();
  },3000);
});

</script>
<style>
.payment-errors{
    color:red;
}
</style>
