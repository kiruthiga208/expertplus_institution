@extends('layouts.frontend')
@section('content')
<link href="<?php echo e(asset('assets/bsetec/static/css/front/style.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('assets/bsetec/static/css/front/sub-style.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('assets/bsetec/static/css/front/common.css')); ?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo e(asset('assets/bsetec/static/css/jquery-ui.css')); ?>">
<style type="text/css">
.form-group{ margin:25px 0px; }
.leftnopad{padding-left:0px !important;}
</style>
<div class="create-course instructors">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				@include('course.courseheader')
			</div>
			<div class="col col-sm-3 multi_development">
				<?php echo $__env->make('course.createsidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			</div>
			<div class="col col-sm-9">

				<div class="lach_dev resp-tab-content course_tab"> 
					<div class="slider_divsblocks">
						<div>
							<?php echo Form::open(array('url'=>'course/updateinstructors', 'method' => 'post', 'class'=>'form-horizontal saveLabel','files' => true , 'parsley-validate'=>'','novalidate'=>' ')); ?>

							<?php echo Form::hidden('course_id', $course->course_id,array('id'=>'course_id') ); ?>

							<?php echo Form::hidden('step', $step ); ?>

							<div class="course_basic course_newbasic"><h4> {!! Lang::get('core.Manage_Instructors')!!}</h4><p>{!! Lang::get('core.instructor_text')!!}</p></div>
							<?php

							if(isset($dinstructors->instrctor_visitble) && $dinstructors->instrctor_visitble=='1'){
								$dvisibles = 'true';
								$dvalues   = '1';
							}elseif(isset($dinstructors->instrctor_visitble) && $dinstructors->instrctor_visitble=='0'){
								$dvisibles = '';
								$dvalues   = '0';
							}else{
								$dvisibles = 'true';
								$dvalues   = '1';
							}
							?>
							<table class="table table-bordered" id="instable">
								<thead>
									<tr>
										<th>{!! Lang::get('core.Instructor')!!}</th>
										
										<th>{!! Lang::get('core.Delete')!!}</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><?php echo \Session::get('fid'); ?><?php echo Form::hidden('user_id[]', \Session::get('uid') ); ?></td>
										
										<td></td>
									</tr>

									<?php 
									if(count($instructors)>0){
										for ($i=0; $i < count($instructors); $i++) { 
											
											$userdetails = \bsetecHelpers::getuserinfobyid($instructors[$i]->user_id);
											if(!empty($userdetails->first_name) && !empty($userdetails->last_name)){
												$userinfo   = $userdetails->first_name.' '.$userdetails->last_name;
											}elseif (!empty($userdetails->first_name)) {
												$userinfo   = $userdetails->first_name;
											}
											?>

											<tr>
												<td><?php echo e($userinfo); ?><?php echo Form::hidden('user_id[]', $instructors[$i]->user_id ); ?></td>
												
												<td><a href="javascript:void(0);" class="btn btn-danger removetr" data-userid="<?php echo e($instructors[$i]->user_id); ?>"><i class="fa fa-trash-o"></i></a></td>
											</tr>

											<?php
										}
									}
									?>


								</tbody>
							</table>
							
							<div class="form-group">
								
								<?php echo Form::text('instructorname','',array('class'=>'form-control','placeholder'=>Lang::get('core.email'),'id' => 'searchinstructor')); ?>
								<input type="button" value="{!! Lang::get('core.manage_add_instructor')!!}" class="btn btn-color" id="addinstructors">
								<span id="helpBlock" class="help-block"><b>{!! Lang::get('core.Note')!!}:</b>  {!! Lang::get('core.Only')!!}<?php echo e(CNF_APPNAME); ?>{!! Lang::get('core.note_user')!!}</span>
								
								
								
								
							</div>

							<div class="form-group">		
								
								<input type="submit" name="" value="{!! Lang::get('core.sb_save')!!}" class="btn btn-color">
								
							</div>
							<?php echo Form::close(); ?>

						</div>
					</div>
				</div> 

			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$( "#searchinstructor" ).autocomplete({
		source: '<?php echo e(\URL::to("course/instructors/".$course->course_id)); ?>',
		minLength: 2,
		open: function() { $('#searchinstructor .ui-menu').width(300) }  

	});

	$(document).on('click','#addinstructors',function(e){
		var me = $(this);
		e.preventDefault();
		if ( me.data('requestRunning') ) {
			return;
		}
		me.data('requestRunning', true);
		var sdata = $('#searchinstructor').val();
		var courseid = $('#course_id').val();
		var lens     = $('.visisblecheck').length;
		$.ajaxSetup({
			headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
		});
		$.ajax({
			type: 'POST',
			url: '<?php echo e(\URL::to("course/addinstructors")); ?>',
			data:'emails='+sdata+'&courseid='+courseid,
			success : function(data) {
				var obj = jQuery.parseJSON(data);
				if(obj.status=='1'){
					$('#instable').append('<tr><td>'+obj.results.username+'<input type="hidden" name="user_id[]" value="'+obj.results.user_id+'"></td><td><a href="javascript:void(0);" class="btn btn-danger removetr" data-userid="'+obj.results.user_id+'"><i class="fa fa-trash-o"></i></a></td></tr>');
					
				}else if(obj.status=='2'){
					toastr.clear();
					toastr.error("Error", "{!! Lang::get('core.already_choosen')!!}");
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "3000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut",
						"preventDuplicates": true
					}
				}else if(obj.status=='3'){
					toastr.clear();
					toastr.error("Error", "{!! Lang::get('core.choose_valid_email')!!}");
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "3000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut",
						"preventDuplicates": true
					}
				}
				return false;
			},complete: function() {
				$('#searchinstructor').val('');
				me.data('requestRunning', false);
			}
		});
});

$(document).on('click','.removetr',function(e){
	$(this).closest('tr').remove();
	var userid = $(this).data('userid');
	var me = $(this);
	e.preventDefault();
	if ( me.data('requestRunning') ) {
		return;
	}
	me.data('requestRunning', true);
	var courseid = $('#course_id').val();

	$.ajaxSetup({
		headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
	});
	$.ajax({
		type: 'POST',
		url: '<?php echo e(\URL::to("course/deleteinstructors")); ?>',
		data:'userid='+userid+'&courseid='+courseid,
		success : function(data) {
			return false;
		},complete: function() {
			me.data('requestRunning', false);
		}
	});

});

$(document).on('ifChecked','.visisblecheck', function(event){
	$(this).parent('div').parent('td').children('#visiblehidden').val('1');
});

$(document).on('ifUnchecked','.visisblecheck',function(event){
	$(this).parent('div').parent('td').children('#visiblehidden').val('0');
});

$(document).on('ifChecked','.caneditcheck', function(event){
	$(this).parent('div').parent('td').children('#canedithidden').val('1');
});

$(document).on('ifUnchecked','.caneditcheck', function(event){
	$(this).parent('div').parent('td').children('#canedithidden').val('0');
});


$('#searchinstructor').keypress(function(e) {
	if (e.keyCode == '13') {
		e.preventDefault();
	}
});



});
</script>

@stop