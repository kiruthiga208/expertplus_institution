@extends('layouts.frontend')
@section('content')
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<style type="text/css">
.form-group{ margin:25px 0px; }
.leftnopad{padding-left:0px !important;}
</style>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
            @include('course.courseheader')
        </div>
		<div class="col col-lg-3 multi_development">
			@include('course.createsidebar')
		</div>
		<div class="col col-lg-9">
		
			 <div class="lach_dev resp-tab-content course_tab"> 
                <div class="slider_divsblocks">
					<div>
						{!! Form::open(array('url'=>'course/updatecourse', 'method' => 'post', 'class'=>'form-horizontal saveLabel','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
						{!! Form::hidden('course_id', $course->course_id )  !!}
						{!! Form::hidden('step', $step )  !!}
						
						{!! Form::close() !!}

					</div>
                </div>
            </div> 
			
		</div>
		
	</div>
</div>
@stop