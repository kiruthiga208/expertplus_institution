@extends('layouts.frontend')
@section('content')
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/css/taginputs/jquery.tagsinput.css') }}" rel="stylesheet">
<style type="text/css">
.form-group{ margin:25px 0px; }
.leftnopad{padding-left:0px !important;}
</style>
<div class="create-course">
<div class="container">
	<div class="row">
		<div class="col-sm-12">
            @include('course.courseheader')
        </div>
		<div class="col col-sm-3 multi_development">
			@include('course.createsidebar')
		</div>
		<div class="col col-sm-9">
		
			 <div class="lach_dev resp-tab-content course_tab"> 
                <div class="slider_divsblocks">
					<div>
						{!! Form::open(array('url'=>'course/updatecourse', 'method' => 'post', 'class'=>'form-horizontal saveLabel','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
						{!! Form::hidden('course_id', $course->course_id )  !!}
						{!! Form::hidden('step', $step )  !!}
						<div class="course_basic course_newbasic"><h4>{!! Lang::get('core.Basics')!!}</h4><p>{!! Lang::get('core.basic_txt') !!}</p></div>
						
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.Title') !!} <span class="asterisk">*</span></label>
							
							{!!  Form::text('title',$course->course_title,array('class'=>'form-control','placeholder'=>Lang::get('core.basic_title'),'maxlength' => 60, 'required'=>'true','onkeypress'=>'max_text($(this),60)'))  !!}
								<span class="textBoxLimit newtextBoxLimit">{!! 60-strlen($course->course_title) !!}</span>
							
						</div>
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.SubTitle') !!}</label>
						
				{!!  Form::textarea('subtitle',$course->subtitle,array('class'=>'form-control','placeholder'=>Lang::get('core.basic_subtitle')))  !!}
							
						</div>
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.Keywords') !!}</label>
							
		<input type="text" autocomplete="off" value="{!! $course->keywords !!}" name="tags" id="tagit" placeholder="Tags" class="form-control" data-original-title=""/>
							
						</div>
                          <div class="row">
                         <div class="col-sm-6">
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.Category') !!}</label>
							
							<select name="cat_id" id="cats" class="form-control" required>
								<option value="9">{!! Lang::get('core.select_category')!!}</option>
								@foreach(\bsetecHelpers::siteCategories() as $category)
								@if($category->id == $course->cat_id)
								<option value="{!!  $category->id  !!}" selected="selected">{!!  $category->name  !!}</option>
								@else
								<option value="{!!  $category->id  !!}">{!!  $category->name  !!}</option>
								@endif
								@endforeach
								</select>
							
						</div>
                        </div>
                        @if(isset($course->sub_cat_id))
                        <div class="col-sm-6">
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.subcategory') !!}</label>
								<select name="sub_cat_id" id="sub_cats" class="form-control">
									<option value="">{!! Lang::get('core.select_subcategory')!!}
									</option>
								</select>
								<input type="hidden" name="hiddensubcat" id="subcat" value="{!! $course->sub_cat_id !!}">
						</div>
                        </div>
                        @endif
                        </div>


						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.language') !!}</label>
						
								<select name="lang_id" class="form-control" required>
									<option value="40">{!! Lang::get('core.select_language')!!}</option>
									@foreach(\bsetecHelpers::siteLanguage() as $language)
									@if($language->id == $course->lang_id)
									<option value="{!!  $language->id  !!}" selected="selected">{!!  $language->native_name  !!}</option>
									@else
									<option value="{!!  $language->id  !!}">{!!  $language->native_name  !!}</option>
									@endif
									@endforeach
								</select>
							
						</div>
               


						<div class="form-group">
<input type="submit" name="" value="Save" class="btn btn-color"> 
						
						</div>
						{!! Form::close() !!}

					</div>
                </div>
            </div> 
			
		</div>
		
	</div>
</div>
</div>
<script type="text/javascript" src="{{ URL::to('assets/bsetec/js/plugins/taginputs/jquery.tagsinput.js') }}"></script>
<script type="text/javascript">
$('#tagit').tagsInput();
$("#tagit_tag").on('paste',function(e){
    var element=this;
    setTimeout(function () {
        var text = $(element).val();
        var target=$("#tagit");
        var tags = (text).split(/[ ,]+/);
        for (var i = 0, z = tags.length; i<z; i++) {
              var tag = $.trim(tags[i]);
              if (!target.tagExist(tag)) {
                    target.addTag(tag);
              }
              else
              {
                  $("#tagit_tag").val('');
              }
                
         }
    }, 0);
});

var cats = $('#cats').val();
var subcat = $('#subcat').val();
$.ajaxSetup({
	headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
});
$.ajax({
	type: 'POST',
	url: '<?php echo e(\URL::to("course/subcategory")); ?>',
	data:'cats='+cats+'&subcat='+subcat,
	success : function(data) {
		$('#sub_cats').html(data);
	}
});
$(document).on('change','#cats',function(){
	var cats = $(this).val();
	$.ajaxSetup({
		headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
	});
	$.ajax({
		type: 'POST',
		url: '<?php echo e(\URL::to("course/subcategory")); ?>',
		data:'cats='+cats+'&subcat=0',
		success : function(data) {
			$('#sub_cats').html(data);
		}
	});

});

</script>
@stop