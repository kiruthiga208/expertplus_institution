<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{!! Lang::get('core.take')!!} </title>
<meta name="keywords" content="">
<meta name="description" content=""/>
<link rel="shortcut icon" href="{{ asset('uploads/images/'.CNF_FAV)}}" type="image/x-icon">	
<link href="{{ asset('assets/bsetec/themes/theme1/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/themes/theme1/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/css/icons.min.css') }}" rel="stylesheet">


<link href="{{ asset('assets/bsetec/static/css/video-js.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/js/plugins/toastr/toastr.css')}}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/coursepreview.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/js/plugins/iCheck/skins/square/green.css')}}" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600italic,700,800,800italic,700italic,600,400italic,300,300italic' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/easing.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/themes/theme1/js/bootstrap.min.js') }}"></script>	
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/toastr/toastr.js') }}"></script>
<!-- <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/tinymce/jscripts/tiny_mce/tiny_mce.js') }}"></script> -->
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/iCheck/icheck.min.js') }}"></script>
<noscript><meta http-equiv="refresh" content="0;url={{ URL::TO('nojs.php') }}"></noscript>
<link href="{{ asset('assets/bsetec/js/plugins/redactor/css/redactor.css') }}" rel="stylesheet">

<script type="text/javascript">
$(document).on('contextmenu', 'video,audio', function(e) {
        e.preventDefault();
    });
</script>
<style>
body{background:#333333;height: 100% !important; color:#FFF;}
html, body, .container-fluid, .row { height: 100%;}

</style>
    
</head>
<body>
<div class="taken-course">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-8 contentView nopadding">
			
				<div class="blackhead">
					<a href="{{ URL::to('courseview/'.$courseid.'/'.$slug) }}"><i class="fa fa-angle-left"></i> {!! Lang::get('core.back_course') !!}</a>
		
					<span class="pull-right righthide"><i class="fa fa-angle-right"></i></span>
					<span class="pull-right use-padding">
						@if(count($getdata)>0)
						{!! Form::label('status','To Complete')!!}
						{!! Form::checkbox('staus', 'value', $completion_status,array('class'=>'updatelstatus','id'=>'lstatusid','data-lid'=> $getdata['0']->lecture_quiz_id,'data-courseid'=>$courseid)); !!}
						@endif
					</span>
				</div>
			<div class="clr-b clearfix">
				<div class="titlehead">
					@if(count($curriculum) > 0)
						@if(count($getdata)> 0) 
							@php  $sec_count = 1; @endphp
							@for($i=0;$i < count($curriculum);$i++)
								@if($curriculum[$i]->section_id == $getdata['0']->section_id)
									@php $section = $sec_count.' - '.$curriculum[$i]->title; @endphp
								@endif
								@php  $sec_count++; @endphp
							@endfor
							<h2>{!! Lang::get('core.sec') !!} @if(!empty($section)) {!! $section !!} @endif </h2>
							<h2><span>{!! $getdata['0']->title !!}</span></h2>
						@endif
					@endif
				</div>
                
            <!--     <div class="arw-nxt">
                 <a href=""><i class="fa fa-angle-left"></i></a>
                 <a href=""><i class="fa fa-angle-right"></i></a>
                 
                 </div>-->
                <div class="arw-nxt">
                	@if($prev)
					<?php $combine = $prev;  $encryptid = \SiteHelpers::encryptID($combine); ?>
					<a href="{{ URL::to('learn-course/'.$courseid.'/'.$slug.'/'.$encryptid) }}"><i class="fa fa-angle-left"></i></a>
					@endif 
					@if($next)
					<?php $combine = $next;  $encryptid = \SiteHelpers::encryptID($combine); ?>
					<a href="{{ URL::to('learn-course/'.$courseid.'/'.$slug.'/'.$encryptid) }}"><i class="fa fa-angle-right"></i></a>
					@endif 
					
                </div>
                </div>
				
				
				<div class="contenthead text-center">

					{!! Form::open(array('url'=>'course/updatecourse', 'method' => 'post', 'class'=>'form-horizontal saveLabel','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
					{!! Form::hidden('courseid', $courseid )  !!}
					<?php
					if(count($getdata)>0 && isset($getdata['0']->media_type) && $getdata['0']->media_type=='0'){ 
						$filename1 = $getdata['0']->video_title.'.mp4'; 
						$filename2 = $getdata['0']->video_title.'.webm'; 
						$filename3 = $getdata['0']->video_title.'.ogv';  ?>
						
						<?php if($getdata['0']->aws_url!=''){
						?>
						<video id='lecture_video' class='video-js vjs-default-skin' controls preload='auto' width='720' height='480' data-setup='{}'><source src="{{ $getdata['0']->aws_url.$filename1 }}" type="video/mp4" id="videosource"><source src="{{ $getdata['0']->aws_url.$filename2 }}" type="video/webm" id="videosource"><source src="{{ $getdata['0']->aws_url.$filename3 }}" type="video/ogg" id="videosource"></video>
						<?php } else { ?>
						<video id='lecture_video' class='video-js vjs-default-skin' controls preload='auto' width='720' height='480' data-setup='{}'><source src="{{ asset('/uploads/videos/'.$filename1) }}" type="video/mp4" id="videosource"><source src="{{ asset('/uploads/videos/'.$filename2) }}" type="video/webm" id="videosource"><source src="{{ asset('/uploads/videos/'.$filename3) }}" type="video/ogg" id="videosource"></video>
						<?php } ?>

						{!! Form::hidden('mediatype', $getdata['0']->media_type )  !!}
						{!! Form::hidden('lectureid', $getdata['0']->lecture_quiz_id )  !!}
						{!! Form::hidden('sectionid', $getdata['0']->section_id )  !!}
						<?php 
					}elseif(count($getdata)>0 && isset($getdata['0']->media_type) && $getdata['0']->media_type=='1'){ 
						$filename = $getdata['0']->file_name.'.'.$getdata['0']->file_type;
						if($getdata['0']->aws_url!='' && $getdata['0']->processed=='1'){
						?>
						<audio controls id='lecture_audio'>
							<source src="{{ $getdata['0']->aws_url.$filename }}" type="audio/mpeg">{!! Lang::get('core.browser_support') !!}</audio>
						<?php } else { ?>
								<audio controls id='lecture_audio'>
									<source src="{{ asset('/uploads/files/'.$filename) }}" type="audio/mpeg">
										{!! Lang::get('core.browser_support') !!}
									</audio>
									<?php } ?>
							{!! Form::hidden('mediatype', $getdata['0']->media_type )  !!}
							{!! Form::hidden('lectureid', $getdata['0']->lecture_quiz_id )  !!}
							{!! Form::hidden('sectionid', $getdata['0']->section_id )  !!}
							<?php 
						}elseif(count($getdata)>0 && isset($getdata['0']->media_type) && $getdata['0']->media_type=='2'){
							$encryptid = \SiteHelpers::encryptID($getdata['0']->id);
							$filepath = URL::to('course/toread/'.$encryptid); ?>
							<iframe src="{!! $filepath !!}#zoom=200" width="100%" height="400px" top="100" class="pdfiframe"></iframe>
							{!! Form::hidden('mediatype', $getdata['0']->media_type )  !!}
							{!! Form::hidden('lectureid', $getdata['0']->lecture_quiz_id )  !!}
							{!! Form::hidden('sectionid', $getdata['0']->section_id )  !!}
						<?php }elseif(count($getdata)>0 && isset($getdata['0']->media_type) && $getdata['0']->media_type=='3' || $getdata['0']->media_type=='6'){ ?>
							<div class="text-left">{!! $getdata['0']->contenttext or '' !!}</div>
							@php $youtube = \bsetecHelpers::ifInstalled('youtube') @endphp
							@if($youtube>0)
							<div class="youtube_video">
								<div id="div_video"> </div>

							<script type="text/javascript">
							 $( document ).ready(function() {
							 		
							  var check_url = '{!! $getdata['0']->youtube_id or '' !!}';

							  if(check_url != ''){

							      var url = '{!! $getdata['0']->youtube_id or '' !!}';
							    
							  $.post( "{!! url('api_youtube/getvideo.php') !!}", { videoid: url, type: "Download" })
							  .done(function( data ) {
							     var json = $.parseJSON(data);
							     var urls = "{!! url('api_youtube') !!}/"+json.url;
							     document.getElementById('div_video').innerHTML = '<video autoplay controls id="video_ctrl" width="400px" height="300px"><source src="'+urls+'" type="video/mp4"></video>';
							    document.getElementById('video_ctrl').play();
							  });
							  }																
							 	});
							</script>

							</div>
							@endif
							{!! Form::hidden('mediatype', $getdata['0']->media_type )  !!}
							{!! Form::hidden('lectureid', $getdata['0']->lecture_quiz_id )  !!}
							{!! Form::hidden('sectionid', $getdata['0']->section_id )  !!}
						<?php
						} 
						?>
						
						{!! Form::close() !!}

						<?php

						$path =  url('')."/uploads/files/chart_14437659073265.pdf";
						// echo '<iframe src="http://localhost/eplus/uploads/files/1_14437658697565.pdf" width="100%" height="400px" top="100" class="pdfiframe"></iframe>';

						?>

					</div>
				</div>
                
				@include('course/right-tab-learn')
                
		</div>
       </div>
</div>
		

	<script>
		$(document).ready(function(e){
				@if(!$completion_status)
					var lid 		= $('.updatelstatus').data('lid');
					var courseid 	= $('.updatelstatus').data('courseid');
					updatestatus(lid,courseid,0);
				@endif
			});
	</script>			


		</body> 
		</html>