@extends('layouts.frontendfullwidth')
@section('content')
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600italic,700,800,800italic,700italic,600,400italic,300,300italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,900italic,700italic,900,700,500italic,400italic,500,300,300italic,100italic,100' rel='stylesheet' type='text/css'>
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/coursepreview.css') }}" rel="stylesheet">
<script src="{{ asset('assets/bsetec/static/js/jquery.raty.min.js') }}"></script>
<style> 
</style>
<!--Description-->
<div id="wrapper">
	<div class="course-header clearfix">
		<div class="course-header-content"> 
			<div class="container">
				<div class="row">
					<div class="col col-lg-7 col-md-6 fxacw course-header-wrapper">
						<div class="flex">
							<h1 class="course-title ellipsis">{!! $courseinfo->course_title !!}</h1>
						</div>
					</div>
					<div class="col col-lg-5 col-md-6 fxacw course-header-wrapper sidehead-container">

						<div class="instructors-container">
							@php  $courseuserinfo = \bsetecHelpers::getuserinfobyid($courseinfo->user_id) @endphp
							<div class="instructor-img"><a href="{{ URL::to('profile/'.$courseuserinfo->username) }}" target="_blank">{!! SiteHelpers::customavatar($courseuserinfo->email,$courseuserinfo->id) !!}</a></div>
							<div class="instructor-title"><a href="{{ URL::to('profile/'.$courseuserinfo->username) }}" target="_blank">{!! $courseuserinfo->first_name.' '.$courseuserinfo->last_name !!}</a></div>
						</div>

						<ul class="course-header-buttons-container">
							
							<li class="btn-group">
								<a class="course-header-icon pull-tooltip-down course-info" href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="Course Information">
									<i class="fa fa-info-circle"></i>
								</a>
							</li>

							@if($coursesettings > 0)
							<li class="btn-group">
								<a class="course-header-icon dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)" aria-expanded="false">
									<i class="fa fa-cog"></i>
								</a>
								<div class="dropdown-menu pull-right">
									<h3>{!! Lang::get('core.course_settings')!!}</h3>
									<input type="hidden" id="course_notification_id" value="@if(isset($coursenotification->id) && !empty($coursenotification->id)) {!! $coursenotification->id !!} @else{!!'0'!!}@endif" />
									<ul class="checkbox-inputs">
										<li class="">
											<div class="switcher-wrapper clearfix">
												<label for="lecture-notification">{!! Lang::get('core.lecture_email') !!}</label>
												<input type="checkbox" class="course_notification" id="lecture-notification" value="1" @if(isset($coursenotification->new_lecture_emails) && $coursenotification->new_lecture_emails == '1') checked="checked" @endif>
											</div>
										</li>
										<li class="">
											<div class="switcher-wrapper clearfix">
												<label for="new-course-announcement">{!! Lang::get('core.announcement_email')!!}</label>
												<input type="checkbox" class="course_notification" id="new-course-announcement" value="2" @if(isset($coursenotification->new_announcement_emails) && $coursenotification->new_announcement_emails == '1') checked="checked" @endif>
											</div>
										</li>
										<li class="">
											<div class="switcher-wrapper clearfix">
												<label for="new-course-promotion">{!! Lang::get('core.promotional_email')!!}</label>
												<input type="checkbox" class="course_notification" id="new-course-promotion" value="3" @if(isset($coursenotification->promotional_emails) && $coursenotification->promotional_emails == '1') checked="checked" @endif>
											</div>
										</li>
									</ul>
									<div class="unsub-wrapper">
										<a href="javascript:void(0)" class="text-right unenroll"><span class="text-info"> {!! Lang::get('core.unenroll')!!}</span></a>
									</div>
								</div>
							</li>
							@endif

							<li class="btn-group">
								<a id="reportabuse" class="course-header-icon pull-tooltip-down" href="javascript:void(0)" data-toggle="tooltip" data-placement="bottom" title="{!! Lang::get('core.report')!!}">
									<i class="fa fa-flag"></i>
								</a>
							</li>
							  <meta property="og:type" content="expert_com:course" />
						      <meta property="og:url" content="{!! Request::url() !!}" />
						      <meta property="og:title" content="{!! $courseinfo->course_title !!}" />
						      <meta property="og:description" content="{!! strip_tags($courseinfo->description) !!}" />
						      <meta property="og:image" content="{!! 
						      \bsetecHelpers::getImage($courseinfo->image); !!}" />
						      <meta property="og:site_name" content="Expertplus" />
						      <meta property="og:locale" content="en_US" />

						      <meta name="twitter:card" content="summary" />
						      <meta name="twitter:url" content="{!! Request::url() !!}" />
						      <meta name="twitter:title" content="{!! $courseinfo->course_title !!}" />
						      <meta name="twitter:description" content="{!! strip_tags($courseinfo->description) !!}" />
						      <meta name="twitter:image" content="{!! \bsetecHelpers::getImage($courseinfo->image); !!}" />
						      <meta name="twitter:site" content="@expertsite" />

							<li class="btn-group" ng-if="$root.isSocialSharingEnabled">
								<a class="course-header-icon dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)" aria-expanded="false">
									<i class="fa fa-share-square-o"></i>
								</a>
								<ul class="dropdown-menu pull-right share-links-icons icons-ul">
									<li>
										<a class="fb" target="_blank" href="https://www.facebook.com/dialog/feed?app_id={!! \Config::get('services.facebook.client_id') !!}&display=popup&link={{ Request::url() }}&redirect_uri={{ Request::url() }}&picture={!! \bsetecHelpers::getImage($courseinfo->image); !!}&name={!! urlencode($courseinfo->course_title) !!}&description={!! urlencode(strip_tags($courseinfo->description)) !!}">
											<i class="fa fa-facebook"></i>
											<span><span class="">{!! Lang::get('core.share_facebook')!!} </span></span>
										</a>
									</li>
									<li>
										<a class="tw" target="_blank" href="https://twitter.com/share">
											<i class="fa fa-twitter"></i>
											<span><span class="">{!! Lang::get('core.share_twitter')!!}</span></span>
										</a>
									</li>
									<li>
										<a class="tw" target="_blank" href="https://plusone.google.com/_/+1/confirm?hl=en&amp;url={{ Request::url() }}">
											<i class="fa fa-google-plus"></i>
											<span><span class="">{!! Lang::get('core.share_google')!!}</span></span>
										</a>
									</li>
									<li>
										<a class="tw" href="javascript:void(run_pinmarklet())">
											<i class="fa fa-pinterest"></i>
											<span><span class="">{!! Lang::get('core.share_pin')!!}</span></span>
										</a>
									</li>
									<li>
										<a class="tw" target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url={{ Request::url() }}">
											<i class="fa fa-linkedin"></i>
											<span><span class="">{!! Lang::get('core.share_linkedin')!!}</span></span>
										</a>
									</li>
									
								</ul>
							</li>
							<li>
								@if($courseinfo->user_id!=session::get('uid'))
								<button type="button" class="btn btn-orange btn-bg" id="ratings">{!! Lang::get('core.rate_course')!!}</button>
								@endif
							</li>
						</ul>
						<div class="clear"></div>


					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="course_student_page">
		<div class="gray-bg" id="page-wrapper-full">
			<div class="container">
				<div class="row">

					<div class="main_second_section">
						<div class="container">
							<div class="row">
								<div class="col-lg-6 col-md-6 photography_section">


									<div class="user-course-progress">
										<div class="user_course_progress_stat">{!! Lang::get('core.completed') !!} {!! $courselecturecompleted !!} {!! Lang::get('core.completed1') !!} {!! $courselecturetotal !!} {!! Lang::get('core.completed2') !!}<div class="course_certification">
										@if($course_completion)
										<a href="{{ URL::to('course/coursecertificate').'/'.$courseid }}" target="_blank"><i class="fa fa-trophy"></i></a>
										@else
										<a href="javascript::"><i class="fa fa-trophy"></i></a>
										@endif
										</div></div>
										<div class="ucp-progress-bar progress">
											<div class="bar bar-success progress-bar" style="width:@if(!empty($courselecturetotal)){!! ($courselecturecompleted / $courselecturetotal) * 100 !!}%; @else 0%; @endif"></div>
											<div class="progress_end_bar"></div>
										</div>
									</div>

								

									<div class="course_student_curriculam">

										@if(count($curriculum)>0)
										@php  $no = 1; 
										 $lno = 1;
										$qno = 1;  @endphp
										@for($i=0;$i<count($curriculum);$i++)
										@php $lectures = \SiteHelpers::getlectures($curriculum[$i]->section_id); @endphp
										<ul class="curriculum-items-list">
											<li class="curriculum-section-container">
												<ul>
													<li class="section-title">
														<h5>
															<span translate=""><span class="">{!! Lang::get('core.section') !!} </span></span>{!! $no !!} - {!! $curriculum[$i]->title !!}
														</h5>
													</li>
													@for($j=0;$j<count($lectures);$j++)
													@php $combine = $lectures[$j]->lecture_quiz_id;  $encryptid = \SiteHelpers::encryptID($combine); @endphp

													<li class="curriculum-item-container">

														<div class="ci-progress-container">
															@php $lecstatus = 'none'; @endphp
															@if(!empty($courseprogress)) @foreach($courseprogress as $cp) @if($cp->lecture_id == $lectures[$j]->lecture_quiz_id && $cp->status == 1) @php $lecstatus = 'completed'; @endphp <span class="ci-progress-mask perc-100" style="width: 100%;"></span> @elseif($cp->lecture_id == $lectures[$j]->lecture_quiz_id && $cp->status == 0) @php $lecstatus = 'incomplete'; @endphp <span class="ci-progress-mask" style="width: 100%;"></span> @endif @endforeach @endif
														</div>

														<div class="ci-info">
															<div class="ci-title">
																@if($lectures[$j]->type==0)
																<span><span class="">  {!! Lang::get('core.Lecture') !!}</span></span>
																<span class="">{!! $lno !!}</span>:
																@else
																<span><span class=""> {!! Lang::get('core.Quiz') !!}</span></span>
																<span class="">{!! $qno !!}</span>:
																@endif
																<span class="title">{{$lectures[$j]->title}}</span>
															</div>
															@if($lectures[$j]->media_type=='0')
															@if(isset($lectures[$j]->duration))
															<div class="ci-details-container clearfix">
																<span class="ci-details course_media_{!! $lectures[$j]->media_type !!}">
																	<span>{!! $lectures[$j]->duration !!}</span>
																</span>
															</div>
															@endif
															@elseif($lectures[$j]->media_type=='1' || $lectures[$j]->media_type=='2' || $lectures[$j]->media_type=='5')
															@php $durations = \SiteHelpers::getlecturesfiles($lectures[$j]->lecture_quiz_id); @endphp
															@if(isset($lectures[$j]->duration))
															<div class="ci-details-container clearfix">
																<span class="ci-details course_media_{!! $lectures[$j]->media_type !!}">
																	<span>{!! $durations['0']->duration !!}</span>
																</span>
															</div>
															@endif
															@elseif($lectures[$j]->media_type=='3')
															<div class="ci-details-container clearfix">
																<span class="ci-details course_media_{!! $lectures[$j]->media_type !!}">
																	<span>Text</span>
																</span>
															</div>
															@elseif($lectures[$j]->type==1)
															<div class="ci-details-container clearfix">
																<span class="ci-details course_media_q">
																	@php  $quizcount = \SiteHelpers::getquizcount($lectures[$j]->lecture_quiz_id); @endphp
																	<span> {!!  $quizcount !!} @if($quizcount > 1) Questions @else Question @endif</span>
																</span>
															</div>
															@endif
														</div>
														<div class="lecture_link_state"><a class="btn btn-primary" href="{{ URL::to('learn-course/'.$courseid.'/'.$slug.'/'.$encryptid) }}">@if(isset($lecstatus) && $lecstatus == 'completed') restart @elseif(isset($lecstatus) && $lecstatus == 'incomplete') resume @else start @endif @if($lectures[$j]->type==0) lecture @else quiz @endif</a></div>
													</li>
													@if($lectures[$j]->type==0) @php $lno++; @endphp @else @php $qno++; @endphp @endif
													@endfor

												</ul>
											</li>
										</ul>
										@php $no++; @endphp
										@endfor
										@endif

									</div>
									


									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" name="courseid" value="{!! $courseid or '' !!}">
								</div>
								<div class="col-lg-6 col-md-6 photography_section">

									<div class="tab-container">
										<ul class="nav nav-tabs preview-tabs">
											<li class="active"><a data-toggle="tab" href="#discussiontab"><i class="icon-bubbles2"></i> {!! Lang::get('core.Discussions') !!}</a></li>
											<li><a data-toggle="tab" href="#announcementtab"><i class="fa fa-bullhorn"></i> {!! Lang::get('core.Announcements') !!}</a></li>
											<li><a href="{{ URL::to('courseusers/'.$courseinfo->course_id) }}"><i class="fa fa-users"></i> {!! Lang::get('core.students') !!}</a></li>
										</ul>
										<div class="tab-content">
											<div id="discussiontab" class="tab-pane active use-padding">
												<div class="col-lg-12 text-center top-discuss">
													<input type="text" class="discussion-search" placeholder="{!! Lang::get('core.search_discussion')!!}" id="searchtext"> 
													<i class="discussion-search-icon fa fa-search"></i>
													<em class="padspace">{!! Lang::get('core.or') !!}</em>
													<button type="button" class="btn btn-orange btn-bg" id="adddiscussions">{!! Lang::get('core.btn_discussion')!!}</button>
												</div>
												<div class="para_lack_hut" id="allcomments">
													<div class="cloader"></div>
													<div class="ajax-comments">
													@include('course.ajax-comments')
													</div>
											</div>
										</div>
										<script>
											function ajaxtinymce(dynamic_class)
											{
												var align = 'ltr';
												@if(CNF_RTL==1)
 														var align = 'rtl';
												@endif	
												if(dynamic_class == 'undefined' || dynamic_class == null)
												{
													var selector = 'lectureeditor';
												}
												else
												{
													var selector = dynamic_class;
												}
												
												if ($("#"+selector+'_parent').length > 0) 
												{
													    
												}
												else
												{
													tinymce.init({	
														mode : "specific_textareas",
														editor_selector : selector,
														theme : "advanced",
														theme_advanced_buttons1 : "bold,italic,underline,bullist,numlist,link,image",
														theme_advanced_toolbar_location : "top",
														theme_advanced_toolbar_align : "left",
														theme_advanced_statusbar_location : "bottom",
														width : "100%",
														plugins : "paste",
														paste_text_sticky : true,
														directionality : align,
														setup : function(ed) {
															ed.onInit.add(function(ed) {
																ed.pasteAsPlainText = true;
															});
														}
													});
												}
												
											}
											$('body').on('click', '.ajax-comments-pagination a', function()
											{
												$('.profile').remove();
												$.ajax({
													type: 'GET',
													url: $(this).attr('href'),
													beforeSend:function(){
														$('.ajax-comments').html('<p style="font-size:20px;padding:40px;"><center><i class="fa fa-refresh fa-spin"></i> {!! Lang::get("core.loading")!!}</center></p>');
													},
													success : function(data) 
													{
														$('.ajax-comments').html(data);
														ajaxtinymce();
													}
												});	
												return false;																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																						
											});
											$('.load-more').click(function()
											    {
											        var this_id = $(this);
											        this_id.hide();
											        var comment_id = $(this).attr('data-id');
											        var offset = $("#replyies-"+comment_id).children('.discussion-comments-container').length;
											        $('div#loadmoreajaxloader-'+comment_id).show();
											        
											        $.ajax({
											        type: "POST",	
											        data: {offset:offset, comment_id:comment_id},
											        url: "{!! url('course/ajax-replies')!!}",
											        success: function(html)
											        {
											            this_id.show();
											            if(html)
											            {
											                $("#replyies-"+comment_id).append(html);
											                $('div#loadmoreajaxloader-'+comment_id).hide();
											                ajaxtinymce();
											            }else
											            {
											            	$('div#loadmoreajaxloader-'+comment_id).hide();
											                this_id.replaceWith('{!! Lang::get("core.no_more_replies")!!}');
											            }
											        }
											        });
											    });
										</script>
										<div id="announcementtab" class="tab-pane use-padding">
											@if($courseinfo->user_id==\Session::get('uid'))
											<div class="text-center top-discuss">
												<button type="button" class="btn btn-orange btn-bg" id="addannouncement">{!! Lang::get('core.add_announcement') !!}</button>
											</div>
											<div class="commentbox addannouncementbox" style="display:none;">
												<div class="form-group">
													<textarea id="announcementcomments" class="form-control lectureeditor" rows="2" placeholder="{!! Lang::get('core.announcement_discuss') !!}" name="comment" cols="50"></textarea> 
												</div> 
												<div class="form-group">              
													<button class="btn btn-orange btn-bg postannouncement" type="button" >{!! Lang::get('core.Post')!!}</button>
												</div>
											</div>
											@endif
											<div id="allannouncement">
												@if(count($announcement)>0)

												@for($j=0;$j<count($announcement);$j++)
												<div class="media profile clearfix" id="announce-block{!!$announcement[$j]->announcement_id!!}">
													<div class="activity-post">
														<div class="profile_image">
															<a href="{{ URL::to('profile/'.$announcement[$j]->username) }}">{!! SiteHelpers::customavatar($announcement[$j]->email,$announcement[$j]->user_id) !!}</a>
														</div>
														<div class="activity-box discussion-activity-box">
															<div class="activity-header clearfix">
																<div class="header-right clearfix pull-right">
																	<div class="hidden-btns">
																		@if($announcement[$j]->user_id==\Session::get('uid'))
																		<a href="javascript:void(0);" data-acommentid="{!!$announcement[$j]->announcement_id!!}" class="editbtnannouncement"><i class="fa fa-pencil"></i></a>														
																		<a href="javascript:void(0);" data-acommentid="{!!$announcement[$j]->announcement_id!!}" class="removeannoucement"><i class="fa fa-trash-o"></i></a>
																		@endif
																	</div>
																</div>
																<div class="header-left clearfix">
																	<a href="{{ URL::to('profile/'.$announcement[$j]->username) }}"><span class="activity-header-link"><span><strong>{{$announcement[$j]->first_name}} {{$announcement[$j]->last_name or ''}}</strong></span></span></a>
																	<a>{!! Lang::get('core.post_announcement')!!} </a>
																	<span class="activity-actions-separator"> &middot; </span>
																	<time class="activity-time nowrap"><a>{!! SiteHelpers::changeFormat($announcement[$j]->created_at) !!}</a></time>
																</div>
															</div>
															<div>
																<div class="">
																	<div class="activity-body" id="editannouncement-{!!$announcement[$j]->announcement_id!!}">
																		{!! $announcement[$j]->announcement !!}
																	</div>
																	<div style="display:none;" id="announceopenbox-{!!$announcement[$j]->announcement_id!!}">
																		<div class="form-group">
																			<input type="hidden" name="pid" value="{!!$announcement[$j]->announcement_id!!}">
																		</div>
																		<div class="form-group">
																			<textarea id="editannouncementarea-{!!$announcement[$j]->announcement_id!!}" class="form-control lectureeditor" rows="2" placeholder="{!! Lang::get('core.comment')!!}" name="comment" cols="50">{!! $announcement[$j]->announcement; !!}</textarea>
																		</div> 
																		<div class="form-group">              
																			<button class="btn btn-orange btn-bg updateannouncebtn" type="button" data-commentid="{!!$announcement[$j]->announcement_id!!}" >{!! Lang::get('core.update')!!}</button>
																			<button class="btn btn-orange btn-bg editannouncementcancel" data-commentid="{!!$announcement[$j]->announcement_id!!}" type="button">{!! Lang::get('core.cancel')!!}</button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

												@endfor
												@else
												<h4 class="noannouncement"><center><em>{!! Lang::get('core.no_announcement') !!} </em></center></h4>
												@endif	
											</div>

										</div>
										<div id="studentstab" class="tab-pane use-padding">
											<p>
												{!! Lang::get('core.announcement_text')!!}</p>
											</div>
										</div>
									</div>
								</div>  
							</div>
						</div>
					</div>
				</div>
			</div>
			

		</div>
	</div>
</div>
<!-- review popup start-->
<div class="modal fade" id="ratingmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title ellipsis" id="exampleModalLabel">{!! Lang::get('core.rate')!!}:{!! $courseinfo->course_title !!}</h4>
			</div>
			<div class="modal-body" id="ratingbody">

			</div>
		</div>
	</div>
</div>
<!-- review popup end -->
<!-- add discussion popup start-->
<div class="modal fade" id="discussionmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title ellipsis" id="exampleModalLabel">{!! Lang::get('core.btn_discussion')!!}</h4>
			</div>
			<div class="modal-body" id="discussionbody">

			</div>
		</div>
	</div>
</div>
<!-- add discussion popup end -->
<!-- report abuse start-->
<div class="modal fade" id="reportmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" id="close_report" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title ellipsis" id="reportModalLabel">{!! Lang::get('core.report') !!} </h4>
			</div>
			<div class="modal-body" id="reportbody">
				{!! Form::open(array('method' => 'post','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
				<div class="form-group">
					<p>{!! Lang::get('core.report_abuse_txt')!!}</p>
				</div>
				<div class="form-group">
					<label for="recipient-name" class="control-label">{!! Lang::get('core.issue_type')!!}</label>
					<select class="select form-control" id="issue_type" name="issue_type"><option value="">Select Type</option><option value="Copyright or Trademark Violation">Copyright or Trademark Violation</option><option value="Inappropriate Course Content">Inappropriate Course Content</option><option value="Inappropriate Behaviour">Inappropriate Behaviour</option><option value="ExpertPlus Policy Violation">ExpertPlus Policy Violation</option><option value="Other">Other</option></select>
				</div>
				<div class="form-group">
					<label for="message-text" class="control-label">{!! Lang::get('core.details')!!}</label>
					<textarea class="form-control issue_details" name="issue_details" id="issue_details"></textarea>
				</div>
				<div class="form-group">
					<button type="button" id="saveissue" class="btn btn-orange">{!! Lang::get('core.sb_submit')!!}</button>
				</div>
				{!! Form::hidden('course_id', $courseid)  !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
<!-- report abuse end -->
<!-- course info start-->
<div class="modal fade" id="infomodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title ellipsis" id="infoModalLabel">{!! Lang::get('core.abt') !!}</h4>
			</div>
			<div class="modal-body" id="infobody">
				<h2>{!! $courseinfo->course_title !!}</h2>
				{!! $courseinfo->description !!}
			</div>
		</div>
	</div>
</div>
<!-- course info end -->


<!-- course privacy start-->
<div class="modal fade" id="passwordmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title ellipsis" id="infoModalLabel">{!! $courseinfo->course_title !!}</h4>
			</div>

			<div class="modal-body" id="passwordbody">

				<div class="form-group">
					<label for="recipient-name" class="control-label">{!! Lang::get('core.password')!!}: {!! \Session::get('cplogin') !!}</label>
					<input type="password" name="cpassword" class="form-control" placeholder="{!! Lang::get('core.enter_pwd') !!}" id="cpassword">
				</div>
				<div class="form-group">
					<input type="button" name="submits" data-cid="{!! $courseinfo->course_id or '' !!}" id="submitpassword" class="btn btn-orange btn-bg" value="Submit"><div class="passwordloader"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- course privacy end -->

@if(isset($recordings) && !empty($recordings))
<div class="modal fade" id="recordingshow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
    <div class="modal-dialog course_popup" role="document" style="width:90% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="btn_close" class="close removeframe" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel3">{!! Lang::get('core.recording_for') !!}</h4>
            </div>
            <div class="modal-body" style="max-height: none !important; ">
                <div class="row">
                    <iframe id="recordview" src="" style="width: 100%; height: 100%; min-height: 820px; border: none;"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(function(){
    $(document).on('click','.viewrecords',function(){
        var elem = $(this);
        var href = elem.attr('data-href');
        $('#recordview').attr('src',href);
        $('#recordingshow').modal({
            backdrop: 'static',
            keyboard: false
        });
    });
    $(document).on('click','.removeframe',function(){
        $('#recordview').attr('src','');
    });

});
</script>
@endif


<script type="text/javascript" src="<?php echo e(URL::to('assets/bsetec/js/plugins/tinymce/jscripts/tiny_mce/tiny_mce.js')); ?>"></script>
<script type="text/javascript">
$(function(){


$(document).on('click','.seereviews',function(){

  var next = $(this).data('next');
  var cid  = $(this).data('cid');
  var me = $(this);
  if ( me.data('requestRunning') ) {
    return;
  }
  me.data('requestRunning', true);

  $.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
  });
  $.ajax({
    type: 'POST',
    url: '<?php echo e(\URL::to("course/reviews")); ?>',
    data:'cid='+cid+'&next='+next,
    beforeSend: function() {
      $('.reviewloader').html('<h5><i class="fa fa-refresh fa-spin"></i> {!! Lang::get("core.loading") !!}</h5>');
    },
    success : function(data) {
     $('#ritems').append(data);
     $("#rpagination-"+next).remove();
     return false;

   },complete: function() {
    $('.reviewloader').html("");
    me.data('requestRunning', false);
  }
});


});

    $(document).on('click','.reply_review',function(e){
        var elem = $(this);
        var rate_id = elem.attr('data-id');
        var reply = elem.attr('data-reply');
        var user = elem.attr('data-user');
        $('#rate_id').val(rate_id);
        $('#replymsg').val(reply);
        $('#ruser_id').val(user);
        if(reply == ''){
            $('#savereply').text("{!! Lang::get('core.add_reply')!!}");
        } else {
            $('#savereply').text("{!! Lang::get('core.update_reply')!!}");
        }
        $('#reviewreplymodel').modal({
            keyboard: false
        });
    });

$('.ReadonlyRating').raty({score: function() {return $(this).attr('data-score');},readOnly: true,starOff : "{{url('').'/assets/bsetec/static/img/star-off.png'}}",starOn: "{{url('').'/assets/bsetec/static/img/star-on.png'}}",starHalf : "{{url('').'/assets/bsetec/static/img/star-half.png'}}"});
	
	@if(isset($showmeet) && $showmeet == true)
	getMeetings(0);
	function getMeetings(init){
		if(init == 0){
			$('.meeting_details').html('<span class="text-info"><i class="fa fa-refresh fa-spin"></i> {!! Lang::get("core.wait") !!}</span>');
			$('.meeting_details').removeClass('hide');
			init++;
		}
		var _token = $('[name="_token"]').val();
		var course_id = "{!! $courseinfo->course_id !!}";
		$.ajax({
			type: 'POST',
			url: '<?php echo e(\URL::to("course/goonline")); ?>',
			data:'course_id='+course_id+'&_token='+_token,
			success : function(data) {
				console.log(data);
				var obj = jQuery.parseJSON(data);
				if(obj.status=='1'){
					$('.meeting_details').html('<a href="'+obj.link+'" class="btn btn-orange"><i class="fa fa-hourglass-start"></i> {!! Lang::get("core.join_meeting") !!}</a>');
					$('.meeting_details').removeClass('hide');
				} else if(obj.status=='2'){
					$('.meeting_details').html('<span class="text-warning"><i class="fa fa-exclamation-circle"></i> {!! Lang::get("core.waiting_to_start") !!}</span>');
					$('.meeting_details').removeClass('hide');
				} else if(obj.status=='0'){
					$('.meeting_details').html('<span class="text-danger"><i class="fa fa-times-circle"></i> {!! Lang::get("core.course_failure") !!}</span>');
					$('.meeting_details').removeClass('hide');
				}
			}
		});
	}
	@endif

  $("#close_report").click(function () {
       tinyMCE.get('issue_details').setContent("");
        });	
  
			//COURSE SETTINGS
			function courseNotification(id,type,status){
				var courseid = $('[name="courseid"]').val();
				var _token = $('[name="_token"]').val();
				$.ajax({
					type: 'POST',
					url: '<?php echo e(\URL::to("course/coursenotification")); ?>',
					data:'id='+id+'&course_id='+courseid+'&type='+type+'&status='+status+'&_token='+_token,
					success : function(data) {
						$('#course_notification_id').val(data);
					}
				});
			}
			$('.course_notification').on('ifChecked', function(event){
				courseNotification($('#course_notification_id').val(),$(this).val(),'1');
			});
			$('.course_notification').on('ifUnchecked', function(event){
				courseNotification($('#course_notification_id').val(),$(this).val(),'0');
			});
			//UN ENROLL
			$(document).on('click','.unenroll',function(e){
				if(confirm("By Selecting this option, you will be permanantely removed from the course and will no longer have access to its contents. Are you sure?")){
					var courseid = $('[name="courseid"]').val();
					var _token = $('[name="_token"]').val();
					$.ajax({
						type: 'POST',
						url: '<?php echo e(\URL::to("course/unenroll")); ?>',
						data:'course_id='+courseid+'&_token='+_token,
						success : function(data) {
							window.location = data;
						}
					});
				}
			});
			// COURSE INFO
			$(document).on('click','.course-info',function(e){
				$('#infomodel').modal({
					keyboard: false
				});
			});
			// REPORT ABUSE
			$(document).on('click','#saveissue',function(e){
				$('#issue_type').removeClass('form-error');
				if($('#issue_type').val() == ''){
					$('#issue_type').addClass('form-error');
				} else if($.trim(tinyClean(tinyMCE.get('issue_details').getContent())) == ''){
					alert('Please enter details');
				} else {
					$('#issue_type').removeClass('form-error');
					$(this).attr('disabled','disabled');
					var courseid    = $('[name="courseid"]').val();
					var _token    = $('[name="_token"]').val();
					var issue_type    = $('#issue_type').val();
					var issue_details    = $.trim(tinyClean(tinyMCE.get('issue_details').getContent()));
					$.ajax({
						type: 'POST',
						url: '<?php echo e(\URL::to("reportabuse/save")); ?>',
						data:'course_id='+courseid+'&type='+issue_type+'&details='+issue_details+'&_token='+_token+'&ajax-request=1',
						success : function(data) {
							$('#reportmodel').modal('hide');
							$('#issue_type').val('');
							tinyMCE.get('issue_details').setContent('');
							toastr.success("", "Thank you. We will review your report as soon as possible.");
							toastr.options = {
								"closeButton": true,
								"debug": false,
								"positionClass": "toast-bottom-right",
								"onclick": null,
								"showDuration": "300",
								"hideDuration": "1000",
								"timeOut": "5000",
								"extendedTimeOut": "1000",
								"showEasing": "swing",
								"hideEasing": "linear",
								"showMethod": "fadeIn",
								"hideMethod": "fadeOut",
								"preventDuplicates": true
							}
							$('#saveissue').removeAttr('disabled');
						}
					});
				}
			});
			$(document).on('click','#reportabuse',function(e){
				$('#issue_type').val('');
				ajaxtinymce('issue_details');
				$('#issue_type').removeClass('form-error');
				$('#reportmodel').modal({
					keyboard: false
				});
			});
			// RATINGS
			$(document).on('click','#ratings',function(e){
				$('#ratingmodel').modal({
					keyboard: false
				});
				$('#ratingbody').html('');
				$('#ratingbody').html('<div class="sending_loadergs"></div>');

				var courseid    = $('[name="courseid"]').val();
				var me = $(this);
				e.preventDefault();
				if ( me.data('requestRunning') ) {
					return;
				}
				me.data('requestRunning', true);
				$.ajaxSetup({
					headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
				});
				$.ajax({
					type: 'POST',
					url: '<?php echo e(\URL::to("course/ratingform")); ?>',
					data:'cid='+courseid,
					beforeSend: function() {
						$('.sending_loadergs').html('<h2><i class="fa fa-refresh fa-spin"></i> {!! Lang::get("core.loading") !!}</h2>');
					},
					success : function(data) {
						$('#ratingbody').html(data);
					},complete: function() {
						$('.sending_loadergs').html("");
						me.data('requestRunning', false);
					}
				});
			});

			//add discussion

			$(document).on('click','#adddiscussions',function(e){
				$('#discussionmodel').modal({
					keyboard: false
				});
				$('#discussionbody').html('');
				$('#discussionbody').html('<div class="sending_loadergs"></div>');

				var courseid    = $('[name="courseid"]').val();
				var me = $(this);
				e.preventDefault();
				if ( me.data('requestRunning') ) {
					return;
				}
				me.data('requestRunning', true);
				$.ajaxSetup({
					headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
				});
				$.ajax({
					type: 'POST',
					url: '<?php echo e(\URL::to("course/adddiscussions")); ?>',
					data:'cid='+courseid,
					beforeSend: function() {
						$('.sending_loadergs').html('<h2><i class="fa fa-refresh fa-spin"></i>{!! Lang::get("core.loading") !!}</h2>');
					},
					success : function(data) {
						$('#discussionbody').html(data);
						$('.nodiscussion').addClass('hide');
					},complete: function() {
						$('.sending_loadergs').html("");
						me.data('requestRunning', false);
					}
				});
			});

			//comments start here
			ajaxtinymce();
			

			$(document).on('click','.replybutton',function () { 
				var cid = $(this).data('commentid');
				ajaxtinymce('replytxt-'+cid);
				if ($('#openbox-'+cid).is(':visible')) { 
					$("#openbox-"+cid).slideUp(300); 
				} 
				if ($("#openbox-"+cid).is(':visible')) { 
					$("#openbox-"+cid).slideUp(300);
				} else {
					$("#openbox-"+cid).slideDown(300); 

				} 
			});

			$(document).on('click','.cancelbtn',function () { 
				var cid = $(this).data('commentid');
				if ($('#openbox-'+cid).is(':visible')) { 
					$("#openbox-"+cid).slideUp(300); 
				} 
				if ($("#openbox-"+cid).is(':visible')) { 
					$("#openbox-"+cid).slideUp(300);
				} else {
					$("#openbox-"+cid).slideDown(300); 

				} 
			});

			$(document).on('click','.editcancelbtn',function () { 
				var cid = $(this).data('commentid');
				if ($('#replyopenbox-'+cid).is(':visible')) { 
					$("#replyopenbox-"+cid).slideUp(300); 
					$('#replytext-'+cid).show();
				} 
				if ($("#replyopenbox-"+cid).is(':visible')) { 
					$("#replyopenbox-"+cid).slideUp(300);
					$('#replytext-'+cid).show();
				} else {
					$("#replyopenbox-"+cid).slideDown(300); 
					$('#replytext-'+cid).show();
				} 
			});

			$(document).on('click','.editccancelbtn',function () { 
				var cid = $(this).data('commentid');
				if ($('#commentopenbox-'+cid).is(':visible')) { 
					$("#commentopenbox-"+cid).slideUp(300); 
					$('#wholewrapper-'+cid).show();
				} 
				if ($("#commentopenbox-"+cid).is(':visible')) { 
					$("#commentopenbox-"+cid).slideUp(300);
					$('#wholewrapper-'+cid).show();
				} else {
					$("#commentopenbox-"+cid).slideDown(300); 
					$('#wholewrapper-'+cid).show();
				} 
			});

			$(document).on('click','.cancelbtn',function(e){
				var cid = $(this).data('commentid');
				var con = tinyMCE.get('textboxcontent-'+cid).setContent('');
			});
			
			$(document).on('click','.replybtn',function(e){
				var cid = $(this).data('commentid');
					var con = tinyMCE.get('textboxcontent-'+cid).getContent({format : 'text'});
					var textvalues =con.trim();
					if($.trim(textvalues).length===0){
						toastr.error("Error", "Sorry! Please enter valid comments.");
						toastr.clear();
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut",
							"preventDuplicates": true
						}
					}else if ($.trim(textvalues).length>0){
						var me = $(this);
						e.preventDefault();
						if ( me.data('requestRunning') ) {
							return;
						}
						me.data('requestRunning', true);
						var lectureid 	= $('[name="lectureid"]').val();
						$.ajaxSetup({
							headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
						});
						$.ajax({
							type: 'POST',
							url: '<?php echo e(\URL::to("course/insertlecturereply")); ?>',
							data:'commentid='+cid+'&comments='+textvalues+'&lid='+cid,
							success : function(data) {
								var obj = jQuery.parseJSON(data);
								if(obj.status=='1'){
									$('#openbox-'+cid).hide();
									// $('#textboxcontent-'+cid).val('');

									tinyMCE.get('textboxcontent-'+cid).setContent('');
									
									$('#replyies-'+obj.results.commentid).append('<div class="discussion-comments-container" id="reply-block'+obj.results.replyid+'"> <li class=""> <div> <div class="comment-box"> <div class="profile_image"><a href="'+obj.results.user_link+'">'+obj.results.imgpath+'</a> </div> <div class="activity-box"> <div class="activity-header"> <div class="header-right clearfix pull-right"> <div class="hidden-btns"> <a data-rcommentid="'+obj.results.replyid+'" class="editreply activity-edit-link mini-act-btn mini-tooltip2"> <i class="fa fa-pencil"></i> </a> <a href="javascript:void(0)"  data-rcommentid="'+obj.results.replyid+'" class="removereply activity-delete-link mini-act-btn mini-tooltip2"> <i class="fa fa-trash-o"></i> </a> </div> </div> <div class="header-left"> <a class="activity-header-link" href="'+obj.results.user_link+'">'+obj.results.fname+'</a> <time class="activity-time"> &middot; Just Now</time> </div> </div> <div class="comment-body w3c-default"><span id="replytext-'+obj.results.replyid+'">'+obj.results.comments+'</span></div> </div> </div> </div> </li> <div style="display:none;" id="replyopenbox-'+obj.results.replyid+'"> <input type="hidden" name="pid" value="'+obj.results.replyid+'"> <div class="question-answers-creation"> <div class="comment-creation"> <div class="comment-submit-box"> <div class="form-group"> <textarea id="edittextboxcontent-'+obj.results.replyid+'" class="form-control lectureeditor" rows="2" placeholder="Comment" name="comment" cols="50">'+obj.results.comments+'</textarea> </div> <div class="form-group"> <button class="btn btn-orange btn-bg updatereplybtn" type="button" data-commentid="'+obj.results.commentid+'" data-replyid="'+obj.results.replyid+'">Save</button> <button class="btn btn-orange btn-bg editcancelbtn" data-commentid="'+obj.results.replyid+'" type="button">Cancel</button> </div> </div> </div> </div> </div> </div>');

										// ajaxtinymce();
										tinyMCE.execCommand('mceAddControl', false, 'edittextboxcontent-'+obj.results.replyid); 

										toastr.success("Success", "{!! Lang::get('core.discussion_thanks') !!}");
										toastr.options = {
											"closeButton": true,
											"debug": false,
											"positionClass": "toast-bottom-right",
											"onclick": null,
											"showDuration": "300",
											"hideDuration": "1000",
											"timeOut": "5000",
											"extendedTimeOut": "1000",
											"showEasing": "swing",
											"hideEasing": "linear",
											"showMethod": "fadeIn",
											"hideMethod": "fadeOut",
											"preventDuplicates": true
										}
									}else if(obj.status=='2'){
										toastr.error("Error", "Sorry! Something went wrong please try again later.");
										toastr.options = {
											"closeButton": true,
											"debug": false,
											"positionClass": "toast-bottom-right",
											"onclick": null,
											"showDuration": "300",
											"hideDuration": "1000",
											"timeOut": "5000",
											"extendedTimeOut": "1000",
											"showEasing": "swing",
											"hideEasing": "linear",
											"showMethod": "fadeIn",
											"hideMethod": "fadeOut",
											"preventDuplicates": true
										}
									}

									return false;
								},complete: function() {
									me.data('requestRunning', false);
								}
							});
}

});


$('#posttitle').focus(function(){
	$('.textareablock').css('display','block');

});

$(document).on('click','.removecomments',function(){
	var removeid  = $(this).data('commentid');
				// alert(removeid);

				var r = confirm("{!! Lang::get('core.co_delete_confirm')!!}");
				if (r == true) {
					$('#comment-'+removeid).remove();
					
					var me = $(this);
					if ( me.data('requestRunning') ) {
						return;
					}
					me.data('requestRunning', true);

					$.ajaxSetup({
						headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
					});
					$.ajax({
						type: 'POST',
						url: '<?php echo e(\URL::to("course/removelecturecomments")); ?>',
						data:'cid='+removeid,
						success : function(data) {
							
						},complete: function() {
							me.data('requestRunning', false);
						}
					});
					data_values = $('.profile').length;
					if(data_values = 3){
						$('.profile').remove();
						$.ajax({
							type: 'GET',
							url: '?page=1',
							beforeSend:function(){
								$('.cloader').html('<p style="font-size:20px;padding:40px;"><center><i class="fa fa-refresh fa-spin"></i> {!! Lang::get("core.loading")!!}</center></p>');
							},success : function(data){
								$('.ajax-comments').html(data);
							},complete:function(){
								$('.cloader').html('');
								$('#searchtext').val('');
							}
						});
					}


				}

			});

$(document).on('click','.removereply',function(){
	var removeid  = $(this).data('rcommentid');
				// alert(removeid);

				var r = confirm("{!! Lang::get('core.co_delete_confirm')!!}");
				if (r == true) {
					$('#reply-block'+removeid).remove();
					
					var me = $(this);
					if ( me.data('requestRunning') ) {
						return;
					}
					me.data('requestRunning', true);

					$.ajaxSetup({
						headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
					});
					$.ajax({
						type: 'POST',
						url: '<?php echo e(\URL::to("course/removelecturereply")); ?>',
						data:'rid='+removeid,
						success : function(data) {
							
						},complete: function() {
							me.data('requestRunning', false);
						}
					});

				}

			});


$(document).on('click','.editreply',function(){
	var replyid = $(this).data('rcommentid');
				// alert(replyid);
				$('#replytext-'+replyid).css('display','none');
				$('#replyopenbox-'+replyid).css('display','block');
				ajaxtinymce('replytext-'+replyid);
			});

$(document).on('click','.editcomments',function(){
	var cid = $(this).data('commentid');
				// alert(cid);
				$('#wholewrapper-'+cid).css('display','none');
				$('#commentopenbox-'+cid).css('display','block');
				ajaxtinymce('commenttext-'+cid);
			});

$(document).on('click','.updatereplybtn',function(e){
	var rid = $(this).data('replyid');
	var cid = $(this).data('commentid');
					var textvalues = tinyMCE.get('edittextboxcontent-'+rid).getContent();//$('#textboxcontent-'+cid).val();

					if($.trim(textvalues).length===0){
						toastr.error("Error", "{!! Lang::get('core.enter_valid_comments') !!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut",
							"preventDuplicates": true
						}
					}else if ($.trim(textvalues).length>0){
						var me = $(this);
						e.preventDefault();
						if ( me.data('requestRunning') ) {
							return;
						}
						me.data('requestRunning', true);
						var lectureid 	= $('[name="lectureid"]').val();
						$.ajaxSetup({
							headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
						});
						$.ajax({
							type: 'POST',
							url: '<?php echo e(\URL::to("course/updatelecturereply")); ?>',
							data:'commentid='+cid+'&comments='+textvalues+'&lid='+cid+'&rid='+rid,
							success : function(data) {
								var obj = jQuery.parseJSON(data);
								if(obj.status=='1'){
									$('#replytext-'+rid).show();
									$('#replyopenbox-'+rid).hide();
									// tinyMCE.get('edittextboxcontent-'+rid).setContent('');
									$('#replytext-'+rid).html(obj.results.comments);
									// $('#replyies-'+obj.results.commentid).append('<div id="reply-block'+obj.results.replyid+'" class="media"><hr><a href="" class="pull-right bla">'+obj.results.imgpath+'</a><a href="javascript:void(0);" data-rcommentid="'+obj.results.replyid+'" class="pull-right removereply"><i class="fa fa-trash-o"></i></a><div class="media-body"><h4 class="media-heading comment"><a href="">'+obj.results.fname+'</a><span class="msg-time pull-right"><span><abbr class="timeago">Just Now</abbr>&nbsp;</span></span></h4>'+obj.results.comments+'</div></div>');
									toastr.success("Success", "{!! Lang::get('core.discussion_thanks') !!}");
									toastr.options = {
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-bottom-right",
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"timeOut": "5000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut",
										"preventDuplicates": true
									}
								}else if(obj.status=='2'){
									toastr.error("Error", "{!! Lang::get('core.comments_error') !!}");
									toastr.options = {
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-bottom-right",
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"timeOut": "5000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut",
										"preventDuplicates": true
									}
								}

								return false;
							},complete: function() {
								me.data('requestRunning', false);
							}
						});
}

});



$(document).on('click','.updatecommentbtn',function(e){
	var cid  = $(this).data('commentid');
	var texttitles = $('#edittitle-'+cid).val();
					// var textvalues = $('#editctextboxcontent-'+cid).val();
					var textvalues = tinyMCE.get('editctextboxcontent-'+cid).getContent({format : 'text'});

					// alert(textvalues);
					if($.trim(texttitles).length===0){
						toastr.error("Error", "{!! Lang::get('core.discussion_valid_title') !!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut",
							"preventDuplicates": true
						}
					}else if($.trim(textvalues).length===0){

						toastr.error("Error", "{!! Lang::get('core.enter_valid_comments') !!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut",
							"preventDuplicates": true
						}

					}else if ($.trim(textvalues).length>0 && $.trim(texttitles).length>0){
						var me = $(this);
						var lectureid 	= $('[name="lectureid"]').val();
						var sectionid 	= $('[name="sectionid"]').val();
						e.preventDefault();
						if ( me.data('requestRunning') ) {
							return;
						}

						me.data('requestRunning', true);

						$.ajaxSetup({
							headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
						});
						$.ajax({
							type: 'POST',
							url: '<?php echo e(\URL::to("course/updatelecturecomments")); ?>',
							data:'comments='+textvalues+'&title='+texttitles+'&lid='+lectureid+'&cid='+cid,
							success : function(data) {
								var obj = jQuery.parseJSON(data);
								if(obj.status=='1'){
									// $('#posttitle').val('');
									// tinyMCE.get('postcomments').setContent('');
									// $('.textareablock').css('display','none');
									$('#commentopenbox-'+cid).css('display','none');
									$('#wholewrapper-'+cid).css('display','block');
									$('#titletext-'+cid).html(texttitles);
									$('#bodytext-'+cid).html(textvalues);
									toastr.success("Success", "{!! Lang::get('core.disccusion_comment_success') !!}");
									toastr.options = {
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-bottom-right",
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"timeOut": "5000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut",
										"preventDuplicates": true
									}
								}else if(obj.status=='2'){
									toastr.error("Error", "{!! Lang::get('core.comments_error') !!}");
									toastr.options = {
										"closeButton": true,
										"debug": false,
										"positionClass": "toast-bottom-right",
										"onclick": null,
										"showDuration": "300",
										"hideDuration": "1000",
										"timeOut": "5000",
										"extendedTimeOut": "1000",
										"showEasing": "swing",
										"hideEasing": "linear",
										"showMethod": "fadeIn",
										"hideMethod": "fadeOut",
										"preventDuplicates": true
									}
								}

								return false;
							},complete: function() {
								me.data('requestRunning', false);
							}
						});
}

});

			//announcement start here

			$(document).on('click','.editbtnannouncement',function(){
				var announcementid = $(this).data('acommentid');
				// alert(replyid);
				$('#editannouncement-'+announcementid).css('display','none');
				$('#announceopenbox-'+announcementid).css('display','block');
			});

			$(document).on('click','.editannouncementcancel',function () { 
				var cid = $(this).data('commentid');
				if ($('#announceopenbox-'+cid).is(':visible')) { 
					$("#announceopenbox-"+cid).slideUp(300); 
					$('#editannouncement-'+cid).show();
				} 
				if ($("#announceopenbox-"+cid).is(':visible')) { 
					$("#announceopenbox-"+cid).slideUp(300);
					$('#editannouncement-'+cid).show();
				} else {
					$("#announceopenbox-"+cid).slideDown(300); 
					$('#editannouncement-'+cid).show();
				} 
			});


			$(document).on('click','.postannouncement',function(e){



				var textannouncement = tinyMCE.get('announcementcomments').getContent();

	// alert(textannouncement);
	if($.trim(textannouncement).length===0){

		toastr.error("Error", "{!! Lang::get('core.enter_valid_comments') !!}");
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut",
			"preventDuplicates": true
		}

	}else if ($.trim(textannouncement).length>0){
		var me = $(this);
		$('.postannouncement').html('<i class="fa fa-spinner fa-spin" aria-hidden="true"> </i> {{ Lang::get("core.loading") }}</div>');
		var courseid    = $('[name="courseid"]').val();

		e.preventDefault();
		if ( me.data('requestRunning') ) {
			return;
		}

		me.data('requestRunning', true);

		$.ajaxSetup({
			headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
		});
		$.ajax({
			type: 'POST',
			url: '<?php echo e(\URL::to("course/insertannouncement")); ?>',
			data:'comments='+textannouncement+'&cid='+courseid,
			success : function(data) {
				$('.postannouncement').html('{!! Lang::get('core.Post')!!}');
				var obj = jQuery.parseJSON(data);
				if(obj.status=='1'){
					$('.noannouncement').hide();

					tinyMCE.get('announcementcomments').setContent('');

					$('#allannouncement').append('<div class="media profile clearfix" id="announce-block'+obj.results.commentid+'"> <div class="activity-post"> <div class="profile_image"> <a href="">'+obj.results.imgpath+'</a> </div> <div class="activity-box discussion-activity-box"> <div class="activity-header clearfix"> <div class="header-right clearfix pull-right"> <div class="hidden-btns"> <a href="javascript:void(0);" data-acommentid="'+obj.results.commentid+'" class="editbtnannouncement"><i class="fa fa-pencil"></i></a> <a href="javascript:void(0);" data-acommentid="'+obj.results.commentid+'" class="removeannoucement"><i class="fa fa-trash-o"></i></a> </div> </div> <div class="header-left clearfix"> <a><span class="activity-header-link"><span><strong>'+obj.results.fname+'</strong></span></span></a> <a>posted an announcement </a> <span class="activity-actions-separator"> &middot; </span> <time class="activity-time nowrap"><a>Just Now</a></time> </div> </div> <div> <div class=""> <div class="activity-body" id="editannouncement-'+obj.results.commentid+'"> '+obj.results.comments+' </div> <div style="display:none;" id="announceopenbox-'+obj.results.commentid+'"> <div class="form-group"> <input type="hidden" name="pid" value="'+obj.results.commentid+'"> </div> <div class="form-group"> <textarea id="editannouncementarea-'+obj.results.commentid+'" class="form-control lectureeditor" rows="2" placeholder="Comment" name="comment" cols="50">'+obj.results.comments+'</textarea> </div>  <div class="form-group"> <button class="btn btn-orange btn-bg updateannouncebtn" type="button" data-commentid="'+obj.results.commentid+'" >Save</button> <button class="btn btn-orange btn-bg editannouncementcancel" data-commentid="'+obj.results.commentid+'" type="button">Cancel</button> </div> </div> </div> </div> </div> </div> </div>');

					tinyMCE.execCommand('mceAddControl', false, 'editannouncementarea-'+obj.results.commentid); 

					toastr.success("Success", "{!! Lang::get('core.announcement_success') !!}");
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut",
						"preventDuplicates": true
					}
				}else if(obj.status=='2'){
					toastr.error("Error", "{!! Lang::get('core.comments_error') !!}");
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut",
						"preventDuplicates": true
					}
				}

				return false;
			},complete: function() {
				me.data('requestRunning', false);
			}
		});
}

});

$(document).on('click','.updateannouncebtn',function(e){
	
	var cid = $(this).data('commentid');
	var textvalues = tinyMCE.get('editannouncementarea-'+cid).getContent();
	var courseid    = $('[name="courseid"]').val();
	if($.trim(textvalues).length===0){
		toastr.error("Error", "{!! Lang::get('core.discussion_valid_comments') !!}");
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut",
			"preventDuplicates": true
		}
	}else if ($.trim(textvalues).length>0){
		var me = $(this);
		e.preventDefault();
		if ( me.data('requestRunning') ) {
			return;
		}
		me.data('requestRunning', true);
		var lectureid 	= $('[name="lectureid"]').val();
		$.ajaxSetup({
			headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
		});
		$.ajax({
			type: 'POST',
			url: '<?php echo e(\URL::to("course/updateannoncement")); ?>',
			data:'commentid='+cid+'&comments='+textvalues+'&courseid='+courseid,
			success : function(data) {
				var obj = jQuery.parseJSON(data);
				if(obj.status=='1'){
					$('#editannouncement-'+cid).show();
					$('#announceopenbox-'+cid).hide();
					$('#editannouncement-'+cid).html(obj.results.comments);

					toastr.success("Success", "{!! Lang::get('core.announcement_edited') !!}");
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut",
						"preventDuplicates": true
					}
				}else if(obj.status=='2'){
					toastr.error("Error", "{!! Lang::get('core.discussion_wrong')!!}");
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut",
						"preventDuplicates": true
					}
				}

				return false;
			},complete: function() {
				me.data('requestRunning', false);
			}
		});
}

});
$(document).on('click','.removeannoucement',function(){
	var removeid  = $(this).data('acommentid');
				// alert(removeid);

				var r = confirm("{!! Lang::get('core.co_delete_confirm')!!}");
				if (r == true) {
					$('#announce-block'+removeid).remove();
					
					var me = $(this);
					if ( me.data('requestRunning') ) {
						return;
					}
					me.data('requestRunning', true);

					$.ajaxSetup({
						headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
					});
					$.ajax({
						type: 'POST',
						url: '<?php echo e(\URL::to("course/removeannoucement")); ?>',
						data:'rid='+removeid,
						success : function(data) {
							
						},complete: function() {
							me.data('requestRunning', false);
						}
					});

				}

			});

$(document).on('click','#addannouncement',function(e){
	$(this).hide();
	$('.addannouncementbox').slideDown();
});

		$('#searchtext').on('input',function(){
			var stext 		= $(this).val();
			var courseid    = $('[name="courseid"]').val();
			$.ajaxSetup({
				headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
			});
			if(stext.length==0){
				$.ajax({
							type: 'GET',
							url: '?page=1',
							beforeSend:function(){
								$('.cloader').html('<p style="font-size:20px;padding:40px;"><center><i class="fa fa-refresh fa-spin"></i> {!! Lang::get("core.loading")!!}</center></p>');
							},success : function(data){
								$('.ajax-comments').html(data);
							},complete:function(){
								$('.cloader').html('');
							}
						});
			}else{
				$.ajax({
					type: 'POST',
					url: '<?php echo e(\URL::to("course/searchdiscussion")); ?>',
					data:'stext='+stext+'&courseid='+courseid,
					beforeSend: function() {
						$('.cloader').html('<h5><center><i class="fa fa-refresh fa-spin"></i> {!! Lang::get("core.loading")!!}</center></h5>');
					},success : function(data) {
						$('.ajax-comments').html(data);
					},complete: function() {
						$('.cloader').html('');
					}
				});
			}
		});

		}); //end functions
		
function tinyClean(value) {
	value = value.replace(/&nbsp;/ig, ' ');
	value = value.replace(/\s\s+/g, ' ');
	if(value == '<p><br></p>' || value == '<p> </p>' || value == '<p></p>') {
		value = '';
	}
	return value;
}
</script>
@stop
