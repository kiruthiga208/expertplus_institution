@extends('layouts.mailtemplate')
@section('content')
<h2>Dear {!! $username !!}</h2>

<p>
		<a href="{!! $link !!}">{!! Lang::get('core.email_click') !!}</a>
</p>

<br /><br /><p> {!! Lang::get('core.thank_you') !!} </p>

{!! CNF_APPNAME !!} 
@stop