@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ ucfirst($pageTitle) }} <small>{{ ucfirst($pageNote) }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('departments?return='.$return) }}">{{ ucfirst($pageTitle) }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('departments?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if(Session::has('iid'))
	   		<a href="{{ URL::to('departments/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo ucfirst($pageTitle) ;?> <small>{{ ucfirst($pageNote) }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
				
				<tr>
				<td width='30%' class='label-view text-right'>Department</td>
				<td>{{ ucfirst($row->department) }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Institution</td>
				<td>{{ ucfirst(bsetecHelpers::getInstitutionName($row->institution_id)) }} </td>

				</tr>

				
				<tr>
				<td width='30%' class='label-view text-right'>Status</td>
				<td>
				@if($row->status =='1')
					<span class="">{{ Lang::get('core.fr_mactive') }}</span>
				@else
					<span class="">{{ Lang::get('core.fr_minactive') }}</span>
				@endif
				</td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Created At</td>
				<td>{{ $row->created_at }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Updated At</td>
				<td>{{ $row->updated_at }} </td>

				</tr>
				
		</tbody>	
	</table>    
	
	</div>
</div>	

	</div>
</div>
	  
@stop