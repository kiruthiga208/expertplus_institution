@extends('layouts.frontend')
@section('content')
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
         <style>
.nav-bar.learn-course-header li {
  float: left;
  font-size: 14px;
}
.nav-bar.learn-course-header li a{
  color: white;
}
</style>

<div class="description des_marg">

<div class="container">
<div class="row-fluid">
<div class="des_border span12">

<div class="des_header">
@if($status == "success")
<span class="des_bold des_color">{!! Lang::get('core.membership_payment_success')!!}</span>
 @else
 <span class="des_bold des_color">{!! Lang::get('core.membership_payment_fail')!!}</span>
 @endif

</div>

 <div class="des_con">
@if($status == "success")
<div class="success_img"></div>
 @else
<div class="failure_img"></div>
 @endif

 @if($transId !=0)
 <span class="readytosuccess id">{!! Lang::get('core.t_id')!!} : {{ $transId }}</span>
 @endif


 <!--<span class="readytosuccess  social_all"><span class="like_color">Like us</span> on Facebook and get Exculsive, Fan-only Offers.</span>-->
 <div class="des_ready">
  <span class="desready_p readytosuccess">{!! Lang::get('core.pay_ready')!!}</span>
 <div>
 @if($status == "success")
 <div class="access access_mrg">
 @if(isset($planId))
<a href="{{url('membership/courselist/'.$planId)}}">{!! Lang::get('core.go_course')!!}</a></div></div>
 @else
<a href="{{url('course')}}">{!! Lang::get('core.go_course')!!}</a></div></div>
 @endif
</div>
 @else
  <div class="access access_mrg">
<a href="{{url('user/membership')}}">{!! Lang::get('core.back_membership')!!}</a></div></div>
</div>
 @endif
</div>
</div>
</div>
</div>
</div>

<!--End Description-->
@stop
