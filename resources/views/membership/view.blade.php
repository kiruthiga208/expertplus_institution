@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('membership?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('membership?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('membership/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
	
				<tr>
				<td width='30%' class='label-view text-right'>Plan Id</td>
				<td>{{ $row->plan_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Plan Name</td>
				<td>{{ $row->plan_name }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Plan Amount</td>
				<td>{{ $row->plan_amount }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Plan Interval</td>
				<td>{{ $row->plan_interval }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Plan Periods</td>
				<td>{{ $row->plan_periods }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Status</td>
				@if($row->status==1)
				<td>{{ Lang::get('core.fr_mactive') }} </td>
				@else
				<td>{{ Lang::get('core.fr_minactive') }}
				@endif
				</tr>
				
		</tbody>	
	</table>    
	
	</div>
</div>	

	</div>
</div>
	  
@stop