@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('membership?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>

    </div>

 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content">

		 {!! Form::open(array('url'=>'membership/save?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
			<fieldset><legend> Membership</legend>

							<div class="form-group hidethis " style="display:none;">
							<label for="Plan Id" class=" control-label col-md-4 text-left"> Plan Id </label>
							<div class="col-md-6">
							{!! Form::text('plan_id', $row['plan_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
							</div>
							<div class="col-md-2">

							</div>
							</div>
							<div class="form-group  " >
							<label for="Plan Name" class=" control-label col-md-4 text-left"> Plan Name <span class="asterix"> * </span></label>
							<div class="col-md-6">
							{!! Form::text('plan_name', $row['plan_name'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!}
							</div>
							<div class="col-md-2">

							</div>
							</div>
							<div class="form-group  " >
							<label for="Plan Statement" class=" control-label col-md-4 text-left"> Plan Statement <span class="asterix"> * </span></label>
							<div class="col-md-6">
							<textarea name='plan_statement' rows='2' id='plan_statement' class='form-control '
			required  >{{ $row['plan_statement'] }}</textarea>
							</div>
							<div class="col-md-2">

							</div>
							</div>
							@if($row['plan_id'] != 4)
							<div class="form-group  " >
							<label for="Status" class=" control-label col-md-4 text-left"> Monthly Plan Status </label>
							<div class="col-md-6">

				<label class='radio radio-inline'>
				<input type='radio' name='status' value ='1'  @if($row['status'] != '0') checked="checked" @endif > Active </label>
				<label class='radio radio-inline'>
				<input type='radio' name='status' value ='0'  @if($row['status'] == '0') checked="checked" @endif > Inactive </label>
							</div>
							<div class="col-md-2">

							</div>
							</div>

							<div class="form-group  " >
							<label for="Plan Amount" class=" control-label col-md-4 text-left"> Monthly Plan Amount <span class="asterix"> * </span></label>
							<div class="col-md-6">
							{!! Form::text('plan_amount', $row['plan_amount'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true',$readonly  )) !!}
							</div>
							<div class="col-md-2">

							</div>
							</div>

							<div class="form-group  " >
							<label for="Status" class=" control-label col-md-4 text-left"> Yearly Plan Status </label>
							<div class="col-md-6">

				<label class='radio radio-inline'>
				<input type='radio' name='status_y' value ='1'  @if($row['status_y'] != '0') checked="checked" @endif > Active </label>
				<label class='radio radio-inline'>
				<input type='radio' name='status_y' value ='0'  @if($row['status_y'] == '0') checked="checked" @endif > Inactive </label>
							</div>
							<div class="col-md-2">

							</div>
							</div>

							<div class="form-group  " >
							<label for="Plan Amount" class=" control-label col-md-4 text-left"> Yearly Plan Amount <span class="asterix"> * </span></label>
							<div class="col-md-6">
							{!! Form::text('plan_amount_y', $row['plan_amount_y'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true',$readonly  )) !!}
							</div>
							<div class="col-md-2">

							</div>
							</div>
							<div class="form-group  hide" >
							<label for="Plan Interval" class=" control-label col-md-4 text-left"> Plan Interval <span class="asterix"> * </span></label>
							<div class="col-md-6">
							{!! Form::text('plan_interval', '1',array('class'=>'form-control', 'placeholder'=>'', 'parsley-type'=>'number'   )) !!}
							</div>
							<div class="col-md-2">

							</div>
							</div>
							<div class="form-group  hide" >
							<label for="Plan Periods" class=" control-label col-md-4 text-left"> Plan Periods <span class="asterix"> * </span></label>
							<div class="col-md-6">

				<?php $plan_periods = explode(',',$row['plan_periods']);
				$plan_periods_opt = array( '1' => 'Monthly' ,  '12' => 'Yearly' , ); ?>
				<select name='plan_periods' rows='5' required  class='select2 '  >
					<?php
					foreach($plan_periods_opt as $key=>$val)
					{
						echo "<option  value ='$key' ".($row['plan_periods'] == $key ? " selected='selected' " : '' ).">$val</option>";
					}
					?></select>
							</div>
							<div class="col-md-2">

							</div>
							</div>
							@else
							<input type="hidden" name="plan_amount" value="0">
							<input type="hidden" name="plan_amount_y" value="0">
							<input type="hidden" name="plan_interval" value="1">
							<input type="hidden" name="plan_periods" value="1">
							@endif
							 </fieldset>
		</div>


			<div style="clear:both"></div>


				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">
					@if(isset($row['plan_id']) && !empty($row['plan_id']))
					<button type="submit" name="submit" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					@else
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					@endif
					<button type="button" onclick="location.href='{{ URL::to('membership?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>

				  </div>

		 {!! Form::close() !!}
	</div>
</div>
</div>

@if(isset($row['plan_id']) && !empty($row['plan_id']) && $row['plan_id'] != 4)
<div class="col-sm-12 animated fadeInRight">
	<div class="sbox animated fadeInRight">
		<div class="sbox-title"> <h4> <i class="fa fa-table"></i> Included Courses </h4></div>
		<div class="sbox-content">
			<br>
			<div class="row form-group">
				<div class="col-sm-6">
					<select name='include_course_id' rows='5' id='include_course_id' class='select2 ' required></select>
				</div>
				<div class="col-sm-6">
					<a href="javascript:void(0)" class="tips btn btn-success addacourse" title="Add a Course"><i class="fa fa-plus"> Add a Course</i></a>
				</div>
			</div>
			<br>
			<div class="table-responsive" style="min-height:300px;">
			    <table class="table table-striped included_table">
			        <thead>
						<tr>
							<th class="number"> No </th>
							<th>Course Title</th>
							<th width="70" >{{ Lang::get('core.btn_action') }}</th>
						  </tr>
			        </thead>

			        <tbody>
						@if(isset($rowData))
			            @foreach ($rowData as $keyno => $rowd)
			                <tr id="included_{{ $rowd->membership_course_id }}">
								<td width="30"> {{ ++$keyno }} </td>
								<td width="30"> {{ $rowd->course_title }} </td>
							 <td>
								<a class="tips btn btn-xs btn-success remcourse" data-id="{{ $rowd->membership_course_id }}" data-cid="{{ $rowd->course_id }}" data-pid="{{ $rowd->plan_id }}" title="{{ Lang::get('core.btn_remove') }}"><i class="fa fa-trash-o "></i></a>
							</td>
			                </tr>
			            @endforeach
			            @endif
			        </tbody>
			    </table>
				<input type="hidden" name="md" value="" />
				</div>
		</div>
	</div>
</div>
@endif

</div>
   <script type="text/javascript">
	$(document).ready(function() {
		$("#include_course_id").jCombo("{{ URL::to('membership/courseselect?filter=course:course_id:course_title') }}",
			{  selected_value : '' });

		 $(document).on('click','.addacourse',function () {
	        var _token=$('[name="_token"]').val();
			var plan_id = $('[name="plan_id"]').val();
	        var course_id = $('#include_course_id').val();
	        if(course_id == ''){
				toastr.error("{{ Lang::get('core.error_msg') }}");
	        } else {
	        	var title = $('#include_course_id').select2('data').text;

		        $.ajax ({
		            type: "POST",
		            url:'{!! url('').'/membership/addacourse'!!}',
		            data: "&course_id="+course_id+"&plan_id="+plan_id+"&_token="+_token,
		            success: function (response)
		            {
						var data = jQuery.parseJSON( response );
						if(data.status == '1'){
			            	var rowCount = parseInt($('.included_table tr').length);
			                $('.included_table tbody').append('<tr id="included_'+data.id+'"><td width="30"> '+rowCount+' </td><td width="30"> '+title+' </td><td><a class="tips btn btn-xs btn-success remcourse" data-id="'+data.id+'" data-cid="'+course_id+'" data-pid="'+plan_id+'" title="" data-original-title="Remove"><i class="fa fa-trash-o "></i></a></td></tr>');
		                	toastr.success("Selected course has been added to this membership");
		                } else {
		                	toastr.warning("Selected course has already been added to this membership");
		                }
		            }
		        });
	        }
	    });

		$(document).on('click','.remcourse',function () {
	        $(this).text('Deleting...');
	        var _token=$('[name="_token"]').val();
	        var mc_id = $(this).data('id');
	        var course_id = $(this).data('cid');
	        var plan_id = $(this).data('pid');
	        $.ajax ({
	            type: "POST",
	            url:'{!! url('').'/membership/remcourse'!!}',
	            data: "&course_id="+course_id+"&plan_id="+plan_id+"&_token="+_token,
	            success: function (msg)
	            {
	                $('#included_'+mc_id).remove();
	                toastr.success("Course has been removed from this membership");
	            }
	        });
	    });

	});
	</script>
@stop
